# Installation

## Debian and derivations (Kubuntu, Neon, etc)

Use "apt install" to auto-install dependencies:

        sudo apt install zombietrackergps*_amd64.deb

## OpenSUSE

Use "zypper install" to auto-install dependencies.  **Warning: this is known to fail on Redhat and CentOS due to missing Qt dependencies.**

        sudo zypper install zombietrackergps*.x86_64.rpm

## Other Distros

It should compile from source since Qt 5.11.3. See BUILDING.md in this directory.
