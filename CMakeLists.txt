#-----------------------------------------------------------------------
# Copyright 2023 Loopdawg Software
# 
# ZombieTrackerGPS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------

set(LDUTILS            "ldutils")

# Default LDUTILS location, if not otherwise defined
if (NOT DEFINED LDUTILS_ROOT)
   set(LDUTILS_ROOT "${CMAKE_BINARY_DIR}/../lib${LDUTILS}")
endif()

include("${LDUTILS_ROOT}/share/cmake/${LDUTILS}/${LDUTILS}.cmake")

ldutils_get_package_version(PKGVERSION)
ldutils_compile_opts()

set(EXENAME            "zombietrackergps")
set(TESTNAME           "testztgps")
set(LIBNAME            "ztgps")
set(CMAKE_PREFIX_PATH  "${LDUTILS_ROOT}") # for find_package

cmake_minimum_required(VERSION 3.13.0)
project("${EXENAME}" VERSION "${PKGVERSION}" LANGUAGES CXX)
include(GNUInstallDirs)

find_package(${Qt} ${QtVer} REQUIRED Test)
find_package(${LDUTILS} ${PKGVERSION} REQUIRED)

# Update build date.  TODO: find a way that doesn't depend on a writable CMAKE_SOURCE_DIR.
# file(TOUCH_NOCREATE "${CMAKE_SOURCE_DIR}/src/core/builddate.cpp")

# Create targets
add_library(${LIBNAME} "")
add_executable(${EXENAME} "")
add_executable(${TESTNAME} "")
ldutils_tidy_autogen(${LIBNAME} ${EXENAME} ${TESTNAME})

add_subdirectory("src"       "${BUILD_DIR}/src")
add_subdirectory("tests/src" "${BUILD_DIR}/tests/src")
add_subdirectory("main"      "${BUILD_DIR}/main")
add_subdirectory("man"       "${BUILD_DIR}/man")
add_subdirectory("data"      "${BUILD_DIR}/data")
add_subdirectory("art"       "${BUILD_DIR}/art")
add_subdirectory("cmake"     "${BUILD_DIR}/cmake")
