.TH ZOMBIETRACKERGPS 1 2023-06-22 "version 1.15" "ZombieTrackerGPS Manual"

.SH NAME
zombietrackergps - GPS Track Manager for KDE

.SH SYNOPSIS
zombietrackergps [--help|--version|--pubkey]

zombietrackergps [--conf FILE] [--desktop NAME] [--no-first-run] [--private-session]

zombietrackergps [--help-batch | --help-filter --help-formats-input | --help-formats-output |
                  --help-fields trk|wpt | --help-units trk|wpt FIELD ]

.SH DESCRIPTION
ZombieTrackerGPS is a GPS track manager for KDE.  It can import several standard
GPS file formats such as GPX, TCX, FIT, and KML, and provides sophisticated query and
filtering abilities.  It offers a customizable user interface, session
management, and integration with the KDE Marble Map for use with OpenStreetMap
or other map providers.
.PP
ZombieTrackerGPS is designed for outdoor sports and recreation activities such
as bicycling, hiking, snowboarding, light aircraft, etc, and includes custom
icons for many popular outdoor activities.
.PP
A more comprehensive tutorial and documentation is available inside the program
and will be displayed automatically the first time the program is launched.  An
option to auto-import some sample GPS tracks to experiment with will also be
offered on the first run.
.PP
ZombieTrackerGPS can track both zombies and non-zombies.

.SH OPTIONS
.IP "--help, -?"
Display command line usage summary and exit.
.IP --version
Display program version and exit.
.IP --pubkey
Dump PGP public key suitable for the email given in AUTHOR below to stdout, and
exit.
.IP "--conf FILE"
Load the file named FILE as the initial configuration.  By convention this file
should end with '.conf'.  It stores the UI layout.  By default, data files for
GPS tracks are stored in the same directory as the .conf file.  If no --conf
option is provided, the default configuration will be loaded from
~/.local/share/zombietrackergps/zombietrackergps.conf, or created there if it
does not exist.
.IP "--no-first-run"
Disable first run handling, even on the first run.
.IP "--private-session"
Disallow saves for this session.
.IP "--desktop NAME"
Sets the XDG_CURRENT_DESKTOP environment variable to NAME.  This can be useful
when running the program under sudo from a different user ID. It is equivalent
to "XDG_CURRENT_DESKTOP=NAME zombietrackergps" from the command line.

.SH HELP OPTIONS
.IP "--help-batch"
Describe batch (command line, non-GUI) GPS file format conversion options.
.IP "--help-filter"
Describe filter query language.
.IP "--help-formats-input"
Describe supported input file formats.
.IP "--help-formats-output"
Describe supported output file formats.
.IP "--help-fields trk|wpt"
Describe available columns for queries.
.IP "--help-units trk|wpt FIELD"
Describe available unit suffixes for given field.

.SH BATCH FILE PROCESSING OPTIONS
.IP "--input, -i FILE..."
One or more GPS input files to process. '-' for stdin. 
.IP "--output, -o FILE..."
One per input, or omit to auto-generate names. '-' for stdout.
.IP "--dir, -d DIR"
Write outputs to this directory. $PWD if unset.
.IP "--clobber"
Do not complain about existing output files.
.IP "--concat, --merge"
Merge multiple input files to a single output file.
.IP "--pretty, --formatted"
Generate human readable XML files.
.IP "--ztgps-extensions"
Generate ZTGPS track extensions.
.IP "--no-ztgps-extensions"
Do not generate ZTGPS track extensions.
.IP "--indent NUM"
Indent XML files with NUM spaces or tabs.
.IP "--indent-tabs"
Use tabs to indent XML files.
.IP "--indent-spaces"
Use sapces to indent XML files (default).
.IP "--type trk|wpt|all"
Write tracks, waypoints, or all.
.IP "--filter-trk QUERY"
Process tracks matching QUERY.  --help-filter for syntax.
.IP "--filter-wpt QUERY"
Process waypoints matching QUERY. --help-filter for syntax.
.IP "--match-case"
Use case sensitive queries.
.IP "--verbose,-v"
Be more verbose.
.IP "--stat"
Print statistics about conversions.

.SH FILTER QUERY LANGUAGE
The batch processing query language is identical to the in-program filter syntax
which can be entered into the filter bars above Track Panes, etc.  The field names
can be combined with comparison and boolean operators. The query string can be
passed as an argument to the --filter-trk or --filter-wpt options.
.PP
The following operators are supported:
   =~ or :    Perl-style regex match.  E.g: 
                 Name : Dirt.*Trail   # all matching given regex
                 Flags : Mexico       # all tracks in Mexico
   !~         Matches if regex is not found.  E.g: Name !~ Trail
   ==         Exact equality.  E.g: Max_HR == '158 bpm'
   !=         Inequality.
   <          Less than.  E.g: Ascent > 100m
   <=         Less than or equal to.
   >          Greater than.  E.g: Moving_Time > 2h30m
   >=         Greater than or equal to.
.PP
The following boolean operators can be used to combine expressions:
   !          Unary logical negation.  E.g, !(Name : Bridge)
   &          Binary logical and. E.g: Tags : Hike & Ascent > 1km
   ^          Binary exclusive or.
   |          Binary inclusive or.
.PP
Parenthetical groupings are supported, but the parens must be space separated:
   ( Tags : Hike | Tags : Run ) & Length > 20km
.PP
A list of field names can be obtained via:
   zombietrackergps --help-fields trk|wpt
.PP
A range of common units suffixes are supported for numeric values.  A list of
suffixes for a given field is available via:
   zombietrackergps --help-units trk|wpt FIELDNAME
.PP
The query string must be a single command line parameter, so use shell quotes for
queries containing spaces.

.SH BUGS
After re-loading a session which was stored with the program window maximized,
the UI does not display correctly.  This appears to be a Qt bug, and can be
worked around by minimizing the program and then maximizing it again.  This does
not effect sessions with non-maximized windows.
.PP
The line drawing performance of geo indexed lines in the Marble Map is very low,
due to not using OpenGL.  More than about 250 tracks displayed at once can cause
sluggish pan and zoom of the map.  You can work around this by using a filter to
show no more than about that many at once.  As of version 1.02, an option under
the "Map Display" tab can draw track lines only when the map is still.  This can
improve interactive performance.
.PP
Currently only Garmin devices that support GPX, TCX, or FIT files can be used as a
direct import device.  Arbitrary GPS, TCX, or FIT files can be imported from files,
however, to support other types of devices.
.PP
Currently, only GPS tracks and waypoints can be imported, not yet routes.

.SH AUTHOR
loopdawg (echo 'enRnzcHNAemg9tYmlldHJhYp2tlcmdwcsy5uZXQK' | tr -d zgps | base64 -d)
.PP
.I Must
include the string
.I ZTGPSREQ
in subject line, or be encrypted to the public key given in --pubkey,
or the email will be spam-filtered.

.SH SEE ALSO
The KDE Marble application and variants can be used to download regions of maps
for ZombieTrackerGPS to use in offline mode.  Also, any new types of maps
configured in Marble will be offered as map sources in ZombieTrackerGPS as well.
.PP
ZombieTrackerGPS's documentation window (available under the
.B "Help/Show Tutorial..."
menu) provides useful information about interactive features.
