# Required Shared Libraries

The following libraries are required by zombietrackergps:

  * libc.so.6
  * libdl.so.2
  * libgcc_s.so.1
  * libmarblewidget-qt5.so.28
  * libm.so.6
  * libQt5Charts.so.5
  * libQt5Concurrent.so.5
  * libQt5Core.so.5
  * libQt5Gui.so.5
  * libQt5Svg.so.5
  * libQt5Widgets.so.5
  * libQt5Xml.so.5
  * libstdc++.so.6

# Packages

## Debian GNU/Linux 10 (buster)

        sudo apt install libc6 libgcc1 libmarblewidget-qt5-28 libqt5charts5 libqt5concurrent5 libqt5core5a libqt5gui5 libqt5svg5 libqt5widgets5 libqt5xml5 libstdc++6 

## Debian GNU/Linux 12 (bookworm)

        sudo apt install libc6 libgcc-s1 libmarblewidget-qt5-28 libqt5charts5 libqt5concurrent5 libqt5core5a libqt5gui5 libqt5svg5 libqt5widgets5 libqt5xml5 libstdc++6 

## openSUSE Leap 15.4

        sudo zypper install glibc libgcc_s1 libmarblewidget-qt5-28 libQt5Charts5 libQt5Concurrent5 libQt5Core5 libQt5Gui5 libQt5Svg5 libQt5Widgets5 libQt5Xml5 libstdc++6 

## Arch Linux

        sudo pacman --needed -S gcc-libs glibc marble-common qt5-base qt5-charts qt5-svg 
