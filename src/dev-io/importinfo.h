/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef IMPORTINFO_H
#define IMPORTINFO_H

#include <QString>
#include <QVector>
#include <QList>
#include <QUrl>
#include <QFileInfoList>

// Info needed for importing GPS tracks.
struct ImportInfo
{
    ImportInfo() { }

    ImportInfo(const QString& file) :
        m_source(file), m_file(file)
    { }

    ImportInfo(const QString& source, const QString& file) :
        m_source(source), m_file(file)
    { }

    QString m_source; // name of source
    QString m_file;   // file to import from
};

class ImportInfoList : public QVector<ImportInfo>
{
public:
    using QVector<ImportInfo>::QVector;

    ImportInfoList(const QList<QUrl>&);   // Convert from list of URLs to ImportInfoList
    ImportInfoList(const QFileInfoList&); // Convert from list of QFileInfo to ImportInfoList
    ImportInfoList(const QStringList&);   // Convert from QStringList of filenames

    qint64 totalFileSize() const;         // total size of files in list
};

#endif // IMPORTINFO_H
