/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <chrono>

#include <QStorageInfo>
#include <QFileInfo>
#include <QHash>
#include <QPixmap>
#include <QFont>

#include <src/util/units.h>
#include <src/util/roles.h>
#include <src/core/modelmetadata.inl.h>

#include "gpsmodel.h"
#include "gpsgarmin.h"

GpsModel::GpsModel(QObject *parent) :
    TreeModel(nullptr, parent),
    NamedItem(getItemNameStatic()),
    m_testMounts([](GpsModel&){ return false; }),
    m_refreshTimer(this)
{
    setupTimers();
    refresh(); // seed initial device list
}

GpsModel::~GpsModel()
{
    // Individual GPS device pointers will be deleted by QSharedPointer.
}

void GpsModel::setupTimers()
{
    using namespace std::chrono_literals;

    // It would be better to do this in some event driven way, but all the ways to do that
    // seem OS specific.  Polling is ugly, but the refresh() is very cheap if nothing changed.
    connect(&m_refreshTimer, &QTimer::timeout, this, &GpsModel::refresh);
    m_refreshTimer.start(3s);
}

bool GpsModel::getVolumes()
{
    const auto volumes = QStorageInfo::mountedVolumes();

    bool changed = false;
    const int initialSize = m_volumes.size();

    m_volumes.resize(volumes.size()); // space enough for all

    // This is cheaper than comparing: QStrings are COW
    int vol = 0;
    for (const auto& mount : volumes)
        if (mount.isReady())
            changed |= m_volumes[vol++].update(mount);

    m_volumes.resize(vol);           // remove unused entries from end
    changed |= m_testMounts(*this);  // test hook for adding fake entries

    changed |= (initialSize != m_volumes.size());

    return changed;
}

void GpsModel::refresh()
{
    if (!getVolumes()) // bail out if no changes since last time
        return;

    clear();     // remove old devices
    reserve(4);  // Typically there won't be more than 1 device.

    // Build list of GPS devices
    for (const auto& mount : m_volumes) {
        if (GpsDevice* device = getDevice(mount.m_rootPath)) {
            beginInsertRows(QModelIndex(), size(), size());
            append(value_type(device));
            endInsertRows();
        }
    }
}

// Factory for device specific derived GPS classes, or nullptr if none apply.
GpsDevice* GpsModel::getDevice(const QString& mount)
{
    // Check each type we know about in turn.
    if (GpsGarmin::is(mount)) return new GpsGarmin(mount);
    // Other types go here, when available.

    return nullptr;
}

void GpsModel::clear()
{
    if (empty())
        return;

    beginRemoveRows(QModelIndex(), 0, size() - 1);
    QVector<QSharedPointer<GpsDevice>>::clear(); // QSharedPointer deletes GpsDevices on heap
    endRemoveRows();
}

QVariant GpsModel::data(const QModelIndex& idx, int role) const
{
    if (!idx.isValid() || idx.row() >= rowCount() || idx.column() >= columnCount())
        return { };

    // Text alignment:
    if (role == Qt::TextAlignmentRole)
        return { mdAlignment(idx.column()) };

    // Data
    if (role == Qt::DisplayRole || role == Util::CopyRole) {
        switch (idx.column()) {
        case GpsModel::Make:       return at(idx.row())->make();
        case GpsModel::Model:      return at(idx.row())->model();
        case GpsModel::Device:     return at(idx.row())->device();
        case GpsModel::MountPoint: return at(idx.row())->mountPoint();
        case GpsModel::Image:      [[fallthrough]];
        case _Count:               break;
        }
    }

    if (role == Qt::DecorationRole) {
        switch (idx.column()) {
        case GpsModel::Image: return QPixmap(at(idx.row())->image());
        default:
            break;
        }
    }

    return { };
}

QVariant GpsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (const QVariant val = ModelMetaData::headerData<GpsModel>(section, orientation, role); val.isValid())
        return val;

    if (orientation == Qt::Horizontal)
        if (role == Qt::DisplayRole)
            return mdName(section);

    return TreeModel::headerData(section, orientation, role);
}

QModelIndex GpsModel::index(int row, int column, const QModelIndex&) const
{
    return createIndex(row, column);
}

QModelIndex GpsModel::parent(const QModelIndex&) const
{
    return { };
}

int GpsModel::rowCount(const QModelIndex& parent) const
{
    return (parent.isValid()) ? 0 : size();
}

Qt::ItemFlags GpsModel::flags(const QModelIndex& idx) const
{
    const Qt::ItemFlags flags = ModelMetaData::flags<GpsModel>(idx) | TreeModel::flags(idx);

    switch (idx.column()) {
    case GpsModel::Image: return flags & ~Qt::ItemIsSelectable;
    default:              return flags;
    }
}

bool GpsModel::setData(const QModelIndex&, const QVariant&, int)
{
    assert(0);
    return false; // not supported
}

bool GpsModel::setHeaderData(int /*section*/, Qt::Orientation /*orientation*/,
                            const QVariant& /*value*/, int /*role*/)
{
    assert(0);
    return false; // not supported
}

bool GpsModel::insertRows(int /*position*/, int /*rows*/, const QModelIndex& /*parent*/)
{
    assert(0);
    return false; // not supported
}

bool GpsModel::removeRows(int /*position*/, int /*rows*/, const QModelIndex& /*parent*/)
{
    assert(0);
    return false; // not supported
}

const Units& GpsModel::units(const QModelIndex& idx) const
{
    return mdUnits(idx.column());
}

QString GpsModel::mdName(ModelType mt)
{
    switch (mt) {
    case GpsModel::Make:       return tr("Make");
    case GpsModel::Model:      return tr("Model");
    case GpsModel::Device:     return tr("Device");
    case GpsModel::Image:      return tr("Image");
    case GpsModel::MountPoint: return tr("Mount Point");
    case _Count:               break;
    }

    assert(0 && "Unknown GpsList column");
    return "";
}

bool GpsModel::mdIsEditable(ModelType)
{
    return false;
}

QString GpsModel::mdTooltip(ModelType mt)
{
    const bool editable = mdIsEditable(mt);

    switch (mt) {
    case GpsModel::Make:      
        return makeTooltip(tr("GPS device manufacturer."), editable);
    case GpsModel::Model:   
        return makeTooltip(tr("GPS device model."), editable);
    case GpsModel::Device:   
        return makeTooltip(tr("GPS device node."), editable);
    case GpsModel::Image:   
        return makeTooltip(tr("GPS device image."), editable);
    case GpsModel::MountPoint: 
        return makeTooltip(tr("GPS device mountpoint."), editable);
    default:
        assert(0 && "Unknown GpsList column");
        return "";
    }
}

QString GpsModel::mdWhatsthis(ModelType mt)
{
    return mdTooltip(mt);   // just pass through the tooltip
}

Qt::Alignment GpsModel::mdAlignment(ModelType)
{
    return Qt::AlignLeft | Qt::AlignVCenter;
}

const Units& GpsModel::mdUnits(ModelType)
{
    static const Units rawString(Format::String);
    return rawString;
}

NamedItemInterface::name_t GpsModel::getItemNameStatic()
{
    return { tr("GPS Device"), tr("GPS Devices") };
}

ImportInfoList GpsModel::importFiles(const QModelIndexList& indexList) const
{
    ImportInfoList importData;

    importData.reserve(indexList.size());

    for (const auto& idx : indexList)
        if (idx.row() < rowCount())
            for (const auto& fileName : at(idx.row())->files(GpsDevice::Data::GPS, GpsDevice::Transfer::Output))
                if (const QFileInfo file(fileName); file.isReadable())
                    importData.append(ImportInfo(at(idx.row())->make() + " " + at(idx.row())->model(),
                                                 file.filePath()));

    return importData;
}

bool GpsModel::Volume::update(const QStorageInfo& volume)
{
    return update(volume.rootPath());
}
