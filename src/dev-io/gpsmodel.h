/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GPSLIST_H
#define GPSLIST_H

#include <functional>

#include <QSharedPointer>
#include <QVector>
#include <QTimer>
#include <QModelIndexList>
#include <QStringList>
#include <src/core/modelmetadata.h>
#include <src/core/treemodel.h>
#include <src/util/nameditem.h>

#include "src/dev-io/importinfo.h"

class QStorageInfo;
class GpsDevice;

class GpsModel final :
        public TreeModel,
        public NamedItem,
        public ModelMetaData,
        public QVector<QSharedPointer<GpsDevice>>
{
    Q_OBJECT

public:
    enum {
        _First,
        Make = _First,
        Model,
        Device,
        MountPoint,
        Image,
        _Count
    };

    GpsModel(QObject *parent = nullptr);
    ~GpsModel() override;
    
    ImportInfoList importFiles(const QModelIndexList&) const;

    const Units& units(const QModelIndex& idx) const override;

    // *** begin ModelMetaData API
    static QString        mdName(ModelType);
    static bool           mdIsEditable(ModelType);
    static QString        mdTooltip(ModelType);
    static QString        mdWhatsthis(ModelType);
    static Qt::Alignment  mdAlignment(ModelType);
    static const Units&   mdUnits(ModelType);
    // *** end ModelMetaData API

    // *** begin NamedItem API
    static name_t getItemNameStatic();
    // *** end NamedItem API

    // *** begin QAbstractItemModel API
    using TreeModel::data;
    QVariant data(const QModelIndex& idx, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column,
                      const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex& idx) const override;

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& /*parent*/ = QModelIndex()) const override { return _Count; }

    Qt::ItemFlags flags(const QModelIndex& idx) const override;
    bool setData(const QModelIndex& idx, const QVariant& value,
                 int role = Qt::DisplayRole) override;
    bool setHeaderData(int section, Qt::Orientation orientation,
                       const QVariant& value, int role = Qt::DisplayRole) override;
    bool insertColumns(int /*position*/, int /*columns*/,
                       const QModelIndex& = QModelIndex()) override { return false; }
    bool removeColumns(int /*position*/, int /*columns*/,
                       const QModelIndex& = QModelIndex()) override { return false; }
    bool insertRows(int position, int rows,
                    const QModelIndex& parent = QModelIndex()) override;
    bool removeRows(int position, int rows,
                    const QModelIndex& parent = QModelIndex()) override;
    void clear() override;  // clear entire model contents
    // *** end QAbstractItemModel API

public slots:
    void refresh() override;  // refresh from currently mounted devices

private:
    friend class TestZtgps;

    // For mountpoints.  For test suite purposes we use this abstraction between the
    // QStorageInfo and the consumption in refresh().
    struct Volume {
        Volume() = default;
        Volume(const QString& rootPath) { update(rootPath); }

        bool update(const QString& rootPath) {
            if (m_rootPath == rootPath)
                return false;
            m_rootPath = rootPath;
            return true;
        }

        bool update(const QStorageInfo&);
        bool operator==(const QString& rootPath) const { return m_rootPath == rootPath; }
        bool operator==(const Volume& v) const { return m_rootPath == v.m_rootPath; }

        QString m_rootPath;
    };

    void setupTimers();
    static GpsDevice* getDevice(const QString& mount);
    bool getVolumes();  // update m_mountPoints, and returns true if any changes

    std::function<bool(GpsModel&)> m_testMounts;   // test hook

    QTimer                         m_refreshTimer; // see comment in setupTimers
    QVector<Volume>                m_volumes;      // current mountpoints
};

#endif // GPSLIST_H
