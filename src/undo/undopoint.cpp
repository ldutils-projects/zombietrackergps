/*
    Copyright 2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "src/core/pointmodel.h"
#include "undopoint.h"

UndoPoint::UndoPoint(PointModel& model, const QModelIndex& index,
                     const PointItem& data) :
    UndoModel(model),
    m_index(Util::SaveIndex(index)),
    m_before(*model.getItem(index)),
    m_after(data)
{
}

bool UndoPoint::apply(const PointItem& data) const
{
    const RunHooks hooks(*this, m_index);

    if (!hooks.hasModel() || !hooks.index().isValid())
        return false;

    *hooks.model<PointModel>()->getItem(hooks.index()) = data;
    return true;
}

size_t UndoPoint::size() const
{
    return sizeof(*this);
}
