/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UNDOUI_H
#define UNDOUI_H

#include <src/undo/undobase.h>

#include "src/ui/windows/mainwindow.h"

// Undoer for some GUI toggles
template <typename T> class UndoUiBase final : public UndoBase
{
public:
    UndoUiBase(MainWindow& mainWindow, T item) :
        m_mainWindow(mainWindow), m_item(item),
        m_before(m_mainWindow.isVisible(item))
    { }

    bool undo() const override { m_mainWindow.setVisible(m_item, !m_before); return true; }
    bool redo() const override { m_mainWindow.setVisible(m_item, m_before); return true; }
    size_t size() const override { return sizeof(*this); }
    const char* className() const override { return "UndoUiMenu"; }

protected:
    MainWindow& m_mainWindow;
    const T     m_item;
    const bool  m_before;
};

using UndoUiMenu = UndoUiBase<MainWindow::CheckableMenu>;
using UndoUiStat = UndoUiBase<MainWindow::Stat>;

#endif // UNDOUI_H
