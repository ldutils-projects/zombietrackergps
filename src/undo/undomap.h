/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UNDOMAPVIEW_H
#define UNDOMAPVIEW_H

#include <src/undo/undobase.h>
#include <src/fwddecl.h>
#include "src/core/viewmodel.h" // for ViewParams
#include "src/core/viewparams.h"

class TrackMap;
class MainWindow;

class UndoMapBase : public UndoBase
{
protected:
    UndoMapBase(TrackMap& trackMap);
    ~UndoMapBase() override; // just to get vtable into cpp

    TrackMap* findMap() const;

    MainWindow& m_mainWindow;
    PaneId_t    m_mapPaneId;
};

// Undoer for map views
class UndoMapView final : public UndoMapBase
{
public:
    UndoMapView(TrackMap&, const ViewParams& before, const ViewParams& after);

    bool undo() const override { return apply(m_before); }
    bool redo() const override { return apply(m_after); }
    size_t size() const override { return sizeof(*this); }
    const char* className() const override { return "UndoMapView"; }

private:
    bool apply(const ViewParams&) const;

    ViewParams m_before;
    ViewParams m_after;
};


// Undoer for map theme
class UndoMapTheme final : public UndoMapBase
{
public:
    UndoMapTheme(TrackMap& trackMap, const QString& before, const QString& after) :
        UndoMapBase(trackMap), m_before(before), m_after(after)
    { }

    using UndoMapBase::size;

    bool undo() const override { return apply(m_before); }
    bool redo() const override { return apply(m_after); }
    size_t size() const override;
    const char* className() const override { return "UndoMapTheme"; }

private:
    bool apply(const QString&) const;

    QString m_before;
    QString m_after;
};

#endif // UNDOMAPVIEW_H
