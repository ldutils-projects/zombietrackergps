/*
    Copyright 2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UNDOPOINT_H
#define UNDOPOINT_H

#include <src/undo/undomodel.h>

#include "src/core/pointitem.h"

// Undo replacement of entire point data, for cases which bypass setData()
// as sometimes happens for efficiency.
class UndoPoint final : public UndoModel
{
public:
    UndoPoint(PointModel&, const QModelIndex&, const PointItem&);

protected:
    bool apply(const PointItem&) const;

    bool undo() const override { return apply(m_before); }
    bool redo() const override { return apply(m_after); }
    size_t size() const override;
    const char* className() const override { return "UndoPoint"; }

    Util::SavableIndex m_index;
    PointItem          m_before;
    PointItem          m_after;
};

#endif // UNDOPOINT_H
