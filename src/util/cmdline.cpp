/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <unistd.h>  // for STDIN_FILENO/STDOUT_FILENO

#include <QtGlobal>
#include <QFileInfo>
#include <QFile>
#include <QDir>

#include <src/core/query.h>
#include <src/core/query.inl.h>
#include <src/util/resources.h>

#include "src/core/app.h"
#include "src/geo-io/geoioconv.h"
#include "src/geo-io/geoioquery.h"
#include "src/core/trackmodel.h"
#include "src/core/waypointmodel.h"
#include "cmdline.h"

CmdLine::CmdLine(const char* appname, const char* version, const char* build, const QStringList& argv) :
    CmdLineBase(appname, version, build, argv),
    m_concat(false),
    m_clobber(false),
    m_import(false),
    m_verbose(0),
    m_indent(2),
    m_stat(false),
    m_filterCase(Qt::CaseInsensitive),
    m_features(GeoIoFeature::AllTypes | GeoIoFeature::AllAux | GeoIoFeature::ExtZtgps |
               GeoIoFeature::Spaces | GeoIoFeature::AllData),
    m_stdin(STDIN_FILENO),
    m_stdout(STDOUT_FILENO),
    m_helpUnitsFeature(GeoIoFeature::Unk)
{
}

CmdLine::CmdLine(const char* appname, const char* version, const char* build, int argc, const char** argv) :
    CmdLine(appname, version, build)
{
    setArgv(argc, argv);
}

CmdLine::CmdLine(const char* appname, const char* version, const char* build, int argc, char** argv) :
    CmdLine(appname, version, build, argc, const_cast<const char**>(argv))
{
}

bool CmdLine::processArgHelp(int& arg)
{
    const QString& astr = m_argv[arg]; // convenience

    if (astr == "--help-filter") {
        helpFilter();
    } if (astr == "--help-batch") {
        helpBatch();
    } else if (astr == "--help-format-input" || astr == "--help-formats-input") {
        helpFormats<GeoLoad>();
    } else if (astr == "--help-format-output" || astr == "--help-formats-output") {
        helpFormats<GeoSave>();
    } else if (astr == "--help-field" || astr == "--help-fields" || astr == "--help-column" || astr == "--help-columns") {
        helpFields(parseFeatures({ next(arg) }));
    } else if (astr == "--help-unit" || astr == "--help-units" || astr == "--help-suffix" || astr == "--help-suffixes") {
        // helpUnits can't be invoked until the app (and thus, units config) is constructed.
        m_helpUnitsFeature = parseFeatures({ next(arg) });
        m_helpUnitsName = next(arg);
    } else {
        return false;
    }

    return true;
}

bool CmdLine::processArgBatch(int& arg)
{
    const QString& astr = m_argv[arg]; // convenience

    if (astr == "--dir" || astr == "-d") {
        m_dir = next(arg);
    } else if (astr ==  "--filter-trk" || astr == "--filter-track" || astr == "--filter-tracks") {
        m_filterTrk = next(arg);
    } else if (astr ==  "--filter-wpt" || astr == "--filter-waypoint" || astr == "--filter-waypoints") {
        m_filterWpt = next(arg);
    } else if (astr ==  "--match-case") {
        m_filterCase = Qt::CaseSensitive;
    } else if (astr == "--concat" || astr == "--merge") {
        m_concat = true;
    } else if (astr == "--clobber") {
        m_clobber = true;
    } else if (astr == "--pretty" || astr == "--formatted") {
        m_features |= GeoIoFeature::Formatted;
    } else if (astr == "--ztgps-extensions") {
        m_features |= GeoIoFeature::ExtZtgps;
    } else if (astr == "--no-ztgps-extensions") {
        m_features &= ~GeoIoFeature::ExtZtgps;
    } else if (astr == "--stat" || astr == "--stats") {
        m_stat = true;
    } else if (astr == "--indent") {
        m_indent = next(arg).toInt();
    } else if (astr == "--indent-tabs") {
        m_features &= ~GeoIoFeature::Spaces;
    } else if (astr == "--indent-spaces") {
        m_features |= GeoIoFeature::Spaces;
    } else if (astr == "--verbose" || astr == "-v") {
        ++m_verbose;
    } else if (astr == "-vv") {
        m_verbose += 2;
    } else if (astr == "-vvv") {
        m_verbose += 3;
    } else if (astr == "--import" || astr == "-I") {
        m_import = true;
    } else if (astr == "--format" || astr == "--formats" || astr == "-f") {
        m_format = next(arg);
    } else if (astr == "--input" || astr == "--inputs" || astr == "--in" || astr == "-i") {
        collect(arg, m_input);
    } else if (astr == "--output" || astr == "--outputs" || astr == "--out" || astr == "-o") {
        collect(arg, m_output);
    } else if (astr == "--type" || astr == "--types" || astr == "-t") {
        collect(arg, m_types);
    } else {
        return false;
    }

    return true;
}

bool CmdLine::processArg(int& arg)
{
    return CmdLineBase::processArg(arg) ||
            processArgHelp(arg) ||
            processArgBatch(arg);
}

void CmdLine::setup()
{
    CmdLineBase::setup();

    if (!batchMode())
        return;

    const GeoSaveQuery saveQuery;
    const GeoLoadQuery loadQuery(m_stdin);
    const GeoFormat format = saveQuery.formatForExt(m_format);

    m_output.reserve(m_input.size());
    m_geoFormatIn.reserve(m_input.size());
    m_geoFormatOut.reserve(m_input.size());

    // create output paths, if none
    if (!m_input.isEmpty()) {
        if (m_output.isEmpty()) { // Create output filenames ourselves
            for (const auto& input : m_input) {
                const QString path = m_dir + (m_dir.isEmpty() ? "" : QString(QDir::separator())) +
                        (input == "-" ? "stdin" : QFileInfo(input).completeBaseName()) +
                        "." + m_format;
                m_outputPaths.append(path);
                m_geoFormatOut.append(format);

                if (m_concat)  // concat mode uses just one output
                    break;
            }
        } else { // Use user provided outputs
            m_outputPaths = m_output;  // use the ones provided
            for (const auto& output : m_outputPaths) {
               m_geoFormatOut.append(format != GeoFormat::Unknown ? format :
                                                                 saveQuery.formatForExt(QFileInfo(output).suffix()));
               if (m_concat)  // concat mode uses just one output
                   break;
            }
        }
    }

    // Collect input formats.
    // TODO: if it's stdin, defer it so we don't read data needed later.
    //        In that case we just use the output format, which is wrong,
    //        but it'll be deduced later.
    for (const auto& input : m_input)
        m_geoFormatIn.append(loadQuery.format(input));

    if (m_output.contains("-")) { // disable our own use of stdout if we're writing data to it
        m_stat = false;
        m_verbose = 0;
    }

    // Build types
    if (!m_types.isEmpty())
        m_features = (m_features & ~GeoIoFeature::AllTypes) | parseFeatures(m_types);
}

GeoIoFeature CmdLine::parseFeatures(const QStringList& features)
{
    GeoIoFeature f = GeoIoFeature::None;
    for (const auto& feature : features)
        f |= GeoLoad::parseFeature(feature);

    return f;
}

void CmdLine::helpFilter() const
{
    qInfo(qUtf8Printable(
              tr("Filter query Language:\n"
                 "The batch processing query language is identical to the in-program filter syntax\n"
                 "which can be entered into the filter bars above Track Panes, etc.  The field names\n"
                 "can be combined with comparison and boolean operators. The query string can be\n"
                 "passed as an argument to the --filter-trk or --filter-wpt options.\n"
                 "   \n"
                 "   The following operators are supported:\n"
                 "      =~ or :    Perl-style regex match.  E.g: \n"
                 "                    Name : Dirt.*Trail   # all matching given regex\n"
                 "                    Flags : Mexico       # all tracks in Mexico\n"
                 "      !~         Matches if regex is not found.  E.g: Name !~ Trail\n"
                 "      ==         Exact equality.  E.g: Max_HR == '158 bpm'\n"
                 "      !=         Inequality.\n"
                 "      <          Less than.  E.g: Ascent > 100m\n"
                 "      <=         Less than or equal to.\n"
                 "      >          Greater than.  E.g: Moving_Time > 2h30m\n"
                 "      >=         Greater than or equal to.\n"
                 "\n"
                 "   The following boolean operators can be used to combine expressions:\n"
                 "      !          Unary logical negation.  E.g, !(Name : Bridge)\n"
                 "      &          Binary logical and. E.g: Tags : Hike & Ascent > 1km\n"
                 "      ^          Binary exclusive or.\n"
                 "      |          Binary inclusive or.\n"
                 "\n"
                 "   Parenthetical groupings are supported, but the parens must be space separated:\n"
                 "      ( Tags : Hike | Tags : Run ) & Length > 20km\n"
                 "\n"
                 "   A list of field names can be obtained via:\n"
                 "      %s --help-fields trk|wpt\n"
                 "\n"
                 "   A range of common units suffixes are supported for numeric values.  A list of\n"
                 "   suffixes for a given field is available via:\n"
                 "      %s --help-units trk|wpt FIELDNAME\n"
                 "\n"
                 "   The query string must be a single command line parameter, so use shell quotes for\n"
                 "   queries containing spaces.\n"
                 )), m_appname, m_appname);

    throw Exit(Exit::Quit);
}

void CmdLine::helpBatch()
{
    qInfo("%s", qUtf8Printable(
              tr("Batch File Processing Options:\n"
                 "   --input, -i FILE...    One or more GPS input files to process. '-' for stdin. \n"
                 "   --output, -o FILE...   One per input, or omit to auto-generate names. '-' for stdout.\n"
                 "   --dir, -d DIR          Write outputs to this directory. $PWD if unset.\n"
                 "   --clobber              Do not complain about existing output files.\n"
                 "   --concat, --merge      Merge multiple input files to a single output file.\n"
                 "   --pretty, --formatted  Generate human readable XML files.\n"
                 "   --ztgps-extensions     Write ZTGPS extensions for GPX files.\n"
                 "   --no-ztgps-extensions  Do not Write ZTGPS extensions for GPX files.\n"
                 "   --indent NUM           Indent XML files with NUM spaces.\n"
                 "   --indent-tabs          Use tabs to indent XML files.\n"
                 "   --indent-spaces        Use spaces to indent XML files (default).\n"
                 "   --type trk|wpt|all     Write tracks, waypoints, or all.\n"
                 "   --filter-trk QUERY     Process tracks matching QUERY.  --help-filter for syntax.\n"
                 "   --filter-wpt QUERY     Process waypoints matching QUERY. --help-filter for syntax.\n"
                 "   --match-case           Use case sensitive queries.\n"
                 "   --verbose,-v           Be more verbose.\n"
                 "   --stat                 Print statistics about conversions.\n"
                 )));
    throw Exit(Exit::Quit);
}

template <class IO> void CmdLine::helpFormats()
{
    for (const auto& name : IO::formatNames())
        qInfo("%s", qUtf8Printable(name));

    throw Exit(Exit::Quit);
}

template <class MODEL> void CmdLine::helpFields()
{
    for (int col = 0; col < MODEL::_Count; ++col)
        qInfo("%s", qUtf8Printable(Query::CanonicalizeColumnName(MODEL::mdName(col))));
}

void CmdLine::helpFields(GeoIoFeature feature)
{
    switch (feature) {
    case GeoIoFeature::Trk: helpFields<TrackModel>(); break;
    case GeoIoFeature::Wpt: helpFields<WaypointModel>(); break;
    default:
        qInfo("%s", qUtf8Printable(tr("Provide a single feature type, such as '--help-fields trk'")));
        throw Exit(1);
    }

    throw Exit(Exit::Quit);
}

template<class MODEL> void CmdLine::helpUnits(const QString& field)
{
    for (int col = 0; col < MODEL::_Count; ++col) {
        if (Query::CanonicalizeColumnName(MODEL::mdName(col)).toLower() == field.toLower()) {
            const Units& units = MODEL::mdUnits(col);

            QStringList alphabetical;

            for (const auto& range : units.rangeSuffixes())
                for (const auto& suffix : Units::suffixes(range))
                    alphabetical.append(suffix);

            std::sort(alphabetical.begin(), alphabetical.end());

            for (const auto& suffix : alphabetical)
                qInfo("%s", qUtf8Printable(suffix));

            throw Exit(Exit::Quit);
        }
    }
}

void CmdLine::helpUnits(GeoIoFeature feature, const QString& field)
{
    if (!field.isEmpty()) {
        switch (feature) {
        case GeoIoFeature::Trk: helpUnits<TrackModel>(field);    break;
        case GeoIoFeature::Wpt: helpUnits<WaypointModel>(field); break;
        default: break;
        }
    }

    qInfo("%s", qUtf8Printable(tr("Provide a feature type and field name, such as '--help-units trk Ascent'")));
    throw Exit(1);
}

void CmdLine::usage() const
{
    CmdLineBase::usage();

    qInfo("%s", qUtf8Printable(tr("Help Options:")));
    qInfo("%s", qUtf8Printable(tr(
                            "   --help-filter               Describe query syntax.\n"
                            "   --help-batch                Describe batch processing options.")));
    qInfo("%s", qUtf8Printable(tr(
                            "   --help-formats-input        Describe supported input file formats.\n"
                            "   --help-formats-output       Describe supported output file formats.\n"
                            "   --help-fields trk|wpt       Available columns for queries.\n"
                            "   --help-units trk|wpt field  Available unit suffixes for given field."
                         )));
}

template <class MODEL> bool CmdLine::verifyQuery(const QString& query) const
{
    if (query.isEmpty())
        return true;

    MODEL dummy;
    return Query::Context(&dummy).isValidQuery(query);
}

bool CmdLine::verify() const
{
    if (!CmdLineBase::verify())
        return false;

    if (!batchMode())
        return true;

    // Must have some inputs
    if (m_input.isEmpty()) {
        qCritical("%s", qUtf8Printable(tr("No input files.")));
        return false;
    }

    // Verify concat mode has a single output
    if (m_concat && m_outputPaths.size() != 1) {
        qCritical("%s", qUtf8Printable(tr("Concatenation mode must have a single output.")));
        return false;
    }

    // Must have one output per input, unless in concat mode
    if (m_input.size() != m_outputPaths.size() && !m_concat) {
        qCritical("%s", qUtf8Printable(tr("Input and output list size mismatch.")));
        return false;
    }

    // Check for missing output format
    if (m_output.isEmpty() && m_format.isEmpty()) {
        qCritical("%s", qUtf8Printable(tr("Output format must be provided.")));
        return false;
    }

    // Verify output directory
    {
        const QString outDir = !m_dir.isEmpty() ? m_dir : QDir::currentPath();
        const QDir info(outDir);

        if (!info.exists() || !info.isReadable()) {
            qCritical("%s", qUtf8Printable(tr("Output directory does not exist or is not readable.")));
            return false;
        }
    }

    // Verify input paths
    for (const auto& file : m_input) {
        if (file == "-") // don't check stdin
            continue;

        const QFileInfo info(file);
        if (!info.exists()) {
            qCritical("%s: %s", qUtf8Printable(tr("Missing input file")), qUtf8Printable(file));
            return false;
        }

        if (!info.isReadable()) {
            qCritical("%s: %s", qUtf8Printable(tr("File is not readable")), qUtf8Printable(file));
            return false;
        }

        if (!info.isFile()) {
            qCritical("%s: %s", qUtf8Printable(tr("Input is not a file")), qUtf8Printable(file));
            return false;
        }
    }

    // Verify output paths
    for (const auto& file : m_outputPaths) {
        const QFileInfo info(file);
        if (info.exists()) {
            if (!m_clobber) {
                qCritical("%s: %s", qUtf8Printable(tr("Output file exists (see --clobber)")), qUtf8Printable(file));
                return false;
            }

            if (!info.isWritable()) {
                qCritical("%s: %s", qUtf8Printable(tr("Output file exists but is not writable")), qUtf8Printable(file));
                return false;
            }
        }

        if (!info.dir().exists() || !info.dir().isReadable()) {
            qCritical("%s: %s", qUtf8Printable(tr("Output directory for file is not accessable")), qUtf8Printable(file));
            return false;
        }
    }

    // Must understand all input formats
    if (const int found = m_geoFormatIn.indexOf(GeoFormat::Unknown); found >= 0) {
        qCritical("%s: %s", qUtf8Printable(tr("Unknown input format")), qUtf8Printable(m_input.at(found)));
        return false;
    }

    // Must understand all output formats
    if (const int found = m_geoFormatOut.indexOf(GeoFormat::Unknown); found >= 0) {
        qCritical("%s: %s", qUtf8Printable(tr("Unknown output format")), qUtf8Printable(m_outputPaths.at(found)));
        return false;
    }

    // Check for inputs and outputs being the same file
    for (int i = 0; i < m_input.size(); ++i) {
        if (m_input.at(i) == "-") // skip stdin
            continue;

        if (QFileInfo(m_input.at(i)) == QFileInfo(m_outputPaths.at(i))) {
            qCritical("%s and %s are the same file.",
                      qUtf8Printable(m_input.at(i)),
                      qUtf8Printable(m_outputPaths.at(i)));
            return false;
        }

        if (m_concat)  // concat mode, don't try to look at >1 output
            break;
    }

    if (m_input.count("-") > 1) {
        qCritical("%s", qUtf8Printable(tr("stdin may appear only once.")));
        return false;
    }

    if (m_output.count("-") > 1) {
        qCritical("%s", qUtf8Printable(tr("stdout may appear only once.")));
        return false;
    }

    if (!m_stat && m_outputPaths.isEmpty()) {
        qCritical("%s", qUtf8Printable(tr("No output files.")));
        return false;
    }

    // Verify indent
    if (m_indent < 0) {
        qCritical("%s", qUtf8Printable(tr("Indent level must be >= 0.")));
        return false;
    }

    // Verify feature types
    for (const auto& feature : m_types) {
        if (HasFeature(GeoLoad::parseFeature(feature), GeoIoFeature::Unk)) {
            qCritical("%s: %s", qUtf8Printable(tr("Unknown feature type")), qUtf8Printable(feature));
            return false;
        }
    }

    // Verify filters
    if (!verifyQuery<TrackModel>(m_filterTrk)) {
        qCritical("%s", qUtf8Printable(tr("Invalid track filter.")));
        return false;
    }

    if (!verifyQuery<WaypointModel>(m_filterWpt)) {
        qCritical("%s", qUtf8Printable(tr("Invalid waypoint filter.")));
        return false;
    }

    return true;
}

bool CmdLine::batchMode() const
{
    return m_concat ||
            !m_input.isEmpty() ||
            !m_output.isEmpty() ||
            !m_format.isEmpty() ||
            !m_dir.isEmpty() ||
            !m_filterTrk.isEmpty() ||
            !m_filterWpt.isEmpty();
}

void CmdLine::reportFile(const GeoIoConv& converter, Stats& stats, int idxOut, int idxIn) const
{
    const int numTrk = converter.count(GeoIoFeature::Trk);
    const int numWpt = converter.count(GeoIoFeature::Wpt);
    const int numPnt = converter.count(GeoIoFeature::Pnt);

    stats.m_totalTrk += numTrk;
    stats.m_totalWpt += numWpt;
    stats.m_totalPnt += numPnt;
    stats.m_totalFiles++;

    if (m_verbose <= 0)
        return;

    const QString& inType  = GeoLoad::formatToName(m_geoFormatIn.at(idxIn));
    const QString& outType = GeoSave::formatToName(m_geoFormatOut.at(idxOut));

    if (m_concat) {
        qInfo("[* -> %s] (%d files) -> %s",
              qUtf8Printable(outType),
              m_input.size(),
              qUtf8Printable(m_verbose > 2 ? m_outputPaths.at(idxOut) :
                                             QFileInfo(m_outputPaths.at(idxOut)).fileName()));
    } else {
        if (!m_concat) {
            const QString counts = QString(" [%1T/%2P/%3W] ").arg(numTrk).arg(numPnt).arg(numWpt);

            qInfo("[%s -> %s]%s%s -> %s",
                  qUtf8Printable(inType),
                  qUtf8Printable(outType),
                  qUtf8Printable(m_verbose > 1 ? counts : " "),
                  qUtf8Printable(m_verbose > 2 ? m_input.at(idxIn) :
                                                 QFileInfo(m_input.at(idxIn)).fileName()),
                  qUtf8Printable(m_verbose > 2 ? m_outputPaths.at(idxOut) :
                                                 QFileInfo(m_outputPaths.at(idxOut)).fileName()));
        }
    }
}

void CmdLine::reportStat(const Stats& stats) const
{
    if (!m_stat)
        return;

    const QString statStr = QString("%1 %2\n%3 %4\n%5 %6\n%7 %8")
                            .arg(stats.m_totalTrk, 8)   .arg(tr("tracks"))
                            .arg(stats.m_totalPnt, 8)   .arg(tr("points"))
                            .arg(stats.m_totalWpt, 8)   .arg(tr("waypoints"))
                            .arg(stats.m_totalFiles, 8) .arg(tr("files"));

    qInfo("%s", qUtf8Printable(statStr));
}

bool CmdLine::batchConvertVerify() const
{
    if (m_concat)
        return m_outputPaths.size() == 1 && m_geoFormatOut.size() == 1;

    return m_input.size() == m_outputPaths.size() &&
           m_input.size() == m_geoFormatIn.size() &&
           m_input.size() == m_geoFormatOut.size();
}

GeoSaveParams CmdLine::saveParams(int i) const
{
    return GeoSaveParams(m_geoFormatOut.at(i), m_features, m_indent);
}

GeoLoadParams CmdLine::loadParams() const
{
    return GeoLoadParams(m_features, { }, { }, { }, QColor(), true, m_filterTrk, m_filterWpt, m_filterCase);
}

int CmdLine::batchConvertSeparate(Stats& stats) const
{
    // per-file mode
    int rc = Exit::Quit;

    for (int i = 0 ; i < m_input.size(); ++i) {
        // Attempt conversion of this file
        if (GeoIoConv converter(m_stdout, m_stdin, saveParams(i), loadParams()); converter.convert(m_outputPaths.at(i), m_input.at(i))) {
            reportFile(converter, stats, i, i);
        } else {
            qCritical("%s: %s", qUtf8Printable(tr("File conversion failed")), qUtf8Printable(m_input.at(i)));
            rc = 5; // attempt to carry on, but remember failure.
        }
    }

    return rc;
}

int CmdLine::batchConvertConcat(Stats& stats) const
{
    // concat / merge mode: keep saving to the first output.
    GeoIoConv converter(m_stdout, m_stdin, saveParams(0), loadParams());

    // Load all inputs
    for (int i = 0 ; i < m_input.size(); ++i) {
        if (!converter.load(m_input.at(i))) {
            qCritical("%s: %s", qUtf8Printable(tr("File load failed")), qUtf8Printable(m_input.at(i)));
            return 5;
        }
    }

    // Save merged result
    if (converter.save(m_outputPaths.front())) {
        reportFile(converter, stats, 0, 0);
    } else {
        qCritical("%s: %s", qUtf8Printable(tr("File save failed")), qUtf8Printable(m_outputPaths.front()));
        return 5;
    }

    return Exit::Quit;
}

void CmdLine::batchConvert() const
{
    // We must load the geopolitical data.  Kick off the load asynchronously.
    if (!app().geoPolMgr().finishLoad())
        throw Exit(5);

    if (!batchConvertVerify()) {
        qCritical("%s", qUtf8Printable(tr("Internal error")));
        throw Exit(5);
    }

    Stats stats;
    int rc = 10;

    if (m_concat)
        rc = batchConvertConcat(stats);
    else
        rc = batchConvertSeparate(stats);

    reportStat(stats);

    throw Exit(rc);
}

void CmdLine::batch() const
{
    CmdLineBase::batch();

    // This can't be done until the app (and thus, units config) is constructed.
    if (m_helpUnitsFeature != GeoIoFeature::Unk)
        helpUnits(m_helpUnitsFeature, m_helpUnitsName);

    if (batchMode())
        batchConvert();
}

