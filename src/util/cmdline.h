/*
    Copyright 2020-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CMDLINE_H
#define CMDLINE_H

#include <src/util/cmdlinebase.h>
#include <src/util/limitedseekstream.h>
#include <src/util/seekablestream.h>
#include <QStringList>
#include <QVector>

enum class GeoFormat;
enum class GeoIoFeature;

class GeoIoConv;
class GeoSaveParams;
class GeoLoadParams;

class CmdLine final : public CmdLineBase
{
    Q_OBJECT

public:
    explicit CmdLine(const char* appname, const char* version, const char* build, const QStringList& = QStringList());
    explicit CmdLine(const char* appname, const char* version, const char* build, int argc, const char** argv);
    explicit CmdLine(const char* appname, const char* version, const char* build, int argc, char** argv);

private:
    [[nodiscard]] bool processArgHelp(int& arg);
    [[nodiscard]] bool processArgBatch(int& arg);
    [[nodiscard]] bool processArg(int& arg) override;
    void usage() const override;

    void setup() override;
    [[nodiscard]] bool verify() const override;

    void helpFilter() const;
    void helpFormatsOutput() const;

    static void helpBatch();
    static void helpFields(GeoIoFeature);
    static void helpUnits(GeoIoFeature, const QString&);

    template <class IO>    static void helpFormats();
    template <class MODEL> static void helpFields();
    template <class MODEL> static void helpUnits(const QString&);

    // Stats
    struct Stats {
        Stats() { }
        uint64_t m_totalFiles = 0;
        uint64_t m_totalTrk   = 0;
        uint64_t m_totalWpt   = 0;
        uint64_t m_totalPnt   = 0;
    };

    bool batchMode() const; // see if we have batch mode work to do
    void batch() const override;  // handle batch (non-GUI) processing
    void batchConvert() const;
    bool batchConvertVerify() const;
    int  batchConvertSeparate(Stats&) const;
    int  batchConvertConcat(Stats&) const;
    void reportFile(const GeoIoConv&, Stats&, int idxOut, int idxIn) const;
    void reportStat(const Stats&) const;

    [[nodiscard]] static GeoIoFeature parseFeatures(const QStringList&);

    [[nodiscard]] GeoSaveParams saveParams(int i) const; // generate save parameters
    [[nodiscard]] GeoLoadParams loadParams() const; // generate load parameters

    template <class MODEL> [[nodiscard]] bool verifyQuery(const QString&) const;

    bool                      m_concat;           // --concat
    bool                      m_clobber;          // --clobber
    bool                      m_import;           // --import
    int                       m_verbose;          // --verbose
    int                       m_indent;           // --indent
    bool                      m_stat;             // --stat
    QStringList               m_input;            // --input ...
    QStringList               m_output;           // --output ...
    QString                   m_format;           // --format
    QString                   m_dir;              // --dir
    QStringList               m_types;            // --type; [wpt, trk]
    QString                   m_filterTrk;        // --filter-trk
    QString                   m_filterWpt;        // --filter-wpt
    Qt::CaseSensitivity       m_filterCase;       // --match-case

                                                  // Computed from other values
    QVector<GeoFormat>        m_geoFormatIn;      // formats of input files, for reporting purposes
    QVector<GeoFormat>        m_geoFormatOut;     // formats of output files, as requested
    QStringList               m_outputPaths;      // processed to same length as m_inputFiles
    GeoIoFeature              m_features;         // trk, wpt
    mutable LimitedSeekStream m_stdin;            // to support stdin seeking
    mutable SeekableStream    m_stdout;           // to support stdout seeking
    GeoIoFeature              m_helpUnitsFeature; // for --help-units
    QString                   m_helpUnitsName;    // ...
};

#endif // CMDLINE_H
