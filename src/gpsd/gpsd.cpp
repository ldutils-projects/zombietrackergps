/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QtConcurrent>
#include <QScopedPointer>
#include <ctime>      // for nanosleep
#include <cmath>      // for isnan
#include <cassert>

#include "gpsd.h"

Gpsd::Gpsd() :
    m_command(Cmd::Continue)
{
    qRegisterMetaType<GpsData>("GpsData");  // register for use with signals
    qRegisterMetaType<Rc>("Rc");

    setupTimers();

    m_idleTimer.start(0); // wait until event loop before sending
}

Gpsd::~Gpsd()
{
    close(); // close dynamic library
}

bool Gpsd::gpsdSupported()
{
    return GPSD_ENABLED != 0 && GpsWrap::hasLibrary();
}

void Gpsd::close()
{
    endCollection();       // Abort GPSD collection
}

void Gpsd::setupTimers()
{
    // We hook this to a timer because open() happens while we're still
    // being constructed, and the owning object may not have connect its
    // signals yet.
    m_idleTimer.setSingleShot(true);
    connect(&m_idleTimer, &QTimer::timeout, this, [this]() { emitStatus(Rc::Idle); });
}

Gpsd::Rc Gpsd::startCollection(const QString& hostname, int port, const QString& device)
{
    if (!GPSD_ENABLED)
        return emitStatus(Rc::NoGpsd);

    m_gpsData.clear();
    command(Cmd::Continue);
    m_rc = QtConcurrent::run(readGpsStatic, this, hostname, port, device);

    return Rc::Success;
}

Gpsd::Rc Gpsd::endCollection()
{
    if (!GPSD_ENABLED)
        return Rc::NoGpsd;

    if (!isRunning())
        return Rc::NotRunning;

    command(Cmd::Halt);

    const Gpsd::Rc rc = m_rc.result();
    emitStatus(Rc::Idle);

    return rc;
}

void Gpsd::command(Cmd cmd)
{
    if (!GPSD_ENABLED)
        return;

    if (cmd == Cmd::TogglePause) {
        if (isRunning())
            command(isPaused() ? Cmd::Continue : Cmd::Pause);
        return;
    }

    m_command = cmd;
}

Gpsd::Rc Gpsd::readGpsStatic(Gpsd* gpsd, const QString& hostname, int port, const QString& device)
{
    return gpsd->readGps(hostname, port, device);
}

Gpsd::Rc Gpsd::process(const GpsWrap& wrap)
{
    // Return on partial packets.  PACKET_SET means a complete response exists.
    if (!(wrap.has(GpsWrap::Set::Packet)))
        return Rc::Success;

    m_gpsData.m_satellites = wrap.satellites_used();

    if (wrap.fix_mode() < 2) // TODO: symbolic
        return Rc::Partial;

    if (wrap.has(GpsWrap::Set::Time))
        m_gpsData.m_point.setTime(wrap.time());

    if (wrap.has(GpsWrap::Set::LatLon)) {
        if (!std::isnan(wrap.lon()) && !std::isnan(wrap.lat()))
            m_gpsData.m_point.setPos(wrap.lon(), wrap.lat());

        if (!std::isnan(wrap.track()))
            m_gpsData.m_point.setCourse(wrap.track());
    }

    if (wrap.has(GpsWrap::Set::Altitude))
        if (!std::isnan(wrap.altMSL()))
            m_gpsData.m_point.setEle(wrap.altMSL());

    if (wrap.has(GpsWrap::Set::Speed))
        if (!std::isnan(wrap.speed()))
            m_gpsData.m_point.setSpeed(Speed_t(wrap.speed()));

    if (wrap.has(GpsWrap::Set::NavData)) {
        if (!std::isnan(wrap.air_temp()))
            m_gpsData.m_point.setATemp(wrap.air_temp());
        if (!std::isnan(wrap.water_temp()))
            m_gpsData.m_point.setWTemp(wrap.water_temp());
    }

    // Require, at minimum, TIME_SET and LATLON_SET
    if (!(wrap.has(GpsWrap::Set::Time) && wrap.has(GpsWrap::Set::LatLon)))
        return Rc::Partial;

    return Rc::Success;
}

Gpsd::Rc Gpsd::readGps(const QString& hostname, int port, const QString& device) // Read from GPS daemon
{
    static const int blockDelayuSec = 500000; // 0.5s
    static const timespec pauseTs = { 0, 250000*1000 }; // 0.25s

    // Open one of the supported shared library versions
    QScopedPointer<GpsWrap> gps(GpsWrap::open(hostname, port, device));

    if (gps.isNull())
        return emitStatus(Rc::NoGpsd);
    if (!gps->isOpen())
        return emitStatus(Rc::NoDevice);

    // Emit signals no matter how we exit the stack frame
    struct signal_t {
        signal_t(Gpsd* gpsd) : m_gpsd(gpsd) {
            m_gpsd->emitStatus(Rc::Acquiring);
            emit m_gpsd->paused(false);
            emit m_gpsd->running(true);
        }
        ~signal_t() {
            emit m_gpsd->running(false);
            emit m_gpsd->paused(false);
        }
        Gpsd* m_gpsd;
    } signal(this);

    bool isPaused = false;

    // Receive loop to gather data from the GPSD
    while (m_command != Cmd::Halt) {
        if (m_command == Cmd::Pause) { // pause collection
            if (!isPaused) {
                emit paused(isPaused = true);
                emitStatus(Rc::Paused);
            }
            nanosleep(&pauseTs, nullptr);
            continue;
        }

        if (isPaused) {
            emit paused(isPaused = false);
            emitStatus(Rc::Acquiring);
        }

        if (!gps->wait(blockDelayuSec)) // continue if there's no response yet
            continue;

        if (Rc rc = gps->read(); rc != Rc::Success) // read and parse
            return emitStatus(rc);

        if (Rc rc = process(*gps); rc == Rc::Success) { // process result
            emit data(m_gpsData);
            emitStatus(Rc::Running);
        } else if (rc != Rc::Partial) {
            return emitStatus(rc);
        }
    }

    return Rc::Success;
}

void Gpsd::GpsData::clear()
{
    m_point.clear();
    m_satellites = 0;
}
