/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GPSWRAPV27_H
#define GPSWRAPV27_H

#include "gpswrap.h"
#include "gpswrapprivate.h"

GPSWRAPDECL(GpsWrapV27, "libgps.so.27", 10)

#include "gpswrapprivateundef.h"

#endif // GPSWRAPV27_H
