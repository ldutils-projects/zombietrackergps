/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <sys/time.h> // for timespec
#include <cassert>
#include <QObject>

#include "gpswrap.h"
#include "gpswrapv30.h"
#include "gpswrapv29.h"
#include "gpswrapv28.h"
#include "gpswrapv27.h"
#include "gpswrapv26.h"
#include "gpswrapv25.h"
#include "gpswrapv24.h"
#include "gpswrapv23.h"

// This is the unix date epoch.
const QDateTime GpsWrap::epochDate = QDateTime::fromString("1970-01-01T00:00:00Z", Qt::ISODate);

GpsWrap::GpsWrap(const char* lib) :
    m_openRc(-1),
    m_libHandle(dlopen(lib, RTLD_NOW))
{
}

GpsWrap::~GpsWrap()
{
    close();
}

bool GpsWrap::hasLibrary()
{
    // API versions we can handle,
    return GpsWrapV29::hasLibrary()  ||
           GpsWrapV30::hasLibrary()  ||
           GpsWrapV28::hasLibrary()  ||
           GpsWrapV27::hasLibrary()  ||
           GpsWrapV26::hasLibrary()  ||
           GpsWrapV25::hasLibrary()  ||
           GpsWrapV24::hasLibrary()  ||
           GpsWrapV23::hasLibrary();
}

GpsWrap::supportedVersions_t GpsWrap::supportedVersions()
{
#define VERDATA(LIB) { LIB::libName, LIB::staticApiVer, LIB::hasLibrary() }
        return { VERDATA(GpsWrapV30),
                 VERDATA(GpsWrapV29),
                 VERDATA(GpsWrapV28),
                 VERDATA(GpsWrapV27),
                 VERDATA(GpsWrapV26),
                 VERDATA(GpsWrapV25),
                 VERDATA(GpsWrapV24),
                 VERDATA(GpsWrapV23) };
#undef VERDATA
}

GpsWrap* GpsWrap::open(const QString& hostname, int port, const QString& device)
{
#define TRYOPEN(LIB) if (LIB::hasLibrary()) return new LIB(hostname, port, device)
    // API versions we can handle, in order from most desirable version to least
    TRYOPEN(GpsWrapV30);
    TRYOPEN(GpsWrapV29);
    TRYOPEN(GpsWrapV28);
    TRYOPEN(GpsWrapV27);
    TRYOPEN(GpsWrapV26);
    TRYOPEN(GpsWrapV25);
    TRYOPEN(GpsWrapV24);
    TRYOPEN(GpsWrapV23);
#undef TRYOPEN

    return nullptr;
}

QString GpsWrap::statusStr(Rc rc)
{
    switch (rc) {
    case Rc::Success:        return QObject::tr("Success");
    case Rc::BadVersion:     return QObject::tr("Required GPSD version not found.");
    case Rc::Partial:        return QObject::tr("Partial data");
    case Rc::NoGpsd:         return QObject::tr("No GPSD found");
    case Rc::GpsdError:      return QObject::tr("Error opening GPSD");
    case Rc::NoDevice:       return QObject::tr("No GPSD device found");
    case Rc::ReadError:      return QObject::tr("Read error from device");
    case Rc::NotRunning:     return QObject::tr("Not running");
    case Rc::AlreadyRunning: return QObject::tr("Already running");
    case Rc::Idle:           return QObject::tr("Idle");
    case Rc::Paused:         return QObject::tr("Paused");
    case Rc::Acquiring:      return QObject::tr("Acquiring");
    case Rc::Running:        return QObject::tr("Running");
    }

    assert(0);
    return QObject::tr("Unknown");
}

void GpsWrap::close()
{
    if (isLibOpen()) {
        dlclose(m_libHandle);  // Close the shared library
        m_libHandle = nullptr; // So foundLibrary() will return false
    }
}

bool GpsWrap::hasLibrary(const char* libName)
{
   if (void* handle = dlopen(libName, RTLD_LAZY); handle != nullptr) {
       dlclose(handle);
       return true;
   }

   return false;
}

bool GpsWrap::has(Set set) const
{
    return has(uint64_t(set));
}

QDateTime GpsWrap::time(const timespec& ts)
{
    return epochDate
            .addSecs(ts.tv_sec)
            .addMSecs(ts.tv_nsec / 1000000);
}

QDateTime GpsWrap::time(double time)
{
    return epochDate.addMSecs(time * 1000.0);
}

