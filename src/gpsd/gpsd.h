/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GPSD_H
#define GPSD_H

#include <QObject>
#include <QFuture>
#include <QString>
#include <QDateTime>
#include <QTimer>
#include <functional>
#include <atomic>

#include "src/core/pointitem.h"
#include "gpswrap.h"

#if DISABLE_GPSD
#  include "gpsdstubs.h"   // dummy values to allow compilation
#  warning "gps.h not found: gpsd support will be disabled."
#  define GPSD_ENABLED 0
#else
#  define GPSD_ENABLED 1
#endif

// Support for linux gpsd daemon.
class Gpsd final : public QObject
{
    Q_OBJECT

public:
    using Rc = GpsWrap::Rc;

    Gpsd();
    ~Gpsd() override;

    // async task commands
    enum class Cmd {
        Continue,    // keep on trucking
        Pause,       // stop collection
        TogglePause, // toggle paused state
        Halt,        // halt data collection
    };

    // Package of data from the GPS, including a constructed PointItem
    struct GpsData {
        void clear();

        PointItem   m_point;          // sampled point
        int         m_satellites = 0; // satellite count
    };

    static const constexpr char* defaultHost = "localhost";
    static const constexpr int   defaultPort = 2947;
    static const constexpr char* defaultDevice = "";

    // Return true if we have static support for GPSD compilation AND a test dlopen succeeds
    static bool gpsdSupported();

    Rc   startCollection(const QString& hostname = defaultHost, int port = defaultPort,
                         const QString& device = defaultDevice); // Read from GPS daemon asynchronously
    Rc   endCollection(); // ask collection loop to exit
    void command(Cmd);  // issue command to background thread
    [[nodiscard]] bool isRunning() const { return m_rc.isRunning(); }
    [[nodiscard]] bool isPaused() const { return isRunning() && m_command == Cmd::Pause; }

    [[nodiscard]] const QString& errorStr() const { return m_errorStr; }
    [[nodiscard]] static QString statusStr(Rc rc) { return GpsWrap::statusStr(rc); }

signals:
    void running(bool);
    void paused(bool);
    void data(const Gpsd::GpsData&);
    void status(Gpsd::Rc);

private:
    void close();
    void setupTimers(); // timer setup

    Rc emitStatus(Rc rc) { emit status(rc); return rc; }

    // The static function is called from QtConcurrent, and simply calls the other.
    static Rc readGpsStatic(Gpsd*, const QString& hostname, int port, const QString& device); // Call non-static read loop
    Rc readGps(const QString&, int port, const QString& device); // Read loop

    Rc process(const GpsWrap&);    // process messages through GpsWrap interface to abstract away GPSD ABI

    QFuture<Rc>      m_rc;         // result from async collection
    std::atomic<Cmd> m_command;    // command the async task to do something
    QString          m_errorStr;   // dlsym error string
    GpsData          m_gpsData;    // data from GPS for consumers of signals
    QTimer           m_idleTimer;  // send idle status

    // Disable copying
    Gpsd& operator=(const Gpsd&) = delete;
    Gpsd(const Gpsd&) = delete;
};

Q_DECLARE_METATYPE(Gpsd::GpsData)
Q_DECLARE_METATYPE(Gpsd::Rc)

#endif // GPSD_H
