/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GPSWRAP_H
#define GPSWRAP_H

#include <dlfcn.h>    // for dlopen
#include <functional>
#include <QDateTime>
#include <QString>
#include <QVector>
#include <tuple>
#include <array>

struct timespec;

// Wrap opening and closing of GPS device to make it return & exception safe
// This also provides abstraction against GPS API format changes, e.g, timespec->timestamp,
// and changes in offsets of members into the gps_data_t structure as the binary
// format changes.  This is awkward!  Unfortunately we can't easily stick to a single
// GPSD version, because that would bake it in at compile time, and different distros
// have different versions, or the user's system might not have the package at all,
// so we need to resolve this at runtime, not compile time.
class GpsWrap {
public:
    // Return & error codes
    enum class Rc {
        Success,        // everything is okay.
        BadVersion,     // failed to find required gpsd version
        Partial,        // partial data
        NoGpsd,         // no GPSD daemon
        GpsdError,      // GPSD error
        NoDevice,       // device not found
        ReadError,      // read error from device
        NotRunning,     // read was not running
        AlreadyRunning, // read is already running
        Idle,           // idle state
        Paused,         // paused state
        Acquiring,      // acquiring state
        Running,        // running state
    };

    // TODO: It is not desirable to replicate these values from gps.h here, but we are dealing
    // with multiple versions of the header at once (to support multiple ABI versions) and we
    // cannot include them all in the came c++ due to namespace collisions.
    enum class Set : uint64_t {
        Time     = (1llu<<2),
        LatLon   = (1llu<<4),
        Altitude = (1llu<<5),
        Speed    = (1llu<<6),
        NavData  = (1llu<<34),
        Packet   = (1llu<<25),
    };

    GpsWrap(const char* lib);
    virtual ~GpsWrap();

    bool isLibOpen() const { return m_libHandle != nullptr; }
    bool isOpen() const { return m_openRc == 0; }
    bool has(Set) const;

    virtual bool wait(int uSec) = 0;
    virtual Rc   read() = 0; // read and parse next response
    virtual int  apiVersion() const = 0;

    virtual bool has(uint64_t set) const = 0;
    virtual double altMSL() const = 0;
    virtual QDateTime time() const = 0;
    virtual double lat() const = 0;
    virtual double lon() const = 0;
    virtual double track() const = 0;
    virtual double speed() const = 0 ;
    virtual double air_temp() const = 0;
    virtual double water_temp() const = 0;
    virtual int satellites_used() const = 0;
    virtual uint32_t fix_mode() const = 0;

    static bool hasLibrary();

    // Query lib name, API version, and detected status of all supported GPSD lib versions
    using supportedVersions_t = QVector<std::tuple<QString, int, bool>>;
    static supportedVersions_t supportedVersions();

    // Open best libgps.so library version we can find. We have to specify exact versions,
    // since the APIs and ABIs change.
    static GpsWrap* open(const QString& hostname, int port, const QString& device);

    static QString statusStr(Rc);

protected:
    // TODO: It is not desirable to replicate these values from gps.h here, but we are dealing
    // with multiple versions of the header at once (to support multiple ABI versions) and we
    // cannot include them all in the came c++ due to namespace collisions.
    enum Watch {
        WatchEnable = 0x000001u,
        WatchJson   = 0x000010u,
        WatchDevice = 0x000800u,
    };

    static const QDateTime epochDate;  // unix epoch

    static bool hasLibrary(const char* libName); // test for dlopen-ability on given lib

    static QDateTime time(const timespec&);
    static QDateTime time(double);

    template <typename FN> bool dlsym(std::function<FN>&, const char* name);
    template <typename PRIVATE> void construct(PRIVATE*&, const QString&, int port, const QString& device);
    template <typename PRIVATE> void destruct(PRIVATE*&);

    void close();

    int         m_openRc;    // open return code
    void*       m_libHandle; // handle for dynamically opening library
};

template <typename FN> bool GpsWrap::dlsym(std::function<FN>& fn, const char* name)
{
    fn = reinterpret_cast<FN*>(::dlsym(m_libHandle, name));

    return bool(fn); // cast to bool returns true if the std::function is valid
}

// Avoid replication in each version-specific class constructor
template <typename PRIVATE>
void GpsWrap::construct(PRIVATE*& priv, const QString& hostname, int port, const QString& device)
{
    const auto fail = [this, &priv]() { destruct(priv); };

    if (!isLibOpen())
        return fail();

    // Allocate ABI-specific private data
    if (priv = new PRIVATE(); priv == nullptr)
        return fail();

    // Find the entry points we need. Close lib if that failed.
#   define DLSYM(s) dlsym(priv->s, #s)  // stringification
    if (!DLSYM(gps_open)    ||
        !DLSYM(gps_close)   ||
        !DLSYM(gps_waiting) ||
        !DLSYM(gps_read)    ||
        !DLSYM(gps_stream)  ||
        !DLSYM(gps_errstr))
        return fail();
#   undef DLSYM

    const char* deviceStr = device.isEmpty() ? nullptr : qUtf8Printable(device);

    // device opened in constructor and closed in destructor
    std::array<char, 16> portStr = { };
    qsnprintf(portStr.data(), sizeof(portStr), "%d", port);

    if (m_openRc = priv->open(qUtf8Printable(hostname), portStr.data()); m_openRc != 0)
        return fail();

    const int devFlag = (device != nullptr) ? WatchDevice : 0;

    // the gps_stream interface should properly take a const char* device, but it's a non-const
    priv->gps_stream(&priv->m_gpsData, WatchEnable | WatchJson | devFlag,
                     const_cast<char*>(deviceStr));
}

template <typename PRIVATE> void GpsWrap::destruct(PRIVATE*& priv)
{
    delete priv;
    priv = nullptr;
    close();
}

#endif // GPSWRAP_H
