/*
    Copyright 2021 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CLIMBITEM_H
#define CLIMBITEM_H

#include "src/core/treeitem.h"

class ClimbModel;

class ClimbItem final : public TreeItem
{
public:
    explicit ClimbItem(TreeItem* parent = nullptr);
    explicit ClimbItem(const TreeItem::ItemData& data, TreeItem* parent = nullptr);

    QVariant data(int column, int role) const override;

    using TreeItem::setData;
    bool setData(int column, const QVariant& value, int role, bool& changed) override;

    int columnCount() const override;

    ClimbItem* factory(const ClimbItem::ItemData& data, TreeItem* parent) override;

    friend class ClimbModel;
};

#endif // CLIMBITEM_H
