/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <QHash>
#include <QSet>
#include <QStringList>
#include <src/util/roles.h>
#include <src/util/util.h>
#include <src/util/units.h>
#include <src/core/modelmetadata.inl.h>

#include "tagmodel.h"
#include "tagitem.h"
#include "app.h"

// Densities in kg/m^3
decltype(TagModel::medium) TagModel::medium = {
    { "None",   { 0,      false }},
    { "Air",    { 1.225,  true  }},
    { "Water",  { 1000.0, false }},
    { "Vacuum", { 0,      false }},
};

TagModel::TagModel(QObject *parent) :
    ContentAddrModel(new TagItem(), TagModel::Name, Util::RawDataRole, parent)
{
}

TagItem* TagModel::getItem(const QModelIndex &idx) const
{
    return static_cast<TagItem*>(ContentAddrModel::getItem(idx)); 
}

QStringList TagModel::mediumNames()
{
    return medium.keys();
}

QString TagModel::tagIconName(const QString& tag) const
{
    // Find entry in the tags list to obtain tag color
    const QModelIndex tagIdx = keyIdx(tag);
    if (!tagIdx.isValid())
        return { };

    const auto tagIcon  = data(TagModel::Icon, tagIdx, Util::IconNameRole).value<QString>();
    const auto tagColor = data(TagModel::Color, tagIdx, Qt::BackgroundRole).value<QColor>();

    return cfgData().svgColorizer.filename(tagIcon, tagColor, cfgData().colorizeTagIcons);
}

QStringList TagModel::tagIconNames(const QStringList& tags) const
{
    QStringList tagIcons;

    for (const auto& tag : tags)
        if (const QString tagIcon = tagIconName(tag); !tagIcon.isEmpty())
            tagIcons.append(tagIcon);

    return tagIcons;
}

void TagModel::setUniqueIds() 
{
    int id = 0;
    Util::Recurse(*this, [this, &id](const QModelIndex& idx) {
        getItem(idx)->m_id = id++;
        return true;
    });
}

// If tags are renamed, we create a map of the old to the new names (as far as we can figure
// that out), so that tag names in tracks can be automatically renamed.
QHash<QString, QString> TagModel::newIdNames(const TagModel& old)
{
    // Map new IDs to new names
    QHash<int, QString> newIds;
    QSet<QString> newNames;

    Util::Recurse(*this, [&](const QModelIndex& idx) {
        const QString newName = data(TagModel::Name, idx, Util::RawDataRole).toString();
        if (getItem(idx)->m_id >= 0)
            newIds.insert(getItem(idx)->m_id, newName);
        newNames.insert(newName);
        return true;
    });

    // Build map of old names to new names, connected via ID.
    decltype(newIdNames(old)) oldToNew;

    Util::Recurse(old, [&](const QModelIndex& idx) {
        const int id = old.getItem(idx)->m_id;
        assert(id >= 0);  // must have an ID for everything in the old map (but not necessarily the new)

        const QString oldName = old.data(TagModel::Name, idx, Util::RawDataRole).toString();

        if (const auto it = newIds.find(id); it != newIds.end()) {
            // If new model has this ID with different name, add it to the map.
            if (oldName != it.value())
                oldToNew.insert(oldName, it.value());
        } else if (!newNames.contains(oldName)) { // accept same names even if missing ID
            // If new model does not have this ID, it was removed.
            oldToNew.insert(oldName, QString());
        }
        return true;
    });

    return oldToNew;
}

const Units& TagModel::units(const QModelIndex& idx) const
{
    return mdUnits(idx.column());
}

// Return any override units for this tag
const Units& TagModel::units(const QString& key, ModelType mt) const
{
    return getItem(keyIdx(key))->getUnits(mt);
}

// We intercept setIon because we only set the IconNameRole, not the DecorationRole,
// because we will get the decoration from the colorization cache.
bool TagModel::setIcon(const QModelIndex& idx, const QString& iconFile)
{
    if (iconFile.isEmpty())
        return setData(idx, QVariant(), Util::IconNameRole);

    return setData(idx, iconFile, Util::IconNameRole);
}

bool TagModel::appendRows(bool category, const QVector<TreeItem::ItemData> &data, const QModelIndex &parent)
{
    const int firstNew = rowCount(parent);
    const bool rc = TreeModel::appendRows(data, parent);

    for (int r = firstNew; r < rowCount(parent); ++r)
        getItem(index(r, 0, parent))->setCategory(category);

    return rc;
}

bool TagModel::appendRow(bool category, const TreeItem::ItemData& data, const QModelIndex& parent)
{
    return appendRows(category, { data }, parent);
}

QVariant TagModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (const QVariant val = ModelMetaData::headerData<TagModel>(section, orientation, role); val.isValid())
        return val;

    if (role == Qt::DisplayRole)
        return mdName(section);

    return ContentAddrModel::headerData(section, orientation, role);
}

Qt::ItemFlags TagModel::flags(const QModelIndex& idx) const
{
    const bool isCat = isCategory(idx);

    Qt::ItemFlags flags = ContentAddrModel::flags(idx) | Qt::ItemIsDragEnabled;

    if (isCat)
        flags |= Qt::ItemIsDropEnabled;

    if (!isCat || (isCat && (idx.column() == TagModel::Name)))
        flags |= Qt::ItemIsEditable;

    if (!isCat && (idx.column() == TagModel::Color ||
                   idx.column() == TagModel::Icon))
        flags &= ~Qt::ItemIsSelectable;

    return flags;
}

QString TagModel::mdName(ModelType mt)
{
    switch (mt) {
    case TagModel::Name:       return QObject::tr("Name");
    case TagModel::Color:      return QObject::tr("Color");
    case TagModel::Icon:       return QObject::tr("Icon");
    case TagModel::CdA:        return QObject::tr("CdA");
    case TagModel::Weight:     return QObject::tr("Weight");
    case TagModel::RR:         return QObject::tr("Roll Resistance");
    case TagModel::Efficiency: return QObject::tr("Thermal Efficiency");
    case TagModel::BioPct:     return QObject::tr("% Bio-power");
    case TagModel::Medium:     return QObject::tr("Medium");
    case TagModel::UnitSpeed:  return QObject::tr("Speed Unit");
    case TagModel::_Count:     break;
    }

    assert(0 && "Unknown TagModel value");
    return "";
}

bool TagModel::mdIsEditable(ModelType)
{
    return true;
}

// Return true if this data can be placed in a chart
bool TagModel::mdIsChartable(ModelType)
{
    return false;
}

// tooltip for container column header
QString TagModel::mdTooltip(ModelType mt)
{
    static const QString powerTxt = QObject::tr(
                "This is used for power estimation.  The value can be "
                "unset by setting it to a negative value.");

    switch (mt) {
    case TagModel::Icon:       return QObject::tr("<i></i>Icon displayed for tracks using this tag.");
    case TagModel::Color:      return QObject::tr("<i></i>Default color for tracks using this tag.  May be "
                                                  "overridden on a per-track basis.");
    case TagModel::Name:       return QObject::tr("<i></i>Name for this tag.");
    case TagModel::CdA:        return QObject::tr("<i></i>Optional drag coefficient * frontal area in m^2. ") +
                                                  powerTxt;
    case TagModel::Weight:     return QObject::tr("<i></i>Optional vehicle weight. ") + powerTxt;
    case TagModel::RR:         return QObject::tr("<i></i>Optional vehicle rolling resistance. ") +
                                                  powerTxt;
    case TagModel::Efficiency: return QObject::tr("<i></i>Optional vehicle thermal efficiency, accounting for "
                                                  "drivetrain and engine losses, etc. ") + powerTxt;
    case TagModel::BioPct:     return QObject::tr("<i></i>Optional percent of power that comes from human power. "
                                                  "For a bicycle or hiking this will be 100%, and for an automobile "
                                                  "or motorcycle it will be 0%.  An E-bike might be 10%. ") + powerTxt;
    case TagModel::Medium:     return QObject::tr("<i></i>Vehicle medium. ") + powerTxt;
    case TagModel::UnitSpeed:  return QObject::tr("<i></i>If set, override the default speed display unit for this tag.");
    case TagModel::_Count:     break;
    }

    assert(0 && "Unknown TagModel value");
    return "";
}

// tooltip for container column header
QString TagModel::mdWhatsthis(ModelType mt)
{
    return mdTooltip(mt);  // just pass through the tooltip
}

Qt::Alignment TagModel::mdAlignment(ModelType mt)
{
    switch (mt) {
    case TagModel::CdA:        [[fallthrough]];
    case TagModel::Weight:     [[fallthrough]];
    case TagModel::RR:         [[fallthrough]];
    case TagModel::Efficiency: [[fallthrough]];
    case TagModel::BioPct:     return Qt::AlignRight | Qt::AlignVCenter;
    default:                   return Qt::AlignLeft | Qt::AlignVCenter;
    }
}

int TagModel::mdDataRole(ModelType mt)
{
    switch (mt) {
    case TagModel::Icon:       return Qt::DecorationRole;
    case TagModel::Color:      return Qt::BackgroundRole;
    case TagModel::Name:       [[fallthrough]];
    case TagModel::CdA:        [[fallthrough]];
    case TagModel::Weight:     [[fallthrough]];
    case TagModel::RR:         [[fallthrough]];
    case TagModel::Efficiency: [[fallthrough]];
    case TagModel::BioPct:     [[fallthrough]];
    case TagModel::Medium:     [[fallthrough]];
    case TagModel::UnitSpeed:  return Util::RawDataRole;
    case TagModel::_Count:     break;
    }

    assert(0 && "Unknown TagModel value");
    return Util::RawDataRole;
}

const Units& TagModel::mdUnits(ModelType mt)
{
    static const Units rawString(Format::String);
    static const Units rawFloat(Format::Float);
    static const Units rawFloatRR(Format::Float, 4);

    switch (mt) {
    case TagModel::Icon:       [[fallthrough]];
    case TagModel::Color:      [[fallthrough]];
    case TagModel::Medium:     [[fallthrough]];
    case TagModel::UnitSpeed:  [[fallthrough]];
    case TagModel::Name:       return rawString;
    case TagModel::CdA:        return rawFloat;
    case TagModel::Weight:     return cfgData().unitsWeight;
    case TagModel::RR:         return rawFloatRR;
    case TagModel::Efficiency: [[fallthrough]];
    case TagModel::BioPct:     return cfgData().unitsPct;
    case TagModel::_Count:     break;
    }

    assert(0 && "Unknown TagModel value");
    return rawString;
}

TagModel& TagModel::operator=(const TagModel &rhs)
{
    ContentAddrModel::operator=(rhs);
    return *this;
}

bool TagModel::isCategory(const QModelIndex& idx) const
{
    return getItem(idx)->isCategory();
}

void TagModel::setCategory(const QModelIndex& idx, bool category)
{
    getItem(idx)->setCategory(category);
}
