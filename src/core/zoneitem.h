/*
    Copyright 2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ZONEITEM_H
#define ZONEITEM_H

#include "src/core/treeitem.h"

class ZoneModel;

class ZoneItem final : public TreeItem
{
private:
    explicit ZoneItem(TreeItem* parent = nullptr);
    explicit ZoneItem(const TreeItem::ItemData& data, TreeItem* parent = nullptr);

    QVariant data(int column, int role) const override;

    using TreeItem::setData;
    bool setData(int column, const QVariant& value, int role, bool& changed) override;

    int columnCount() const override;

    ZoneItem* factory(const ZoneItem::ItemData& data, TreeItem* parent) override;

    friend class ZoneModel;
};

#endif // ZONEITEM_H
