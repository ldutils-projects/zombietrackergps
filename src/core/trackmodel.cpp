/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <type_traits>
#include <tuple>

#include <QDataStream>

#include <marble/GeoDataLatLonBox.h>

#include <src/util/util.h>
#include <src/util/ui.h>
#include <src/util/units.h>
#include <src/util/versionedstream.h>
#include <src/util/variantcmp.h>
#include <src/core/modelmetadata.inl.h>

#include "app.h"
#include "pointmodel.h"
#include "trackupdatethread.h"
#include "trackmodel.h"
#include "trackitem.h"
#include "geotypes.h"

TrackModel::TrackModel(QObject* parent) :
    MergeableModel(new TrackItem(), parent),
    NamedItem(getItemNameStatic()),
    m_loadPending(false)
{
    setHorizontalHeaderLabels(headersList<TrackModel>());
}

QModelIndex TrackModel::appendRow(const QString& name, TrackType type,
                                  const QStringList& tags,
                                  const QColor& color,
                                  const QString& notes,
                                  const QString& keywords,
                                  const QString& source,
                                  PointModel& newPoints,
                                  const QModelIndex& parent)
{
    TreeItem::ItemData data;

    // TrackModel namespace not needed here, but it improves greppability.
    data.resize(TrackModel::_Count);

    if (!name.isEmpty())     data[TrackModel::Name]     = name;
    if (!tags.isEmpty())     data[TrackModel::Tags]     = tags;
    if (color.isValid())     data[TrackModel::Color]    = color;
    if (!notes.isEmpty())    data[TrackModel::Notes]    = notes;
    if (!keywords.isEmpty()) data[TrackModel::Keywords] = keywords;
    if (!source.isEmpty())   data[TrackModel::Source]   = source;

    data[TrackModel::Type] = type;

    MergeableModel::appendRow(data, parent);

    const QModelIndex newIdx = index(rowCount()-1, 0, parent);

    getItem(newIdx)->append(*this, newPoints);
    getItem(newIdx)->setPersonIdx(m_personIdx); // set configured person

    emit dataChanged(newIdx, rowSibling(columnCount()-1, newIdx));

    return newIdx;
}

QModelIndex TrackModel::appendRow(const QString& name, TrackType type,
                                  const QStringList& tags, const QColor& color,
                                  const QModelIndex& parent)
{
    PointModel pm;

    return appendRow(name, type, tags, color,
                     QString(), // notes
                     QString(), // keywords
                     QString(), // source,
                     pm, parent);
}

Marble::GeoDataLatLonBox TrackModel::bounds(const QModelIndex& idx) const
{
    return getItem(idx)->bounds();
}

QVariant TrackModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (const QVariant val = ModelMetaData::headerData<TrackModel>(section, orientation, role); val.isValid())
        return val;

    return MergeableModel::headerData(section, orientation, role);
}

const Units& TrackModel::units(const QModelIndex& idx) const
{
    if (idx.isValid())
        return getItem(idx)->units(idx.column());  // use tag specific override if available

    return mdUnits(idx.column());
}


QString TrackModel::tooltip(const QModelIndex& idx) const
{
    static const QVector<ModelType> tooltipColumns = {
        TrackModel::BeginDate,
        TrackModel::Length,
        TrackModel::TotalTime,
        TrackModel::MovingTime,
        TrackModel::MaxElevation,
        TrackModel::AvgMovSpeed,
        TrackModel::MaxSpeed,
        TrackModel::Ascent,
        TrackModel::MaxGrade,
        TrackModel::AvgMovPower,
        TrackModel::AvgHR,
        TrackModel::MaxHR,
        TrackModel::Energy,
        TrackModel::Notes,
    };

    QString tooltip = QString("<p><b><u><nobr><big>") +
            data(idx.sibling(idx.row(), TrackModel::Name), Qt::DisplayRole).toString() +
            + "</big></nobr></u></b></p>";

    tooltip += "<table>";

    const auto colorWrap = [this](QString& tooltip, const QVariant& colData, const QModelIndex& idx, ModelType auxCol, const QString& prefix = "", const QString& suffix = "")
    {
        if (auxCol < TrackModel::_First || !colData.isValid())
            return;

        const QVariant color = cfgData().trackColorizer.colorize(rowSibling(auxCol, idx), Qt::ForegroundRole);

        tooltip += prefix;
        if (color.isValid())
            tooltip += "<font color=\"" + color.value<QColor>().name() +  "\">";

        tooltip += colData.toString();

        if (color.isValid())
            tooltip += "</font>";

        tooltip += suffix;
    };

    for (const auto col : tooltipColumns) {
        const QModelIndex colIdx = idx.sibling(idx.row(), col);
        const int         role   = isTextField(col) ? Util::RawDataRole : Qt::DisplayRole;

        if (QVariant colData = data(colIdx, role); colData.isValid()) {
            // If text exceeds size threshold, replace with icon.
            if (isTextField(col) && Util::PlainTextLen(colData) > cfgData().tooltipInlineLimit)
                colData = "<img src=\"" + cfgData().trackNoteIcon + "\"></img>";

            tooltip += "<tr><td><b>" + mdName(col) + " </b></td><td>";
            colorWrap(tooltip, colData, colIdx, col);

            ModelType auxCol = -1;
            if (col == TrackModel::AvgHR) auxCol = TrackModel::AvgHRPct;
            if (col == TrackModel::MaxHR) auxCol = TrackModel::MaxHRPct;

            // Add aux data in parens if present
            colorWrap(tooltip, colData, colIdx, auxCol, " (", ")" );

            tooltip += "</td></tr>";
        }
    }

    tooltip += "</table>";

    return tooltip;
}

void TrackModel::beginThreads()
{
    // Use threads for recalculation.
    m_loadPending = true;
    // Leave one thread for disk IO and so forth.
    const int threadCount = std::max(QThread::idealThreadCount() - 1, 1);

    for (int t = 0; t < threadCount; ++t)
        m_threadPool.start(new TrackUpdateThread(*this));
}

void TrackModel::endThreads()
{
    m_loadPending = false;
    m_threadPool.waitForDone(); // wait for all threads to complete
    assert(m_updateQueue.isEmpty());              // we must have updated everything
}

// Handle settings changes: recalculate per-track data.
void TrackModel::refresh()
{
    // Use threads for recalculation.
    beginThreads();
    Util::Recurse(*this, [this](const QModelIndex& idx) {
        getItem(idx)->refresh(*this);
        return true;
    });
    endThreads();

    MergeableModel::refresh();   // let parent class send data changed event
}

std::tuple<qreal, qreal, qreal, qreal, bool>
TrackModel::bounds(const QModelIndexList& selection) const
{
    static const qreal NaN = std::numeric_limits<qreal>::quiet_NaN();

    qreal maxlat = NaN;
    qreal maxlon = NaN;
    qreal minlat = NaN;
    qreal minlon = NaN;

    for (const auto& idx : selection) {
        maxlat = std::fmax(maxlat, data(TrackModel::MaxLat, idx, Util::RawDataRole).toDouble());
        maxlon = std::fmax(maxlon, data(TrackModel::MaxLon, idx, Util::RawDataRole).toDouble());
        minlat = std::fmin(minlat, data(TrackModel::MinLat, idx, Util::RawDataRole).toDouble());
        minlon = std::fmin(minlon, data(TrackModel::MinLon, idx, Util::RawDataRole).toDouble());
    }

    return std::make_tuple(maxlat, maxlon, minlat, minlon,
                           (!std::isnan(maxlat) && !std::isnan(maxlon) && !std::isnan(minlat) && !std::isnan(minlon)));
}

// TODO: share with WaypointModel, others
Marble::GeoDataLatLonBox TrackModel::boundsBox(const QModelIndexList &selection) const
{
    const auto [maxlat, maxlon, minlat, minlon, valid] = bounds(selection);

    if (!valid)
        return { };

    return { maxlat, minlat, maxlon, minlon, Marble::GeoDataCoordinates::Degree };
}

Qt::ItemFlags TrackModel::flags(const QModelIndex& idx) const
{
    const Qt::ItemFlags flags = MergeableModel::flags(idx) | ModelMetaData::flags<TrackModel>(idx) |
            Qt::ItemIsDragEnabled;

    switch (idx.column()) {
    case TrackModel::Color: [[fallthrough]];
    case TrackModel::Tags:  [[fallthrough]];
    case TrackModel::Notes: [[fallthrough]];
    case TrackModel::Flags: return flags & ~Qt::ItemIsSelectable;
    default:                return flags;
    }
}

TrackItem* TrackModel::getItem(const QModelIndex& idx) const
{
    return static_cast<TrackItem*>(MergeableModel::getItem(idx));
}

QString TrackModel::mdName(ModelType mt)
{
    switch (mt) {
    case TrackModel::Name:         return tr("Name");
    case TrackModel::Type:         return tr("Type");
    case TrackModel::Tags:         return tr("Tags");
    case TrackModel::Color:        return tr("Color");
    case TrackModel::Notes:        return tr("Notes");
    case TrackModel::Keywords:     return tr("Keywords");
    case TrackModel::Source:       return tr("Source");
    case TrackModel::Flags:        return tr("Flags");
    case TrackModel::Length:       return tr("Length");
    case TrackModel::BeginDate:    return tr("Begin Date");
    case TrackModel::EndDate:      return tr("End Date");
    case TrackModel::BeginTime:    return tr("Begin Time");
    case TrackModel::EndTime:      return tr("End Time");
    case TrackModel::StoppedTime:  return tr("Stopped Time");
    case TrackModel::MovingTime:   return tr("Moving Time");
    case TrackModel::TotalTime:    return tr("Total Time");
    case TrackModel::MinElevation: return tr("Min Elev");
    case TrackModel::AvgElevation: return tr("Avg Elev");
    case TrackModel::MaxElevation: return tr("Max Elev");
    case TrackModel::MinSpeed:     return tr("Min Speed");
    case TrackModel::AvgOvrSpeed:  return tr("Overall Spd");
    case TrackModel::AvgMovSpeed:  return tr("Moving Spd");
    case TrackModel::MaxSpeed:     return tr("Max Speed");
    case TrackModel::MinGrade:     return tr("Min Grade");
    case TrackModel::AvgGrade:     return tr("Avg Grade");
    case TrackModel::MaxGrade:     return tr("Max Grade");
    case TrackModel::MinCad:       return tr("Min Cad");
    case TrackModel::AvgMovCad:    return tr("Moving Cad");
    case TrackModel::MaxCad:       return tr("Max Cad");
    case TrackModel::MinPower:     return tr("Min Pow");
    case TrackModel::AvgMovPower:  return tr("Moving Pow");
    case TrackModel::MaxPower:     return tr("Max Pow");
    case TrackModel::Energy:       return tr("Energy");
    case TrackModel::Ascent:       return tr("Ascent");
    case TrackModel::Descent:      return tr("Descent");
    case TrackModel::BasePeak:     return tr("Peak-Base");
    case TrackModel::Segments:     return tr("Segs");
    case TrackModel::Points:       return tr("Points");
    case TrackModel::Area:         return tr("Area");
    case TrackModel::MinTemp:      return tr("Min T");
    case TrackModel::AvgTemp:      return tr("Avg T");
    case TrackModel::MaxTemp:      return tr("Max T");
    case TrackModel::MinHR:        return tr("Min HR");
    case TrackModel::AvgHR:        return tr("Avg HR");
    case TrackModel::MaxHR:        return tr("Max HR");
    case TrackModel::Laps:         return tr("Lap Count");
    case TrackModel::MinLon:       return tr("Min Lon");
    case TrackModel::MinLat:       return tr("Min Lat");
    case TrackModel::MaxLon:       return tr("Max Lon");
    case TrackModel::MaxLat:       return tr("Max Lat");
    case TrackModel::MinHRPct:     return tr("Min HR %");
    case TrackModel::AvgHRPct:     return tr("Avg HR %");
    case TrackModel::MaxHRPct:     return tr("Max HR %");
    case TrackModel::BeginToEndEle:return tr("BeginToEndEle");

    case TrackModel::_Count:       break;
    }

    assert(0 && "Unknown TrackModel value");
    return "";
}

bool TrackModel::mdIsEditable(ModelType mt)
{
    switch (mt) {
    case TrackModel::Name:         [[fallthrough]];
    case TrackModel::Type:         [[fallthrough]];
    case TrackModel::Tags:         [[fallthrough]];
    case TrackModel::Color:        [[fallthrough]];
    case TrackModel::Notes:        [[fallthrough]];
    case TrackModel::Keywords:     [[fallthrough]];
    case TrackModel::Source:       return true;
    default:                       return false;
    }
}

// Return true if this data can be placed in a chart
bool TrackModel::mdIsChartable(ModelType mt)
{
    switch (mt) {
    case TrackModel::Length:       [[fallthrough]];
    case TrackModel::StoppedTime:  [[fallthrough]];
    case TrackModel::MovingTime:   [[fallthrough]];
    case TrackModel::TotalTime:    [[fallthrough]];
    case TrackModel::MinElevation: [[fallthrough]];
    case TrackModel::AvgElevation: [[fallthrough]];
    case TrackModel::MaxElevation: [[fallthrough]];
    case TrackModel::MinSpeed:     [[fallthrough]];
    case TrackModel::AvgOvrSpeed:  [[fallthrough]];
    case TrackModel::AvgMovSpeed:  [[fallthrough]];
    case TrackModel::MaxSpeed:     [[fallthrough]];
    case TrackModel::MinGrade:     [[fallthrough]];
    case TrackModel::AvgGrade:     [[fallthrough]];
    case TrackModel::MaxGrade:     [[fallthrough]];
    case TrackModel::MinCad:       [[fallthrough]];
    case TrackModel::AvgMovCad:    [[fallthrough]];
    case TrackModel::MaxCad:       [[fallthrough]];
    case TrackModel::MinPower:     [[fallthrough]];
    case TrackModel::AvgMovPower:  [[fallthrough]];
    case TrackModel::MaxPower:     [[fallthrough]];
    case TrackModel::Energy:       [[fallthrough]];
    case TrackModel::Ascent:       [[fallthrough]];
    case TrackModel::Descent:      [[fallthrough]];
    case TrackModel::BasePeak:     [[fallthrough]];
    case TrackModel::Segments:     [[fallthrough]];
    case TrackModel::Points:       [[fallthrough]];
    case TrackModel::Area:         [[fallthrough]];
    case TrackModel::MinTemp:      [[fallthrough]];
    case TrackModel::AvgTemp:      [[fallthrough]];
    case TrackModel::MaxTemp:      [[fallthrough]];
    case TrackModel::MinHR:        [[fallthrough]];
    case TrackModel::AvgHR:        [[fallthrough]];
    case TrackModel::MaxHR:        [[fallthrough]];
    case TrackModel::Laps:         [[fallthrough]];
    case TrackModel::MinLon:       [[fallthrough]];
    case TrackModel::MinLat:       [[fallthrough]];
    case TrackModel::MaxLon:       [[fallthrough]];
    case TrackModel::MaxLat:       [[fallthrough]];
    case TrackModel::MinHRPct:     [[fallthrough]];
    case TrackModel::AvgHRPct:     [[fallthrough]];
    case TrackModel::MaxHRPct:     return true;

    default:                       return false;
    }
}

QString TrackModel::makeTooltip(const QString& description, bool editable, bool reqPersonData)
{
    QString toolTip = ModelMetaData::makeTooltip(description, editable);

    if (reqPersonData) {
        toolTip += makeTooltipHeader(tr("Person Data")) +
                   tr("Requires person data to be set in preferences, and a "
                      "specific person selected (see the <img src=\"") +
                   Util::LocalThemeIcon("actions/22/system-switch-user.svg") +
                   tr("\"/> icon or <b>Edit/Select Person...</b> menu).");
    }

    return toolTip;
}

bool TrackModel::isTextField(ModelType mt) const
{
    return mt == TrackModel::Notes;
}

// tooltip for column header
QString TrackModel::mdTooltip(ModelType mt)
{
    const bool editable = mdIsEditable(mt);

    switch (mt) {
    case TrackModel::Name:
        return makeTooltip(tr("Descriptive track name."), editable);
    case TrackModel::Type:
        return makeTooltip(tr("Data type: Route (Rte), or Track (Trk)."), editable);
    case TrackModel::Tags:
        return makeTooltip(tr("Tags applied to this track.  The master list of available "
                              "tags may be edited from the Settings dialog."), editable);
    case TrackModel::Color:
        return makeTooltip(tr("Track display color."), editable);
    case TrackModel::Notes:
        return makeTooltip(tr("User notes."), editable);
    case TrackModel::Keywords:
        return makeTooltip(tr("Track keywords."), editable);
    case TrackModel::Source:
        return makeTooltip(tr("Import source (device or file)."), editable);
    case TrackModel::Flags:
        return makeTooltip(tr("Flags of regions containing the track."), editable);
    case TrackModel::Length:
        return makeTooltip(tr("Total track length."), editable);
    case TrackModel::BeginDate:
        return makeTooltip(tr("Earliest datestamp, including date + time of day."), editable);
    case TrackModel::EndDate:
        return makeTooltip(tr("Last datestamp, including date + time of day."), editable);
    case TrackModel::BeginTime:
        return makeTooltip(tr("Earliest timestamp, not including date."), editable);
    case TrackModel::EndTime:
        return makeTooltip(tr("Last timestamp, not including date."), editable);
    case TrackModel::StoppedTime:
        return makeTooltip(tr("Total stopped time."), editable);
    case TrackModel::MovingTime:
        return makeTooltip(tr("Total moving time."), editable);
    case TrackModel::TotalTime:
        return makeTooltip(tr("Total track duration (last timestamp - first timestamp)."), editable);
    case TrackModel::MinElevation:
        return makeTooltip(tr("Min (lowest) elevation contained (if any)."), editable);
    case TrackModel::AvgElevation:
        return makeTooltip(tr("Average elevation."), editable);
    case TrackModel::MaxElevation:
        return makeTooltip(tr("Max (highest) elevation contained (if any)."), editable);
    case TrackModel::MinSpeed:
        return makeTooltip(tr("Min (slowest) speed contained (if any)."), editable);
    case TrackModel::AvgOvrSpeed:
        return makeTooltip(tr("Average overall speed."), editable);
    case TrackModel::AvgMovSpeed:
        return makeTooltip(tr("Average moving speed."), editable);
    case TrackModel::MaxSpeed:
        return makeTooltip(tr("Max (fastest) speed contained (if any)."), editable);
    case TrackModel::MinGrade:
        return makeTooltip(tr("Min grade, in percent."), editable);
    case TrackModel::AvgGrade:
        return makeTooltip(tr("Average grade, in percent."), editable);
    case TrackModel::MaxGrade:
        return makeTooltip(tr("Max grade, in percent."), editable);
    case TrackModel::MinCad:
        return makeTooltip(tr("Min cadence."), editable);
    case TrackModel::AvgMovCad:
        return makeTooltip(tr("Average moving cadence."), editable);
    case TrackModel::MaxCad:
        return makeTooltip(tr("Max cadence."), editable);
    case TrackModel::MinPower:
        return makeTooltip(tr("Min power."), editable);
    case TrackModel::AvgMovPower:
        return makeTooltip(tr("Average moving power."), editable);
    case TrackModel::MaxPower:
        return makeTooltip(tr("Max power."), editable);
    case TrackModel::Energy:
        return makeTooltip(tr("Estimated total input energy."), editable);
    case TrackModel::Ascent:
        return makeTooltip(tr("Total ascent."), editable);
    case TrackModel::Descent:
        return makeTooltip(tr("Total descent."), editable);
    case TrackModel::BasePeak:
        return makeTooltip(tr("Peak elevation - base elevation."), editable);
    case TrackModel::Segments:
        return makeTooltip(tr("Number of separate segments in the track."), editable);
    case TrackModel::Points:
        return makeTooltip(tr("Number of sample points in the entire track."), editable);
    case TrackModel::Area:
        return makeTooltip(tr("Covered area."), editable);
    case TrackModel::MinTemp:
        return makeTooltip(tr("Min (lowest) temperature, if avaialble."), editable);
    case TrackModel::AvgTemp:
        return makeTooltip(tr("Average temperature, if available."), editable);
    case TrackModel::MaxTemp:
        return makeTooltip(tr("Max (highest) temperature, if avaialble."), editable);
    case TrackModel::MinHR:
        return makeTooltip(tr("Min (lowest) heart rate, if available."), editable);
    case TrackModel::AvgHR:
        return makeTooltip(tr("Average heart rate, if available."), editable);
    case TrackModel::MaxHR:
        return makeTooltip(tr("Max (highest) heart rate, if available."), editable);
    case TrackModel::MinHRPct:
        return makeTooltip(tr("Min (lowest) heart rate, as % of maximum."), editable, true);
    case TrackModel::AvgHRPct:
        return makeTooltip(tr("Average heart rate, as % of maximum."), editable, true);
    case TrackModel::MaxHRPct:
        return makeTooltip(tr("Max (highest) heart rate, as % of maximum."), editable, true);
    case TrackModel::Laps:
        return makeTooltip(tr("Number of laps, if available."), editable);
    case TrackModel::MinLon:
        return makeTooltip(tr("Min longitude."), editable);
    case TrackModel::MinLat:
        return makeTooltip(tr("Min latitude."), editable);
    case TrackModel::MaxLon:
        return makeTooltip(tr("Max longitude."), editable);
    case TrackModel::MaxLat:
        return makeTooltip(tr("Max latitude."), editable);

    default:
        assert(0 && "Unknown TrackModel value");
        return { };
    }
}

// tooltip for container column header
QString TrackModel::mdWhatsthis(ModelType mt)
{
    return mdTooltip(mt);  // just pass through the tooltip
}

Qt::Alignment TrackModel::mdAlignment(ModelType mt)
{
    switch (mt) {
    case TrackModel::Length:       [[fallthrough]];
    case TrackModel::BeginDate:    [[fallthrough]];
    case TrackModel::EndDate:      [[fallthrough]];
    case TrackModel::BeginTime:    [[fallthrough]];
    case TrackModel::EndTime:      [[fallthrough]];
    case TrackModel::StoppedTime:  [[fallthrough]];
    case TrackModel::MovingTime:   [[fallthrough]];
    case TrackModel::TotalTime:    [[fallthrough]];
    case TrackModel::MinElevation: [[fallthrough]];
    case TrackModel::AvgElevation: [[fallthrough]];
    case TrackModel::MaxElevation: [[fallthrough]];
    case TrackModel::MinSpeed:     [[fallthrough]];
    case TrackModel::AvgOvrSpeed:  [[fallthrough]];
    case TrackModel::AvgMovSpeed:  [[fallthrough]];
    case TrackModel::MaxSpeed:     [[fallthrough]];
    case TrackModel::MinGrade:     [[fallthrough]];
    case TrackModel::AvgGrade:     [[fallthrough]];
    case TrackModel::MaxGrade:     [[fallthrough]];
    case TrackModel::MinCad:       [[fallthrough]];
    case TrackModel::AvgMovCad:    [[fallthrough]];
    case TrackModel::MaxCad:       [[fallthrough]];
    case TrackModel::MinPower:     [[fallthrough]];
    case TrackModel::Energy:       [[fallthrough]];
    case TrackModel::AvgMovPower:  [[fallthrough]];
    case TrackModel::MaxPower:     [[fallthrough]];
    case TrackModel::Ascent:       [[fallthrough]];
    case TrackModel::Descent:      [[fallthrough]];
    case TrackModel::BasePeak:     [[fallthrough]];
    case TrackModel::Segments:     [[fallthrough]];
    case TrackModel::Points:       [[fallthrough]];
    case TrackModel::Area:         [[fallthrough]];
    case TrackModel::MinTemp:      [[fallthrough]];
    case TrackModel::AvgTemp:      [[fallthrough]];
    case TrackModel::MaxTemp:      [[fallthrough]];
    case TrackModel::MinHR:        [[fallthrough]];
    case TrackModel::AvgHR:        [[fallthrough]];
    case TrackModel::MaxHR:        [[fallthrough]];
    case TrackModel::Laps:         [[fallthrough]];
    case TrackModel::MinLon:       [[fallthrough]];
    case TrackModel::MinLat:       [[fallthrough]];
    case TrackModel::MaxLon:       [[fallthrough]];
    case TrackModel::MaxLat:       [[fallthrough]];
    case TrackModel::MinHRPct:     [[fallthrough]];
    case TrackModel::AvgHRPct:     [[fallthrough]];
    case TrackModel::MaxHRPct:     return Qt::AlignRight   | Qt::AlignVCenter;
    default:                       return Qt::AlignLeft    | Qt::AlignVCenter;
    }
}

const Units& TrackModel::mdUnits(ModelType mt)
{
    static const Units rawString(Format::String);
    static const Units rawFloat(Format::Float);

    switch (mt) {
    case TrackModel::Length:         return cfgData().unitsTrkLength;
    case TrackModel::StoppedTime:    [[fallthrough]];
    case TrackModel::MovingTime:     [[fallthrough]];
    case TrackModel::TotalTime:      return cfgData().unitsDuration;
    case TrackModel::BeginDate:      [[fallthrough]];
    case TrackModel::EndDate:        return cfgData().unitsTrkDate;
    case TrackModel::BeginTime:      [[fallthrough]];
    case TrackModel::EndTime:        return cfgData().unitsTrkTime;
    case TrackModel::MinElevation:   [[fallthrough]];
    case TrackModel::AvgElevation:   [[fallthrough]];
    case TrackModel::MaxElevation:   return cfgData().unitsElevation;
    case TrackModel::MinSpeed:       [[fallthrough]];
    case TrackModel::AvgOvrSpeed:    [[fallthrough]];
    case TrackModel::AvgMovSpeed:    [[fallthrough]];
    case TrackModel::MaxSpeed:       return cfgData().unitsSpeed;
    case TrackModel::MinGrade:       [[fallthrough]];
    case TrackModel::AvgGrade:       [[fallthrough]];
    case TrackModel::MaxGrade:       return cfgData().unitsSlope;
    case TrackModel::MinCad:         [[fallthrough]];
    case TrackModel::AvgMovCad:      [[fallthrough]];
    case TrackModel::MaxCad:         return cfgData().unitsCad;
    case TrackModel::MinPower:       [[fallthrough]];
    case TrackModel::AvgMovPower:    [[fallthrough]];
    case TrackModel::MaxPower:       return cfgData().unitsPower;
    case TrackModel::Energy:         return cfgData().unitsEnergy;
    case TrackModel::Ascent:         [[fallthrough]];
    case TrackModel::Descent:        [[fallthrough]];
    case TrackModel::BasePeak:       return cfgData().unitsClimb;
    case TrackModel::Area:           return cfgData().unitsArea;
    case TrackModel::MinTemp:        [[fallthrough]];
    case TrackModel::AvgTemp:        [[fallthrough]];
    case TrackModel::MaxTemp:        return cfgData().unitsTemp;
    case TrackModel::MinHR:          [[fallthrough]];
    case TrackModel::AvgHR:          [[fallthrough]];
    case TrackModel::MaxHR:          return cfgData().unitsHr;
    case TrackModel::MinHRPct:       [[fallthrough]];
    case TrackModel::AvgHRPct:       [[fallthrough]];
    case TrackModel::MaxHRPct:       return cfgData().unitsPct;
    case TrackModel::MinLat:         [[fallthrough]];
    case TrackModel::MaxLat:         return cfgData().unitsLat;
    case TrackModel::MinLon:         [[fallthrough]];
    case TrackModel::MaxLon:         return cfgData().unitsLon;
    case TrackModel::BeginToEndEle:  return rawFloat;
    default:                         return rawString;
    }
}

TrackModel::name_t TrackModel::getItemNameStatic()
{
   return { tr("Track"), tr("Tracks") };
}

QVariant TrackModel::mdAccumInit(ModelType mt)
{
    switch (mt) {
    case TrackModel::Name:         [[fallthrough]]; // string
    case TrackModel::Type:         [[fallthrough]];
    case TrackModel::Notes:        [[fallthrough]];
    case TrackModel::Source:       return QString();
    case TrackModel::Color:        return QColor(0,0,0,0);
    case TrackModel::Tags:         [[fallthrough]]; // string list
    case TrackModel::Keywords:     [[fallthrough]];
    case TrackModel::Flags:        return QStringList();
    case TrackModel::BeginDate:    [[fallthrough]]; // date/time
    case TrackModel::EndDate:      return QDateTime();
    case TrackModel::BeginTime:    [[fallthrough]];
    case TrackModel::EndTime:      return QTime();  // time
    case TrackModel::Segments:     [[fallthrough]]; // integer 0
    case TrackModel::Points:       return 0;
    default:                       return 0.0;      // double 0
    }
}

QVariant TrackModel::mdAccum(ModelType mt, const QVariant& lhs, const QVariant& rhs)
{
    switch (mt) {
    case TrackModel::Name:         [[fallthrough]];
    case TrackModel::Source:       return lhs.toString() + "+" + rhs.toString();
    case TrackModel::Type:         return lhs;
    case TrackModel::Color:        return lhs; // TODO: ...
    case TrackModel::Notes:        return lhs; // TODO: maybe append...
    case TrackModel::Tags:         [[fallthrough]]; // accumulate via string list concatenation
    case TrackModel::Keywords:     [[fallthrough]];
    case TrackModel::Flags:        return lhs.toStringList() + rhs.toStringList();
    case TrackModel::BeginDate:    [[fallthrough]]; // accumulate via min
    case TrackModel::BeginTime:    [[fallthrough]];
    case TrackModel::MinElevation: [[fallthrough]];
    case TrackModel::MinSpeed:     [[fallthrough]];
    case TrackModel::MinGrade:     [[fallthrough]];
    case TrackModel::MinCad:       [[fallthrough]];
    case TrackModel::MinPower:     [[fallthrough]];
    case TrackModel::MinTemp:      [[fallthrough]];
    case TrackModel::MinHR:        [[fallthrough]];
    case TrackModel::MinHRPct:     [[fallthrough]];
    case TrackModel::MinLon:       [[fallthrough]];
    case TrackModel::MinLat:       return QtCompat::min(lhs, rhs);
    case TrackModel::EndDate:      [[fallthrough]]; // accumulate via max
    case TrackModel::EndTime:      [[fallthrough]];
    case TrackModel::MaxElevation: [[fallthrough]];
    case TrackModel::MaxSpeed:     [[fallthrough]];
    case TrackModel::MaxGrade:     [[fallthrough]];
    case TrackModel::MaxCad:       [[fallthrough]];
    case TrackModel::MaxPower:     [[fallthrough]];
    case TrackModel::MaxTemp:      [[fallthrough]];
    case TrackModel::MaxHR:        [[fallthrough]];
    case TrackModel::MaxHRPct:     [[fallthrough]];
    case TrackModel::MaxLon:       [[fallthrough]];
    case TrackModel::MaxLat:       return QtCompat::max(lhs, rhs);
    default:                       return lhs.toDouble() + rhs.toDouble(); // plain old numeric add
    }
}

QVariant TrackModel::mdAccumFinal(ModelType mt, QVariant final, int count)
{
    static const constexpr qreal nsToH  = 1.0/3.6e+12;     // nsToH
    static const constexpr qreal nsToMs = 1.0 / 1000000.0; // for QDateTime::fromMSecSinceEpoch

    switch (mt) {
    case TrackModel::StoppedTime:  [[fallthrough]];
    case TrackModel::MovingTime:   [[fallthrough]];
    case TrackModel::TotalTime:    return final.toDouble() * nsToH;
    case TrackModel::BeginDate:    [[fallthrough]];
    case TrackModel::EndDate:      [[fallthrough]];
    case TrackModel::BeginTime:    [[fallthrough]];
    case TrackModel::EndTime:      return final.toDouble() * nsToMs;
    case TrackModel::AvgGrade:     [[fallthrough]]; // finalize by averaging
    case TrackModel::AvgMovSpeed:  [[fallthrough]];
    case TrackModel::AvgMovCad:    [[fallthrough]];
    case TrackModel::AvgMovPower:  [[fallthrough]];
    case TrackModel::AvgOvrSpeed:  [[fallthrough]];
    case TrackModel::AvgElevation: [[fallthrough]];
    case TrackModel::AvgTemp:      [[fallthrough]];
    case TrackModel::AvgHR:        [[fallthrough]];
    case TrackModel::AvgHRPct:     return final.toDouble() / std::max(count, 1);
    default:                       return final;    // no-op finalization
    }
}

bool TrackModel::mdAccumSummed(ModelType mt)
{
    switch (mt) {
    case TrackModel::BeginDate:    [[fallthrough]];
    case TrackModel::BeginTime:    [[fallthrough]];
    case TrackModel::MinElevation: [[fallthrough]];
    case TrackModel::MinSpeed:     [[fallthrough]];
    case TrackModel::MinGrade:     [[fallthrough]];
    case TrackModel::MinCad:       [[fallthrough]];
    case TrackModel::MinPower:     [[fallthrough]];
    case TrackModel::MinTemp:      [[fallthrough]];
    case TrackModel::MinHR:        [[fallthrough]];
    case TrackModel::MinHRPct:     [[fallthrough]];
    case TrackModel::MinLon:       [[fallthrough]];
    case TrackModel::MinLat:       [[fallthrough]];
    case TrackModel::EndDate:      [[fallthrough]];
    case TrackModel::EndTime:      [[fallthrough]];
    case TrackModel::MaxElevation: [[fallthrough]];
    case TrackModel::MaxSpeed:     [[fallthrough]];
    case TrackModel::MaxGrade:     [[fallthrough]];
    case TrackModel::MaxCad:       [[fallthrough]];
    case TrackModel::MaxPower:     [[fallthrough]];
    case TrackModel::MaxTemp:      [[fallthrough]];
    case TrackModel::MaxHR:        [[fallthrough]];
    case TrackModel::MaxHRPct:     [[fallthrough]];
    case TrackModel::MaxLon:       [[fallthrough]];
    case TrackModel::MaxLat:       [[fallthrough]];
    case TrackModel::AvgMovSpeed:  [[fallthrough]];
    case TrackModel::AvgMovCad:    [[fallthrough]];
    case TrackModel::AvgMovPower:  [[fallthrough]];
    case TrackModel::AvgOvrSpeed:  [[fallthrough]];
    case TrackModel::AvgTemp:      [[fallthrough]];
    case TrackModel::AvgHR:        [[fallthrough]];
    case TrackModel::AvgGrade:     [[fallthrough]];
    case TrackModel::AvgHRPct:     return false;
    default:                       return true;
    }
}

PointModel* TrackModel::geoPoints(const QModelIndex& idx)
{
    TrackItem* lm = getItem(idx);
    return (lm != nullptr) ? &lm->m_geoPoints : nullptr;
}

const PointModel* TrackModel::geoPoints(const QModelIndex& idx) const
{
    const TrackItem* lm = getItem(idx);
    return (lm != nullptr) ? &lm->m_geoPoints : nullptr;
}

// Only for use by save classes and the TrackMap.  We only provide template specializations
// for classes that should have access.
class TrackMap;
template<>
const TrackSegLines& TrackModel::trackLines(const TrackMap&, const QModelIndex& idx) const
{
    return getItem(idx)->m_trackLines;
}

void TrackModel::setAllVisible(bool v)
{
    Util::Recurse(*this, [this, v](const QModelIndex& idx) {
        getItem(idx)->setVisible(v);
        return true;
    });
}

void TrackModel::setVisible(const QModelIndex& idx, bool v)
{
    getItem(idx)->setVisible(v);
}

bool TrackModel::isVisible(const QModelIndex& idx) const
{
    return getItem(idx)->isVisible();
}

class AreaDialog;
template<> void TrackModel::setAreaSelected(const AreaDialog&, bool v)
{
    Util::Recurse(*this, [this, v](const QModelIndex& idx) {
        getItem(idx)->setAreaSelect(v);
        return true;
    });
}

template<> void TrackModel::setAreaSelected(const AreaDialog&, const QModelIndex& idx, bool v)
{
    getItem(idx)->setAreaSelect(v);
}

template<> bool TrackModel::isAreaSelected(const AreaDialog&, const QModelIndex& idx) const
{
    return getItem(idx)->isAreaSelected();
}

template <> void TrackModel::selectPointsWithin(const AreaDialog&, const Marble::GeoDataLatLonBox& region)
{
    Util::Recurse(*this, [this, &region](const QModelIndex& idx) {
        getItem(idx)->selectPointsWithin(region);
        return true;
    });
}

template<> void TrackModel::updateQueueAdd(TrackItem* item)
{
    QMutexLocker lock(&m_mutex);
    m_updateQueue.enqueue(item);
}

template<> bool TrackModel::updateQueueProcess(const TrackUpdateThread&)
{
    TrackItem* nextItem = nullptr;
    { // mutex scope
        QMutexLocker lock(&m_mutex);

        if (m_updateQueue.isEmpty())  // nothing to do
            return false;

        nextItem = m_updateQueue.dequeue(); // dequeue the next work item
    }
        
    nextItem->update(); // do the work
    return true;
}

template<> bool TrackModel::loadPending(const TrackUpdateThread&) const
{
    return m_loadPending;
}

bool TrackModel::isDuplicate(const QModelIndex& idx, const PointModel& points) const
{
    return getItem(idx)->pointEqual(points);
}

QStringList TrackModel::recentTags(int maxTags, int maxTracks) const
{
    QModelIndexList indexes;
    const int rows = rowCount();

    // Otherwise, use most recent tracks from the trackModel
    for (int row = rows - 1; row >= 0 && row > (rows - maxTracks); --row)
        indexes.push_back(index(row, TrackModel::Tags));

    return recentTags(indexes, maxTags);
}

QStringList TrackModel::recentTags(const QModelIndexList& indexes, int maxTags) const
{
    QMap<QString, int> count;

    // Count occurances of each tag we find in recent tracks
    for (const QModelIndex& idx : indexes)
        for (const auto& tag : data(idx, Util::RawDataRole).toStringList())
            ++count[tag];

    // Linear list for sorting by occurrence
    using it_t = decltype(count)::const_iterator;
    QVector<it_t> sortedCount;
    sortedCount.reserve(count.size());

    for (auto it = count.constBegin(); it != count.constEnd(); ++it)
        sortedCount.append(it);

    std::sort(sortedCount.begin(), sortedCount.end(), [](it_t x, it_t y) {
        return x.value() != y.value() ?  // sort first by count
               x.value() > y.value() :
               x.key() < y.key();        // by name if count is equal
    });

    // Limit to max number we want to have
    while (sortedCount.size() > maxTags)
        sortedCount.removeLast();

    QStringList tags;
    tags.reserve(sortedCount.size());

    for (auto it : sortedCount)
        tags.append(it.key());

    return tags;
}

bool TrackModel::isDuplicate(const QModelIndex& lhs, const QModelIndex& rhs) const
{
    // Handle one or the other being in a foreign model
    const auto* lhsModel = qobject_cast<const TrackModel*>(lhs.model());
    const auto* rhsModel = qobject_cast<const TrackModel*>(rhs.model());

    if (lhsModel == nullptr || rhsModel == nullptr)
        return false;

    return lhsModel->getItem(lhs)->pointEqual(*rhsModel->getItem(rhs));
}

void TrackModel::emitDataChange(int row, int col)
{
    emit dataChanged(index(row, col), index(row, col));
}

void TrackModel::postUndoHook(const QModelIndex& parent, int start, int end)
{
    for (int row = start; row <= end; ++row)
        getItem(child(row, parent))->update();
}

QVariant TrackModel::data(const QModelIndex& idx, int role) const
{
    if (!idx.isValid())
        return { };

    if (const QVariant val = getItem(idx)->data(idx.column(), role); val.isValid()) {
        // Expand variables in notes for tooltips, up to given max size
        if (isTextField(idx.column()) && role == Qt::ToolTipRole) {
            if (Util::PlainTextLen(val) < cfgData().tooltipFullLimit)
                return m_varExpander.expandHtml(val.toString(), idx);
            else
                return { };
        }

        // Otherwise, colorize if necessary.
        return cfgData().trackColorizer.maybeUse(val, idx, role);
    }

    if (role == Qt::ToolTipRole && !isTextField(idx.column()))
        return tooltip(idx);

    return cfgData().trackColorizer.colorize(idx, role);
}

bool TrackModel::setData(const QModelIndex& idx, const QVariant& value, int role)
{
    // Emit explicitly, because we don't pass this though to the base class.
    emitAboutToChange(idx, value, role);

    bool changed;
    const bool rc = getItem(idx)->setData(idx.column(), value, role, changed, m_personIdx);

    if (rc && changed) {
        // Emit explicitly, because we don't pass this though to the base class.
        emit dataChanged(idx, idx);

        // Changing tags also changes power data
        if (idx.column() == TrackModel::Tags)
            for (const auto c : { MinPower, AvgMovPower, MaxPower, Energy })
                emitDataChange(idx.row(), c);
    }

    return rc;
}

bool TrackModel::is(const QModelIndex& idx, TrackType type) const
{
    if (auto* pm = getItem(idx); pm != nullptr)
        return pm->is(type);

    return false;
}

void TrackModel::setPerson(const QModelIndex& personIdx)
{
    m_personIdx = personIdx;

    // Update all tracks to use power data for the new person.
    Util::Recurse(*this, [this](const QModelIndex& idx) {
        getItem(idx)->setPersonIdx(m_personIdx);
        return true;
    });
}

void TrackModel::setPerson(const QString& person)
{
    if (person.isEmpty())
        return;

    setPerson(cfgData().people.keyIdx(person));
}

QDataStream& TrackModel::load(QDataStream& stream, const QModelIndex& parent, bool append)
{
    m_updateQueue.reserve(256);

    // Start background threads to recalculate thread data on load, for super-speed!
    MergeableModel::load(stream, parent, append,
        [this]() { beginThreads(); },
        [this]() { endThreads(); }
    );

    return stream;
}

bool TrackModel::loadForUndo(QIODevice& io, const QModelIndex& parent, int first)
{
    m_updateQueue.reserve(256);
    const LoaderThreads loaderThreads(this);
    return MergeableModel::loadForUndo(io, parent, first);
}

quint32 TrackModel::streamMagic() const
{
    return App::NativeMagic + quint32(App::Model::Track);
}

quint32 TrackModel::streamVersionMin() const
{
    return App::NativeVersionMin;
}

quint32 TrackModel::streamVersionCurrent() const
{
    return App::NativeVersionCurrent;
}

QDataStream& operator<<(QDataStream& stream, const TrackModel& model)
{
    return model.save(stream);
}

QDataStream& operator>>(QDataStream& stream, TrackModel& model)
{
    return model.load(stream);
}

QDataStream& operator<<(QDataStream& out, const TrackType& tt)
{
    return out << std::underlying_type_t<Marble::MapQuality>(tt);
}

QDataStream& operator>>(QDataStream& in, TrackType& tt)
{
    return in >> reinterpret_cast<std::underlying_type_t<Marble::MapQuality>&>(tt);
}

QMultiHash<uint, QModelIndex> TrackModel::hashes() const
{
    QMultiHash<uint, QModelIndex> hashes;

    Util::Recurse(*this, [this, &hashes](const QModelIndex& idx) {
        hashes.insert(qHash(*getItem(idx)), idx);
        return true;
    });

    return hashes;
}

uint TrackModel::hash(const QModelIndex& idx) const
{
    return qHash(*getItem(idx));
}

// Create track to merge into
QModelIndex TrackModel::beginMerge(const QModelIndexList& selections, const QVariant& userData)
{
    // Create data to populate the new track
    TrackType   newType = TrackType(data(rowSibling(TrackModel::Type, selections.front())).toInt());
    QString     newName = userData.toString();
    QStringList newTags;
    QColor      newColor;
    QString     newNotes;
    QString     newKeywords;
    QString     newSource;
    PointModel  empty;

    for (const auto& idx : selections) {
        // tags: don't insert duplicates
        const QStringList mergeTags = data(rowSibling(TrackModel::Tags, idx), Util::RawDataRole).toStringList();
        for (const auto& tag : mergeTags)
            if (!newTags.contains(tag))
                newTags.append(tag);

        if (!newColor.isValid())
            newColor = data(rowSibling(TrackModel::Color, idx), Util::RawDataRole).value<QColor>();

        // TODO: we could parse the XML and concatenate the track notes
        if (newNotes.isEmpty())
            newNotes = data(rowSibling(TrackModel::Notes, idx), Util::RawDataRole).toString();

        // Keywords
        if (!newKeywords.isEmpty())
            newKeywords.append(" ");
        newKeywords.append(data(rowSibling(TrackModel::Keywords, idx), Util::RawDataRole).toString());

        // Source
        if (newSource.isEmpty())
            newSource = data(rowSibling(TrackModel::Source, idx), Util::RawDataRole).toString();
    }

    return appendRow(newName,
                     newType,
                     newTags,
                     newColor,
                     newNotes,
                     newKeywords,
                     newSource,
                     empty);
}

std::tuple<QModelIndex, bool> TrackModel::merge(const QModelIndexList& selection, const QVariant& userData)
{
    // Step 0: get model to merge into.  Use persistent index so it lives
    // across the row deletion step below.
    const QPersistentModelIndex to = beginMerge(selection, userData);

    if (!to.isValid())
        return { to, false };

    // Step 1: Merge everything
    for (const auto& idx : selection)
        if (idx != to)
            merge(to, idx, userData);

    // Step 2: delete now-unused items, but not the one we merged into.
    removeRows([&to, &selection](const QModelIndex& idx) {
        return selection.contains(idx) && idx != to;
    });

    endMerge(to, userData);

    return { to, true };
}

void TrackModel::merge(const QModelIndex& into, const QModelIndex& from, const QVariant&)
{
    PointModel* intoPm = geoPoints(into);
    const PointModel* fromPm = geoPoints(from);

    if (intoPm == nullptr || fromPm == nullptr)
        return;

    intoPm->append(*fromPm);
}

void TrackModel::endMerge(const QModelIndex& to, const QVariant&)
{
    if (PointModel* pm = geoPoints(to); pm != nullptr) {
        pm->sortTrkSegs();         // sort post-merge track
        getItem(to)->update(true); // update calculated data
    }
}

SimplifiableModel::Count TrackModel::simplifyPoints(const QModelIndexList& selection,
                                                    const std::function<Count(SimplifiableModel&)>& fn)
{
    SimplifiableModel::Count count;

    for (const auto& idx : selection)
        if (SimplifiableModel* pm = geoPoints(idx); pm != nullptr)
            count += fn(*pm);

    return count;
}

SimplifiableModel::Count TrackModel::simplifyTime(const QModelIndexList& selection, const Params& params)
{
    return simplifyPoints(selection, [&params](SimplifiableModel& pm) {
        return pm.simplifyTime(QModelIndexList(), params);
    });
}

SimplifiableModel::Count TrackModel::simplifyDist(const QModelIndexList& selection, const Params& params)
{
    return simplifyPoints(selection, [&params](SimplifiableModel& pm) {
        return pm.simplifyDist(QModelIndexList(), params);
    });
}

SimplifiableModel::Count TrackModel::simplifyAdaptive(const QModelIndexList& selection, const Params& params)
{
    return simplifyPoints(selection, [&params](SimplifiableModel& pm) {
        return pm.simplifyAdaptive(QModelIndexList(), params);
    });
}

void TrackModel::reverse(const QModelIndexList& selection)
{
    for (const auto& idx : selection)
        if (auto* pm = getItem(idx); pm != nullptr)
            pm->reverse();
}

void TrackModel::reverse()
{
    Util::Recurse(*this, [this](const QModelIndex& idx) {
        if (auto* pm = getItem(idx); pm != nullptr)
            pm->reverse();
        return true;
    });
}

void TrackModel::unsetSpeed(const QModelIndexList& selection)
{
    for (const auto& idx : selection)
        if (auto* pm = getItem(idx); pm != nullptr)
            pm->unsetSpeed();
}

void TrackModel::unsetSpeed()
{
    Util::Recurse(*this, [this](const QModelIndex& idx) {
        if (auto* pm = getItem(idx); pm != nullptr)
            pm->unsetSpeed();
        return true;
    });
}
