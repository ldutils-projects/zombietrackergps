/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <QDataStream>
#include <src/util/roles.h>
#include <src/util/math.h>
#include <src/util/units.h>
#include <src/util/versionedstream.h>
#include <src/core/modelmetadata.h>
#include <src/core/cfgdata.h>
#include <src/core/app.h>

#include "pointitem.h"
#include "pointitem.inl.h"
#include "pointmodel.h"
#include "pointaccum.h"

const QVariant PointItem::m_empty;
decltype(PointItem::m_sinAtanTable) PointItem::m_sinAtanTable; // fixed point sin(atan(x)) tables
bool PointItem::m_sinAtanTableInit = false;

void PointItem::clear()
{
    *this = PointItem();
}

bool PointItem::hasExtData() const
{
    return hasAtemp()         ||
           hasWtemp()         ||
           hasDepth()         ||
           hasSpeed(Measured) ||
           hasHr()            ||
           hasCad()           ||
           hasPower(Measured) ||
           hasCourse()        ||
           hasBearing();
}

PointItem PointItem::interpolate(const PointItem& p0, const PointItem& p1, const QDateTime& ts)
{
    // % of distance between p0 and p1.  If >1, it's extrapolation.
    const qreal pct = qreal(p0.time().msecsTo(ts)) / qreal(p0.time().msecsTo(p1.time()));

    // Interpolation: p0 * (1.0 - pct) + p1 * pct
    return PointAccum(p0)
            .add(p0, 1.0 - pct)
            .add(p1, pct)
            .average(ts);
}

PointItem PointItem::interpolate(const PointItem& next, const QDateTime& ts) const
{
    return interpolate(*this, next, ts);
}

template <>
QVariant PointItem::data<QVariant>(ModelType mt, int row, int role, bool flt,
                                   const PointItem* startPt,
                                   const PointItem* nextPt,
                                   const PowerData& pd) const
{
    static const constexpr qint64 mStonS = 1000000;  // mS to nS

    if (!hasData(mt, nextPt))
        return m_empty;

    QVariant value;

    // The uint8_t (for size) x/min rates must be converted to the Unit's base measurement of x/sec.
    static const double rateConv = (1.0 / 60.0);

    switch (mt) {
    case PointModel::Index:     value = row;                         break;
    case PointModel::Time:      value = time();                      break;
    case PointModel::Elapsed:
        value = (startPt == nullptr) ? m_empty : Elaps_t::base_type(mStonS * elapsed(startPt->time()));
        break;
    case PointModel::Lon:       value = Lon_t::base_type(lon());     break;
    case PointModel::Lat:       value = Lat_t::base_type(lat());     break;
    case PointModel::Ele:       value = Ele_t::base_type(ele(flt));  break;
    case PointModel::Length:
        value = (nextPt == nullptr) ? m_empty : qreal(nextPt->distance() - distance());
        break;
    case PointModel::Distance:  value = qreal(distance());           break;
    case PointModel::Vert:
        value = (nextPt != nullptr) ? Ele_t::base_type(vert(*nextPt)) : m_empty;
        break;
    case PointModel::Grade:     value = Grade_t::base_type(grade()); break;
    case PointModel::Duration:
        value = (nextPt == nullptr) ? m_empty : Dur_t::base_type(duration(*nextPt) * Dur_t(mStonS));
        break;
    case PointModel::Temp:      value = Temp_t::base_type(atemp());  break;
    case PointModel::Depth:     value = depth();                     break;
    case PointModel::Speed:     value = Speed_t::base_type(speed(nextPt)); break;
    case PointModel::Hr:        value = double(hr()) * rateConv;     break;
    case PointModel::Cad:       value = double(cad()) * rateConv;    break;
    case PointModel::Power:     value = Power_t::base_type(power(pd, *nextPt)); break;
    case PointModel::Course:    value = course();                    break;
    case PointModel::Bearing:   value = bearing();                   break;
    case PointModel::Name:      value = name();                      break;
    case PointModel::Comment:   value = comment();                   break;
    case PointModel::Desc:      value = desc();                      break;
    case PointModel::Symbol:    value = symbol();                    break;
    case PointModel::Type:      value = type();                      break;
    default: assert(0); break;
    }

    // For these fields, use an icon to replace the text, and use the whole field as a tooltip.
    switch (mt) {
    case PointModel::Comment: [[fallthrough]];
    case PointModel::Desc:
        if (!value.isNull()) {
            if (role == Qt::DisplayRole || role == Util::CopyRole)
                return { }; // we render an icon
            if (role == Qt::DecorationRole)
                return QIcon(cfgData().trackNoteIcon); // display icon when there is text
            if (role == Qt::SizeHintRole)
                return cfgData().iconSizeTrack; // icon size
            if (role == Qt::ToolTipRole)
                return value;
        }
        break;
    }

    switch (role) {
    case Qt::EditRole:       [[fallthrough]];
    case Qt::DisplayRole:    [[fallthrough]];
    case Util::CopyRole:     [[fallthrough]];
    case Util::RawDataRole:  return value;
    default:                 return { };
    }
}

template <>
qreal PointItem::data<qreal>(ModelType mt, int row, int role, bool flt,
                             const PointItem* startPt,
                             const PointItem* nextPt,
                             const PowerData& pd) const
{
    return data<QVariant>(mt, row, role, flt, startPt, nextPt, pd).toDouble();
}

void PointItem::clearData(ModelType mt)
{
    const auto auxClear = [this](ModelType mt) {
        if (hasAuxData()) {
            m_auxData->clearData(mt);
            maybeRemoveAuxData();
        }
    };

    switch (mt) {
    case PointModel::Temp:    m_atemp = badATempI;  break;
    case PointModel::Hr:      setHr(badHr);         break;
    case PointModel::Cad:     setCad(badCad);       break;
    case PointModel::Depth:   [[fallthrough]];
    case PointModel::Course:  [[fallthrough]];
    case PointModel::Bearing: [[fallthrough]];
    case PointModel::Name:    [[fallthrough]];
    case PointModel::Comment: [[fallthrough]];
    case PointModel::Desc:    [[fallthrough]];
    case PointModel::Symbol:  [[fallthrough]];
    case PointModel::Type:    auxClear(mt);         break;
    }
}

bool PointItem::setData(ModelType mt, QVariant value, int role, bool& changed)
{
    changed = true; // TODO: we could be smarter about this.

    if (role == Qt::EditRole) {
        value = PointModel::mdUnits(mt).from(value);

        // Unset certain columns if set to zero.  (OK to check ints as doubles, for convenience)
        switch (mt) {
        case PointModel::Temp:  [[fallthrough]];
        case PointModel::Depth: [[fallthrough]];
        case PointModel::Hr:    [[fallthrough]];
        case PointModel::Cad:
            if (value.toDouble() == 0.0) {
                clearData(mt);
                return true;
            }
            break;
        default:
            break;
        }
    }

    // The uint8_t (for size) x/min rates must be converted from the Unit's base measurement of x/sec.
    const double rateConv = 60.0;

    switch (mt) {
    case PointModel::Time:     setTime(value.toDateTime());      break;
    case PointModel::Lon:      setLon(value.toDouble());         break;
    case PointModel::Lat:      setLat(value.toDouble());         break;
    case PointModel::Ele:      setEle(value.toDouble());         break;
    case PointModel::Temp:     setATemp(value.toFloat());        break;
    case PointModel::Depth:    setDepth(value.toFloat());        break;
    case PointModel::Speed:    setSpeed(Speed_t(value.toFloat())); break;
    case PointModel::Hr:       setHr(Hr_t(value.toDouble() * rateConv)); break;
    case PointModel::Cad:      setCad(Cad_t(value.toDouble() * rateConv)); break;
    case PointModel::Power:    setPower(value.toFloat());        break;
    case PointModel::Course:   setCourse(value.toFloat());       break;
    case PointModel::Bearing:  setBearing(value.toFloat());      break;
    case PointModel::Name:     setName(value.toString());        break;
    case PointModel::Comment:  setComment(value.toString());     break;
    case PointModel::Desc:     setDesc(value.toString());        break;
    case PointModel::Symbol:   setSymbol(value.toString());      break;
    case PointModel::Type:     setType(value.toString());        break;

    default:
        assert(0);
        return false;
    }

    return true;
}

bool PointItem::hasData(ModelType mt, const PointItem* nextPt) const
{
    switch (mt) {
    case PointModel::Index:     return true;
    case PointModel::Time:      return hasTime();
    case PointModel::Elapsed:   return hasElapsed();
    case PointModel::Lon:       return hasLoc();
    case PointModel::Lat:       return hasLoc();
    case PointModel::Ele:       return hasEle();
    case PointModel::Length:    return hasLength(nextPt);
    case PointModel::Distance:  return hasDistance();
    case PointModel::Vert:      return hasVert(nextPt);
    case PointModel::Grade:     return hasGrade();
    case PointModel::Duration:  return hasDuration(nextPt);
    case PointModel::Temp:      return hasAtemp();
    case PointModel::Depth:     return hasDepth();
    case PointModel::Speed:     return hasSpeed(Either, nextPt);
    case PointModel::Hr:        return hasHr();
    case PointModel::Cad:       return hasCad();
    case PointModel::Power:     return hasPower(Either, nextPt);
    case PointModel::Course:    return hasCourse();
    case PointModel::Bearing:   return hasBearing();
    case PointModel::Name:      return hasName();
    case PointModel::Comment:   return hasComment();
    case PointModel::Desc:      return hasDesc();
    case PointModel::Symbol:    return hasSymbol();
    case PointModel::Type:      return hasType();
    }

    assert(0);
    return false;
}

bool PointItem::operator==(const PointItem& rhs) const
{
    // Do not compare the filtered data!  We can change that per config settings.
    // No need to compare derived (calculated) values such as distance.
    return
        hasTime()          == rhs.hasTime()               &&
        hasLoc()           == rhs.hasLoc()                &&
        hasEle()           == rhs.hasEle()                &&
        hasSpeed(Measured) == rhs.hasSpeed(Measured)      &&
        hasPower(Measured) == rhs.hasPower(Measured)      &&
        hasAuxData()       == rhs.hasAuxData()            &&

        (!hasTime()      || time() == rhs.time())         &&
        (!hasAuxData()   || *m_auxData == *rhs.m_auxData) &&
        (!hasLoc()       || Math::almost_equal(m_lon , rhs.m_lon)) &&
        (!hasLoc()       || Math::almost_equal(m_lat , rhs.m_lat)) &&
        (!hasEle()       || Math::almost_equal(m_ele , rhs.m_ele)) &&

        m_atemp            == rhs.m_atemp  &&
        (!hasSpeed(Measured) || Math::almost_equal(m_speed, rhs.m_speed)) &&
        m_hr               == rhs.m_hr     &&  // int types can use exact comparison
        m_cad              == rhs.m_cad    &&  // ...
        (!hasPower(Measured) || Math::almost_equal(m_power, rhs.m_power));
}

PointItem& PointItem::maybeRemoveAuxData()
{
    if (!hasAuxData() || !m_auxData->isDefault())
        return *this;

    return clearAuxData(); // remove default AuxData
}

PointItem& PointItem::operator=(const PointItem& rhs)
{
    if (this == &rhs)
        return *this;
    
    m_time    = rhs.m_time;
    m_lon     = rhs.m_lon;
    m_lat     = rhs.m_lat;
    m_ele     = rhs.m_ele;
    m_fltEle  = rhs.m_fltEle;
    m_dist    = rhs.m_dist;
    m_speed   = rhs.m_speed;
    m_power   = rhs.m_power;
    m_grade   = rhs.m_grade;
    m_atemp   = rhs.m_atemp;
    m_flags   = rhs.m_flags;
    m_hr      = rhs.m_hr;
    m_cad     = rhs.m_cad;

    if (rhs.hasAuxData())
        getAuxData() = *rhs.m_auxData;
    else
        clearAuxData();

    return *this;
}

PointItem::PointItem(const PointItem& rhs)
{
    *this = rhs;
}

PointItem::AuxData& PointItem::getAuxData()
{
    if (!m_auxData)
        m_auxData.reset(new AuxData);

    return *m_auxData;
}

// Create table of sin(atan(x)) values
void PointItem::calcSinAtans()
{
    static QMutex mutex;

    const QMutexLocker lock(&mutex);

    if (m_sinAtanTableInit)
        return;

    for (int i = 0; i < int(m_sinAtanTable.size()); ++i) {
        const float grade = (1.0 / (badGradeI-1)) * (i + int(std::numeric_limits<decltype(m_grade)>::min()));
        m_sinAtanTable[i] = std::sin(std::atan(grade)) * (badGradeI-1);
    }

    m_sinAtanTableInit = true;
}

uint qHash(const PointItem& pt, uint seed)
{
    // Distance, etc are computed from other values: no need to hash
    if (pt.hasTime())                     seed = qHash(std::make_pair(seed, pt.m_time));
    if (pt.hasLoc()) {
        seed = qHash(std::make_pair(seed, pt.m_lon));
        seed = qHash(std::make_pair(seed, pt.m_lat));
    }

    if (pt.hasEle())                      seed = qHash(std::make_pair(seed, pt.m_ele));
    if (pt.hasAtemp())                    seed = qHash(std::make_pair(seed, pt.m_atemp)); // hash raw data
    if (pt.hasSpeed(PointItem::Measured)) seed = qHash(std::make_pair(seed, pt.m_ele));
    if (pt.hasHr())                       seed = qHash(std::make_pair(seed, pt.m_hr));
    if (pt.hasCad())                      seed = qHash(std::make_pair(seed, pt.m_cad));
    if (pt.hasPower(PointItem::Measured)) seed = qHash(std::make_pair(seed, pt.m_power));
    if (pt.hasAuxData())                  seed = qHash(std::make_pair(seed, *pt.m_auxData));

    return seed;
}

bool PointItem::AuxData::operator==(const AuxData& rhs) const
{
    return m_name    == rhs.m_name &&
           m_comment == rhs.m_comment &&
           m_desc    == rhs.m_desc &&
           m_symbol  == rhs.m_symbol &&
           m_type    == rhs.m_type &&
           m_wtemp   == rhs.m_wtemp &&
           m_depth   == rhs.m_depth &&
           m_course  == rhs.m_course &&
           m_bearing == rhs.m_bearing;
}

void PointItem::AuxData::clearData(ModelType mt)
{
    switch (mt) {
    case PointModel::Depth:   m_depth = badDepthI;     break;
    // m_wtemp not currently handled in PointModel
    case PointModel::Course:  m_course  = badCourseI;  break;
    case PointModel::Bearing: m_bearing = badBearingI; break;
    case PointModel::Name:    m_name.clear();          break;
    case PointModel::Comment: m_comment.clear();       break;
    case PointModel::Desc:    m_desc.clear();          break;
    case PointModel::Symbol:  m_symbol.clear();        break;
    case PointModel::Type:    m_type.clear();          break;
    default: return;
    }
}
