/*
    Copyright 2019-2020 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TAGITEM_H
#define TAGITEM_H

#include <QString>
#include <QColor>

#include <src/core/treeitem.h>
#include <src/util/units.h>
#include <src/core/modelmetadata.h>

class TagModel;

class TagItem final : public TreeItem
{
private:
    explicit TagItem(TreeItem *parent = nullptr);
    explicit TagItem(const TreeItem::ItemData&, TreeItem* parent = nullptr);

    void init(const TreeItem::ItemData& data = TreeItem::ItemData());

    [[nodiscard]] QVariant data(int column, int role) const override;

    void setCategory(bool);
    [[nodiscard]] bool isCategory() const;

    using TreeItem::setData;
    bool setData(int column, const QVariant &value, int role, bool& changed) override;
    bool setIcon(int column, const QString& iconFile);

    int columnCount() const override;

    void shallowCopy(const TreeItem* src) override; // copy data, but not structure
    [[nodiscard]] TagItem* factory(const TagItem::ItemData& data, TreeItem* parent) override;

    void setUnits(ModelType mt, const QString& name);
    [[nodiscard]] const Units& getUnits(ModelType mt) const;

    // *** begin Settings API
    using TreeItem::save; // unneeded, but here for clarity.
    void load(QSettings&) override;
    // *** end Settings API

    // *** begin Stream Save API
    QDataStream& load(QDataStream&, TreeModel&) override;
    // *** end Stream Save API

    int            m_id;          // to track renaming
    Units          m_unitsSpeed;  // for per-tag speed unit override

    friend class TagModel;
};

#endif // TAGITEM_H
