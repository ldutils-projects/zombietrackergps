/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <cstdlib>
#include <chrono>

#include <marble/GeoDataCoordinates.h>

#include <QFile>
#include <QStringList>
#include <QtConcurrent>
#include <QApplication>

#include <src/util/units.h>
#include <src/util/geomath.h>
#include <src/util/resources.h>

#include "geolocmodel.inl.h"
#include "geopolmgr.h"
#include "viewparams.h"
#include "app.h"

QFont GeoLocModel::m_italicFont;

GeoLocModel::GeoLocModel(const QString& nameFile, const QString& locFile, const QString& tzFile) :
    m_root(nullptr, -1U),
    m_startedLoad(false),
    m_abortLoad(false),
    m_tzFile(tzFile),
    m_dictOffsets(nameFile),
    m_centerLatRad(0.0f),
    m_centerLonRad(0.0f),
    m_nameCache(nameFile, GeoLocFileHeader::typeName, 1U<<17, 0, 2, *this),
    m_locCache(locFile, GeoLocFileHeader::typeLoc, 1U<<14, 0, 2, *this)
{
    asyncLoad();
    setupTimer();
    setupFont();
}

GeoLocModel::GeoLocModel() :
    GeoLocModel(QStandardPaths::locate(QStandardPaths::AppDataLocation, "geoloc/locName.dat"),
                QStandardPaths::locate(QStandardPaths::AppDataLocation, "geoloc/locData.dat"),
                QStandardPaths::locate(QStandardPaths::AppDataLocation, "geoloc/locTz.dat"))
{
}

GeoLocModel::~GeoLocModel()
{
    abortLoad();
}

void GeoLocModel::setupTimer()
{
    using namespace std::chrono_literals;

    m_closeTimer.setSingleShot(true);
    m_closeTimer.setInterval(6s);

    connect(&m_closeTimer, &QTimer::timeout, this, &GeoLocModel::closeFiles);
}

void GeoLocModel::setupFont()
{
    m_italicFont = QApplication::font();
    m_italicFont.setItalic(true);
}

void GeoLocModel::asyncLoad()
{
    m_loaded = QtConcurrent::run(asyncLoadStatic, this);
    m_startedLoad = true;
}

bool GeoLocModel::finishLoad() const
{
    if (!m_loaded.isFinished()) {
        qApp->setOverrideCursor(Qt::WaitCursor);

        // Make sure we're finished loading.
        if (!m_loaded.result()) {
            qCritical("%s: Unable to load geographic name data",
                      qUtf8Printable(QApplication::applicationDisplayName()));
            throw Exit(5);
        }

        qApp->restoreOverrideCursor();
    }

    return true;
}

void GeoLocModel::abortLoad()
{
    m_abortLoad = true;
    m_loaded.waitForFinished();
}

void GeoLocModel::setCenter(const Marble::GeoDataCoordinates& coords)
{
    m_centerLatRad = float(coords.latitude());
    m_centerLonRad = float(coords.longitude());
}

ViewParams GeoLocModel::viewParams(const QModelIndex& idx) const
{
    const double latDeg = data(GeoLocModel::Lat, idx, Util::RawDataRole).toDouble();
    const double lonDeg = data(GeoLocModel::Lon, idx, Util::RawDataRole).toDouble();
    const auto feature = data(GeoLocModel::Type, idx, Util::RawDataRole).value<GeoLocEntry::Feature>();

    const Marble::GeoDataCoordinates center(lonDeg, latDeg, 0.0, Marble::GeoDataCoordinates::Degree);
    Marble::GeoDataLatLonBox bounds;
    double sizeGuess = 0.0; // in meters

    // The database doesn't have any info about bounds or sizes, so we'll make an attempt to
    // be helpful by supplying a zoom level for a few feature types.
    switch (feature) {
    case GeoLocEntry::Feature::Region:   sizeGuess = 650000.0; break;
    case GeoLocEntry::Feature::City:     sizeGuess = 65000.0;  break;
    case GeoLocEntry::Feature::Park:     sizeGuess = 10000.0;  break;
    case GeoLocEntry::Feature::Forest:   sizeGuess = 25000.0;  break;
    case GeoLocEntry::Feature::Mountain: sizeGuess =  2500.0;  break;
    case GeoLocEntry::Feature::Water:    sizeGuess = 20000.0;  break;
    case GeoLocEntry::Feature::Undersea: sizeGuess =  5000.0;  break;
    default:                             break;
    }

    if (sizeGuess > 0.0) {
        sizeGuess = sizeGuess / GeoMath::earthRadiusM / 2.0;

        const auto north = center.moveByBearing(0.0,        sizeGuess);
        const auto south = center.moveByBearing(M_PI,       sizeGuess);
        const auto east  = center.moveByBearing(M_PI_2,     sizeGuess);
        const auto west  = center.moveByBearing(3.0*M_PI_2, sizeGuess);

        bounds.setBoundaries(north.latitude(), south.latitude(),
                             east.longitude(), west.longitude());
    }

    return ViewParams(latDeg, lonDeg).setBounds(bounds);
}

// find parent node and its associated iterator for the given name.
std::pair<const GeoLocModel::Node*, QModelIndex>
GeoLocModel::parentFor(const QString& name) const
{
    QModelIndex idx;
    const Node* node = &m_root;

    const int size = std::min(name.size(), treeChars);
    for (int i = 0; i < size; ++i) {
        if (const auto it = node->constFind(name.at(i).toLower()); it == node->constEnd()) {
            node = nullptr;
            break;
        } else {
            // Regardless of what clang-tidy thinks, we need the 'it' iterator in the else clause!
            node = it.value();
            idx = index(int(node->childNumber()), 0, idx);
        }
    }

    return std::make_pair(node, idx);
}

bool GeoLocModel::asyncLoadStatic(GeoLocModel* model)
{
    return model->setup();
}

QString GeoLocModel::mdName(ModelType mt)
{
    switch (mt) {
    case GeoLocModel::Name:   return tr("Name");
    case GeoLocModel::Flags:  return tr("Flags");
    case GeoLocModel::TZ:     return tr("TZ");
    case GeoLocModel::Type:   return tr("Type");
    case GeoLocModel::Dist:   return tr("Distance");
    case GeoLocModel::Lat:    return tr("Latitude");
    case GeoLocModel::Lon:    return tr("Longitude");
    case GeoLocModel::_Count: break;
    }

    assert(0 && "Unknown TrackModel value");
    return "";
}

bool GeoLocModel::mdIsEditable(ModelType)
{
    return false;
}

bool GeoLocModel::mdIsChartable(ModelType)
{
    return false;
}

QString GeoLocModel::mdTooltip(ModelType mt)
{
    const bool editable = mdIsEditable(mt);

    switch (mt) {
    case GeoLocModel::Name:
        return makeTooltip(tr("Location name"), editable);
    case GeoLocModel::Flags:
        return makeTooltip(tr("Flags for this geopolitical area."), editable);
    case GeoLocModel::TZ:
        return makeTooltip(tr("Location's time zone."), editable);
    case GeoLocModel::Type:
        return makeTooltip(tr("Location type."), editable);
    case GeoLocModel::Dist:
        return makeTooltip(tr("Distance from center of map."), editable);
    case GeoLocModel::Lat:
        return makeTooltip(tr("Location's latitude."), editable);
    case GeoLocModel::Lon:
        return makeTooltip(tr("Location's longitude."), editable);
    default:
        assert(0 && "Unknkown GeoLocModel column");
        return "";
    }
}

QString GeoLocModel::mdTooltip(const QModelIndex& idx) const
{
    switch (idx.column()) {
    case GeoLocModel::Flags: {
        const QString name = data(GeoLocModel::Name, idx, Util::RawDataRole).toString();
        const QStringList& regionNames = data(GeoLocModel::Flags, idx, Util::RawDataRole).toStringList();

        return app().geoPolMgr().flagToolTip(name, regionNames);
    }

    default: return mdTooltip(idx.column());
    }
}

// tooltip for container column header
QString GeoLocModel::mdWhatsthis(ModelType mt)
{
    return mdTooltip(mt);  // just pass through the tooltip
}

Qt::Alignment GeoLocModel::mdAlignment(ModelType mt)
{
    switch (mt) {
    case GeoLocModel::Name:  [[fallthrough]];
    case GeoLocModel::Flags: [[fallthrough]];
    case GeoLocModel::TZ:    [[fallthrough]];
    case GeoLocModel::Type:  return Qt::AlignLeft | Qt::AlignVCenter;
    case GeoLocModel::Dist:  [[fallthrough]];
    case GeoLocModel::Lat:   [[fallthrough]];
    case GeoLocModel::Lon:   return Qt::AlignRight | Qt::AlignVCenter;
    default:                 return Qt::AlignLeft | Qt::AlignVCenter;
    }
}

const Units& GeoLocModel::mdUnits(ModelType mt)
{
    static const Units rawString(Format::String);

    switch (mt) {
    case GeoLocModel::Name:  [[fallthrough]];
    case GeoLocModel::Flags: [[fallthrough]];
    case GeoLocModel::TZ:    [[fallthrough]];
    case GeoLocModel::Type:  return rawString;
    case GeoLocModel::Dist:  return cfgData().unitsTrkLength;
    case GeoLocModel::Lat:   return cfgData().unitsLat;
    case GeoLocModel::Lon:   return cfgData().unitsLon;
    default:                 return rawString;
    }
}

QVariant GeoLocModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    static const QVariant empty;

    if (orientation != Qt::Horizontal)
        return empty;

    if (section >= 0) {
        switch (role) {
        case Qt::TextAlignmentRole: return { mdAlignment(section) };
        case Qt::ToolTipRole:       return mdTooltip(section);
        case Qt::DisplayRole:       [[fallthrough]];
        case Qt::EditRole:          return mdName(section);
        }
    }

    return QAbstractItemModel::headerData(section, orientation, role);
}

QVariant GeoLocModel::data(int column, const QModelIndex &idx, int role) const
{
    return data(sibling(idx.row(), column, idx), role);
}

QVariant GeoLocModel::data(const QModelIndex& idx, int role) const
{
    static const QVariant empty;

    if (!idx.isValid() || idx.row() >= m_nameCache.size())
        return empty;

    switch (role) {
    case Qt::TextAlignmentRole:
        return { mdAlignment(idx.column()) };

    case Qt::FontRole:
        if (idx.column() == GeoLocModel::TZ || idx.column() == GeoLocModel::Type)
            return m_italicFont;
        else
            return QApplication::font();

    case Qt::ToolTipRole:
        return mdTooltip(idx);

    case Qt::DisplayRole:
    case Qt::EditRole: {
        const QVariant rawData = GeoLocModel::data(idx, Util::RawDataRole);
        if (rawData.isValid()) {
            switch (idx.column()) {
            case GeoLocModel::TZ:
                return cfgData().unitsTz(tz(rawData.toInt()), QDateTime::currentDateTime());
            case GeoLocModel::Type:
                return GeoLocEntry::featureName(rawData.value<GeoLocEntry::Feature>());
            default:
                return mdUnits(idx.column())(rawData);
            }
        }
        break;
    }

    case Util::RawDataRole:
        if (const Node* node = getNode(idx); node != nullptr) {
            if (Node::isLeaf(idx)) {
                switch (idx.column()) {
                case GeoLocModel::Name: return m_nameCache(node->leafRec(idx.row())).name();

                case GeoLocModel::Flags:
                    return app().geoPolMgr().intersectionHierarchicalNames(locDataFor(node, idx.row()));

                case GeoLocModel::TZ:   return locDataFor(node, idx.row()).tz();
                case GeoLocModel::Type: return int(locDataFor(node, idx.row()).feature());
                case GeoLocModel::Dist: return locDataFor(node, idx.row()).greatCircleDist(m_centerLatRad, m_centerLonRad);
                case GeoLocModel::Lat:  return locDataFor(node, idx.row()).lat();
                case GeoLocModel::Lon:  return locDataFor(node, idx.row()).lon();
                }
            }  else {
                switch (idx.column()) {
                case GeoLocModel::Name: return node->name();
                default:                break;
                }
            }
        }
        break;

    default: break;
    }

    return empty;
}

QModelIndex GeoLocModel::index(int row, int column, const QModelIndex& parent) const
{
    const Node* node = getNode(parent);

    if (loadFinished()) {
        if (node->isLeaf(row))
            return createIndex(row, column, quintptr(node) | Node::leafMarker);

        node = node->child(uint32_t(row));
        return createIndex(row, column, quintptr(node));
    }

    return { };
}

QModelIndex GeoLocModel::parent(const QModelIndex& idx) const
{
    if (!idx.isValid())
        return { };

    const Node* childNode = getNode(idx);
    const Node* parentNode = Node::isLeaf(idx) ? childNode : childNode->parent();

    if (parentNode == &m_root || parentNode == nullptr)
        return { };

    return createIndex(int(parentNode->childNumber()), 0, quintptr(parentNode));
}

int GeoLocModel::rowCount(const QModelIndex& parent) const
{
    if (loadFinished() && !Node::isLeaf(parent))
        if (const Node* parentNode = getNode(parent); parentNode != nullptr)
            return int(parentNode->rowCount());

    return 0;
}

int GeoLocModel::columnCount(const QModelIndex&) const
{
    return GeoLocModel::_Count;
}

void GeoLocModel::closeFiles()
{
    m_nameCache.close();
    m_locCache.close();
}

QVector<GeoLocModel::NameData>
GeoLocModel::NameData::read(const uchar*& in, GeoLocModel& model, uint32_t start, uint32_t end)
{
    GeoLocHdr nameHdr;
    QVector<NameData> items;

    const auto* const readStartIt = model.m_dictOffsets.find(start);
    if (readStartIt == model.m_dictOffsets.cend())
        return items;

    items.reserve(int(end-start));

    in += readStartIt->offset();  // start at dictionary offset

    assert(GeoLocHdrDefs::isDict(in));  // we should have landed on a dictionary item

    for (auto currentRec = readStartIt->recNum(); currentRec < end; ++currentRec) {
        nameHdr.read(in);
        if (currentRec >= start)
            items.append(NameData(nameHdr));
    }

    return items;
}


QVector<GeoLocModel::LocData>
GeoLocModel::LocData::read(const uchar*& in, GeoLocModel&, uint32_t start, uint32_t end)
{
    QVector<LocData> items;

    items.reserve(int(end-start));
    LocData loc;

    for (auto rec = start; rec < end; ++rec) {
        if (!loc.read(in, rec))
            break;
        items.append(loc);
    }

    return items;
}

// Return dictionary offset (to the nearest prior uncompressed record) for
// the given record.
GeoLocModel::DictOffsets::const_iterator GeoLocModel::DictOffsets::find(uint32_t recNum) const
{
    const auto* notLess = std::lower_bound(begin(), end(), recNum, [](const DictOffset& lhs, const uint32_t rec) {
        return lhs.recNum() < rec;
    });

    // std::lower_bound returns the first item that is >= the query.  If the query is not found,
    // we'll get one past what we want, so we might need to move to the prior.
    if (notLess == end() || (notLess != begin() && notLess->recNum() > recNum))
        --notLess;

    return notLess;
}

uint32_t GeoLocModel::DictOffsets::operator()(uint32_t recNum) const
{
    if (const auto* const it = find(recNum); it != end())
        return it->offset();

    return GeoLocFileHeader::headerSize;
}

bool GeoLocModel::readTzFile()
{
    QFile inFile(m_tzFile);

    if (!inFile.open(QIODevice::ReadOnly))
        return false;

    m_tzNames.reserve(450);

    while (!inFile.atEnd())
        m_tzNames.append(inFile.readLine().trimmed());

    return inFile.error() == QFile::FileError::NoError;
}

bool GeoLocModel::readDictOffsets()
{
    QFile inFile(m_dictOffsets.m_fileName);

    if (!inFile.open(QIODevice::ReadOnly))
        return false;

    const uchar* base = inFile.map(0, inFile.size());
    const uchar* in = base;
    if (in == nullptr)
        return false;

    GeoLocFileHeader fileHdr(GeoLocFileHeader::typeName);
    GeoLocHdr nameHdr;

    if (!fileHdr.read(in))
        return false;

    m_dictOffsets.reserve(8192);

    // For performance
    QString priorName;
    Node* priorNode = nullptr;

    // Collect file offsets for dictionary (uncompressed) entries.
    uint32_t recNum = 0;
    while ((in - base) < inFile.size()) {
        if (m_abortLoad.load(std::memory_order_relaxed))
            return false;

        const auto offset = uint32_t(in - base);

        if (!nameHdr.read(in))
            return false;

        if (nameHdr.isDict())
            m_dictOffsets.push_back(DictOffset(uint32_t(recNum), offset));

        // Find or create tree nodes for top treeChars levels of hierarchy
        // TODO: this is broken for name lengths < treeChars
        Node* node = &m_root;

        if (nameHdr.name().leftRef(treeChars) == priorName) {
            node = priorNode;
        } else {
            const int nameTreeChars = std::min(nameHdr.name().size(), treeChars);

            for (int i = 0; i < nameTreeChars; ++i) {
                const QChar c = nameHdr.name().at(i).toLower();

                if (auto it = node->constFind(c); it != node->cend()) {
                    node = it.value();
                } else {
                    node = node->insert(c, new Node(node, node->childCount()));
                }
            }

            priorNode = node;
            priorName = nameHdr.name().left(treeChars);
        }

        // Track leaf ranges (half-open)
        if (node != nullptr)
            node->expand(recNum);

        recNum++;
    }

    return inFile.error() == QFile::FileError::NoError;
}

const QTimeZone& GeoLocModel::tz(int id) const
{
    if (id < m_tzCache.size() && m_tzCache.at(id).isValid())
        return m_tzCache.at(id);

    if (id >= m_tzCache.size())
        m_tzCache.resize(QTimeZone::availableTimeZoneIds().size());

    return m_tzCache[id] = QTimeZone(m_tzNames.at(id));
}

// Read the data we need to operate
bool GeoLocModel::setup()
{
    beginResetModel();
    const bool rc = readTzFile() && readDictOffsets();
    endResetModel();

    return rc;
}

GeoLocModel::Node::Node(const Node* parent, uint32_t childNo, uint32_t begin, uint32_t end) :
    m_parent(parent), m_leafBegin(begin), m_leafEnd(end), m_childNo(childNo)
{
}

GeoLocModel::Node*GeoLocModel::Node::insert(QChar c, GeoLocModel::Node* newNode)
{
    m_child.append(c);
    QHash<QChar, Node*>::insert(c, newNode);

    return newNode;
}

QString GeoLocModel::Node::name() const
{
    if (parent() != nullptr)
        return parent()->name() + parent()->m_child.at(int(childNumber()));

    return "";
}

