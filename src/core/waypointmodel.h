/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef WAYPOINTMODEL_H
#define WAYPOINTMODEL_H

#include <cassert>

#include <QMultiHash>
#include <QDateTime>

#include <src/core/modelmetadata.h>
#include <src/core/changetrackingmodel.h>
#include <src/core/duplicablemodel.h>
#include <src/core/removablemodel.h>

#include "mapdatamodel.h"

namespace Marble {
class GeoDataLatLonBox;
class GeoDataCoordinates;
} // namespace Marble

class QDataStream;
class WaypointItem;
class Units;

class WaypointModel final :
        public ChangeTrackingModel,
        public MapDataModel,
        public DuplicableModel,
        public RemovableModel,
        public NamedItem,
        public ModelMetaData
{
    Q_OBJECT

public:
    // It is much easier to add to the END of this list, because it doesn't require
    // as much adjustment on load.
    //
    // Columns commented [saved] are saved in the binary save format.
    // Columns commented [calc] are recalculated on load in WaypointItem::update()
    //
    enum {
        _First = 0,
        Name  = _First, // [saved] GPS waypoint name
        Icon  = Name,   // [saved] same as Name: icon stored in the name column
        Tags,           // [saved] waypoint tags, comma separated list
        Notes,          // [saved] waypoint user notes
        Type,           // [saved] waypoint type
        Symbol,         // [saved] waypoint symbol
        Source,         // [saved] waypoint source (device/file)
        Time,           // [saved] timestamp, if any
        Lat,            // [saved] point latitude
        Lon,            // [saved] point longitude
        Ele,            // [saved] point elevation
        Flags,          // [calc] flags. E.g, ("Canada", "Alberta")
        _Count,
    };

    WaypointModel(QObject* parent = nullptr);

    using ChangeTrackingModel::data;
    QVariant data(const QModelIndex& idx, int role = Qt::DisplayRole) const override;

    // Append data
    QModelIndex appendRow(const QString& name,
                          qreal lat, qreal lon, qreal ele,
                          const QStringList& tags = QStringList(),
                          const QString& notes = QString(),
                          const QString& type = QString(),
                          const QString& symbol = QString(),
                          const QString& source = QString(),
                          const QDateTime& time = QDateTime(),
                          const QString& iconName = QString(),
                          const QModelIndex& parent = QModelIndex());

    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    const Units& units(const QModelIndex& idx) const override;
    QString tooltip(const QModelIndex& idx) const;

    // Disable save/restore for this: our data will be populated from a disk DB.
    // We can't use "= delete" here though
    // *** begin Settings API
    void load(QSettings&) override { assert(0); }
    void save(QSettings&) const override { assert(0); }
    // *** end Settings API

    // *** begin Stream Save API
    using ChangeTrackingModel::save;
    using ChangeTrackingModel::load;
    quint32 streamMagic() const override;
    quint32 streamVersionMin() const override;
    quint32 streamVersionCurrent() const override;
    // *** end Stream Save API

    // *** end Stream Save API

    // *** begin ModelMetaData API
    static QString        mdName(ModelType);
    static bool           mdIsEditable(ModelType);
    static bool           mdIsChartable(ModelType);
    static QString        mdTooltip(ModelType);
    static QString        mdWhatsthis(ModelType);
    static Qt::Alignment  mdAlignment(ModelType);
    static const Units&   mdUnits(ModelType);
    static inline bool    mdSave(ModelType);
    // *** end ModelMetaData API

    // *** begin NamedItem API
    static name_t getItemNameStatic();
    // *** end NamedItem API

    bool isEditable(const ModelType mt) const override { return mdIsEditable(mt); }

    // Set and query map visibility
    void setAllVisible(bool) override;
    void setVisible(const QModelIndex&, bool) override;
    bool isVisible(const QModelIndex&) const override;

    QMultiHash<uint, QModelIndex> hashes() const override; // return set of hashes for contained waypoints (unordered!)
    uint hash(const QModelIndex& idx) const override; // hash for a single waypoint

    bool isDuplicate(const QModelIndex& lhs, const QModelIndex& rhs) const override;
    bool isDuplicate(const QModelIndex& lhs, qreal lat, qreal lon, qreal ele, const QDateTime&) const;

    std::tuple<qreal, qreal, qreal, qreal, bool> bounds(const QModelIndexList&) const override;
    Marble::GeoDataLatLonBox boundsBox(const QModelIndexList&) const override;
    using MapDataModel::boundsBox;

    Marble::GeoDataCoordinates coords(const QModelIndex&) const; // coordinates as Marble format

    void guessIcons(const QModelIndexList&, bool overwrite);

protected:
    WaypointItem* getItem(const QModelIndex&) const;

    Qt::ItemFlags flags(const QModelIndex&) const override;

private:
    void emitDataChange(int row, int col);
    void postUndoHook(const QModelIndex&, int, int) override;

    Qt::DropActions supportedDragActions() const override { return Qt::MoveAction; }

    friend QDataStream& operator<<(QDataStream&, const WaypointModel&);
    friend QDataStream& operator>>(QDataStream&, WaypointModel&);

    WaypointModel(const WaypointModel&) = delete;
    WaypointModel& operator=(const WaypointModel&) = delete;
};

QDataStream& operator<<(QDataStream&, const WaypointModel&);
QDataStream& operator>>(QDataStream&, WaypointModel&);

// True if this column is saved to disk.  Otherwise it is computed on load in
// WaypointItem::update().  This is inlined for performance during load.
inline bool WaypointModel::mdSave(ModelType mt)
{
    switch (mt) {
    case WaypointModel::Flags: return false;
    default:                   return true;
    }
}

#endif // WAYPOINTMODEL_H
