/*
    Copyright 2021 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TRACKMODEL_INL_H
#define TRACKMODEL_INL_H

#include "trackmodel.h"

// True if this column is saved to disk.  Otherwise it is computed on load in
// TrackItem::update().  This is inlined for performance during load.
inline bool TrackModel::mdSave(ModelType mt)
{
    switch (mt) {
    case TrackModel::Name:         [[fallthrough]]; // also stores Icon
    case TrackModel::Type:         [[fallthrough]];
    case TrackModel::Tags:         [[fallthrough]];
    case TrackModel::Color:        [[fallthrough]];
    case TrackModel::Notes:        [[fallthrough]];
    case TrackModel::Keywords:     [[fallthrough]];
    case TrackModel::Source:       return true;
    default:                       return false;
    }
}

#endif // TRACKMODEL_INL_H
