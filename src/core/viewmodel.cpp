/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>

#include <QModelIndex>

#include <src/util/roles.h>
#include <src/util/units.h>
#include <src/core/modelmetadata.inl.h>

#include "src/util/ui.h"

#include "app.h"
#include "viewmodel.h"
#include "viewitem.h"
#include "viewparams.h"

ViewModel::ViewModel(QObject *parent) :
    ChangeTrackingModel(new ViewItem(), parent),
    NamedItem(getItemNameStatic())
{  
    setHorizontalHeaderLabels(headersList<ViewModel>());
}

void ViewModel::setRow(const QString& name, const ViewParams& vp, const QModelIndex& idx)
{
    const QString undoName = UndoMgr::genName(tr("Set"), 1, getItemName());
    const UndoMgr::ScopedUndo undoSet(undoMgr(), undoName);

    // ViewModel namespace not needed here, but use it to improve greppability.
    setData(ViewModel::Name,      idx, name,           Util::RawDataRole);
    setData(ViewModel::CenterLat, idx, vp.centerLat(), Util::RawDataRole);
    setData(ViewModel::CenterLon, idx, vp.centerLon(), Util::RawDataRole);
    setData(ViewModel::Heading,   idx, vp.heading(),   Util::RawDataRole);
    setData(ViewModel::Zoom,      idx, vp.zoom(),      Util::RawDataRole);
}

void ViewModel::appendRow(const QString& name, const ViewParams& vp, const QString& icon)
{
    const QString undoName = UndoMgr::genName(tr("Add"), 1, getItemName());
    const UndoMgr::ScopedUndo undoSet(undoMgr(), undoName);

    appendRow();
    setRow(name, vp, index(rowCount()-1, 0));

    if (!icon.isNull())
        setIcon(index(rowCount()-1, ViewModel::Icon), icon);
}

ViewParams ViewModel::viewParams(const QModelIndex& idx) const
{
    const auto centerLat = as<qreal>(idx, ViewModel::CenterLat);
    const auto centerLon = as<qreal>(idx, ViewModel::CenterLon);
    const auto heading   = as<qreal>(idx, ViewModel::Heading);
    const auto zoom      = as<int>(idx, ViewModel::Zoom);

    return { centerLat, centerLon, heading, zoom };
}

QVariant ViewModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (const QVariant val = ModelMetaData::headerData<ViewModel>(section, orientation, role); val.isValid())
        return val;

    return ChangeTrackingModel::headerData(section, orientation, role);
}

const Units& ViewModel::units(const QModelIndex& idx) const
{
    return mdUnits(idx.column());
}

Qt::ItemFlags ViewModel::flags(const QModelIndex& idx) const
{
    return ChangeTrackingModel::flags(idx) | ModelMetaData::flags<ViewModel>(idx) |
            Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled;
}

void ViewModel::load(QSettings& settings)
{
    ChangeTrackingModel::load(settings);
    setHorizontalHeaderLabels(headersList<ViewModel>());
}

QString ViewModel::mdName(ModelType d)
{
    switch (d) {
    case ViewModel::Name:         return QObject::tr("Name");
    case ViewModel::CenterLat:    return QObject::tr("Lat");
    case ViewModel::CenterLon:    return QObject::tr("Lon");
    case ViewModel::Heading:      return QObject::tr("Heading");
    case ViewModel::Zoom:         return QObject::tr("Zoom");

    case ViewModel::_Count:       break;
    }

    assert(0 && "Unknown TrackData value");
    return "";
}

bool ViewModel::mdIsEditable(ModelType)
{
    return true;
}

// tooltip for container column header
QString ViewModel::mdTooltip(ModelType mt)
{
    const bool editable = mdIsEditable(mt);

    switch (mt) {
    case ViewModel::Name:
        return makeTooltip(tr("Descriptive name."), editable);
    case ViewModel::CenterLat:
        return makeTooltip(tr("View center latitude."), editable);
    case ViewModel::CenterLon:
        return makeTooltip(tr("View center longitude."), editable);
    case ViewModel::Heading:
        return makeTooltip(tr("View heading in degrees."), editable);
    case ViewModel::Zoom:
        return makeTooltip(tr("View zoom level."), editable);
    default:
        assert(0 && "Unknown ViewModel value");
        return { };
    }
}

// tooltip for container column header
QString ViewModel::mdWhatsthis(ModelType mt)
{
    return mdTooltip(mt);  // just pass through the tooltip
}

Qt::Alignment ViewModel::mdAlignment(ModelType mt)
{
    switch (mt) {
    case ViewModel::Name:      return Qt::AlignLeft  | Qt::AlignVCenter;
    default:                   return Qt::AlignRight | Qt::AlignVCenter;
    }
}

const Units& ViewModel::mdUnits(ModelType mt)
{
    static const Units rawString(Format::String);

    switch (mt) {
    case ViewModel::CenterLat: return cfgData().unitsLat;
    case ViewModel::CenterLon: return cfgData().unitsLon;
    default:                   return rawString;
    }
}

ViewModel::name_t ViewModel::getItemNameStatic()
{
    return { tr("View"), tr("Views") };
}

quint32 ViewModel::streamMagic() const
{
    return App::NativeMagic + quint32(App::Model::View);
}

quint32 ViewModel::streamVersionMin() const
{
    return App::NativeVersionMin;
}

quint32 ViewModel::streamVersionCurrent() const
{
    return App::NativeVersionCurrent;
}

