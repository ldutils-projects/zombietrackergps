/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOLOCMODEL_H
#define GEOLOCMODEL_H

#include <cstdint>
#include <cassert>
#include <deque>
#include <utility>
#include <atomic>

#include <QFile>
#include <QHash>
#include <QString>
#include <QFuture>
#include <QVector>
#include <QByteArray>
#include <QAbstractItemModel>
#include <QModelIndex>
#include <QTimeZone>
#include <QTimer>
#include <QFont>

#include <src/core/modelmetadata.h>

#include "viewmodel.h"
#include "geolocheader.h"

namespace Marble {
    class GeoDataCoordinates;
} // namespace Marble

// This model maps geographic names to lat/lon points and is used by the
// map search text box completer.  It reads data from disk to avoid a
// large memory footprint.
class GeoLocModel final : public QAbstractItemModel, public ModelMetaData
{
    Q_OBJECT

public:
    GeoLocModel(const QString& nameFile, const QString& locFile, const QString& tzFile);
    GeoLocModel();
    ~GeoLocModel() override;

    enum {
        Name,    // location name
        Flags,   // flags
        TZ,      // time zone
        Type,    // feature type
        Dist,    // distance from view center
        Lat,     // latitude
        Lon,     // longitude
        _Count,
    };

    // The top 'treeChars' levels of the hierarchy are explicit in the Node structure.  Any Node
    // can contain both "leafs" (record numbers in the NameData database) and "children" (sub-nodes).
    // The model row numbers for a node are as follows:
    //    0: child 0
    //    1: child 1
    //    ...
    //    N: child N
    //    N+1: leaf 0
    //    N+2: ...

    // *** begin QAbstractItemModel API
    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation, int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex&) const override;

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    // *** end QAbstractItemModel API

    QVariant data(int column, const QModelIndex &idx, int role = Qt::DisplayRole) const;

    // *** begin ModelMetaData API
    static QString        mdName(ModelType);
    static bool           mdIsEditable(ModelType);
    static bool           mdIsChartable(ModelType);
    static QString        mdTooltip(ModelType);
    static QString        mdWhatsthis(ModelType);
    static Qt::Alignment  mdAlignment(ModelType);
    static const Units&   mdUnits(ModelType);
    static inline bool    mdSave(ModelType);

    QString               mdTooltip(const QModelIndex&) const;
    // *** end ModelMetaData API

    bool finishLoad() const;                          // let any pending load finish

    static void fontSizeChanged() { setupFont(); }    // font size change

    void setCenter(const Marble::GeoDataCoordinates&);

    ViewParams viewParams(const QModelIndex&) const;  // view for given index

    static const constexpr int treeChars = 3; // top few characters form tree

public slots:
    void closeFiles();

private:
    friend class GeoLocFilter;

    void asyncLoad();  // asynchronous load
    void abortLoad();                                 // abort pending load
    bool loadStarted() const { return m_startedLoad && m_loaded.isStarted(); }
    bool loadFinished() const { return m_startedLoad && m_loaded.isFinished(); }

    // Offset to dictionary entries in buffer
    class DictOffset {
    public:
        DictOffset(uint32_t recNum = 0, uint32_t offset = 0) : m_recNum(recNum), m_offset(offset) { }
        auto recNum() const { return m_recNum; }
        auto offset() const { return m_offset; }
    private:
        uint32_t m_recNum;
        uint32_t m_offset;
    };

    static_assert(sizeof(DictOffset) == sizeof(uint32_t)*2);

    class DictOffsets : public QVector<DictOffset> {
    public:
        DictOffsets(const QString& f) : m_fileName(f) { }
        const_iterator find(uint32_t recNum) const;
        uint32_t operator()(uint32_t recNum) const;

        QString m_fileName;
    };

    class NameData {
    public:
        NameData() : m_locRec(-1U) { }
        NameData(const GeoLocHdr& hdr) : m_name(hdr.name()), m_locRec(hdr.locRec()) { }

        const QString& name() const { return m_name; }
        auto locRec() const { return m_locRec; }

        static QVector<NameData> read(const uchar*&, GeoLocModel&, uint32_t start, uint32_t end);

        bool operator<(const QString& name) const { return m_name < name; }
    private:
        QString             m_name;   // name of location
        GeoLocHdr::locRec_t m_locRec; // location record
    };

    // Per-loc-name data.
    struct LocData : public GeoLocEntry {
        using GeoLocEntry::GeoLocEntry;
        using GeoLocEntry::read;
        static QVector<LocData> read(const uchar*&, GeoLocModel&, uint32_t start, uint32_t end);
    };

    // Cache a number of records in memory for performance.  If we don't hit the cache,
    // then it's read from disk using memory mapped file.
    template <typename T> class Cache final
    {
    public:
        Cache(const QString& fileName, uint8_t type, uint32_t size, uint32_t before, uint32_t after, GeoLocModel& m);

        virtual ~Cache() { }

        const T& operator()(uint32_t record);
        int size() const { return m_fileHdr.size(); }
        QString fileName() const { return m_file.fileName(); }

        void close();

    protected:
        const uchar*& open();

        QHash<uint32_t, T> m_cache;   // data cache
        QFile              m_file;    // file to read cached data from
        const uchar*       m_map;     // mapped memory
        const uchar*       m_cur;     // current location
        GeoLocFileHeader   m_fileHdr; // file header
        GeoLocModel&       m_model;   // model ref
        uint32_t           m_cacheSize;
        uint32_t           m_fetchBefore;
        uint32_t           m_fetchAfter;

        using cache_iterator = typename decltype(m_cache)::const_iterator;

        std::deque<uint32_t> m_order; // for expirations

        // These must appear after the var decls above so we can use decltype()
        void trimCache(uint32_t size);
        cache_iterator fetch(uint32_t record);
    };

    // Hierarchy.  We have two views into this: the hash, and the vector of
    // chars in alphabetical (load) order, for the model's use by index.
    struct Node : public QHash<QChar, Node*>
    {
        Node(const Node* parent, uint32_t childNo, uint32_t begin = std::numeric_limits<uint32_t>::max(), uint32_t end = 0);

        Node* insert(QChar c, Node* newNode);

        bool hasLeafs() const { return leafCount() > 0; }
        bool isRoot() const { return parent() == nullptr; }
        uint32_t childCount() const { return uint32_t(m_child.size()); }
        uint32_t leafBegin() const { return m_leafBegin; }
        uint32_t leafEnd() const { return m_leafEnd; }
        bool hasRecord(uint32_t rec) const { return leafBegin() <= rec && rec < leafEnd(); }
        uint32_t leafCount() const {
            return (leafBegin() < leafEnd()) ? (leafEnd() - leafBegin()) : 0;
        }
        uint32_t rowCount() const { return childCount() + leafCount(); }

        bool isChild(uint32_t n) const { return n < childCount(); }
        bool isChild(int32_t n) const { return uint32_t(n) < childCount(); }
        bool isLeaf(uint32_t n) const { return !isChild(n); }
        bool isLeaf(int32_t n) const { return !isChild(n); }

        const Node* child(uint32_t n) const { return find(m_child.at(int(n))).value(); }
        const Node* child(int32_t n) const { return child(uint32_t(n)); }
        Node* child(uint32_t n) { return find(m_child.at(int(n))).value(); }
        const Node* parent() const { return m_parent; }
        uint32_t childNumber() const { return m_childNo; }
        QString name() const;

        // NameData record id for a given row
        inline uint32_t rowToLeaf(int row) const { return uint32_t(row) - childCount(); }
        inline int leafToRow(uint32_t leaf) const { return int(leaf + childCount()); }
        inline uint32_t leafRec(int row) const { return leafBegin() + rowToLeaf(row); }

        static bool isLeaf(const QModelIndex& idx) { return (idx.internalId() & ~Node::leafMask) == Node::leafMarker; }
        static void* asVoid(const Node* node) { return const_cast<void*>(reinterpret_cast<const void*>(node)); }

        static const constexpr quintptr leafMarker = 0x01; // marks node as a leaf
        static const constexpr quintptr leafMask = ~quintptr(0x03);

        void expand(uint32_t recNum) {
            m_leafBegin = std::min(m_leafBegin, recNum);
            m_leafEnd = std::max(m_leafEnd, recNum + 1);
        }

    private:
        const Node*    m_parent;       // parent node
        uint32_t       m_leafBegin;    // record range begin for the leaf
        uint32_t       m_leafEnd;      // record range end for the leaf
        uint32_t       m_childNo;      // the child number we are in the parent
        QString        m_child;        // children nodes in sort order, per character
    };

    std::pair<const Node*, QModelIndex> parentFor(const QString&) const; // find index for given name

    // Return LocData for given row in given node
    const GeoLocEntry& locDataFor(const Node* node, int row) const;
    const GeoLocEntry& locDataFor(const Node* node, uint32_t row) const;

    const Node* getNode(quintptr id) const {
        return (id != 0) ? reinterpret_cast<const Node*>(id & Node::leafMask) : &m_root;
    }
    const Node* getNode(const QModelIndex& parent) const {
        return parent.isValid() ? reinterpret_cast<const Node*>(parent.internalId() & Node::leafMask) : &m_root;
    }

    static bool asyncLoadStatic(GeoLocModel*); // asynchronous load
    bool setup();                              // initial setup
    void setupTimer();                         // timers
    bool readTzFile();                         // read tz's
    bool readDictOffsets();                    // read dictionary offsets
    static void setupFont();                   // fonts

    const QTimeZone& tz(int id) const;         // time zone cache

    Node                    m_root;            // tree root

    QTimer                  m_closeTimer;      // close file after idle timeout
    QFuture<bool>           m_loaded;          // future set during async load
    bool                    m_startedLoad;     // true if async load has started
    std::atomic_bool        m_abortLoad;       // try to abort a pending load

    QString                 m_tzFile;          // for setup
    DictOffsets             m_dictOffsets;     // offsets to dictionary entries
    QVector<QByteArray>     m_tzNames;         // timezone names by index

    float                   m_centerLatRad;    // for distance calcs
    float                   m_centerLonRad;    // ...

    // These are mutable because they provide internal caching, but the
    // external behavior is const.
    mutable Cache<NameData> m_nameCache;       // cache name records
    mutable Cache<LocData>  m_locCache;        // cache location records

    mutable QVector<QTimeZone> m_tzCache;      // time zone cache from int id in GeoLocEntry

    static QFont            m_italicFont;      // for timezone

    // Disable copying
    GeoLocModel& operator=(const GeoLocModel&) = delete;
    GeoLocModel(const GeoLocModel&) = delete;
};

#endif // GEOLOCMODEL_H
