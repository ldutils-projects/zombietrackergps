/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TRACKMODEL_H
#define TRACKMODEL_H

#include <QPersistentModelIndex>
#include <QMultiHash>
#include <QQueue>
#include <QThreadPool>
#include <cassert>
#include <tuple>
#include <limits>
#include <functional>

#include <src/core/modelmetadata.h>
#include <src/core/modelvarexpander.h>
#include <src/core/removablemodel.h>
#include <src/core/duplicablemodel.h>

#include "src/core/mergeablemodel.h"
#include "src/core/simplifiablemodel.h"
#include "src/core/reverseablemodel.h"
#include "src/core/speededitmodel.h"

#include "mapdatamodel.h"
#include "geotypes.h"

class QDataStream;
class QIODevice;
class TrackItem;
class PointModel;
class TrackUpdateThread;
class Units;

namespace Marble {
class GeoDataLatLonBox;
} // namespace Marble

class TrackModel final :
        public MergeableModel,
        public SimplifiableModel,
        public ReverseableModel,
        public DuplicableModel,
        public RemovableModel,
        public SpeedEditModel,
        public MapDataModel,
        public NamedItem,
        public ModelMetaData
{
    Q_OBJECT

public:
    // It is much easier to add to the END of this list, because it doesn't require
    // as much adjustment on load.  Many columns are computed: see mdSave() below,
    // but their IDs are stored for e.g, UI hidden columns in the trackpane,
    // colorizers, etc.  See defaultColumnPosition() for a way to set the default
    // visual position independent of this list order.
    //
    // Columns commented [saved] are saved in the binary save format.
    // Columns commented [calc] are recalculated on load in TrackItem::update()
    //
    // If new data columns are added, matching changes must be made to:
    //    1. The TrackDetailPane display format, to include the new data.
    //    2. The binary save format, if it's a saved column.
    enum {
        _First = 0,
        Name  = _First,   // [saved] GPS track name
        Icon  = Name,     // [saved] same as Name: icon stored in the name column
        Type,             // [saved] route, or track (waypoints have their own model)
        Tags,             // [saved] track tags, comma separated list
        Color,            // [saved] track display color
        Notes,            // [saved] track user notes
        Keywords,         // [saved] track keywords
        Source,           // [saved] track source (device/file)
        _LastSavedColumn = Source,
        Length,           // [calc] track length, m
        BeginDate,        // [calc] earliest timestamp in track
        EndDate,          // [calc] latest timestamp in track
        BeginTime,        // [calc] earliest time (no date) in track
        EndTime,          // [calc] latest time (no date) in track
        StoppedTime,      // [calc] total stopped time
        MovingTime,       // [calc] total moving time
        TotalTime,        // [calc] total track time (LastTime - FirstTime)
        MinElevation,     // [calc] track min elevation, m
        AvgElevation,     // [calc] track avg elevation, m
        MaxElevation,     // [calc] track max elevation, m
        MinSpeed,         // [calc] track min speed, m/s
        AvgOvrSpeed,      // [calc] average overall speed, m/s
        AvgMovSpeed,      // [calc] average moving speed, m/s
        MaxSpeed,         // [calc] track max speed, m/s
        MinGrade,         // [calc] min grade, %
        AvgGrade,         // [calc] avg grade, %
        MaxGrade,         // [calc] max grade, %
        MinCad,           // [calc] min cadence
        AvgMovCad,        // [calc] average moving cadence
        MaxCad,           // [calc] max cadence
        MinPower,         // [calc] min power, watts
        AvgMovPower,      // [calc] average moving power, watts
        MaxPower,         // [calc] max power, watts
        Energy,           // [calc] estimated input energy
        Ascent,           // [calc] track ascent, m
        Descent,          // [calc] track descent, m
        BasePeak,         // [calc] track peak-base, m
        Segments,         // [calc] number of track segments
        Points,           // [calc] number of points in track
        Area,             // [calc] track area
        MinTemp,          // [calc] min temperature, C
        AvgTemp,          // [calc] avg temperature, C
        MaxTemp,          // [calc] max temperature, C
        MinHR,            // [calc] min heart rate, bps
        AvgHR,            // [calc] average HR, bps
        MaxHR,            // [calc] max heart rate, bps
        Laps,             // [calc] number of laps (TODO: record lap data)
        MinLon,           // [calc] min longitude
        MinLat,           // [calc] min latitude
        MaxLon,           // [calc] max longitude
        MaxLat,           // [calc] max latitude
        Flags,            // [calc] flags. E.g, ("Canada", "Alberta")
        MinHRPct,         // [calc] min heart rate, % of max
        AvgHRPct,         // [calc] average HR, % of max
        MaxHRPct,         // [calc] max heart rate, % of max
        _Count,

        // Internal use, not displayed by interface
        BeginToEndEle,    // [calc] end elevation - begin elevation
    };

    TrackModel(QObject* parent = nullptr);

    using ChangeTrackingModel::data;
    [[nodiscard]] QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const override;

    using ChangeTrackingModel::setData;
    bool setData(const QModelIndex&, const QVariant& value, int role = Qt::DisplayRole) override;

    // Return true if given track is of the provided TrackType
    [[nodiscard]] bool is(const QModelIndex&, TrackType) const;

    using TreeModel::appendRow;

    // NOTE: for efficiency, this is destructive to the newPoint list: it uses std::swap.
    QModelIndex appendRow(const QString& name, TrackType type,
                          const QStringList& tags,
                          const QColor& color,
                          const QString& notes,
                          const QString& keywords,
                          const QString& source,
                          PointModel& newPoints,
                          const QModelIndex& parent = QModelIndex());

    // Append with empty PointModel data
    QModelIndex appendRow(const QString& name, TrackType type,
                          const QStringList& tags = QStringList(),
                          const QColor& color = QColor(),
                          const QModelIndex& parent = QModelIndex());

    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    [[nodiscard]] const Units& units(const QModelIndex& idx) const override;

    void refresh() override; // threaded recalculation of calculated data

    [[nodiscard]] Marble::GeoDataLatLonBox bounds(const QModelIndex&) const;
    [[nodiscard]] std::tuple<qreal, qreal, qreal, qreal, bool> bounds(const QModelIndexList&) const override;
    [[nodiscard]] Marble::GeoDataLatLonBox boundsBox(const QModelIndexList&) const override;
    using MapDataModel::boundsBox;

    [[nodiscard]] QString tooltip(const QModelIndex& idx) const;

    // Disable save/restore for this: our data will be populated from a disk DB.
    // We can't use "= delete" here though
    // *** begin Settings API
    void load(QSettings&) override { assert(0); }
    void save(QSettings&) const override { assert(0); }
    // *** end Settings API

    // *** begin Stream Save API
    QDataStream& load(QDataStream&, const QModelIndex& parent = QModelIndex(), bool append = false) override;
    bool loadForUndo(QIODevice& io, const QModelIndex& parent, int first) override;

    using ChangeTrackingModel::save;
    [[nodiscard]] quint32 streamMagic() const override;
    [[nodiscard]] quint32 streamVersionMin() const override;
    [[nodiscard]] quint32 streamVersionCurrent() const override;
    // *** end Stream Save API

    // *** begin ModelMetaData API
    static QString        mdName(ModelType);
    static bool           mdIsEditable(ModelType);
    static bool           mdIsChartable(ModelType);
    static QString        mdTooltip(ModelType);
    static QString        mdWhatsthis(ModelType);
    static Qt::Alignment  mdAlignment(ModelType);
    static const Units&   mdUnits(ModelType);
    static inline bool    mdSave(ModelType);
    // *** end ModelMetaData API

    // *** begin NamedItem API
    static name_t getItemNameStatic();
    // *** end NamedItem API

    // *** begin StaticAccum API
    static QVariant       mdAccumInit(ModelType);   // Accumulation initial value
    static QVariant       mdAccum(ModelType, const QVariant&, const QVariant&);  // Accumulate
    static QVariant       mdAccumFinal(ModelType, QVariant, int count);  // Finalize accumulation
    static bool           mdAccumSummed(ModelType);
    // *** end StaticAccum API

    [[nodiscard]] bool isEditable(const ModelType mt) const override { return mdIsEditable(mt); }

    // APIs only for use by selected classes.  Template guards against accidental general use.
    template <class T> const TrackSegLines& trackLines(const T&, const QModelIndex&) const;
    template <class T> void setAreaSelected(const T&, bool);
    template <class T> void setAreaSelected(const T&, const QModelIndex&, bool);
    template <class T> bool isAreaSelected(const T&, const QModelIndex&) const;
    template <class T> void selectPointsWithin(const T&, const Marble::GeoDataLatLonBox&);
    template <class T> bool updateQueueProcess(const T &);
    template <class T> void updateQueueAdd(T* item);
    template <class T> bool loadPending(const T&) const;

    // Set and query map visibility
    void setAllVisible(bool) override;
    void setVisible(const QModelIndex&, bool) override;
    [[nodiscard]] bool isVisible(const QModelIndex&) const override;

    [[nodiscard]] PointModel* geoPoints(const QModelIndex&);
    [[nodiscard]] const PointModel* geoPoints(const QModelIndex&) const;

    // For power estimations
    void setPerson(const QString& person);
    void setPerson(const QModelIndex& personIdx);
    [[nodiscard]] QModelIndex personIdx() const { return { m_personIdx }; }

    [[nodiscard]] QMultiHash<uint, QModelIndex> hashes() const override; // return set of hashes for contained tracks (unordered!)
    [[nodiscard]] uint hash(const QModelIndex&) const override; // hash for a single track

    [[nodiscard]] bool isDuplicate(const QModelIndex& lhs, const QModelIndex& rhs) const override;
    [[nodiscard]] bool isDuplicate(const QModelIndex& idx, const PointModel&) const;

    // Find most commonly used tags from indexes in the given list, sorted by use frequency.
    // maxTags is the maximum number to return.
    [[nodiscard]] QStringList recentTags(const QModelIndexList&, int maxTags) const;
    [[nodiscard]] QStringList recentTags(int maxTags, int maxTracks) const;

protected:
    [[nodiscard]] TrackItem* getItem(const QModelIndex&) const;

    [[nodiscard]] Qt::ItemFlags flags(const QModelIndex&) const override;

private:
    // Make sure we terminate threads on exiting stack frame
    struct LoaderThreads {
        LoaderThreads(TrackModel* tm) : m_tm(tm) { m_tm->beginThreads(); }
        ~LoaderThreads() { m_tm->endThreads(); }
        TrackModel* m_tm;
    };

    // *** begin MergeableModel API
    std::tuple<QModelIndex, bool> merge(const QModelIndexList&, const QVariant& userData) override;
    // *** end MergeableModel API
    QModelIndex beginMerge(const QModelIndexList&, const QVariant& userData);
    void merge(const QModelIndex& into, const QModelIndex& from, const QVariant& userData);
    void endMerge(const QModelIndex& to, const QVariant& userData);

    // Helpers for simplification
    Count simplifyPoints(const QModelIndexList&, const std::function<Count(SimplifiableModel&)>&);

    // *** begin SimplifiableModel API
    Count simplifyAdaptive(const QModelIndexList&, const Params&) override;
    Count simplifyTime(const QModelIndexList&, const Params&) override;
    Count simplifyDist(const QModelIndexList&, const Params&) override;
    // *** end SimplifiableModel API

    // *** begin ReverseableModel API
    void reverse(const QModelIndexList&) override;
    void reverse() override;
    // *** end ReverseableModel API

    // *** begin SpeedEditModel API
    void unsetSpeed(const QModelIndexList&) override;
    void unsetSpeed() override;
    // *** end SpeedEditModel API

    void emitDataChange(int row, int col);
    void postUndoHook(const QModelIndex&, int, int) override;

    void beginThreads();
    void endThreads();

    [[nodiscard]] static QString makeTooltip(const QString& description,
                                             bool editable, bool reqPersonData = false);

    [[nodiscard]] Qt::DropActions supportedDragActions() const override { return Qt::MoveAction; }

    [[nodiscard]] bool isTextField(ModelType) const;

    QQueue<TrackItem*>    m_updateQueue; // outstanding indexes to update in a BG thread.
    bool                  m_loadPending; // for multithreaded processing at load time
    QPersistentModelIndex m_personIdx;   // index of acive person
    ModelVarExpander      m_varExpander; // expand variable values in HTML
    QThreadPool           m_threadPool;  // so we can wait without interfering with global pool

    friend QDataStream& operator<<(QDataStream&, const TrackModel&);
    friend QDataStream& operator>>(QDataStream&, TrackModel&);
};

// Serialization
extern QDataStream& operator<<(QDataStream&, const TrackModel&);
extern QDataStream& operator>>(QDataStream&, TrackModel&);

#endif // TRACKMODEL_H

