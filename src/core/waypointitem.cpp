/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <marble/GeoDataCoordinates.h>

#include <QDataStream>
#include <QVector>
#include <QMap>
#include <QFileInfo>

#include <src/util/roles.h>
#include <src/util/math.h>
#include <src/util/qtcompat.h>

#include "waypointitem.h"
#include "waypointmodel.h"                 // for the headers
#include "app.h"

const QString WaypointItem::emptyString;

WaypointItem::WaypointItem(TreeItem *parent) :
    TreeItem(parent),
    m_isVisible(true)
{
    verifyPictogramMaps();
}

WaypointItem::WaypointItem(const TreeItem::ItemData& data, TreeItem *parent) :
    TreeItem(data, parent, Util::RawDataRole),
    m_isVisible(true)
{
    update(true);
    verifyPictogramMaps();
}

WaypointItem::~WaypointItem()
{
}

Marble::GeoDataCoordinates WaypointItem::coords() const
{
    return { lon(), lat(), hasEle() ? ele() : 0.0, Marble::GeoDataCoordinates::Degree };
}

qreal WaypointItem::lon() const
{
    return data(WaypointModel::Lon, Util::RawDataRole).toDouble();
}

qreal WaypointItem::lat() const
{
    return data(WaypointModel::Lat, Util::RawDataRole).toDouble();
}

qreal WaypointItem::ele() const
{
    return data(WaypointModel::Ele, Util::RawDataRole).toDouble();
}

QDateTime WaypointItem::time() const
{
    return data(WaypointModel::Time, Util::RawDataRole).toDateTime();
}

bool WaypointItem::hasEle() const
{
    const QVariant val = data(WaypointModel::Ele, Util::RawDataRole);

    return val.isValid() && !std::isnan(val.toDouble());
}

bool WaypointItem::hasTime() const
{
    const QVariant val = data(WaypointModel::Ele, Util::RawDataRole);

    return val.isValid() && val.toDateTime().isValid();
}

void WaypointItem::verifyPictogramMaps()
{
    for (ModelType mt : { WaypointModel::Type, WaypointModel::Symbol })
        for (const auto& iconName : pictogramMap(mt))
            if (!iconName.isEmpty())
                assert(QFileInfo(iconName).isFile() && "Waypoint icon not found");
}

const WaypointItem::pictogramMap_t& WaypointItem::pictogramMap(ModelType mt)
{
#define SYMDIR  ":art/us-nps-symbols/Light/"
#define FLAGDIR ":art/points/Flags/Dark/"
#define NAUTDIR ":art/points/Flags/Nautical/"
#define PINDIR  ":art/points/Pins/Light/"
#define CIRCDIR ":art/points/Circle/Dark/"
#define TRIDIR  ":art/points/Triangle/Dark/"
#define NUMDIR  ":art/us-nps-symbols/Numbers/Blue/"

    static const pictogramMap_t symToPictogram = {
        { "airport",                   SYMDIR "Services/Airport.svg" },
        { "alert",                     SYMDIR "Warnings/Danger.svg" },
        { "amusement park",            SYMDIR "Miscellaneous/Coaster.svg" },
        { "anchor",                    SYMDIR "WaterRecreation/Marina.svg" },
        { "animal tracks",             SYMDIR "LandRecreation/DeerWatching.svg" },
        { "atv",                       SYMDIR "LandRecreation/ATV.svg" },
        { "ball park",                 SYMDIR "LandRecreation/Baseball.svg" },
        { "bank",                      SYMDIR "Services/Cashier.svg" },
        { "bar",                       SYMDIR "Services/FoodService.svg" },
        { "beach",                     SYMDIR "Miscellaneous/Beach.svg" },
        { "bell",                      SYMDIR "Miscellaneous/Bell.svg" },
        { "big game",                  SYMDIR "LandRecreation/BearWatching.svg" },
        { "bike trail",                SYMDIR "LandRecreation/BicycleTrail.svg" },
        { "binoculars",                SYMDIR "LandRecreation/WildlifeViewing.svg" },
        { "blind",                     SYMDIR "Accessibility/LowVisionAccess.svg" },
        { "block, blue",               "" },
        { "block, green",              "" },
        { "block, red",                "" },
        { "blood trail",               "" },
        { "boat ramp",                 SYMDIR "WaterRecreation/BoatRamp.svg" },
        { "bowling",                   "" },
        { "bridge",                    SYMDIR "Miscellaneous/Bridge.svg" },
        { "building",                  SYMDIR "Miscellaneous/House.svg" },
        { "buoy, white",               SYMDIR "Miscellaneous/Buoy.svg" },
        { "campground",                SYMDIR "Accommodations/Campground.svg" },
        { "camp fire",                 SYMDIR "Miscellaneous/Campfire.svg" },
        { "canoe",                     SYMDIR "WaterRecreation/Canoe.svg" },
        { "car",                       SYMDIR "Miscellaneous/Automobiles.svg" },
        { "car rental",                SYMDIR "Miscellaneous/Automobiles.svg" },
        { "car repair",                SYMDIR "Services/Mechanic.svg" },
        { "cemetery",                  SYMDIR "Miscellaneous/Cemetary.svg" },
        { "church",                    SYMDIR "Services/Church.svg" },
        { "circle with x",             "" },
        { "circle, blue",              CIRCDIR "Blu10Blk00.svg" },
        { "circle, green",             CIRCDIR "Grn10Blk00.svg" },
        { "circle, red",               CIRCDIR "Red10Blk00.svg" },
        { "circle, yellow",            CIRCDIR "Yel10Blk00.svg" },
        { "city (large)",              "" },
        { "city (medium)",             "" },
        { "city (small)",              "" },
        { "civil",                     "" },
        { "controlled area",           "" },
        { "convenience store",         SYMDIR "Services/Store.svg" },
        { "cover",                     "" },
        { "covey",                     "" },
        { "crossing",                  SYMDIR "Warnings/PedestrianCrossing.svg" },
        { "dam",                       SYMDIR "Miscellaneous/Dam.svg" },
        { "danger area",               SYMDIR "Warnings/Danger.svg" },
        { "department store",          SYMDIR "Services/Store.svg" },
        { "diver down flag 1",         NAUTDIR "ICS_Alpha.svg" },
        { "diver down flag 2",         NAUTDIR "ICS_Diver.svg" },
        { "drinking water",            SYMDIR "Services/DrinkingWater.svg" },
        { "fast food",                 SYMDIR "Services/FoodService.svg" },
        { "fishing area",              SYMDIR "WaterRecreation/Fishing.svg" },
        { "fishing hot spot facility", SYMDIR "WaterRecreation/FishingPier.svg" },
        { "fitness center",            SYMDIR "LandRecreation/Fitness.svg" },
        { "flag, blue",                FLAGDIR "Blu10Blk00.svg" },
        { "flag, green",               FLAGDIR "Grn10Blk00.svg" },
        { "flag, red",                 FLAGDIR "Red10Blk00.svg" },
        { "flag, yellow",              FLAGDIR "Yel08Blk00.svg" },
        { "food source",               SYMDIR "Services/FoodService.svg" },
        { "forest",                    SYMDIR "Miscellaneous/Forest.svg" },
        { "gas station",               SYMDIR "Services/GasStation.svg" },
        { "glider area",               SYMDIR "LandRecreation/HangGliding.svg" },
        { "geocache",                  SYMDIR "Miscellaneous/Cache.svg" },
        { "golf course",               SYMDIR "LandRecreation/Golfing.svg" },
        { "horse trail",               SYMDIR "LandRecreation/Horse.svg" },
        { "hunting",                   "" },
        { "kayak",                     SYMDIR "WaterRecreation/Kayaking.svg" },
        { "letter a, red",             "" },
        { "letter b, red",             "" },
        { "letter c, red",             "" },
        { "letter d, red",             "" },
        { "lighthouse",                SYMDIR "Miscellaneous/Lighthouse.svg" },
        { "marina",                    SYMDIR "WaterRecreation/Marina.svg" },
        { "man overboard",             NAUTDIR "ICS_Oscar.svg" },
        { "medical facility",          SYMDIR "Emergency/Hospital.svg" },
        { "mine",                      SYMDIR "LandRecreation/RockCollecting.svg" },
        { "movie theater",             SYMDIR "Services/Theater.svg" },
        { "museum",                    SYMDIR "Miscellaneous/Museum.svg" },
        { "navaid, amber",             "" },
        { "navaid, black",             "" },
        { "navaid, blue",              "" },
        { "navaid, green",             "" },
        { "navaid, orange",            "" },
        { "navaid, red",               "" },
        { "navaid, violet",            "" },
        { "navaid, white",             "" },
        { "number 0, blue",            NUMDIR "0.svg" },
        { "number 1, blue",            NUMDIR "1.svg" },
        { "number 2, blue",            NUMDIR "2.svg" },
        { "number 3, blue",            NUMDIR "3.svg" },
        { "number 4, blue",            NUMDIR "4.svg" },
        { "number 5, blue",            NUMDIR "5.svg" },
        { "number 6, blue",            NUMDIR "6.svg" },
        { "number 7, blue",            NUMDIR "7.svg" },
        { "number 8, blue",            NUMDIR "8.svg" },
        { "number 9, blue",            NUMDIR "9.svg" },
        { "oil field",                 "" },
        { "parachute area",            "" },
        { "park",                      SYMDIR "Services/PicnicArea.svg" },
        { "parking area",              SYMDIR "Services/Parking.svg" },
        { "pharmacy",                  SYMDIR "Emergency/FirstAid.svg" },
        { "picnic area",               SYMDIR "Services/PicnicArea.svg" },
        { "pin, blue",                 PINDIR "Blu10Wht10.svg" },
        { "pin, green",                PINDIR "Grn10Wht10.svg" },
        { "pin, red",                  PINDIR "Red10Wht10.svg" },
        { "pin, yellow",               PINDIR "Yel10Wht10.svg" },
        { "pizza",                     SYMDIR "Services/FoodService.svg" },
        { "police station",            SYMDIR "Emergency/Emergencies.svg" },
        { "post office",               SYMDIR "Services/PostOffice.svg" },
        { "private field",             "" },
        { "radio beacon",              SYMDIR "Miscellaneous/Radios.svg" },
        { "residence",                 SYMDIR "Miscellaneous/House.svg" },
        { "restaurant",                SYMDIR "Services/FoodService.svg" },
        { "restricted area",           SYMDIR "Warnings/Danger.svg" },
        { "restroom",                  SYMDIR "Accommodations/Restrooms.svg" },
        { "rv park",                   SYMDIR "Accommodations/RVCampground.svg" },
        { "scales",                    "" },
        { "scenic area",               SYMDIR "Miscellaneous/ViewingArea.svg" },
        { "school",                    SYMDIR "Miscellaneous/School.svg" },
        { "shelter",                   SYMDIR "Accommodations/Shelter.svg" },
        { "shipwreck",                 SYMDIR "Miscellaneous/Shipwreck.svg" },
        { "shopping center",           SYMDIR "Services/Store.svg" },
        { "short tower",               SYMDIR "Miscellaneous/LookoutTower.svg" },
        { "shower",                    SYMDIR "Services/Showers.svg" },
        { "ski resort",                SYMDIR "WinterRecreation/DownhillSki.svg" },
        { "skiing area",               SYMDIR "WinterRecreation/CrossCountrySki.svg" },
        { "skull and crossbones",      "" },
        { "small game",                SYMDIR "LandRecreation/DeerWatching.svg" },
        { "snowmobile",                SYMDIR "WinterRecreation/SnowmobileTrail.svg" },
        { "square, yellow",            "" },
        { "stadium",                   SYMDIR "Miscellaneous/Amphitheater.svg" },
        { "stop sign",                 "" },
        { "street intersection",       SYMDIR "Miscellaneous/Sign.svg" },
        { "summit",                    "" },
        { "swimming area",             SYMDIR "WaterRecreation/Swimming.svg" },
        { "tall tower",                SYMDIR "Miscellaneous/LookoutTower.svg" },
        { "telephone",                 SYMDIR "Services/Telephone.svg" },
        { "toll booth",                "" },
        { "trail head",                SYMDIR "LandRecreation/Trailhead.svg" },
        { "tree",                      SYMDIR "Miscellaneous/Tree.svg" },
        { "tree stand",                SYMDIR "Miscellaneous/Tree.svg" },
        { "treed quarry",              SYMDIR "Miscellaneous/Tree.svg" },
        { "triangle, blue",            TRIDIR "Blu10Blk00.svg" },
        { "triangle, green",           TRIDIR "Grn10Blk00.svg" },
        { "triangle, red",             TRIDIR "Red10Blk00.svg" },
        { "triangle, yellow",          TRIDIR "Yel10Blk00.svg" },
        { "truck",                     SYMDIR "Miscellaneous/Trucks.svg" },
        { "truck stop",                SYMDIR "Miscellaneous/Trucks.svg" },
        { "tunnel",                    SYMDIR "Miscellaneous/Tunnel.svg" },
        { "ultralight area",           SYMDIR "LandRecreation/HangGliding.svg" },
        { "upland game",               "" },
        { "waterfowl",                 SYMDIR "WaterRecreation/Waterfoul.svg" },
        { "water source",              SYMDIR "Services/DrinkingWater.svg" },
        { "wind turbine",              SYMDIR "Miscellaneous/WindTurbine.svg" },
        { "wrecker",                   "" },
        { "xski",                      SYMDIR "WinterRecreation/CrossCountrySki.svg" },
        { "zoo",                       SYMDIR "Miscellaneous/Zoo.svg" },
    };

    static const pictogramMap_t typeToPictogram = {
        { "beach",                     SYMDIR "Miscellaneous/Beach.svg" },
        { "crosswalk",                 SYMDIR "Miscellaneous/Crosswalk.svg" },
        { "station",                   SYMDIR "Services/BusStop.svg" },
    };

    static const pictogramMap_t empty;
#undef SYMDIR
#undef FLAGDIR
#undef PINDIR
#undef CIRCDIR
#undef TRIDIR
#undef NUMDIR
#undef NAUTDIR

    switch (mt) {
    case WaypointModel::Symbol: return symToPictogram;
    case WaypointModel::Type:   return typeToPictogram;
    default: assert(0); return empty;
    }
}

// This is slow, but it doesn't matter for the intended use.
QString WaypointItem::symbolCase(const QString& name)
{
    QStringList fixed;

    for (QString word : name.split(' ', QtCompat::SplitBehavior::KeepEmptyParts)) {
        if (word == "rv" || word == "atv")
            word = word.toUpper();
        else if (word == "XSki")
            word = "XSki";
        else
            word[0] = word[0].toUpper();

        fixed.append(word);
    }

    return fixed.join(' ');
}

const QString& WaypointItem::guessIcon(const QString& symbol, const QString& type)
{
    std::array<QString, WaypointModel::_Count> value;
    value[WaypointModel::Symbol] = symbol;
    value[WaypointModel::Type]   = type;

    // Order matters: type overrides symbol
    for (ModelType mt : { WaypointModel::Type, WaypointModel::Symbol })
        if (!value.at(mt).isEmpty())
            if (const auto it = pictogramMap(mt).find(value.at(mt).toLower()); it != pictogramMap(mt).end())
                return *it;

    return emptyString;
}

const QString& WaypointItem::guessIcon(bool overwrite) const
{
    // Don't reset existing value unless asked to
    if (data(WaypointModel::Icon, Util::IconNameRole).isValid() && !overwrite)
        return emptyString;

    return guessIcon(data(WaypointModel::Symbol, Util::RawDataRole).toString(),
                     data(WaypointModel::Type, Util::RawDataRole).toString());
}

QString WaypointItem::tagTooltip(const QVariant& rawData) const
{
    return TagFlagItem::tagTooltip(data(WaypointModel::Name, Qt::DisplayRole).toString(), rawData);
}

QString WaypointItem::flagTooltip(const QVariant& rawData) const
{
    return TagFlagItem::flagTooltip(data(WaypointModel::Name, Qt::DisplayRole).toString(), rawData);
}

QVariant WaypointItem::data(int column, int role) const
{
    if (parent() == nullptr)
        return TreeItem::data(column, role);
    const QVariant rawData = TreeItem::data(column, Util::RawDataRole);

    switch (role) {
    case Util::RawDataRole: // small performance tweak to avoid redundant TreeItem::data()
        return rawData;

    case Qt::TextAlignmentRole:
        return { WaypointModel::mdAlignment(column) };

    case Qt::SizeHintRole:
        switch (column) {
        case WaypointModel::Notes:
            return cfgData().iconSizeTrack; // TODO: actualSize...
        }
        break;

    case Qt::DecorationRole:
        switch (column) {
        case WaypointModel::Notes:
            if (!rawData.isNull())
                return QIcon(cfgData().trackNoteIcon);
        }
        break;

    case Qt::ToolTipRole:
        switch (column) {
        case WaypointModel::Notes: return rawData;
        case WaypointModel::Tags:  return tagTooltip(rawData);
        case WaypointModel::Flags: return flagTooltip(rawData);
        }

        break;

    case Qt::EditRole:  return rawData;

    case Qt::DisplayRole: [[fallthrough]];
    case Util::CopyRole:
        if (rawData.isValid()) {
            switch (column) {
            case WaypointModel::Flags: [[fallthrough]];
            case WaypointModel::Tags:
                if (role == Util::CopyRole && rawData.type() == QVariant::StringList)
                    return static_cast<const QStringList*>(rawData.constData())->join(", ");
                break;
            case WaypointModel::Notes:
                if (role == Qt::DisplayRole)
                    return QString(); // we render an icon.
                break;
            default:
                return WaypointModel::mdUnits(column)(rawData);
            }
        }
    }

    // We didn't handle anything specially for the copy: get the display value instead
    if (role == Util::CopyRole)
        role = Qt::DisplayRole;

    return TreeItem::data(column, role);
}

bool WaypointItem::setData(int column, const QVariant &value, int role, bool &changed)
{
    if (role == Qt::BackgroundRole)
        return false;

    if (role == Qt::EditRole)
        role = Util::RawDataRole;

    return TreeItem::setData(column, value, role, changed);
}

int WaypointItem::columnCount() const
{
    return WaypointModel::_Count;
}

WaypointItem* WaypointItem::factory(const TreeItem::ItemData& data, TreeItem* parent)
{
    assert(dynamic_cast<WaypointItem*>(parent) != nullptr);

    return new WaypointItem(data, parent);
}

// Update (in same thread)
template<> void WaypointItem::update(const WaypointModel&)
{
    update(true);
}

// force==true to recalculate already calculated data, e.g, in case of data changes.
void WaypointItem::update(bool force)
{
    updateFlags<WaypointModel::Flags>(*this, force); // update track Flags column
}

bool WaypointItem::waypointEqual(const WaypointItem& other) const
{
    return Math::almost_equal(lat(), other.lat()) &&
           Math::almost_equal(lon(), other.lon()) &&
           ((!hasEle() && !other.hasEle()) || Math::almost_equal(ele(), other.ele())) &&
           ((!hasTime() && !other.hasTime()) || time() == other.time());
}

bool WaypointItem::waypointEqual(qreal latVal, qreal lonVal, qreal eleVal, const QDateTime& timeVal) const
{
    return Math::almost_equal(lat(), latVal) &&
           Math::almost_equal(lon(), lonVal) &&
           ((!hasEle() && std::isnan(eleVal)) || Math::almost_equal(ele(), eleVal)) &&
           (time() == timeVal);
}

QDataStream& WaypointItem::save(QDataStream& stream, const TreeModel& /*model*/) const
{
    if (stream.status() != QDataStream::Ok)
        return stream;

    // Don't invoke TreeItem::save here, which would save everything.  We'll save a subset
    // of our data.  The rest is recalculated on load.
    if (const auto rawData = m_itemData.find(Util::RawDataRole); rawData != m_itemData.end())
        for (int mt = WaypointModel::_First; mt < WaypointModel::_Count; ++mt)
            if (WaypointModel::mdSave(mt))
                stream << mt << (*rawData).at(mt);

    // Save guard, and track icon.
    return stream << guard << data(WaypointModel::Icon, Util::IconNameRole);
}

QDataStream& WaypointItem::load(QDataStream& stream, TreeModel&)
{
    if (stream.status() != QDataStream::Ok)
        return stream;

    // Don't invoke TreeItem::load here: we have a custom save/load to handle a subset
    // of our data.  The rest is recalculated on load.
    m_itemData.clear();

    ModelType mt;
    // Insert RawDataRole entry in the itemData, which we'll load data into.
    auto& rawData = *m_itemData.insert(Util::RawDataRole, ItemData(WaypointModel::_Count));
    while (true) {
        stream >> mt;           // the column being saved.
        if (mt < 0 || mt >= WaypointModel::_Count)
            break;
        stream >> rawData[mt];  // the contents of the column
    }

    // Verify we didn't read corrupted data.
    if (unsigned(mt) != guard) {
        stream.setStatus(QDataStream::ReadCorruptData);
        return stream;
    }

    // Load icon data
    auto& iconData = *m_itemData.insert(Util::IconNameRole, ItemData(WaypointModel::Icon + 1));
    stream >> iconData[WaypointModel::Icon];

    // Update everything
    updateIcon(iconData[WaypointModel::Icon]); // update for newer config versions
    createIcons();                             // create icon from name
    update(true);                              // do in a sub-thread from WayPointModel

    return stream;
}

void WaypointItem::updateIcon(QVariant& iconName)
{
    if (iconName.isNull() || iconName.toString().isEmpty())
        return;

    // version 14 replaced some pngs with svgs
    if (cfgData().priorCfgDataVersion < CfgData::pointPngToSvgVersion)
        if (const auto [newPath, changed] = CfgData::pngToSvg(iconName.toString()); changed)
            iconName = newPath;
}

uint WaypointItem::hash(qreal lat, qreal lon, qreal ele, bool hasEle, const QDateTime& time, uint seed)
{
    seed = qHash(std::make_pair(seed, lat));
    seed = qHash(std::make_pair(seed, lon));

    if (hasEle && !std::isnan(ele)) seed = qHash(std::make_pair(seed, ele));
    if (time.isValid())             seed = qHash(std::make_pair(seed, time));

    return seed;
}

uint qHash(const WaypointItem& wpt, uint seed)
{
    return WaypointItem::hash(wpt.lat(), wpt.lon(), wpt.ele(), wpt.hasEle(), wpt.time(), seed);
}
