/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PERSONITEM_H
#define PERSONITEM_H

#include <QString>
#include <QDate>

#include "src/fwddecl.h"

#include <src/core/treeitem.h>

class TagModel;

class PersonItem final : public TreeItem
{
private:
    explicit PersonItem(TreeItem *parent = nullptr);
    explicit PersonItem(const TreeItem::ItemData&, TreeItem* parent = nullptr);

    void init(const TreeItem::ItemData& data = TreeItem::ItemData());

    QVariant data(int column, int role) const override;

    using TreeItem::setData;
    bool setData(int column, const QVariant &value, int role, bool& changed) override;

    int columnCount() const override;

    int ageInDays(const QDate& relativeTo = QDate::currentDate()) const;  // returns -1 if no birthdate set
    int maxBpm(const QDate& relativeTo = QDate::currentDate()) const;     // return max BPM, 220-age if no birthdate, -1 if no data
    Power_t ftp(const QDate& relativeTo = QDate::currentDate()) const;    // return ftp

    PersonItem* factory(const PersonItem::ItemData& data, TreeItem* parent) override;

    friend class PersonModel;
};

#endif // PERSONITEM_H
