/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <src/core/modelmetadata.inl.h>

#include "app.h"
#include "waypointmodel.h"
#include "waypointitem.h"

WaypointModel::WaypointModel(QObject* parent) :
    ChangeTrackingModel(new WaypointItem(), parent),
    NamedItem(getItemNameStatic())
{
    setHorizontalHeaderLabels(headersList<WaypointModel>());
}

QModelIndex WaypointModel::appendRow(const QString& name,
                                     qreal lat, qreal lon, qreal ele,
                                     const QStringList& tags,
                                     const QString& notes,
                                     const QString& type,
                                     const QString& symbol,
                                     const QString& source,
                                     const QDateTime& time,
                                     const QString& iconName,
                                     const QModelIndex& parent)
{
    TreeItem::ItemData data;

    // WaypointModel namespace not needed here, but it improves greppability.
    data.resize(WaypointModel::_Count);

    if (!std::isnan(lat))    data[WaypointModel::Lat]      = lat;
    if (!std::isnan(lon))    data[WaypointModel::Lon]      = lon;
    if (!std::isnan(ele))    data[WaypointModel::Ele]      = ele;
    if (!name.isEmpty())     data[WaypointModel::Name]     = name;
    if (!tags.isEmpty())     data[WaypointModel::Tags]     = tags;
    if (!notes.isEmpty())    data[WaypointModel::Notes]    = notes;
    if (!type.isEmpty())     data[WaypointModel::Type]     = type;
    if (!symbol.isEmpty())   data[WaypointModel::Symbol]   = symbol;
    if (!source.isEmpty())   data[WaypointModel::Source]   = source;
    if (time.isValid())      data[WaypointModel::Time]     = time;

    ChangeTrackingModel::appendRow(data, parent);

    const QModelIndex newIdx = index(rowCount()-1, 0, parent);
    setIcon(newIdx, iconName);

    emit dataChanged(newIdx, rowSibling(columnCount()-1, newIdx));

    return newIdx;
}

QVariant WaypointModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (const QVariant val = ModelMetaData::headerData<WaypointModel>(section, orientation, role); val.isValid())
        return val;

    return ChangeTrackingModel::headerData(section, orientation, role);
}

const Units& WaypointModel::units(const QModelIndex& idx) const
{
    return mdUnits(idx.column());
}

QString WaypointModel::tooltip(const QModelIndex& idx) const
{
    static const QVector<ModelType> tooltipColumns = {
        WaypointModel::Time,
        WaypointModel::Lat,
        WaypointModel::Lon,
        WaypointModel::Ele,
        WaypointModel::Symbol,
        WaypointModel::Type,
    };

    const WaypointItem* item = getItem(idx);

    QString tooltip = QString("<p><b><u><nobr><big>") +
            item->data(WaypointModel::Name, Qt::DisplayRole).toString() +
            + "</big></nobr></u></b></p>";

    tooltip += "<table>";

    for (const auto col : tooltipColumns) {
        if (const QVariant colData = item->data(col, Qt::DisplayRole); colData.isValid()) {
            tooltip += "<tr><td><b>" + mdName(col) + " </b></td><td>";
            tooltip += colData.toString();
            tooltip += "</td></tr>";
        }
    }

    tooltip += "</table>";

    return tooltip;
}

Qt::ItemFlags WaypointModel::flags(const QModelIndex& idx) const
{
    const Qt::ItemFlags flags = ChangeTrackingModel::flags(idx) | ModelMetaData::flags<WaypointModel>(idx) |
            Qt::ItemIsDragEnabled;

    switch (idx.column()) {
    case WaypointModel::Tags:
    case WaypointModel::Notes:
    case WaypointModel::Flags:
        return flags & ~Qt::ItemIsSelectable;
    default:
        return flags;
    }
}


WaypointItem* WaypointModel::getItem(const QModelIndex& idx) const
{
    return static_cast<WaypointItem*>(ChangeTrackingModel::getItem(idx));
}

QString WaypointModel::mdName(ModelType mt)
{
    switch (mt) {
    case WaypointModel::Name:     return QObject::tr("Name");
    case WaypointModel::Tags:     return QObject::tr("Tags");
    case WaypointModel::Notes:    return QObject::tr("Notes");
    case WaypointModel::Type:     return QObject::tr("Type");
    case WaypointModel::Symbol:   return QObject::tr("Symbol");
    case WaypointModel::Source:   return QObject::tr("Source");
    case WaypointModel::Time:     return QObject::tr("Time");
    case WaypointModel::Lat:      return QObject::tr("Latitude");
    case WaypointModel::Lon:      return QObject::tr("Longitude");
    case WaypointModel::Ele:      return QObject::tr("Elevation");
    case WaypointModel::Flags:    return QObject::tr("Flags");
    case WaypointModel::_Count:   break;
    }

    assert(0 && "Unknown WaypointModel value");
    return "";
}

bool WaypointModel::mdIsEditable(ModelType mt)
{
    switch (mt) {
    case WaypointModel::Name:     [[fallthrough]];
    case WaypointModel::Tags:     [[fallthrough]];
    case WaypointModel::Notes:    [[fallthrough]];
    case WaypointModel::Type:     [[fallthrough]];
    case WaypointModel::Symbol:   [[fallthrough]];
    case WaypointModel::Source:   [[fallthrough]];
    case WaypointModel::Time:     [[fallthrough]];
    case WaypointModel::Lat:      [[fallthrough]];
    case WaypointModel::Ele:      [[fallthrough]];
    case WaypointModel::Lon:      return true;
    default:                      return false;
    }
}

// Return true if this data can be placed in a chart
bool WaypointModel::mdIsChartable(ModelType)
{
    return false;
}

// tooltip for column header
QString WaypointModel::mdTooltip(ModelType mt)
{
    const bool editable = mdIsEditable(mt);

    switch (mt) {
    case WaypointModel::Name:
        return makeTooltip(tr("Descriptive waypoint name.."), editable);
    case WaypointModel::Tags:
        return makeTooltip(tr("Tags applied to this waypoint. The master list of available "
                              "tags may be edited from the Settings dialog."), editable);
    case WaypointModel::Notes: 
        return makeTooltip(tr("User notes."), editable);
    case WaypointModel::Type: 
        return makeTooltip(tr("Waypoint type."), editable);
    case WaypointModel::Symbol: 
        return makeTooltip(tr("Waypoint symbol."), editable);
    case WaypointModel::Source:
        return makeTooltip(tr("Import source (device or file)."), editable);
    case WaypointModel::Time:  
        return makeTooltip(tr("Waypoint timestamp."), editable);
    case WaypointModel::Lat:    
        return makeTooltip(tr("Latitude of waypoint."), editable);
    case WaypointModel::Lon:    
        return makeTooltip(tr("Longitude of waypoint."), editable);
    case WaypointModel::Ele:
        return makeTooltip(tr("Elevation of waypoint."), editable);
    case WaypointModel::Flags:  
        return makeTooltip(tr("Flags of regions containing the waypoint."), editable);
    default:
        assert(0 && "Unknown TrackModel value");
        return "";
    }
}

// tooltip for container column header
QString WaypointModel::mdWhatsthis(ModelType mt)
{
    return mdTooltip(mt);  // just pass through the tooltip
}

Qt::Alignment WaypointModel::mdAlignment(ModelType mt)
{
    switch (mt) {
    case WaypointModel::Lat: [[fallthrough]];
    case WaypointModel::Lon: [[fallthrough]];
    case WaypointModel::Ele: return Qt::AlignRight   | Qt::AlignVCenter;
    default:                 return Qt::AlignLeft    | Qt::AlignVCenter;
    }
}

const Units& WaypointModel::mdUnits(ModelType mt)
{
    static const Units rawString(Format::String);

    switch (mt) {
    case WaypointModel::Lat:  return cfgData().unitsLat;
    case WaypointModel::Lon:  return cfgData().unitsLon;
    case WaypointModel::Ele:  return cfgData().unitsElevation;
    case WaypointModel::Time: return cfgData().unitsTrkDate;
    default:                  return rawString;
    }
}

WaypointModel::name_t WaypointModel::getItemNameStatic()
{
    return { tr("Waypoint"), tr("Waypoints") };
}

bool WaypointModel::isDuplicate(const QModelIndex& lhs, const QModelIndex& rhs) const
{
    // Handle one or the other being in a foreign model
    const auto* lhsModel = qobject_cast<const WaypointModel*>(lhs.model());
    const auto* rhsModel = qobject_cast<const WaypointModel*>(rhs.model());

    if (lhsModel == nullptr || rhsModel == nullptr)
        return false;

    return lhsModel->getItem(lhs)->waypointEqual(*rhsModel->getItem(rhs));
}

bool WaypointModel::isDuplicate(const QModelIndex& lhs, qreal lat, qreal lon, qreal ele, const QDateTime& time) const
{
    return getItem(lhs)->waypointEqual(lat, lon, ele, time);
}

std::tuple<qreal, qreal, qreal, qreal, bool>
WaypointModel::bounds(const QModelIndexList& selection) const
{
    static const qreal NaN = std::numeric_limits<qreal>::quiet_NaN();

    qreal maxlat = NaN;
    qreal maxlon = NaN;
    qreal minlat = NaN;
    qreal minlon = NaN;

    for (const auto& idx : selection) {
        maxlat = std::fmax(maxlat, data(WaypointModel::Lat, idx, Util::RawDataRole).toDouble());
        maxlon = std::fmax(maxlon, data(WaypointModel::Lon, idx, Util::RawDataRole).toDouble());
        minlat = std::fmin(minlat, data(WaypointModel::Lat, idx, Util::RawDataRole).toDouble());
        minlon = std::fmin(minlon, data(WaypointModel::Lon, idx, Util::RawDataRole).toDouble());
    }

    return std::make_tuple(maxlat, maxlon, minlat, minlon,
                           (!std::isnan(maxlat) && !std::isnan(maxlon) && !std::isnan(minlat) && !std::isnan(minlon)));
}

// TODO: share with TrackModel
Marble::GeoDataLatLonBox WaypointModel::boundsBox(const QModelIndexList& selection) const
{
    const auto [maxlat, maxlon, minlat, minlon, valid] = bounds(selection);

    if (!valid)
        return { };

    return { maxlat, minlat, maxlon, minlon,  Marble::GeoDataCoordinates::Degree };
}

Marble::GeoDataCoordinates WaypointModel::coords(const QModelIndex& idx) const
{
    if (const WaypointItem* wpt = getItem(idx); wpt != nullptr)
        return wpt->coords();

    return { };
}

void WaypointModel::guessIcons(const QModelIndexList& selection, bool overwrite)
{
    for (const auto& idx : selection)
        if (WaypointItem* wpt = getItem(idx); wpt != nullptr)
            if (const QString iconFile = wpt->guessIcon(overwrite); !iconFile.isNull())
                setIcon(idx, iconFile);
}

void WaypointModel::emitDataChange(int row, int col)
{
    emit dataChanged(index(row, col), index(row, col));
}

void WaypointModel::postUndoHook(const QModelIndex& parent, int start, int end)
{
    for (int row = start; row <= end; ++row)
        getItem(child(row, parent))->update();
}

QVariant WaypointModel::data(const QModelIndex& idx, int role) const
{
    if (!idx.isValid())
        return { };

    if (const QVariant val = getItem(idx)->data(idx.column(), role); val.isValid())
        return val;

    if (role == Qt::ToolTipRole)
        return tooltip(idx);

    return { };
}

QDataStream& operator<<(QDataStream& stream, const WaypointModel& model)
{
    return model.save(stream);
}

QDataStream& operator>>(QDataStream& stream, WaypointModel& model)
{
    return model.load(stream);
}

QMultiHash<uint, QModelIndex> WaypointModel::hashes() const
{
    QMultiHash<uint, QModelIndex> hashes;

    Util::Recurse(*this, [this, &hashes](const QModelIndex& idx) {
        hashes.insert(qHash(*getItem(idx)), idx);
        return true;
    });

    return hashes;
}

uint WaypointModel::hash(const QModelIndex& idx) const
{
    return qHash(*getItem(idx));
}

void WaypointModel::setAllVisible(bool v)
{
    Util::Recurse(*this, [this, v](const QModelIndex& idx) {
        getItem(idx)->setVisible(v);
        return true;
    });
}

void WaypointModel::setVisible(const QModelIndex& idx, bool v)
{
    getItem(idx)->setVisible(v);
}

bool WaypointModel::isVisible(const QModelIndex& idx) const
{
    return getItem(idx)->isVisible();
}

quint32 WaypointModel::streamMagic() const
{
    return App::NativeMagic + quint32(App::Model::Waypoint);
}

quint32 WaypointModel::streamVersionMin() const
{
    return App::NativeVersionMin;
}

quint32 WaypointModel::streamVersionCurrent() const
{
    return App::NativeVersionCurrent;
}

