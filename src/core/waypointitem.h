/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef WAYPOINTITEM_H
#define WAYPOINTITEM_H

#include <QStringList>
#include <src/core/treeitem.h>
#include "tagflagitem.h"

class QDataStream;
class TrackModel;
class GeoPolMgr;

namespace Marble {
class GeoDataCoordinates;
} // namespace Marble

class WaypointItem final : public TreeItem, public TagFlagItem
{
public:
    template <class T> void update(const T&);   // updates both track metadata and placemarks

    Marble::GeoDataCoordinates coords() const;  // coordinates as Marble format
    static uint hash(qreal lat, qreal lon, qreal ele, bool hasEle, const QDateTime&, uint seed = 0);

    // Map of values (e.g, waypoint symbol names) to icons
    using pictogramMap_t = QMap<QString, QString>;
    static const pictogramMap_t& pictogramMap(ModelType);
    static QString symbolCase(const QString&); //  hack: fix case to match GPX

    static const QString& guessIcon(const QString& symbol, const QString& type);

private:
    friend class TagFlagItem;

    explicit WaypointItem(TreeItem *parent = nullptr);
    explicit WaypointItem(const TreeItem::ItemData&, TreeItem *parent = nullptr);

    ~WaypointItem() override;

    qreal lon() const;      // waypoint longitude
    qreal lat() const;      // waypoint latitude
    qreal ele() const;      // waypoint elevation
    QDateTime time() const; // waypoint timestamp
    bool hasEle() const;    // true if elevation is set
    bool hasTime() const;   // true if time is set

    static void verifyPictogramMaps();  // sanity test to verify all icons are found

    const QString& guessIcon(bool overwrite) const; // Attempt to guess icon from symbol.

    // *** begin Stream Save API
    QDataStream& save(QDataStream&, const TreeModel&) const override;
    QDataStream& load(QDataStream&, TreeModel&) override;
    // *** end Stream Save API

    // Update icons for filename changes
    static void updateIcon(QVariant&);

    friend uint qHash(const WaypointItem& wpt, uint seed);

    QVariant data(ModelType column, int role) const override;

    using TreeItem::setData;
    bool setData(ModelType column, const QVariant &value, int role, bool& changed) override;

    int  columnCount() const override;

    WaypointItem* factory(const WaypointItem::ItemData& data, TreeItem* parent) override;

    friend class WaypointModel;

    void update(bool force = false);      // updates metadata, placemarks, and flags

    // performance cache for the TrackMap
    void setVisible(bool v)     { m_isVisible = v; }
    bool isVisible() const      { return m_isVisible; }

    bool waypointEqual(const WaypointItem& other) const;
    bool waypointEqual(qreal lat, qreal lon, qreal ele, const QDateTime&) const;

    // Generate tooltips
    QString flagTooltip(const QVariant& rawData) const;
    QString tagTooltip(const QVariant& rawData) const;

    bool             m_isVisible;    // performance opt for TrackMap display

    static const QString emptyString;

    WaypointItem(const WaypointItem&) = delete;
    WaypointItem& operator=(const WaypointItem&) = delete;
};

// Hashing
uint qHash(const WaypointItem&, uint seed = 0);

#endif // WAYPOINTITEM_H
