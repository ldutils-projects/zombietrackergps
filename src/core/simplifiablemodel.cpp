/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <type_traits>
#include <QDataStream>
#include <cassert>

#include "simplifiablemodel.h"

#include "src/ui/dialogs/tracksimplifydialog.h"

SimplifiableModel::SimplifiableModel()
{
}

SimplifiableModel::~SimplifiableModel()
{
}

SimplifiableModel::Count SimplifiableModel::simplify(const QModelIndexList& selection,
                                                     const Params& params)
{
    switch (params.m_type) {
    case SimplifyType::Invalid:  assert(0); break;
    case SimplifyType::Adaptive: return simplifyAdaptive(selection, params);
    case SimplifyType::ByTime:   return simplifyTime(selection, params);
    case SimplifyType::ByDist:   return simplifyDist(selection, params);
    }

    return { };
}

QDataStream& operator<<(QDataStream& out, const SimplifiableModel::SimplifyType& st)
{
    return out << std::underlying_type_t<SimplifiableModel::SimplifyType>(st);
}

QDataStream& operator>>(QDataStream& in, SimplifiableModel::SimplifyType& st)
{
    return in >> reinterpret_cast<std::underlying_type_t<SimplifiableModel::SimplifyType>&>(st);
}

SimplifiableModel::Count&operator+=(SimplifiableModel::Count& lhs, const SimplifiableModel::Count& rhs)
{
    lhs.m_tracks       += rhs.m_tracks;
    lhs.m_beforePoints += rhs.m_beforePoints;
    lhs.m_afterPoints  += rhs.m_afterPoints;

    return lhs;
}

// Create params for adaptive smode
SimplifiableModel::Params SimplifiableModel::Params::Adaptive(Dist_t dist, SimplifiableModel::Mode mode)
{
    return { SimplifyType::Adaptive, dist, mode };
}

// Create params for Time mode
SimplifiableModel::Params SimplifiableModel::Params::Time(Dur_t time, SimplifiableModel::Mode mode)
{
    return { SimplifyType::ByTime, time, mode };
}

// Create params for Dist mode
SimplifiableModel::Params SimplifiableModel::Params::Dist(Dist_t dist, SimplifiableModel::Mode mode)
{
    return { SimplifyType::ByDist, dist, mode };
}

// Invalid settings
SimplifiableModel::Params::Params() :
    m_type(SimplifyType::Invalid), m_mode(Mode::Preview), m_time(0)
{
    assert(0);
}

SimplifiableModel::Params::Params(SimplifyType type, Dist_t dist, Mode mode) :
    m_type(type), m_mode(mode), m_dist(dist)
{
    assert(type == SimplifyType::Adaptive || type == SimplifyType::ByDist);
}

SimplifiableModel::Params::Params(SimplifyType type, Dur_t time, Mode mode) :
    m_type(type), m_mode(mode), m_time(time)
{
    assert(type == SimplifyType::ByTime);
}

