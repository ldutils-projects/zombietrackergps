/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef POINTITEMCONV_H
#define POINTITEMCONV_H

#include <cstdint>
#include <type_traits>

#include <QtCore>

#include "pointmodel.h"

class QDataStream;
class PointModel;

// For load-time conversion from older binary save formats

namespace V1000 { // binary format 0x1000
struct PointItem {
    uint8_t   m_flags;     // cache for render performance
    QDateTime m_time;      // sample timestamp

    qreal     m_lon;       // longitude, degrees
    qreal     m_lat;       // latitude, degrees
    qreal     m_ele;       // elevation, m

    qreal     m_fltLon;    // noise-filtered longitude
    qreal     m_fltLat;    // noise-filtered latitude
    qreal     m_fltEle;    // noise-filtered elevation

    float     m_length;    // leg distance (to next point), m
    float     m_dist;      // total distance within the track to this point, m
    float     m_vert;      // vertical distance, m
    float     m_grade;     // grade, %
    float     m_gradeSinAtan; // cached
    int       m_duration;  // in mSec

    float     m_atemp;     // air temp
    float     m_wtemp;     // water temp
    float     m_depth;     // water depth
    float     m_speed;     // vehicle speed, m/s
    float     m_accel;     // acceleration, m/s^2
    uint8_t   m_hr;        // heart rate, beat/min
    uint8_t   m_cad;       // cadence, rev/min
    float     m_power;     // rider power
    float     m_course;    // course (TBD: is this magnetic or true?)
    float     m_bearing;   // bearing
};
} // namespace V1000

namespace V1001 { // binary format 0x1001 & 0x1002
struct PointItem {
    QDateTime m_time;      // [saved] sample timestamp

    Lon_t     m_lon;       // [saved] longitude, degrees
    Lat_t     m_lat;       // [saved] latitude, degrees

    Ele_t     m_ele;       // [saved] elevation, m
    Ele_t     m_fltEle;    // [calc] noise-filtered elevation

    Dist_t    m_dist;      // [calc] total distance within the track to this point, m
    Speed_t   m_speed;     // [saved] vehicle speed, m/s
    Accel_t   m_accel;     // [calc] acceleration, m/s^2
    Power_t   m_power;     // [saved] rider power
    short     m_grade;     // [calc] grade, %
    short     m_depth;     // [saved] water depth, meters * 5
    int16_t   m_atemp;     // [saved] air temp, degrees C * 100
    int16_t   m_wtemp;     // [saved] water temp, degrees C * 100
    ushort    m_course;    // [saved] course, degrees * 100
    ushort    m_bearing;   // [saved] bearing, degrees * 100

    uint8_t   m_flags;     // [calc] cache for render performance
    Hr_t      m_hr;        // [saved] heart rate, beat/min
    Cad_t     m_cad;       // [saved] cadence, rev/min
};
}  // namespace V1001

extern QDataStream& operator>>(QDataStream&, V1000::PointItem&);
extern QDataStream& operator>>(QDataStream&, V1001::PointItem&);

// Convert from old version
template <typename OLDPOINT> QDataStream& convert(QDataStream& stream, PointModel& p)
{
    QList<QVector<OLDPOINT>> oldPt;
    stream >> oldPt;

    p.clear();

    // Point-wise conversion using PointItem's constructor from older versions
    for (const auto& seg : oldPt) {
        p.push_back(PointModel::container_type::value_type());
        p.back().reserve(seg.size());

        for (const auto& pt : seg)
            p.back().push_back(pt);
    }

    return stream;
}

#endif // POINTITEMCONV_H
