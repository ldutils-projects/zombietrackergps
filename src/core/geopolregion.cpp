/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <memory> // for unique_ptr

#include <marble/GeoDataLinearRing.h>
#include <marble/GeoDataCoordinates.h>

#include <QMutexLocker>
#include <QDir>
#include <QDataStream>

#include "geolocheader.h"
#include "trackitem.h"
#include "waypointitem.h"
#include "geopolregion.h"
#include "geopolmgr.h" // just for checkEndMarker
#include "viewparams.h"

const QString& GeoPolRegion::parentName() const
{
    static const QString null;

    return parent() != nullptr ? parent()->name() : null;
}

QString GeoPolRegion::tooltipTableRow() const
{
    return "<tr><td valign=middle><img src=" + flag() + "></img></td>" +
            "<td valign=middle>" + name() + "</td>" +
            "<td valign=middle>" + isoA2() + "</td>" +
            "<td valign=middle>" + type() + "</td></tr>";
}

namespace {
void addNames(QString& name, const GeoPolRegion* region)
{
    if (region == nullptr)
        return;

    addNames(name, region->parent());

    if (!region->name().isEmpty())
        name.append((name.isEmpty() ? "" : "/") + region->name());
}
} // anonymous namespace

QString GeoPolRegion::hierarchicalName() const
{
    QString name;
    name.reserve(64);

    addNames(name, this);

    return name;
}

bool GeoPolRegion::loadPolygon(QDataStream& in)
{
    const uint32_t boundaryCount = GeoPolMgr::readLoopCount(in, 4096);
    if (boundaryCount == uint32_t(-1)) // sanity
        return false;

    std::unique_ptr<Marble::GeoDataPolygon> polygon(new Marble::GeoDataPolygon());

    // Loop over boundaries (first one is outer, then inner)
    bool isOuter = true;
    for (uint32_t boundaryIdx = 0; boundaryIdx < boundaryCount; ++boundaryIdx) {
        const uint32_t coordCount = GeoPolMgr::readLoopCount(in, 1000000);
        if (coordCount == uint32_t(-1)) // sanity
            return false;

        Marble::GeoDataLinearRing ring;
        ring.reserve(coordCount); // for efficiency

        // Loop over all coordinates in this boundary
        for (uint32_t coordIdx = 0; coordIdx < coordCount; ++coordIdx) {
            // Hack: since this is 2D data, while the GeoDataPolygons always factor elevation into
            //       containment, we force our elevation bounds to encompass any possible inputs.
            //       Otherwise, contains() always returns false.
            const qreal ele = (coordIdx == 0 ? -10000000.0 :
                               coordIdx == 1 ?  10000000.0 : 0);

            double lon, lat;
            in >> lon >> lat;

            const Marble::GeoDataCoordinates coord(lon, lat, ele, Marble::GeoDataCoordinates::Degree);

            ring.append(coord);
        }

        if (!GeoPolMgr::checkEndMarker(in))
            return false;

        if (isOuter)
            polygon->setOuterBoundary(ring);
        else
            polygon->appendInnerBoundary(ring);

        isOuter = false; // only the first boundary is the outer one.
    }

    if (!GeoPolMgr::checkEndMarker(in))
        return false;

    // Polygon is a unique_ptr to avoid leaks during early returns, so use release().
    Marble::GeoDataMultiGeometry::append(polygon.release());

    return true;
}

bool GeoPolRegion::loadRegion(QDataStream& in)
{
    const uint32_t polygonCount = GeoPolMgr::readLoopCount(in, 1000000);
    if (polygonCount == uint32_t(-1)) // sanity
        return false;

    for (uint32_t polygonIdx = 0; polygonIdx < polygonCount; ++polygonIdx)
        if (!loadPolygon(in))
            return false;

    if (!GeoPolMgr::checkEndMarker(in))
        return false;

    return true;
}

GeoPolRegion* GeoPolRegion::findParent(const QStringList& names, int level)
{
    // We found the level: return this object.
    if ((level+1) >= names.size())
        return this;

    const auto it = m_nameToRegionMap.constFind(names[level]);

    // Return nullptr if not found
    if (it == m_nameToRegionMap.cend())
        return nullptr;

    // Recurse down into the structure to find the next level
    return (*it)->findParent(names, level+1);
}

void GeoPolRegion::append(const QString& name, GeoPolRegion* region)
{
    region->m_parent = this;  // track parent

    // GeoPolRegions are only appended to the map, not the underlying GeoDataMultiGeometry
    m_nameToRegionMap.insert(name, region); // track name to pointer
}

GeoPolRegion* GeoPolRegion::copy() const
{
    // We don't copy this class because it contains a QMutex, so we better never
    // get here.
    assert(0);
    return nullptr;
}

inline qreal GeoPolRegion::abslondiff(qreal lon0, qreal lon1 )
{
    const qreal diff = abs(lon0 - lon1);
    if (diff < M_PI)
        return abs(diff);

    return M_PI*2 - abs(diff);
}

// Expand box to encompass longitude, in smallest expansion direction.
inline void GeoPolRegion::extend(Marble::GeoDataLatLonAltBox& lhs, qreal lon)
{
    const bool idl = lhs.crossesDateLine();

    if (idl) {
        // lhs straddles antimeridian
        // No-op if it's inside already
        if (lon >= lhs.west() || lon <= lhs.east())
            return;
    } else {
        // lhs doesn't straddle antimeridian
        // No-op if it's inside already
        if (lhs.west() <= lon && lon <= lhs.east())
            return;
    }

    // Else, it's outside: expand east, or west, whichever is smaller.  This might
    // extend the box across the antimeridian.
    if (abslondiff(lhs.east(), lon) < abslondiff(lhs.west(), lon)) {
        lhs.setEast(lon);
    } else {
        lhs.setWest(lon);
    }
}

void GeoPolRegion::extend(Marble::GeoDataLatLonAltBox& lhs, const Marble::GeoDataLatLonAltBox& rhs)
{
    if (lhs.isNull()) {
        lhs = rhs;
        return;
    }

    if (rhs.isNull())
        return;

    lhs.setNorth(qMax(lhs.north(), rhs.north()));
    lhs.setSouth(qMin(lhs.south(), rhs.south()));
    extend(lhs, rhs.east());
    extend(lhs, rhs.west());
}

Marble::GeoDataLatLonAltBox GeoPolRegion::updateBounds()
{
    m_bounds.clear();

    // Refresh all children, expanding our own bounds as we go
    for (auto* it : qAsConst(m_nameToRegionMap))
        extend(m_bounds, it->updateBounds());

    // Expand bounds based on marble polygons, but do not use the Marble::GeoDataMultiGeometry
    // latLonAltBox method, since it is bugged.  We'll do our own bounds unioning.
    for (const auto* geo : const_cast<GeoPolRegion*>(this)->vector())
        extend(m_bounds, geo->latLonAltBox());

    extend(m_bounds, latLonAltBox());  // expand bounds based on any marble polygons
    m_bounds.setMinAltitude(-1000000.0); // expand height to encompass all points
    m_bounds.setMaxAltitude(1000000.0);

    return m_bounds;
}

bool GeoPolRegion::load(QDataStream& in,
                        const QStringList& names, const QString& isoA2, const QString& type, int level)
{
    // Find the GeoPolRegion to add into.
    GeoPolRegion* addTo = findParent(names);

    if (addTo == nullptr) // Failed to find appropriate region to add to.  Bad input data.
        return false;

    auto* const region = new GeoPolRegion(names.back(), isoA2, type, level);

    if (!region->loadRegion(in)) {
        delete region;
        return false;
    }

    addTo->append(region->name(), region);

    return true;
}

bool GeoPolRegion::intersects(const TrackItem& trk) const
{
    const auto& lines = trk.trackSegLines();
    if (lines.isEmpty())
        return false;

    // For performance reasons, only test the begin and end points.
    return (!lines.first().isEmpty() && intersects(lines.first().first())) ||
           (!lines.last().isEmpty() && intersects(lines.last().last()));

    return false;
}

bool GeoPolRegion::intersects(const WaypointItem& wpt) const
{
    return intersects(wpt.coords());
}

bool GeoPolRegion::intersects(const Marble::GeoDataCoordinates& point) const
{
    if (bounds().contains(point)) {
        if (size() == 0)  // if no GeoData hierarchy, hit if hits bounding box.
            return true;

        // If it's the last queried point that hit, then it hit.
        if (point.latitude() == m_lastHitLat && point.longitude() == m_lastHitLon)
            return true;

        // Marble bug? vector() works, but constBegin()/end() iterators crash
        for (const auto* geo : const_cast<GeoPolRegion*>(this)->vector()) {
            if (const auto* polygon = dynamic_cast<const Marble::GeoDataPolygon*>(geo); polygon != nullptr) {
                if (polygon->contains(point)) {
                    m_lastHitLat = point.latitude();
                    m_lastHitLon = point.longitude();
                    return true;
                }
            }
        }
    }

    return false;
}

bool GeoPolRegion::appendWorld(GeoPolRegionVec& regions, int initSize, WorldOpt worldOpt) const
{
    // If at the world level, add it if asked.
    if (level() < 0)
        if ((regions.isEmpty() && worldOpt == WorldOpt::IncludeIfNoOther) ||
            (!regions.isEmpty() && worldOpt == WorldOpt::IncludeIfOther) ||
            (worldOpt == WorldOpt::IncludeAlways))
            regions.prepend(this);

    return regions.size() != initSize;
}

bool GeoPolRegion::intersections(const GeoLocEntry& loc, GeoPolRegionVec& regions, WorldOpt worldOpt) const
{
    const Marble::GeoDataCoordinates coord(qreal(loc.lon()), qreal(loc.lat()), 0, Marble::GeoDataCoordinates::Degree);

    return intersections(coord, regions, worldOpt);
}

bool GeoPolRegion::intersections(const Marble::GeoDataCoordinates& coord, GeoPolRegionVec& regions, WorldOpt worldOpt) const
{
    const int initSize = regions.size();

    if (!intersects(coord))
        return appendWorld(regions, initSize, worldOpt);

    // Add our name to the list if it isn't already present
    if (!name().isEmpty() && !regions.contains(this))
        if (level() >= 0)
            regions.append(this);

    // Check cache in case this track coord hits the same entry as the prior.
    if (!disableIntersectionCache) {
        decltype (m_lastHitNameCache) nameCache; // make a copy so we can release the mutex.
        {
            QMutexLocker lock(&m_cacheMutex);
            nameCache = m_lastHitNameCache;
        }
        for (const auto& cacheName : qAsConst(nameCache)) {
            if (const auto it = m_nameToRegionMap.find(cacheName); it != m_nameToRegionMap.end()) {
                if ((*it)->intersections(coord, regions, worldOpt))
                    return appendWorld(regions, initSize, worldOpt);
            }
        }
    }

    // Check children for interections, stopping at the first one we find.
    for (auto* it : m_nameToRegionMap) {
        if ((*it).intersections(coord, regions, worldOpt)) {
            if (!disableIntersectionCache) {
                QMutexLocker lock(&m_cacheMutex);

                if (!m_lastHitNameCache.contains(it->name())) {
                    while (m_lastHitNameCache.size() >= nameCacheSize)
                        m_lastHitNameCache.pop_back();
                    m_lastHitNameCache.push_front(it->name());
                }
            }
            return appendWorld(regions, initSize, worldOpt);  // we can stop looking: we assume non-overlapping children.
        }
    }

    return appendWorld(regions, initSize, worldOpt);   // don't count the world level in our return code
}

bool GeoPolRegion::intersections(const TrackItem& trackItem, GeoPolRegionVec& regions, WorldOpt worldOpt) const
{
    const int initSize = regions.size();

    const auto& lines = trackItem.trackSegLines();
    if (lines.isEmpty())
        return appendWorld(regions, initSize, worldOpt);

    // Only accumulate from the first and last point in the track, but always do both ends.
    // If one end or the other hasn't hit anything but international space, it could be very close to
    // a country boundary, so make a reasonable attempt with a few other points along the track.
    // Begin:
    if (!lines.first().isEmpty()) {
        const int earlyPoint = int(int64_t(lines.first().size()) * 5 / 100);

        // If first point didn't hit, try about 5% of the way into the segment.
        if (!intersections(lines.first().first(), regions))
            intersections(lines.first()[earlyPoint], regions);
    }

    // End:
    if (!lines.last().isEmpty()) {
        const int latePoint = int(int64_t(lines.last().size()) * 95 / 100);

        // If last point didn't hit, try about 95% of the way into the segment.
        if (!intersections(lines.last().last(), regions))
            intersections(lines.last()[latePoint], regions);
    }

    return appendWorld(regions, initSize, worldOpt);
}

bool GeoPolRegion::intersections(const WaypointItem& wpt, GeoPolRegionVec& regions, WorldOpt worldOpt) const
{
    return intersections(wpt.coords(), regions, worldOpt);
}

bool GeoPolRegion::intersections(const std::pair<double, double>& coords, GeoPolRegionVec& regions, WorldOpt worldOpt) const
{
    return intersections(Marble::GeoDataCoordinates(coords.first, coords.second, 0.0, Marble::GeoDataCoordinates::Degree),
                         regions, worldOpt);
}

bool GeoPolRegion::intersections(const ViewParams& viewParams, GeoPolRegionVec& regions, WorldOpt worldOpt) const
{
    return intersections(viewParams.center(), regions, worldOpt);
}

QSize GeoPolRegion::flagSize(int flagHeight, const QSize& margin)
{
    const int flagAspect = 180;  // average aspect: we'll shrink to fit this.
    const QSize flagBoxSize(flagHeight * flagAspect / 100, flagHeight);
    return flagBoxSize - margin;
}

