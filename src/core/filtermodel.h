/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FILTERMODEL_H
#define FILTERMODEL_H

#include <src/core/changetrackingmodel.h>
#include <src/core/modelmetadata.h>
#include <src/core/duplicablemodel.h>
#include <src/core/removablemodel.h>

// Track filter model
class FilterModel final :
        public ChangeTrackingModel,
        public DuplicableModel,
        public RemovableModel,
        public NamedItem,
        public ModelMetaData
{
    Q_OBJECT

public:
    enum {
        _First    = 0,
        Name      = _First,
        Icon      = Name,   // Icon in name column
        Query,
        _Count,
    };

    FilterModel(QObject *parent = nullptr);

    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    const Units& units(const QModelIndex& idx) const override;

    // *** begin Settings API
    using ChangeTrackingModel::save;
    void load(QSettings&) override;
    // *** end Settings API

    // *** begin Stream Save API
    quint32 streamMagic() const override;
    quint32 streamVersionMin() const override;
    quint32 streamVersionCurrent() const override;
    // *** end Stream Save API

    // *** begin ModelMetaData API
    static QString        mdName(ModelType);
    static bool           mdIsEditable(ModelType);
    static QString        mdTooltip(ModelType);
    static QString        mdWhatsthis(ModelType);
    static Qt::Alignment  mdAlignment(ModelType);
    static int            mdDataRole(ModelType);
    static const Units&   mdUnits(ModelType);
    // *** end ModelMetaData API

    // *** begin NamedItem API
    static name_t getItemNameStatic();
    // *** end NamedItem API

private:
    Qt::ItemFlags flags(const QModelIndex&) const override;
    Qt::DropActions supportedDropActions() const override { return Qt::MoveAction; }

    FilterModel(const FilterModel&) = delete;
    FilterModel& operator=(const FilterModel&) = delete;
};

#endif // FILTERMODEL_H
