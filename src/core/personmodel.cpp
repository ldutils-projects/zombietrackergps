/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QSortFilterProxyModel>

#include <cassert>
#include <src/util/roles.h>
#include <src/util/util.h>
#include <src/util/units.h>
#include <src/core/modelmetadata.inl.h>

#include "app.h"
#include "personmodel.h"
#include "personitem.h"

PersonModel::PersonModel(QObject *parent) :
    ContentAddrModel(new PersonItem(), PersonModel::Name, Util::RawDataRole, parent)
{
}

PersonItem* PersonModel::getItem(const QModelIndex &idx) const
{
    return static_cast<PersonItem*>(TreeModel::getItem(idx)); 
}

QVariant PersonModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (const QVariant val = ModelMetaData::headerData<PersonModel>(section, orientation, role); val.isValid())
        return val;

    if (role == Qt::DisplayRole)
        return mdName(section);

    return TreeModel::headerData(section, orientation, role);
}

void PersonModel::resetRows(const QModelIndexList& selection)
{
    for (const auto& idx : selection)
        for (ModelType mt : { PersonModel::Birthdate, PersonModel::MaxHR, PersonModel::FTP } )
            clearData(rowSibling(mt, idx), Util::RawDataRole);
}

int PersonModel::ageInDays(const QModelIndex& idx, const QDate& refDate) const
{
    if (const PersonItem* person = getItem(idx); person != nullptr)
        return person->ageInDays(refDate);

    return -1;
}

int PersonModel::maxBpm(const QModelIndex& idx, const QDate& refDate) const
{
    if (const PersonItem* person = getItem(idx); person != nullptr)
        return person->maxBpm(refDate);

    return -1; // unavailable; make something up.
}

Power_t PersonModel::ftp(const QModelIndex& idx, const QDate& refDate) const
{
    if (const PersonItem* person = getItem(idx); person != nullptr)
        return person->ftp(refDate);

    return { 0 }; // unavailable
}

Qt::ItemFlags PersonModel::flags(const QModelIndex& idx) const
{
    Qt::ItemFlags flags = TreeModel::flags(idx) | Qt::ItemIsDragEnabled | Qt::ItemIsEditable;

    if (!idx.isValid())
        flags |= Qt::ItemIsDropEnabled;

    return flags;
}

QString PersonModel::mdName(ModelType mt)
{
    switch (mt) {
    case PersonModel::Name:       return QObject::tr("Name");
    case PersonModel::Weight:     return QObject::tr("Weight");
    case PersonModel::Efficiency: return QObject::tr("Bio Efficiency");
    case PersonModel::Birthdate:  return QObject::tr("Birthdate");
    case PersonModel::MaxHR:      return QObject::tr("Max HR");
    case PersonModel::FTP:        return QObject::tr("FTP");
    case PersonModel::_Count:     break;
    }

    assert(0 && "Unknown PersonModel value");
    return "";
}

bool PersonModel::mdIsEditable(ModelType)
{
    return true;
}

// Return true if this data can be placed in a chart
bool PersonModel::mdIsChartable(ModelType)
{
    return false;
}

// tooltip for container column header
QString PersonModel::mdTooltip(ModelType mt)
{
    switch (mt) {
    case PersonModel::Name:       return QObject::tr("<i></i>Person name (athlete, passenger).");
    case PersonModel::Weight:     return QObject::tr("<i></i>Weight, used for power estimation.");
    case PersonModel::Efficiency: return QObject::tr("<i></i>Biomechanical efficiency, used for calorie estimation.");
    case PersonModel::Birthdate:  return QObject::tr("<i></i>Birth date, used for maximum heart rate and other age related factors.");
    case PersonModel::MaxHR:      return QObject::tr("<i></i>Maximum heart rate in BPM for this individual. If unset, will estimate as 220 - age. Set to 0 to unset.");
    case PersonModel::FTP:        return QObject::tr("<i></i>Functional threshold power.");
    case PersonModel::_Count:     break;
    }

    assert(0 && "Unknown PersonModel value");
    return "";
}

// tooltip for container column header
QString PersonModel::mdWhatsthis(ModelType mt)
{
    return mdTooltip(mt);  // just pass through the tooltip
}

Qt::Alignment PersonModel::mdAlignment(ModelType mt)
{
    switch (mt) {
    case PersonModel::Name:       return Qt::AlignLeft  | Qt::AlignVCenter;
    case PersonModel::Weight:     [[fallthrough]];
    case PersonModel::Efficiency: [[fallthrough]];
    case PersonModel::Birthdate:  [[fallthrough]];
    case PersonModel::MaxHR:      [[fallthrough]];
    case PersonModel::FTP:        return Qt::AlignRight | Qt::AlignVCenter;
    case PersonModel::_Count:     break;
    }

    assert(0 && "Unknown PersonModel value");
    return Qt::AlignLeft  | Qt::AlignVCenter;
}

const Units& PersonModel::mdUnits(ModelType mt)
{
    static const Units rawString(Format::String);
    static const Units rawInt(Format::Int);
    static const Units birthdayUnit(Format::DateyyyyMMdd);

    switch (mt) {
    case PersonModel::Name:       return rawString;
    case PersonModel::Weight:     return cfgData().unitsWeight;
    case PersonModel::Efficiency: return cfgData().unitsPct;
    case PersonModel::Birthdate:  return birthdayUnit;
    case PersonModel::MaxHR:      return rawInt;
    case PersonModel::FTP:        return cfgData().unitsPower;
    case PersonModel::_Count:     break;
    }

    assert(0 && "Unknown PersonModel value");
    return rawString;
}

int PersonModel::mdDataRole(ModelType)
{
    return Util::RawDataRole;
}

PersonModel& PersonModel::operator=(const PersonModel& rhs) 
{
    ContentAddrModel::operator=(rhs);
    return *this;
}
