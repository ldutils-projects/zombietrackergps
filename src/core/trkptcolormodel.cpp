/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <QRgb>
#include <QVector>

#include "pointmodel.h"
#include "trkptcolormodel.h"

TrkPtColorModel::TrkPtColorModel(QObject *parent) :
    ColorListModel(parent)
{
    // Create map from PointModel column to our row
    map.resize(PointModel::_Count);
    map.fill(-1);
    int pos = 0;
    for (int mt = PointModel::_First; mt < PointModel::_Count; ++mt)
        if (PointModel::mdIsChartable(mt))
            map[mt] = pos++;

    addMissing(); // set default colors
}

// since we skip some columns, map column ID to our row.
QColor TrkPtColorModel::operator[](ModelType mt) const
{
    if (map.at(mt) < 0)
        return { };

    return ColorListModel::operator[](map.at(mt));
}

void TrkPtColorModel::removeEntry(ModelType mt)
{
    if (const int rowOf = map.at(mt); rowOf >= 0) {
        // Remove row from parent model and our map
        ColorListModel::removeRow(rowOf);
        map.remove(mt);
        // Renumber other entries
        for (int row = mt; row < map.size(); ++row)
            --map[row];
    }
}

void TrkPtColorModel::addMissing()
{
    static const QVector<QRgb> defColors = {
        QRgb(0xff25beff), // Elapsed
        QRgb(0xff888844), // Lon
        QRgb(0xffaaaa66), // Lat
        QRgb(0xff00e165), // Ele
        QRgb(0xff916100), // Grade
        QRgb(0xff55aaff), // Temp
        QRgb(0xff0000c7), // Depth
        QRgb(0xff669999), // Speed
        QRgb(0xff994444), // HR
        QRgb(0xff9a4c73), // Cad
        QRgb(0xff778833), // Power
        QRgb(0xff939393), // Course
        QRgb(0xff7a7a7a), // Bearing
    };

    const int initRowCount = rowCount();

    int pos = 0;
    for (int mt = 0; mt < PointModel::_Count; ++mt) {
        if (PointModel::mdIsChartable(mt)) {
            if (pos++ >= initRowCount) {
                assert(map.at(mt) >= 0);
                assert(map.at(mt) < defColors.size() && "Update default colors");
                appendRow({ PointModel::mdName(mt), QColor(defColors[map.at(mt)]) });
            }
        }
    }

    // Trim any excess ones (if # types has shunk from save)
    while (rowCount() > map.size())
        removeRow(rowCount() - 1);
}
