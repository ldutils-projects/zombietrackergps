/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>

#include <QObject> // for tr()
#include <QFontMetrics>

#include <src/util/util.h>
#include <src/util/geomath.h>
#include <src/util/io.h>

#include "geolocheader.h"

// Write header to given file.
#ifdef GEOLOCHDR_WRITE
bool GeoLocFileHeader::write(FILE* out)
{
    return Io::write32(magic(), out) &&
           Io::write32(m_size, out);
}
#endif // GEOLOCHDR_WRITE

// Read header from given file.
bool GeoLocFileHeader::read(const uchar*& in)
{
    if (in == nullptr)
        return false;

    const auto inMagic = uint32_t((in[0] << 24) | (in[1] << 16) | (in[2] << 8) | (in[3] << 0));
    in += sizeof(uint32_t);

    if (inMagic != magic())
        return false;

    m_size = uint32_t((in[0] << 24) | (in[1] << 16) | (in[2] << 8) | (in[3] << 0));
    in += sizeof(uint32_t);

    return true;
}

void GeoLocHdr::setCompression(int32_t start, int32_t end, int32_t lineDiff)
{
    if (abs(lineDiff) < GeoLocHdrS::maxRelDiff()) {
        setUseLongHdr(false);
        setShare(start, end);
        setRelRec(lineDiff);
    } else if (abs(lineDiff) < GeoLocHdrL::maxRelDiff()) {
        setUseLongHdr(true);
        setShare(start, end);
        setRelRec(lineDiff);
    } else {
        setUseLongHdr(false);
        setShare(start, end);
    }
}

QString GeoLocEntry::featureName(Feature feature)
{
    switch (feature) {
    case Feature::None:     return QObject::tr("None");
    case Feature::Region:   return QObject::tr("Regions");
    case Feature::City:     return QObject::tr("Cities");
    case Feature::Park:     return QObject::tr("Parks");
    case Feature::Forest:   return QObject::tr("Forests");
    case Feature::Mountain: return QObject::tr("Mountains");
    case Feature::Water:    return QObject::tr("Water");
    case Feature::Undersea: return QObject::tr("Undersea");
    case Feature::_Count:   assert(0); break;
    }

    return QObject::tr("None");
}

const char* GeoLocEntry::featureIcon(Feature feature)
{
    switch (feature) {
    case Feature::None:     return nullptr;
    case Feature::Region:   return ":art/tags/Misc/Map.svg";
    case Feature::City:     return ":art/tags/Misc/City.svg";
    case Feature::Park:     return ":art/tags/Misc/Park.svg";
    case Feature::Forest:   return ":art/tags/Misc/Tree-01.svg";
    case Feature::Mountain: return ":art/tags/Misc/Mountains.svg";
    case Feature::Water:    return ":art/tags/Misc/Lake.svg";
    case Feature::Undersea: return ":art/tags/Misc/Lake.svg";
    case Feature::_Count:   assert(0); break;
    }

    return nullptr;
}

QString GeoLocEntry::tooltip(Feature feature)
{
    switch (feature) {
    case Feature::None:     return QObject::tr("None");
    case Feature::Region:   return QObject::tr("Countries, states, provinces");
    case Feature::City:     return QObject::tr("Cities, towns, villiages");
    case Feature::Park:     return QObject::tr("Parks");
    case Feature::Forest:   return QObject::tr("Forests, bogs");
    case Feature::Mountain: return QObject::tr("Mountains, hills");
    case Feature::Water:    return QObject::tr("Lakes, ponds, rivers, streams");
    case Feature::Undersea: return QObject::tr("Reefs, undersea trenches, etc");
    case Feature::_Count:   assert(0); break;
    }

    return QObject::tr("None");
}

QString GeoLocEntry::whatsthis(Feature feature)
{
    return tooltip(feature);
}

int GeoLocEntry::longestFeatureName(const QFontMetrics& fontMetric)
{
    int longest = 0;
    for (Feature f = Feature::_First; f < Feature::_Count; Util::inc(f))
        longest = std::max(longest, fontMetric.size(Qt::TextSingleLine,
                                                    featureName(f)).width());

    return longest;
}

bool GeoLocEntry::read(const uchar* in, uint32_t record)
{
    if (in == nullptr)
        return false;

    const auto* inEntries = reinterpret_cast<const GeoLocEntry*>(in + GeoLocFileHeader::headerSize);
    memcpy(this, inEntries + record, sizeof(*this));

    return true;
}

double GeoLocEntry::greatCircleDist(const GeoLocEntry& lhs, const GeoLocEntry& rhs)
{
    return GeoMath::greatCircleDist(lhs.latRad(), lhs.lonRad(), rhs.latRad(), rhs.lonRad());
}

double GeoLocEntry::greatCircleDist(double rhsLatRad, double rhsLonRad) const
{
    return GeoMath::greatCircleDist(latRad(), lonRad(), rhsLatRad, rhsLonRad);
}
