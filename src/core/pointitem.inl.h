/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef POINTITEM_INL_H
#define POINTITEM_INL_H

#include "pointitem.h"

inline float PointItem::gradeSinAtan() const
{
    static_assert(sizeof(m_grade) == 2); // revisit if this changes

    // Already calculated
    if (!m_sinAtanTableInit)
        calcSinAtans();

    const int pos = int(m_grade) + -int(std::numeric_limits<decltype(m_grade)>::min());

    return m_sinAtanTable.at(pos) * (1.0 / (badGradeI-1));
}

inline PointItem::Power_t PointItem::power(const PowerData& pd, const PointItem& nextPt) const
{
    if (hasPower(Measured))
        return power();

    if (pd.isZero())
        return 0.0;

    const auto  v       = float(speed(&nextPt));
    const Temp_t temp   = hasAtemp() ? atemp() : 15.0;              // use atemp, or default 15C
    const auto  density = float(pd.density(ele(), temp));           // medium density

    return std::max(v*(0.5f * density * pd.CdA * v*v +              // drag resistance
                       pd.mass * pd.g * (gradeSinAtan() + pd.rr)),  // climb and rolling resistance
                    0.0f);
}

inline Grade_t PointItem::grade(Ele_t rise, Dist_t run)
{
    return { Ele_t::base_type(rise) / Dist_t::base_type(run) };
}

inline Grade_t PointItem::grade(const PointItem& next, bool flt) const
{
    return grade(vert(next, flt), length(next));
}

inline Ele_t PointItem::riseForGrade(Dist_t dist, Grade_t grade)
{
    return { Dist_t::base_type(dist) * Grade_t::base_type(grade) };
}


inline QDataStream& operator<<(QDataStream& stream, const PointItem::AuxData& ad)
{
    return stream << ad.m_name
                  << ad.m_comment
                  << ad.m_desc
                  << ad.m_symbol
                  << ad.m_type
                  << ad.m_depth
                  << ad.m_wtemp
                  << ad.m_course
                  << ad.m_bearing;
}

inline QDataStream& operator>>(QDataStream& stream, PointItem::AuxData& ad)
{
    return stream >> ad.m_name
                  >> ad.m_comment
                  >> ad.m_desc
                  >> ad.m_symbol
                  >> ad.m_type
                  >> ad.m_depth
                  >> ad.m_wtemp
                  >> ad.m_course
                  >> ad.m_bearing;
}

inline uint qHash(const PointItem::AuxData& ad, uint seed)
{
    seed = qHash(std::make_pair(seed, ad.m_name));
    seed = qHash(std::make_pair(seed, ad.m_comment));
    seed = qHash(std::make_pair(seed, ad.m_desc));
    seed = qHash(std::make_pair(seed, ad.m_symbol));
    seed = qHash(std::make_pair(seed, ad.m_type));
    seed = qHash(std::make_pair(seed, ad.m_depth));
    seed = qHash(std::make_pair(seed, ad.m_wtemp));
    seed = qHash(std::make_pair(seed, ad.m_course));
    seed = qHash(std::make_pair(seed, ad.m_bearing));

    return seed;
}

inline QDataStream& operator<<(QDataStream& stream, const PointItem& pt)
{
    const bool hasAuxData = pt.hasAuxData();

    // Distance, etc are computed from other values: no need to save/load
    stream << pt.m_time
           << pt.m_lon
           << pt.m_lat
           << pt.m_ele
           << pt.m_speed
           << pt.m_power
           << pt.m_atemp
           << pt.m_hr
           << pt.m_cad
           << hasAuxData; // flag for whether to load AuxData on read

    // Save aux data if there is any.
    if (hasAuxData)
        stream << *pt.m_auxData;

    return stream;
}

inline QDataStream& operator>>(QDataStream& stream, PointItem& pt)
{
    bool hasAuxData = false;

    // Distance, etc are computed from other values: no need to save/load
    stream >> pt.m_time
           >> pt.m_lon
           >> pt.m_lat
           >> pt.m_ele
           >> pt.m_speed
           >> pt.m_power
           >> pt.m_atemp
           >> pt.m_hr
           >> pt.m_cad
           >> hasAuxData;

    // Load AuxData if it was saved
    if (hasAuxData)
        stream >> pt.getAuxData();

    return stream;
}

#endif // POINTITEM_INL_H
