/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef POINTMODEL_H
#define POINTMODEL_H

#include <cassert>
#include <limits>
#include <array>
#include <tuple>
#include <bitset>
#include <functional>
#include <QList>
#include <QVector>
#include <QMap>
#include <QModelIndexList>

#include <src/core/modelmetadata.h>
#include <src/util/roles.h>
#include <src/util/util.h>
#include <src/core/removablemodel.h>

#include "src/core/mergeablemodel.h"
#include "src/core/simplifiablemodel.h"
#include "src/core/speededitmodel.h"
#include "pointitem.h"
#include "powerdata.h"

class QSortFilterProxyModel;
class Units;
class QDataStream;
class TrackItem;
class CfgData;

namespace Marble {
class GeoDataLatLonBox;
class GeoDataCoordinates;
} // namespace Marble

// Each element in the vector tracks a new point.  Each element in the list tracks
// a separate route segment.  We want the inner container to be a vector to get
// effecient contiguous storage.
class PointModel final :
        public MergeableModel,
        public RemovableModel,
        public SimplifiableModel,
        public SpeedEditModel,
        public ModelMetaData,
        public QVector<QVector<PointItem>>
{
    Q_OBJECT

public:
    // See PointAccum if adding data, to handle point merging.
    enum {
        _First,
        Index = _First,
        Time,
        Elapsed,
        Lon,
        Lat,
        Ele,
        Length,
        Distance,
        Vert,
        Grade,
        Duration,
        Temp,
        Depth,
        Speed,
        AccelRemoved,
        Hr = AccelRemoved,
        Cad,
        Power,
        Course,
        Bearing,
        Name,      // string data, for route points
        Comment,   // ...
        Desc,      // ...
        Symbol,    // ...
        Type,      // ...
        _Count
    };

    PointModel();
    PointModel(TrackItem*);

    void filter();
    // void update start index cache, for data() performance
    void updateStartIdx();

    using TreeModel::units;
    const Units& units(const QModelIndex& idx) const override;

    using container_type       = QVector<QVector<PointItem>>;
    using segment_type         = container_type::value_type;
    using index_range          = std::tuple<QModelIndex, QModelIndex, PointItem::Grade_t>;

    // *** begin ModelMetaData API
    static QString        mdName(ModelType);
    static bool           mdIsEditable(ModelType);
    static QString        mdTooltip(ModelType);
    static QString        mdWhatsthis(ModelType);
    static Qt::Alignment  mdAlignment(ModelType);
    static const Units&   mdUnits(ModelType);
    static bool           mdIsChartable(ModelType);
    // *** end ModelMetaData API

    // *** begin NamedItem API
    const name_t& getItemName() const override;
    const name_t& getItemName(const QModelIndex&) const override;
    static const name_t& getItemNameStatic();
    // *** end NamedItem API

    std::tuple<Lat_t, Lon_t, Lat_t, Lon_t, bool> bounds(const QModelIndexList&) const;
    std::tuple<Lat_t, Lon_t, Lat_t, Lon_t, bool> bounds(PointItem::Flags) const;
    Marble::GeoDataLatLonBox boundsBox(const QModelIndexList&) const;
    Marble::GeoDataLatLonBox boundsBox(PointItem::Flags) const;

    bool selectPointsWithin(const Marble::GeoDataLatLonBox&);

    // Something of a kludge, just for the TrackMap drawing function performance.. TODO: remove
    template <class T> [[nodiscard]] bool is(const T&, const QModelIndex& idx, const PointItem&) const;

    template <class T> void unselectAll(const T&);
    template <class T> void select(const T&, const QModelIndex&, bool newState);
    template <class T> void resetHasData(const T&);
    template <class T> void accumulateHasData(const T&, const PointItem& pt, const PointItem* nextPt);
    template <class T> void accumulateHasData(const T&, std::bitset<_Count>&) const;
    template <class T> [[nodiscard]] QModelIndex areaExtreme(const T&) const;
    template <class T> void invalidateFilter(const T&);

    template <class T> void reverse(const T&, const QDateTime& beginDate, const QDateTime& endDate);

    // Select points in list
    void select(const QModelIndexList&);

    // Detect climbs in the given range (whole model on invalid indexes)
    template <class T> [[nodiscard]] QVector<index_range> climbs(const T&) const;

    using TreeModel::as;
    template <class T> [[nodiscard]] T as(const QModelIndex&) const;  // convert a point to given type

    // Indexes for selected points
    QModelIndexList selectedIndexes(PointItem::Flags = PointItem::Flags::Select) const;

    bool hasData(ModelType mt) const { return m_hasData.test(size_t(mt)); }

    enum Extreme { High, Low, Avg };
    template <class T> std::tuple<qreal, int> extremeVal(T&, ModelType, Extreme) const;

    inline QVariant data(const PointItem&, ModelType col, const QModelIndex&,
                         int role = Qt::DisplayRole, bool filtered = true) const;

    // *** begin QAbstractItemModel API
    using TreeModel::data;
    [[nodiscard]] QVariant data(const QModelIndex& idx, int role = Qt::DisplayRole) const override;

    [[nodiscard]] QVariant headerData(int section, Qt::Orientation orientation,
                                      int role = Qt::DisplayRole) const override;
    [[nodiscard]] QModelIndex index(int row, int column,
                                    const QModelIndex& parent = QModelIndex()) const override;
    [[nodiscard]] QModelIndex parent(const QModelIndex& idx) const override;

    [[nodiscard]] int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    [[nodiscard]] int columnCount(const QModelIndex& /*parent*/ = QModelIndex()) const override { return _Count; }

    [[nodiscard]] Qt::ItemFlags flags(const QModelIndex&) const override;
    using TreeModel::setData;
    bool multiSet(const QModelIndexList&, const QVariant& value, int role) override;
    bool setData(const QModelIndex&, const QVariant& value,
                 int role = Qt::DisplayRole) override;
    bool appendPoint(const PointItem&, const QModelIndex& parent);
    bool setHeaderData(int section, Qt::Orientation orientation,
                       const QVariant& value, int role = Qt::DisplayRole) override;
    bool insertColumns(int /*position*/, int /*columns*/,
                       const QModelIndex& = QModelIndex()) override { return false; }
    bool removeColumns(int /*position*/, int /*columns*/,
                       const QModelIndex& = QModelIndex()) override { return false; }
    bool insertRows(int position, int rows,
                    const QModelIndex& parent = QModelIndex()) override;
    bool removeRows(int position, int rows,
                    const QModelIndex& parent = QModelIndex()) override;
    using QAbstractItemModel::insertRow;
    // *** end QAbstractItemModel API

    // Insert interpolated point for given timestamp
    QModelIndex addInterpolatedPoint(PointItem::Lat_t, PointItem::Lon_t,
                                     const QModelIndex& parent, int beforeRow);

    // Remove segments with no data points
    void removeEmptySegments();

    // Merge selected points or segments
    std::tuple<QModelIndex, bool> mergeSegments(const QModelIndexList&);
    std::tuple<QModelIndex, bool> mergePoints(const QModelIndexList&);

    // Split segments at selected points.
    void splitSegments(const QModelIndexList& splitAt);

    [[nodiscard]] QVariant siblingData(int column, const QModelIndex& idx, int role = Qt::DisplayRole) const;

    void setPowerData(const QModelIndex& tagIdx, const QModelIndex& personIdx);
    [[nodiscard]] const PowerData& powerData() const { return m_powerData; }
    [[nodiscard]] const QDateTime& trackEndTime() const;
    [[nodiscard]] qreal            trackTotalDistance() const;
    [[nodiscard]] int              trackTotalPoints() const;
    [[nodiscard]] QModelIndex      closestPoint(PointItem::Dist_t distance) const; // find closest idx to this distance from track start
    [[nodiscard]] QModelIndex      modelIndexFor(uint idx, ModelType col = 0) const; // model index for our internal index integer
    [[nodiscard]] QModelIndex      modelIndexFor(const segment_type&) const;
    [[nodiscard]] QModelIndex      modelIndexFor(const segment_type&, const PointItem&, ModelType col = 0) const;
    [[nodiscard]] static bool      isSegment(const QModelIndex& idx) { return idx.isValid() && qintptr(idx.internalId()) < 0; }
    [[nodiscard]] static bool      isPoint(const QModelIndex& idx) { return idx.isValid() && !isSegment(idx);}
    [[nodiscard]] static bool      allSegments(const QModelIndexList&);
    [[nodiscard]] static bool      allPoints(const QModelIndexList&);
    [[nodiscard]] static bool      sameSegment(const QModelIndexList&);

    // Shortcut to get to data if we have a PointItem already.
    template <typename T> [[nodiscard]] T ptData(const PointItem&, ModelType, int row, bool flt = true) const;
    template <typename T> [[nodiscard]] T ptData(const PointItem&, const PointItem* nextPt, ModelType, int row, bool flt = true) const;

    // Interpolate value in column 'mt' for given distance
    template <class T = QVariant> [[nodiscard]] T interpolate(qreal dist, ModelType mt, T invalid = QVariant()) const;

    void sortTrkSegs();   // sort segments by first point times
    void sortItems(bool force = false); // sort points in all segments
    bool speedToTimestamps(const QModelIndexList& selection, const QVariant&); // upon speed change, fix subsequent timestamps

    void clear() override { container_type::clear(); } // pass model clear to the segment vector

    // Converts lon/lat to projected X/Y
    [[nodiscard]] static std::pair<PointItem::Lon_t, PointItem::Lat_t> projectPt(const PointItem* pt);

    // Calculate data for ranges of points: either all, or selected.  This is used by the TrackItem.
    // The map is indexed by the TrackModel column.
    using ValueMap_t = QMap<ModelType, QVariant>;

    [[nodiscard]] ValueMap_t calcPowerData(PointItem::Dur_t movingTimeMs, PointItem::Flags = PointItem::Flags::NoFlags,
                                           const QModelIndex& beginIdx = QModelIndex(),
                                           const QModelIndex& endIdx = QModelIndex()) const;
    [[nodiscard]] ValueMap_t calcTrackData(PointItem::Flags = PointItem::Flags::NoFlags,
                                           const QModelIndex& beginIdx = QModelIndex(),
                                           const QModelIndex& endIdx = QModelIndex()) const;
    [[nodiscard]] ValueMap_t updateTrackData(PointItem::Flags = PointItem::Flags::NoFlags,
                                             const QModelIndex& beginIdx = QModelIndex(),
                                             const QModelIndex& endIdx = QModelIndex()); // refilter/index + calcTrackData

    [[nodiscard]] const PointItem* getItem(const QModelIndex& idx) const {
        return const_cast<const PointItem*>(const_cast<PointModel*>(this)->getItem(idx));
    }

    [[nodiscard]] PointItem* getItem(const QModelIndex& idx) {
        if (!idx.isValid() || isSegment(idx) || idx.model() != this)
            return nullptr;

        return &(*this)[int(idx.internalId())][idx.row()];
    }

private:
    friend class UndoPoint;
    friend QDataStream& operator>>(QDataStream&, PointModel&);

    // Adapter to iterate over subset of segments
    class SegRange_t {
    public:
        SegRange_t(const PointModel&, const QModelIndex& beginIdx, const QModelIndex& endIdx);
        [[nodiscard]] const_iterator begin() const { return m_begin; }
        [[nodiscard]] const_iterator end() const { return m_end; }
    private:
        const_iterator m_begin, m_end;
    };

    // Adapter to iterate over subset of points in a segment
    class PtRange_t {
    public:
        PtRange_t(const PointModel&, const segment_type&, const QModelIndex& beginIdx, const QModelIndex& endIdx);
        [[nodiscard]] segment_type::const_iterator begin() const { return m_begin; }
        [[nodiscard]] segment_type::const_iterator end() const { return m_end; }
    private:
        segment_type::const_iterator m_begin, m_end;
    };

    // Acceleration structure for rapidly testing which points are selected. We can generate
    // this from a QModelIndexList.
    struct FastSelection_t {
        FastSelection_t(const PointModel&, const QModelIndexList&);

        // a little pack-inefficient compared to a bitset, but we'd rather have the speed.
        QVector<bool>          m_isSegSelected; // one entry per segment
        QVector<QVector<bool>> m_isPtSelected;  // outer vector = segments, inner = points
    };

    // This can be pushed on the stack to handle changes to the entire model, such as
    // simplifying or reversing data. It will handle the appropriate init and cleanup
    // no matter how we exit the stack frame.
    struct reset_t {
        reset_t(PointModel& pm, bool enable = true);
        ~reset_t();

        PointModel& m_pm;
        bool m_enable;
    };

    [[nodiscard]] const PointItem* firstPointInTrack() const;
    [[nodiscard]] const PointItem* nextPointInSeg(const QModelIndex&) const;
    [[nodiscard]] const PointItem* nextPointInSeg(const PointItem&) const;
    [[nodiscard]] const PointItem* prevPointInSeg(const QModelIndex&) const;

    void processDataChanged();
    void emitDirtyStateChanged(bool dirty) override;

    void updateHasData();
    inline void unselectAll();
    inline void select(const QModelIndex&, bool newState);

    [[nodiscard]] int segmentStart(const QModelIndex& idx) const { return isSegment(idx) ? 0 : m_startIdxCache[int(idx.internalId())]; }
    [[nodiscard]] int pointIndex(const QModelIndex& idx) const { return segmentStart(idx) + idx.row(); }
    [[nodiscard]] const_iterator segmentOf(const QModelIndex& idx) const { return begin() + int(idx.internalId()); }

    void saveItem(const QModelIndex&, QDataStream&, const TreeModel&) const override;
    void loadItem(const QModelIndex&, QDataStream&, TreeModel&) override;

    // *** begin MergeableModel API
    std::tuple<QModelIndex, bool> merge(const QModelIndexList&, const QVariant& userData) override;
    // *** end MergeableModel API

    // See comments for PointItem::interpolate()
    [[nodiscard]] PointItem interpolate(const QModelIndex& idx0, const QModelIndex& idx1, QDateTime&) const;

    // Helper for simplification
    Count simplifyPoints(const QModelIndexList&, Mode mode,
                         const std::function<bool(const PointItem&, const PointItem&, const PointItem&)>&);

    // *** begin SimplifiableModel API
    Count simplifyTime(const QModelIndexList&, const Params&) override;
    Count simplifyDist(const QModelIndexList&, const Params&) override;
    Count simplifyAdaptive(const QModelIndexList&, const Params&) override;
    // *** end SimplifiableModel API

    // *** begin SpeedEditModel API
    void unsetSpeed(const QModelIndexList&) override;
    void unsetSpeed() override;
    // *** end SpeedEditModel API

    // Workaround for: https://bugreports.qt.io/browse/QTBUG-44962
    [[nodiscard]] bool wrongModel(const QModelIndex& idx) const {
        return idx.model() != this && idx.model() != nullptr;
    }

    // *** begin ChangeTrackingModel API
    void preUndoSet(QVariantMap&) override;
    void postUndoSet(QVariantMap&) override;
    // *** end ChangeTrackingModel API

    // Obtain first item whose distance() is not less than 'dist'
    [[nodiscard]] std::tuple<const PointItem*, int, const PointItem*, int> getItemBound(qreal dist) const;

    [[nodiscard]] static bool setsColumn(const QModelIndexList&, int column);
    [[nodiscard]] static bool setsColumn(const QModelIndex&, int column);

    const QModelIndex& verify(const QModelIndex& idx) const {
        assert(idx.model() == nullptr || idx.model() == this);
        return idx;
    }

    // Determine average time spacing of sample points
    [[nodiscard]] static qreal avgTimeSpacing(const value_type&);

    // Call update() on the TrackItem
    void updateTrackItem();

    // Convolution for single point single point
    template <typename T>
    inline T convolve(T PointItem::*, value_type& trkSeg, int i, int fHalf);

    // filter single vector of points, for all members in "in", to members in "out".
    inline void filter(value_type&);

    static void calcDistValues(value_type&, qreal& accumDistance);
    void calcPointValues(value_type&, const PointItem* firstPt);

    [[nodiscard]] int totalSegmentPoints() const;  // total point count in all segments

    [[nodiscard]] QString tooltip(const QModelIndex& idx) const; // tooltip for singular selections
    [[nodiscard]] QString multiPointTooltip() const;             // tooltip to average/min/max things for multi-point selection

    [[nodiscard]] bool isTextField(ModelType) const;

    static const constexpr Dur_t mStonS = Dur_t(1000000);  // mS to nS
    static const PointItem m_invalidPoint;

    // Track nesting on multiSet/setData to avoid redundant init/cleanup
    struct DataChangedNest {
    public:
        DataChangedNest(PointModel& pm, const QModelIndex&);
        DataChangedNest(PointModel& pm, const QModelIndexList&);
        DataChangedNest(PointModel& pm, bool editsSpeed);
        ~DataChangedNest();
    private:
        PointModel& m_pm;
    };

    int                 m_eleFilterSizeCache = -1; // for efficiency: avoid needless re-filters

    PowerData           m_powerData;          // rolling resistances, etc as fn of tag and person
    QVector<uint>       m_startIdxCache;      // cache of index start values for each segment
    std::bitset<_Count> m_hasData;            // bitset of PointModel columns present in any contained point
    mutable QModelIndex m_areaExtreme;        // index of area extreme point.
    bool                m_timeDirty = false;  // time changed: needs a resort
    int                 m_selectCount = 0;    // used by tooltip creation
    int                 m_setDataNesting = 0; // nesting to avoid redundant processDataChanged() calls

    TrackItem*          m_trackItem;          // owner track
};

// Serialization
extern QDataStream& operator<<(QDataStream&, const PointModel&);
extern QDataStream& operator>>(QDataStream&, PointModel&);

// Hash
uint qHash(const PointModel&, uint seed = 0x76920aba);

#endif // POINTMODEL_H
