/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SIMPLIFIABLEMODEL_H
#define SIMPLIFIABLEMODEL_H

#include <QtGlobal>
#include <QModelIndexList>

#include <src/util/nameditem.h>

#include "src/fwddecl.h"

class QDataStream;

// Models that support simplification of entries
class SimplifiableModel :
        virtual public NamedItemInterface
{
public:
    // Ways we can simplify the track
    enum class SimplifyType {
        Invalid,
        Adaptive, // automatic based on threshold distance from interp
        ByTime,   // remove if < delta time
        ByDist,   // remove if < delta dist
    };

    enum class Mode {
        Preview,
        Execute,
    };

    // Descriptor for the simplification operation
    class Params {
    public:
        Params(); // invalid params

        // Construct by time or distance
        static Params Adaptive(Dist_t, Mode = Mode::Execute);
        static Params Time(Dur_t, Mode = Mode::Execute);
        static Params Dist(Dist_t, Mode = Mode::Execute);

        SimplifyType m_type;
        Mode         m_mode;

        union { // simplification parameters
            Dist_t  m_dist = 0.0f; // for Adaptive and ByDist modes
            Dur_t   m_time;        // for ByTime mode
        };

    private:
        Params(SimplifyType, Dist_t, Mode = Mode::Execute);
        Params(SimplifyType, Dur_t, Mode = Mode::Execute);
    };

    SimplifiableModel();
    virtual ~SimplifiableModel();

    // Track count, before points, after points
    struct Count {
        Count(int tracks = 0) : m_tracks(tracks) { }
        int m_tracks;
        int m_beforePoints = 0;
        int m_afterPoints = 0;
    };

    // Farms out to one of the specific methods below
    [[nodiscard]] virtual Count simplify(const QModelIndexList&, const Params&);

    // Per-mode simplification functions for subclasses to provide
    [[nodiscard]] virtual Count simplifyAdaptive(const QModelIndexList&, const Params&) = 0;
    [[nodiscard]] virtual Count simplifyTime(const QModelIndexList&, const Params&) = 0;
    [[nodiscard]] virtual Count simplifyDist(const QModelIndexList&, const Params&) = 0;
};

SimplifiableModel::Count& operator+=(SimplifiableModel::Count& lhs, const SimplifiableModel::Count& rhs);

Q_DECLARE_METATYPE(SimplifiableModel::SimplifyType)
QDataStream& operator<<(QDataStream&, const SimplifiableModel::SimplifyType&);
QDataStream& operator>>(QDataStream&, SimplifiableModel::SimplifyType&);

#endif // SIMPLIFIABLEMODEL_H
