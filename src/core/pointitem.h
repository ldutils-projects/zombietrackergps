/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef POINTITEM_H
#define POINTITEM_H

#include <limits>
#include <type_traits>
#include <array>
#include <memory> // for unique_ptr

#include <QDateTime>
#include <QString>
#include <marble/GeoDataCoordinates.h>

#include <src/core/modelmetadata.h>
#include <src/util/math.h>
#include <src/util/geomath.h>

#include "src/strongops.h"
#include "powerdata.h"

class QDataStream;
class PointModel;
class Units;

namespace V1000 {
struct PointItem;
} // namespace V1000

namespace V1001 {
struct PointItem;
} // namespace V1001

class CfgData;

#pragma pack(push)
#pragma pack(4)

// Single track point, and any associated data
class PointItem final
{
public:
    // Computed data is after our own filtering
    enum {
        Measured = (1<<0),
        Computed = (1<<1),
        Either   = (Measured | Computed),
    };

    using Hr_t    = ::Hr_t;
    using Cad_t   = ::Cad_t;
    using Power_t = ::Power_t;
    using Speed_t = ::Speed_t;
    using Grade_t = ::Grade_t;  // external: stored as short internally to save space
    using Dist_t  = ::Dist_t;
    using Ele_t   = ::Ele_t;
    using Lat_t   = ::Lat_t;
    using Lon_t   = ::Lon_t;
    using Dur_t   = ::Dur_t;
    using Elaps_t = ::Elaps_t;
    using Temp_t  = ::Temp_t;

    PointItem(Lon_t lon = Lon_t(badLon), Lat_t lat = Lat_t(badLat), const QDateTime& time = QDateTime(),
              Ele_t ele = Ele_t(badEle), Hr_t hr = badHr, Cad_t cad = badCad, Power_t power = badPower) :
        m_time(time),
        m_lon(lon),    m_lat(lat),
        m_ele(ele), m_fltEle(fNaN),
        m_dist(fNaN),
        m_speed(NaN), m_power(power),
        m_grade(badGradeI), m_atemp(badATempI),
        m_flags(Flags::NoFlags),
        m_hr(hr), m_cad(cad)
    { }

    PointItem(const PointItem&);            // copy construct
    PointItem& operator=(const PointItem&); // assign
    PointItem(const V1000::PointItem&);     // conversion from old format
    PointItem(const V1001::PointItem&);     // conversion from old format

    void clear();

    // Point accessor functions.  'flt' parameters indicate whether to return filtered, or
    // unfiltered data.
    [[nodiscard]] Lon_t     lon()                   const { return m_lon; }
    [[nodiscard]] Lat_t     lat()                   const { return m_lat; }
    [[nodiscard]] Ele_t     ele(bool flt = true)    const { return hasFltEle() && flt ? m_fltEle : m_ele; }
    [[nodiscard]] Lon_t     lonRad()                const { return Math::toRad(lon()); }
    [[nodiscard]] Lat_t     latRad()                const { return Math::toRad(lat()); }
    [[nodiscard]] Dist_t    distance()              const { return { m_dist }; }   // total distance within the track
    [[nodiscard]] Grade_t   grade()                 const { return m_grade * (1.0 / (badGradeI-1)); }
    [[nodiscard]] const QDateTime& time()           const { return m_time; }
    [[nodiscard]] Temp_t    atemp()                 const { return Temp_t::base_type(m_atemp) * 0.01f; }
    [[nodiscard]] Hr_t      hr()                    const { return m_hr; }
    [[nodiscard]] Cad_t     cad()                   const { return m_cad; }
    [[nodiscard]] Power_t   power()                 const { return m_power; }
    [[nodiscard]] QString   name()                  const { return hasAuxData() ? m_auxData->m_name : QString(); }
    [[nodiscard]] QString   comment()               const { return hasAuxData() ? m_auxData->m_comment : QString(); }
    [[nodiscard]] QString   desc()                  const { return hasAuxData() ? m_auxData->m_desc : QString(); }
    [[nodiscard]] QString   symbol()                const { return hasAuxData() ? m_auxData->m_symbol : QString(); }
    [[nodiscard]] QString   type()                  const { return hasAuxData() ? m_auxData->m_type : QString(); }
    [[nodiscard]] Temp_t    wtemp()                 const { return hasAuxData() ? m_auxData->wtemp() : Temp_t(0); }
    [[nodiscard]] float     depth()                 const { return hasAuxData() ? m_auxData->depth() : 0.0f; }
    [[nodiscard]] float     course()                const { return hasAuxData() ? m_auxData->course() : 0.0f; }
    [[nodiscard]] float     bearing()               const { return hasAuxData() ? m_auxData->bearing() : 0.0f; }

    // These accessors require a next-point to calculate relative to
    [[nodiscard]] Dur_t    duration(const QDateTime& next) const { return Dur_t(time().msecsTo(next)); }
    [[nodiscard]] Dur_t    duration(const PointItem& next) const { return duration(next.time()); }
    [[nodiscard]] Elaps_t  elapsed(const QDateTime& start) const { return start.msecsTo(time()); }
    [[nodiscard]] Elaps_t  elapsed(const PointItem& start) const { return elapsed(start.time()); }
    [[nodiscard]] Ele_t    vert(const PointItem& next, bool flt = true) const { return next.ele(flt) - ele(flt); }
    [[nodiscard]] Dist_t   length(const PointItem& next)   const { return next.distance() - distance(); }
    [[nodiscard]] Speed_t  speed(const PointItem* next = nullptr) const {
        return hasSpeed(Measured, next) ? m_speed :
               (next == nullptr) ? Speed_t(0.0f) :
               (length(*next) * 1000.0f / duration(*next));
    }

    // Grade to given other point
    [[nodiscard]] Grade_t grade(const PointItem& next, bool flt = true) const;

    [[nodiscard]] inline float gradeSinAtan() const;
    [[nodiscard]] inline Power_t power(const PowerData& pd, const PointItem& nextPt) const;

    [[nodiscard]] static bool isMoving(Speed_t speed) { return speed > stoppedThresholdSpeed; }
    [[nodiscard]] bool isMoving(const PointItem* next) const { return isMoving(speed(next)); }

    // Queries for whether some piece of data is set and valid
    [[nodiscard]] bool hasLoc()      const    { return !std::isnan(m_lon) && !std::isnan(m_lat); }
    [[nodiscard]] bool hasTime()     const    { return m_time.isValid(); }
    [[nodiscard]] bool hasEle()      const    { return !std::isnan(m_ele);  }
    [[nodiscard]] bool hasFltEle()   const    { return !std::isnan(m_fltEle); }
    [[nodiscard]] bool hasDistance() const    { return !std::isnan(m_dist); }
    [[nodiscard]] bool hasGrade()    const    { return m_grade != badGradeI; }
    [[nodiscard]] bool hasElapsed()  const    { return hasTime(); }
    [[nodiscard]] bool hasSpeed(int m, const PointItem* next = nullptr)  const {
        return (bool(m & Measured) && !std::isnan(m_speed)) ||
               (bool(m & Computed) && hasLength(next) && hasDuration(next));
    }
    [[nodiscard]] bool hasAtemp()   const   { return m_atemp != badATempI;     }
    [[nodiscard]] bool hasWtemp()   const   { return hasAuxData() && m_auxData->hasWtemp(); }
    [[nodiscard]] bool hasDepth()   const   { return hasAuxData() && m_auxData->hasDepth(); }
    [[nodiscard]] bool hasCourse()  const   { return hasAuxData() && m_auxData->hasCourse(); }
    [[nodiscard]] bool hasBearing() const   { return hasAuxData() && m_auxData->hasBearing(); }
    [[nodiscard]] bool hasHr()      const   { return m_hr != badHr;            }
    [[nodiscard]] bool hasCad()     const   { return m_cad != badCad;          }
    [[nodiscard]] bool hasPower(int m, const PointItem* next = nullptr) const {
        return (bool(m & Measured) && !std::isnan(m_power)) ||
               (bool(m & Computed) && hasGrade() && hasSpeed(Either, next));
    }

    [[nodiscard]] bool hasVert(const PointItem* next) const {
        return next != nullptr && next->hasEle() && hasEle();
    }
    [[nodiscard]] bool hasLength(const PointItem* next) const {
        return next != nullptr && next->hasDistance() && hasDistance();
    }
    [[nodiscard]] bool hasDuration(const PointItem* next) const {
        return next != nullptr && next->hasTime() && hasTime() && next->time() != time();
    }

    [[nodiscard]] bool hasName()    const { return hasAuxData() && !m_auxData->m_name.isEmpty(); }
    [[nodiscard]] bool hasComment() const { return hasAuxData() && !m_auxData->m_comment.isEmpty(); }
    [[nodiscard]] bool hasDesc()    const { return hasAuxData() && !m_auxData->m_desc.isEmpty(); }
    [[nodiscard]] bool hasSymbol()  const { return hasAuxData() && !m_auxData->m_symbol.isEmpty(); }
    [[nodiscard]] bool hasType()    const { return hasAuxData() && !m_auxData->m_type.isEmpty(); }

    [[nodiscard]] bool hasExtData() const;

    // Interpolate between this and given point (will also extrapolate if time is
    // past next point's time).  If you extrapolate too far, the results may be nonsensical.
    // Data items will only be populated in the result if both p0 and p1 have them.
    [[nodiscard]] static PointItem interpolate(const PointItem& p0, const PointItem& p1, const QDateTime&);
    [[nodiscard]] PointItem interpolate(const PointItem&, const QDateTime&) const;

    [[nodiscard]] static qreal greatCircleDist(const PointItem& p0, const PointItem& p1) {
        return GeoMath::greatCircleDist(p0.latRad(), p0.lonRad(), p1.latRad(), p1.lonRad());
    }

    [[nodiscard]] qreal greatCircleDist(const PointItem& p1) const {
        return GeoMath::greatCircleDist(latRad(), lonRad(), p1.latRad(), p1.lonRad());
    }

    [[nodiscard]] qreal greatCircleDist(Lat_t lat, Lon_t lon) const {
        return GeoMath::greatCircleDist(latRad(), lonRad(), lat, lon);
    }

    // Grade from rise and run
    [[nodiscard]] static inline Grade_t grade(Ele_t, Dist_t);

    // Elevation for run and grade
    [[nodiscard]] static inline Ele_t riseForGrade(Dist_t, Grade_t);

    enum class Flags : uint8_t {
        NoFlags     = 0,
        Select      = (1<<0), // normally selected point
        AreaSelect  = (1<<1), // lies within area select region
    };

    using Flags_t = std::underlying_type_t<Flags>;

    // Flag query and set
    [[nodiscard]] bool test(Flags f) const  { return Flags(Flags_t(m_flags) & Flags_t(f)) == f; }

    // Flag set/unset: returns +1 if set, -1 if unset, or 0 if no change
    inline int set(bool on, Flags f) {
        const Flags old = m_flags;
        m_flags = Flags(on ? (Flags_t(m_flags) | Flags_t(f)) : (Flags_t(m_flags) & ~Flags_t(f)));

        // return increment if something changed
        return (old == m_flags) ? 0 : (on ? 1 : -1);
    }

    Flags flags() const { return m_flags; }

    // Set methods
    PointItem& setPos(Lon_t lon, Lat_t lat) { m_lon = lon; m_lat = lat;         return *this; }
    PointItem& setTime(const QDateTime& t)  { m_time    = t;                    return *this; }
    PointItem& setLat(Lat_t lat)            { m_lat     = lat;                  return *this; }
    PointItem& setLon(Lon_t lon)            { m_lon     = lon;                  return *this; }
    PointItem& setEle(Ele_t ele, bool flt = false) {
        if (flt) m_fltEle = ele; else m_ele = ele;
        return *this;
    }
    PointItem& setSpeed(Speed_t speed)      { m_speed   = speed;                return *this; }
    PointItem& unsetSpeed()                 { m_speed   = Speed_t(fNaN);        return *this; }
    PointItem& setATemp(Temp_t atemp)       { m_atemp   = int16_t(atemp * 100.0f); return *this; }
    PointItem& setGrade(Grade_t grade)      { m_grade   = decltype(m_grade)(grade * (badGradeI-1)); return *this; }
    PointItem& setHr(Hr_t hr)               { m_hr      = hr;                   return *this; }
    PointItem& setCad(Cad_t cad)            { m_cad     = cad;                  return *this; }
    PointItem& setPower(Power_t power)      { m_power   = power;                return *this; }
    PointItem& setDistance(Dist_t dist)     { m_dist    = dist;                 return *this; }

    PointItem& setName(const QString& s)    { getAuxData().m_name = s;          return maybeRemoveAuxData(); }
    PointItem& setComment(const QString& s) { getAuxData().m_comment = s;       return maybeRemoveAuxData(); }
    PointItem& setDesc(const QString& s)    { getAuxData().m_desc = s;          return maybeRemoveAuxData(); }
    PointItem& setSymbol(const QString& s)  { getAuxData().m_symbol = s;        return maybeRemoveAuxData(); }
    PointItem& setType(const QString& s)    { getAuxData().m_type = s;          return maybeRemoveAuxData(); }
    PointItem& setWTemp(Temp_t wtemp)       { getAuxData().setWTemp(wtemp);     return maybeRemoveAuxData(); }
    PointItem& setDepth(float depth)        { getAuxData().setDepth(depth);     return maybeRemoveAuxData(); }
    PointItem& setCourse(float course)      { getAuxData().setCourse(course);   return maybeRemoveAuxData(); }
    PointItem& setBearing(float bearing)    { getAuxData().setBearing(bearing); return maybeRemoveAuxData(); }

    template <typename T = QVariant>
    [[nodiscard]] T data(ModelType, int row, int role, bool flt,
                         const PointItem* startPt,
                         const PointItem* nextPt,
                         const PowerData&) const;
    bool               setData(ModelType, QVariant value, int role, bool& changed);
    [[nodiscard]] bool hasData(ModelType, const PointItem* nextPt) const;
    void               clearData(ModelType);

    // convert to the format T, provided we know about it.
    template <typename T> [[nodiscard]] T as() const;

    [[nodiscard]] bool operator==(const PointItem&) const;

    PointItem& clearAuxData() { m_auxData.reset(); return *this; };

private:
    // This structure is dynamically allocated when needed, to keep rarely used data from
    // bloating the size of the PointItem.
    struct AuxData final {
        AuxData() :
            m_depth(badDepthI), m_wtemp(badWTempI),
            m_course(badCourseI), m_bearing(badBearingI)
        { }

        AuxData(const AuxData&) = default;
        AuxData& operator=(const AuxData&) = default;
        [[nodiscard]] bool operator==(const AuxData&) const;

        void clearData(ModelType);

        // return true if this AuxData has all default values (ergo, we can remove it)
        [[nodiscard]] bool isDefault() const { return *this == AuxData(); }

        [[nodiscard]] Temp_t    wtemp()   const { return Temp_t::base_type(m_wtemp) * 0.01f; }
        [[nodiscard]] float     depth()   const { return m_depth * 0.2; }
        [[nodiscard]] float     course()  const { return m_course * 0.01; }
        [[nodiscard]] float     bearing() const { return m_bearing * 0.01; }

        void setWTemp(Temp_t wtemp)             { m_wtemp   = int16_t(wtemp * 100.0f); }
        void setDepth(float depth)              { m_depth   = depth * 5.0; }
        void setCourse(float course)            { m_course  = course * 100.0; }
        void setBearing(float bearing)          { m_bearing = bearing * 100.0; }

        [[nodiscard]] bool hasWtemp()   const   { return m_wtemp != badWTempI; }
        [[nodiscard]] bool hasDepth()   const   { return m_depth != badDepthI; }
        [[nodiscard]] bool hasCourse()  const   { return m_course != badCourseI;   }
        [[nodiscard]] bool hasBearing() const   { return m_bearing != badBearingI; }

        QString  m_name;      // [saved] corresponds to GPX <name>
        QString  m_comment;   // [saved] corresponds to GPX <cmt>
        QString  m_desc;      // [saved] corresponds to GPX <desc>
        QString  m_symbol;    // [saved] corresponds to GPX <sym>
        QString  m_type;      // [saved] corresponds to GPX <type>

        int16_t  m_depth;     // [saved] water depth, meters * 5
        int16_t  m_wtemp;     // [saved] water temp, degrees C * 100
        uint16_t m_course;    // [saved] course, degrees * 100
        uint16_t m_bearing;   // [saved] bearing, degrees * 100

        static const constexpr auto badWTempI   = std::numeric_limits<decltype(m_wtemp)>::max();
        static const constexpr auto badDepthI   = std::numeric_limits<decltype(m_depth)>::max();
        static const constexpr auto badCourseI  = std::numeric_limits<decltype(m_course)>::max();
        static const constexpr auto badBearingI = std::numeric_limits<decltype(m_bearing)>::max();
    };

    PointItem& maybeRemoveAuxData(); // remove AuxData if it has entirely default values

    friend class TestZtgps; // test hook
    friend class PointModel;
    friend uint qHash(const PointItem&, uint);
    friend uint qHash(const AuxData&, uint);
    friend QDataStream& operator<<(QDataStream&, const PointItem&);
    friend QDataStream& operator>>(QDataStream&, PointItem&);
    friend QDataStream& operator<<(QDataStream&, const PointItem::AuxData&);
    friend QDataStream& operator>>(QDataStream&, PointItem::AuxData&);

    static void calcSinAtans();
    bool hasAuxData() const { return bool(m_auxData); } // true if there is aux data for this point
    AuxData& getAuxData(); // potentially allocate, return AuxData

    QDateTime m_time;      // [saved] sample timestamp

    Lon_t     m_lon;       // [saved] longitude, degrees
    Lat_t     m_lat;       // [saved] latitude, degrees

    Ele_t     m_ele;       // [saved] elevation, m
    Ele_t     m_fltEle;    // [calc] noise-filtered elevation

    Dist_t    m_dist;      // [calc] total distance within the track to this point, m
    Speed_t   m_speed;     // [saved] vehicle speed, m/s
    Power_t   m_power;     // [saved] rider power
    int16_t   m_grade;     // [calc] grade, %
    int16_t   m_atemp;     // [saved] air temp, degrees C * 100

    Flags     m_flags;     // [calc] cache for render performance
    Hr_t      m_hr;        // [saved] heart rate, beat/min
    Cad_t     m_cad;       // [saved] cadence, rev/min

    std::unique_ptr<AuxData> m_auxData; // pointer to out-of-struct data

    static const QVariant   m_empty;
    static std::array<int16_t, std::numeric_limits<uint16_t>::max()> m_sinAtanTable; // fixed point sin(atan(x)) tables
    static bool m_sinAtanTableInit;

    static const constexpr Speed_t stoppedThresholdSpeed = Speed_t(0.5);  // in meters per second.

public:
    static const constexpr auto     NaN        = std::numeric_limits<qreal>::quiet_NaN();
    static const constexpr auto     fNaN       = std::numeric_limits<float>::quiet_NaN();
    static const constexpr auto     badLat     = Lat_t(std::numeric_limits<decltype(m_lat)>::quiet_NaN());
    static const constexpr auto     badLon     = Lon_t(std::numeric_limits<decltype(m_lon)>::quiet_NaN());
    static const constexpr auto     badEle     = Ele_t(std::numeric_limits<decltype(m_ele)>::quiet_NaN());
    static const constexpr auto     badPower   = std::numeric_limits<decltype(m_power)>::quiet_NaN();
    static const constexpr uint8_t  badHr      = std::numeric_limits<decltype(m_hr)>::max();
    static const constexpr uint8_t  badCad     = std::numeric_limits<decltype(m_cad)>::max();

private:
    static const constexpr auto     badATempI   = std::numeric_limits<decltype(m_atemp)>::max();
    static const constexpr auto     badGradeI   = std::numeric_limits<decltype(m_grade)>::max();
};

#pragma pack(pop)

// Guard against unexpected size changes. Size is CRITICAL for this structure, since it is
// per-point, and points arrive by the boatloads.
static_assert(sizeof(PointItem) == (44 + sizeof(QDateTime) + sizeof(std::unique_ptr<int>)));

// Verify nobody has added anything foolish, since this is performance critical.
static_assert(!std::is_polymorphic_v<PointItem> && std::is_final_v<PointItem>);

// Serialization
extern inline QDataStream& operator<<(QDataStream&, const PointItem&);
extern inline QDataStream& operator>>(QDataStream&, PointItem&);
extern inline QDataStream& operator<<(QDataStream&, const PointItem::AuxData&);
extern inline QDataStream& operator>>(QDataStream&, PointItem::AuxData&);

// Hashing.
extern uint qHash(const PointItem&, uint seed = 0xc725139f);
extern inline uint qHash(const PointItem::AuxData&, uint seed = 0x49dc1485);

// Convert to GeoDataCoordinates for Marble
template<> inline Marble::GeoDataCoordinates PointItem::as<Marble::GeoDataCoordinates>() const
{
    return { Lon_t::base_type(lon()), Lat_t::base_type(lat()),
             hasEle() ? Ele_t::base_type(ele()) : 0.0f,
             Marble::GeoDataCoordinates::Degree };
}

// Flag accumulation
inline PointItem::Flags operator|(PointItem::Flags lhs, PointItem::Flags rhs)
{
    return PointItem::Flags(PointItem::Flags_t(lhs) | PointItem::Flags_t(rhs));
}

inline PointItem::Flags& operator|=(PointItem::Flags& lhs, PointItem::Flags rhs)
{
    return lhs = lhs | rhs;
}

#endif // POINTITEM_H
