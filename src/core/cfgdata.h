/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CFGDATA_H
#define CFGDATA_H

#include <utility>
#include <cstdint>
#include <QList>
#include <QString>
#include <QStringList>
#include <QSize>

#include <src/core/cfgdatabase.h>
#include <src/core/svgcolorizer.h>
#include <src/core/colorizermodel.h>

#include <src/util/units.h>
#include "src/core/tagmodel.h"
#include "src/core/personmodel.h"
#include "src/core/zonemodel.h"
#include "src/core/trkptcolormodel.h"

class PointModel;

class CfgData final : public CfgDataBase
{
private:
    // helper to call static initializers before other object construction
    // must be initialized first
    struct InitStatic { InitStatic(); } initStatic;

public:
    explicit CfgData(bool defaults = false);
    explicit CfgData(const CfgData& rhs);

    CfgData& operator=(const CfgData& rhs) = default;

    // Modes for interactive map movement.  This enum order must match the .ui widget text order.
    enum class MapMoveMode {
        AllTracks,   // display all tracks during interactive movement
        ActiveTrack, // display only the single active track
        NoTracks,    // display no tracks during interactive movement
        _Count,
    };

    enum class AutoImportMode {
        Disabled,   // Do not auto-import files
        Menu,       // Auto-import on menu selection
        Startup,    // Auto-import at startup
    };

    enum class AutoImportPost {
        Backup,     // Backup files to a suffix after auto-importing
        Move,       // Move files to a new location after auto-importing. Clobbers existing.
        Delete,     // Delete files after auto-import.
        Leave,      // Leave files in place after auto-import.
    };

    int32_t         eleFilterSize;            // convolution filter size for elevation data
    int32_t         tooltipInlineLimit;       // limit for inline display of long text fields.
    int32_t         tooltipFullLimit;         // limit for text display in dedicated tooltips.

    Units           unitsTrkLength;           // units for track length
    Units           unitsLegLength;           // units for leg (point to point) length
    Units           unitsDuration;            // units for durations
    Units           unitsTrkDate;             // units for track date+time stamp
    Units           unitsTrkTime;             // units for track time of day (no date)
    Units           unitsPointDate;           // units for point time
    Units           unitsTz;                  // units for time zones
    Units           unitsElevation;           // units for elevation
    Units           unitsLat;                 // units for latitudes
    Units           unitsLon;                 // units for longitudes
    Units           unitsSpeed;               // units for speeds
    Units           unitsClimb;               // units for ascent/descent
    Units           unitsArea;                // units for areas
    Units           unitsTemp;                // units for temperatures
    Units           unitsSlope;               // units for slopes
    Units           unitsPower;               // units for power
    Units           unitsEnergy;              // units for energy
    Units           unitsWeight;              // units for weight
    Units           unitsPct;                 // units for percents
    Units           unitsCad;                 // units for cadence
    Units           unitsHr;                  // units for heart rate

    QColor          unassignedTrackColor;     // color for tracks without one.
    QColor          outlineTrackColor;        // color for current track outline

    float           defaultTrackWidthC;       // normal track draw width, close
    float           defaultTrackWidthF;       // normal track draw width, far
    float           defaultTrackWidthO;       // normal track draw width, outline
    float           currentTrackWidthC;       // current track width, close
    float           currentTrackWidthF;       // current track width, far
    float           currentTrackWidthO;       // current track width, outline

    int32_t         defaultTrackAlphaC;       // default track alpha, close
    int32_t         defaultTrackAlphaF;       // default track alpha, far
    int32_t         currentTrackAlphaC;       // current track alpha, close
    int32_t         currentTrackAlphaF;       // current track alpha, far

    MapMoveMode     mapMoveMode;              // map movement mode
    bool            mapMovePoints;            // whether to draw map points during movement
    bool            mapMoveWaypoints;         // whether to draw waypoints during movement
    bool            mapInertialMovement;      // whether map should have inertia during interactive movement

    QString         defaultPointIcon;         // unselected point icon
    int32_t         defaultPointIconSize;     // unselected point icon size
    int32_t         defaultPointIconProx;     // unselected point icon minimum proximity
    QString         selectedPointIcon;        // selected point icon
    int32_t         selectedPointIconSize;    // selected point icon size
    QString         currentPointIcon;         // current point icon
    int32_t         currentPointIconSize;     // current point icon size
    QString         gpsdLivePointIcon;        // icon for GPSD live points
    int32_t         gpsdLivePointIconSize;    // current GPSD live point icon size
    QString         waypointDefaultIcon;      // default icon for waypoints
    int32_t         waypointDefaultIconSize;  // size of default waypoint icon
    int32_t         waypointIconSize;         // size of waypoint icons on the map

    QString         trackNoteIcon;            // icon to indicate track has a note.
    bool            colorizeTagIcons;         // colorize tag SVG icons with tag color.
    QSize           iconSizeTrack;            // icon size in track panes
    QSize           flagSizeTrack;            // flag size in track panes
    QSize           iconSizeView;             // icon size in view panes
    QSize           iconSizeTag;              // icon size in tag selectors
    QSize           iconSizeFilter;           // icon size in view panes
    QSize           iconSizeClimb;            // icon size for climb analysis panes
    int32_t         maxTrackPaneIcons;        // maximum icons to display in track lists
    int32_t         maxTrackPaneFlags;        // maximum flags to display in track lists

    AutoImportMode  autoImportMode;           // autoimport mode
    QString         autoImportDir;            // autoimport from this dir
    QString         autoImportPattern;        // pattern to use to select auto-import files
    QStringList     autoImportTags;           // tags to apply at import time
    AutoImportPost  autoImportPost;           // post-import behavior
    QString         autoImportBackupSuffix;   // autoimport backup suffix
    QString         autoImportBackupDir;      // autoimport backup directory
    QString         autoImportCommand;        // command for auto-import
    bool            autoImportStdout;         // read stdout as if it was a GPS file
    int32_t         autoImportTimeout;        // external command timeout

    TagModel        tags;                     // tag data
    PersonModel     people;                   // people (athletes, passengers...)
    ZoneModel       zones;                    // training zones
    ColorizerModel  trackColorizer;           // track data colorization
    ColorizerModel  pointColorizer;           // track point colorization
    ColorizerModel  climbColorizer;           // climb colorization
    TrkPtColorModel trkPtColor;               // track point colors for graphs etc.

    QColor          trkPtMarkerColor;         // color for position marker in TrackLinePane
    QColor          trkPtRangeColor;          // color for drawing ranges in TrackLinePane
    float           trkPtLineWidth;           // track point line width
    float           trkPtMarkerWidth;         // track point marker line width
    float           trkPtRangeWidth;          // track point range line width
    int             asMaxDateSpans;           // activity summary max date spans in graph
    int             asBarWidth;               // activity summary bar width %
    float           hillMinGrade;             // minimum hill grade, %
    float           hillMinHeight;            // minimum hill height, meters
    float           hillGradeLength;          // to find steepest sections of hills over given distance

    float           mapUndoStill;             // create a map undo point after it's still for this many seconds.
    int             maxUndoCountView;         // max view undos (used for map pane movement undo)

    void reset() override;                    // reset to defaults
    void postLoadHook() override;             // post-load tasks

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

    void setupDefaultConfig();               // Setup default config, e.g, on first run.

    static SvgColorizer          svgColorizer;  // common SVG colorizer cache
    static const PointModel*     emptyPointModel;

    static QVector<QColor> valColorSet();
    static QVector<QColor> ampmColorSet();

    size_t size() const; // deep size in bytes, approximate.

    static std::pair<QString, bool> pngToSvg(const QString&);  // convert .png name to .svg (unconditional)

    static const constexpr int pointPngToSvgVersion = 14; // changed point pngs to svgs at this version

private:
    CfgData& operator=(const CfgDataBase& rhs) override {
        return *this = reinterpret_cast<const CfgData&>(rhs);
    }

    // This is the compile-time current cfg data version.  On load, older save versions will be
    // updated to this version via updateFormat().
    static const constexpr uint32_t currentCfgDataVersion = 26;
    static const constexpr char* air   = "Air";
    static const constexpr char* water = "Water";

    static size_t size(const TreeModel&);   // helper to estimate size of model
    static size_t size(const QString&);     // helper to estimate size of string
    static size_t size(const QStringList&); // helper to estimate size of string list

    // ver is first version where this tag appeared, for updating old configs.
    TreeItem::ItemData tag(const QString& dir, uint32_t ver, const QString& name, QRgb color,
                           QString iconName = "",
                           qreal CdA = 0.0, qreal weight = 0.0, qreal rr = 0.0,
                           qreal efficiency = 0.0, qreal bioPct = -1.0,
                           const char* medium = nullptr);

    QModelIndex find(const QString& name) const {
        return tags.findRow(QModelIndex(), name, TagModel::Name, Util::RawDataRole);
    }

    // append rows if data is not already present
    bool appendTagsIfMissing(const QString& dir, uint32_t ver, const QVector<TreeItem::ItemData>& data);

    void defaultTagsActivity        ();     // create default tags for Activities
    void defaultTagsTransport();            // create default tags for Transport
    void defaultTagsSeasons();              // create default tags for Seasons
    void defaultTagsSigns();                // create default tags for Signs
    void defaultTagsWeather();              // create default tags for Weather
    void defaultTagsMisc();                 // create default tags for Misc
    void defaultPeople();                   // create default people
    void updateTrackColorizers();           // update track colorizer to current cfgDataVersion
    void updatePointColorizers();           // update point colorizer to current cfgDataVersion
    void updateClimbColorizers();           // update climb colorizer to current cfgDataVersion
    void updateTrkPtColorModel();           // update track point color model to current cfgDataVersion
    void updateTags();                      // create all tags for active cfgDataVersion
    void updateFormat();                    // update old save formats to latest format
    void updatePointIcons();                // update old png point icons to svg
    void updateColors(ColorizerModel&);     // Blue/green were swapped in old defaults
    void updateZones();                     // setup/update default zones
    void insertColumnOnLoad(uint32_t ver, ModelType column); // update data (e.g, track colorizer) on new column
};

// Matches defined colored points, without pre or suffixes.
#define PointColorNameRe "[A-Z][a-z][a-z][0-9][0-9][A-Z][a-z][a-z][0-9][0-9]"

Q_DECLARE_METATYPE(CfgData::MapMoveMode)
Q_DECLARE_METATYPE(CfgData::AutoImportMode)
Q_DECLARE_METATYPE(CfgData::AutoImportPost)

QDataStream& operator<<(QDataStream&, const CfgData::MapMoveMode&);
QDataStream& operator>>(QDataStream&, CfgData::MapMoveMode&);

QDataStream& operator<<(QDataStream&, const CfgData::AutoImportMode&);
QDataStream& operator>>(QDataStream&, CfgData::AutoImportMode&);

QDataStream& operator<<(QDataStream&, const CfgData::AutoImportPost&);
QDataStream& operator>>(QDataStream&, CfgData::AutoImportPost&);

#endif // CFGDATA_H
