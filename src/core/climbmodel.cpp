/*
    Copyright 2021-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cmath> // for std::copysign
#include <chrono>

#include <QModelIndex>

#include <src/util/roles.h>
#include <src/util/units.h>
#include <src/core/modelmetadata.inl.h>

#include "src/ui/windows/mainwindow.h"

#include "app.h"
#include "pointmodel.h"
#include "climbmodel.h"
#include "climbitem.h"

ClimbModel::ClimbModel(QObject *parent) :
    TreeModel(new ClimbItem(), parent),
    NamedItem(getItemNameStatic())
{
    setHorizontalHeaderLabels(headersList<ClimbModel>());
    setupTimers();
    setupSignals();
}

ClimbModel::~ClimbModel()
{
}

void ClimbModel::setupTimers()
{
    m_updateTimer.setSingleShot(true);
    connect(&m_updateTimer, &QTimer::timeout, this,
            static_cast<void(ClimbModel::*)(void)>(&ClimbModel::deferredUpdate),
            Qt::UniqueConnection);
}

void ClimbModel::newConfig()
{
    update();
}

QModelIndex ClimbModel::currentTrack() const
{
    return { m_currentTrackIdx };
}

void ClimbModel::setupSignals() const
{
    if (MainWindow::mainWindowStatic == nullptr)
        return;

    // Notify ourselves and the filter about current container changes
    connect(MainWindow::mainWindowStatic, &MainWindow::currentTrackChanged, this, &ClimbModel::currentTrackChanged,
            Qt::UniqueConnection);

    // Row deletion from model
    connect(&app().trackModel(), &TrackModel::rowsAboutToBeRemoved, this, &ClimbModel::processRowsAboutToBeRemoved,
            Qt::UniqueConnection);
    // Model reset
    connect(&app().trackModel(), &TrackModel::modelAboutToBeReset, this, &ClimbModel::processModelAboutToBeReset,
            Qt::UniqueConnection);
}

QVariant ClimbModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (const QVariant val = ModelMetaData::headerData<ClimbModel>(section, orientation, role); val.isValid())
        return val;

    return TreeModel::headerData(section, orientation, role);
}

QVariant ClimbModel::data(const QModelIndex& idx, int role) const
{
    verify(idx);

    if (!idx.isValid() || idx.column() >= columnCount())
        return { };

    cfgDataWritable().climbColorizer.setModel(this);

    if (const QVariant val = getItem(idx)->data(idx.column(), role); val.isValid()) {
        const QVariant maybeVal = cfgData().climbColorizer.maybeUse(val, idx, role);
        return cfgData().trackColorizer.maybeUse(maybeVal, idx, role);
    }

    // Give precedence to the Climb Colorizer...
    if (const QVariant colorVal = cfgData().climbColorizer.colorize(idx, role); colorVal.isValid())
        return colorVal;

    // If climb colorizer didn't match, fall back to the track colorizer.
    return cfgData().trackColorizer.colorize(idx, role);
}

QModelIndex ClimbModel::parent(const QModelIndex&) const
{
    return { };
}

const Units& ClimbModel::units(const QModelIndex& idx) const
{
    return mdUnits(idx.column());
}

Qt::ItemFlags ClimbModel::flags(const QModelIndex& idx) const
{
    return TreeModel::flags(idx) | ModelMetaData::flags<ClimbModel>(idx) |
            Qt::ItemIsDragEnabled;
}

const ClimbItem* ClimbModel::getItem(const QModelIndex& idx) const
{
    return const_cast<const ClimbItem*>(static_cast<ClimbItem*>(TreeModel::getItem(idx)));
}

QString ClimbModel::mdName(ModelType md)
{
    switch (md) {
    case ClimbModel::Type:     return QObject::tr("Type");
    case ClimbModel::Start:    return QObject::tr("Start");
    case ClimbModel::End:      return QObject::tr("End");
    case ClimbModel::Steep:    return QObject::tr("Steep");
    case ClimbModel::Vertical: return QObject::tr("Vertical");
    default:                   return TrackModel::mdName(md);
    }

    assert(0 && "Unknown ClimbModel value");
    return "";
}

bool ClimbModel::mdIsEditable(ModelType)
{
    return false;
}

// tooltip for container column header
QString ClimbModel::mdTooltip(ModelType mt)
{
    const bool editable = mdIsEditable(mt);

    switch (mt) {
    case ClimbModel::Type:
        return makeTooltip(tr("Type (climb or descent)."), editable);
    case ClimbModel::Start:
        return makeTooltip(tr("Horizontal position of start of climb."), editable);
    case ClimbModel::End:
        return makeTooltip(tr("Horizontal position of end of climb."), editable);
    case ClimbModel::Steep:
        return makeTooltip(tr("Steepest short section of track. Distance set in configuration."), editable);
    case ClimbModel::Vertical:
        return makeTooltip(tr("Total vertical distance."), editable);
    default:
        return TrackModel::mdTooltip(mt);
    }

    assert(0 && "Unknown ClimbModel value");
    return { };
}

// tooltip for container column header
QString ClimbModel::mdWhatsthis(ModelType md)
{
    return mdTooltip(md);  // just pass through the tooltip
}

Qt::Alignment ClimbModel::mdAlignment(ModelType)
{
    return Qt::AlignRight   | Qt::AlignVCenter;
}

const Units& ClimbModel::mdUnits(ModelType md)
{
    static const Units rawText(Format::String);
    static const Units rawFloat(Format::Float);

    switch (md) {
    case ClimbModel::Type:  return rawText;
    case ClimbModel::Start: [[fallthrough]];
    case ClimbModel::End:   return cfgData().unitsTrkLength;
    case ClimbModel::Steep: return cfgData().unitsSlope;
    default:                return TrackModel::mdUnits(md);
    }

    assert(0 && "Unknown ClimbModel value");
    return rawFloat;
}

bool ClimbModel::mdIgnore(ModelType md)
{
    switch (md) {
    case ClimbModel::_unused1: [[fallthrough]];
    case ClimbModel::_unused2: [[fallthrough]];
    case ClimbModel::_unused3: return true;
    default:                   return false;
    }
}

ClimbModel::name_t ClimbModel::getItemNameStatic()
{
    return { tr("Climb"), tr("Climbs") };
}

quint32 ClimbModel::streamMagic() const
{
    return App::NativeMagic + quint32(App::Model::Climb);
}

quint32 ClimbModel::streamVersionMin() const
{
    return App::NativeVersionMin;
}

quint32 ClimbModel::streamVersionCurrent() const
{
    return App::NativeVersionCurrent;
}

void ClimbModel::unsetIndex(QPersistentModelIndex& idx, const QModelIndex& parent, int first, int last)
{
    if (idx.parent() == parent && idx.row() >= first && idx.row() <= last) {
        idx = QModelIndex();
        clear();
    }
}

void ClimbModel::processRowsAboutToBeRemoved(const QModelIndex& parent, int first, int last)
{
    if (!m_currentTrackIdx.isValid())
        return;

    unsetIndex(m_currentTrackIdx, parent, first, last);
    unsetIndex(m_nextTrackIdx, parent, first, last);
}

void ClimbModel::processModelAboutToBeReset()
{
    m_currentTrackIdx = QModelIndex();
    m_nextTrackIdx    = QModelIndex();
    clear();
}

void ClimbModel::update(const PointModel* pointModel)
{
    clear();

    if (pointModel == nullptr)
        return;

    // Propagate values from the climb ranges into our model
    for (const auto& climb : pointModel->climbs(*this)) {
        appendRow();  // Add new row for this hill

        // Set start and end of climb distances
        setData(index(rowCount() - 1, ClimbModel::Start),
                pointModel->data(PointModel::Distance, std::get<0>(climb), Util::RawDataRole),
                Util::RawDataRole);

        setData(index(rowCount() - 1, ClimbModel::End),
                pointModel->data(PointModel::Distance, std::get<1>(climb), Util::RawDataRole),
                Util::RawDataRole);

        // Maximum grade over short sections
        setData(index(rowCount() - 1, ClimbModel::Steep),
                PointItem::Grade_t::base_type(std::get<2>(climb)), Util::RawDataRole) ;

        // Analyze the range
        const auto values = pointModel->calcTrackData(PointItem::Flags::NoFlags, std::get<0>(climb), std::get<1>(climb));
        // Set our values accordingly
        for (auto it = values.constBegin(); it != values.constEnd(); ++it) {
            const QModelIndex newRowIdx = index(rowCount() - 1, it.key());
            QVariant value = it.value();

            // TrackModel's BasePeak value is always positive.  Fix sign, since we want negative values for
            // our vertical extent.
            if (it.key() == ClimbModel::Vertical)
                value = std::copysign(value.toFloat(), values[ClimbModel::BeginToEndEle].toFloat());

            setData(newRowIdx, value, Util::RawDataRole);
        }
    }
}

void ClimbModel::currentTrackChanged(const QModelIndex& current)
{
    // Return if no row change.
    if (m_currentTrackIdx.model() == current.model() && m_currentTrackIdx.row() == current.row())
        return;

    deferredUpdate(current);
}

void ClimbModel::update()
{
    update(app().trackModel().geoPoints(m_currentTrackIdx));
}

void ClimbModel::deferredUpdate(const QModelIndex& next)
{
    using namespace std::chrono_literals;

    m_nextTrackIdx = next.sibling(next.row(), 0);  // save index for deferredUpdate()
    m_updateTimer.start(250ms);
}

void ClimbModel::deferredUpdate()
{
    if (m_currentTrackIdx == m_nextTrackIdx) {
        m_nextTrackIdx    = QModelIndex();
        return;
    }

    m_currentTrackIdx = m_nextTrackIdx;
    m_nextTrackIdx    = QModelIndex();
    update();
}

