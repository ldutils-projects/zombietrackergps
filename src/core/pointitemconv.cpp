/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "pointitem.h"
#include "pointitemconv.h"

PointItem::PointItem(const V1000::PointItem& pt) :
    PointItem()
{
    if (!std::isnan(pt.m_atemp))   setATemp(pt.m_atemp);
    if (!std::isnan(pt.m_wtemp))   setWTemp(pt.m_wtemp);
    if (!std::isnan(pt.m_depth))   setDepth(pt.m_depth);
    if (!std::isnan(pt.m_speed))   setSpeed(Speed_t(pt.m_speed));
    if (pt.m_hr != badHr)          setHr(pt.m_hr);
    if (pt.m_cad != badCad)        setCad(pt.m_cad);
    if (!std::isnan(pt.m_power))   setPower(pt.m_power);
    if (!std::isnan(pt.m_course))  setCourse(pt.m_course);
    if (!std::isnan(pt.m_bearing)) setBearing(pt.m_bearing);
    if (pt.m_time.isValid())       setTime(pt.m_time);
    if (!std::isnan(pt.m_lon))     setLon(pt.m_lon);
    if (!std::isnan(pt.m_lat))     setLat(pt.m_lat);
    if (!std::isnan(pt.m_ele))     setEle(pt.m_ele);
}

PointItem::PointItem(const V1001::PointItem& pt) :
    PointItem()
{
    if (pt.m_time.isValid())                  setTime(pt.m_time);
    if (pt.m_lat != badLat)                   setLat(pt.m_lat);
    if (pt.m_lon != badLon)                   setLon(pt.m_lon);
    if (pt.m_ele != badEle)                   setEle(pt.m_ele);
    if (!std::isnan(pt.m_speed))              setSpeed(Speed_t(pt.m_speed));
    if (pt.m_power != badPower)               setPower(pt.m_power);
    if (pt.m_atemp != badATempI)              setATemp(pt.m_atemp);
    if (pt.m_depth != AuxData::badDepthI)     setDepth(pt.m_depth);
    if (pt.m_wtemp != AuxData::badWTempI)     setWTemp(pt.m_wtemp);
    if (pt.m_course != AuxData::badCourseI)   setCourse(pt.m_course);
    if (pt.m_bearing != AuxData::badBearingI) setBearing(pt.m_bearing);
    if (pt.m_hr != badHr)                     setHr(pt.m_hr);
    if (pt.m_cad != badCad)                   setCad(pt.m_cad);
}

QDataStream& operator>>(QDataStream& stream, V1000::PointItem& pt)
{
    // Distance, etc are computed from other values: no need to save/load
    return stream >> pt.m_atemp
                  >> pt.m_wtemp
                  >> pt.m_depth
                  >> pt.m_speed
                  >> pt.m_hr
                  >> pt.m_cad
                  >> pt.m_power
                  >> pt.m_course
                  >> pt.m_bearing
                  >> pt.m_time
                  >> pt.m_lon
                  >> pt.m_lat
                  >> pt.m_ele;
}

QDataStream& operator>>(QDataStream& stream, V1001::PointItem& pt)
{
    // Distance, etc are computed from other values: no need to save/load
    return stream >> pt.m_time
                  >> pt.m_lon
                  >> pt.m_lat
                  >> pt.m_ele
                  >> pt.m_speed
                  >> pt.m_power
                  >> pt.m_depth
                  >> pt.m_atemp
                  >> pt.m_wtemp
                  >> pt.m_course
                  >> pt.m_bearing
                  >> pt.m_hr
                  >> pt.m_cad;
}
