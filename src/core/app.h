/*
    Copyright 2020-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef APP_H
#define APP_H

#include <src/core/appbase.h>

#include "src/core/filtermodel.h"
#include "src/core/viewmodel.h"
#include "src/core/trackmodel.h"
#include "src/core/waypointmodel.h"
#include "src/core/geolocmodel.h"
#include "src/core/climbmodel.h"
#include "src/dev-io/gpsmodel.h"

#include "src/core/geopolmgr.h"
#include "src/core/cfgdata.h"

class QString;
class ChangeTrackingModel;
class CmdLine;

class App final : public AppBase
{
public:
    // Models we manage
    enum class Model {
        _First,
        Track = _First,        // tracks
        View,                  // map views
        Filter,                // track filters
        Waypoint,              // waypoints
        _LastSaved,
        GeoLoc,                // GeoLoc database, these are saved as parts of tracks, so they're after _Count
        Point,                 //
        GpsDevice,             // GPS devices
        Climb,                 // climbs
        _Invalid = -1,
    };

    App(int &argc, char **argv, const CmdLine&);
    App(int &argc, const char **argv, const CmdLine&);
    ~App() override;

    ChangeTrackingModel& getModel(Model);
    const ChangeTrackingModel& getModel(Model) const;
    static QString modelDataSuffix(Model);

    FilterModel&   filterModel()         { return m_filterModel; }
    ViewModel&     viewModel()           { return m_viewModel; }
    TrackModel&    trackModel()          { return m_trackModel; }
    WaypointModel& waypointModel()       { return m_waypointModel; }
    GpsModel&      gpsModel()            { return m_gpsModel; }
    ClimbModel&    climbModel()          { return m_climbModel; }
    GeoLocModel&   geoLocModel()         { return m_geoLocModel; }
    GeoPolMgr&     geoPolMgr()           { return m_geoPolMgr; }
    CfgData&       cfgData() override    { return m_cfgData; }

    const GeoLocModel&   geoLocModel()   const { return m_geoLocModel; }
    const FilterModel&   filterModel()   const { return m_filterModel; }
    const ViewModel&     viewModel()     const { return m_viewModel; }
    const TrackModel&    trackModel()    const { return m_trackModel; }
    const WaypointModel& waypointModel() const { return m_waypointModel; }
    const GpsModel&      gpsModel()      const { return m_gpsModel; }
    const ClimbModel&    climbModel()    const { return m_climbModel; }
    const GeoPolMgr&     geoPolMgr()     const { return m_geoPolMgr; }

    const CfgData&       cfgData() const override { return m_cfgData; }
    const CmdLine&       cmdLine() const { return reinterpret_cast<const CmdLine&>(m_cmdLine); }

    void newSession() override;   // start a new session

    static App::Model parseModel(const QString& name);  // return model given name

    // Political region data file
    static const constexpr char* geoPolFile = ":data/geopol/geopol.dat";

    // Binary save magic values & program versions that use them:
    // 0x1000 - [0.96 .. 1.04]
    // 0x1001 - [1.05 .. 1.08] space savings for track points
    // 0x1002 - [1.09 .. 1.13] only save certain roles in TreeItem::save()
    // 0x1003 - [1.14 ..]      point item AuxData for names, etc
    static const constexpr quint32 NativeVersionMin     = 0x1000;
    static const constexpr quint32 NativeVersionCurrent = 0x1003;
    static const constexpr quint32 NativeMagic          = 0xd9baf758;

    enum WWW {
        Main,      // main page
        Donations, // donation section
    };

    static QUrl ZtgpsWWW(WWW = WWW::Main);

private:
    static void setupResources();
    static void setupIcons();
    static void registerTypes();
    void setupTranslators();

    struct InitStatic { InitStatic(); } initStatic;  // static initialization happens first, before other members

    // Models: must appear before CfgData, which uses them
    GeoLocModel            m_geoLocModel;
    FilterModel            m_filterModel;
    ViewModel              m_viewModel;
    TrackModel             m_trackModel;
    WaypointModel          m_waypointModel;
    GpsModel               m_gpsModel;
    ClimbModel             m_climbModel;

    QTranslator            m_ztgpsTranslator;

    CfgData                m_cfgData;     // app configuration
    GeoPolMgr              m_geoPolMgr;   // geopolitical region manager
};

inline App& app() { return reinterpret_cast<App&>(*qApp); }
inline const CfgData& cfgData() { return app().cfgData(); }
inline CfgData& cfgDataWritable() { return app().cfgData(); }

#endif // APP_H
