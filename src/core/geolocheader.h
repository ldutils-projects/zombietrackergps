/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOLOCHEADER_H
#define GEOLOCHEADER_H

#include <QString>
#include <QFile>
#include <QtEndian>

#include <cstdint>
#include <cassert>
#include <cstdio>
#include <cmath>
#include <type_traits>
#include <array>

#include <src/util/math.h>
#include <src/util/pack.h>

class QFontMetrics;

// File header for custom compression format.
struct GeoLocFileHeader
{
    GeoLocFileHeader(uint8_t type) : GeoLocFileHeader(0, type) { }
    GeoLocFileHeader(uint32_t size, uint8_t type) : m_size(size), m_type(type), m_version(1) { }

    bool read(const uchar*&);
#ifdef GEOLOCHDR_WRITE
    bool write(FILE* out);
#endif // GEOLOCHDR_WRITE

    uint32_t size() const { return m_size; }
    uint32_t magic() const { return (0xafb26600 + (uint32_t(m_type)<<8)) | m_version; }

    static const constexpr uint32_t headerSize = sizeof(uint32_t)*2;
    static const constexpr uint8_t typeName = 0;
    static const constexpr uint8_t typeLoc = 1;

private:
    uint32_t m_size;    // number of binary records
    uint8_t  m_type;    // file type, included in magic
    uint8_t  m_version; // file format version
};

struct GeoLocHdrDefs {
    static const constexpr int shareFirstBits = 5;
    static const constexpr int shareLastBits = 3;
    static const constexpr int shareFirstMul = 1;
    static const constexpr int shareLastMul = 2;
    static const constexpr int shareFirstMax = (1<<shareFirstBits)-1;
    static const constexpr int shareLastMax = (1<<shareLastBits)-1;

    // Place uncompressed entries every so often.
    static const constexpr uint32_t dictStride = 1024;

    // Value which marks absolute record number
    static const constexpr uint8_t absMarkerU = (1<<6);

    // Tests on raw byte stream: can be used without unpacking.
    static bool isShortHdr(const uchar* in) { return (in[1] & 0x80) == 0; }
    static bool isRelLine(const uchar* in) { return in[1] != absMarkerU; }
    static bool isDict(const uchar* in) {
        return in[0] == 0x0 && in[1] == absMarkerU;
    }
};

// These header structures are very carefully packed for both best compression
// and endian-independent serialization of packed bitfields.  Don't go poking
// at this unless you know what you're doing.
template <int totalBits>
struct GeoLocHdrBase : public GeoLocHdrDefs
{
    GeoLocHdrBase(uint8_t first = 0, uint8_t last = 0, int32_t rel = absMarker)
    {
        setShort();
        setShare(first, last);
        setRelRec(rel);
    }

    static const constexpr int relLineBits = totalBits - shareFirstBits - shareLastBits - 1;
    static const constexpr int32_t absMarker   = -(1<<(relLineBits-1));

    bool isShortHdr() const { return (m_rel[0] & 0x80) == 0; }
    bool isDict() const { return totalBits == 16 && m_share == 0 && !isRelLine(); }
    bool isRelLine() const { return relRec() != absMarker; }
    uint8_t shareFirst() const { return unpack<shareLastBits, shareFirstBits>(m_share) * shareFirstMul; }
    uint8_t shareLast() const { return unpack<0, shareLastBits>(m_share) * shareLastMul; }
    uint8_t shareTotal() const { return shareFirst() + shareLast(); }
    int32_t relRec() const {
        // manual sign extension from the smaller packed signed int.
        const uint32_t signExt = ((m_rel[0] & 0x40) ? -1U : 0x0) << relLineBits;
        return int32_t(signExt) |
                int32_t((unpack<0, 7>(m_rel[0]) << (totalBits - 16)) |
                        (totalBits == 24 ? m_rel[relSize - 1] : 0));
    }
    static int32_t maxRelDiff() { return 1<<(relLineBits-1); }

    void setShare(int32_t first, int32_t last) {
        m_share = uint8_t(first << shareLastBits) | uint8_t(last);
    }

    void setRelRec(int32_t rel) {
        pack<0, 7>(m_rel[0], (rel >> (totalBits - 16)) & 0x7f);
        if (totalBits == 24)
            m_rel[relSize - 1] = rel & 0xff;
    }

    bool skip(const uchar*&);
    bool read(const uchar*&, QString& scratch, QString& name, uint32_t& record);

#ifdef GEOLOCHDR_WRITE
    bool write(FILE* out, const QString& name, uint32_t lineNo) const;
    void dump(const QString& name, uint32_t lineRec) const;
#endif // GEOLOCHDR_WRITE

protected:
    static const constexpr int relSize = (totalBits - 8) / 8;

    void setShort() { pack<7, 1>(m_rel[0], totalBits == 16 ? 0 : 1); }

    uint8_t m_share = 0;                // share first, share last as packed bitfields
    std::array<uint8_t, relSize> m_rel; // endianness: do NOT make this a uint16_t or bitfield!
};

struct GeoLocHdrS final : public GeoLocHdrBase<16> { };
struct GeoLocHdrL final : public GeoLocHdrBase<24> { };

static_assert(sizeof(GeoLocHdrS) == 2, "Bad GeoLocHdrS size");
static_assert(sizeof(GeoLocHdrL) == 3, "Bad GeoLocHdrL size");

static_assert(!std::is_polymorphic_v<GeoLocHdrS>, "GeoLocHdr cannot be polymorphic");
static_assert(!std::is_polymorphic_v<GeoLocHdrL>, "GeoLocHdr cannot be polymorphic");

// Wrapper around both kinds of headers.  The header classes match the disk format and
// MUST NOT be polymorphic.
class GeoLocHdr final : public GeoLocHdrDefs
{
public:
    GeoLocHdr(uint32_t locRec = 0, const QString& name = "") :
        m_name(name), m_locRec(locRec)
    { }

    using locRec_t = uint32_t;

#define HDRCALL(call) (m_useLongHdr ? m_hdrL.call : m_hdrS.call)
#define HDRCALL2(long, call) ( long ? m_hdrL.call : m_hdrS.call)
    bool isDict() const { return !m_useLongHdr && HDRCALL(isDict()); }
    uint8_t shareFirst() const { return HDRCALL(shareFirst()); }
    uint8_t shareLast() const { return HDRCALL(shareLast()); }
    uint8_t shareTotal() const { return HDRCALL(shareTotal()); }
    int32_t maxRelDiff() { return HDRCALL(maxRelDiff()); }

    GeoLocHdr& setShare(int32_t first, int32_t last) { HDRCALL(setShare(first, last)); return *this; }
    GeoLocHdr& setRelRec(int32_t rel) { HDRCALL(setRelRec(rel)); return *this; }
    GeoLocHdr& setUseLongHdr(bool b) { m_useLongHdr = b; return *this; }

    bool read(const uchar*& in)
    {
        m_useLongHdr = !isShortHdr(in);
        return HDRCALL(read(in, m_scratch, m_name, m_locRec));
    }

    bool skip(const uchar*& in) { return HDRCALL2(!isShortHdr(in), skip(in)); }

#   ifdef GEOLOCHDR_WRITE
       bool write(FILE* out) const { return HDRCALL(write(out, m_name, m_locRec)); }
       void dump() { HDRCALL(dump(m_name, m_locRec)); }
#   endif // GEOLOCHDR_WRITE
#undef HDRCALL

    const QString& name() const { return m_name; }
    auto locRec() const { return m_locRec; }
    void setLocRec(locRec_t locRec) { m_locRec = locRec; }

    void setCompression(int32_t start, int32_t end, int32_t lineDiff);

    bool operator<(const GeoLocHdr& rhs) const {
        return (name() != rhs.name()) ?
                    (QString::compare(name(), rhs.name(), Qt::CaseInsensitive) < 0) :
                    (locRec() < rhs.locRec());
    }

    static const constexpr locRec_t invalidRec = locRec_t(-1);

private:
    GeoLocHdrL m_hdrL;               // long header
    GeoLocHdrS m_hdrS;               // short header
    QString    m_name;               // uncompressed name
    QString    m_scratch;            // scratch space to avoid reallocations
    locRec_t   m_locRec;             // uncompressed location record number
    bool       m_useLongHdr = false; // true if its a long header
};

// Data about a single GeoLoc point we can find on the map.
class GeoLocEntry
{
public:
    enum class Feature : uint8_t
    {
        _First,
        None = _First, // unknown
        Region,        // country, state, etc.
        City,          // cities, towns, villages
        Park,          // parks
        Forest,        // forests
        Mountain,      // mountains, hills
        Water,         // lakes, streams, ponds
        Undersea,      // reefs, shoals, etc.
        _Count,
    };

    static const constexpr int tzBits = 10;
    static const constexpr int featureBits = 6;

    static_assert((tzBits + featureBits) == sizeof(uint8_t)*2*8);

    GeoLocEntry(float lat = 0.0f, float lon = 0.0f, uint16_t tz = 0, Feature feature = Feature::None) :
        m_lat(lat), m_lon(lon)
    {
        setTz(tz);
        setFeature(feature);
    }

    static QString featureName(Feature);
    static const char* featureIcon(Feature);
    static QString tooltip(Feature);
    static QString whatsthis(Feature);
    QString featureName() const { return featureName(feature()); }
    static int longestFeatureName(const QFontMetrics&);

    double lat() const { return double(m_lat); }
    double lon() const { return double(m_lon); }
    double latRad() const { return double(Math::toRad(lat())); }
    double lonRad() const { return double(Math::toRad(lon())); }

    Feature feature() const { return Feature(unpack<0, featureBits>(m_data[0])); }
    uint16_t tz() const {
        return uint16_t((unpack<featureBits, 8 - featureBits>(m_data[0]) << 8)) | m_data[1];
    }

    bool hasTz() const { return tz() != ((1U<<tzBits)-1); }

    void setFeature(Feature f) { pack<0, featureBits>(m_data[0], f); }
    void setTz(uint16_t tz) {
        pack<featureBits, 8 - featureBits>(m_data[0], tz >> 8);
        m_data[1] = tz & 0xff;
    }

    bool read(const uchar*, uint32_t record);

    // Returns approximate distance in meters.
    double greatCircleDist(const GeoLocEntry& rhs) const { return greatCircleDist(*this, rhs); }
    double greatCircleDist(double rhsLatRad, double rhsLonRad) const;
    static double greatCircleDist(const GeoLocEntry& lhs, const GeoLocEntry& rhs);

#ifdef GEOLOCHDR_WRITE
    inline bool write(FILE* out) const {
        return fwrite(this, sizeof(*this), 1, out) == 1;
    }
#endif // GEOLOCHDR_WRITE

private:
    float m_lat;
    float m_lon;
    std::array<uint8_t, 2> m_data = { };
} __attribute__((packed));

Q_DECLARE_METATYPE(GeoLocEntry::Feature)

static_assert(sizeof(GeoLocEntry) == (sizeof(float)*2 + 2), "Bad GeoLocEntry size");
static_assert(!std::is_polymorphic_v<GeoLocEntry>, "GeoLocEntry cannot be polymorphic");

template <int totalBits>
bool GeoLocHdrBase<totalBits>::read(const uchar*& in, QString& scratch, QString& name, uint32_t& record)
{
    if (in == nullptr)
        return false;

    memcpy(this, in, sizeof(*this));
    in += sizeof(*this);

    assert(!(isDict() && isRelLine()));

    if (isRelLine()) {
        assert(int32_t(record) + relRec() >= 0);
        record = uint32_t(int32_t(record) + relRec());
    } else {
        record = uint32_t((in[0] << 16) | (in[1] << 8) | (in[2] << 0));
        in += 3;
    }

    scratch = reinterpret_cast<const char*>(in);
    // Don't use scratch.size, which is in unicode char units
    while (*in++ != '\0');

    if (shareTotal() > 0) {
        scratch.append(name.rightRef(shareLast()));
        name.replace(shareFirst(), name.size() - shareFirst(), scratch);
    } else {
        name = scratch;
    }

    return true;
}

// Skip over record without reading it.
template <int totalBits>
bool GeoLocHdrBase<totalBits>::skip(const uchar*& in)
{
    if (in == nullptr)
        return false;

    const bool isRel = GeoLocHdrDefs::isRelLine(in);

    // Absolute lines stored as 24 bit integers, so skip 3 bytes for them.
    in += sizeof(*this) + (isRel ? 0 : 3);

    while (*in++ != '\0');

    return true;
}

#ifdef GEOLOCHDR_WRITE
template <int totalBits>
bool GeoLocHdrBase<totalBits>::write(FILE* out, const QString& name, uint32_t locRec) const
{
    assert(shareTotal() <= int(name.length()));
    assert(!(isDict() && isRelLine()));

    if (fwrite(this, sizeof(*this), 1, out) != 1)
        return false;

    if (!isRelLine()) {
        if (fputc((locRec >> 16) & 0xff, out) == EOF ||
            fputc((locRec >> 8) & 0xff, out) == EOF ||
            fputc((locRec >> 0) & 0xff, out) == EOF)
            return false;
    }

    if (name.length() > shareTotal()) {
        // We must calculate these sizes in unicode character space, not in 8 bit characters!
        const QStringRef notShared = name.midRef(shareFirst(), uint32_t(name.length() - shareTotal()));

        if (fputs(qUtf8Printable(notShared), out) == EOF)
            return false;
    }

    if (fputc('\0', out) == EOF)
        return false;

    return true;
}

template <int totalBits>
void GeoLocHdrBase<totalBits>::dump(const QString& name, uint32_t lineRec) const
{
    if (false)
        printf("%s%d: %d,%d,%s,%d,%s\n",
               ((totalBits == 16 && isDict()) ? "Dict: " : ""),
               totalBits, int(shareFirst()), int(shareLast()), (isRelLine() ? "Rel" : "Abs"),
               lineRec, qUtf8Printable(name));
}

#endif // GEOLOCHDR_WRITE

#endif // GEOLOCHEADER_H
