/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cmath>
#include <cassert>
#include <limits>
#include <tuple>
#include <marble/GeoDataLatLonBox.h>
#include <QApplication>
#include <QDataStream>
#include <QVector>
#include <QColor>
#include <QString>
#include <QStringList>
#include <QVariant>

#include <src/util/math.h>
#include <src/util/geomath.h>
#include <src/util/roles.h>
#include "app.h"
#include "tagflagitem.h"
#include "trackitem.h"
#include "trackmodel.h"                 // for the headers
#include "trackmodel.inl.h"
#include "pointitem.h"
#include "geopolmgr.h"

QFont TrackItem::m_smallerFont;

TrackItem::TrackItem(TreeItem *parent) :
    TreeItem(parent),
    m_geoPoints(this),
    m_isVisible(true),
    m_areaSelected(false)
{
    setupSmallFont();
}

TrackItem::TrackItem(const TreeItem::ItemData& data, TreeItem *parent) :
    TreeItem(data, parent, Util::RawDataRole),
    m_geoPoints(this),
    m_isVisible(true),
    m_areaSelected(false)
{
    setupSmallFont();
}

TrackItem::~TrackItem()
{
}

void TrackItem::setupSmallFont()
{
    m_smallerFont = QApplication::font();
    m_smallerFont.setPointSize(int(m_smallerFont.pointSizeF() * 0.75));
}

QVariant TrackItem::nthTagData(int n, ModelType column, int role) const
{
    // Little painful way to extract the string, but avoids dynamic allocations.
    const auto tags = TreeItem::data(TrackModel::Tags, Util::RawDataRole).value<QStringList>();

    if (n >= tags.size())
        return { };

    // Return requested data from the tag model, for given index in our tag string list.
    return cfgData().tags.value(tags[n], column, role);
}

QVariant TrackItem::trackColor(const QVariant& rawData) const
{
    if (rawData.type() == QVariant::Color)
        return rawData; // Use track-specific color if there is one

    if (const QVariant tagColor = nthTagData(0, TagModel::Color, Qt::BackgroundRole);
             tagColor.type() == QVariant::Color)
        return tagColor; // Use tag color if there is one.

    return cfgData().unassignedTrackColor; // Otherwise, unassigned.
}

const Units& TrackItem::units(ModelType mt) const
{
    static const Units unitsFloat(Format::Float);

    switch (mt) {
    // Use tag specific speed units, if available, else global cfgData speed units.
    case TrackModel::MinSpeed:      [[fallthrough]];
    case TrackModel::AvgOvrSpeed:   [[fallthrough]];
    case TrackModel::AvgMovSpeed:   [[fallthrough]];
    case TrackModel::MaxSpeed: {
        const auto tags = TreeItem::data(TrackModel::Tags, Util::RawDataRole).value<QStringList>();
        return tags.isEmpty() ? cfgData().unitsSpeed : cfgData().tags.units(tags.front(), TagModel::UnitSpeed);
    }

    default:
        return TrackModel::mdUnits(mt);
    }
}

QString TrackItem::tagTooltip(const QVariant& rawData) const
{
    return TagFlagItem::tagTooltip(data(TrackModel::Name, Qt::DisplayRole).toString(),
                                   rawData);
}

QString TrackItem::flagTooltip(const QVariant& rawData) const
{
    return TagFlagItem::flagTooltip(data(TrackModel::Name, Qt::DisplayRole).toString(),
                                    rawData);
}

QVariant TrackItem::data(int column, int role) const
{
    if (parent() == nullptr)
        return TreeItem::data(column, role);

    const QVariant rawData = TreeItem::data(column, Util::RawDataRole);

    switch (role) {
    case Util::RawDataRole: // small performance tweak to avoid redundant TreeItem::data()
        return rawData;

    case Qt::TextAlignmentRole:
        return { TrackModel::mdAlignment(column) };

    case Qt::SizeHintRole:
        switch (column) {
        case TrackModel::Notes: [[fallthrough]];
        case TrackModel::Tags:  return cfgData().iconSizeTrack;
        case TrackModel::Flags: return cfgData().flagSizeTrack;
        }
        break;

    case Qt::DecorationRole:
        switch (column) {
        case TrackModel::Notes:
            if (!rawData.isNull())
                return QIcon(cfgData().trackNoteIcon);
        }
        break;

    case Util::IconNameRole:
        switch (column) {
        case TrackModel::Flags: return app().geoPolMgr().flagIconNames(rawData.toStringList());
        case TrackModel::Tags:  return cfgData().tags.tagIconNames(rawData.toStringList());
        case TrackModel::Notes:
            if (!rawData.isNull())
                return cfgData().trackNoteIcon;
            break;
        }
        break;

    case Qt::ToolTipRole:
        switch (column) {
        case TrackModel::Notes: return rawData;
        case TrackModel::Tags:  return tagTooltip(rawData);
        case TrackModel::Flags: return flagTooltip(rawData);
        }

        break;

    case Qt::EditRole:
        if (column == TrackModel::Type) // in this case, return string for combo box to edit.
            return MapDataModel::trackTypeName(TrackType(rawData.toInt()));
        return rawData;
    case Util::PlotRole:
        return rawData;

    case Qt::ForegroundRole:
        if (column == TrackModel::Color) {
            if (QVariant fgColor = trackColor(TreeItem::data(column, Util::RawDataRole));
                    fgColor.type() == QVariant::Color)
                return (fgColor.value<QColor>().lightness() > 127) ? QColor(QRgb(0x00000000)) : QColor(QRgb(0x00ffffffff));
        }
        break;

    case Qt::BackgroundRole:
        if (column == TrackModel::Color)
            return trackColor(rawData);
        break;

    case Qt::FontRole:
        if (column == TrackModel::Color)
            return m_smallerFont;
        else
            return QApplication::font();

    case Qt::DisplayRole: [[fallthrough]];
    case Util::CopyRole:
        // Inidicator of how color was determined.
        if (column == TrackModel::Color && role == Qt::DisplayRole) {
            if (rawData.type() == QVariant::Color)
                return QObject::tr("(track)");
            if (nthTagData(0, TagModel::Color, Qt::BackgroundRole).type() == QVariant::Color)
                return QObject::tr("(tag)");
            return QObject::tr("(default)");
        }

        if (rawData.isValid()) {
            switch (column) {
            case TrackModel::Type:
                return TrackModel::trackTypeName(rawData.value<TrackType>());
            case TrackModel::Flags: [[fallthrough]];
            case TrackModel::Tags:
                if (role == Util::CopyRole && rawData.type() == QVariant::StringList)
                    return static_cast<const QStringList*>(rawData.constData())->join(", ");
                break;
            case TrackModel::Notes:
                if (role == Qt::DisplayRole)
                    return { }; // we render an icon.
                break;
            default:
                return units(column)(rawData);
            }
        }
    }

    // We didn't handle anything specially for the copy: get the display value instead
    if (role == Util::CopyRole)
        role = Qt::DisplayRole;

    return TreeItem::data(column, role);
}

bool TrackItem::is(TrackType type) const
{
    return data(TrackModel::Type, Util::RawDataRole).value<TrackType>() == type;
}

bool TrackItem::setData(ModelType column, QVariant value, int role, bool &changed,
                        const QModelIndex& personIdx)
{
    if (role == Qt::BackgroundRole)
        return false;

    if (role == Qt::EditRole)
        role = Util::RawDataRole;

    // Possibly convert textual type to value
    if (column == TrackModel::Type) {
        if (value.type() == QVariant::String) {
            if (const TrackType tt = MapDataModel::trackNameType(value.toString()); tt != TrackType::Unk)
                value = tt;
            else
                return false;
        }
    }

    const bool rc = TreeItem::setData(column, value, role, changed);

    // If tags are set, update power data.
    if (column == TrackModel::Tags)
        setPersonIdx(personIdx);

    return rc;
}

int TrackItem::columnCount() const
{
    return TrackModel::_Count;
}

bool TrackItem::saveRole(int role) const
{
    return TreeItem::saveRole(role) && role >= Qt::UserRole;
}

TrackItem* TrackItem::factory(const TreeItem::ItemData& data, TreeItem* parent)
{
    assert(dynamic_cast<TrackItem*>(parent) != nullptr);

    return new TrackItem(data, parent);
}

// Append segments from geoPoints.  NOTE: for efficiency, this is destructive
// to the input list: it uses std::swap.
void TrackItem::append(TrackModel& model, PointModel& newPoints)
{
    // Add it to our geo point master data
    for (auto& pointVec : newPoints) {
        m_geoPoints.append(PointModel::value_type());
        m_geoPoints.back().swap(pointVec); // for efficiency, swap instead of copy
    }

    m_geoPoints.setPowerData(firstPowerTag(), model.personIdx());
    update();
}

void TrackItem::append(const PointModel& newPoints)
{
    // Add it to our geo point master data
    for (const auto& pointVec : newPoints)
        m_geoPoints.append(pointVec);

    update();
}

// Refresh placemark data from base GeoPoints
void TrackItem::refresh(TrackModel& model)
{
    // Use current tag and person data.
    m_geoPoints.setPowerData(firstPowerTag(), model.personIdx());
    m_trackLines.clear();
    threadedUpdate(model);
}

// Update (in same thread) after geo data change
template<> void TrackItem::update(const PointModel&)
{
    m_geoPoints.invalidateFilter(*this);
    update(true);
}

// force==true to recalculate already calculated data, e.g, in case of data changes.
void TrackItem::update(bool force)
{
    updateTrackData();         // update track metadata
    updateTrackLines(force);   // create placemarks for new data
    updateFlags<TrackModel::Flags>(*this, force); // update track Flags column
}

void TrackItem::threadedUpdate(TrackModel& model)
{
    static const bool threadedUpdate = true;

    if (threadedUpdate)
        model.updateQueueAdd(this); // background thread update
    else
        update();                   // foreground version for debugging
}

// Adds any data present in the geoPoints data to the placemark data.
void TrackItem::updateTrackLines(bool force)
{
    // If segments were removed, refresh from scratch.
    if (m_geoPoints.size() < m_trackLines.size() || force)
        m_trackLines.clear();

    m_trackLines.resize(m_geoPoints.size());

    // Add it to any maps we are tracking
    auto* segLines = m_trackLines.begin();
    for (const auto& trkseg : qAsConst(m_geoPoints)) {
        int addFrom = segLines->size();

        // we already have all the points in this track segment
        if (trkseg.size() == addFrom) {
            ++segLines;
            continue;
        }

        // Items were removed.  Refresh segment lines from scratch.
        if (trkseg.size() < addFrom) {
            segLines->clear();
            addFrom = 0;
        }

        segLines->reserve(trkseg.size());
        for (const auto* pt = trkseg.begin() + addFrom; pt != trkseg.end(); ++pt)
            segLines->append(pt->as<Marble::GeoDataCoordinates>());

        ++segLines;
    }
}

void TrackItem::reverse()
{
    const QDateTime beginDate = data(TrackModel::BeginDate, Util::RawDataRole).toDateTime();
    const QDateTime endDate   = data(TrackModel::EndDate, Util::RawDataRole).toDateTime();

    m_geoPoints.reverse(*this, beginDate, endDate);
}

void TrackItem::unsetSpeed()
{
    if (auto* model = m_geoPoints.as<SpeedEditModel>())
        model->unsetSpeed();
}

bool TrackItem::pointEqual(const TrackItem& other) const
{
    return m_geoPoints == other.m_geoPoints;
}

bool TrackItem::pointEqual(const PointModel& other) const
{
    return m_geoPoints == other;
}

// This happens when the user changes the configured person, or the track tags, so it's a
// more common event than updateTrackData.  It's also an expensive calculation, so it
// happens at its own frequency.
void TrackItem::updatePowerData()
{
    const Dur_t movingTime = Dur_t(data(TrackModel::MovingTime, Util::RawDataRole).toLongLong());

    applyValueMap(m_geoPoints.calcPowerData(movingTime));
}

void TrackItem::updateHrPct(const QModelIndex& personIdx)
{
    // Update heart rate percentages from the base BPM values
    static const QVector<std::tuple<ModelType, ModelType>> hrPctColumns = {
        { TrackModel::MinHR, TrackModel::MinHRPct },
        { TrackModel::AvgHR, TrackModel::AvgHRPct },
        { TrackModel::MaxHR, TrackModel::MaxHRPct },
    };

    // Find current track start date, if any
    QDate refDate = data(TrackModel::BeginDate, Util::RawDataRole).toDate();

    if (!refDate.isValid()) // if no track start date, use current date.
        refDate = QDate::currentDate();

    // Find person's max HR on reference date
    const float maxBpm = cfgData().people.maxBpm(personIdx, refDate);

    for (const auto& col : hrPctColumns) {
        if (const QVariant hr = data(std::get<0>(col), Util::RawDataRole); maxBpm > 1.0f && hr.isValid()) {
            const float hrPct = hr.toFloat() * 60.0f / maxBpm;
            setData(std::get<1>(col), hrPct, Util::RawDataRole);
        } else {
            // Clear if either maxBpm or HR unavailable
            setData(std::get<1>(col), QVariant(), Util::RawDataRole);
        }
    }
}

void TrackItem::applyValueMap(const PointModel::ValueMap_t& values)
{
    // Unable to use range-based for, since with QMap that only provides values, and we need key+values.
    for (auto it = values.constBegin(); it != values.constEnd(); ++it)
        setData(it.key(), it.value(), Util::RawDataRole);
}

// Recalculate the track metadata from the GeoPoints.
void TrackItem::updateTrackData()
{
    applyValueMap(m_geoPoints.updateTrackData());
}

Marble::GeoDataLatLonBox TrackItem::bounds() const
{
    return { data(TrackModel::MaxLat, Util::RawDataRole).toDouble(),
             data(TrackModel::MinLat, Util::RawDataRole).toDouble(),
             data(TrackModel::MaxLon, Util::RawDataRole).toDouble(),
             data(TrackModel::MinLon, Util::RawDataRole).toDouble(),
             Marble::GeoDataCoordinates::Degree };
}

QDataStream& TrackItem::save(QDataStream& stream, const TreeModel& model) const
{
    if (stream.status() != QDataStream::Ok)
        return stream;

    // Don't invoke TreeItem::save here, which would save everything.  We'll save a subset
    // of our data.  The rest is recalculated on load.
    if (const auto rawData = m_itemData.find(Util::RawDataRole); rawData != m_itemData.end())
        for (int mt = TrackModel::_First; mt < TrackModel::_LastSavedColumn; ++mt)
            if (TrackModel::mdSave(mt))
                stream << mt << (*rawData).at(mt);

    // Save guard, and track icon.
    stream << guard << data(TrackModel::Icon, Util::IconNameRole);

    // If this is for an undo, save the persistent model index for the points
    if (model.undoing())
        stream << app().persistentIdForModel(m_geoPoints);

    // Finally, save the point data for this track.
    return stream << m_geoPoints;
}

QDataStream& TrackItem::load(QDataStream& stream, TreeModel& model)
{
    if (stream.status() != QDataStream::Ok)
        return stream;

    // Don't invoke TreeItem::load here: we have a custom save/load to handle a subset
    // of our data.  The rest is recalculated on load.
    m_itemData.clear();

    ModelType mt;
    // Insert RawDataRole entry in the itemData, which we'll load data into.
    auto& rawData = *m_itemData.insert(Util::RawDataRole, ItemData(TrackModel::_Count));
    while (true) {
        stream >> mt;           // the column being saved.
        if (mt < 0 || mt >= TrackModel::_Count)
            break;
        stream >> rawData[mt];  // the contents of the column
    }

    // Verify we didn't read corrupted data.
    if (unsigned(mt) != guard) {
        stream.setStatus(QDataStream::ReadCorruptData);
        return stream;
    }

    // Load icon data
    auto& m_iconData = *m_itemData.insert(Util::IconNameRole, ItemData(TrackModel::Icon + 1));
    stream >> m_iconData[TrackModel::Icon];

    // If this is for an undo, load the persistent model index for the points
    if (model.undoing()) {
        ModelId_t persistentId;
        stream >> persistentId;
        app().updatePersistentIdForModel(m_geoPoints, persistentId);
    }

    // Load geo data
    stream >> m_geoPoints;

    // Update everything
    createIcons();
    refresh(static_cast<TrackModel&>(model));

    return stream;
}

void TrackItem::selectPointsWithin(const Marble::GeoDataLatLonBox& region)
{
    if (!region.intersects(bounds()))
        return;

    if (m_geoPoints.selectPointsWithin(region))
        setAreaSelect(true);
}

void TrackItem::setPersonIdx(const QModelIndex& personIdx)
{
    m_geoPoints.setPowerData(firstPowerTag(), personIdx);
    updatePowerData();       // update our min/max/avg power
    updateHrPct(personIdx);  // update heart rate percentages
}

// Look for first tag that has power data, and return its index.
QModelIndex TrackItem::firstPowerTag() const
{
    const QVariant tagsVar = data(TrackModel::Tags, Util::RawDataRole);

    // Return first tag we find that has some power data attached to it.
    if (tagsVar.type() == QVariant::StringList)
        for (const auto& tag : *static_cast<const QStringList*>(tagsVar.constData()))
            if (const QModelIndex tagIdx = cfgData().tags.keyIdx(tag); tagIdx.isValid())
                if (cfgData().tags.data(TagModel::CdA, tagIdx, Util::RawDataRole).isValid() ||
                    cfgData().tags.data(TagModel::Weight, tagIdx, Util::RawDataRole).isValid() ||
                    cfgData().tags.data(TagModel::RR, tagIdx, Util::RawDataRole).isValid())
                    return tagIdx;

    return { };
}

void TrackItem::shallowCopy(const TreeItem* src)
{
    TreeItem::shallowCopy(src);

    const auto* trkSrc = static_cast<const TrackItem*>(src);

    m_geoPoints.clear();
    append(trkSrc->m_geoPoints);
    m_trackLines   = trkSrc->m_trackLines;
    m_isVisible    = trkSrc->m_isVisible;
    m_areaSelected = trkSrc->m_areaSelected;
}

uint qHash(const TrackItem& trk, uint /*seed*/)
{
    return qHash(trk.m_geoPoints); // just pass through, so we can compare hashes to PointModel;
}
