/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cstdlib>
#include <cmath>
#include <cstdint>
#include <cassert>
#include <algorithm>
#include <chrono>

#include <marble/GeoDataLatLonBox.h>
#include <marble/GeoDataCoordinates.h>

#include <QSortFilterProxyModel>

#include <src/core/modelmetadata.inl.h>

#include <src/util/math.h>
#include <src/util/geomath.h>
#include <src/util/util.h>
#include <src/util/roles.h>
#include <src/util/units.h>
#include <src/util/qtcompat.h>
#include <src/util/versionedstream.h>
#include <src/util/reverseadapter.h>
#include <src/undo/undomodel.h>

#include "app.h"
#include "pointmodel.h"
#include "pointaccum.h"
#include "pointitem.inl.h"
#include "pointitemconv.h"
#include "tagmodel.h"
#include "trackitem.h"
#include "personmodel.h"
#include "src/undo/undopoint.h"
#include "src/ui/windows/mainwindow.h"

PointModel::PointModel(TrackItem* trackItem) :
    MergeableModel(nullptr),
    m_trackItem(trackItem)
{
}

PointModel::PointModel() :
    PointModel(nullptr)
{
}

const PointItem PointModel::m_invalidPoint;

// Sort segments
void PointModel::sortTrkSegs()
{
    std::stable_sort(begin(), end(),
                     [](const value_type& lhs, const value_type& rhs) {
        return lhs.front().time() < rhs.front().time();
    });
}

// Sort points in all segments, as well as the segments after that.
void PointModel::sortItems(bool force)
{
    if (!m_timeDirty && !force)
        return;

    // TODO: handle timestamp changes that move across segments
    int parent = 0;
    for (auto& trkseg : *this) {
        std::stable_sort(trkseg.begin(), trkseg.end(),
                         [](const PointItem& lhs, const PointItem& rhs) {
            return lhs.time() < rhs.time();
        });

        if (!trkseg.empty()) {
            const QModelIndex parentIdx = index(parent, 0);
            emit dataChanged(index(0, 0, parentIdx),
                             index(trkseg.size(), columnCount(parentIdx), parentIdx));
        }

        parent++;
    }

    sortTrkSegs(); // now sort the segments

    m_timeDirty = false;
}

bool PointModel::speedToTimestamps(const QModelIndexList& selection, const QVariant& value)
{
    Dur_t adjust = Dur_t(0);

    // Convert from input units
    const Speed_t speed(PointModel::mdUnits(PointModel::Speed).fromDouble(value));

    // Put selection in a set for faster lookup
    const auto indexSet = Util::IndexSet(selection);

    for (auto& trkseg : *this) {
        PointItem* prevPt = nullptr;
        for (auto& pt : trkseg) {
            if (prevPt != nullptr) {
                const QModelIndex idx = modelIndexFor(trkseg, *prevPt, PointModel::Speed);

                if (indexSet.contains(idx) && prevPt->hasSpeed(PointItem::Either, &pt)) {
                    if (speed > Speed_t(0.1f)) {
                        const Dur_t newDur  = prevPt->length(pt) * 1000.0f / speed;
                        const Dur_t calcDur = prevPt->duration(pt);
                        adjust = (newDur - calcDur);
                    }

                    // Set measured speed if there is one.
                    if (prevPt->hasSpeed(PointItem::Measured, &pt))
                        prevPt->setSpeed(speed);
                }
            }

            pt.setTime(pt.time().addMSecs(Dur_t::base_type(adjust)));
            prevPt = &pt;
        }
    }

    return true;
}

class TrackMap;
template<>
bool PointModel::is(const TrackMap&, const QModelIndex &idx, const PointItem& point) const
{
    return getItem(idx) == &point;
}

inline void PointModel::unselectAll()
{
    for (auto& trkseg : *this)
        for (auto& pt : trkseg)
            pt.set(false, PointItem::Flags::Select);

    m_selectCount = 0;
}

inline void PointModel::select(const QModelIndex& idx, bool newState)
{
    if (auto* pt = getItem(idx); pt != nullptr)
        m_selectCount += pt->set(newState, PointItem::Flags::Select);
}

class PointPane;
template<> void PointModel::unselectAll(const PointPane&) { unselectAll(); }
template<> void PointModel::select(const PointPane&, const QModelIndex& idx, bool newState) { select(idx, newState); }

class TrackItem;
template<> void PointModel::resetHasData(const TrackItem&)
{
    m_hasData.reset();
}

template<> void PointModel::accumulateHasData(const TrackItem&, const PointItem& pt, const PointItem* nextPt)
{
    for (size_t d = _First; d < _Count; ++d)
        m_hasData[d] = m_hasData[d] || pt.hasData(ModelType(d), nextPt);
}

class AreaDialog;
template<> void PointModel::accumulateHasData(const AreaDialog&, std::bitset<_Count>& set) const
{
    set |= m_hasData;
}

template<> QModelIndex PointModel::areaExtreme(const AreaDialog&) const
{
    return m_areaExtreme;
}

template <> void PointModel::invalidateFilter(const TrackItem&)
{
    m_eleFilterSizeCache = -1;
}

int PointModel::totalSegmentPoints() const
{
    int totalPoints = 0;
    for (const auto& trkSeg : *this)
        totalPoints += trkSeg.size();

    return totalPoints;
}

class ClimbModel;
template<> QVector<PointModel::index_range> PointModel::climbs(const ClimbModel&) const
{
    QVector<index_range> climbs; // for returning: ranges by QModelIndex
    climbs.reserve(32);

    const int totalPoints = totalSegmentPoints();
    if (totalPoints <= 1)
        return climbs;

    QVector<const PointItem*> pt;  // flatten this out for convenience
    pt.reserve(totalPoints);

    // Collect all the point grades
    for (const auto& trkSeg : *this) // for each segment
        for (const auto& trkPt : trkSeg)   // for each point in the segment
            pt.append(&trkPt);

    const PointItem::Grade_t minHillGrade    = cfgData().hillMinGrade;  // in %
    const PointItem::Ele_t   minHillHeight   = cfgData().hillMinHeight; // in m
    const PointItem::Dist_t  hillGradeLength = cfgData().hillGradeLength;
    const PointItem::Dist_t  hillSegLength   = 100.0f;   // in meters.
    const PointItem::Dist_t  maxSearch       = 25000.0f; // in meters, to limit horizontal hill search

    // Gizmos to find length and vert distance for any two points, supplied either order.
    const auto length = [&pt](int p0, int p1) { return pt.at(std::min(p0, p1))->length(*pt.at(std::max(p0, p1))); };
    const auto vert   = [&pt](int p0, int p1) { return pt.at(std::min(p0, p1))->vert(*pt.at(std::max(p0, p1)), false); };
    const auto grade  = [&pt](int p0, int p1) { return pt.at(std::min(p0, p1))->grade(*pt.at(std::max(p0, p1)), false); };

    // Return point that's at least some minimum distance from point p0, starting the search at p1.
    const auto minDistancePoint = [&](int p0, int p1, PointItem::Dist_t minDist, int inc) {
        while ((p1 + inc) >= 0 && (p1 + inc) < totalPoints && std::abs(length(p0, p1)) < minDist)
            p1 += inc;
        return p1;
    };

    const auto accept = [&](int firstPt, int lastPt) {
        return std::abs(grade(firstPt, lastPt))  >= minHillGrade &&
               std::abs(vert(firstPt, lastPt))   >= minHillHeight;
    };

    const auto trim = [&](int x0, int x1, int inc, bool isUp) {
        const float up = isUp ? 1.0f : -1.0f;
        int newX = x0;
        for (int x = x0 + inc; x != x1 && x>=0 && x<totalPoints; x += inc) {
            if (vert(x0, x) * up < 2.0f && grade(x0, x) * up < minHillGrade * 0.25f)
                newX = x;
        }
        return newX;
    };

    // find the steepest short section
    const auto steepest = [&](int first, int last, bool isUp) {
        PointItem::Grade_t steepest = 0.0f;
        for (int g0 = first, g1 = first + 1; g0 <= last && g1 <= last; ++g0) {
            if (g1 = minDistancePoint(g0, g1, hillGradeLength, 1); length(g0, g1) >= hillGradeLength) {
                if (isUp) steepest = std::max(steepest, grade(g0, g1));
                else      steepest = std::min(steepest, grade(g0, g1));
            }
        }
        return steepest;
    };

    int prevEnd = 0;
    for (int x0 = 0, x1 = 1; x0 < totalPoints; ++x0) {
        if (x1 = minDistancePoint(x0, std::max(x0+1, x1), hillSegLength, 1); x1 >= totalPoints) // find end of minimum hill distance
            break;

        // We must have at least one range that passes our grade threshold.
        if (std::abs(grade(x0, x1)) < minHillGrade)
            continue;

        const bool isUp  = (vert(x0, x1) > 0.0f);
        int        first = x0;
        int        last  = x1;

        for (int t = 0; t < 2; ++t) { // repeat twice, in case we can expand the other end more after finding the top or bottom
            const int x0Init = first;
            const int x1Init = last;
            // Expand range to apparent end of hill.  This is heuristic, not perfect.  We alternate which end we try to expand,
            // to avoid pushing one out to a shallow grade and being unable to expand the other.
            bool endLeft = false, endRight = false;
            for (int inc = 1; !(endLeft && endRight); inc = (inc > 0) ? -inc : (-inc + 1)) {
                int&       expandEnd  = (inc > 0) ? last : first;
                int&       fixedEnd   = (inc > 0) ? first : last;
                const int  candidate  = ((inc > 0) ? x1Init : x0Init) + inc;

                endLeft  |= (candidate < prevEnd);      // latches for halting search to left...
                endRight |= (candidate >= totalPoints); // ... and right

                if (candidate < prevEnd || candidate >= totalPoints) // don't look off end of valid points
                    continue;

                const PointItem::Ele_t   vertRange      = vert(first, last);          // Total vert range for hill so far
                const PointItem::Ele_t   vertCandidate  = vert(fixedEnd, candidate);  // Vert to candidate point
                const PointItem::Grade_t gradeCandidate = grade(fixedEnd, candidate); // Grade to candidate point

                if ((isUp  && gradeCandidate >= +minHillGrade && vertCandidate > vertRange) ||
                    (!isUp && gradeCandidate <= -minHillGrade && vertCandidate < vertRange)) {
                    expandEnd = candidate;
                  } else {
                    if (length(fixedEnd, candidate) > maxSearch)
                        break;
                }
            }
        }

        // Trim range ends if there are sections which don't contribute meaningfully to the height
        first = trim(first, last, 1, isUp);
        last = trim(last, first, -1, isUp);

        if (accept(first, last)) {// Whee! A hill!
            bool use = true;

            // If it overlaps the prior entry, it means we found a better starting place: pick the highest one
            if (!climbs.isEmpty()) {
                const int backFirst = pointIndex(std::get<0>(climbs.back()));
                const int backLast  = pointIndex(std::get<1>(climbs.back()));

                if (first < backLast) {
                    if (std::abs(vert(first, last)) > std::abs(vert(backFirst, backLast)))
                        climbs.pop_back();
                    else
                        use = false;
                }
            }

            if (use)
                climbs.append(std::make_tuple(modelIndexFor(first), modelIndexFor(last),
                                              steepest(first, last, isUp)));
        }

        prevEnd = first;
        x0 = x1 = std::max(x0, last + 1);  // move to point just after the hill we just detected
    }

    return climbs;
}

// Converts lon/lat to projected X/Y
std::pair<PointItem::Lon_t, PointItem::Lat_t> PointModel::projectPt(const PointItem* pt)
{
    static const qreal NaN = std::numeric_limits<qreal>::quiet_NaN();
    static const qreal lat_dist = Math::toRad(1.0) * GeoMath::earthRadiusM;

    if (pt == nullptr)
        return std::make_pair(Lon_t(NaN), Lat_t(NaN));

    return std::make_pair(pt->lon() * cos(Math::toRad(qreal(pt->lat()))) * lat_dist,
                          pt->lat() * lat_dist);
}

// Helper to return data, or an empty variant if it was unset (nan)
template <typename T> QVariant issetf(T d)
{
    return std::isnan(d) ? QVariant() : double(d);
}

template <typename T> QVariant issetf0(T d)
{
    return d == 0.0 ? QVariant() : double(d);
}

void PointModel::updateHasData()
{
    resetHasData(*m_trackItem);

    // For each segment in the track
    for (const auto& trkSeg : *this) {
        const PointItem* prevPt = nullptr;
        for (const auto& pt : trkSeg) {
            if (prevPt != nullptr)
                accumulateHasData(*m_trackItem, *prevPt, &pt);
            prevPt = &pt;
        }
    }
}

PointModel::ValueMap_t PointModel::calcPowerData(PointItem::Dur_t movingTimeMs,
                                                 PointItem::Flags flags,
                                                 const QModelIndex& beginIdx,
                                                 const QModelIndex& endIdx) const
{
    static const qreal NaN  = std::numeric_limits<qreal>::quiet_NaN();

    PointItem::Dur_t   powerTime   = Dur_t(0);   // time under non-zero power
    PointItem::Power_t minPower    = NaN;
    PointItem::Power_t avgMovPower = NaN;
    PointItem::Power_t maxPower    = NaN;
    PointItem::Power_t inputWattHr = NaN;
    PointItem::Power_t wattmsec    = 0.0;          // for average power, watt*milliseconds

    if (movingTimeMs == 0)
        return { };

    for (const auto& trkSeg : SegRange_t(*this, beginIdx, endIdx)) {
        const PointItem* prevPt = nullptr;
        for (const auto& pt : PtRange_t(*this, trkSeg, beginIdx, endIdx)) {
            if (!pt.test(flags))  // skip if it doesn't satisfy the flags we want
                continue;

            if (prevPt != nullptr && prevPt->hasPower(PointItem::Either, &pt)) {
                if (const PointItem::Power_t power = prevPt->power(powerData(), pt); power > 1.0) {
                    minPower  = std::fmin(minPower, power);
                    maxPower  = std::fmax(maxPower, power);

                    const PointItem::Dur_t ptDur = prevPt->duration(pt);
                    wattmsec  += qreal(power) * qreal(ptDur);
                    powerTime += ptDur;
                }
            }

            prevPt = &pt;
        }
    }

    if (powerTime == 0) // no time under power
        return { };

    if (!std::isnan(minPower) && !std::isnan(maxPower)) {
        avgMovPower = wattmsec / qreal(powerTime);
        inputWattHr = (wattmsec * 1.0_DatemS / 1.0_DateH) / qreal(std::max(powerData().efficiency, 0.001f));
    }

    const ValueMap_t values = {
        { TrackModel::MinPower,    issetf(minPower) },
        { TrackModel::AvgMovPower, issetf(avgMovPower) },
        { TrackModel::MaxPower,    issetf(maxPower) },
        { TrackModel::Energy,      issetf(inputWattHr) },
    };

    return values;
}

PointModel::ValueMap_t PointModel::updateTrackData(PointItem::Flags flags,
                                                   const QModelIndex& beginIdx,
                                                   const QModelIndex& endIdx)
{
    updateStartIdx();
    filter();  // Apply smoothing filters to the data
    updateHasData();

    return calcTrackData(flags, beginIdx, endIdx);
}

template <typename T>
inline QVariant issetu8(T d, double conv = (1.0/60.0))
{
    return (d == std::numeric_limits<decltype(d)>::max()) ? QVariant() : double(d) * conv;
}

PointModel::ValueMap_t PointModel::calcTrackData(PointItem::Flags flags,
                                                 const QModelIndex& beginIdx,
                                                 const QModelIndex& endIdx) const
{
    static const qreal NaN  = std::numeric_limits<qreal>::quiet_NaN();

    PointItem::Dist_t  length        = 0.0;  // point-to-next
    PointItem::Dist_t  distance      = 0.0;  // whole track (or selected subset thereof)
    QDateTime          beginDate;  // default constructor sets invalid flag
    QDateTime          endDate;    // ...
    PointItem::Dur_t   stoppedTime   = Dur_t(0);
    PointItem::Dur_t   movingTime    = Dur_t(0);
    PointItem::Dur_t   pedallingTime = Dur_t(0);
    PointItem::Dur_t   totalTime     = Dur_t(0);
    PointItem::Ele_t   minEle        = PointItem::badEle;
    PointItem::Ele_t   avgEle        = 0.0;
    PointItem::Ele_t   maxEle        = PointItem::badEle;
    PointItem::Ele_t   beginEle      = PointItem::badEle;
    PointItem::Ele_t   endEle        = PointItem::badEle;
    PointItem::Speed_t minSpeed      = Speed_t(NaN);
    PointItem::Speed_t avgOvrSpeed   = Speed_t(NaN);
    PointItem::Speed_t avgMovSpeed   = Speed_t(NaN);
    PointItem::Speed_t maxSpeed      = Speed_t(NaN);
    PointItem::Grade_t minGrade      = NaN;
    PointItem::Grade_t avgGrade      = NaN;
    PointItem::Grade_t maxGrade      = NaN;
    PointItem::Cad_t   minCad        = PointItem::badCad;
    PointItem::Cad_t   avgMovCad     = PointItem::badCad;
    PointItem::Cad_t   maxCad        = PointItem::badCad;
    PointItem::Ele_t   ascent        = 0.0f;
    PointItem::Ele_t   descent       = 0.0f;
    PointItem::Ele_t   baseToPeak    = 0.0f;
    int                points        = 0;
    qreal              area          = NaN;
    PointItem::Temp_t  minTemp       = NaN;
    PointItem::Temp_t  avgTemp       = NaN;
    PointItem::Temp_t  maxTemp       = NaN;
    PointItem::Hr_t    minHR         = PointItem::badHr;
    PointItem::Hr_t    avgHR         = PointItem::badHr;
    PointItem::Hr_t    maxHR         = PointItem::badHr;
    PointItem::Lon_t   minLon        = NaN;
    PointItem::Lon_t   maxLon        = NaN;
    PointItem::Lat_t   minLat        = NaN;
    PointItem::Lat_t   maxLat        = NaN;

    CadAccum_t         cadmsec       = 0; // for average pedal RPM
    HrAccum_t          hrmsec        = 0; // for average heart rate
    TempAccum_t        tempmsec      = 0; // for average temperature
    PointItem::Dur_t   duration      = Dur_t(0); // from prior point in track

    const PointItem* lastPt = nullptr;
    if (!empty() && !back().empty())
        lastPt = &back().back();

    auto [prevProjX, prevProjY] = projectPt(lastPt);

    // For each segment in the track
    for (const auto& trkSeg : SegRange_t(*this, beginIdx, endIdx)) {
        const PointItem* prevPt = nullptr;

        PointItem::Speed_t smoothSpeed = Speed_t(0.0f);  // for smoothing
        PointItem::Speed_t prevSpeed = Speed_t(0.0f);
        points += trkSeg.size();

        if (std::isnan(area) && points >= 3)
            area = 0.0;

        // For each point in the segment
        for (const auto& pt : PtRange_t(*this, trkSeg, beginIdx, endIdx)) {
            if (!pt.test(flags))  // skip if it doesn't satisfy the flags we want
                continue;

            if (prevPt != nullptr) {
                // For avg moving cadence/hr
                duration = prevPt->duration(pt);
                if (prevPt->hasCad() && prevPt->cad() > 0) {
                    cadmsec  += prevPt->cad() * duration;
                    pedallingTime += duration;
                }
                if (prevPt->hasHr())
                    hrmsec   += prevPt->hr() * duration;
                if (prevPt->hasAtemp())
                    tempmsec += prevPt->atemp() * duration;

                if (prevPt->hasLength(&pt)) {
                    length = prevPt->length(pt);
                    distance += length;

                    if (prevPt->hasSpeed(PointItem::Either, &pt))
                        prevSpeed = prevPt->speed(&pt);

                    // Smooth out the speed a little (there are occasional spikes)  TODO: use convolution
                    smoothSpeed          = Math::mix(smoothSpeed, prevSpeed, Speed_t(0.25));
                    const bool isMoving  = PointItem::isMoving(smoothSpeed);

                    // Times, in mS
                    totalTime   += duration;
                    movingTime  += (isMoving ? duration : Dur_t(0));
                    stoppedTime += (isMoving ? Dur_t(0) : duration);

                    // Calculate ascent, descent, and grade
                    if (isMoving) {
                        minSpeed = std::fmin(minSpeed, smoothSpeed);
                        maxSpeed = std::fmax(maxSpeed, smoothSpeed);

                        // Early track data often has dodgy elevations, so skip that.
                        if (prevPt->hasGrade()) {
                            const PointItem::Ele_t vert = prevPt->vert(pt);
                            const PointItem::Grade_t grade = prevPt->grade();
                            ascent  += (grade >= 0.0f) ?  vert : 0.0f;
                            descent += (grade < 0.0f)  ? -vert : 0.0f;

                            minGrade = std::fmin(minGrade, grade);
                            maxGrade = std::fmax(maxGrade, grade);
                        }
                    }
                }
            }

            if (pt.hasLoc()) {
                const auto lon = pt.lon();
                const auto lat = pt.lat();
                minLon    = std::fmin(minLon,   lon);
                maxLon    = std::fmax(maxLon,   lon);
                minLat    = std::fmin(minLat,   lat);
                maxLat    = std::fmax(maxLat,   lat);
            }

            if (pt.hasTime()) {
                beginDate = std::min(beginDate.isValid() ? beginDate : pt.time(), pt.time());
                endDate   = std::max(endDate.isValid() ? endDate : pt.time(),  pt.time());
            }

            if (pt.hasEle()) {
                const PointItem::Ele_t ele = pt.ele();
                minEle    = std::fmin(minEle,   ele);
                maxEle    = std::fmax(maxEle,   ele);
                if (std::isnan(beginEle))
                    beginEle = ele;
                endEle = ele;
                avgEle += ele * PointItem::Dist_t::base_type(length);
            }

            if (pt.hasCad() && pt.cad() > 5) { // ignore very low values
                minCad    = std::min(minCad,    pt.cad());
                maxCad    = std::max((maxCad == PointItem::badCad) ? decltype(maxCad)(0) : maxCad, pt.cad());
            }

            if (pt.hasAtemp()) {
                const Temp_t atemp = pt.atemp();
                minTemp   = std::fmin(minTemp,  atemp);
                maxTemp   = std::fmax(maxTemp,  atemp);
            }

            if (pt.hasHr() && pt.hr() > 40) { // ignore very low values
                minHR     = std::min(minHR,     pt.hr());
                maxHR     = std::max((maxHR == PointItem::badHr) ? decltype(maxHR)(0) : maxHR, pt.hr());
            }

            if (!std::isnan(area)) {
                const auto& [projX, projY] = projectPt(&pt);
                area     += qreal(projX + prevProjX) * qreal(projY - prevProjY);
                prevProjX = projX;
                prevProjY = projY;
            }

            prevPt = &pt;
        }
    }

    // Compute average HR and temperature
    if (totalTime > 0) {
        avgOvrSpeed = distance * 1000.0 / totalTime;

        if (!(minHR == maxHR && minHR == PointItem::badHr))
            avgHR  = uint8_t(hrmsec / totalTime);

        if (tempmsec > 0)
            avgTemp = uint8_t(tempmsec / totalTime);
    }

    // Compute moving averages for speed and cadance
    if (movingTime > 0)
        avgMovSpeed = distance * 1000.0 / movingTime;

    if (pedallingTime > 0) {
        if (!(minCad == maxCad && minCad == PointItem::badCad))
            avgMovCad   = uint8_t(cadmsec / pedallingTime);
    }

    // calculate average grade
    if (distance > 0.0) {
        if (!std::isnan(beginEle) && !std::isnan(endEle))
            avgGrade = PointItem::Ele_t::base_type(endEle - beginEle) / PointItem::Dist_t::base_type(distance);
        avgEle /= qreal(distance);
    }

    area = std::abs(area) * 0.5;

    // Units class wants nS
    stoppedTime *= mStonS;
    movingTime  *= mStonS;
    totalTime   *= mStonS;

    // Time of day (not including date).  Because QTime has no notion of time zones, we have to
    // do the conversion here.
    const QTime beginTime = cfgData().unitsTrkTime.isUTC() ? beginDate.time() : beginDate.toLocalTime().time();
    const QTime endTime   = cfgData().unitsTrkTime.isUTC() ? endDate.time() : endDate.toLocalTime().time();

    // base/peak diff
    baseToPeak = maxEle - minEle;

    // We return a map of values rather than directly setting the model, so that the values can be used for
    // other purposes as well such as creating tooltips of a subset of the points in the model.
    ValueMap_t values = {
        { TrackModel::Length,        issetf0(distance) },
        { TrackModel::BeginDate,     beginDate },
        { TrackModel::EndDate,       endDate },
        { TrackModel::BeginTime,     beginTime },
        { TrackModel::EndTime,       endTime },
        { TrackModel::StoppedTime,   Dur_t::base_type(stoppedTime) },
        { TrackModel::MovingTime,    Dur_t::base_type(movingTime) },
        { TrackModel::TotalTime,     Dur_t::base_type(totalTime) },
        { TrackModel::MinElevation,  issetf(minEle) },
        { TrackModel::AvgElevation,  issetf(avgEle) },
        { TrackModel::MaxElevation,  issetf(maxEle) },
        { TrackModel::MinSpeed,      issetf(minSpeed) },
        { TrackModel::AvgOvrSpeed,   issetf(avgOvrSpeed) },
        { TrackModel::AvgMovSpeed,   issetf(avgMovSpeed) },
        { TrackModel::MaxSpeed,      issetf(maxSpeed) },
        { TrackModel::MinGrade,      issetf(minGrade) },
        { TrackModel::AvgGrade,      issetf(avgGrade) },
        { TrackModel::MaxGrade,      issetf(maxGrade) },
        { TrackModel::MinCad,        issetu8(minCad) },
        { TrackModel::AvgMovCad,     issetu8(avgMovCad) },
        { TrackModel::MaxCad,        issetu8(maxCad) },
        { TrackModel::Ascent,        issetf0(ascent) },
        { TrackModel::Descent,       issetf0(descent) },
        { TrackModel::BasePeak,      issetf(baseToPeak) },
        { TrackModel::Segments,      size() },
        { TrackModel::Points,        points },
        { TrackModel::Area,          issetf(area) },
        { TrackModel::MinTemp,       issetf(minTemp) },
        { TrackModel::AvgTemp,       issetf(avgTemp) },
        { TrackModel::MaxTemp,       issetf(maxTemp) },
        { TrackModel::MinHR,         issetu8(minHR) },
        { TrackModel::AvgHR,         issetu8(avgHR) },
        { TrackModel::MaxHR,         issetu8(maxHR) },
        { TrackModel::MinLon,        issetf(minLon) },
        { TrackModel::MinLat,        issetf(minLat) },
        { TrackModel::MaxLon,        issetf(maxLon) },
        { TrackModel::MaxLat,        issetf(maxLat) },
        // Internal use, not for interface:
        { TrackModel::BeginToEndEle, issetf(endEle - beginEle) }
    };

    // Update power after we've worked out the moving time above
    return QtCompat::Merge(values, calcPowerData(movingTime, flags, beginIdx, endIdx));
}

// Also mark the owning track as dirty when we are changed.
void PointModel::emitDirtyStateChanged(bool dirty)
{
    ChangeTrackingModel::emitDirtyStateChanged(dirty);

    app().trackModel().setDirty(dirty);
}

void PointModel::processDataChanged()
{
    if (m_trackItem == nullptr)
        return;

    sortItems();          // sort points by time
    updateTrackItem();    // update track level data

    // Refresh display
    if (rowCount() > 0) {
        SignalBlocker block(*this); // don't track these in undos
        emit dataChanged(createIndex(0, 0, quintptr(-1)),
                         createIndex(rowCount()-1, PointModel::_Count-1, quintptr(-1)));
    }

    setDirty();
}

// We keep a cache of the index start for each segment, for performance when returning
// same in data() method.
void PointModel::updateStartIdx()
{
    m_startIdxCache.clear();
    m_startIdxCache.reserve(size());

    uint start = 0;
    for (const auto& it : *this) {
        m_startIdxCache.append(start);
        start += uint(it.size());
    }
}

template <typename T>
inline T PointModel::convolve(T PointItem::* val, value_type& trkSeg, int i, int fHalf)
{
    T sum(0.0);
    qreal weight(0.0);

    // Process convolution neighborhood
    for (int j = i-fHalf; j <= i+fHalf; ++j) {
        if (j < 0 || j >= trkSeg.size())
            continue;

        const qreal dDist = qreal(std::abs(trkSeg[i].length(trkSeg[j])));
        const qreal factor = 25.0 / std::max(dDist, 25.0); // roll off contribution outside 25m

        weight += factor;
        sum    += (trkSeg[j].*val) * factor;
    }

    return sum / weight;
}

inline void PointModel::filter(value_type& trkSeg)
{
    // Don't re-filter if last used filter size matches current one
    const bool fltEle = (m_eleFilterSizeCache != cfgData().eleFilterSize);

    if (!fltEle)
        return;

    if (trkSeg.size() <= 3) {
        for (auto& pt : trkSeg)
            pt.m_fltEle = PointItem::badEle;
        return;
    }

    const int fHalfEle = (cfgData().eleFilterSize | 0x1) * 0.5; // half of next higher odd numer

    for (int i = 0; i < trkSeg.size(); ++i)
        trkSeg[i].m_fltEle = convolve(&PointItem::m_ele, trkSeg, i, fHalfEle);
}

std::tuple<Lat_t, Lon_t, Lat_t, Lon_t, bool> PointModel::bounds(const QModelIndexList& selection) const
{
    PointItem::Lat_t maxlat = PointItem::badLat, minlat = PointItem::badLat;
    PointItem::Lon_t maxlon = PointItem::badLon, minlon = PointItem::badLon;

    const auto minmax = [&](const PointItem* pt) {
        if (pt != nullptr) {
            maxlat = std::fmax(maxlat, pt->lat());
            maxlon = std::fmax(maxlon, pt->lon());
            minlat = std::fmin(minlat, pt->lat());
            minlon = std::fmin(minlon, pt->lon());
        }
    };

    for (const auto& idx : selection) {
        if (isSegment(idx)) { // segment level
            for (const auto& pt : at(idx.row()))
                minmax(&pt);
        } else {              // point level
            minmax(getItem(idx));
        }
    }

    return std::make_tuple(maxlat, maxlon, minlat, minlon,
                           (!std::isnan(maxlat) && !std::isnan(maxlon) && !std::isnan(minlat) && !std::isnan(minlon)));
}

std::tuple<Lat_t, Lon_t, Lat_t, Lon_t, bool> PointModel::bounds(PointItem::Flags flags) const
{
    PointItem::Lat_t maxlat = PointItem::badLat, minlat = PointItem::badLat;
    PointItem::Lon_t maxlon = PointItem::badLon, minlon = PointItem::badLon;

    for (const auto& trkseg : *this) {
        for (const auto& pt : trkseg) {
            if (pt.test(flags)) {
                maxlat = std::fmax(maxlat, pt.lat());
                maxlon = std::fmax(maxlon, pt.lon());
                minlat = std::fmin(minlat, pt.lat());
                minlon = std::fmin(minlon, pt.lon());
            }
        }
    }

    return std::make_tuple(maxlat, maxlon, minlat, minlon,
                           (!std::isnan(maxlat) && !std::isnan(maxlon) && !std::isnan(minlat) && !std::isnan(minlon)));
}

Marble::GeoDataLatLonBox PointModel::boundsBox(const QModelIndexList &selection) const
{
    const auto [maxlat, maxlon, minlat, minlon, valid] = bounds(selection);

    if (!valid)
        return { };

    return { double(maxlat), double(minlat), double(maxlon), double(minlon), Marble::GeoDataCoordinates::Degree };
}

Marble::GeoDataLatLonBox PointModel::boundsBox(PointItem::Flags flags) const
{
    const auto [maxlat, maxlon, minlat, minlon, valid] = bounds(flags);

    if (!valid)
        return { };

    return { double(maxlat), double(minlat), double(maxlon), double(minlon), Marble::GeoDataCoordinates::Degree };
}

// Determine average time spacing of sample points
qreal PointModel::avgTimeSpacing(const value_type& trkSeg)
{
    if (trkSeg.size() < 2)  // not enough data to filter
        return 0.0;

    return qreal(trkSeg.back().time().msecsTo(trkSeg.front().time())) / trkSeg.size();
}

void PointModel::calcDistValues(value_type& pt, qreal& accumDistance)
{
    if (pt.isEmpty())
        return;

    PointItem::Dist_t length = 0.0;

    const int size = pt.size();
    for (int i = 0; i < size; ++i) {
        if (i > 0)
            length = pt[i-1].greatCircleDist(pt[i]);

        accumDistance += qreal(length);
        pt[i].setDistance(accumDistance);
    }
}

// Calculates leg distance (to next point, not previous!)
void PointModel::calcPointValues(value_type& pt, const PointItem* firstPt)
{
    if (pt.isEmpty())
        return;

    PointItem::Grade_t smoothGrade = 0.0f;  // for smoothing
    bool hasGrade                  = false;
    PointItem::Elaps_t prevElapsed = 0;
    PointItem::Elaps_t elapsed     = 0;
    PointItem::Speed_t prevSpeed   = Speed_t(0.0f);
    PointItem::Speed_t speed       = Speed_t(0.0f);
    PointItem::Dist_t  prevLength  = 0.0;

    const int size = pt.size();
    for (int i = 1; i < size; ++i) {
        prevLength = pt[i-1].length(pt[i]);
        prevElapsed = elapsed;
        elapsed = pt[i-1].elapsed(*firstPt);

        // Once in a while, for some reason, tracks have points out of order, resulting in
        // negative durations.  This fixes that by sorting and starting over.  Thankfully,
        // this seems quite rare.
        if (elapsed < prevElapsed) {
            sortItems(true);
            i = 0;
            continue;
        }

        prevSpeed = speed;
        speed = pt[i-1].speed(&pt[i]);

        if (PointItem::isMoving(speed) && prevLength > 2.0) {
            const PointItem::Grade_t ptGrade = pt[i-1].grade(pt[i]);
            // Consider shallower grades as more reliable for the running average than steeper ones,
            // to hopefully avoid too much contribution from one or two bad data points.
            const auto ratio = std::clamp(1.0f - std::fabs(ptGrade) * 5.0f, 0.05f, 0.75f);
            smoothGrade = Math::mix(smoothGrade, ptGrade, ratio);
            hasGrade = (std::fabs(smoothGrade) < 0.15f) || (elapsed > 120000);
        }

        if (hasGrade)
            pt[i-1].setGrade(smoothGrade);
    }
}

// Apply time smart smoothing convolutions (nondestructive to original data)
void PointModel::filter()
{
    qreal accumDistance = 0.0;

    // Process each separate segment individually: no filtering across segments.
    for (auto& trkSeg : *this) {
        calcDistValues(trkSeg, accumDistance);  // do after filtering, so it can use filtered elevations
        filter(trkSeg);
        calcPointValues(trkSeg, firstPointInTrack());  // do after filtering, so it can use filtered elevations
    }

    m_eleFilterSizeCache = cfgData().eleFilterSize;
}

// Return the first track point, or the invalid point
const PointItem* PointModel::firstPointInTrack() const
{
    return (!isEmpty() && !at(0).isEmpty()) ? &at(0).at(0) : nullptr;
}

const PointItem* PointModel::nextPointInSeg(const PointItem& pt) const
{
    // Find segment containing point
    const auto* segIt = std::lower_bound(begin(), end(), pt.distance(), [](const value_type& trkSeg, PointItem::Dist_t dist) {
        return !trkSeg.empty() && trkSeg.last().distance() < dist;
    });

    if (segIt == end())
        return nullptr;

    return nextPointInSeg(index(&pt - segIt->begin(), 0, index(segIt - begin(), 0)));
}

const PointItem* PointModel::nextPointInSeg(const QModelIndex& idx) const
{
    if (isSegment(idx))
        return nullptr;

    const int segId = int(idx.internalId());
    const auto& seg = at(segId);

    // It's not the last one in the segment
    if ((idx.row() + 1) < seg.size())
        return &seg.at(idx.row() + 1);

    return nullptr;
}

const PointItem* PointModel::prevPointInSeg(const QModelIndex& idx) const
{
    if (isSegment(idx))
        return nullptr;

    const int segId = int(idx.internalId());
    const auto& seg = at(segId);

    // It's not the first one in the segment
    if (idx.row() > 0)
        return &seg.at(idx.row() - 1);

    return nullptr;
}

const QDateTime& PointModel::trackEndTime() const
{
    static const QDateTime invalid;

    if (isEmpty())
        return invalid;

    const int lastSegNum = size() - 1;
    const auto& lastSeg = at(lastSegNum);

    return (!lastSeg.isEmpty()) ? lastSeg.back().time() : invalid;
}

qreal PointModel::trackTotalDistance() const
{
    return (!isEmpty() && !last().isEmpty()) ? qreal(last().last().distance()) : 0.0;
}

int PointModel::trackTotalPoints() const
{
    int total = 0;
    for (const auto& trkSeg : *this)
        total += trkSeg.size();

    return total;
}

QModelIndex PointModel::closestPoint(PointItem::Dist_t distance) const
{
    // Don't do this if there are no segments
    if (isEmpty())
        return { };

    // Find first segment starting after given distance
    const auto* segIt = std::upper_bound(cbegin(), cend(), distance, [](PointItem::Dist_t dist, const value_type& seg) {
       if (seg.isEmpty())
          return false;
       return dist < seg.first().distance();
    });

    // back up one segment to get to last seg starting before given distance.
    if (segIt != cbegin())
        --segIt;

    while (segIt != cend() && segIt->isEmpty()) // Skip any empty segments
        ++segIt;

    if (segIt == cend()) // return if all segments were empty
        return { };

    const QModelIndex segIdx = index(segIt - cbegin(), 0);

    // We found a non-empty segment.  Find first point > distance
    const auto* pointIt = std::upper_bound(segIt->cbegin(), segIt->cend(), distance, [](PointItem::Dist_t dist, const PointItem& point) {
        return dist < point.distance();
    });

    // Back up one point to get last point before given distance
    if (pointIt != segIt->cbegin())
        --pointIt;

    // Switch to next point if it's closer
    if ((pointIt + 1) < segIt->cend())
        if (std::abs((pointIt + 1)->distance() - distance) < std::abs(pointIt->distance() - distance))
            ++pointIt;

    // Return index for this point
    return index(pointIt - segIt->cbegin(), 0, segIdx);
}

QModelIndex PointModel::modelIndexFor(uint idx, ModelType col) const
{
    if (m_startIdxCache.isEmpty())
        return { };

    // Get segment iterator
    const auto* segIt = std::upper_bound(m_startIdxCache.cbegin(), m_startIdxCache.cend(), idx);

    if (segIt != m_startIdxCache.cbegin()) // back up to segment starting before our idx
        --segIt;

    const QModelIndex segIdx   = index(segIt - m_startIdxCache.cbegin(), 0);
    const QModelIndex modelIdx = index(idx - *segIt, col, segIdx);

    assert(data(modelIdx, PointModel::Index).toUInt() == idx);

    return modelIdx;
}

QModelIndex PointModel::modelIndexFor(const PointModel::segment_type& trkseg) const
{
    return index(&trkseg - cbegin(), 0);
}

QModelIndex PointModel::modelIndexFor(const PointModel::segment_type& trkseg, const PointItem& pt, ModelType col) const
{
    return index(&pt - trkseg.cbegin(), col, modelIndexFor(trkseg));
}

bool PointModel::allSegments(const QModelIndexList& selections)
{
    return std::all_of(selections.begin(), selections.end(), isSegment);
}

bool PointModel::allPoints(const QModelIndexList& selections)
{
    return std::all_of(selections.begin(), selections.end(), isPoint);
}

// Return true if all indexes are points in the same segment
bool PointModel::sameSegment(const QModelIndexList& selections)
{
    QModelIndex parent;

    for (const auto& idx : selections) {
        if (!isPoint(idx))
            return false;
        if (!parent.isValid())
            parent = idx.parent();

        if (parent != idx.parent())
            return false;
    }

    return true;
}

// Wrapper to supply some data held by the PointModel such as power, and call the PointItem::data
inline QVariant PointModel::data(const PointItem& item, ModelType col, const QModelIndex& idx, int role, bool filtered) const
{
    return item.data(col, pointIndex(idx), role, filtered,
                     firstPointInTrack(),
                     nextPointInSeg(idx),
                     powerData());
}

QString PointModel::multiPointTooltip() const
{
    if (m_trackItem == nullptr) {
        assert(0);
        return "N/A";  // should not happen: we must always have a m_trackItem
    }

    // Aggregate min/max/avg/etc values for selected PointItems
    const auto values = PointModel::calcTrackData(PointItem::Flags::Select);

    struct Line {
        QString            m_title;    // column title
        ModelType          m_pointCol; // for colorization, if the track colorizer didn't
        QVector<ModelType> m_col;      // vector of entries for this table row
    };

    static const QVector<Line> tooltipInfo = {
        { TrackModel::mdName(TrackModel::TotalTime), -1,
          { TrackModel::TotalTime } },
        { TrackModel::mdName(TrackModel::Length),    -1,
          { TrackModel::Length } },
        { PointModel::mdName(PointModel::Speed),     PointModel::Speed,
          { TrackModel::MinSpeed, TrackModel::AvgMovSpeed, TrackModel::MaxSpeed } },
        { PointModel::mdName(PointModel::Ele),       PointModel::Ele,
          { TrackModel::MinElevation, TrackModel::AvgElevation, TrackModel::MaxElevation } },
        { TrackModel::mdName(TrackModel::Ascent),    -1,
          { TrackModel::Ascent } },
        { TrackModel::mdName(TrackModel::Descent),   -1,
          { TrackModel::Descent } },
        { PointModel::mdName(PointModel::Grade),     PointModel::Grade,
          { TrackModel::MinGrade, TrackModel::AvgGrade, TrackModel::MaxGrade } },
        { PointModel::mdName(PointModel::Hr),        PointModel::Hr,
          { TrackModel::MinHR, TrackModel::AvgHR, TrackModel::MaxHR } },
        { PointModel::mdName(PointModel::Cad),       PointModel::Cad,
          { TrackModel::MinCad, TrackModel::AvgMovCad, TrackModel::MaxCad } },
        { PointModel::mdName(PointModel::Power),     PointModel::Power,
          { TrackModel::MinPower, TrackModel::AvgMovPower, TrackModel::MaxPower } },
        { TrackModel::mdName(TrackModel::Energy),    -1,
          { TrackModel::Energy } },
    };

    QString tooltip = QString("<p><b><u><nobr><big>") +
                      tr("Selected Points: ") +
                      QString::number(m_selectCount) +
                      "</big></nobr></u></b></p>";

    tooltip += "<table><tr>";
    tooltip += "<th style=\"text-align:left;\">" + tr("Data") + "</th>";
    for (const QString& colTitle : { tr("Min"), tr("Avg"), tr("Max") })
        tooltip += "<th style=\"text-align:right; padding:0em 0.5em 0em 0em;\">" + colTitle + "</th>";
    tooltip += "</tr>";

    for (const Line& line : tooltipInfo) {
        QString rowHtml = "<tr><td><i>" + line.m_title + " </i></td>";
        bool rowHasData = false;

        for (const auto col : line.m_col) {
            if (const QVariant colData = values.value(col); colData.isValid()) {
                rowHasData = true;
                // Skip middle for min/max only
                if (col == line.m_col.back() && line.m_col.size() == 2)
                    rowHtml += "<td></td>";

                rowHtml += "<td style=\"text-align:right; padding:0em 0.5em 0em 0em;\">";

                const Units& colUnits = TrackModel::mdUnits(col);
                QVariant color = cfgData().trackColorizer.colorize(col, colData, Qt::ForegroundRole);

                // Small kludge: Use point colorizer if track didn't colorize the values
                if (!color.isValid() && line.m_pointCol >= 0)
                    color = cfgData().pointColorizer.colorize(line.m_pointCol, colData, Qt::ForegroundRole);

                if (color.isValid())
                    rowHtml += "<font color=\"" + color.value<QColor>().name() +  "\">";

                rowHtml += colUnits(colData);

                if (color.isValid())
                    rowHtml += "</font>";
                rowHtml += "</td>";
            }
        }

        rowHtml += "</tr>";

        if (rowHasData) // only append rows which have non-empty data
            tooltip += rowHtml;
    }

    tooltip += "</table>";
    return tooltip;
}

bool PointModel::isTextField(ModelType mt) const
{
    return mt == PointModel::Comment || mt == PointModel::Desc;
}

QString PointModel::tooltip(const QModelIndex& idx) const
{
    static const QVector<ModelType> tooltipColumns = {
        PointModel::Time,
        PointModel::Elapsed,
        PointModel::Lat,
        PointModel::Lon,
        PointModel::Ele,
        PointModel::Distance,
        PointModel::Grade,
        PointModel::Temp,
        PointModel::Hr,
        PointModel::Cad,
        PointModel::Power,
        PointModel::Course,
        PointModel::Bearing,
        PointModel::Name,
        PointModel::Comment,
        PointModel::Desc,
    };

    const QModelIndex idxIdx = idx.sibling(idx.row(), PointModel::Index);

    // If we're over a selected item, build a multi-select tooltip.
    if (m_selectCount > 1 && getItem(idxIdx)->test(PointItem::Flags::Select))
        return multiPointTooltip();

    QString tooltip = QString("<p><b><u><nobr><big>Index: ") +
        data(idxIdx, Qt::DisplayRole).toString() +
        + "</big></nobr></u></b></p>";

    tooltip += "<table>";

    for (const auto col : tooltipColumns) {
        const QVariant color = cfgData().pointColorizer.colorize(rowSibling(col, idx), Qt::ForegroundRole);
        const QModelIndex colIdx = idx.sibling(idx.row(), col);

        // These columns normally display an icon instead of contents. Force contents here.
        const int role = isTextField(col) ? Util::RawDataRole : Qt::DisplayRole;

        if (QVariant colData = data(colIdx, role); colData.isValid()) {
            // If text exceeds size threshold, replace with icon.
            if (isTextField(col) && Util::PlainTextLen(colData) > cfgData().tooltipInlineLimit)
                colData = "<img src=\"" + cfgData().trackNoteIcon + "\"></img>";

            tooltip += "<tr><td><b>" + mdName(col) + " </b></td><td>";
            if (color.isValid())
                tooltip += "<font color=\"" + color.value<QColor>().name() +  "\">";

            tooltip += colData.toString();

            if (color.isValid())
                tooltip += "</font>";

            tooltip += "</td></tr>";
        }
    }

    tooltip += "</table>";

    return tooltip;
}

QVariant PointModel::data(const QModelIndex& idx, int role) const
{
    verify(idx);

    if (!idx.isValid() || idx.column() >= columnCount())
        return { };

    // Segment level
    if (isSegment(idx)) {
        if (role == Qt::DisplayRole)
            if (idx.column() == 0)
                return QString(tr("Seg %1")).arg(idx.row());
        return { };
    }

    // Two level hierarchy: track segments / track points
    const PointItem* pt  = getItem(idx);
    if (pt == nullptr)
        return { };

    const bool filtered = (role != Qt::EditRole);
    const QVariant rawData = data(*pt, idx.column(), idx, role, filtered);

    switch (role) {
    case Qt::TextAlignmentRole: return { mdAlignment(idx.column()) };
    case Qt::EditRole:          [[fallthrough]];
    case Qt::DecorationRole:    [[fallthrough]];
    case Util::PlotRole:        [[fallthrough]];
    case Qt::SizeHintRole:      [[fallthrough]];
    case Util::RawDataRole:     return rawData;
    case Qt::DisplayRole:       [[fallthrough]];
    case Util::CopyRole:
        if (rawData.isValid())
            return cfgData().pointColorizer.maybeUse(units(idx)(rawData), idx, role);
        break;

    case Qt::ToolTipRole:
        // If PointItem returned a tooltip, use that. Else, construct our own.
        if (rawData.isValid() && Util::PlainTextLen(rawData) < cfgData().tooltipFullLimit)
            return rawData;
        else if (!isTextField(idx.column()))
            return tooltip(idx);
        break;
    }

    return cfgData().pointColorizer.colorize(idx, role);
}

QVariant PointModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (const QVariant val = ModelMetaData::headerData<PointModel>(section, orientation, role); val.isValid())
        return val;

    if (orientation == Qt::Horizontal) {
        switch (role) {
        case Qt::DisplayRole:        return mdName(section);
        case Qt::TextAlignmentRole:  return { mdAlignment(section) };
        }
    }

    return MergeableModel::headerData(section, orientation, role);
}

QModelIndex PointModel::index(int row, int column, const QModelIndex& parent) const
{
    // Workaround for: https://bugreports.qt.io/browse/QTBUG-44962
    if (wrongModel(parent))
        return { };

    // Two level hierarchy: track segments / track points
    if (!verify(parent).isValid())
        return createIndex(row, column, quintptr(-1));  // This is the top level (trksegs)

    return createIndex(row, column, quintptr(parent.row()));
}

QModelIndex PointModel::parent(const QModelIndex& idx) const
{
    // Workaround for: https://bugreports.qt.io/browse/QTBUG-44962
    if (wrongModel(idx))
        return { };

    if (!verify(idx).isValid() || isSegment(idx))
        return { };

    return createIndex(int(idx.internalId()), 0, quintptr(-1));
}

int PointModel::rowCount(const QModelIndex& parent) const
{
    // Workaround for: https://bugreports.qt.io/browse/QTBUG-44962
    if (wrongModel(parent))
        return 0;

    // Two level hierarchy: track segments / track points
    if (!verify(parent).isValid())
        return size();

    return isSegment(parent) ? at(parent.row()).size() : 0;
}

Qt::ItemFlags PointModel::flags(const QModelIndex& idx) const
{
    return ModelMetaData::flags<PointModel>(verify(idx)) | MergeableModel::flags(idx);
}

bool PointModel::setsColumn(const QModelIndexList& selection, int column)
{
    return std::any_of(selection.begin(), selection.end(), [column](const QModelIndex& idx) {
        return isPoint(idx) && idx.column() == column;
    });
}

bool PointModel::setsColumn(const QModelIndex& idx, int column)
{
    return isPoint(idx) && idx.column() == column;
}

bool PointModel::multiSet(const QModelIndexList& selection, const QVariant& value, int role)
{
    // Handle nesting to avoid redundant processDataChanged
    const DataChangedNest dataChangeNest(*this, selection);

    if (setsColumn(selection, PointModel::Speed))
        return speedToTimestamps(selection, value);

    return TreeModel::multiSet(selection, value, role);
}

bool PointModel::setData(const QModelIndex& idx, const QVariant& value, int role)
{
    // Handle nesting to avoid redundant processDataChanged
    const DataChangedNest dataChangeNest(*this, idx);

    if (isSegment(idx))  // cannot set segment level data
        return false;

    if (setsColumn(idx, PointModel::Speed))
        return speedToTimestamps({idx}, value);

    using namespace std::chrono_literals;

    // Emit explicitly, because we don't pass this though to the base class.
    // Not for speed though, because that restores the whole model.
    emitAboutToChange(idx, value, role);

    bool changed;
    const bool result = getItem(idx)->setData(idx.column(), value, role, changed);

    if (result && changed) {
        emit dataChanged(idx, idx);

        // Mark flags so we can fix things up in processDataChanged()
        switch (idx.column()) {
        case PointModel::Time:  m_timeDirty  = true; break;
        default: break;
        }
    }

    return result;
}

bool PointModel::appendPoint(const PointItem& point, const QModelIndex& parent)
{
    using namespace std::chrono_literals;

    // must insert points into a segment
    if (!parent.isValid())
        return false;

    // Datestamp must be after last present datestamp, if any
    const QDateTime lastTime = trackEndTime();
    if (lastTime.isValid() && point.time() <= lastTime)
        return false;

    const int rows = rowCount(parent); // appending
    insertRows(rows, 1, parent);  // insert row for the new point

    const QModelIndex pointIdx = index(rows, 0, parent);
    PointItem* pointItem = getItem(pointIdx);
    if (pointItem == nullptr) {
        removeRow(rows, parent); // unwind the insert
        return false;
    }

    *pointItem = point;
    emit dataChanged(pointIdx, rowSibling(columnCount(), pointIdx));

    processDataChanged();

    return true;
}

bool PointModel::setHeaderData(int /*section*/, Qt::Orientation /*orientation*/,
                                 const QVariant& /*value*/, int /*role*/)
{
    assert(0);
    return false; // not supported
}

bool PointModel::insertRows(int position, int rows, const QModelIndex& parent)
{
    beginInsertRows(parent, position, position + rows - 1);

    if (!parent.isValid()) {
        while (--rows >= 0)
            insert(position, value_type());  // segment level
    } else {
        (*this)[parent.row()].insert(position, rows, value_type::value_type());
        // point level
    }

    updateStartIdx();
    endInsertRows();

    return true;
}

void PointModel::updateTrackItem()
{
    if (m_trackItem != nullptr)
        m_trackItem->update(*this); // recalc track item data
}

QModelIndex PointModel::addInterpolatedPoint(PointItem::Lat_t latDeg, PointItem::Lon_t lonDeg,
                                             const QModelIndex& parent, int beforeRow)
{
    using namespace std::chrono_literals;

    assert(isSegment(parent));

    insertRows(beforeRow, 1, parent);  // insert point
    const QModelIndex newPointIdx = index(beforeRow, 0, parent);

    const PointItem* prevPoint = prevPointInSeg(newPointIdx);
    const PointItem* nextPoint = nextPointInSeg(newPointIdx);
    PointItem*       thisPoint = getItem(newPointIdx);

    thisPoint->setLon(lonDeg).setLat(latDeg);

    if (prevPoint != nullptr && nextPoint != nullptr) {
        // Ratio of: (prev-to-this) / (this-to-next)
        const qreal distRatio = prevPoint->greatCircleDist(*thisPoint) /
                                std::max(prevPoint->greatCircleDist(*thisPoint) +
                                         thisPoint->greatCircleDist(*nextPoint), 0.01);

        const PointItem::Dur_t durationPrevToNext = prevPoint->duration(*nextPoint);
        const QDateTime interpTime = prevPoint->time().addMSecs(qint64(distRatio * Dur_t::base_type(durationPrevToNext)));

        const PointItem interpPoint = prevPoint->interpolate(*nextPoint, interpTime);

        *thisPoint = interpPoint;
    }

    setData(PointModel::Lon, newPointIdx, PointItem::Lon_t::base_type(lonDeg), Util::RawDataRole);
    setData(PointModel::Lat, newPointIdx, PointItem::Lat_t::base_type(latDeg), Util::RawDataRole);

    processDataChanged();

    return newPointIdx;
}

bool PointModel::removeRows(int position, int rows, const QModelIndex& parent)
{
    if (rows == 0)
        return true;

    if (!parent.isValid()) {
        // parent is root
        beginRemoveRows(parent, position, position + rows - 1);
        erase(begin() + position, begin() + position + rows);
        endRemoveRows();
    } else {
        // parent is trkseg
        assert(qintptr(parent.internalId()) == -1);

        beginRemoveRows(parent, position, position + rows - 1);
        value_type& segs = (*this)[parent.row()];
        segs.erase(segs.begin() + position, segs.begin() + position + rows);
        endRemoveRows();
    }

    updateStartIdx();
    updateTrackItem();

    return true;
}

void PointModel::removeEmptySegments()
{
    for (int seg = size() - 1; seg >= 0; --seg)
        if (at(seg).isEmpty())
            removeRows(seg, 1);

    // removeRows() handles updating the point index values
}

void PointModel::preUndoSet(QVariantMap& userData)
{
    // Only execute once, not once per undo on this model
    if (userData[__func__].value<uintptr_t>() == uintptr_t(this))
        return;

    userData[__func__].setValue(uintptr_t(this));

    beginResetModel();
}

void PointModel::postUndoSet(QVariantMap& userData)
{
    // Only execute once, not once per undo on this model
    if (userData[__func__].value<uintptr_t>() == uintptr_t(this))
        return;

    userData[__func__].setValue(uintptr_t(this));

    updateTrackItem();
    endResetModel();

    // Re-expand point panes after model reset
    MainWindow::mainWindowStatic->expandPointPanes();
}

// We could make a very good initial guess by starting dist/trackTotalDistance() through
// the points, which would be faster than a pure binary chop.  This is OK for now since it only
// has to keep up with mouse movement events.  If performance is ever more critical, this can be
// revisited.
std::tuple<const PointItem*, int, const PointItem*, int> PointModel::getItemBound(qreal dist) const
{
    // First, find first segment whose final distance is not less than 'dist'
    const auto* segIt = std::lower_bound(begin(), end(), dist, [](const value_type& trkSeg, qreal dist) {
        return !trkSeg.empty() && trkSeg.last().distance() < dist;
    });

    if (segIt == end())
        return std::make_tuple(nullptr, -1, nullptr, -1);

    // Binary chop within that segment.
    const auto* upperIt = std::lower_bound(segIt->begin(), segIt->end(), dist, [](const PointItem& trkPt, qreal dist) {
        return trkPt.distance() < dist;
    });

    const int upperRow = (upperIt - segIt->begin()) + m_startIdxCache.at(segIt - begin());

    const auto* lowerIt = upperIt;
    const auto* lowerSegIt = segIt;

    if (lowerIt == lowerSegIt->begin()) {
        if (lowerSegIt != begin()) // last item from previous segment
            lowerIt = (--lowerSegIt)->begin() + segIt->size() - 1;
    } else {
        --lowerIt;
    }

    const int lowerRow = (lowerIt - lowerSegIt->begin()) + m_startIdxCache.at(lowerSegIt - begin());

    return std::make_tuple(lowerIt, lowerRow, upperIt, upperRow);
}

const Units& PointModel::units(const QModelIndex& idx) const
{
    if (idx.isValid() && m_trackItem != nullptr) {
        // Override speed with per-tag speed unit, if available, by asking the track for its speed unit.
        if (idx.column() == PointModel::Speed)
            return m_trackItem->units(TrackModel::AvgMovSpeed);
    }

    return mdUnits(idx.column());
}

// Split track into segments at selected points.
void PointModel::splitSegments(const QModelIndexList& splitAt)
{
    QModelIndexList rows = Util::MapDown(splitAt);

    // reverse order to avoid invalidation
    std::sort(rows.begin(), rows.end(), [](const QModelIndex& lhs, const QModelIndex& rhs) {
        return lhs.row() > rhs.row();
    });

    rows.erase(std::remove_if(rows.begin(), rows.end(), [](const QModelIndex& idx) { return !idx.isValid(); }),
               rows.end());

    if (rows.isEmpty())
        return;

    QModelIndexList splits; // split points

    // Remove middles of contiguous ranges.  Note we're sorted from last to first.
    for (int i = rows.size() - 1; i >= 0; --i) { // always leave first point
        if (i == (rows.size()-1) || rows[i].row() != (rows[i+1].row()+1))
            splits.prepend(rows[i]);
        else if (i == 0 || rows[i].row() != (rows[i-1].row()-1))
            if (rows[i].row() != (rowCount(parent(rows[i])) - 1))
                splits.prepend(index(rows[i].row()+1, 0, rows[i].parent()));
    }

    for (const auto& split : splits) {
        if (split.row() == 0)  // skip if it would move the whole segment, leaving an empty one.
            continue;

        const int thisSegment = split.parent().row();
        const int segInsertionPoint = thisSegment + 1;  // insert BEFORE this spot.
        const int rowsToInsert = at(thisSegment).size() - split.row();
        const QModelIndex segInsertionIdx = index(segInsertionPoint, 0);

        insertRow(segInsertionPoint);
        (*this)[segInsertionPoint].reserve(rowsToInsert);

        // Insert copies of segments.  (move semantics not worthwhile on the small/flat PointItems)
        beginInsertRows(segInsertionIdx, 0, rowsToInsert - 1);
        for (int r = 0; r < rowsToInsert; ++r)
            (*this)[segInsertionPoint].append((*this)[thisSegment][split.row() + r]);
        endInsertRows();

        // Remove old segments
        removeRows(split.row(), rowsToInsert, index(thisSegment, 0));
    }

    updateTrackItem();
}

QVariant PointModel::siblingData(int column, const QModelIndex &idx, int role) const
{
    return data(index(idx.row(), column, parent(idx)), role);
}

QString PointModel::mdName(ModelType mt)
{
    switch (mt) {
    case PointModel::Index:     return tr("Index");
    case PointModel::Time:      return tr("Time");
    case PointModel::Elapsed:   return tr("Elapsed");
    case PointModel::Lon:       return tr("Longitude");
    case PointModel::Lat:       return tr("Latitude");
    case PointModel::Ele:       return tr("Elevation");
    case PointModel::Length:    return tr("Length");
    case PointModel::Distance:  return tr("Distance");
    case PointModel::Vert:      return tr("Vertical");
    case PointModel::Grade:     return tr("Grade");
    case PointModel::Duration:  return tr("Duration");
    case PointModel::Temp:      return tr("Temp");
    case PointModel::Depth:     return tr("Depth");
    case PointModel::Speed:     return tr("Speed");
    case PointModel::Hr:        return tr("Heart Rate");
    case PointModel::Cad:       return tr("Cadence");
    case PointModel::Power:     return tr("Power");
    case PointModel::Course:    return tr("Course");
    case PointModel::Bearing:   return tr("Bearing");
    case PointModel::Name:      return tr("Name");
    case PointModel::Comment:   return tr("Comment");
    case PointModel::Desc:      return tr("Description");
    case PointModel::Symbol:    return tr("Symbol");
    case PointModel::Type:      return tr("Type");

    case _Count:    break;
    }

    assert(0 && "Unknown GeoPoint column");
    return "";
}

bool PointModel::mdIsEditable(ModelType mt)
{
    switch (mt) {
    case PointModel::Time:    [[fallthrough]];
    case PointModel::Lat:     [[fallthrough]];
    case PointModel::Lon:     [[fallthrough]];
    case PointModel::Ele:     [[fallthrough]];
    case PointModel::Temp:    [[fallthrough]];
    case PointModel::Speed:   [[fallthrough]];
    case PointModel::Depth:   [[fallthrough]];
    case PointModel::Hr:      [[fallthrough]];
    case PointModel::Cad:     [[fallthrough]];
    case PointModel::Power:   [[fallthrough]];
    case PointModel::Course:  [[fallthrough]];
    case PointModel::Bearing: [[fallthrough]];
    case PointModel::Name:    [[fallthrough]];
    case PointModel::Comment: [[fallthrough]];
    case PointModel::Desc:    [[fallthrough]];
    case PointModel::Symbol:  [[fallthrough]];
    case PointModel::Type:    return true;
    default:                  return false;
    }
}

QString PointModel::mdTooltip(ModelType mt)
{
    const bool editable = mdIsEditable(mt);

    switch (mt) {
    case PointModel::Index:
        return makeTooltip(tr("Index of point within its track segment."), editable);
    case PointModel::Time:
        return makeTooltip(tr("Timestamp for point."), editable);
    case PointModel::Elapsed:
        return makeTooltip(tr("Elapsed time within track."), editable);
    case PointModel::Lon:
        return makeTooltip(tr("Longitude for point."), editable);
    case PointModel::Lat:
        return makeTooltip(tr("Latitude for point."), editable);
    case PointModel::Ele:
        return makeTooltip(tr("Elevation at point."), editable);
    case PointModel::Length:
        return makeTooltip(tr("Horizontal distance to the next point."), editable);
    case PointModel::Distance:
        return makeTooltip(tr("Distance from track start to this point."), editable);
    case PointModel::Vert:
        return makeTooltip(tr("Vertical distance to the next point."), editable);
    case PointModel::Grade:
        return makeTooltip(tr("Grade at this point."), editable);
    case PointModel::Duration:
        return makeTooltip(tr("Duration of this leg."), editable);
    case PointModel::Temp:
        return makeTooltip(tr("Temperature."), editable);
    case PointModel::Depth:
        return makeTooltip(tr("Depth."), editable);
    case PointModel::Speed:
        return makeTooltip(tr("Speed at this point."), editable);
    case PointModel::Hr:
        return makeTooltip(tr("Heart Rate at this point."), editable);
    case PointModel::Cad:
        return makeTooltip(tr("Cadence at this point."), editable);
    case PointModel::Power:
        return makeTooltip(tr("Power at this point."), editable);
    case PointModel::Course:
        return makeTooltip(tr("Course to next point."), editable);
    case PointModel::Bearing:
        return makeTooltip(tr("Bearing."), editable);
    case PointModel::Name:
        return makeTooltip(tr("Waypoint name."), editable);
    case PointModel::Comment:
        return makeTooltip(tr("Waypoint comment."), editable);
    case PointModel::Desc:
        return makeTooltip(tr("Description."), editable);
    case PointModel::Symbol:
        return makeTooltip(tr("GPS symbol name."), editable);
    case PointModel::Type:
        return makeTooltip(tr("Point classification."), editable);

    default:
        assert(0 && "Unknown PointModel column");
        return "";
    }
}

QString PointModel::mdWhatsthis(ModelType mt)
{
    return mdTooltip(mt);   // just pass through the tooltip
}

Qt::Alignment PointModel::mdAlignment(ModelType mt)
{
    switch (mt) {
    case PointModel::Name:    [[fallthrough]];
    case PointModel::Comment: [[fallthrough]];
    case PointModel::Desc:    [[fallthrough]];
    case PointModel::Symbol:  [[fallthrough]];
    case PointModel::Type:    return Qt::AlignLeft | Qt::AlignVCenter;
    default:                  return Qt::AlignRight | Qt::AlignVCenter;
    }
}

const Units& PointModel::mdUnits(ModelType mt)
{
    static const Units rawFloat(Format::Float);
    static const Units rawString(Format::String);
    static const Units rawInt(Format::Int);
    static const Units indexUnits(Format::Int, 4, true);

    switch (mt) {
    case PointModel::Index:    return indexUnits;
    case PointModel::Time:     return cfgData().unitsPointDate;
    case PointModel::Lon:      return cfgData().unitsLon;
    case PointModel::Lat:      return cfgData().unitsLat;
    case PointModel::Ele:      return cfgData().unitsElevation;
    case PointModel::Length:   return cfgData().unitsLegLength;
    case PointModel::Distance: return cfgData().unitsTrkLength; // use track units for total distance
    case PointModel::Vert :    [[fallthrough]];
    case PointModel::Depth:    return cfgData().unitsClimb;
    case PointModel::Grade :   return cfgData().unitsSlope;
    case PointModel::Elapsed:  [[fallthrough]];
    case PointModel::Duration: return cfgData().unitsDuration;
    case PointModel::Temp:     return cfgData().unitsTemp;
    case PointModel::Speed:    return cfgData().unitsSpeed;
    case PointModel::Hr:       return cfgData().unitsHr;
    case PointModel::Cad:      return cfgData().unitsCad;
    case PointModel::Power:    return cfgData().unitsPower;
    case PointModel::Course:   return rawFloat;  // TODO: ...
    case PointModel::Bearing:  return rawFloat;  // TODO: ...
    case PointModel::Name:     [[fallthrough]];
    case PointModel::Comment:  [[fallthrough]];
    case PointModel::Desc:     [[fallthrough]];
    case PointModel::Symbol:   [[fallthrough]];
    case PointModel::Type:     return rawString;
    }

    assert(0);
    return rawFloat;
}

bool PointModel::mdIsChartable(ModelType mt)
{
    switch (mt) {
    case PointModel::Index:    [[fallthrough]];
    case PointModel::Time:     [[fallthrough]];
    case PointModel::Length:   [[fallthrough]];
    case PointModel::Distance: [[fallthrough]];
    case PointModel::Vert :    [[fallthrough]];
    case PointModel::Duration: [[fallthrough]];
    case PointModel::Name:     [[fallthrough]];
    case PointModel::Comment:  [[fallthrough]];
    case PointModel::Desc:     [[fallthrough]];
    case PointModel::Symbol:   [[fallthrough]];
    case PointModel::Type:     return false;
    default:                   return true;
    }
}

const PointModel::name_t& PointModel::getItemName() const
{
    return getItemName({});
}

const PointModel::name_t& PointModel::getItemName(const QModelIndex& idx) const
{
    static const name_t segName = { tr("Segment"), tr("Segments") };

    return isSegment(idx) ? segName : getItemNameStatic();
}

const NamedItemInterface::name_t& PointModel::getItemNameStatic()
{
    static const name_t pointName = { tr("Point"), tr("Points") };
    return pointName;
}

QDataStream& operator<<(QDataStream& stream, const PointModel& p)
{
    return stream << static_cast<const PointModel::container_type&>(p);
}

QDataStream& operator>>(QDataStream& stream, PointModel& p)
{
    const uint streamVer = reinterpret_cast<VersionedStream&>(stream).getVersion();

    // Most recent should be first in this test for performance reasons. We do this a LOT on load.
    if (streamVer >= 0x1003)
        return stream >> static_cast<PointModel::container_type&>(p);

    if (streamVer >= 0x1001) // convert from old binary format. 0x1002 is identical for the PointModel.
        return convert<V1001::PointItem>(stream, p);

    if (streamVer == 0x1000) // convert from old binary format
        return convert<V1000::PointItem>(stream, p);

    assert(0);
    return stream;
}

uint qHash(const PointModel& p, uint seed)
{
    return qHashRange(p.begin(), p.end(), seed);
}

bool PointModel::selectPointsWithin(const Marble::GeoDataLatLonBox& bounds)
{
    bool found = false;

    for (auto& trkseg : *this) {
        for (auto& pt : trkseg) {
            if (bounds.contains(double(Math::toRad(pt.lon())), double(Math::toRad(pt.lat()))))
                pt.set(found = true, PointItem::Flags::AreaSelect);
            else
                pt.set(false, PointItem::Flags::AreaSelect);
        }
    }

    return found;
}

void PointModel::select(const QModelIndexList& selection)
{
    // Select each point
    for (const auto& idx : selection)
        select(Util::MapDown(idx), true);
}

QModelIndexList PointModel::selectedIndexes(PointItem::Flags flags) const
{
    QModelIndexList selected;
    selected.reserve(256);

    for (const auto& trkseg : *this) {
        const QModelIndex segIdx = index(&trkseg - cbegin(), 0);
        for (const auto& pt : trkseg)
            if (pt.test(flags))
                selected.append(index(&pt - trkseg.cbegin(), 0, segIdx));
    }

    return selected;
}

template<>
std::tuple<qreal, int> PointModel::extremeVal(AreaDialog&, ModelType mt, Extreme type) const
{
    int count     = 0;
    qreal extreme = (type == Extreme::Avg)  ? 0.0 :
                    (type == Extreme::High) ? -std::numeric_limits<qreal>::max() :
                    (type == Extreme::Low)  ? std::numeric_limits<qreal>::max() : 0.0;

    QModelIndex extremeIdx;

    m_areaExtreme = QModelIndex();

    int trkRow = 0;
    int prevPtRow = -1;
    for (const auto& trkSeg : *this) {
        const PointItem* prevPt = nullptr;
        const QModelIndex parent = index(trkRow++, 0);

        int ptRow = 0;
        for (const auto& pt : trkSeg) {
            if (prevPt != nullptr) {
                if (prevPt->test(PointItem::Flags::AreaSelect) && prevPt->hasData(mt, &pt)) {
                    const QModelIndex idx = index(prevPtRow, 0, parent);
                    const qreal val = prevPt->data(mt, -1, Util::RawDataRole, false,
                                                   firstPointInTrack(), &pt, powerData()).toDouble();
                    switch (type) {
                    case Extreme::High:
                        if (val > extreme) {
                            extreme    = val;
                            extremeIdx = idx;
                            count      = 1;
                        }
                        break;
                    case Extreme::Low:
                        if (val < extreme) {
                            extreme    = val;
                            extremeIdx = idx;
                            count      = 1;
                        }
                        break;
                    case Extreme::Avg:
                        extreme += val;
                        ++count;
                        break;
                    }
                }

            }

            prevPt = &pt;
            prevPtRow = ptRow++;
        }
    }

    if (extremeIdx.isValid())
        m_areaExtreme = extremeIdx;

    if (count > 0 && type == Extreme::Avg)
        extreme /= count;

    return std::make_tuple(extreme, count);
}

// Set power parameters as function of tag and person
void PointModel::setPowerData(const QModelIndex& tagIdx, const QModelIndex& personIdx)
{
    if (!tagIdx.isValid() || !personIdx.isValid()) {
        m_powerData.clear();
        return;
    }

    const qreal weight =
            cfgData().people.data(PersonModel::Weight, personIdx, Util::RawDataRole).toDouble() +
            cfgData().tags.data(TagModel::Weight, tagIdx, Util::RawDataRole).toDouble();

    const qreal CdA     = cfgData().tags.data(TagModel::CdA, tagIdx, Util::RawDataRole).toDouble();
    const qreal rr      = cfgData().tags.data(TagModel::RR, tagIdx, Util::RawDataRole).toDouble();

    const QVariant personEffVar = cfgData().people.data(PersonModel::Efficiency, personIdx, Util::RawDataRole);
    const QVariant tagEffVar    = cfgData().tags.data(TagModel::Efficiency, tagIdx, Util::RawDataRole);
    const QVariant bioPctVar    = cfgData().tags.data(TagModel::BioPct, tagIdx, Util::RawDataRole);

    const qreal personEff  = personEffVar.isValid() ? personEffVar.toDouble() : 1.0;
    const qreal tagEff     = tagEffVar.isValid()    ? tagEffVar.toDouble()    : 1.0;
    const qreal bioPct     = bioPctVar.isValid()    ? bioPctVar.toDouble()    : 1.0;

    // System efficiency: vehicle efficiency always applies, but apply only a % of the bio efficiency
    // depending on the vehicle's % power coming from biology.
    const qreal efficiency = (1.0 - (1.0 - personEff) * bioPct) * tagEff;

    const QString medium = cfgData().tags.data(TagModel::Medium, tagIdx, Util::RawDataRole).toString();
    qreal density     = 1.225;  // default to air density
    bool  eleVariance = true;   // whether to vary density with altitude
    
    if (const auto it = TagModel::medium.find(medium); it != TagModel::medium.end()) {
        density     = qreal(std::get<0>(it.value()));
        eleVariance = std::get<1>(it.value());
    }

    m_powerData = PowerData(CdA, weight, rr, efficiency, density, eleVariance);
}

void PointModel::saveItem(const QModelIndex& idx, QDataStream& stream, const TreeModel&) const
{
    if (const PointItem* item = getItem(idx); item != nullptr)
        stream << *item;
    else
        stream << at(idx.row());
}

void PointModel::loadItem(const QModelIndex& idx, QDataStream& stream, TreeModel&)
{
    if (PointItem* item = getItem(idx); item != nullptr)
        stream >> *item;
    else
        stream >> (*this)[idx.row()];
}

// Merge selected segments into a single segment
std::tuple<QModelIndex, bool> PointModel::mergeSegments(const QModelIndexList& segs)
{
    // Process the segments in order
    QVector<int> sortedSegs;
    QVector<int> mergedSegs; // the segments we merged into others

    for ([[maybe_unused]] const auto& seg : segs)
        assert(isSegment(seg));

    // Merge all segments if zero or one selection.
    if (segs.size() < 2) {
        sortedSegs.reserve(size());

        for (int row = 0; row < size(); ++row)
            sortedSegs.append(row);
    } else {
        for (const auto& seg : segs)
            sortedSegs.append(seg.row());

        // Process in order, so we can find contiguous groups
        std::sort(sortedSegs.begin(), sortedSegs.end());
    }

    int toSegId = -1;  // segment to merge into
    int prevSegId = -1;
    int firstMergedId = -1;

    // Merge contiguous groups. A non-selected segment will break the sequence and the next selected
    // segment will be the new merge destination.
    for (const auto& segId : sortedSegs) {
        if (prevSegId < 0 || segId != (prevSegId + 1)) {
            prevSegId = toSegId = segId; // merge into this one next time around
            continue;
        }

        const int toRowCount = at(toSegId).size();
        const int rowCount   = at(segId).size();

        if (rowCount > 0) {
            beginInsertRows(index(toSegId, 0), toRowCount, rowCount + toRowCount - 1);
            (*this)[toSegId].append(at(segId));
            endInsertRows();
        }

        if (firstMergedId < 0)
            firstMergedId = toSegId;

        mergedSegs.append(segId);
        prevSegId = segId;
    }

    // Remove appended ones (in reverse order, to avoid invalidation).
    for (const auto seg : mergedSegs)
        removeRow(seg);

    updateTrackItem();

    return { index(firstMergedId, 0), true };
}

// Merge selected points into a single point
std::tuple<QModelIndex, bool> PointModel::mergePoints(const QModelIndexList& points)
{
    for ([[maybe_unused]] const auto& pt : points)
        assert(isPoint(pt));

    QList<QModelIndexList> groups;

    // Sort by segment and then by row within the segment
    QModelIndexList sortedPoints = points;
    std::sort(sortedPoints.begin(), sortedPoints.end(), [](const QModelIndex& lhs, QModelIndex& rhs) {
        return lhs.parent().row() != rhs.parent().row() ? (lhs.parent().row() < rhs.parent().row()) :
                                                          (lhs.row() < rhs.row());
    });

    // Divide list up into contiguous groups. Groups must share a common parent.
    QModelIndex prevPt;
    for (const auto& idx : sortedPoints) {
        if (!prevPt.isValid() || prevPt.parent() != idx.parent() || (prevPt.row() + 1) != idx.row())
            groups.append(QModelIndexList());

        if (getItem(idx) != nullptr) // should never be null, but include for robustness.
            groups.back().append(idx);

        prevPt = idx;
    }

    QModelIndexList removePoints;
    removePoints.reserve(sortedPoints.size());

    for (const auto& list : groups) {   // groups of indexes
        if (list.size() <= 1)
            continue;

        PointAccum accum(*getItem(list.first()));
        for (const auto& idx : list) {  // index within group
            accum.add(*getItem(idx));   // accumulate the data
            if (idx != list.first())    // remove all but the 1st point per group
                removePoints.append(idx);
        }

        // Update point with undo tracking
        const PointItem average = accum.average();
        app().undoMgr().add(new UndoPoint(*this, list.first(), average));
        *getItem(list.first()) = average; // replace first point with average
    }

    // Remove the merged points
    for (const auto& list : groups)
        removePoints.append(list.mid(1));

    Util::RemoveRows(*this, removePoints);
    updateTrackItem();

    return { groups.front().front(), true };
}

std::tuple<QModelIndex, bool> PointModel::merge(const QModelIndexList& selections, const QVariant&)
{
    if (allSegments(selections))
        return mergeSegments(selections);

    if (sameSegment(selections))
        return mergePoints(selections);

    return { QModelIndex(), false };
}

PointItem PointModel::interpolate(const QModelIndex& idx0, const QModelIndex& idx1, QDateTime& ts) const
{
    const PointItem* p0 = getItem(idx0);
    const PointItem* p1 = getItem(idx1);

    if (p0 == nullptr || p1 == nullptr)
        return { };

    return PointItem::interpolate(*p0, *p1, ts);
}

PointModel::FastSelection_t::FastSelection_t(const PointModel& pm, const QModelIndexList& selection)
{
    m_isPtSelected.resize(pm.size());
    m_isSegSelected.resize(pm.size());

    // Resize to avoid overflow below while testing.
    for (int seg = 0; seg < pm.size(); ++seg)
        m_isPtSelected[seg].resize(pm.at(seg).size());

    // Populate selection vectors. A bit space-inefficient given bools, and time-inefficient for
    // reszies, but oh well; we only expect a thousands of these, not millions.
    for (const auto& idx : selection) {
        if (idx.parent().isValid())
            m_isPtSelected[idx.parent().row()][idx.row()] = true; // point seletion
        else
            m_isSegSelected[idx.row()] = true; // segment selection
    }
}

// Simplify track by keeping points which pass the given "keep" function.
// The first and last points in any given segment are always kept.
PointModel::Count PointModel::simplifyPoints(const QModelIndexList& selection, Mode mode,
                                             const std::function<bool(const PointItem&, const PointItem&, const PointItem&)>& keep)
{
    Count count(1); // 1 = we're just part of one track

    const reset_t reset(*this, mode == Mode::Execute);

    // There are 3 selection possibilities:
    // 1. No entry is selected: process every point
    // 2. Segment-level selection: process every point under the segment.
    // 3. Point-level selection: process every selected point

    const bool emptySelection = selection.isEmpty();
    const FastSelection_t selected(*this, selection);

    // Iterate through track and keep only segments passing keep() function.
    for (int seg = 0; seg < size(); ++seg) {
        const segment_type& segData = at(seg);

        const bool segIsSelected = selected.m_isSegSelected.at(seg);

        segment_type newSegment; // build up new segment
        if (mode == Mode::Execute)
            newSegment.reserve(segData.size());

        int pt0 = 0, pt1 = 0;
        const int lastPt = segData.size() - 1;

        for (int pt2 = 0; pt2 < segData.size(); ++pt2) {
            const bool isSelected = // order matters here: we short-circuit for efficiency.
                    emptySelection ||            // mode 1
                    segIsSelected  ||            // mode 2
                    selected.m_isPtSelected.at(seg).at(pt2); // mode 3

            const PointItem& p0 = segData.at(pt0);
            const PointItem& p1 = segData.at(pt1);
            const PointItem& p2 = segData.at(pt2);

            if (isSelected)
                ++count.m_beforePoints;

            // always include first and last points in segment, and all non-selected points.
            if (!isSelected || pt2 == 0 || pt2 == lastPt || keep(p0, p1, p2)) {
                if (mode == Mode::Execute)
                    newSegment.append(segData.at(pt2));

                if (isSelected)
                    ++count.m_afterPoints;

                pt0 = pt1;
                pt1 = pt2;
            }
        }

        // Swap the newly built list and the old list
        if (mode == Mode::Execute)
            newSegment.swap((*this)[seg]);
    }

    return count;
}

SimplifiableModel::Count PointModel::simplifyTime(const QModelIndexList& selection, const Params& params)
{
    // Keep points with no timestamp or those whose time difference exceeds
    // the threshold value.
    return simplifyPoints(selection, params.m_mode,
                          [time=params.m_time](const PointItem&, const PointItem& p1, const PointItem& p2) {
        return !p1.hasTime() || !p2.hasTime() ||
                Dur_t(p1.time().secsTo(p2.time())) >= time;
    });
}

SimplifiableModel::Count PointModel::simplifyDist(const QModelIndexList& selection, const Params& params)
{
    // Keep points with no distance or those whose dist difference exceeds
    // the threshold value.
    return simplifyPoints(selection, params.m_mode,
                          [dist=params.m_dist](const PointItem&, const PointItem& p1, const PointItem& p2) {
        return !p1.hasDistance() || !p2.hasDistance() ||
                (p2.distance() - p1.distance()) >= dist;
    });
}

SimplifiableModel::Count PointModel::simplifyAdaptive(const QModelIndexList& selection, const Params& params)
{
    // Keep points using adaptive algorithm
    return simplifyPoints(selection, params.m_mode,
                          [dist=params.m_dist](const PointItem& p0, const PointItem& p1, const PointItem& p2) {
        if (&p0 == &p1 || &p1 == &p2 || // we need two prior points: always keep first ones.
            !p0.hasLoc() || !p1.hasLoc() || !p2.hasLoc() ||  // keep if they don't have locations
            !p0.hasTime() || !p1.hasTime() || !p2.hasTime()) // ... or times
            return true;

        // Extrapolate this point from prior two, and remove if error is over threshold.
        const PointItem extrap = p0.interpolate(p1, p2.time());

        // Keep if distance from extrapolated point to actual point
        // exceeds threshold.
        return extrap.greatCircleDist(p2) > dist;
    });
}

PointModel::reset_t::reset_t(PointModel& pm, bool enable) :
    m_pm(pm), m_enable(enable)
{
    if (m_enable) {
        app().undoMgr().add(new UndoModelData(m_pm, QModelIndex(), 0, m_pm.rowCount()-1));
        m_pm.beginResetModel();
    }
}

PointModel::reset_t::~reset_t()
{
    if (m_enable) {
        m_pm.sortTrkSegs();   // fix segment order
        m_pm.updateTrackItem();
        m_pm.endResetModel();
        m_pm.setDirty(); // since we bypass the setData methods, the changes are not otherwise tracked.
    }
}

template <> void PointModel::reverse(const TrackItem&, const QDateTime& beginDate, const QDateTime& endDate)
{
    const reset_t reset(*this);

    for (auto& trkSeg : *this) {
        segment_type newSegment; // build up new segment
        newSegment.reserve(trkSeg.size());

        for (const auto& pt : Util::reverse_adapter(trkSeg))
             newSegment.append(pt);

        trkSeg.swap(newSegment);
    }

    // Reverse timestamps for all points. Distances will be fixes in filter()
    if (beginDate.isValid() && endDate.isValid()) {
        for (auto& trkSeg : *this)
            for (auto& pt : trkSeg)
                pt.setTime(endDate.addMSecs(-qint64(pt.elapsed(beginDate))));
    }
}

void PointModel::unsetSpeed(const QModelIndexList& selection)
{
    const DataChangedNest dataChangeNest(*this, true);

    for (const auto& idx : selection)
        if (auto* pt = getItem(idx); pt != nullptr)
            pt->unsetSpeed();
}

void PointModel::unsetSpeed()
{
    const DataChangedNest dataChangeNest(*this, true);

    for (auto& trkseg : *this)
        for (auto& pt : trkseg)
            pt.unsetSpeed();
}

template<> Marble::GeoDataCoordinates PointModel::as(const QModelIndex& idx) const
{
    const auto* pt = getItem(idx);
    if (pt == nullptr)
        return { };

    return { PointItem::Lon_t::base_type(pt->lon()),
             PointItem::Lat_t::base_type(pt->lat()),
             PointItem::Ele_t::base_type(pt->ele()),
             Marble::GeoDataCoordinates::Degree };
}

PointModel::SegRange_t::SegRange_t(const PointModel& pm, const QModelIndex& beginIdx, const QModelIndex& endIdx)
{
    m_begin = !beginIdx.isValid() ? pm.cbegin() :
              (pm.cbegin() + (pm.isSegment(beginIdx) ? beginIdx.row() : beginIdx.parent().row()));

    m_end   = !endIdx.isValid() ? pm.cend() :
              (pm.cbegin() + (pm.isSegment(endIdx) ? (endIdx.row() + 1) : (endIdx.parent().row() + 1)));
}

PointModel::PtRange_t::PtRange_t(const PointModel& pm, const segment_type& seg,
                             const QModelIndex& beginIdx, const QModelIndex& endIdx)
{
    const int segId = &seg - pm.begin();

    m_begin = (!beginIdx.isValid() || pm.isSegment(beginIdx)) ? seg.cbegin() :
              (segId > beginIdx.parent().row()) ? seg.cbegin() : (seg.cbegin() + beginIdx.row());

    m_end = (!endIdx.isValid() || pm.isSegment(endIdx)) ? seg.cend() :
             (segId < endIdx.parent().row()) ? seg.cend() : (seg.cbegin() + endIdx.row());
}

PointModel::DataChangedNest::DataChangedNest(PointModel& pm, bool editsSpeed) :
    m_pm(pm)
{
    if (m_pm.m_setDataNesting++ == 0) {
        if (editsSpeed) {
            // Since there is no way to reconstruct timestamps from computed velocities,
            // we have to save everything if a velocity is changed.
            app().undoMgr().add(new UndoModelData(m_pm, QModelIndex(), 0, m_pm.rowCount()-1));
        }
    }
}

PointModel::DataChangedNest::DataChangedNest(PointModel& pm, const QModelIndex& idx) :
    DataChangedNest(pm, isPoint(idx) && idx.column() == PointModel::Speed)
{
}

PointModel::DataChangedNest::DataChangedNest(PointModel& pm, const QModelIndexList& selection) :
    DataChangedNest(pm, setsColumn(selection, PointModel::Speed))
{
}

PointModel::DataChangedNest::~DataChangedNest()
{
    if (--m_pm.m_setDataNesting == 0)
        m_pm.processDataChanged();
}
