/*
    Copyright 2022-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "pointaccum.h"
#include "pointitem.h"

PointAccum::PointAccum(const PointItem& first) :
    m_first(first)
{
}

PointAccum& PointAccum::add(const PointItem& pt, qreal weight)
{
    if (pt.hasTime())                     { m_msecFromT0 += pt.elapsed(m_first) * weight;  m_timeWeight += weight; }
    if (pt.hasLoc())                      { m_lon += pt.lon() * weight; m_lat += pt.lat() * weight; m_locWeight  += weight; }
    if (pt.hasEle())                      { m_ele     += pt.ele(false) * weight;    m_eleWeight     += weight; }
    if (pt.hasFltEle())                   { m_fltEle  += pt.ele(true) * weight;     m_fltEleWeight  += weight; }
    if (pt.hasDistance())                 { m_dist    += pt.distance() * weight;    m_distWeight    += weight; }
    if (pt.hasSpeed(PointItem::Measured)) { m_speed   += pt.speed() * Speed_t(weight); m_speedWeight   += weight; }
    if (pt.hasPower(PointItem::Measured)) { m_power   += pt.power() * weight;       m_powerWeight   += weight; }
    if (pt.hasDepth())                    { m_depth   += pt.depth() * weight;       m_depthWeight   += weight; }
    if (pt.hasAtemp())                    { m_atemp   += pt.atemp() * weight;       m_atempWeight   += weight; }
    if (pt.hasWtemp())                    { m_wtemp   += pt.wtemp() * weight;       m_wtempWeight   += weight; }
    if (pt.hasCourse())                   { m_course  += pt.course() * weight;      m_courseWeight  += weight; }
    if (pt.hasBearing())                  { m_bearing += pt.bearing() * weight;     m_bearingWeight += weight; }
    if (pt.hasHr())                       { m_hr      += qint64(pt.hr()) * weight;  m_hrWeight      += weight; }
    if (pt.hasCad())                      { m_cad     += qint64(pt.cad()) * weight; m_cadWeight     += weight; }

    // String data: just keep the first one we find.
    if (pt.hasName()    && m_name.isEmpty())    m_name    = pt.name();
    if (pt.hasComment() && m_comment.isEmpty()) m_comment = pt.comment();
    if (pt.hasDesc()    && m_desc.isEmpty())    m_desc    = pt.desc();
    if (pt.hasSymbol()  && m_symbol.isEmpty())  m_symbol  = pt.symbol();
    if (pt.hasType()    && m_type.isEmpty())    m_type    = pt.type();

    m_flags |= pt.flags();

    return *this;
}

PointItem PointAccum::average() const
{
    PointItem merged;

    if (m_timeWeight > 0.0)
        merged.setTime(m_first.time().addMSecs(Elaps_t::base_type(m_msecFromT0) / m_timeWeight));

    // Numeric data
    if (m_locWeight > 0.0)     merged.setLon(m_lon / m_locWeight) .setLat(m_lat / m_locWeight);
    if (m_eleWeight > 0.0)     merged.setEle(m_ele / m_eleWeight);
    if (m_fltEleWeight > 0.0)  merged.setEle(m_fltEle / m_fltEleWeight, true);
    if (m_distWeight > 0.0)    merged.setDistance(m_dist / m_distWeight);
    if (m_speedWeight > 0.0)   merged.setSpeed(m_speed / Speed_t(m_speedWeight));
    if (m_powerWeight > 0.0)   merged.setPower(m_power / m_powerWeight);
    if (m_depthWeight > 0.0)   merged.setDepth(m_depth / m_depthWeight);
    if (m_atempWeight > 0.0)   merged.setATemp(m_atemp / m_atempWeight);
    if (m_wtempWeight > 0.0)   merged.setWTemp(m_wtemp / m_wtempWeight);
    if (m_courseWeight > 0.0)  merged.setCourse(m_course / m_courseWeight);
    if (m_bearingWeight > 0.0) merged.setBearing(m_bearing / m_bearingWeight);
    if (m_hrWeight > 0.0)      merged.setHr(m_hr / m_hrWeight);
    if (m_cadWeight > 0.0)     merged.setCad(m_cad / m_cadWeight);

    // String data
    if (!m_name.isEmpty())     merged.setName(m_name);
    if (!m_comment.isEmpty())  merged.setComment(m_comment);
    if (!m_desc.isEmpty())     merged.setDesc(m_desc);
    if (!m_symbol.isEmpty())   merged.setSymbol(m_symbol);
    if (!m_type.isEmpty())     merged.setType(m_type);

    merged.set(true, m_first.flags());

    return merged;
}

PointItem PointAccum::average(const QDateTime& ts) const
{
    return average().setTime(ts);
}
