/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOPOLMGR_H
#define GEOPOLMGR_H

#include <atomic>
#include <QString>
#include <QFuture>
#include <QMap>

#include "geopolregion.h"

class TrackItem;
class QDataStream;

// Manager class for political boundary data. E.g, country/state/province boundaries.
class GeoPolMgr final
{
public:
    GeoPolMgr();
    ~GeoPolMgr();

    bool finishLoad() const;                         // let any pending load finish

    const GeoPolRegion* operator[](const QString&) const; // index by hierarchical name
    GeoPolRegionVec operator[](const QStringList&) const; // index list by hierarchical name list

    // Find all intersections recursively, finding names.  E.g, ("Canada", "Alberta")
    template <typename T> GeoPolRegionVec intersections(const T&, GeoPolRegion::WorldOpt = GeoPolRegion::IncludeNever) const;
    template <typename T> const GeoPolRegion* intersection(const T&, GeoPolRegion::WorldOpt = GeoPolRegion::IncludeNever) const;
    template <typename T> QStringList intersectionNames(const T&, GeoPolRegion::WorldOpt = GeoPolRegion::IncludeNever) const;
    template <typename T> QStringList intersectionHierarchicalNames(const T&, GeoPolRegion::WorldOpt = GeoPolRegion::IncludeNever) const;

    static QStringList hierarchicalNames(const GeoPolRegionVec&);

    // Icon names for flags in the list of hierarchical names
    QStringList flagIconNames(const QStringList& hierarchicalNames) const;

    // Canonicalize name to prevent comparison issues with data from different sources,
    // which may have or not have various accents.  This also maps spaces and dashes to
    // underscores.
    template <typename T> static QString canonicalizeName(const T& inputName);

    QString flagToolTip(const QString& name, const QStringList& regionNames) const;

    static const constexpr char* flagRoot = ":art/tags/Flags";

    static bool checkEndMarker(QDataStream&);
    static uint32_t readLoopCount(QDataStream&, uint32_t maxCount);

private:
    bool load(const QString& filename);              // load the GeoPolData from this data file
    void asyncLoad(const QString& filename);         // asynchronous load
    void abortLoad();                                // abort async load
    const auto& future() const { return m_loaded; }  // return the async future

    void setupFlags(GeoPolRegion&, const QMap<QString, QString>& flagMap);

    // Generate mapping of canonical flag names to the names under art/tags/flags
    static auto canonicalNameToFlagNameMap(const QString& root);

    static bool asyncLoadStatic(GeoPolMgr*, const QString& filename);  // asynchronous load

    static const uint32_t ztgpsBeginMagic     = 0xbeb10101; // begin magic
    static const uint32_t ztgpsEndMagic       = 0xbfb10f0f; // end magic
    static const uint8_t  ztgpsArrayEndMarker = 0xff;       // array end sanity marker

    GeoPolRegion     m_world;     // top level region (for whole world)
    QFuture<bool>    m_loaded;    // future set during async load
    std::atomic_bool m_asyncLoad; // true if async load has been initiated
    std::atomic_bool m_abortLoad; // try to abort a pending load

    GeoPolMgr(const GeoPolMgr&) = delete;
    GeoPolMgr& operator=(const GeoPolMgr&) = delete;
};

template <typename T>
GeoPolRegionVec GeoPolMgr::intersections(const T& item, GeoPolRegion::WorldOpt worldOpt) const
{
    if (m_asyncLoad && !future().result())  // Wait for loading to complete.
        return { };

    GeoPolRegionVec regions;
    m_world.intersections(item, regions, worldOpt);
    return regions;
}

template <typename T> const GeoPolRegion* GeoPolMgr::intersection(const T& item, GeoPolRegion::WorldOpt worldOpt) const
{
    if (const auto list = intersections(item, worldOpt); !list.isEmpty())
        return list.last();

    return nullptr;
}

template <typename T>
QStringList GeoPolMgr::intersectionNames(const T& item, GeoPolRegion::WorldOpt worldOpt) const
{
    QStringList names;

    for (const auto& region : intersections(item, worldOpt))
        names.append(region->name());

    return names;
}

template <typename T>
QStringList GeoPolMgr::intersectionHierarchicalNames(const T& item, GeoPolRegion::WorldOpt worldOpt) const
{
    return hierarchicalNames(intersections(item, worldOpt));
}

template <typename T> QString GeoPolMgr::canonicalizeName(const T& inputName)
{
    QString name(inputName);

    for (auto& c : name)
    {
        switch (c.unicode()) {
        case ' ':    [[fallthrough]];
        case '-':    c = '_'; break;
        case 0x00E0: c = 'a'; break;
        case 0x00E1: c = 'a'; break;
        case 0x00E2: c = 'a'; break;
        case 0x00E3: c = 'a'; break;
        case 0x00E4: c = 'a'; break;
        case 0x00E5: c = 'a'; break;
        case 0x00E7: c = 'c'; break;
        case 0x00E8: c = 'e'; break;
        case 0x00E9: c = 'e'; break;
        case 0x00EA: c = 'e'; break;
        case 0x00EB: c = 'e'; break;
        case 0x00EC: c = 'i'; break;
        case 0x00ED: c = 'i'; break;
        case 0x00EE: c = 'i'; break;
        case 0x00EF: c = 'i'; break;
        case 0x00F2: c = 'o'; break;
        case 0x00F3: c = 'o'; break;
        case 0x00F4: c = 'o'; break;
        case 0x00F5: c = 'o'; break;
        case 0x00F6: c = 'o'; break;
        case 0x00F8: c = 'o'; break;
        case 0x00F9: c = 'u'; break;
        case 0x00FA: c = 'u'; break;
        case 0x00FB: c = 'u'; break;
        case 0x00FC: c = 'u'; break;
        case 0x00FD: c = 'y'; break;
        case 0x00FF: c = 'y'; break;
        case 0x00C3: c = 'A'; break;
        case 0x00C4: c = 'A'; break;
        case 0x00C5: c = 'A'; break;
        case 0x00C8: c = 'E'; break;
        case 0x00C9: c = 'E'; break;
        case 0x00CA: c = 'E'; break;
        case 0x00CB: c = 'E'; break;
        case 0x00CC: c = 'I'; break;
        case 0x00CD: c = 'I'; break;
        case 0x00CE: c = 'I'; break;
        case 0x00CF: c = 'I'; break;
        case 0x00D2: c = 'O'; break;
        case 0x00D3: c = 'O'; break;
        case 0x00D4: c = 'O'; break;
        case 0x00D5: c = 'O'; break;
        case 0x00D6: c = 'O'; break;
        case 0x00D9: c = 'U'; break;
        case 0x00DA: c = 'U'; break;
        case 0x00DB: c = 'U'; break;
        case 0x00DC: c = 'U'; break;
        case 0x00DD: c = 'Y'; break;
        case 0x014D: c = 'o'; break;
        case 0x0160: c = 'S'; break;
        case 0x0161: c = 's'; break;
        case 0x017E: c = 'Z'; break;
        default:  break;
        }
    }

    return name;
}

#endif // GEOPOLMGR_H
