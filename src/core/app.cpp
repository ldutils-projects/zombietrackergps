/*
    Copyright 2020-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <type_traits>
#include <QVector>
#include <QString>
#include <QDataStream>
#include <marble/MarbleGlobal.h>

#include <src/util/versionedstream.h>
#include <src/util/ui.h>
#include <src/util/resources.h>

#include "src/ui/panes/pane.h"
#include "src/geo-io/geoio.h"
#include "src/util/cmdline.h"
#include "app.h"

App::App(int &argc, char **argv, const CmdLine& cmdLine) :
    AppBase(argc, argv, cmdLine)
{
    setupTranslators();
    m_rc = m_cmdLine.processArgsPostApp();
}

App::App(int& argc, const char** argv, const CmdLine& cmdLine) :
    App(argc, const_cast<char**>(argv), cmdLine)
{
}

App::~App()
{
}

App::InitStatic::InitStatic()
{
    setupResources();
    setupIcons();
    registerTypes();
}

void App::setupResources()
{
    // Resources that react to light or dark themes
    const QString art   = Util::IsLightTheme() ? "art-light.rcc" : "art-dark.rcc";
    const QString icons = Util::IsLightTheme() ? "icons-light.rcc" : "icons-dark.rcc";

    for (const QString& file : { { "art.rcc" },
                                 { "geopol.rcc" },
                                 { "us-nps-symbols.rcc" },
                                 { "gps-images.rcc" },
                                 art,
                                 icons } )
        loadResourceFile(file);
}

void App::setupIcons()
{
    QApplication::setWindowIcon(QIcon(":art/logos/projects/zombietrackergps.svg"));
}

void App::setupTranslators()
{
    loadTranslation(m_ztgpsTranslator, "ztgps");
}

// Allow conversion to QVariant
QDataStream& operator<<(QDataStream& out, const Marble::MapQuality& mc)
{
    return out << std::underlying_type_t<Marble::MapQuality>(mc);
}

QDataStream& operator>>(QDataStream& in, Marble::MapQuality& mc)
{
    return in >> reinterpret_cast<std::underlying_type_t<Marble::MapQuality>&>(mc);
}

QDataStream& operator<<(QDataStream& out, const Qt::SortOrder& s) {
    return out << int(s);
}

QDataStream& operator>>(QDataStream& in, Qt::SortOrder& s) {
    return in >> reinterpret_cast<int &>(s);
}

void App::registerTypes()
{
    qRegisterMetaType<TrackType>("TrackType");

    // Allow passing these things between threads
    qRegisterMetaTypeStreamOperators<Marble::MapQuality>("Marble::MapQuality");
    qRegisterMetaTypeStreamOperators<PaneClass>("PaneClass");
    qRegisterMetaTypeStreamOperators<TrackType>("TrackType");
    qRegisterMetaTypeStreamOperators<GeoFormat>("GeoFormat");
    qRegisterMetaTypeStreamOperators<Qt::SortOrder>("Qt::SortOrder");
    qRegisterMetaTypeStreamOperators<QVector<QVariant>>("QVector<QVariant>");
    qRegisterMetaTypeStreamOperators<CfgData::MapMoveMode>("CfgData::MapMoveMode");
    qRegisterMetaTypeStreamOperators<CfgData::AutoImportMode>("CfgData::AutoImportMode");
    qRegisterMetaTypeStreamOperators<CfgData::AutoImportPost>("CfgData::AutoImportPost");
    qRegisterMetaTypeStreamOperators<SimplifiableModel::SimplifyType>("SimplifiableModel::SimplifyType");
}

ChangeTrackingModel& App::getModel(Model mtype)
{
    switch (mtype) {
    case Model::Track:    return trackModel();
    case Model::View:     return viewModel();
    case Model::Filter:   return filterModel();
    case Model::Waypoint: return waypointModel();

    default: assert(0 && "Bad model type");
             return trackModel();
    }
}

const ChangeTrackingModel& App::getModel(Model mtype) const
{
    return const_cast<const ChangeTrackingModel&>(const_cast<App&>(*this).getModel(mtype));
}

QString App::modelDataSuffix(Model model)
{
    switch (model) {
    case Model::Track:    return ".track.ztgps";
    case Model::View:     return ".view.ztgps";
    case Model::Filter:   return ".filter.ztgps";
    case Model::Waypoint: return ".waypoint.ztgps";
    default:              return "";
    }
}

void App::newSession()
{
    for (App::Model mtype = App::Model::_First; mtype != App::Model::_LastSaved; Util::inc(mtype))
        getModel(mtype).clear();

    AppBase::newSession();  // reset undos
}

App::Model App::parseModel(const QString& name)
{
    const QString lower = name.toLower();

    if (lower == "trk" || lower == "track" || lower == "tracks")
        return App::Model::Track;
    if (lower == "view" || lower == "views")
        return App::Model::View;
    if (lower == "filter" || lower == "filters")
        return App::Model::Filter;
    if (lower == "wpt" || lower == "waypoint" || lower == "waypoints")
        return App::Model::Waypoint;
    if (lower == "geoloc" || lower == "location" || lower == "locations")
        return App::Model::GeoLoc;
    if (lower == "climb" || lower == "climbs")
        return App::Model::Climb;

    return App::Model::_Invalid;
}

QUrl App::ZtgpsWWW(WWW section)
{
    QUrl ztgpsWWW("https://zombietrackergps.net/ztgps");

    switch (section) {
    case WWW::Main:      break;  // nothing more to do
    case WWW::Donations: ztgpsWWW.setFragment("link-donations"); break;
    }

    return ztgpsWWW;
}
