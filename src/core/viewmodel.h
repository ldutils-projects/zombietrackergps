/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef VIEWMODEL_H
#define VIEWMODEL_H

#include <cmath>

#include <src/util/math.h>
#include <src/util/roles.h>
#include <src/core/modelmetadata.h>
#include <src/core/changetrackingmodel.h>
#include <src/core/duplicablemodel.h>
#include <src/core/removablemodel.h>

class ViewItem;
class Units;
class ViewParams;
class QModelIndex;

class ViewModel final :
        public ChangeTrackingModel,
        public RemovableModel,
        public DuplicableModel,
        public NamedItem,
        public ModelMetaData
{
    Q_OBJECT

public:
    enum {
        _First    = 0,
        Name      = _First,
        Icon      = Name,  // icon goes in name column
        CenterLat,
        CenterLon,
        Heading,
        Zoom,
        _Count,
    };

    ViewModel(QObject *parent = nullptr);

    using ChangeTrackingModel::appendRow;
    void setRow(const QString& name, const ViewParams& vp, const QModelIndex&);
    void appendRow(const QString& name, const ViewParams& vp, const QString& icon);

    ViewParams viewParams(const QModelIndex&) const;

    QVariant headerData(int section, Qt::Orientation,
                        int role = Qt::DisplayRole) const override;

    const Units& units(const QModelIndex& idx) const override;

    // *** begin Settings API
    using ChangeTrackingModel::save;
    void load(QSettings&) override;
    // *** end Settings API

    // *** begin Stream Save API
    quint32 streamMagic() const override;
    quint32 streamVersionMin() const override;
    quint32 streamVersionCurrent() const override;
    // *** end Stream Save API

    // *** begin ModelMetaData API
    static QString        mdName(ModelType);
    static bool           mdIsEditable(ModelType);
    static QString        mdTooltip(ModelType);
    static QString        mdWhatsthis(ModelType);
    static Qt::Alignment  mdAlignment(ModelType);
    static const Units&   mdUnits(ModelType);
    // *** end ModelMetaData API

    // *** begin NamedItem API
    static name_t getItemNameStatic();
    // *** end NamedItem API

private:
    Qt::DropActions supportedDropActions() const override { return Qt::MoveAction; }
    Qt::ItemFlags flags(const QModelIndex&) const override;
};

#endif // TAGMODEL_H
