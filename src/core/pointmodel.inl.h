/*
    Copyright 2021-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef POINTMODEL_INL_H
#define POINTMODEL_INL_H

#include "pointmodel.h"

template <typename T>
inline T PointModel::ptData(const PointItem& pt, const PointItem* nextPt, ModelType mt, int row, bool flt) const
{
    return pt.data<T>(mt, row, Util::RawDataRole, flt, firstPointInTrack(), nextPt, powerData());
}

template <typename T>
inline T PointModel::ptData(const PointItem& pt, ModelType mt, int row, bool flt) const
{
    return ptData<T>(pt, nextPointInSeg(pt), mt, row, flt);
}

// Interpolate data in column 'mt' at position 'dist' (even if between sample points) and return
// interpolated value.
template <class T>
T PointModel::interpolate(qreal dist, ModelType mt, T invalid) const
{
    const auto [lower, lowerRow, upper, upperRow] = getItemBound(dist);
    if (lower == nullptr || upper == nullptr) // bogus
        return invalid;

    const qreal interp = (upper->distance() == lower->distance()) ? 0.0 :
            (dist - qreal(lower->distance())) / qreal(upper->distance() - lower->distance());

    return T(Util::Lerp(ptData<T>(*lower, mt, lowerRow, true),
                        ptData<T>(*upper, mt, upperRow, true), interp));
}

#endif // POINTMODEL_INL_H
