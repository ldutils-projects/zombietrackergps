/*
    Copyright 2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SPEEDEDITMODEL_H
#define SPEEDEDITMODEL_H

#include <QModelIndexList>

#include <src/util/nameditem.h>

// Model supporting reversal of entries
class SpeedEditModel :
        virtual public NamedItemInterface
{
public:
    SpeedEditModel();

    virtual void unsetSpeed(const QModelIndexList&) = 0;  // unset speed for selection
    virtual void unsetSpeed() = 0;                        // unset speed for all
};

#endif // SPEEDEDITMODEL_H
