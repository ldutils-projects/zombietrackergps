/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PERSONMODEL_H
#define PERSONMODEL_H

#include <QModelIndex>
#include <QString>
#include <QDate>

#include <src/core/modelmetadata.h>
#include <src/core/contentaddrmodel.h>

#include "src/strongops.h"

class PersonItem;
class Units;

class PersonModel final : public ContentAddrModel, public ModelMetaData
{
    Q_OBJECT

public:
    enum {
        Name = 0,     // person name
        Weight,       // weight in Kg
        Efficiency,   // biomechanical efficiency
        Birthdate,    // birth year (for age computations, max HR, etc)
        MaxHR,        // max heart rate.  uses 220-age if unset.
        FTP,          // functional threshold power
        _Count,
    };

    PersonModel(QObject* parent = nullptr);

    [[nodiscard]] QVariant headerData(int section, Qt::Orientation orientation,
                                      int role = Qt::DisplayRole) const override;

    // Reset Birthdate and MaxHR fields to unset state
    void resetRows(const QModelIndexList&);

    // Returns age in days, or -1 if no birthdate set
    [[nodiscard]] int ageInDays(const QModelIndex&, const QDate& refDate = QDate::currentDate()) const;
    // Returns max BPM, using 220-age if no birthday, or -1 if no data available.
    [[nodiscard]] int maxBpm(const QModelIndex&, const QDate& refDate = QDate::currentDate()) const;
    // Returns FTP
    [[nodiscard]] Power_t ftp(const QModelIndex&, const QDate& refDate = QDate::currentDate()) const;

    // *** begin ModelMetaData API
    static QString        mdName(ModelType);
    static bool           mdIsEditable(ModelType);
    static bool           mdIsChartable(ModelType);
    static QString        mdTooltip(ModelType);
    static QString        mdWhatsthis(ModelType);
    static Qt::Alignment  mdAlignment(ModelType);
    static int            mdDataRole(ModelType);
    static const Units&   mdUnits(ModelType);
    // *** end ModelMetaData API

    PersonModel& operator=(const PersonModel&);

private:
    PersonItem* getItem(const QModelIndex&) const;

    Qt::DropActions supportedDropActions() const override { return Qt::MoveAction; }
    Qt::ItemFlags flags(const QModelIndex&) const override;
};

#endif // PERSONMODEL_H
