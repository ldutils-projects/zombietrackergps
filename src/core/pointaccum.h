/*
    Copyright 2022-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef POINTACCUM_H
#define POINTACCUM_H

#include <QString>

#include "src/fwddecl.h"

#include "pointitem.h"

// For point averaging. We can't accumulate into a point, because some of the
// fields such as Hr, etc, do not have enough precision. Instead, we accumulate
// into this structure. Each individual value has a unique weight, because there is
// no guarantee every point has the same values available. E.g, point 1 and 3
// may have a HR, while 2 does not.
class PointAccum {
public:
    PointAccum(const PointItem& first);
    PointAccum& add(const PointItem& pt, qreal weight = 1.0);
    [[nodiscard]] PointItem average() const;
    [[nodiscard]] PointItem average(const QDateTime& ts) const;

private:
    Elaps_t m_msecFromT0     = 0;     // msec from first point's time
    Lon_t   m_lon            = 0.0;   // accum lon
    Lat_t   m_lat            = 0.0;   // accum lat
    Ele_t   m_ele            = 0.0f;  // accum ele
    Ele_t   m_fltEle         = 0.0f;  // accum filtered ele
    Dist_t  m_dist           = 0.0f;  // accum dist
    Speed_t m_speed          = Speed_t(0.0f);  // accum speed
    Power_t m_power          = 0.0f;  // accum power
    Grade_t m_grade          = 0.0f;  // accum grade
    float   m_depth          = 0.0f;  // accum depth
    Temp_t  m_atemp          = 0.0f;  // accum air temp
    Temp_t  m_wtemp          = 0.0f;  // accum air temp
    float   m_course         = 0.0f;  // accum course
    float   m_bearing        = 0.0f;  // accum bearing
    quint32 m_hr             = 0;     // accum hr
    quint32 m_cad            = 0;     // accum cad

    QString m_name;                   // string data
    QString m_comment;                // ...
    QString m_desc;                   // ...
    QString m_symbol;                 // ...
    QString m_type;                   // ...

    PointItem::Flags m_flags = PointItem::Flags::NoFlags;     // accum flags

    // weights for each value
    qreal m_timeWeight       = 0.0;
    qreal m_locWeight        = 0.0;
    qreal m_eleWeight        = 0.0;
    qreal m_fltEleWeight     = 0.0;
    qreal m_distWeight       = 0.0;
    qreal m_speedWeight      = 0.0;
    qreal m_powerWeight      = 0.0;
    qreal m_depthWeight      = 0.0;
    qreal m_atempWeight      = 0.0;
    qreal m_wtempWeight      = 0.0;
    qreal m_courseWeight     = 0.0;
    qreal m_bearingWeight    = 0.0;
    qreal m_hrWeight         = 0.0;
    qreal m_cadWeight        = 0.0;

    const PointItem& m_first;
};

#endif // POINTACCUM_H
