/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>

#include <src/core/modelmetadata.inl.h>

#include "src/util/ui.h"

#include "filtermodel.h"
#include "filteritem.h"
#include "cfgdata.h"
#include "app.h"

FilterModel::FilterModel(QObject* parent) :
    ChangeTrackingModel(new FilterItem(), parent),
    NamedItem(getItemNameStatic())
{
    setHorizontalHeaderLabels(headersList<FilterModel>());
}

QVariant FilterModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (const QVariant val = ModelMetaData::headerData<FilterModel>(section, orientation, role); val.isValid())
        return val;

    return ChangeTrackingModel::headerData(section, orientation, role);
}

const Units& FilterModel::units(const QModelIndex& idx) const
{
    return mdUnits(idx.column());
}

Qt::ItemFlags FilterModel::flags(const QModelIndex& idx) const
{
    return ChangeTrackingModel::flags(idx) | ModelMetaData::flags<FilterModel>(idx) |
        Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled;
}

void FilterModel::load(QSettings& settings)
{
    ChangeTrackingModel::load(settings);
    setHorizontalHeaderLabels(headersList<FilterModel>());  // put header labels back, in case not present in save.
}

QString FilterModel::mdName(ModelType d)
{
    switch (d) {
    case FilterModel::Name:    return QObject::tr("Name");
    case FilterModel::Query:   return QObject::tr("Query");
    case FilterModel::_Count:  break;
    }

    assert(0 && "Unknown TrackData value");
    return "";
}

bool FilterModel::mdIsEditable(ModelType)
{
    return true;
}

// tooltip for container column header
QString FilterModel::mdTooltip(ModelType mt)
{
    const bool editable = mdIsEditable(mt);

    switch (mt) {
    case FilterModel::Name:
        return makeTooltip(tr("Descriptive name."), editable);

    case FilterModel::Query:
        return makeTooltip(tr("Query string."), editable);

    case FilterModel::_Count:
        break;
    }

    assert(0 && "Unknown TrackData value");
    return nullptr;
}

// tooltip for container column header
QString FilterModel::mdWhatsthis(ModelType td)
{
    return mdTooltip(td);  // just pass through the tooltip
}

Qt::Alignment FilterModel::mdAlignment(ModelType)
{
    return Qt::AlignLeft | Qt::AlignVCenter;
}

const Units& FilterModel::mdUnits(ModelType)
{
    static const Units rawString(Format::String);
    return rawString;
}

NamedItemInterface::name_t FilterModel::getItemNameStatic()
{
    return { tr("Filter"), tr("Filters") };
}

int FilterModel::mdDataRole(ModelType)
{
    return Util::RawDataRole;
}

quint32 FilterModel::streamMagic() const
{
    return App::NativeMagic + quint32(App::Model::Filter);
}

quint32 FilterModel::streamVersionMin() const
{
    return App::NativeVersionMin;
}

quint32 FilterModel::streamVersionCurrent() const
{
    return App::NativeVersionCurrent;
}
