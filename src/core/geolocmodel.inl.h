/*
    Copyright 2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOLOCMODEL_INL_H
#define GEOLOCMODEL_INL_H

#include "geolocmodel.h"
#include "geolocheader.h"

template<typename T>
GeoLocModel::Cache<T>::Cache(const QString& fileName, uint8_t type, uint32_t size, uint32_t before, uint32_t after, GeoLocModel& m) :
    m_file(fileName),
    m_map(nullptr),
    m_cur(nullptr),
    m_fileHdr(type),
    m_model(m),
    m_cacheSize(size),
    m_fetchBefore(before),
    m_fetchAfter(after)
{
    m_fileHdr.read(open());
    close();
}

template<typename T> const uchar*& GeoLocModel::Cache<T>::open()
{
    if (m_map == nullptr)
        if (!m_file.isOpen())
            if (m_file.open(QIODevice::ReadOnly))
                m_map = m_file.map(0, m_file.size());

    m_cur = m_map; // start from beginning
    return m_cur;
}

template<typename T> void GeoLocModel::Cache<T>::close()
{
    m_file.unmap(const_cast<uchar*>(m_map));
    m_file.close();

    m_map = m_cur = nullptr;
}

template<typename T> void GeoLocModel::Cache<T>::trimCache(uint32_t size)
{
    // Trim cache
    while (uint32_t(m_order.size()) > size) {
        m_cache.remove(m_order.front());
        m_order.pop_front();
    }
}

template<typename T>
const T& GeoLocModel::Cache<T>::operator()(uint32_t record)
{
    static const T empty;

    // Return existing record, if any.
    if (const auto recordIt = m_cache.find(record); recordIt != m_cache.end())
        return *recordIt;

    m_model.m_closeTimer.start();

    // Otherwise, fetch new record.
    if (const auto recordIt = fetch(record); recordIt != m_cache.end())
        return *recordIt;

    // We failed.
    return empty;
}

inline const GeoLocEntry& GeoLocModel::locDataFor(const Node* node, int row) const
{
    assert(node != nullptr && node->hasLeafs());
    return m_locCache(m_nameCache(node->leafRec(row)).locRec());
}

inline const GeoLocEntry& GeoLocModel::locDataFor(const Node* node, uint32_t row) const
{
    return locDataFor(node, int(row));
}

template<typename T>
typename GeoLocModel::Cache<T>::cache_iterator GeoLocModel::Cache<T>::fetch(uint32_t record)
{
    const uint32_t fetchStart = (record >= m_fetchBefore) ? (record - m_fetchBefore) : 0;
    const uint32_t fetchEnd   = std::min(record + m_fetchAfter + 1, m_fileHdr.size());

    trimCache(m_cacheSize - (fetchEnd - fetchStart));

    // TODO: instead of collecting a list, let the T class insert it directly.
    uint32_t pos = fetchStart;
    for (const T& item : T::read(open(), m_model, fetchStart, fetchEnd))
        m_order.push_back(m_cache.insert(pos++, item).key());

    return m_cache.find(record);
}

#endif // GEOLOCMODEL_INL_H
