/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <type_traits>
#include <algorithm>

#include <QApplication>
#include <QBuffer>

#include "cfgdata.h"

#include <src/util/util.h>
#include <src/util/ui.h>
#include <src/util/icons.h>
#include "src/core/trackmodel.h"
#include "src/core/climbmodel.h"
#include "src/core/pointmodel.h"
#include "src/core/app.h"

decltype(CfgData::svgColorizer)    CfgData::svgColorizer;
decltype(CfgData::emptyPointModel) CfgData::emptyPointModel = nullptr;

CfgData::CfgData(bool defaults) :
    CfgDataBase(currentCfgDataVersion),
    eleFilterSize(5),
    tooltipInlineLimit(512),
    tooltipFullLimit(4096),
    unitsTrkLength(Format::Distkm,           2, false),
    unitsLegLength(Format::Distm,            0, false),
    unitsDuration(Format::DurDHMS,           0, false),
    unitsTrkDate(Format::DateTimeLocaleShort,0, false),
    unitsTrkTime(Format::Timehhmmss,         0, false),
    unitsPointDate(Format::DateTimehhmmss,   0, false),
    unitsTz(Format::TzOffset,                0, false),
    unitsElevation(Format::Distm,            0, false),
    unitsLat(Format::GeoPosDMS,              2, false, 'S', 'N'),
    unitsLon(Format::GeoPosDMS,              2, false, 'W', 'E'),
    unitsSpeed(Format::SpeedKPH,             1, false),
    unitsClimb(Format::Distm,                2, false),
    unitsArea(Format::Areakm2,               2, false),
    unitsTemp(Format::TempC,                 0, false),
    unitsSlope(Format::SlopePct,             0, false),
    unitsPower(Format::Watt,                 0, false),
    unitsEnergy(Format::kcal,                0, false),
    unitsWeight(Format::kg,                  0, false),
    unitsPct(Format::Percent,                2, false),
    unitsCad(Format::RevsPerM,               0, false),
    unitsHr(Format::BeatsPerM,               0, false),
    unassignedTrackColor(QRgb(0x00c80000)),
    outlineTrackColor(QRgb(0x00000000)),
    defaultTrackWidthC(2.0),
    defaultTrackWidthF(3.0),
    defaultTrackWidthO(0.5),
    currentTrackWidthC(6.0),
    currentTrackWidthF(4.0),
    currentTrackWidthO(2.5),
    defaultTrackAlphaC(128),
    defaultTrackAlphaF(196),
    currentTrackAlphaC(255),
    currentTrackAlphaF(255),
    mapMoveMode(MapMoveMode::ActiveTrack),
    mapMovePoints(true),
    mapMoveWaypoints(true),
    mapInertialMovement(false),
    defaultPointIcon(":art/points/Circle/Dark/Wht10Blk00.svg"),
    defaultPointIconSize(8),
    defaultPointIconProx(16),
    selectedPointIcon(":art/points/Circle/Dark/Yel10Blk00.svg"),
    selectedPointIconSize(11),
    currentPointIcon(":art/points/Bullseye/Dark/Yel10Blk00.svg"),
    currentPointIconSize(20),
    gpsdLivePointIcon(":art/points/Bullseye/Dark/Cya10Blk00.svg"),
    gpsdLivePointIconSize(20),
    waypointDefaultIcon(":art/points/Pins/Light/Blu10Wht10.svg"),
    waypointDefaultIconSize(16),
    waypointIconSize(20),
    trackNoteIcon(Util::LocalThemeIcon("actions/22/note.svg")),
    colorizeTagIcons(true),
    iconSizeTrack(40, 28),
    flagSizeTrack(40, 28),
    iconSizeView(45, 31),
    iconSizeTag(45, 31),
    iconSizeFilter(40, 28),
    iconSizeClimb(40, 28),
    maxTrackPaneIcons(5),
    maxTrackPaneFlags(4),
    autoImportMode(AutoImportMode::Disabled),
    autoImportPattern("*.fit *.gpx *.kml *.tcx"),
    autoImportPost(AutoImportPost::Backup),
    autoImportBackupSuffix(".imported"),
    autoImportStdout(false),
    autoImportTimeout(15),
    trackColorizer(&app().trackModel()),
    pointColorizer(emptyPointModel),
    climbColorizer(&app().climbModel()),
    trkPtMarkerColor(Util::IsLightTheme() ? QRgb(0xff999900) : QRgb(0xffffff00)),
    trkPtRangeColor(app().palette().color(QPalette::Highlight)),
    trkPtLineWidth(1.5),
    trkPtMarkerWidth(1.0),
    trkPtRangeWidth(3.0),
    asMaxDateSpans(260),
    asBarWidth(75),
    hillMinGrade(0.03),
    hillMinHeight(75.0),
    hillGradeLength(100.0),
    mapUndoStill(2),
    maxUndoCountView(25)
{
    if (defaults)
        setupDefaultConfig();
}

CfgData::CfgData(const CfgData& rhs) :
    CfgData()
{
    *this = rhs;
}

CfgData::InitStatic::InitStatic()
{
    if (emptyPointModel == nullptr)
        emptyPointModel = new PointModel(nullptr);
}

size_t CfgData::size(const QString& str)
{
    return size_t(sizeof(QString) + str.toUtf8().size());
}

size_t CfgData::size(const QStringList& str)
{
    return std::accumulate(str.begin(), str.end(), 0, [](size_t s, const QString& str) {
        return size(str) + s;
    });
}

size_t CfgData::size(const TreeModel& model)
{
    QBuffer tmp;
    if (!tmp.open(QBuffer::WriteOnly))
        return 0;

    model.saveForUndo(tmp, QModelIndex());

    return tmp.size();
}

size_t CfgData::size() const
{
    return sizeof(*this) +
            // strings
            size(defaultPointIcon) +
            size(selectedPointIcon) +
            size(currentPointIcon) +
            size(gpsdLivePointIcon) +
            size(waypointDefaultIcon) +
            size(trackNoteIcon) +
            size(dataAutosavePath) +
            size(autoImportDir) +
            size(autoImportPattern) +
            size(autoImportTags) +
            size(autoImportBackupSuffix) +
            size(autoImportBackupDir) +
            size(autoImportCommand) +
            // models
            size(tags) +
            size(people) +
            size(zones) +
            size(trackColorizer) +
            size(pointColorizer) +
            size(climbColorizer) +
            size(trkPtColor);
}

std::pair<QString, bool> CfgData::pngToSvg(const QString& iconName)
{
    static const QRegularExpression pngRe("(.*)/(" PointColorNameRe ")[.]png");    // *.png -> Dark/*.svg
    static const QRegularExpression renameRe("(:art/points/[^/]+)/([A-Z][a-z][a-z][0-9][0-9])Blk00[.]svg"); // *.svg  -> Light/*.svg

    if (const auto match = pngRe.match(iconName); match.hasMatch())
        return std::make_pair(match.captured(1) + "/Dark/" + match.captured(2) + ".svg", true);

    if (const auto match = renameRe.match(iconName); match.hasMatch())
        return std::make_pair(match.captured(1) + "/Light/" + match.captured(2) + "Wht10.svg", true);

    return std::make_pair(iconName, false);
}

// Setup default config, e.g, on first run.
void CfgData::setupDefaultConfig()
{
    // Update from version 0
    // Structure to ensure restoration of cfg data version on stack pop
    class RestoreCfgData {
    public:
        RestoreCfgData(CfgData& cfgData, uint32_t newVer) : cfgData(cfgData), oldVer(cfgData.cfgDataVersion) {
            cfgData.cfgDataVersion = newVer;
        }
        ~RestoreCfgData() { cfgData.cfgDataVersion = oldVer; }
    private:
        CfgData& cfgData;
        uint32_t oldVer;
    } restoreCfgData(*this, 0);

    updateTags();
    defaultPeople();
    updateTrackColorizers();
    updatePointColorizers();
    updateClimbColorizers();
    updatePointIcons();
    updateZones();
}

// Return tag model data to create an entry from the provided data.
TreeItem::ItemData CfgData::tag(const QString& dir, uint32_t ver, const QString& name, QRgb color,
                                QString iconName,
                                qreal CdA, qreal weight, qreal rr,
                                qreal efficiency, qreal bioPct, const char* medium)
{
    TreeItem::ItemData data(TagModel::_Count);
    // If the cfg data version is new enough, don't re-add the tag (in case user removed it in their config)
    if (cfgDataVersion >= ver)
        return data;

    if (iconName.isEmpty())
        iconName = name;

    data[TagModel::Name]  = name.mid(name.lastIndexOf('/') + 1);

    if (color != QRgb(0))  data[TagModel::Color]      = QColor(color);
    if (CdA != 0.0)        data[TagModel::CdA]        = CdA;
    if (weight != 0.0)     data[TagModel::Weight]     = weight;
    if (rr != 0.0)         data[TagModel::RR]         = rr;
    if (efficiency != 0.0) data[TagModel::Efficiency] = efficiency;
    if (bioPct >= 0.0)     data[TagModel::BioPct]     = bioPct;
    if (medium != nullptr) data[TagModel::Medium]     = QString(medium);

    data[TagModel::Icon] = QString(":art/tags/") + dir + "/" + iconName + ".svg";

    return data;
}

bool CfgData::appendTagsIfMissing(const QString& dir, uint32_t ver, const QVector<TreeItem::ItemData>& data)
{
    // If the cfg data version is new enough, don't re-add the tag (in case user removed it in their config)
    if (cfgDataVersion >= ver)
        return true;

    bool additions = false;

    // Add category if missing
    if (const QModelIndex idx = find(dir); !idx.isValid()) {
        if (!tags.appendRow(true, { dir } ))
            return false;

        additions = true;
    }

    // Add missing tags to the category.
    if (const QModelIndex parent = find(dir); parent.isValid() && tags.isCategory(parent)) {
        for (const auto& rowData : data) {
            if (const QString tagName = rowData[TagModel::Name].toString(); !tagName.isEmpty()) {
                // Remove old (for updating new data)
                if (const QModelIndex idx = find(tagName); idx.isValid())
                    tags.removeRow(idx.row(), parent);

                // Add new tag
                if (!tags.appendRow(false, rowData, parent))
                    return false;
                additions = true;
            }
        }

        // Sort additions
        if (additions)
            tags.sort(TagModel::Name, parent);
    }

    return true;
}

void CfgData::defaultTagsActivity()
{
    static const QString name = "Activity";

    appendTagsIfMissing(name, 8,
    { tag(name, 8, "Bikepack",    0xff50b088, "", 0.425, 18.0, 0.045, 0.95, 1.0, air),
      tag(name, 1, "Camp",        0xff009548),
      tag(name, 1, "Commute",     0xff4371d5, "", 0.400, 12.5, 0.00325, 0.97, 1.0, air),
      tag(name, 1, "Errands",     0xffb05075),
      tag(name, 1, "Recreation",  0xff688800),
      tag(name, 1, "Soccer",      0xff779777),
      tag(name, 1, "Work",        0xffafa844),
      tag(name, 1, "Zombies",     0xffb364d4),
    });
}

void CfgData::defaultTagsTransport()
{
    static const QString name = "Transport";

    appendTagsIfMissing(name,  25,
    { tag(name,  1, "Board",       0xffd9efff),
      tag(name,  6, "Canoe",       0xff226496, "", 0.500, 34.0,   0.00,  0.95, 1.0, water),
      tag(name,  1, "Car",         0xff96651f, "", 0.700, 1400.0, 0.03,  0.25, 0.0, air),
      tag(name, 22, "CargoBike",   0xff66c653),
      tag(name,  1, "Climb",       0xff9c977f),
      tag(name,  1, "Cross",       0xff33eedd, "", 0.400, 13.0,  0.0030, 0.97, 1.0, air),
      tag(name, 17, "FatBike",     0xffb37048, "", 0.450, 14.0,  0.0100, 0.95, 1.0, air),
      tag(name,  8, "Gravel",      0xffe6c646, "", 0.420, 12.5,  0.0045, 0.95, 1.0, air),
      tag(name,  2, "Handcycle",   0xff33e1dd, "", 0.399, 12.0,  0.0030, 0.98, 1.0, air),
      tag(name,  1, "HangGlide",   0xffa14885),
      tag(name,  1, "Heli",        0xff00c1c1),
      tag(name,  1, "Hike",        0xff04b645, "", 1.0, 1.0, 0.045),
      tag(name,  1, "Horse",       0xffdf9140),
      tag(name,  1, "Motorcycle",  0xff4f7882),
      tag(name,  1, "Mountain",    0xffc37938, "", 0.450, 13.0,  0.0060, 0.95, 1.0, air),
      tag(name,  1, "Nordic",      0xffa9b7dc),
      tag(name, 12, "Paddleboard", 0xff3884c6),
      tag(name,  2, "Parachute",   0xffb34775),
      tag(name,  1, "Paraglide",   0xff813875),
      tag(name,  1, "Plane",       0xff88478d),
      tag(name, 25, "Recumbent",   0xff3adfca, "", 0.380, 17.0,  0.0030, 0.98, 1.0, air),
      tag(name,  1, "Road",        0xff00d1ff, "", 0.325, 12.0,  0.0025, 0.98, 1.0, air),
      tag(name,  1, "Run",         0xffdf9100, "", 1.0, 1.0, 0.063),
      tag(name,  1, "Sail",        0xff1347cc),
      tag(name,  1, "Ski",         0xffa9b7dc),
      tag(name, 17, "Snowmobile",  0xff89a7cc),
      tag(name, 12, "Snowshoe",    0xffc9d7cc),
      tag(name,  1, "Surf",        0xff1367cc),
      tag(name,  1, "Swim",        0xff008bd0),
      tag(name, 25, "Tandem",      0xff7acd75, "", 0.350, 28.0,  0.0028, 0.98, 1.0, air),
      tag(name,  1, "Train",       0xff6e6f94),
      tag(name,  1, "TT",          0xff02ff9e, "", 0.240, 8.50,  0.0020, 0.98, 1.0, air),
      tag(name,  2, "Wheelchair",  0xff55c1dd, "", 0.399, 12.0,  0.0030, 0.98, 1.0, air),
      tag(name, 16, "WinterBike",  0xffaabbcc, "", 0.450, 12.5,  0.0150, 0.98, 1.0, air),
    } );
}

void CfgData::defaultTagsSeasons()
{
    static const QString name = "Seasons";

    appendTagsIfMissing(name, 10,
    { tag(name, 10, "Autumn",     0x0),
      tag(name, 10, "Spring",     0x0),
      tag(name, 10, "Summer",     0x0),
      tag(name, 10, "Winter",     0x0),
      tag(name, 10, "Winter-2",   0x0),
    } );
}

void CfgData::defaultTagsSigns()
{
    static const QString name = "Signs";

    appendTagsIfMissing(name, 26,
    { tag(name, 26, "BikeRoute-1",     0x0),
      tag(name, 26, "BikeRoute-2",     0x0),
      tag(name, 26, "Bicycle",         0x0),
      tag(name, 26, "Bus",             0x0),
      tag(name, 26, "LeftOrRight",     0x0),
      tag(name, 26, "Left",            0x0),
      tag(name, 26, "Right",           0x0),
      tag(name, 26, "StraightOrLeft",  0x0),
      tag(name, 26, "StraightOrRight", 0x0),
      tag(name, 26, "Straight",        0x0),
    } );
}

void CfgData::defaultTagsWeather()
{
    static const QString name = "Weather";

    appendTagsIfMissing(name, 10,
    { tag(name,  1, "Clear",        0x0),
      tag(name,  1, "Clouds",       0x0),
      tag(name, 10, "Cold",         0x0),
      tag(name,  1, "Fog",          0x0),
      tag(name, 10, "Hot",          0x0),
      tag(name,  1, "Overcast",     0x0),
      tag(name,  1, "Showers-Day",  0x0),
      tag(name,  1, "Snow",         0x0),
      tag(name,  1, "Storm",        0x0),
      tag(name, 10, "Warm",         0x0),
      tag(name,  9, "Wind",         0x0)
    } );
}

void CfgData::defaultTagsMisc()
{
    static const QString name = "Misc";

    appendTagsIfMissing(name, 22,
    { tag(name,  4, "BatteryLow",      0x0, "Battery/BatteryLow"),
      tag(name,  4, "Bonk",            0x0, "Frown"),
      tag(name,  3, "BicyclesOnly",    0x0),
      tag(name,  3, "Earth-01",        0x0),
      tag(name,  3, "Earth-02",        0x0),
      tag(name,  3, "Earth-03",        0x0),
      tag(name,  3, "Earth-04",        0x0),
      tag(name,  3, "EcoLeaf",         0x0),
      tag(name, 10, "Firefighter",     0x0),
      tag(name,  8, "Forest",          0x0),
      tag(name,  3, "HillUp",          0x0),
      tag(name,  3, "HillDown",        0x0),
      tag(name,  5, "Home",            0x0),
      tag(name,  5, "Hourglass",       0x0),
      tag(name, 15, "Cache",           0x0),
      tag(name,  4, "Injury",          0x0, "Medical/MedKit"),
      tag(name,  3, "Internet",        0x0),
      tag(name,  3, "MapPin",          0x0),
      tag(name, 22, "Package",         0x0),
      tag(name, 21, "Patch",           0x0),
      tag(name,  3, "Recent",          0x0),
      tag(name,  3, "Recycle",         0x0),
      tag(name, 26, "Route",           0xff449966),
      tag(name,  3, "Satellite",       0x0),
      tag(name, 22, "Shopping-01",     0x0),
      tag(name, 10, "Wildfire",        0x0)
    } );
}

void CfgData::updateTags()
{
    defaultTagsActivity();
    defaultTagsTransport();
    defaultTagsMisc();
    defaultTagsSeasons();
    defaultTagsSigns();
    defaultTagsWeather();
}

void CfgData::defaultPeople()
{
    people.appendRows({ { "Male 1",   70.0_kg, 0.22 },
                        { "Female 1", 60.0_kg, 0.22 } });
}

void CfgData::updateZones()
{
    if (cfgDataVersion < 23)
        zones.setPresetModel(1);  // default to 5 zone model
}

// Not static data because we react to IsLightTheme
QVector<QColor> CfgData::valColorSet()
{
    // Colors for default colorizer entries
    return {
        QRgb(Util::IsLightTheme() ? 0xffcc0000 : 0xffff0000),
        QRgb(Util::IsLightTheme() ? 0xffbb6600 : 0xffff9900),
        QRgb(Util::IsLightTheme() ? 0xffaaaa00 : 0xffffff00),
        QRgb(Util::IsLightTheme() ? 0xff00bb77 : 0xff00ffaa),  // This is swapped with...
        QRgb(Util::IsLightTheme() ? 0xff0077bb : 0xff0099ff),  // this on format update: see updateColors
        QRgb(Util::IsLightTheme() ? 0xff8800cc : 0xff9900ff),
    };
}

// Not static data because we react to IsLightTheme
QVector<QColor> CfgData::ampmColorSet()
{
    return {
        Util::IsLightTheme() ? QRgb(0xff77aadd) : QRgb(0xff99ccff),
        Util::IsLightTheme() ? QRgb(0xffddaa77) : QRgb(0xffffcc99),
    };
}

void CfgData::updateTrackColorizers()
{
    const auto valColor  = valColorSet();
    const auto ampmColor = ampmColorSet();

    if (cfgDataVersion < 1) {
        trackColorizer.appendRows({
            { TrackModel::Ascent,       "Ascent >= 1000m",     valColor.at(0) },
            { TrackModel::Ascent,       "Ascent >= 750m",      valColor.at(1) },
            { TrackModel::Ascent,       "Ascent >= 500m",      valColor.at(2) },
            { TrackModel::Ascent,       "Ascent >= 250m",      valColor.at(3) },
            { TrackModel::Ascent,       "Ascent >= 100m",      valColor.at(4) },

            { TrackModel::BeginDate,    "Begin_Time < 12:00",  ampmColor.at(0) }, // date colorized by am/pm time
            { TrackModel::BeginDate,    "Begin_Time >= 12:00", ampmColor.at(1) }, // ...
            { TrackModel::EndDate,      "End_Time < 12:00",    ampmColor.at(0) }, // ...
            { TrackModel::EndDate,      "End_Time >= 12:00",   ampmColor.at(1) }, // ...
            { TrackModel::BeginTime,    "Begin_Time < 12:00",  ampmColor.at(0) },
            { TrackModel::BeginTime,    "Begin_Time >= 12:00", ampmColor.at(1) },
            { TrackModel::EndTime,      "End_Time < 12:00",    ampmColor.at(0) },
            { TrackModel::EndTime,      "End_Time >= 12:00",   ampmColor.at(1) },

            { TrackModel::MovingTime,   "Moving_Time >= 5h",   valColor.at(0) },
            { TrackModel::MovingTime,   "Moving_Time >= 4h",   valColor.at(1) },
            { TrackModel::MovingTime,   "Moving_Time >= 3h",   valColor.at(2) },
            { TrackModel::MovingTime,   "Moving_Time >= 2h",   valColor.at(3) },
            { TrackModel::MovingTime,   "Moving_Time >= 1h",   valColor.at(4) },

            { TrackModel::TotalTime,    "Total_Time >= 5h",    valColor.at(0) },
            { TrackModel::TotalTime,    "Total_Time >= 4h",    valColor.at(1) },
            { TrackModel::TotalTime,    "Total_Time >= 3h",    valColor.at(2) },
            { TrackModel::TotalTime,    "Total_Time >= 2h",    valColor.at(3) },
            { TrackModel::TotalTime,    "Total_Time >= 1h",    valColor.at(4) },

            { TrackModel::Length,       "Length >= 25km & Tags =~ Hike", valColor.at(0) },
            { TrackModel::Length,       "Length >= 20km & Tags =~ Hike", valColor.at(1) },
            { TrackModel::Length,       "Length >= 15km & Tags =~ Hike", valColor.at(2) },
            { TrackModel::Length,       "Length >= 10km & Tags =~ Hike", valColor.at(3) },
            { TrackModel::Length,       "Length >= 5km  & Tags =~ Hike", valColor.at(4) },

            { TrackModel::Length,       "Length >= 100km",     valColor.at(0) },
            { TrackModel::Length,       "Length >= 80km",      valColor.at(1) },
            { TrackModel::Length,       "Length >= 60km",      valColor.at(2) },
            { TrackModel::Length,       "Length >= 40km",      valColor.at(3) },
            { TrackModel::Length,       "Length >= 20km",      valColor.at(4) },

            { TrackModel::MaxElevation, "Max_Elev >= 5000m",   valColor.at(0) },
            { TrackModel::MaxElevation, "Max_Elev >= 4000m",   valColor.at(1) },
            { TrackModel::MaxElevation, "Max_Elev >= 3000m",   valColor.at(2) },
            { TrackModel::MaxElevation, "Max_Elev >= 2000m",   valColor.at(3) },
            { TrackModel::MaxElevation, "Max_Elev >= 1000m",   valColor.at(4) },

            { TrackModel::MinElevation, "Min_Elev >= 5000m",   valColor.at(0) },
            { TrackModel::MinElevation, "Min_Elev >= 4000m",   valColor.at(1) },
            { TrackModel::MinElevation, "Min_Elev >= 3000m",   valColor.at(2) },
            { TrackModel::MinElevation, "Min_Elev >= 2000m",   valColor.at(3) },
            { TrackModel::MinElevation, "Min_Elev >= 1000m",   valColor.at(4) },

            { TrackModel::BasePeak,     "Peak-Base >= 1200m",  valColor.at(0) },
            { TrackModel::BasePeak,     "Peak-Base >= 800m",   valColor.at(1) },
            { TrackModel::BasePeak,     "Peak-Base >= 600m",   valColor.at(2) },
            { TrackModel::BasePeak,     "Peak-Base >= 400m",   valColor.at(3) },
            { TrackModel::BasePeak,     "Peak-Base >= 200m",   valColor.at(4) },

            { TrackModel::MaxGrade,     "Max_Grade >= 20%",    valColor.at(0) },
            { TrackModel::MaxGrade,     "Max_Grade >= 15%",    valColor.at(1) },
            { TrackModel::MaxGrade,     "Max_Grade >= 10%",    valColor.at(2) },
            { TrackModel::MaxGrade,     "Max_Grade >= 5%",     valColor.at(3) },
            { TrackModel::MaxGrade,     "Max_Grade >= 2%",     valColor.at(4) },

            { TrackModel::MinGrade,     "Min_Grade <= -20%",   valColor.at(0) },
            { TrackModel::MinGrade,     "Min_Grade <= -15%",   valColor.at(1) },
            { TrackModel::MinGrade,     "Min_Grade <= -10%",   valColor.at(2) },
            { TrackModel::MinGrade,     "Min_Grade <= -5%",    valColor.at(3) },
            { TrackModel::MinGrade,     "Min_Grade <= -2%",    valColor.at(4) },

            { TrackModel::Energy,       "Energy >= 2000kcal",  valColor.at(0) },
            { TrackModel::Energy,       "Energy >= 1500kcal",  valColor.at(1) },
            { TrackModel::Energy,       "Energy >= 1000kcal",  valColor.at(2) },
            { TrackModel::Energy,       "Energy >= 500kcal",   valColor.at(3) },
            { TrackModel::Energy,       "Energy >= 250kcal",   valColor.at(4) },
        });
    }

    // Version 13 added HR Pct
    if (cfgDataVersion < 13) {
        trackColorizer.appendRows({
            { TrackModel::MinHRPct, "Min_Hr_% >= 90%",   valColor.at(0) },
            { TrackModel::MinHRPct, "Min_Hr_% >= 80%",   valColor.at(1) },
            { TrackModel::MinHRPct, "Min_Hr_% >= 70%",   valColor.at(2) },
            { TrackModel::MinHRPct, "Min_Hr_% >= 60%",   valColor.at(3) },
            { TrackModel::MinHRPct, "Min_Hr_% >= 50%",   valColor.at(4) },

            { TrackModel::AvgHRPct, "Avg_Hr_% >= 90%",   valColor.at(0) },
            { TrackModel::AvgHRPct, "Avg_Hr_% >= 80%",   valColor.at(1) },
            { TrackModel::AvgHRPct, "Avg_Hr_% >= 70%",   valColor.at(2) },
            { TrackModel::AvgHRPct, "Avg_Hr_% >= 60%",   valColor.at(3) },
            { TrackModel::AvgHRPct, "Avg_Hr_% >= 50%",   valColor.at(4) },

            { TrackModel::MaxHRPct, "Max_Hr_% >= 90%",   valColor.at(0) },
            { TrackModel::MaxHRPct, "Max_Hr_% >= 80%",   valColor.at(1) },
            { TrackModel::MaxHRPct, "Max_Hr_% >= 70%",   valColor.at(2) },
            { TrackModel::MaxHRPct, "Max_Hr_% >= 60%",   valColor.at(3) },
            { TrackModel::MaxHRPct, "Max_Hr_% >= 50%",   valColor.at(4) },
        });
    }

    // Version 18 added Avg Grade, Avg Elevation.  Also Cadence colorization
    if (cfgDataVersion < 18) {
        trackColorizer.appendRows({
            { TrackModel::AvgElevation, "Avg_Elev >= 5000m",   valColor.at(0) },
            { TrackModel::AvgElevation, "Avg_Elev >= 4000m",   valColor.at(1) },
            { TrackModel::AvgElevation, "Avg_Elev >= 3000m",   valColor.at(2) },
            { TrackModel::AvgElevation, "Avg_Elev >= 2000m",   valColor.at(3) },
            { TrackModel::AvgElevation, "Avg_Elev >= 1000m",   valColor.at(4) },

            { TrackModel::AvgGrade,     "Avg_Grade >= 20%",    valColor.at(0) },
            { TrackModel::AvgGrade,     "Avg_Grade >= 15%",    valColor.at(1) },
            { TrackModel::AvgGrade,     "Avg_Grade >= 10%",    valColor.at(2) },
            { TrackModel::AvgGrade,     "Avg_Grade >= 5%",     valColor.at(3) },
            { TrackModel::AvgGrade,     "Avg_Grade >= 2%",     valColor.at(4) },
        });
    }

    updateColors(trackColorizer);
}

void CfgData::updatePointColorizers()
{
    const auto valColor = valColorSet();

    if (cfgDataVersion < 1) {
        pointColorizer.appendRows({
            { PointModel::Ele,   "Elevation >= 5000m",   valColor.at(0) },
            { PointModel::Ele,   "Elevation >= 4000m",   valColor.at(1) },
            { PointModel::Ele,   "Elevation >= 3000m",   valColor.at(2) },
            { PointModel::Ele,   "Elevation >= 2000m",   valColor.at(3) },
            { PointModel::Ele,   "Elevation >= 1000m",   valColor.at(4) },

            { PointModel::Grade, "Grade >= 20%",         valColor.at(0) },
            { PointModel::Grade, "Grade >= 10%",         valColor.at(1) },
            { PointModel::Grade, "Grade >= 2%",          valColor.at(2) },
            { PointModel::Grade, "Grade <= -2%",         valColor.at(3) },
            { PointModel::Grade, "Grade <= -10%",        valColor.at(4) },
            { PointModel::Grade, "Grade <= -20%",        valColor.at(5) },

            { PointModel::Power, "Power >= 500w",        valColor.at(0) },
            { PointModel::Power, "Power >= 250w",        valColor.at(1) },
            { PointModel::Power, "Power >= 150w",        valColor.at(2) },
            { PointModel::Power, "Power >= 100w",        valColor.at(3) },
            { PointModel::Power, "Power >= 50w",         valColor.at(4) },
        });
    }

    if (cfgDataVersion < 19) {
        pointColorizer.appendRows({
            { PointModel::Cad,   "Cadence >= 120",               valColor.at(0) },
            { PointModel::Cad,   "Cadence >= 110",               valColor.at(1) },
            { PointModel::Cad,   "Cadence >= 100",               valColor.at(2) },
            { PointModel::Cad,   "Cadence >= 90",                valColor.at(3) },
            { PointModel::Cad,   "Cadence >= 80",                valColor.at(4) },
            { PointModel::Cad,   "Cadence >= 10 & Cadence < 80 & Power >= 100w", valColor.at(2) },
            { PointModel::Cad,   "Cadence >= 10 & Cadence < 70 & Power >= 100w", valColor.at(1) },
            { PointModel::Cad,   "Cadence >= 10 & Cadence < 60 & Power >= 100w", valColor.at(0) },
        });
    }

    if (cfgDataVersion < 26) {
        // Acceleration was removed in version 26
        pointColorizer.removeRows([](const QModelIndex& idx) {
            return idx.data(Util::RawDataRole).toUInt() == PointModel::AccelRemoved;
        }, QModelIndex(), ColorizerModel::Column);

        // Update indexes for everything after the old Accel field.
        Util::Recurse(pointColorizer, [this](const QModelIndex& idx) {
            const ModelType column = idx.data(Util::RawDataRole).toInt();
            if (column > PointModel::AccelRemoved)
                pointColorizer.setData(idx, column - 1, Util::RawDataRole);
            return true;
        }, QModelIndex(), false, ColorizerModel::Column);
    }

    updateColors(pointColorizer);
}

void CfgData::updateClimbColorizers()
{
    const auto valColor = valColorSet();

    if (cfgDataVersion < 20) {
        climbColorizer.appendRows({
            { ClimbModel::Vertical,     "Vertical >= 1200m",  valColor.at(0) },
            { ClimbModel::Vertical,     "Vertical >= 700m",   valColor.at(1) },
            { ClimbModel::Vertical,     "Vertical >= 400m",   valColor.at(2) },
            { ClimbModel::Vertical,     "Vertical >= 200m",   valColor.at(3) },
            { ClimbModel::Vertical,     "Vertical >= 100m",   valColor.at(4) },
            { ClimbModel::Vertical,     "Vertical <= -1200m", valColor.at(0) },
            { ClimbModel::Vertical,     "Vertical <= -700m",  valColor.at(1) },
            { ClimbModel::Vertical,     "Vertical <= -400m",  valColor.at(2) },
            { ClimbModel::Vertical,     "Vertical <= -200m",  valColor.at(3) },
            { ClimbModel::Vertical,     "Vertical <= -100m",  valColor.at(4) },

            { ClimbModel::Type,         "Vertical >= 1200m", valColor.at(0), QVariant(),
              ":art/tags/Misc/Hill/Up-Red10Blk00.svg", Qt::Unchecked, Qt::Checked },
            { ClimbModel::Type,         "Vertical >= 700m",  valColor.at(1), QVariant(),
              ":art/tags/Misc/Hill/Up-Ora10Blk00.svg", Qt::Unchecked, Qt::Checked },
            { ClimbModel::Type,         "Vertical >= 400m",  valColor.at(2), QVariant(),
              ":art/tags/Misc/Hill/Up-Yel10Blk00.svg", Qt::Unchecked, Qt::Checked },
            { ClimbModel::Type,         "Vertical >= 200m",  valColor.at(3), QVariant(),
              ":art/tags/Misc/Hill/Up-Cya10Blk00.svg", Qt::Unchecked, Qt::Checked },
            { ClimbModel::Type,         "Vertical >= 100m",  valColor.at(4), QVariant(),
              ":art/tags/Misc/Hill/Up-Grn10Blk00.svg", Qt::Unchecked, Qt::Checked },
            { ClimbModel::Type,         "Vertical >= 0m",  QVariant(), QVariant(),
              ":art/tags/Misc/Hill/Up-Gry08Blk00.svg", Qt::Unchecked, Qt::Checked },

            { ClimbModel::Type,         "Vertical <= -1200m", valColor.at(0), QVariant(),
              ":art/tags/Misc/Hill/Dn-Red10Blk00.svg", Qt::Unchecked, Qt::Checked },
            { ClimbModel::Type,         "Vertical <= -700m",  valColor.at(1), QVariant(),
              ":art/tags/Misc/Hill/Dn-Ora10Blk00.svg", Qt::Unchecked, Qt::Checked },
            { ClimbModel::Type,         "Vertical <= -400m",  valColor.at(2), QVariant(),
              ":art/tags/Misc/Hill/Dn-Yel10Blk00.svg", Qt::Unchecked, Qt::Checked },
            { ClimbModel::Type,         "Vertical <= -200m",  valColor.at(3), QVariant(),
              ":art/tags/Misc/Hill/Dn-Cya10Blk00.svg", Qt::Unchecked, Qt::Checked },
            { ClimbModel::Type,         "Vertical <= -100m",  valColor.at(4), QVariant(),
              ":art/tags/Misc/Hill/Dn-Grn10Blk00.svg", Qt::Unchecked, Qt::Checked },
            { ClimbModel::Type,         "Vertical < 0m",  QVariant(), QVariant(),
              ":art/tags/Misc/Hill/Dn-Gry08Blk00.svg", Qt::Unchecked, Qt::Checked },

            { ClimbModel::Steep,     "Steep >= 20%",    valColor.at(0) },
            { ClimbModel::Steep,     "Steep >= 15%",    valColor.at(1) },
            { ClimbModel::Steep,     "Steep >= 10%",    valColor.at(2) },
            { ClimbModel::Steep,     "Steep >= 5%",     valColor.at(3) },
            { ClimbModel::Steep,     "Steep >= 2%",     valColor.at(4) },
            { ClimbModel::Steep,     "Steep <= -20%",   valColor.at(0) },
            { ClimbModel::Steep,     "Steep <= -15%",   valColor.at(1) },
            { ClimbModel::Steep,     "Steep <= -10%",   valColor.at(2) },
            { ClimbModel::Steep,     "Steep <= -5%",    valColor.at(3) },
            { ClimbModel::Steep,     "Steep <= -2%",    valColor.at(4) },
        });
    }

    updateColors(climbColorizer);
}

void CfgData::updateTrkPtColorModel()
{
    // Acceleration removed in version 26
    if (cfgDataVersion < 26) {
        trkPtColor.removeEntry(PointModel::AccelRemoved);
    }
}

void CfgData::reset()
{
    *this = CfgData(true);
}

void CfgData::postLoadHook()
{
    // We can't do this during the normal load phase, because we are loaded before
    // the models, and recentTags() needs model data. We supply defaults for the
    // autoImportTags based on recent track tags, if available.
    if (priorCfgDataVersion < 25) {
        if (autoImportTags.isEmpty())
            autoImportTags = app().trackModel().recentTags(1, 25);

        // If none were found, just wing it.
        if (autoImportTags.isEmpty())
            autoImportTags.append("Road");
    }
}

void CfgData::insertColumnOnLoad(uint32_t ver, ModelType column)
{
    if (cfgDataVersion >= ver)
        return;

    Util::Recurse(trackColorizer, [this, column](const QModelIndex& idx) {
       if (const ModelType mt = trackColorizer.data(ColorizerModel::Column, idx, Util::RawDataRole).toInt(); mt >= column)
           trackColorizer.setData(ColorizerModel::Column, idx, mt+1, Util::RawDataRole);
       return true;
    });

    Util::Recurse(climbColorizer, [this, column](const QModelIndex& idx) {
       if (const ModelType mt = climbColorizer.data(ColorizerModel::Column, idx, Util::RawDataRole).toInt(); mt >= column)
           climbColorizer.setData(ColorizerModel::Column, idx, mt+1, Util::RawDataRole);
       return true;
    });
}

// If the CfgData loaded was old, we might want to update some values.
void CfgData::updateFormat()
{
    priorCfgDataVersion = cfgDataVersion; // save for other classes to look at on load

    if (cfgDataVersion == currentCfgDataVersion)  // up to date
        return;

    const auto find = [this](const char* name) {
        return tags.findRow(QModelIndex(), name, TagModel::Name, Util::RawDataRole);
    };

    // To version 1: add new icons, tweak some values.
    if (cfgDataVersion < 1) {
        if (const QModelIndex idx = find("Run"); idx.isValid()) {
            tags.setData(TagModel::RR, idx, 0.063, Util::RawDataRole);
            tags.setData(TagModel::Weight, idx, 1.0, Util::RawDataRole);
        }

        if (const QModelIndex idx = find("Hike"); idx.isValid()) {
            tags.setData(TagModel::RR, idx, 0.045, Util::RawDataRole);
            tags.setData(TagModel::Weight, idx, 1.0, Util::RawDataRole);
        }
    }

    // To version 7: new Source column: renumber others.
    insertColumnOnLoad(7, TrackModel::Source);

    // To version 18: new AvgElevation / AvgGrade column: renumber others.
    insertColumnOnLoad(18, TrackModel::AvgElevation);
    insertColumnOnLoad(18, TrackModel::AvgGrade);

    updateTags();
    updateTrackColorizers();
    updatePointColorizers();
    updateClimbColorizers();
    updateTrkPtColorModel();
    updatePointIcons();
    updateZones();

    cfgDataVersion = currentCfgDataVersion;
}

void CfgData::updatePointIcons()
{
    if (cfgDataVersion < pointPngToSvgVersion) {
        // Point icons converted from pngs to svgs.  Some other icons did not (e.g, track note icon).
        for (QString* pointIcon : {
             &defaultPointIcon, &selectedPointIcon, &currentPointIcon, &gpsdLivePointIcon, &waypointDefaultIcon })
            if (const auto [newPath, changed] = pngToSvg(*pointIcon); changed)
                *pointIcon = newPath;
    }
}

void CfgData::updateColors(ColorizerModel& colorizer)
{
    // In original defaults, blue and green were swapped WRT their meaning for ski slopes,
    // etc.  This changes old saves, provided the colors have not been altered.
    if (cfgDataVersion < 20) {
        const auto valColor = valColorSet();

        Util::Recurse(colorizer, [&valColor, &colorizer](const QModelIndex& idx) {
            for (const ModelType mt : { ColorizerModel::FgColor, ColorizerModel::BgColor }) {
                const QModelIndex colIdx = idx.sibling(idx.row(), mt);
                if (const QVariant color = colorizer.data(colIdx, Qt::BackgroundRole); color.isValid()) {
                    if (color.value<QColor>() == valColor.at(3))
                        colorizer.setData(colIdx, valColor.at(4), Qt::EditRole);
                    else if (color.value<QColor>() == valColor.at(4))
                        colorizer.setData(colIdx, valColor.at(3), Qt::EditRole);
                }
            }
            return true;
        });
    }
}

void CfgData::save(QSettings& settings) const
{
    SL::Save(settings, versionKey, version());

    CfgDataBase::save(settings);

    MemberSave(settings, tags);
    MemberSave(settings, people);
    MemberSave(settings, zones);
    MemberSave(settings, trackColorizer);
    MemberSave(settings, pointColorizer);
    MemberSave(settings, climbColorizer);
    MemberSave(settings, trkPtColor);

    MemberSave(settings, trkPtLineWidth);
    MemberSave(settings, trkPtMarkerWidth);
    MemberSave(settings, trkPtRangeWidth);
    MemberSave(settings, trkPtMarkerColor);
    MemberSave(settings, trkPtRangeColor);
    MemberSave(settings, asMaxDateSpans);
    MemberSave(settings, asBarWidth);
    MemberSave(settings, hillMinGrade);
    MemberSave(settings, hillMinHeight);
    MemberSave(settings, hillGradeLength);

    MemberSave(settings, eleFilterSize);
    MemberSave(settings, tooltipInlineLimit);
    MemberSave(settings, tooltipFullLimit);

    MemberSave(settings, unassignedTrackColor);
    MemberSave(settings, outlineTrackColor);
    MemberSave(settings, defaultTrackWidthC);
    MemberSave(settings, defaultTrackWidthF);
    MemberSave(settings, defaultTrackWidthO);
    MemberSave(settings, currentTrackWidthC);
    MemberSave(settings, currentTrackWidthF);
    MemberSave(settings, currentTrackWidthO);
    MemberSave(settings, defaultTrackAlphaC);
    MemberSave(settings, defaultTrackAlphaF);
    MemberSave(settings, currentTrackAlphaC);
    MemberSave(settings, currentTrackAlphaF);

    MemberSave(settings, mapMoveMode);
    MemberSave(settings, mapMovePoints);
    MemberSave(settings, mapMoveWaypoints);
    MemberSave(settings, mapInertialMovement);

    MemberSave(settings, defaultPointIcon);
    MemberSave(settings, defaultPointIconSize);
    MemberSave(settings, defaultPointIconProx);
    MemberSave(settings, selectedPointIcon);
    MemberSave(settings, selectedPointIconSize);
    MemberSave(settings, currentPointIcon);
    MemberSave(settings, currentPointIconSize);
    MemberSave(settings, gpsdLivePointIcon);
    MemberSave(settings, gpsdLivePointIconSize);
    MemberSave(settings, waypointDefaultIcon);
    MemberSave(settings, waypointDefaultIconSize);
    MemberSave(settings, waypointIconSize);

    MemberSave(settings, trackNoteIcon);
    MemberSave(settings, colorizeTagIcons);
    MemberSave(settings, iconSizeTrack);
    MemberSave(settings, flagSizeTrack);
    MemberSave(settings, iconSizeView);
    MemberSave(settings, iconSizeTag);
    MemberSave(settings, iconSizeFilter);
    MemberSave(settings, iconSizeClimb);
    MemberSave(settings, maxTrackPaneIcons);
    MemberSave(settings, maxTrackPaneFlags);

    MemberSave(settings, autoImportMode);
    MemberSave(settings, autoImportDir);
    MemberSave(settings, autoImportPattern);
    MemberSave(settings, autoImportTags);
    MemberSave(settings, autoImportPost);
    MemberSave(settings, autoImportBackupSuffix);
    MemberSave(settings, autoImportBackupDir);
    MemberSave(settings, autoImportCommand);
    MemberSave(settings, autoImportStdout);
    MemberSave(settings, autoImportTimeout);

    MemberSave(settings, unitsTrkLength);
    MemberSave(settings, unitsLegLength);
    MemberSave(settings, unitsDuration);
    MemberSave(settings, unitsTrkDate);
    MemberSave(settings, unitsTrkTime);
    MemberSave(settings, unitsPointDate);
    MemberSave(settings, unitsTz);
    MemberSave(settings, unitsElevation);
    MemberSave(settings, unitsLat);
    MemberSave(settings, unitsLon);
    MemberSave(settings, unitsSpeed);
    MemberSave(settings, unitsClimb);
    MemberSave(settings, unitsArea);
    MemberSave(settings, unitsTemp);
    MemberSave(settings, unitsSlope);
    MemberSave(settings, unitsPower);
    MemberSave(settings, unitsEnergy);
    MemberSave(settings, unitsWeight);
    MemberSave(settings, unitsPct);
    MemberSave(settings, unitsCad);
    MemberSave(settings, unitsHr);

    MemberSave(settings, mapUndoStill);
    MemberSave(settings, maxUndoCountView);
}

void CfgData::load(QSettings& settings)
{
    reset(); // defaults for things not read

    // Don't load incompatible versions
    if (SL::Load(settings, versionKey, 0) != Settings::version())
        return;

    CfgDataBase::load(settings);

    MemberLoad(settings, tags);
    MemberLoad(settings, people);
    MemberLoad(settings, zones);
    MemberLoad(settings, trackColorizer);
    MemberLoad(settings, pointColorizer);
    MemberLoad(settings, climbColorizer);
    MemberLoad(settings, trkPtColor);

    MemberLoad(settings, trkPtLineWidth);
    MemberLoad(settings, trkPtMarkerWidth);
    MemberLoad(settings, trkPtRangeWidth);
    MemberLoad(settings, trkPtMarkerColor);
    MemberLoad(settings, trkPtRangeColor);
    MemberLoad(settings, asMaxDateSpans);
    MemberLoad(settings, asBarWidth);
    MemberLoad(settings, hillMinGrade);
    MemberLoad(settings, hillMinHeight);
    MemberLoad(settings, hillGradeLength);

    MemberLoad(settings, eleFilterSize);
    MemberLoad(settings, tooltipInlineLimit);
    MemberLoad(settings, tooltipFullLimit);

    MemberLoad(settings, unassignedTrackColor);
    MemberLoad(settings, outlineTrackColor);
    MemberLoad(settings, defaultTrackWidthC);
    MemberLoad(settings, defaultTrackWidthF);
    MemberLoad(settings, defaultTrackWidthO);
    MemberLoad(settings, currentTrackWidthC);
    MemberLoad(settings, currentTrackWidthF);
    MemberLoad(settings, currentTrackWidthO);
    MemberLoad(settings, defaultTrackAlphaC);
    MemberLoad(settings, defaultTrackAlphaF);
    MemberLoad(settings, currentTrackAlphaC);
    MemberLoad(settings, currentTrackAlphaF);

    MemberLoad(settings, mapMoveMode);
    MemberLoad(settings, mapMovePoints);
    MemberLoad(settings, mapMoveWaypoints);
    MemberLoad(settings, mapInertialMovement);

    MemberLoad(settings, defaultPointIcon);
    MemberLoad(settings, defaultPointIconSize);
    MemberLoad(settings, defaultPointIconProx);
    MemberLoad(settings, selectedPointIcon);
    MemberLoad(settings, selectedPointIconSize);
    MemberLoad(settings, currentPointIcon);
    MemberLoad(settings, currentPointIconSize);
    MemberLoad(settings, gpsdLivePointIcon);
    MemberLoad(settings, gpsdLivePointIconSize);
    MemberLoad(settings, waypointDefaultIcon);
    MemberLoad(settings, waypointDefaultIconSize);
    MemberLoad(settings, waypointIconSize);

    MemberLoad(settings, trackNoteIcon);
    MemberLoad(settings, colorizeTagIcons);
    MemberLoad(settings, iconSizeTrack);
    MemberLoad(settings, flagSizeTrack);
    MemberLoad(settings, iconSizeView);
    MemberLoad(settings, iconSizeTag);
    MemberLoad(settings, iconSizeFilter);
    MemberLoad(settings, iconSizeClimb);
    MemberLoad(settings, maxTrackPaneIcons);
    MemberLoad(settings, maxTrackPaneFlags);

    MemberLoad(settings, autoImportMode);
    MemberLoad(settings, autoImportDir);
    MemberLoad(settings, autoImportPattern);
    MemberLoad(settings, autoImportTags);
    MemberLoad(settings, autoImportPost);
    MemberLoad(settings, autoImportBackupSuffix);
    MemberLoad(settings, autoImportBackupDir);
    MemberLoad(settings, autoImportCommand);
    MemberLoad(settings, autoImportStdout);
    MemberLoad(settings, autoImportTimeout);

    MemberLoad(settings, unitsTrkLength);
    MemberLoad(settings, unitsLegLength);
    MemberLoad(settings, unitsDuration);
    MemberLoad(settings, unitsTrkDate);
    MemberLoad(settings, unitsTrkTime);
    MemberLoad(settings, unitsPointDate);
    MemberLoad(settings, unitsTz);
    MemberLoad(settings, unitsElevation);
    MemberLoad(settings, unitsLat);
    MemberLoad(settings, unitsLon);
    MemberLoad(settings, unitsSpeed);
    MemberLoad(settings, unitsClimb);
    MemberLoad(settings, unitsArea);
    MemberLoad(settings, unitsTemp);
    MemberLoad(settings, unitsSlope);
    MemberLoad(settings, unitsPower);
    MemberLoad(settings, unitsEnergy);
    MemberLoad(settings, unitsWeight);
    MemberLoad(settings, unitsPct);
    MemberLoad(settings, unitsCad);
    MemberLoad(settings, unitsHr);

    MemberLoad(settings, mapUndoStill);
    MemberLoad(settings, maxUndoCountView);

    updateFormat();
}

QDataStream& operator<<(QDataStream& out, const CfgData::MapMoveMode& mm)
{
    return out << std::underlying_type_t<CfgData::MapMoveMode>(mm);
}

QDataStream& operator>>(QDataStream& in, CfgData::MapMoveMode& mm)
{
    return in >> reinterpret_cast<std::underlying_type_t<CfgData::MapMoveMode>&>(mm);
}

QDataStream& operator<<(QDataStream& out, const CfgData::AutoImportMode& mm)
{
    return out << std::underlying_type_t<CfgData::AutoImportMode>(mm);
}

QDataStream& operator>>(QDataStream& in, CfgData::AutoImportMode& mm)
{
    return in >> reinterpret_cast<std::underlying_type_t<CfgData::AutoImportMode>&>(mm);
}

QDataStream& operator<<(QDataStream& out, const CfgData::AutoImportPost& mm)
{
    return out << std::underlying_type_t<CfgData::AutoImportPost>(mm);
}

QDataStream& operator>>(QDataStream& in, CfgData::AutoImportPost& mm)
{
    return in >> reinterpret_cast<std::underlying_type_t<CfgData::AutoImportPost>&>(mm);
}
