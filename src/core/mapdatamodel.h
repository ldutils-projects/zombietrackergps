/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAPDATAMODEL_H
#define MAPDATAMODEL_H

#include <tuple>
#include <QModelIndexList>
#include <QMultiHash>
#include <src/core/modelmetadata.h>

namespace Marble {
class GeoDataLatLonBox;
} // namespace Marble

enum TrackType : uint8_t {
    _First,
    Trk = _First,  // tracks
    Rte,           // routes
    Wpt,           // waypoints
    Unk,           // unknown
    _Count = Unk,
};

Q_DECLARE_METATYPE(TrackType)
QDataStream& operator<<(QDataStream&, const TrackType&);
QDataStream& operator>>(QDataStream&, TrackType&);

// Common base for models (Track, Waypoint, etc) which handle map visible data.
class MapDataModel
{
public:
    MapDataModel();

    [[nodiscard]] virtual bool isDuplicate(const QModelIndex& lhs, const QModelIndex& rhs) const = 0;
    [[nodiscard]] virtual QMultiHash<uint, QModelIndex> hashes() const = 0; // return set of hashes for items (unordered!)

    [[nodiscard]] virtual uint hash(const QModelIndex& idx) const = 0; // hash for a single item

    virtual void setAllVisible(bool) = 0;
    virtual void setVisible(const QModelIndex&, bool) = 0;
    [[nodiscard]] virtual bool isVisible(const QModelIndex&) const = 0;

    [[nodiscard]] virtual std::tuple<qreal, qreal, qreal, qreal, bool> bounds(const QModelIndexList&) const = 0;
    [[nodiscard]] virtual Marble::GeoDataLatLonBox boundsBox(const QModelIndexList&) const = 0;
    [[nodiscard]] virtual Marble::GeoDataLatLonBox boundsBox(const QVector<QPersistentModelIndex>&) const;

    [[nodiscard]] virtual bool isEditable(ModelType) const = 0;

    [[nodiscard]] static QString trackTypeName(TrackType);
    [[nodiscard]] static TrackType trackNameType(const QString&);
};

#endif // MAPDATAMODEL_H
