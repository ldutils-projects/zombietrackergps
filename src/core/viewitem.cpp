/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>

#include <src/util/roles.h>
#include <src/util/units.h>

#include "viewitem.h"
#include "viewmodel.h"

ViewItem::ViewItem(TreeItem* parent) :
    TreeItem(parent)
{
}

ViewItem::ViewItem(const TreeItem::ItemData& data, TreeItem* parent) :
    TreeItem(data, parent, Util::RawDataRole)
{
}

QVariant ViewItem::data(int column, int role) const
{
    if (parent() != nullptr) {
        const QVariant rawData = TreeItem::data(column, Util::RawDataRole);

        switch (role) {
        case Qt::TextAlignmentRole:
            return { ViewModel::mdAlignment(column) };

        case Qt::EditRole:   [[fallthrough]];
        case Util::PlotRole:
            return rawData;

        case Qt::DisplayRole: [[fallthrough]];
        case Util::CopyRole:
            if (rawData.isValid())
                return ViewModel::mdUnits(column)(rawData);
            break;
        }
    }

    // We didn't handle anything specially for the copy: get the display value instead
    if (role == Util::CopyRole)
        role = Qt::DisplayRole;

    return TreeItem::data(column, role);
}

bool ViewItem::setData(int column, const QVariant& value, int role, bool &changed)
{
    if (role == Qt::EditRole)
        role = Util::RawDataRole;

    return TreeItem::setData(column, value, role, changed);
}

int ViewItem::columnCount() const
{
    return ViewModel::_Count;
}

bool ViewItem::saveRole(int role) const
{
    return TreeItem::saveRole(role) && role >= Qt::UserRole;
}

ViewItem* ViewItem::factory(const TreeItem::ItemData& data, TreeItem* parent)
{
    assert(dynamic_cast<ViewItem*>(parent) != nullptr);

    return new ViewItem(data, parent);
}
