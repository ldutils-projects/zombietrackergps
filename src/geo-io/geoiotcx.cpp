/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <limits>
#include <QDateTime>
#include <QModelIndexList>

#include <src/util/roles.h>
#include "src/core/trackmodel.h"
#include "src/core/pointmodel.h"
#include "geoiotcx.h"

const QString GeoLoadTcx::nameStatic = "TCX";

namespace TcxExt {
    static const char* xmlns   = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2";
    static const char* xsi     = "http://www.w3.org/2001/XMLSchema-instance";
    static const char* tp1     = "http://www.garmin.com/xmlschemas/TrackPointExtension/v1";
    static const char* gpx     = "http://www.topografix.com/GPX/1/1";
} // namespace TcxExt

namespace TcxTag {
    static const char* TrainingCenterDatabase  = "TrainingCenterDatabase";
    static const char* Activities              = "Activities";
    static const char* Activity                = "Activity";
    static const char* Id                      = "Id";
    static const char* Lap                     = "Lap";
    static const char* HeartRateBpm            = "HeartRateBpm";
    static const char* Cadence                 = "Cadence";
    static const char* Value                   = "Value";
    static const char* Sport                   = "Sport";
    static const char* StartTime               = "StartTime";
// TODO: ...
//    static const char* Calories                = "Calories";
//    static const char* TotalTimeSeconds        = "TotalTimeSeconds";
//    static const char* DistanceMeters          = "DistanceMeters";
//    static const char* Intensity               = "Intensity";
//    static const char* TriggerMethod           = "TriggerMethod";
    static const char* Track                   = "Track";
    static const char* Trackpoint              = "Trackpoint";
    static const char* Time                    = "Time";
    static const char* Position                = "Position";
    static const char* LatitudeDegrees         = "LatitudeDegrees";
    static const char* LongitudeDegrees        = "LongitudeDegrees";
    static const char* AltitudeMeters          = "AltitudeMeters";
} // namespace TcxTag

bool GeoLoadTcx::is(QIODevice& io) const
{
    return GeoLoadTcx(geoLoad).is(io, topTag());
}

void GeoLoadTcx::parseXml()
{
    foundTopTag();

    parseKeys([this]() {
        if      (xml.name() == TcxTag::Activities)   parseActivities();
        else xml.skipCurrentElement();
    });
}

void GeoLoadTcx::parseActivities()
{
    parseKeys([this]() {
        if      (xml.name() == TcxTag::Activity) parseActivity();
        else xml.skipCurrentElement();
    });
}

void GeoLoadTcx::parseActivity()
{
    track.clear();

    parseKeys([this]() {
        if      (xml.name() == TcxTag::Id)  track.name = xml.readElementText();
        else if (xml.name() == TcxTag::Lap) parseLap();
        else xml.skipCurrentElement();
    });

    // Create a new TrackItem for the track we just made.
    track.append(geoLoad);
}

void GeoLoadTcx::parseLap()
{
    ++track.laps;

    parseKeys([this]() {
        if   (xml.name() == TcxTag::Track) parseTrack();
        else xml.skipCurrentElement();
    });
}

void GeoLoadTcx::parseTrack()
{
    track.newSegment();

    reportRead(xml.characterOffset()); // for progress reports

    parseKeys([this]() {
        if   (xml.name() == TcxTag::Trackpoint) parseTrackpoint();
        else xml.skipCurrentElement();
    });
}

void GeoLoadTcx::parseTrackpoint()
{
    pt.clear();

    parseKeys([this]() {
        if      (xml.name() == TcxTag::Time)           pt.setTime(QDateTime::fromString(xml.readElementText(), Qt::ISODate));
        else if (xml.name() == TcxTag::Position)       parsePosition();
        else if (xml.name() == TcxTag::AltitudeMeters) pt.setEle(xml.readElementText().toDouble());
        else if (xml.name() == TcxTag::HeartRateBpm)   parseHeartRateBpm();
        else if (xml.name() == TcxTag::Cadence)        pt.setCad(xml.readElementText().toInt());
        else xml.skipCurrentElement();
    });

    track.append(pt, geoLoad().hasFeature(GeoIoFeature::AuxTrk));
}

void GeoLoadTcx::parseHeartRateBpm()
{
    parseKeys([this]() {
        if   (xml.name() == TcxTag::Value) pt.setHr(xml.readElementText().toInt());
        else xml.skipCurrentElement();
    });
}

void GeoLoadTcx::parsePosition()
{
    parseKeys([this]() {
        if      (xml.name() == TcxTag::LatitudeDegrees)  pt.setLat(xml.readElementText().toDouble());
        else if (xml.name() == TcxTag::LongitudeDegrees) pt.setLon(xml.readElementText().toDouble());
        else xml.skipCurrentElement();
    });
}

const char* GeoLoadTcx::topTag() const
{
    return TcxTag::TrainingCenterDatabase;
}

void GeoSaveTcx::savePosition(const PointItem& trkpt)
{
    xml.writeStartElement(TcxTag::Position); {
        xml.writeTextElement(TcxTag::LatitudeDegrees, QString::number(PointItem::Lat_t::base_type(trkpt.lat()), 'g', 16));
        xml.writeTextElement(TcxTag::LongitudeDegrees, QString::number(PointItem::Lon_t::base_type(trkpt.lon()), 'g', 16));
    } xml.writeEndElement();
}

void GeoSaveTcx::saveTrackpoint(const PointItem& trkpt)
{
    xml.writeStartElement(TcxTag::Trackpoint); {
        if (trkpt.hasTime())
            xml.writeTextElement(TcxTag::Time, trkpt.time().toString(Qt::ISODate));
        if (trkpt.hasLoc())
            savePosition(trkpt);
        if (trkpt.hasEle())
            xml.writeTextElement(TcxTag::AltitudeMeters, QString::number(PointItem::Ele_t::base_type(trkpt.ele(false)), 'g', 10));
    } xml.writeEndElement();
}

// In TCX files, we save each track *segment* in the track tag.
void GeoSaveTcx::saveTrack(const PointModel::value_type& trkseg)
{
    xml.writeStartElement(TcxTag::Track); {
        for (const auto& geoPt : trkseg)
            saveTrackpoint(geoPt);
    } xml.writeEndElement();
}

// <Lap StartTime="2017-05-06T16:31:32Z">
void GeoSaveTcx::saveLap(const PointModel::value_type& trkseg)
{
    xml.writeStartElement(TcxTag::Lap); {
        if (!trkseg.isEmpty() && trkseg.front().hasTime()) {
            const QDateTime st = trkseg.front().time();
            xml.writeAttribute(TcxTag::StartTime, st.toString(Qt::ISODate));
            xml.writeTextElement(TcxTag::Id, st.toString(Qt::ISODate));
        }

        saveTrack(trkseg);
    } xml.writeEndElement();
}

void GeoSaveTcx::saveActivity(const QModelIndex& idx)
{
    xml.writeStartElement(TcxTag::Activity); {
        xml.writeAttribute(TcxTag::Sport, "Other");

        for (const auto& trkseg : *geoSave.trkModel().geoPoints(idx))
            saveLap(trkseg);
    } xml.writeEndElement();
}

void GeoSaveTcx::saveActivities()
{
    xml.writeStartElement(TcxTag::Activities); {
        for (const auto& idx : geoSave().m_trkSelection)
            saveActivity(idx);

    } xml.writeEndElement();
}

void GeoSaveTcx::saveXml()
{
    xml.writeStartElement(TcxTag::TrainingCenterDatabase); {
        xml.writeAttribute("xmlns",     TcxExt::xmlns);
        xml.writeNamespace(TcxExt::xsi, "xsi");
        xml.writeNamespace(TcxExt::tp1, "tp1");
        xml.writeNamespace(TcxExt::gpx, "gpx");

        saveActivities();
    } xml.writeEndElement();
}

