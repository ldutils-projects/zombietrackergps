/*
    Copyright 2020 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOIOCONV_H
#define GEOIOCONV_H

#include <QString>
#include "src/core/trackmodel.h"
#include "src/core/waypointmodel.h"
#include "geoio.h"
#include "geoioparams.h"

class QIODevice;

// Geo conversions: A loader, a saver, and models for the data
class GeoIoConv final
{
public:
    explicit GeoIoConv(QIODevice& stdout, QIODevice& stdin, const GeoSaveParams&, const GeoLoadParams&);

    [[nodiscard]] bool convert(const QString& output, const QString& input);  // perform conversion
    [[nodiscard]] bool load(const QString& input);
    [[nodiscard]] bool save(const QString& output);
    void clear();

    [[nodiscard]] int count(GeoIoFeature) const; // statistics

private:
    TrackModel    m_trk;     // track data (must be constructed before saver/loader below)
    WaypointModel m_wpt;     // waypoint data (...)
    GeoSave       m_saver;   // saver
    GeoLoad       m_loader;  // loader
};

#endif // GEOIOCONV_H
