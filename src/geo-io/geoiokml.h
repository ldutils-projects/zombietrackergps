/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOIOKML_H
#define GEOIOKML_H

#include <QVector>
#include <QString>
#include <QDateTime>
#include <QColor>

#include "src/core/pointmodel.h"
#include "geoioxml.h"

class GeoLoadKml final : public GeoLoadXml
{
public:
    using GeoLoadXml::GeoLoadXml;
    using GeoLoadXml::is;

    bool is(QIODevice&) const override;
    GeoFormat format() const override { return GeoFormat::Kml; }
    bool isBinary() const override { return false; }
    const QString& name() const override { return nameStatic; }

    static const QString nameStatic;

private:
    void parseXml() override;
    void parseDocument();
    void parseFolder();
    void parsePlacemark();
    void parseTrack();
    inline void parseWhen();
    inline void parseCoord();
    void parseExtendedData();
    void parseSchemaData();
    void parseSimpleArrayData();

    const char* topTag() const override;

    struct Coord { qreal lon, lat, ele; };

    // Collect some info from parsing the track data
    struct {
        void clear() {
            name.clear();
            desc.clear();
            geoPoint.clear();
            timeArr.clear();
            coordArr.clear();
            cadArr.clear();
            hrArr.clear();
            powerArr.clear();
        }

        bool coherent() const {
            return (timeArr.size() == coordArr.size() &&
                    (cadArr.isEmpty() || timeArr.size() == cadArr.size()) &&
                    (hrArr.isEmpty() || timeArr.size() == hrArr.size()) &&
                    (powerArr.isEmpty() || timeArr.size() == powerArr.size()));
        }

        bool empty() const { return timeArr.size() == 0; }

        void addDataToGeoPoints() {
            for (int c = 0; c < timeArr.size(); ++c)
                geoPoint.back().append(PointItem(!coordArr.isEmpty() ? coordArr[c].lon : PointItem::NaN,
                                                 !coordArr.isEmpty() ? coordArr[c].lat : PointItem::NaN,
                                                 !timeArr.isEmpty()  ? timeArr[c]      : QDateTime(),
                                                 !coordArr.isEmpty() ? coordArr[c].ele : PointItem::badEle,
                                                 !hrArr.isEmpty()    ? hrArr[c]        : PointItem::badHr,
                                                 !cadArr.isEmpty()   ? cadArr[c]       : PointItem::badCad,
                                                 !powerArr.isEmpty() ? powerArr[c]     : PointItem::badPower));
        }

        QString                      name;
        QString                      desc;
        PointModel                   geoPoint;

        QVector<QDateTime>           timeArr;  // these come in in an inconvenient
        QVector<Coord>               coordArr; // parallel-array manner from the KML,
        QVector<PointItem::Cad_t>    cadArr;   // so we store them in intermediate form.
        QVector<PointItem::Hr_t>     hrArr;    // ...
        QVector<PointItem::Power_t>  powerArr; // ...
    } trackData;
};

class GeoSaveKml final : public GeoSaveXml
{
public:
    using GeoSaveXml::GeoSaveXml;

    bool hasExt(const QString& ext) const override { return ext.toLower() == "kml"; }
    GeoFormat format() const override { return GeoFormat::Kml; }

private:
    void saveXml() override;
    void saveDocument();
    void saveFolder();
    void savePlacemark(const QModelIndex&);
    void saveTrk(const QModelIndex&);
    void saveWpt(const QModelIndex&);
    void saveRte(const QModelIndex&);
    void saveExtendedData(const QModelIndex&);
    void saveSchema();
};

#endif // GEOIOKML_H
