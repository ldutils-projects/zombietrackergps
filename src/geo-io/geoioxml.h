/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOIOXML_H
#define GEOIOXML_H

#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include "geoiobase.h"

// Subclass of GeoLoadBase for reading XML based formats.
class GeoLoadXml : public GeoLoadBase
{
public:
    bool load(QIODevice&) override;

protected:
    using GeoLoadBase::GeoLoadBase;

    bool done() const { return xml.atEnd() || xml.hasError(); }

    [[nodiscard]] static bool isStart(const QXmlStreamReader::TokenType& token) {
        return token == QXmlStreamReader::StartElement;
    }

    [[nodiscard]] static bool isEnd(const QXmlStreamReader::TokenType& token) {
        return token == QXmlStreamReader::EndElement;
    }

    inline void parseKeys(const std::function<void()>&);

    using GeoLoadBase::is;
    [[nodiscard]] bool is(QIODevice&, const char* startTag);

    [[nodiscard]] bool openReader(QIODevice&);
    void foundTopTag() { m_foundTopTag = true; }
    virtual void parseXml() = 0;
    [[nodiscard]] virtual const char* topTag() const = 0;

    [[nodiscard]] QIODevice::OpenMode openMode() const override { return QIODevice::ReadOnly | QIODevice::Text; }

    // Conversions from color names used by Garmin's TrackColor extension
    [[nodiscard]] QColor  nameToColor(const QString&) const;

    QXmlStreamReader xml;

private:
    bool m_foundTopTag;
};

// Helper to parse a set of XML tokens.  Inline for performance.
inline void GeoLoadXml::parseKeys(const std::function<void()>& fn)
{
    const QStringRef& end = xml.name();

    while (!done()) {
        if (const auto token = xml.readNext(); isStart(token)) {
            fn();
        } else if (isEnd(token) && xml.name() == end) {
            break;
        }
    }
}

// Subclass of GeoSaveBase for writing XML based formats.
class GeoSaveXml : public GeoSaveBase
{
public:
    bool save(QIODevice&) override;

protected:
    using GeoSaveBase::GeoSaveBase;

    [[nodiscard]] bool openWriter(QIODevice&);
    virtual void saveXml() = 0;

    [[nodiscard]] QIODevice::OpenMode openMode() const override { return QIODevice::WriteOnly | QIODevice::Text; }

    // Conversions to color names used by Garmin's TrackColor extension
    [[nodiscard]] QString colorToName(const QColor&) const;

    QXmlStreamWriter xml;
};

#endif // GEOIOXML_H
