/*
    Copyright 2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOIOBASE_INL_H
#define GEOIOBASE_INL_H

#include "src/core/trackmodel.h"
#include "src/core/waypointmodel.h"
#include "geoiobase.h"

template <typename T> inline T GeoSaveBase::trkGet(ModelType mt, const QModelIndex& idx) const
{
    return geoSave.trkModel().data(mt, idx, Util::RawDataRole).value<T>();
}

template <typename T> inline T GeoSaveBase::wptGet(ModelType mt, const QModelIndex& idx) const
{
    return geoSave.wptModel().data(mt, idx, Util::RawDataRole).value<T>();
}

#endif // GEOIOBASE_INL_H
