/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOIOFIT_H
#define GEOIOFIT_H

#include <cstdint>
#include <cassert>
#include <array>

#include <QtGlobal>
#include <QString>
#include <QVector>
#include <QDateTime>

#include "geoiobase.h"

// Data used by both the FIT loader and the saver
class GeoCommonFit
{
protected:
    enum class Sport       : uint8_t;  // sport values (e.g, cycling, running)
    enum class SubSport    : uint8_t;  // sub-sport values (e.g, road, mountain)
    enum class TypeNum     : uint8_t;  // Fit raw data types
    enum class RecordField : uint8_t;  // Record fields of interest to us.
    enum class SportField  : uint8_t;  // Sport fields of interest to us.
    enum class LapField    : uint8_t;  // Lap fields of interest to us.

    struct FileHeader {
        FileHeader() { }

        uint8_t   size            = 0;         // header size
        uint8_t   version         = 0;         // FIT version
        uint16_t  profile         = 0;         // profile version
        uint32_t  dataSize        = 0;         // data size
        std::array<char, 4> magic = {0,0,0,0}; // magic
        uint16_t  crc             = 0;         // crc

        static const constexpr uint8_t minHeaderLength = 12;
    };

    struct RecordHeader {
        RecordHeader(int localId = 0, int defMessage = 0) :
            localId(localId), reserved(0), devFlag(0), defMessage(defMessage), cmpTimestamp(0) { }

        unsigned char localId      : 4;
        unsigned char reserved     : 1;
        unsigned char devFlag      : 1;
        unsigned char defMessage   : 1;
        unsigned char cmpTimestamp : 1;
    };

    // Compressed time header
    struct CmpTimeHeader {
        CmpTimeHeader() { *reinterpret_cast<char*>(this) = 0; }
        CmpTimeHeader(const RecordHeader& rh) { *(uint8_t*)(this) = *(uint8_t*)(&rh); }
        unsigned char timeOffset   : 5;
        unsigned char localId      : 2;
        unsigned char cmpTimestamp : 1;
    };

    struct FieldType {
        FieldType() { *reinterpret_cast<char*>(this) = 0; }
        FieldType(TypeNum typeNum, bool hasEndian) :
            typeNum(uint8_t(typeNum)), reserved(0), hasEndian(hasEndian ? 1u : 0u)
        { }

        uint8_t typeNum    : 5;  // enum TypeNum above defines this namespace
        uint8_t reserved   : 2;
        uint8_t hasEndian  : 1;  // 0 = no endianness, 1 if has endianness
    };

    struct Field {
        Field() { }

        // Union holds different views to the field identifier
        union {
            RecordField record;
            SportField  sport;
            LapField    lap;
            uint8_t     rawId = 0;
        };

        uint8_t      size = 0;  // field data size (it can be arrayed)
        FieldType    type;  // field base type info
    };

    // FIT architecture type
    enum class Architecture : uint8_t {
        Little = 0,
        Big,
    };

#if Q_BYTE_ORDER == Q_LITTLE_ENDIAN
    static const constexpr Architecture currentArchitecture = Architecture::Little;
#else
    static const constexpr Architecture currentArchitecture = Architecture::Big;
#endif

    // Message data record types of interest to us.
    enum class Message : uint16_t {
        UserProfile = 3,
        BikeProfile = 6,
        Sport       = 12,
        Session     = 18,
        Lap         = 19,
        Record      = 20,
        Event       = 21,
        Invalid     = 0xffff,
    };

    // Header for a definition: this matches the disk format.
    struct DefinitionHeader {
        DefinitionHeader(Message globalId = Message::Invalid,
                         Architecture architecture = currentArchitecture) :
            architecture(architecture), globalId(globalId) { }

        void clear() { *this = DefinitionHeader(); }

        uint8_t        reserved = 0;
        Architecture   architecture;  // 0 = little endian, 1 = big endian
        union {
            Message    globalId;      // global message type: see Message enum
            uint16_t   globalIdRaw;   // for raw read
        };
    };

    // A record definition: does not match disk format (fields are individually loaded into vectors)
    struct Definition : public DefinitionHeader {
        Definition() { clear(); }

        void clear() {
            fields.clear();
            devFields.clear();
        }

        QVector<Field> fields;        // vector of fields
        QVector<Field> devFields;     // ...
    };

    static const QDateTime epochDate;

    static_assert(sizeof(RecordHeader) == 1);
    static_assert(sizeof(CmpTimeHeader) == 1);
    static_assert(sizeof(Architecture) == 1);
    static_assert(sizeof(TypeNum) == 1);
    static_assert(sizeof(FieldType) == 1);
    static_assert(sizeof(Field) == 3);
    static_assert(sizeof(DefinitionHeader) == 4);

    template <typename T> inline T invalid() const;
};

class GeoLoadFit final : public GeoLoadBase, public GeoCommonFit
{
public:
    using GeoLoadBase::GeoLoadBase;

    bool load(QIODevice&) override;
    bool is(QIODevice&) const override;
    GeoFormat format() const override { return GeoFormat::Fit; }
    bool isBinary() const override { return true; }
    const QString& name() const override { return nameStatic; }

    static const QString nameStatic; // format name

    // Should really be private, but having enum defs in the .cpp which use this
    // for some reason requires it to be public.
    static const constexpr uint8_t Timestamp = 253;

private:
    template <typename T> bool read(QIODevice&, T& t, Architecture);
    template <typename T> bool read(QIODevice&, T& t, int size = sizeof(T));
    template <typename T> bool read(QIODevice&, T* t, int size);

    template <typename T, typename VALTYPE, bool endian = true>
    inline T read(QIODevice&, int size, Architecture, bool& error, bool& isInvalid);

    template <typename T> T read(QIODevice&, const Field& field, Architecture, bool& error, bool& isInvalid);

    inline static bool skip(QIODevice&, int64_t size);
    inline bool skip(QIODevice&, const Field& field, Architecture architecture);
    bool skip(QIODevice&, const Definition&);
    bool skip(QIODevice&, const QVector<Field>&, Architecture architecture);
    bool parse(QIODevice&);
    bool parse(QIODevice&, FileHeader& header);
    bool parseRecord(QIODevice&);
    bool parseCmpTimestamp(QIODevice&, CmpTimeHeader);
    bool parseDefinition(QIODevice&, RecordHeader);
    bool parseData(QIODevice&, RecordHeader);
    bool parseData(QIODevice&, const Definition&);
    bool parseUserProfile(QIODevice&, const Definition&);
    bool parseBikeProfile(QIODevice&, const Definition&);
    bool parseSport(QIODevice&, const Definition&);
    bool parseRecord(QIODevice&, const Definition&);
    bool parseEvent(QIODevice&, const Definition&);
    bool parseLap(QIODevice&, const Definition&);

    void guessTags(Sport sport, SubSport subSport);    // see comment in .cpp

    static inline QDateTime dateTime(uint32_t fit_time);

    QIODevice::OpenMode openMode() const override { return QIODevice::ReadOnly; }

    void reset();

    ParseTrk   m_track;    // parse data for a track
    PointItem  m_pt;       // parse data for a track point

    uint32_t   m_time;     // current file time: used for time delta increments
    uint32_t   m_prevTime;

    std::array<Definition, 16> m_definitions;  // record definitions
};

class GeoSaveFit final : public GeoSaveBase, public GeoCommonFit
{
public:
    using GeoSaveBase::GeoSaveBase;

    bool save(QIODevice&) override;
    bool hasExt(const QString& ext) const override { return ext.toLower() == "fit"; }
    GeoFormat format() const override { return GeoFormat::Fit; }

private:
    static const QVector<RecordField> recordFields;

    QIODevice::OpenMode openMode() const override { return QIODevice::ReadWrite; }

    template <typename T> bool write(QIODevice&, const T& t, int size = sizeof(T));
    static inline uint32_t dateTime(const QDateTime& time);
    static inline uint16_t crc(uint16_t crc, uint8_t byte);

    static const constexpr int recordId = 0;

    bool saveFileHeader(QIODevice&, uint32_t dataSize);
    bool saveCRC(QIODevice&);
    bool saveRecordDef(QIODevice&);
    bool saveTrkpt(QIODevice&, const PointItem&);
    bool saveTrkseg(QIODevice&, const PointModel::value_type&);
    bool saveTrack(QIODevice&, const QModelIndex&);
    bool saveTracks(QIODevice&);
};

#endif // GEOIOFIT_H
