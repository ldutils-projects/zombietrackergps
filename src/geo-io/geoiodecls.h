/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOIODECLS_H
#define GEOIODECLS_H

#include <type_traits>

// Currently supported I/O formats
enum class GeoFormat {
    Native = 0,  // Our own binary format for super-fast save/load.  Yay!
    Gpx,         // https://en.wikipedia.org/wiki/GPS_Exchange_Format
    Tcx,         // https://en.wikipedia.org/wiki/Training_Center_XML
    Kml,         // https://en.wikipedia.org/wiki/Keyhole_Markup_Language
    Fit,         // (no public spec yet discovered)
    Unknown,     // no known format
};

// Features we can import (e.g, tracks, or waypoints)
enum class GeoIoFeature {
    None      = 0,
    Trk       = (1<<0),  // tracks
    Wpt       = (1<<1),  // waypoints
    Rte       = (1<<2),  // routes
    Pnt       = (1<<3),  // points, for queries etc
    Unk       = (1<<4),  // unknown
    AuxTrk    = (1<<5),  // track aux data
    AuxRte    = (1<<6),  // route aux data
    ExtZtgps  = (1<<7),  // produce Ztgps GPX extensions
    Formatted = (1<<8),  // produce formatted output
    Spaces    = (1<<9),  // write spaces instead of tabs
    AllTrk    = (1<<10), // add all tracks to save list
    AllRte    = (1<<11), // add all routes to save list
    AllWpt    = (1<<12), // add all waypoints to save list

    AllAux    = (AuxTrk|AuxRte),
    AllTypes  = (Trk|Wpt|Rte),  // primary types
    AllData   = (AllTrk | AllRte | AllWpt),
};

inline GeoIoFeature operator~(GeoIoFeature rhs)
{
    return GeoIoFeature(~std::underlying_type_t<GeoIoFeature>(rhs));
}

inline GeoIoFeature operator|(GeoIoFeature lhs, GeoIoFeature rhs)
{
    return GeoIoFeature(std::underlying_type_t<GeoIoFeature>(lhs) | std::underlying_type_t<GeoIoFeature>(rhs));
}

inline GeoIoFeature operator&(GeoIoFeature lhs, GeoIoFeature rhs)
{
    return GeoIoFeature(std::underlying_type_t<GeoIoFeature>(lhs) & std::underlying_type_t<GeoIoFeature>(rhs));
}

inline GeoIoFeature& operator|=(GeoIoFeature& lhs, const GeoIoFeature& rhs)
{
    return lhs = lhs | rhs;
}

inline GeoIoFeature& operator&=(GeoIoFeature& lhs, const GeoIoFeature& rhs)
{
    return lhs = lhs & rhs;
}

#endif // GEOIODECLS_H
