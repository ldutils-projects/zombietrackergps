/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <src/util/util.h>

#include "src/core/pointmodel.h"
#include "geoioconv.h"

GeoIoConv::GeoIoConv(QIODevice& stdout, QIODevice& stdin, const GeoSaveParams& saveParam, const GeoLoadParams& loadParam) :
    m_saver(nullptr, m_trk, m_wpt, saveParam),
    m_loader(nullptr, m_trk, m_wpt, loadParam)
{
    m_saver.setStdout(stdout);
    m_loader.setStdin(stdin);
}

bool GeoIoConv::convert(const QString& output, const QString& input)
{
    return load(input) && save(output);
}

bool GeoIoConv::load(const QString& input)
{
    return input.isEmpty() || m_loader.load(input);
}

bool GeoIoConv::save(const QString& output)
{
    return output.isEmpty() || m_saver.save(output);
}

void GeoIoConv::clear()
{
    m_trk.clear();
    m_wpt.clear();
}

int GeoIoConv::count(GeoIoFeature f) const
{
    int count = 0;
    if (HasFeature(f, GeoIoFeature::Trk))
        count += m_trk.rowCount();

    if (HasFeature(f, GeoIoFeature::Wpt))
        count += m_wpt.rowCount();

    // count all points in model
    if (HasFeature(f, GeoIoFeature::Pnt)) {
        Util::Recurse(m_trk, [this, &count](const QModelIndex& idx) {
            if (const PointModel* pm = m_trk.geoPoints(idx); pm != nullptr)
                for (const auto& seg : *pm)
                    count += seg.size();
            return true;
        });
    }

    return count;
}
