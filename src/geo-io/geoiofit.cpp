/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <array>

#include <QtEndian>
#include <QFileInfo>
#include <QModelIndex>

#include "src/core/cfgdata.h"
#include "src/core/trackmodel.h"
#include "src/core/app.h"

#include "geoiofit.h"

const QString GeoLoadFit::nameStatic = "FIT";

// This is the FIT format epoch.  Times are 32 bits as epoch + N sec.
const QDateTime GeoCommonFit::epochDate = QDateTime::fromString("1989-12-31T00:00:00Z", Qt::ISODate);

enum class GeoCommonFit::TypeNum : uint8_t {
    Enum    = 0x00,
    Sint8   = 0x01,
    Uint8   = 0x02,
    Sint16  = 0x03,
    Uint16  = 0x04,
    Sint32  = 0x05,
    Uint32  = 0x06,
    String  = 0x07,
    Float32 = 0x08,
    Float64 = 0x09,
    Uint8z  = 0x0A,
    Uint32z = 0x0C,
    Uint16z = 0x0B,
    Byte    = 0x0D,
    Sint64  = 0x0E,
    Uint64  = 0x0F,
    Uint64z = 0x10,
};

enum class GeoCommonFit::Sport : uint8_t {
    Generic                 = 0,
    Running                 = 1,
    Cycling                 = 2,
    Transition              = 3,
    Fitness_Equipment       = 4,
    Swimming                = 5,
    Basketball              = 6,
    Soccer                  = 7,
    Tennis                  = 8,
    American_Football       = 9,
    Training                = 10,
    Walking                 = 11,
    Cross_Country_Skiing    = 12,
    Alpine_Skiing           = 13,
    Snowboarding            = 14,
    Rowing                  = 15,
    Mountaineering          = 16,
    Hiking                  = 17,
    Multisport              = 18,
    Paddling                = 19,
    Flying                  = 20,
    E_Biking                = 21,
    Motorcycling            = 22,
    Boating                 = 23,
    Driving                 = 24,
    Golf                    = 25,
    Hang_Gliding            = 26,
    Horseback_Riding        = 27,
    Hunting                 = 28,
    Fishing                 = 29,
    Inline_Skating          = 30,
    Rock_Climbing           = 31,
    Sailing                 = 32,
    Ice_Skating             = 33,
    Sky_Diving              = 34,
    Snowshoeing             = 35,
    Snowmobiling            = 36,
    Stand_Up_Paddleboarding = 37,
    Surfing                 = 38,
    Wakeboarding            = 39,
    Water_Skiing            = 40,
    Kayaking                = 41,
    Rafting                 = 42,
    Windsurfing             = 43,
    Kitesurfing             = 44,
    Tactical                = 45,
    Jumpmaster              = 46,
    Boxing                  = 47,
    Floor_Climbing          = 48,
};

enum class GeoCommonFit::SubSport : uint8_t {
    Generic                 = 0,
    Treadmill               = 1,
    Street                  = 2,
    Trail                   = 3,
    Track                   = 4,
    Spin                    = 5,
    Indoor_Cycling          = 6,
    Road                    = 7,
    Mountain                = 8,
    Downhill                = 9,
    Recumbent               = 10,
    Cyclocross              = 11,
    Hand_Cycling            = 12,
    Track_Cycling           = 13,
    Indoor_Rowing           = 14,
    Elliptical              = 15,
    Stair_Climbing          = 16,
    Lap_Swimming            = 17,
    Open_Water              = 18,
    Flexibility_Training    = 19,
    Strength_Training       = 20,
    Warm_Up                 = 21,
    Match                   = 22,
    Exercise                = 23,
    Challenge               = 24,
    Indoor_Skiing           = 25,
    Cardio_Training         = 26,
    Indoor_Walking          = 27,
    E_Bike_Fitness          = 28,
    Bmx                     = 29,
    Casual_Walking          = 30,
    Speed_Walking           = 31,
    Bike_To_Run_Transition  = 32,
    Run_To_Bike_Transition  = 33,
    Swim_To_Bike_Transition = 34,
    Atv                     = 35,
    Motocross               = 36,
    Backcountry             = 37,
    Resort                  = 38,
    Rc_Drone                = 39,
    Wingsuit                = 40,
    Whitewater              = 41,
    Skate_Skiing            = 42,
    Yoga                    = 43,
    Pilates                 = 44,
    Indoor_Running          = 45,
    Gravel_Cycling          = 46,
    E_Bike_Mountain         = 47,
    Commuting               = 48,
    Mixed_Surface           = 49,
    Navigate                = 50,
    Track_Me                = 51,
    Map                     = 52,
    Single_Gas_Diving       = 53,
    Multi_Gas_Diving        = 54,
    Gauge_Diving            = 55,
    Apnea_Diving            = 56,
    Apnea_Hunting           = 57,
    Virtual_Activity        = 58,
    Obstacle                = 59,
};

// Record fields of interest to us.
enum class GeoCommonFit::RecordField : uint8_t {
    Lat           = 0,
    Lon           = 1,
    Alt           = 2,
    HR            = 3,
    Cad           = 4,
    Dist          = 5,
    Speed         = 6,
    Power         = 7,
    SpeedDistance = 8,
    Grade         = 9,
    Temp          = 13,
    Timestamp     = GeoLoadFit::Timestamp,
};

// Sport fields of interest to us.
enum class GeoCommonFit::SportField : uint8_t {
    Sport    = 0,
    SubSport = 1,
    Name     = 3,
};

// Lap fields of interest to us.
enum class GeoCommonFit::LapField : uint8_t {
    TotalCalories    = 11,
    TotalFatCalories = 12,
    Sport            = 25,
    SubSport         = 39,
    Timestamp        = GeoLoadFit::Timestamp,
};

template <typename T> inline T GeoCommonFit::invalid() const { return T(-1); }
template <> inline int32_t GeoCommonFit::invalid() const { return 0x7fffffff; }
template <> inline int16_t GeoCommonFit::invalid() const { return 0x7fff; }
template <> inline int8_t GeoCommonFit::invalid() const { return 0x7f; }
template <> inline QString GeoCommonFit::invalid() const { return ""; }

template <> inline float GeoCommonFit::invalid() const
 {
    static const uint32_t val = 0xffffffff;
    const char* charp = reinterpret_cast<const char*>(&val);
    return *(reinterpret_cast<const float *>(charp));
}

bool GeoLoadFit::load(QIODevice& io)
{
    if (!io.isOpen() && !io.open(openMode()))
        return false;

    io.seek(0); // start at beginning
    reset(); // reset data for parse
    m_track.newSegment(); // initial segment to populate

    if (auto* file = qobject_cast<QFile*>(&io); file != nullptr)
        m_track.name = QFileInfo(*file).baseName();

    if (parse(io)) {
        m_track.append(geoLoad);
        return true;
    }

    return false;
}

bool GeoLoadFit::is(QIODevice& io) const
{
    if (!io.isOpen() && !io.open(QIODevice::ReadOnly))
        return false;

    if (!io.isSequential())
        io.seek(0); // start at beginning

    GeoLoadFit query(geoLoad);
    FileHeader header;
    return query.parse(io, header);
}

void GeoLoadFit::reset()
{
    m_time = m_prevTime = 0;

    m_track.clear();
    m_pt.clear();
}

inline QDateTime GeoLoadFit::dateTime(uint32_t fit_time)
{
    return epochDate.addSecs(fit_time);
}

// We attempt to guess a suitable set of tags from the Sport/SubSport info, if any,
// in the FIT file.  The user might have changed the tag names in the configuration,
// or deleted them.  There's nothing we can do about that, so this is merely an
// attempt to be helpful.  If it fails, oh well.
void GeoLoadFit::guessTags(Sport sport, SubSport subSport)
{
    QString tag;

    switch (sport) {
    case Sport::Running:           tag = "Run";        break;
    case Sport::Cycling:
        switch (subSport) {
        case SubSport::Mountain:   tag = "Mountain";   break;
        case SubSport::Gravel_Cycling: [[fallthrough]];
        case SubSport::Cyclocross: tag = "Cross";      break;
        case SubSport::Commuting:  tag = "Commute";    break;
        case SubSport::Road:       [[fallthrough]];
        default:                   tag = "Road";       break;
        }
        break;
    case Sport::Swimming:          tag = "Swim";       break;
    case Sport::Soccer:            tag = "Soccer";     break;
    case Sport::Walking:           tag = "Hike";       break;
    case Sport::Alpine_Skiing:     tag = "Ski";        break;
    case Sport::Snowboarding:      tag = "Board";      break;
    case Sport::Rowing:            tag = "Row";        break;
    case Sport::Hiking:            tag = "Hike";       break;
    case Sport::Flying:            tag = "Plane";      break;
    case Sport::Boating:           tag = "Sail";       break;
    case Sport::Motorcycling:      tag = "Motorcycle"; break;
    case Sport::Hang_Gliding:      tag = "HangGlide";  break;
    case Sport::Driving:           tag = "Car";        break;
    case Sport::Horseback_Riding:  tag = "Horse";      break;
    case Sport::Rock_Climbing:     tag = "Climb";      break;
    case Sport::Surfing:           tag = "Surf";       break;
    case Sport::Kayaking:          tag = "Kayak";      break;
    case Sport::Rafting:           tag = "Raft";       break;
    default:
        break;
    }

    // We didn't recognize it.  Give up, but leave any old data in place.
    if (tag.isEmpty())
        return;

    m_track.tags.clear();

    // Only add this tag if it exists.  If not, we just give up: there's nothing much else to do.
    if (cfgData().tags.contains(tag))
        m_track.tags += tag;
}

template <typename T> bool GeoLoadFit::read(QIODevice& io, T& t, int size)
{
    return io.read(reinterpret_cast<char *>(&t), size) == size;
}

template <> bool GeoLoadFit::read(QIODevice& io, char* t, int size)
{
    return io.read(t, size) == size;
}

template <typename T> bool GeoLoadFit::read(QIODevice& io, T& t, Architecture architecture)
{
    const bool rc = read(io, t, sizeof(T));
    t = (architecture == Architecture::Big) ? qFromBigEndian(t) : qFromLittleEndian(t);
    return rc;
}

// Read a single value, converting to T, from the base type in VALTYPE
template <typename T, typename VALTYPE, bool endian>
inline T GeoLoadFit::read(QIODevice& io, int size, Architecture architecture, bool& error, bool& isInvalid)
{
    if (error || size != sizeof(VALTYPE)) { // we don't yet support arrayed data
        error = !skip(io, size);
        isInvalid = true;
        return invalid<T>();
    }

    VALTYPE raw;
    if (error = !read(io, raw, size); error)
        return invalid<T>();

    if (endian)
        raw = (architecture == Architecture::Big) ? qFromBigEndian(raw) : qFromLittleEndian(raw);

    const auto invalidVal = invalid<VALTYPE>();
    isInvalid = (memcmp(&raw, &invalidVal, sizeof(raw)) == 0);

    return T(raw);
}

// Read a field value in the type defined in the Field spec, and convert to type T
template <typename T> 
T GeoLoadFit::read(QIODevice& io, const Field& field, Architecture architecture, bool& error, bool& isInvalid)
{
    // No array reads yet supported
    switch (TypeNum(field.type.typeNum)) {
    case TypeNum::Byte:    [[fallthrough]];
    case TypeNum::Enum:    [[fallthrough]];
    case TypeNum::Uint8:   return read<T, uint8_t>(io, field.size, architecture, error, isInvalid);
    case TypeNum::Sint8:   return read<T, int8_t>(io, field.size, architecture, error, isInvalid);
    case TypeNum::Uint16:  return read<T, uint16_t>(io, field.size, architecture, error, isInvalid);
    case TypeNum::Sint16:  return read<T, int16_t>(io, field.size, architecture, error, isInvalid);
    case TypeNum::Uint32:  return read<T, uint32_t>(io, field.size, architecture, error, isInvalid);
    case TypeNum::Sint32:  return read<T, int32_t>(io, field.size, architecture, error, isInvalid);
    case TypeNum::Float32: return read<T, float, false>(io, field.size, architecture, error, isInvalid);
    case TypeNum::Float64: [[fallthrough]]; // TODO: ...
    case TypeNum::String:  [[fallthrough]]; // TODO: ...
    default:
        skip(io, field.size);
        isInvalid = true;
        return invalid<T>();
    }
}

bool GeoLoadFit::parseDefinition(QIODevice& io, RecordHeader header)
{
    const auto readFields = [this](QIODevice& io, QVector<Field>& fields) {
        uint8_t numFields;
        if (!read(io, numFields))
            return false;
        
        fields.reserve(numFields);

        Field field;
        while (numFields-- != 0) {
            if (!read(io, field))
                return false;
            fields.append(field);
        }

        return true;
    };

    Definition& def = m_definitions.at(header.localId);
    def.clear(); // definitions can be redefined in the file.

    return read(io, def.reserved) &&
           read(io, def.architecture) &&
           read(io, def.globalIdRaw, def.architecture) &&
           readFields(io, def.fields) &&
           (!header.devFlag || readFields(io, def.devFields));
}

inline bool GeoLoadFit::skip(QIODevice& io, int64_t size)
{
    return (size == 0) ? true : io.seek(io.pos() + size);
}

inline bool GeoLoadFit::skip(QIODevice& io, const Field& field, Architecture architecture)
{
    // Timestamps can appear in messages we don't recognize, and we must still account for them.
    if (field.rawId == Timestamp) {
        bool error = false, invalid;
        m_time = read<uint32_t>(io, field, architecture, error, invalid);
        return !error;
    }

    return skip(io, field.size);
}

bool GeoLoadFit::skip(QIODevice& io, const QVector<Field>& fields, Architecture architecture)
{
    for (const auto& field : fields)
        if (!skip(io, field, architecture))
            return false;

    return true;
}

bool GeoLoadFit::skip(QIODevice& io, const Definition& def)
{
    return skip(io, def.fields, def.architecture) &&
           skip(io, def.devFields, def.architecture);
}

bool GeoLoadFit::parseUserProfile(QIODevice& io, const Definition& def)
{
    return skip(io, def); // TODO: ...
}

bool GeoLoadFit::parseBikeProfile(QIODevice& io, const Definition& def)
{
    return skip(io, def); // TODO: ...
}

bool GeoLoadFit::parseLap(QIODevice& io, const Definition& def)
{
    ++m_track.laps;

    bool error = false;
    bool invalid = false;

    Sport      sport    = Sport::Generic;
    SubSport   subSport = SubSport::Generic;

    for (const auto& field : def.fields) {
        const auto val = read<uint32_t>(io, field, def.architecture, error, invalid);
        if (error)   return false;
        if (invalid) continue;

        switch (field.lap) {
        case LapField::Sport:    sport    = Sport(val);    break;
        case LapField::SubSport: subSport = SubSport(val); break;
        default: break;
        }
    }

    guessTags(sport, subSport);

    return true;
}

bool GeoLoadFit::parseSport(QIODevice& io, const Definition& def)
{
    bool error = false;
    bool invalid = false;

    Sport      sport    = Sport::Generic;
    SubSport   subSport = SubSport::Generic;

    for (const auto& field : def.fields) {
        const auto val = read<uint32_t>(io, field, def.architecture, error, invalid);
        if (error)   return false;
        if (invalid) continue;

        switch (field.sport) {
        case SportField::Sport:    sport    = Sport(val);    break;
        case SportField::SubSport: subSport = SubSport(val); break;
        default: break;
        }
    }

    guessTags(sport, subSport);

    return true;
}

bool GeoLoadFit::parseRecord(QIODevice& io, const Definition& def)
{
    bool error = false;
    bool invalid = false;

    assert(!m_track.geoPoint.empty());  // we must have made this already.

    m_pt.clear();

    for (const auto& field : def.fields) {
        const auto val = read<uint32_t>(io, field, def.architecture, error, invalid);
        if (error)   return false;
        if (invalid) continue;

        switch (field.record) {
        case RecordField::Lat:       m_pt.setLat((int32_t(val) / double(0x7fffffff)) * 180.0); break;
        case RecordField::Lon:       m_pt.setLon((int32_t(val) / double(0x7fffffff)) * 180.0); break;
        case RecordField::Alt:       m_pt.setEle((val / 5.0) - 500.0); break;
        case RecordField::HR:        m_pt.setHr(uint8_t(val)); break;
        case RecordField::Cad:       m_pt.setCad(uint8_t(val)); break;
        case RecordField::Power:     m_pt.setPower(val); break;
        case RecordField::Speed:     m_pt.setSpeed(Speed_t(val/1000.0f)); break;
        case RecordField::Temp:      m_pt.setATemp(int32_t(val)); break;
        case RecordField::Timestamp: m_time = val; break;
        default: break;
        }
    }

    if (m_time > m_prevTime && m_pt.hasLoc()) {
        m_pt.setTime(dateTime(m_time));
        m_track.append(m_pt, geoLoad().hasFeature(GeoIoFeature::AuxTrk));
        m_prevTime = m_time;
    }

    return true;
}

bool GeoLoadFit::parseEvent(QIODevice& io, const Definition& def)
{
    return skip(io, def); // TODO: ...
}

bool GeoLoadFit::parseCmpTimestamp(QIODevice& io, CmpTimeHeader header)
{
    static const uint32_t lsb5 = 0x1f;

    const uint32_t offset = header.timeOffset;

    // Odd, but that's how it works.  I don't know why it isn't a simple additive offset.
    m_time = (m_time & ~lsb5) + offset + ((offset >= (m_time & lsb5)) ? 0x00 : 0x20);

    return parseData(io, m_definitions.at(header.localId));
}

bool GeoLoadFit::parseData(QIODevice& io, RecordHeader header)
{
    return parseData(io, m_definitions.at(header.localId));
}

bool GeoLoadFit::parseData(QIODevice& io, const Definition& def)
{
    if (def.fields.isEmpty() && def.devFields.isEmpty())
        return false;

    switch (def.globalId) {
    case Message::UserProfile:  return parseUserProfile(io, def);
    case Message::BikeProfile:  return parseBikeProfile(io, def);
    case Message::Sport:        return parseSport(io, def);
    case Message::Lap:          return parseLap(io, def);
    case Message::Record:       return parseRecord(io, def);
    case Message::Event:        return parseEvent(io, def);
    default:                    return skip(io, def);
    }
}

bool GeoLoadFit::parseRecord(QIODevice& io)
{
    RecordHeader header;

    reportRead(io.pos()); // for the progress bar

    if (!read(io, header))
        return false;

    if (header.cmpTimestamp)
        return parseCmpTimestamp(io, header);

    if (header.defMessage)
        return parseDefinition(io, header);

    return parseData(io, header);
}

bool GeoLoadFit::parse(QIODevice& io, FileHeader& header)
{
    if (!read(io, header.size)     ||
        !read(io, header.version)  ||
        !read(io, header.profile)  ||
        !read(io, header.dataSize) ||
        !read(io, header.magic, sizeof(header.magic)))
        return false;

    if (strncmp(header.magic.data(), ".FIT", 4) != 0)
        return false;

    if (header.size >= (FileHeader::minHeaderLength + 2))
        if (!read(io, header.crc))
            return false;

    return skip(io, header.size - io.pos());
}

bool GeoLoadFit::parse(QIODevice& io)
{
    FileHeader header;

    if (!parse(io, header))
        return false;

    // CRC: should always be 2 bytes, but we ignore anything after it, just in case.
    const uint32_t trailDataSize = uint32_t(io.bytesAvailable()) - header.dataSize;

    while (io.bytesAvailable() > trailDataSize)
        if (!parseRecord(io))
            return false;

    return true;
}

// We save lat,lon,Alt,HR,Cad,Temp,Timestamp
const QVector<GeoSaveFit::RecordField> GeoSaveFit::recordFields = {
    RecordField::Lat,
    RecordField::Lon,
    RecordField::Alt,
    RecordField::HR,
    RecordField::Cad,
    RecordField::Temp,
    RecordField::Speed,
    RecordField::Timestamp,
};

template <typename T> bool GeoSaveFit::write(QIODevice& io, const T& t, int size)
{
    return io.write(reinterpret_cast<const char*>(&t), size) == size;
}

inline uint32_t GeoSaveFit::dateTime(const QDateTime& time)
{
    return uint32_t(epochDate.secsTo(time));
}

inline uint16_t GeoSaveFit::crc(uint16_t crc, uint8_t byte)
{
    static const std::array<uint16_t, 16> crc_table = {
        0x0000, 0xCC01, 0xD801, 0x1400, 0xF001, 0x3C00, 0x2800, 0xE401,
        0xA001, 0x6C00, 0x7800, 0xB401, 0x5000, 0x9C01, 0x8801, 0x4400
    };

    uint16_t tmp;

    tmp = crc_table.at(crc & 0xF);
    crc = (crc >> 4) & 0x0FFF;
    crc = crc ^ tmp ^ crc_table.at(byte & 0xF);

    tmp = crc_table.at(crc & 0xF);
    crc = (crc >> 4) & 0x0FFF;
    crc = crc ^ tmp ^ crc_table.at((byte >> 4) & 0xF);

    return crc;
}

bool GeoSaveFit::saveFileHeader(QIODevice& io, uint32_t dataSize)
{
    FileHeader header;
    header.size     = sizeof(FileHeader);
    header.version  = 16;
    header.profile  = 152;
    header.dataSize = dataSize;
    header.magic    = { '.', 'F', 'I', 'T' };
    header.crc      = 0;

    return write(io, header);
}

bool GeoSaveFit::saveRecordDef(QIODevice& io)
{
    const RecordHeader recHeader(recordId, 1);

    // TODO: fix to make archicture conditional on actual endianness
    const DefinitionHeader defHeader(Message::Record);

    const auto numFields = uint8_t(recordFields.size());

    if (!write(io, recHeader) ||
        !write(io, defHeader) ||
        !write(io, numFields))
        return false;

    for (const auto rec : recordFields) {
        Field field;
        field.record = rec;

        switch (rec) {
        case RecordField::Lat:       [[fallthrough]];
        case RecordField::Lon:       field.type = FieldType(TypeNum::Sint32, true); field.size = 4; break;
        case RecordField::Alt:       field.type = FieldType(TypeNum::Uint16, true); field.size = 2; break;
        case RecordField::HR:        field.type = FieldType(TypeNum::Uint8, true);  field.size = 1; break;
        case RecordField::Cad:       field.type = FieldType(TypeNum::Uint8, true);  field.size = 1; break;
        case RecordField::Power:     field.type = FieldType(TypeNum::Uint16, true); field.size = 2; break;
        case RecordField::Speed:     field.type = FieldType(TypeNum::Uint16, true); field.size = 2; break;
        case RecordField::Temp:      field.type = FieldType(TypeNum::Sint8, true);  field.size = 1; break;
        case RecordField::Timestamp: field.type = FieldType(TypeNum::Uint32, true); field.size = 4; break;
        default: assert(0);
        }

        if (!write(io, field))
            return false;
    }

    return true;
}

bool GeoSaveFit::saveTrkpt(QIODevice& io, const PointItem& trkpt)
{
    RecordHeader header(recordId);

    if (!write(io, header))
        return false;

    bool success = true;

    for (const auto rec : recordFields) {
        switch (rec) {
        case RecordField::Lat:       success &= write(io, int32_t(PointItem::Lat_t::base_type(trkpt.lat()) * PointItem::Lat_t::base_type(0x7fffffff) / 180.0)); break;
        case RecordField::Lon:       success &= write(io, int32_t(PointItem::Lon_t::base_type(trkpt.lon()) * PointItem::Lon_t::base_type(0x7fffffff) / 180.0)); break;
        case RecordField::Alt:       success &= write(io, trkpt.hasEle() ? uint16_t(((PointItem::Ele_t::base_type(trkpt.ele(false)) + 500.0) * 5.0)) : invalid<uint16_t>()); break;
        case RecordField::HR:        success &= write(io, trkpt.hasHr() ? trkpt.hr() : invalid<uint8_t>()); break;
        case RecordField::Cad:       success &= write(io, trkpt.hasCad() ? trkpt.cad() : invalid<uint8_t>()); break;
        case RecordField::Power:     success &= write(io, trkpt.hasPower(PointItem::Measured) ? uint16_t(trkpt.power()) : invalid<uint16_t>()); break;
        case RecordField::Speed:     success &= write(io, trkpt.hasSpeed(PointItem::Measured) ? uint16_t(Speed_t::base_type(trkpt.speed()) * 1000.0) : invalid<uint16_t>()); break;
        case RecordField::Temp:      success &= write(io, trkpt.hasAtemp() ? int8_t(trkpt.atemp()) : invalid<int8_t>()); break;
        case RecordField::Timestamp: success &= write(io, trkpt.hasTime() ? dateTime(trkpt.time()) : invalid<uint32_t>()); break;
        default: assert(0);
        }
    }

    return success;
}

bool GeoSaveFit::saveTrkseg(QIODevice& io, const PointModel::value_type& trkseg)
{
    for (const auto& geoPt : trkseg)
        if (!saveTrkpt(io, geoPt))
            return false;

    return true;
}

bool GeoSaveFit::saveTrack(QIODevice& io, const QModelIndex& idx)
{
    // For each track segment
    for (const auto& trkseg : *geoSave.trkModel().geoPoints(idx))
        if (!saveTrkseg(io, trkseg))
            return false;

    return true;
}

bool GeoSaveFit::saveTracks(QIODevice& io)
{
    if (!saveRecordDef(io))  // write definition for our record format
        return false;

    for (const auto& idx : geoSave().m_trkSelection)
        if (!saveTrack(io, idx))
            return false;

    return true;
}

bool GeoSaveFit::saveCRC(QIODevice& io)
{
    io.seek(0);

    // Calculate and save CRC
    uint16_t crcVal = 0;
    uint8_t byte;

    io.seek(0);

    while (!io.atEnd()) {
        if (!io.getChar(reinterpret_cast<char*>(&byte)))
            return false;
        crcVal = crc(crcVal, byte);
    }

    return write(io, crcVal);
}

bool GeoSaveFit::save(QIODevice& io)
{
    // Abort if more than one track is selected.  TODO: can FIT support that?
    if (geoSave().m_trkSelection.size() != 1) {
        geoSave.m_errorString = QObject::tr("FIT export only supports one track per file.");
        return false;
    }

    if (!io.isOpen() && !io.open(openMode() | QIODevice::Truncate))
        return false;

    // For the moment, we write a header without a size.  It's at the start of the file:
    // we'll overwrite it later once we know the file size.
    if (!saveFileHeader(io, 0) ||
        !saveTracks(io))
        return false;

    // Overwrite header now that we know the final data size
    assert(uint32_t(io.pos()) >= sizeof(FileHeader));
    const uint32_t dataSize = uint32_t(io.pos()) - sizeof(FileHeader);

    io.seek(0);

    return saveFileHeader(io, dataSize) &&
           saveCRC(io);
}
