/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOIOBASE_H
#define GEOIOBASE_H

#include <QObject>
#include <QModelIndex>
#include <QString>
#include <QIODevice>
#include <QDateTime>

#include "src/core/pointmodel.h"
#include "geoio.h"

class GeoLoad;
class GeoSave;

// Interface to reading GPX/KML/etc formats, to be derived from for each format.
class GeoLoadBase : public QObject
{
public:
    GeoLoadBase(GeoLoad&);

    [[nodiscard]] virtual bool load(const QString& path);      // load zero or more tracks, appending.  returns success.
    [[nodiscard]] virtual bool load(QIODevice&) = 0;           // load zero or more tracks, appending.  returns success.
    [[nodiscard]] virtual bool is(const QString& path) const;  // true if this is the file type we load
    [[nodiscard]] virtual bool is(QIODevice&) const = 0;       // true if this is the file type we load
    [[nodiscard]] virtual GeoFormat format() const = 0;        // format
    [[nodiscard]] virtual bool isBinary() const = 0;           // true if it's a binary format
    [[nodiscard]] virtual const QString& name() const = 0;     // format name (GPX, FIT, etc)
    [[nodiscard]] virtual QIODevice::OpenMode openMode() const = 0;

protected:
    // Parse data for tracks.  append() adds the accumulated data to the model.
    struct ParseCommon {
        void clear() {
            name.clear();
            desc.clear();
            tags.clear();
        }

        QString     name;
        QString     desc;
        QStringList tags;
    };

    // Parse data for tracks
    struct ParseTrk : public ParseCommon {
        ParseTrk() { geoPoint.incDontTrack(); } // don't track changes for this scratch model

        void clear() {
            ParseCommon::clear();
            geoPoint.clear();
            keywords.clear();
            color    = QColor();
            laps = 0;
        }

        void append(PointItem&, bool addAuxData); // add point to pending track
        void append(GeoLoad&);   // append track to model
        void newSegment();

        QString     keywords;
        QColor      color;
        PointModel  geoPoint;
        uint32_t    laps = 0;
    };

    // Parse data for routes: mostly shared with track parser, but add as a Route.
    struct ParseRte : public ParseTrk {
        using ParseTrk::append;
        void append(GeoLoad&);
    };

    // Parse data for waypoints.
    struct ParseWpt : public ParseCommon {
        void clear() {
            ParseCommon::clear();
            symbol.clear();
            type.clear();
            lat = lon = ele = std::numeric_limits<double>::quiet_NaN();
        }

        void append(GeoLoad&);

        QString   symbol;
        QString   type;
        QDateTime time;
        double    lat, lon, ele;
    };

    // Report progress to the progres bar.
    // We report differences, not positions, because we may be part of a multi-file read.
    void reportRead(qint64 b) { geoLoad.reportRead(b - lastPos); lastPos = b; }

    GeoLoad& geoLoad;
    qint64   lastPos;

private:
    GeoLoadBase(const GeoLoadBase&)            = delete;
    GeoLoadBase& operator=(const GeoLoadBase&) = delete;
};

// Interface to reading GPX/KML/etc formats, to be derived from for each format.
class GeoSaveBase : public QObject
{
public:
    GeoSaveBase(GeoSave& geoSave);

    [[nodiscard]] virtual bool save(QIODevice&) = 0;       // save tracks from selection.  returns success.
    [[nodiscard]] virtual bool save(const QString& path);  // save tracks from selection.  returns success.
    [[nodiscard]] virtual bool hasExt(const QString& ext) const = 0; // true if this extension is for our format
    [[nodiscard]] virtual GeoFormat format() const = 0;    // format
    [[nodiscard]] virtual QIODevice::OpenMode openMode() const = 0;

protected:
    // Report progress to the progres bar.
    void reportWrite(qint64 i) { geoSave.reportWrite(i); }

    // convenience function:
    template <typename T> T trkGet(ModelType, const QModelIndex&) const;
    template <typename T> T wptGet(ModelType, const QModelIndex&) const;

    GeoSave& geoSave;

    GeoSaveBase(const GeoSaveBase&)            = delete;
    GeoSaveBase& operator=(const GeoSaveBase&) = delete;
};

#endif // GEOIOBASE_H
