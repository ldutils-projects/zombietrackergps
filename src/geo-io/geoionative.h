/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOIONATIVE_H
#define GEOIONATIVE_H

#include <src/util/versionedstream.h>

#include "geoiobase.h"

// Subclass of GeoLoadBase for high performance private binary blob
class GeoLoadNative final : public GeoLoadBase
{
public:
    GeoLoadNative(GeoLoad&);

    using GeoLoadBase::is;

    bool load(QIODevice&) override;
    bool is(QIODevice&) const override;
    GeoFormat format() const override { return GeoFormat::Native; }
    bool isBinary() const override { return true; }
    const QString& name() const override { return nameStatic; }

    static const QString nameStatic;

private slots:
    void itemLoaded(qint64 pos) { reportRead(pos); }

private:
    bool openReader(QIODevice&);
    QIODevice::OpenMode openMode() const override { return QIODevice::ReadOnly; }

    VersionedStream readStream;
};

class GeoSaveNative final : public GeoSaveBase
{
public:
    GeoSaveNative(GeoSave& geoSave);

    bool save(QIODevice&) override;
    bool hasExt(const QString& ext) const override { return ext.toLower() == "ztgps"; }
    GeoFormat format() const override { return GeoFormat::Native; }

private slots:
    void itemSaved(qint64 i) { reportWrite(i); }

private:
    bool openWriter(QIODevice&);

    QIODevice::OpenMode openMode() const override { return QIODevice::WriteOnly; }

    VersionedStream writeStream;
};

#endif // GEOIONATIVE_H

