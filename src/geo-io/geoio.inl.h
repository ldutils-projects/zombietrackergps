/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOIO_INL_H
#define GEOIO_INL_H

#include <cassert>

#include "src/geo-io/geoio.h"
#include "src/geo-io/geoionative.h"
#include "src/geo-io/geoiofit.h"
#include "src/geo-io/geoiogpx.h"
#include "src/geo-io/geoiokml.h"
#include "src/geo-io/geoiotcx.h"

template <typename SL, typename RC, typename PRED, typename EXEC, typename FMT, typename... REST>
RC GeoSaveLoadBase::tryFormat(const PRED& pred, const EXEC& exec, SL& sl, const RC& def)
{
    if (FMT saveload(sl); pred(saveload))
        return exec(saveload);

    return tryFormat<SL, RC, PRED, EXEC, REST...>(pred, exec, sl, def);
}

// Try predicate on each supported format in turn, and execute exec fn on first that succeeds.
template <typename RC, typename PRED, typename EXEC>
RC GeoLoad::execIf(const PRED& pred, const EXEC& exec, const RC& rc)
{
    // Execute exec fn if predicate passes on one of these formats.
    return tryFormat<GeoLoad, RC, PRED, EXEC,
                     GeoLoadNative, GeoLoadGpx, GeoLoadTcx, GeoLoadKml, GeoLoadFit>
            (pred, exec, *this, rc);
}

template <typename RC, typename EXEC>
RC GeoLoad::execIf(GeoFormat format, const EXEC& exec, const RC& rc)
{
    return execIf<RC>([format](const GeoLoadBase& l) { return l.format() == format; }, exec, rc);
}

template <typename RC, typename EXEC>
RC GeoLoad::execIf(const QString& format, const EXEC& exec, const RC& rc)
{
    return execIf<RC>([format](const GeoLoadBase& l) { return l.name() == format; }, exec, rc);
}

// Try predicate on each supported format in turn, and execute exec fn on first that succeeds.
template <typename RC, typename PRED, typename EXEC>
RC GeoSave::execIf(const PRED& pred, const EXEC& exec, const RC& rc)
{
    // Execute exec fn if predicate passes on one of these formats.
    return tryFormat<GeoSave, RC, PRED, EXEC,
                     GeoSaveNative, GeoSaveGpx, GeoSaveTcx, GeoSaveKml, GeoSaveFit>
            (pred, exec, *this, rc);
}

#endif // GEOIO_INL_H
