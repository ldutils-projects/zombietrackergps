/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOIOGPX_H
#define GEOIOGPX_H

#include <QVector>
#include <QString>
#include <QDateTime>
#include <QColor>
#include <tuple>

#include "geoioxml.h"

class GeoLoadGpx final : public GeoLoadXml
{
public:
    using GeoLoadXml::GeoLoadXml;
    using GeoLoadXml::is;

    bool is(QIODevice&) const override;
    GeoFormat format() const override { return GeoFormat::Gpx; }
    bool isBinary() const override { return false; }
    const QString& name() const override { return nameStatic; }

    static const QString nameStatic;

private:
    void parseXml() override;
    void parseMetadata();
    void parseLink();
    void parseAuthor();
    void parseCopyright();
    void parseGpxxExtension();
    void parseZtgpsExtension();
    void parseTrk();
    void parseWpt();
    void parseRte();
    void parseTrkseg();
    void parseTrkpt();
    void parseRtept();
    void parsePtCommon(void (GeoLoadGpx::*)(void));
    void parseGpxDataLap();
    void parseExtensionsGpxContext();
    void parseExtensionsTrkContext();
    void parseExtensionsRteContext();
    void parseExtensionsTrkRteCommon();
    void parseExtensionsRteptContext();
    void parseExtensionsTrkptContext();
    void parseTrackExtension();
    void parseTrackPointExtension();
    void parseRoutePointExtension();
    void parsePowerExtension();

    const char* topTag() const override;

    struct {
        void clear() {
            links.clear();
            name.clear();
            desc.clear();
            keywords.clear();
        }

        QVector<std::tuple<QString, QString, QString>> links; // tuple: URL, text, type
        QString          name;     // metadata level name
        QString          desc;     // description field
        QString          keywords; // metadata keywords
    } fileMetaData;

    ParseTrk   track;    // parse data for a track
    ParseRte   route;    // parse data for a route
    PointItem  pt;       // parse data for a track point
    ParseWpt   wpt;      // parse data for waypoints

    // These are stored in the class to reduce lambda capture size (for perf).
    QStringRef href;     // for parsing links
    QString    text;     // for link text, etc.
    QString    linkType; // for links
};

class GeoSaveGpx final : public GeoSaveXml
{
public:
    using GeoSaveXml::GeoSaveXml;

    bool hasExt(const QString& ext) const override { return ext.toLower() == "gpx"; }
    GeoFormat format() const override { return GeoFormat::Gpx; }

private:
    void saveXml() override;
    void saveMetadata();
    void saveTracksAndRoutes();
    void saveWaypoints();
    void saveWpt(const QModelIndex& idx);
    void saveTrk(const QModelIndex& idx);
    void saveRte(const QModelIndex& idx);
    void saveTrkCommon(const QModelIndex& idx);
    void saveTrkseg(const PointModel::value_type&);
    void saveTrkpt(const PointItem&);
    void saveRtept(const PointItem&);
    void savePtDataCommon(const PointItem&);
    void saveTrkExtensions(const QModelIndex&);
    void saveTrkptExtensions(const PointItem&);
};

#endif // GEOIOGPX_H
