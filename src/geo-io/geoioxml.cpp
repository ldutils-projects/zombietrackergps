/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "geoio.h"
#include "geoioxml.h"

// Strangely, Garmin's extension uses text color names like "Red", rather than RGB values.
namespace {
const QMap<QString, QColor> ColorNameMap =
{
    { "Default",      QColor() },
    { "Black",        QColor(0,0,0) },
    { "DarkRed",      QColor(128,0,0) },
    { "DarkGreen",    QColor(0,128,0) },
    { "DarkYellow",   QColor(128,128,0) },
    { "DarkBlue",     QColor(0,0,128) },
    { "DarkMagenta",  QColor(128,0,128) },
    { "DarkCyan",     QColor(0, 128, 128) },
    { "LightGray",    QColor(192,192,192) },
    { "DarkGray",     QColor(128,128,128) },
    { "Red",          QColor(255,0,0) },
    { "Green",        QColor(0,255,0) },
    { "Yellow",       QColor(255,255,0) },
    { "Blue",         QColor(0,0,255) },
    { "Magenta",      QColor(255,0,255) },
    { "Cyan",         QColor(0,255,255) },
    { "White",        QColor(255,255,255) },
    { "Transparent",  QColor() },
};
} // anonymous namespace

bool GeoLoadXml::is(QIODevice& io, const char* startTag)
{
    if (!openReader(io))
        return false;

    while (!done())
        if (const auto token = xml.readNext(); isStart(token))
            return xml.name() == startTag;

    return false;
}

bool GeoLoadXml::load(QIODevice& io)
{
    if (!openReader(io))
        return false;

    m_foundTopTag = false;

    parseKeys([this]() {
        if (xml.name() == topTag()) parseXml();
        else xml.skipCurrentElement();
    });

    if (xml.error() == QXmlStreamReader::NoError) {
        if (!m_foundTopTag)
            xml.raiseError(QObject::tr("No %1 element found in file").arg(topTag()));
    }

    if (xml.hasError()) {
        geoLoad.m_errorString = xml.errorString();
        return false;
    }

    return true;
}

bool GeoLoadXml::openReader(QIODevice& io)
{
    geoLoad.m_errorString.clear();

    if (!io.isOpen() && !io.open(openMode()))
        return false;

    if (!io.isSequential())
        io.seek(0); // start at beginnings

    xml.setDevice(&io);

    if (xml.error() != QXmlStreamReader::NoError) {
        geoLoad.m_errorString = xml.errorString();
        return false;
    }

    return true;
}

QColor GeoLoadXml::nameToColor(const QString& name) const
{
    if (const auto entry = ColorNameMap.constFind(name); entry != ColorNameMap.constEnd())
        return entry.value();

    return QColor();
}

bool GeoSaveXml::save(QIODevice& io)
{
    if (!openWriter(io))
        return false;

    xml.writeStartDocument();
    saveXml();
    xml.writeEndDocument();

    return !xml.hasError();
}

bool GeoSaveXml::openWriter(QIODevice& io)
{
    geoSave.m_errorString.clear();

    if (!io.isOpen() && !io.open(openMode() | QIODevice::Truncate))
        return false;
    
    xml.setDevice(&io);

    if (xml.hasError()) {
        geoSave.m_errorString = io.errorString();
        return false;
    }

    // Set formatting
    xml.setAutoFormatting(geoSave().hasFeature(GeoIoFeature::Formatted));
    xml.setAutoFormattingIndent(geoSave().m_indentLevel *
                               (geoSave().hasFeature(GeoIoFeature::Spaces) ? 1 : -1));

    return true;
}

QString GeoSaveXml::colorToName(const QColor& color) const
{
    if (!color.isValid())
        return QString();

    // Meh, just iterate through; there aren't very many anyway, and this
    // is per track, not per point.
    for (auto it = ColorNameMap.begin(); it != ColorNameMap.end(); ++it)
        if (it.value() == color)
            return it.key();

    return QString();
}
