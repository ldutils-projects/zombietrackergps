/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "src/core/trackmodel.h"
#include "geoio.h"
#include "geoionative.h"

const QString GeoLoadNative::nameStatic = "ZTGPS";

namespace {
    const quint32 NativeMagic   = 0xd9baf758;
    const quint32 NativeVersion = 0x1002;
} // anonymous namespace

GeoLoadNative::GeoLoadNative(GeoLoad& geoLoad) :
    GeoLoadBase(geoLoad)
{
    if (geoLoad.m_mainWindow != nullptr)
        connect(&geoLoad.trkModel(), &TrackModel::itemLoaded, this, &GeoLoadNative::itemLoaded);
}

bool GeoLoadNative::load(QIODevice& io)
{
    if (!openReader(io))
        return false;

    readStream >> geoLoad.trkModel();

    return readStream.error() == VersionedStream::NoError;
}

bool GeoLoadNative::is(QIODevice& io) const
{
    return GeoLoadNative(geoLoad).openReader(io);
}

bool GeoLoadNative::openReader(QIODevice& io)
{
    geoLoad.m_errorString.clear();

    if (!readStream.openRead(io, NativeMagic, 0x1000, 0x1004)) {
        geoLoad.m_errorString = readStream.errorString();
        return false;
    }

    return true;
}

GeoSaveNative::GeoSaveNative(GeoSave& geoSave) :
    GeoSaveBase(geoSave)
{
    if (geoSave.m_mainWindow != nullptr)
        connect(&geoSave.trkModel(), &TrackModel::itemSaved, this, &GeoSaveNative::itemSaved);
}

bool GeoSaveNative::save(QIODevice& io)
{
    if (!openWriter(io))
        return false;

    // TODO: we should abide the selection list
    writeStream << geoSave.trkModel();

    return writeStream.error() == VersionedStream::NoError;
}

bool GeoSaveNative::openWriter(QIODevice& io)
{
    geoSave.m_errorString.clear();

    if (!writeStream.openWrite(io, NativeMagic, NativeVersion)) {
        geoSave.m_errorString = writeStream.errorString();
        return false;
    }

    return true;
}
