/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOIOPARAMS_H
#define GEOIOPARAMS_H

#include <QModelIndexList>
#include <QStringList>
#include <QColor>

#include "geoiodecls.h"

[[nodiscard]] bool HasFeature(GeoIoFeature f0, GeoIoFeature f1);

class GeoParamsBase {
public:
    explicit GeoParamsBase(GeoIoFeature         features,
                           const QString&       filterTrk,
                           const QString&       filterWpt,
                           Qt::CaseSensitivity  filterCase);

    [[nodiscard]] bool hasFeature(GeoIoFeature f) const { return HasFeature(m_feature, f); }

    GeoIoFeature        m_feature;      // trk/wpt/etc
    QString             m_filterTrk;    // track filter: only import matching tracks
    QString             m_filterWpt;    // waypoint filter: only import matching waypoints
    Qt::CaseSensitivity m_filterCase;   // filter case sensitivity

    GeoParamsBase(const GeoParamsBase&) = default;
    GeoParamsBase& operator=(const GeoParamsBase&) = default;
};

// Package of data to drive GeoLoad.  Produced by import dialog, and can be set up otherwise.
class GeoLoadParams final : public GeoParamsBase
{
public:
    explicit GeoLoadParams(GeoIoFeature         features     = GeoIoFeature::AllTypes | GeoIoFeature::AllAux,
                           const QStringList&   trkTags      = QStringList(),
                           const QStringList&   rteTags      = QStringList(),
                           const QStringList&   wptTags      = QStringList(),
                           const QColor&        trackColor   = QColor(),
                           bool                 deduplicate  = true,
                           const QString&       filterTrk    = QString(),
                           const QString&       filterWpt    = QString(),
                           Qt::CaseSensitivity  filterCase   = Qt::CaseInsensitive);

    QStringList         m_trkTags;      // track tags to apply
    QStringList         m_rteTags;      // route tags
    QStringList         m_wptTags;      // waypoint tags
    QColor              m_trackColor;   // track override color, if valid
    bool                m_deduplicate;  // true to deduplicate on import

    GeoLoadParams(const GeoLoadParams&) = default;
    GeoLoadParams& operator=(const GeoLoadParams&) = default;
};

class GeoSaveParams final : public GeoParamsBase
{
public:
    explicit GeoSaveParams(GeoFormat                                = GeoFormat::Unknown,
                           GeoIoFeature        features             = GeoIoFeature::AllTypes |
                                                                      GeoIoFeature::AllAux |
                                                                      GeoIoFeature::AllData,
                           int                 indentLevel          = 2,
                           const QString&      filterTrk            = QString(),
                           const QString&      filterWpt            = QString(),
                           Qt::CaseSensitivity filterCase           = Qt::CaseInsensitive);

    explicit GeoSaveParams(const QModelIndexList& trkSel, const QModelIndexList& wptSel);

    GeoFormat           m_format;
    int                 m_indentLevel;
    QModelIndexList     m_trkSelection;   // track selections to save
    QModelIndexList     m_wptSelection;   // waypoint selections to save

    GeoSaveParams(const GeoSaveParams&) = default;
    GeoSaveParams& operator=(const GeoSaveParams&) = default;
};

#endif // GEOIOPARAMS_H
