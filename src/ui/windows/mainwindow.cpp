/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <cassert>
#include <chrono>

#include <QGuiApplication>
#include <QDesktopServices>
#include <QMessageBox>
#include <QIcon>
#include <QAction>
#include <QMenu>
#include <QWhatsThis>
#include <QItemSelectionModel>
#include <QSignalBlocker>
#include <QStandardPaths>
#include <QMouseEvent>
#include <QMimeData>
#include <QFontMetrics>
#include <QDir>
#include <QFileInfoList>

#include <src/util/qtcompat.h>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "src/version.h"
#include <src/util/util.h>
#include <src/util/ui.h>
#include <src/util/resources.h>
#include <src/util/icons.h>
#include <src/util/posixexpander.h>
#include <src/util/versionedstream.h>
#include <src/undo/undowincfg.h>
#include <src/ui/widgets/multiicondelegate.h>
#include <src/ui/widgets/tabwidget.h>

#include "src/ui/panes/trackpane.h"
#include "src/ui/panes/waypointpane.h"
#include "src/ui/panes/pointpane.h"
#include "src/ui/panes/mappane.h"
#include "src/ui/panes/trackcmppane.h"
#include "src/undo/undoui.h"
#include "src/geo-io/geoio.h"
#include "src/core/pointmodel.h"
#include "src/core/personmodel.h"
#include "src/core/app.h"
#include "src/util/cmdline.h"
#include "src/gpsd/gpsd.h"

int MainWindow::ProgressHandler::nest = 0;
MainWindow* MainWindow::mainWindowStatic = nullptr;

const QString MainWindow::autoImportError = QObject::tr("AutoImport Error");

MainWindow::MainWindow(QWidget *parent, const CmdLine* cmdLineOverride) :
    MainWindowBase(::Apptitle, cmdLineOverride != nullptr ? *cmdLineOverride : app().cmdLine(), parent),
    initStatic(*this),
    ui(new Ui::MainWindow),
    m_appConfig(this),
    m_aboutDialog(this),
    m_docDialog(*this),
    m_exportOpts(this),
    m_importOpts(*this),
    m_areaDialog(*this),
    m_personDialog(*this),
    m_deviceDialog(*this),
    m_trackSimplifyDialog(this),
    m_gpsCaptureDialog(*this),
    m_mapDownloadDialog(*this),
    m_newWaypointDialog(*this),
    m_gpsdVersionDialog(*this),
    m_newPaneDialog(*this),
    m_newTrackDialog(*this),
    m_gotoLatLonDialog(*this),
    m_launchSplash(5, LogoHtml), // count of statuses we set
    m_autoImportTimer(this),
    m_autosaveTimer(this),
    m_statusBarMenu(tr("Configure Status Bar"))
{
    ui->setupUi(this);
    ui->centralWidget->layout()->addWidget(new TabWidget(*this));

    resize(1920, 1280);        // default window size

    setupSession();            // session management
    setupMenus();              // menu slot connections
    setupActionTooltips();     // from parent class: set WhatsThis to match ToolTip
    setupStatus();             // create status bar widgets
    setupSignals();            // connect signals/slots
    setupEnableTimer();        // enable timer
    setupAutosave();           // initialize timers
    setupAutoImportTimeout();  // handle auto-import timeouts of async commands
    setupAppConfig();          // application name, etc
    setupActionIcons();        // default icons if there's no theme icon
    Util::SetupWhatsThis(this);

    sessionRestore();          // restore any prior session
    checkGeoPolMgr();          // verify and wait for geopol data load

    setupDefaultPanels();      // setup default panel config (after loading session)
    recentSessionsChanged();   // can't be invoked from parent constructor since we change our own UI
    setMapMode(MapMode::Move); // set default map mode
    updateActions();           // configure initial enable status

    m_launchSplash.finished(); // done with the launch splash
}

MainWindow::~MainWindow()
{
    // Only do this under the test suite, lest we close other windows before saving
    if (appBase().testing())
        close();   // test suite doesn't send close event

    // Kill any running auto-import process
    autoImportAsyncKill();

    cleanup(); // things to do before removing the GUI.

    delete ui;
}

MainWindow::InitStatic::InitStatic(MainWindow& mainWindow)
{
    if (mainWindowStatic == nullptr)
        mainWindowStatic = &mainWindow;   // avoid passing singleton through N layers
}

MainWindow::InitStatic::~InitStatic()
{
    // There should be no downstream uses of this in real code. Test suite has some unusual patterns.
    mainWindowStatic = nullptr;
}

void MainWindow::openAppConfig(AppConfig::Page page)
{
    m_appConfig.showPage(page);
}

void MainWindow::setupSession() const
{
    QGuiApplication::setFallbackSessionManagementEnabled(false);
    connect(qApp, &QApplication::commitDataRequest, this, &MainWindow::commitData);
}

void MainWindow::checkGeoPolMgr()
{
    m_launchSplash.setStatus(tr("Loading geopolitical data..."));
    app().geoPolMgr().finishLoad();
}

// Handle mouse forward/back button traveral through view undo stacks
void MainWindow::mouseReleaseEvent(QMouseEvent* event)
{
    if (event->button() == Qt::BackButton)
        on_action_Undo_View_triggered();
    else if (event->button() == Qt::ForwardButton)
        on_action_Redo_View_triggered();
    else
        MainWindowBase::mouseReleaseEvent(event);
}

void MainWindow::dragEnterEvent(QDragEnterEvent* event)
{
    // Drop from external files (e.g, from a file browser)
    if (event->mimeData()->hasUrls()) {
        // For testing to see if files are of formats we can load
        const GeoLoad loader(this, app().trackModel(), app().waypointModel());

        for (const auto& url : event->mimeData()->urls())
            if (loader.format(url.toLocalFile()) == GeoFormat::Unknown) // don't allow drop if any files are of unknown format
                return MainWindowBase::dragEnterEvent(event);

        event->acceptProposedAction(); // if all files were of recognized type, allow the drop
    }

    MainWindowBase::dragEnterEvent(event);
}

void MainWindow::dropEvent(QDropEvent* event)
{
    // Drop external files (e.g, from a file browser)
    if (event->mimeData()->hasUrls()) {
        importTracks(event->mimeData()->urls(), true, true);
        event->accept();
        return;
    }

    MainWindowBase::dropEvent(event);
}

QWidget* MainWindow::paneFactory(PaneClass_t paneClass) const
{
    if (paneClass == int(PaneClass::Group))
        return containerFactory();

    if (paneClass < int(PaneClass::_First))
        paneClass = PaneClass_t(int(PaneClass::Empty));

    return Pane::factory<QWidget>(PaneClass(int(paneClass)), const_cast<MainWindow&>(*this));
}

// Default panel config
void MainWindow::setupDefaultPanels()
{
    TabWidget* tabs = mainWindowTabs();

    const QSignalBlocker bts(tabs);

    m_launchSplash.setStatus(tr("Creating default interface..."));

    if (tabs->count() > 1)  // skip if the session loaded panels already.
        return;

    auto* mapPane    = paneFactory(PaneClass::Map);
    auto* filterPane = paneFactory(PaneClass::Filter);
    auto* viewPane   = paneFactory(PaneClass::View);
    auto* pointPane  = paneFactory(PaneClass::Points);
    auto* trackPane  = paneFactory(PaneClass::Track);
    auto* lineChart  = paneFactory(PaneClass::LineChart);
    auto* cmpChart   = paneFactory<TrackCmpPane>(PaneClass::CmpChart);
    auto* vGroupL    = containerFactory();
    auto* vGroupM    = containerFactory();
    auto* hGroupB    = containerFactory();
    auto* hGroupC    = containerFactory();

    // Set up some init-time defaults
    cmpChart->setBarWidth(12);
    cmpChart->setBarValuesShown(false);

    // Toggle UI bar off for some of these (different from the new-pane default)
    for (PaneBase* pane : QList<PaneBase*>({ viewPane, filterPane, pointPane, lineChart, cmpChart }))
        pane->paneToggled(false);

    QTimer::singleShot(0, this, &MainWindow::enablePaneChildren); // see comment above the function

    vGroupL->setOrientation(Qt::Vertical);
    vGroupM->setOrientation(Qt::Vertical);
    hGroupB->setOrientation(Qt::Horizontal);
    hGroupC->setOrientation(Qt::Horizontal);

    // VGroup: LHS vertical
    addPane(filterPane, vGroupL);
    addPane(viewPane,   vGroupL);
    addPane(pointPane,  vGroupL);

    // Hgroup: LHS vertical group + map
    addPane(vGroupL,    hGroupC, 0);
    addPane(mapPane,    hGroupC);

    // HGroup: Bottom horizontal
    addPane(trackPane,  hGroupB);
    addPane(cmpChart,   hGroupB);
    addPane(lineChart,  hGroupB);

    // VGroup: main tab VGroup
    addPane(hGroupC, vGroupM, 0);
    addPane(hGroupB, vGroupM, 1);

    tabs->addTab("Main", vGroupM);

    Util::SetFocus(mapPane);

    tabs->setCurrentIndex(0);

    resizeColumnsAllPanes();

    hGroupC->setSizes({ 200, 800 });
    hGroupC->setStretchFactor(0, 20);
    hGroupC->setStretchFactor(1, 80);

    hGroupB->setSizes({ 600, 200, 300 });
    hGroupB->setStretchFactor(0, 60);
    hGroupB->setStretchFactor(1, 20);
    hGroupB->setStretchFactor(2, 30);

    vGroupM->setSizes({ 750, 250 });
    vGroupM->setStretchFactor(0, 75);
    vGroupM->setStretchFactor(1, 25);
}

// Example default queries, intended to be updated/replaced/etc by user.
void MainWindow::setupDefaultFilters()
{
    FilterModel& model = app().filterModel();

    if (model.rowCount() > 0)
        return;

    model.appendRow({ "Bike",     "Tags =~ Commute|Cross|Mountain|Road|TT|Gravel|WinterBike" });
    model.appendRow({ "Foot",     "Tags =~ Hike|Run" });
    model.appendRow({ "Water",    "Tags =~ Canoe|Sail|Swim" });
    model.appendRow({ "Internet", "Tags =~ Internet" });

    const QModelIndex bike  = model.index(0, 0);
    const QModelIndex foot  = model.index(1, 0);
    const QModelIndex water = model.index(2, 0);
    const QModelIndex inet  = model.index(3, 0);

    model.appendRow({ "Big Climbs", "Ascent > 500 m" }, bike);
    const QModelIndex climbs = model.index(0, 0, bike);

    model.setSiblingIcon(FilterModel::Icon, bike,   ":art/tags/Transport/Road.svg");
    model.setSiblingIcon(FilterModel::Icon, foot,   ":art/tags/Transport/Run.svg");
    model.setSiblingIcon(FilterModel::Icon, water,  ":art/tags/Transport/Sail.svg");
    model.setSiblingIcon(FilterModel::Icon, climbs, ":art/tags/Misc/HillUp.svg");
    model.setSiblingIcon(FilterModel::Icon, inet,   ":art/tags/Misc/Internet.svg");
}

// Example view presets, intended to be updated/replaced/etc by user.
void MainWindow::setupDefaultViewPresets()
{
    ViewModel& model = app().viewModel();

    if (model.rowCount() > 0)
        return;

    model.appendRow({ "Canada", 57.98525257396584, -93.8615056166874, 0.0, 1525 });
    model.appendRow({ "France", 46.69553533747557, 2.486498135897202, 0.0, 1660 });
    model.appendRow({ "USA", 40.212622574023236, -98.23197951780352, 0.0, 1525 });

    const QModelIndex canada = model.index(0, 0);
    const QModelIndex france = model.index(1, 0);
    const QModelIndex usa    = model.index(2, 0);

    model.appendRow({ "Minnesota", 46.15787512003232, -92.86976379630738, 0.0, 1810 }, usa);
    model.appendRow({ "Washington", 46.92173418716036, -122.58105221389884, 0.0, 1950 }, usa);
    model.appendRow({ "Normandie", 49.26, -0.19, 0.0, 2200 }, france);

    const QModelIndex MN = model.index(0, 0, usa);
    const QModelIndex WA = model.index(1, 0, usa);
    const QModelIndex ND = model.index(0, 0, france);

    model.setSiblingIcon(ViewModel::Icon,   canada, ":art/tags/Flags/Countries/Canada.jpg");
    model.setSiblingIcon(ViewModel::Icon,   france, ":art/tags/Flags/Countries/France.jpg");
    model.setSiblingIcon(  ViewModel::Icon, ND,     ":art/tags/Flags/Regions/France/Normandie.jpg");
    model.setSiblingIcon(ViewModel::Icon,   usa,    ":art/tags/Flags/Countries/United_States.jpg");
    model.setSiblingIcon(  ViewModel::Icon, MN,     ":art/tags/Flags/Regions/United_States/Minnesota.jpg");
    model.setSiblingIcon(  ViewModel::Icon, WA,     ":art/tags/Flags/Regions/United_States/Washington.jpg");
}

void MainWindow::setupProgress()
{
    // Add progress bar
    m_progressBar.setOrientation(Qt::Horizontal);
    m_progressBar.setMaximumWidth(600);
    finishProgress();
    statusBar()->addPermanentWidget(&m_progressBar, 10);
}

void MainWindow::setupStatus()
{
    // Sized selected by trial and error to look decent.
    const int iconHeight = QFontMetrics(statusBar()->font()).height();
    const int iconWidth  = QFontMetrics(statusBar()->font()).boundingRect('M').width() * 25 / 10;

    // Icons for the status bar
    const QMap<Stat, const char*> icons = {
        { Stat::Duration, "clock" },
        { Stat::Length,   "ruler" },
    };

    // Build vectors to status bar widgets.  The content of each vector can be shown/hidden with the
    // status bar context menu.
    for (Stat stat = Stat::_First; stat != Stat::_Count; Util::inc(stat)) {
        auto& statVec = statWidget(stat); // vector of status widgets

        if (stat != Stat::Offline && stat != Stat::MapMode)
            statVec.append(new QLabel(s_statSeparator)); // must be a separate label, since it isn't colorized.

        switch (stat) {
        case Stat::Flags:
            statVec.append(new QWidget());
            statVec.back()->setLayout(new QHBoxLayout());
            break;
        default:
            // Add icon if we have one.
            if (const auto iconName = icons.find(stat); iconName != icons.end()) {
                const QIcon icon = Icons::get(iconName.value());
                auto* label = new QLabel();
                const QSize actualSize = icon.actualSize(QSize(iconWidth, iconHeight));
                label->setPixmap(icon.pixmap(actualSize));
                statVec.append(label);
            }

            statVec.append(new QLabel());
            break;
        }

        // Add each widget in the vector into the status bar
        for (auto& w : statVec)
            statusBar()->addWidget(w);
    }

    connect(statusBar(), &QStatusBar::messageChanged, this, &MainWindow::statusChanged);
    setupStatusMenus();  // setup context menus for status
    setupProgress();     // attach progress bar to status
}

auto MainWindow::statActions() const
{
    auto actions = m_statusBarMenu.actions();

    if (!actions.isEmpty())
        actions.pop_front(); // pop off the header, and only return the actions of interest.

    return actions;
}

// Show or hide a stat from the status bar
void MainWindow::setVisible(Stat stat, bool visible, bool setDirty)
{
    const auto actions = statActions();

    // Safety.  Shouldn't happen, unless someone manually edits the .conf file.
    if (stat >= Stat::_Count || int(stat) >= actions.size())
        return;

    // Update the context menu item (although it probably already is, if this is due to a user action).
    // However we don't get those for API setting, because the QMenu only has a triggered signal, not toggled.
    actions.at(int(stat))->setChecked(visible);

    for (auto* w : statWidget(stat))
        w->setVisible(visible);

    // Update separator bar visibility to make sense - e.g, not on first visible item.
    const bool flagVisible = actions.at(int(Stat::Flags))->isChecked();
    bool anyPriorVisible = false;

    for (Stat other = Stat::_First; other != Stat::_Count; Util::inc(other)) {
        const auto& widgetVec = statWidget(other);
        if (widgetVec.size() > 1) {
            const bool thisVisible = actions.at(int(other))->isChecked();

            // Don't separate flag from geocoords visually
            if (other == Stat::GeoLocation)
                widgetVec.first()->setVisible(anyPriorVisible && thisVisible && !flagVisible);
            else
                widgetVec.first()->setVisible(anyPriorVisible && thisVisible);
            anyPriorVisible |= thisVisible;
        }
    }

    if (setDirty)
        dirtyStateChanged(true);
}

bool MainWindow::isVisible(Stat stat) const
{
    const auto actions = statActions();

    // Safety.  Shouldn't happen, unless someone manually edits the .conf file.
    if (stat >= Stat::_Count || int(stat) >= actions.size())
        return false;

    return actions.at(int(stat))->isChecked();
}

// Show the status bar context menu
void MainWindow::showStatusBarContextMenu(const QPoint& pos)
{
    m_statusBarMenu.exec(statusBar()->mapToGlobal(pos));
}

// Make show/hide menu for undos
QString MainWindow::showHideMsg(bool checked, QString name)
{
    // Remove any existing "Show..." prefix to avoid "Show Show".
    if (name.indexOf(tr("Show")) < name.indexOf(' '))
        name = name.mid(name.indexOf(' ') + 1);

    return (checked ? tr("Show") : tr("Hide")) + " " + name;
}

// This happens when a status bar context menu action is triggered.
void MainWindow::statusBarItemToggled(QAction* action)
{
    if (const QVariant statVar = action->property(s_actionPropertyStat); statVar.isValid() && statVar.type() == QVariant::Int) {
        const bool checked = action->isChecked();

        const UndoMgr::ScopedUndo undoSet(app().undoMgr(), showHideMsg(checked, action->text()));
        app().undoMgr().add(new UndoUiStat(*this, Stat(statVar.toInt())));

        setVisible(Stat(statVar.toInt()), checked);
    }
}

// Setup status bar context menus
void MainWindow::setupStatusMenus()
{
    static const QMap<Stat, QString> m_statName =
    {
        { Stat::Offline,      tr("Online") },
        { Stat::MapMode,      tr("Map Mode") },
        { Stat::Capture,      tr("Live Capture") },
        { Stat::Total,        tr("Total Tracks") },
        { Stat::Visible,      tr("Visible Tracks") },
        { Stat::Selected,     tr("Selected Tracks") },
        { Stat::Length,       tr("Length") },
        { Stat::Duration,     tr("Duration") },
        { Stat::Ascent,       tr("Ascent") },
        { Stat::Descent,      tr("Descent") },
        { Stat::Flags,        tr("Flags") },
        { Stat::GeoLocation,  tr("Geo-Location") },
        { Stat::GeoPolId,     tr("Geo ID") },
        { Stat::GeoPolName,   tr("Region Name") },
    };

    assert(int(Stat::_Count) == m_statName.count());

    const auto newAction = [&](Stat stat) {
        assert(!m_statName.value(stat).isEmpty());

        const QString text(tr("Show ") + m_statName.value(stat)); // menu item text
        const QIcon icon = QIcon::fromTheme("configure");

        if (auto* action = new QAction(icon, text, this)) {
            action->setCheckable(true);
            action->setChecked(true);
            action->setProperty(s_actionPropertyStat, int(stat)); // so we can find it later
            // If GPSD support was not compiled in, disabled this one.
            action->setEnabled(Gpsd::gpsdSupported() || stat != Stat::Capture);

            m_statusBarMenu.addAction(action);
        }
    };

    m_statusBarMenu.addSection(m_statusBarMenu.title());

    // Add each action to the status bar context menu.
    for (Stat stat = Stat::_First; stat != Stat::_Count; Util::inc(stat))
        newAction(stat);

    // Hide by default
    setVisible(Stat::GeoPolName, false, false);

    statusBar()->setContextMenuPolicy(Qt::CustomContextMenu);

    // Context menu signal.  Unfortunately it only has a triggered signal, not toggled, so we only
    // get these events for user input, not API usage.  Accordingly, setStatVisible updates
    // the action state as well.
    connect(&m_statusBarMenu, &QMenu::triggered, this, &MainWindow::statusBarItemToggled);

    // Status bar context menu request
    connect(statusBar(), &QStatusBar::customContextMenuRequested, this, &MainWindow::showStatusBarContextMenu);
}

// Tedious: fix actions which didn't find icons in any default theme.  See comment in Icons get() method
// for why this appears necessary.
void MainWindow::setupActionIcons()
{
    m_launchSplash.setStatus(tr("Loading default icons..."));

    Icons::defaultIcon(ui->action_About_Qt,                "system-help");
    Icons::defaultIcon(ui->action_About_ZT,                "help-about");
    Icons::defaultIcon(ui->action_Add_Points_Mode,         "cross-shape");
    Icons::defaultIcon(ui->action_Configure,               "configure");
    Icons::defaultIcon(ui->action_Copy_Selected,           "edit-copy");
    Icons::defaultIcon(ui->action_Create_New_Track,        "createpath");
    Icons::defaultIcon(ui->action_Delete_Selection,        "delete");
    Icons::defaultIcon(ui->action_Donate,                  "help-donate");
    Icons::defaultIcon(ui->action_Download_Region,         "download");
    Icons::defaultIcon(ui->action_Duplicate_Selection,     "edit-duplicate");
    Icons::defaultIcon(ui->action_Enlarge_Font,            "format-font-size-more");
    Icons::defaultIcon(ui->action_Live_GPSD_Capture,       "kstars_satellites");
    Icons::defaultIcon(ui->action_Export_Track,            "document-save");
    Icons::defaultIcon(ui->action_Import_Track,            "document-open");
    Icons::defaultIcon(ui->action_Import_from_Device,      "kstars_satellites");
    Icons::defaultIcon(ui->action_Goto_Lat_Lon,            "globe");
    Icons::defaultIcon(ui->action_Map_Move_Mode,           "transform-move");
    Icons::defaultIcon(ui->action_Merge_Selection,         "merge");
    Icons::defaultIcon(ui->action_New_Window,              "window-new");
    Icons::defaultIcon(ui->menu_Add_Pane,                  "window-new");
    Icons::defaultIcon(ui->menu_Add_Group,                 "window-new");
    Icons::defaultIcon(ui->menu_Replace_Pane,              "window-duplicate");
    Icons::defaultIcon(ui->action_Offline_Mode,            "offline");
    Icons::defaultIcon(ui->action_Open_Settings,           "document-open");
    Icons::defaultIcon(ui->action_Pane_Balance_Siblings,   "object-columns");
    Icons::defaultIcon(ui->action_Pane_Close,              "tab-close");
    Icons::defaultIcon(ui->action_Pane_Dialog,             "window-new");
    Icons::defaultIcon(ui->action_Pane_Group_Left,         "go-previous");
    Icons::defaultIcon(ui->action_Pane_Group_Right,        "go-next");
    Icons::defaultIcon(ui->action_Pane_Left,               "go-previous");
    Icons::defaultIcon(ui->action_Pane_Right,              "go-next");
    Icons::defaultIcon(ui->action_Pane_Split_Horizontal,   "view-split-left-right");
    Icons::defaultIcon(ui->action_Pane_Split_Vertical,     "view-split-top-bottom");
    Icons::defaultIcon(ui->action_Quit,                    "application-exit");
    Icons::defaultIcon(ui->action_Quit_without_Save,       "application-exit");
    Icons::defaultIcon(ui->action_Reload_Visible_Area,     "view-refresh");
    Icons::defaultIcon(ui->action_Reset_Font,              "application-x-font-ttf");
    Icons::defaultIcon(ui->action_Reset_Ui,                "document-new");
    Icons::defaultIcon(ui->action_Revert,                  "document-revert");
    Icons::defaultIcon(ui->action_Save_Settings,           "document-save");
    Icons::defaultIcon(ui->action_Save_Settings_As,        "document-save-as");
    Icons::defaultIcon(ui->action_Select_Person,           "system-switch-user");
    Icons::defaultIcon(ui->action_Select_Points_Mode,      "edit-select");
    Icons::defaultIcon(ui->action_Show_Filters,            "view-filter");
    Icons::defaultIcon(ui->action_Show_Statusbar,          "kt-show-statusbar");
    Icons::defaultIcon(ui->menu_Show_Toolbar,              "configure-toolbars");
    Icons::defaultIcon(ui->action_Show_Tutorial,           "documentation");
    Icons::defaultIcon(ui->action_Shrink_Font,             "format-font-size-less");
    Icons::defaultIcon(ui->action_Supported_GPSD_Versions, "kstars_satellites");
    Icons::defaultIcon(ui->action_Size_Cols,               "zoom-fit-best");
    Icons::defaultIcon(ui->action_View_AsTree,             "view-list-tree");
    Icons::defaultIcon(ui->action_View_Collapse_All,       "format-indent-less");
    Icons::defaultIcon(ui->action_View_Expand_All,         "format-indent-more");
    Icons::defaultIcon(ui->action_View_Select_All,         "edit-select-all");
    Icons::defaultIcon(ui->action_View_Select_None,        "edit-select-none");
    Icons::defaultIcon(ui->action_View_Show_All,           "view-restore");
    Icons::defaultIcon(ui->action_What_is,                 "help-whatsthis");
    Icons::defaultIcon(ui->action_Zoom_To_Selection,       "zoom-fit-selection");
    Icons::defaultIcon(ui->action_Undo,                    "edit-undo");
    Icons::defaultIcon(ui->action_Redo,                    "edit-redo");
    Icons::defaultIcon(ui->action_Clear_Undo,              "edit-clear");
    Icons::defaultIcon(ui->action_Undo_View,               "zoom-previous");
    Icons::defaultIcon(ui->action_Redo_View,               "zoom-next");
    Icons::defaultIcon(ui->action_Clear_Undo,              "edit-clear");
    Icons::defaultIcon(ui->action_Simplify_Selection,      "path-simplify");
    Icons::defaultIcon(ui->action_Reverse_Selection,       "path-reverse");
    Icons::defaultIcon(ui->action_Unset_Speed,             "speedometer");
    Icons::defaultIcon(ui->action_Visit_ZombieTrackerGPS_Web_Site, "viewhtml");
    Icons::defaultIcon(ui->action_Auto_Import,             "document-import");
}

void MainWindow::setupMenus()
{
    const auto newAction = [&](QList<QAction*>& actions, PaneClass pc) {
        if (auto* action = new QAction(Icons::get(Pane::iconFile(pc)), Pane::name(pc), this)) {
            action->setToolTip(Pane::tooltip(pc));
            action->setWhatsThis(Pane::tooltip(pc));
            action->setStatusTip(Pane::tooltip(pc));
            // Disable capture pane if GPSD support not compiled in
            action->setEnabled(Gpsd::gpsdSupported() || pc != PaneClass::GpsCapture);

            actions.push_back(action);
        }
    };

    // Create actions for adding all the kinds of panes we have.
    for (PaneClass pc = PaneClass::_First; pc < PaneClass::_Count; Util::inc(pc)) {
        newAction(paneClassAddActions(), pc);
        newAction(paneClassGrpActions(), pc);
        newAction(paneClassRepActions(), pc);
        newAction(paneClassWinActions(), pc);
    }

    ui->menuPane->addSeparator();
    ui->menuPane->addActions({ getPaneAction(PaneAction::PaneBalanceTab),
                               getPaneAction(PaneAction::PaneAddTab),
                               getPaneAction(PaneAction::PaneRenameTab),
                               getPaneAction(PaneAction::PaneCloseTab) });

    ui->menu_Add_Pane->addActions(paneClassAddActions());
    connect(ui->menu_Add_Pane, &QMenu::triggered, this,
            static_cast<void(MainWindow::*)(QAction*)>(&MainWindow::addPaneAction));

    ui->menu_Add_Group->addActions(paneClassGrpActions());
    connect(ui->menu_Add_Group, &QMenu::triggered, this,
            static_cast<void(MainWindow::*)(QAction*)>(&MainWindow::addGroupAction));

    ui->menu_Replace_Pane->addActions(paneClassRepActions());
    connect(ui->menu_Replace_Pane, &QMenu::triggered, this,
            static_cast<void(MainWindow::*)(QAction*)>(&MainWindow::replacePaneAction));

    ui->menu_Pane_in_Window->addActions(paneClassWinActions());
    connect(ui->menu_Pane_in_Window, &QMenu::triggered, this,
            static_cast<void(MainWindow::*)(QAction*)>(&MainWindow::paneInWindowAction));

    // menu connections
    connect(ui->action_About_Qt, &QAction::triggered, QCoreApplication::instance(), &QApplication::aboutQt);

    connect(ui->action_View_AsTree, &QAction::triggered, this, &MainWindow::viewAsTree);
    connect(ui->action_What_is, &QAction::triggered, this, &QWhatsThis::enterWhatsThisMode);

    // Recent sessions menu
    connect(ui->menu_Recent_Sessions, &QMenu::triggered, this, &MainWindow::loadRecentSession);

    // Toolbar menu
    for (CheckableMenu tb = CheckableMenu::_ToolBarBegin; tb < CheckableMenu::_ToolBarEnd; Util::inc(tb)) {
        QAction* action = getToolBar(tb)->toggleViewAction();
        Icons::defaultIcon(action, "configure-toolbars");
        ui->menu_Show_Toolbar->addAction(action);
        connect(action, &QAction::triggered, this, [this, tb](bool checked) { toolBarToggled(tb, checked); });
    }

    // defaults
    ui->action_Show_Statusbar->setChecked(true);
}

void MainWindow::setupActionTooltips()
{
    MainWindowBase::setupActionTooltips();
}

void MainWindow::recentSessionsChanged()
{
    if (ui == nullptr)
        return;

    return recentSessionsChanged(ui->menu_Recent_Sessions);
}

void MainWindow::sampleSession()
{
    setupDefaultFilters();     // default track filters
    setupDefaultViewPresets(); // default view presets

    const QString sampleData = QStandardPaths::locate(QStandardPaths::AppDataLocation, "sample/mystic_basin_trail.gpx");
    bool loadOK = false;
    if (!sampleData.isEmpty()) {
        GeoLoad loader(this, app().trackModel(), app().waypointModel(),
                       GeoLoadParams(GeoIoFeature::Trk | GeoIoFeature::AuxTrk,
                                     {"Road", "Recreation"},           // track tags
                                     {ImportDialog::defaultRouteTag},  // route tags
                                     { },                              // waypoint tags
                                     QColor(), true));
        loadOK = loader.load(sampleData) && (app().trackModel().rowCount() > 0);

        if (loadOK) {
            runOnPanels([](PaneBase* pane) { pane->expandAll(); });
            (void)postImport(loader, { loadOK }, false, false); // we checked RC above.

            if (auto* trackPane = findPane<TrackPane>(); trackPane != nullptr) {
                trackPane->select(app().trackModel().index(0, 0),
                                  QItemSelectionModel::Clear | QItemSelectionModel::SelectCurrent | QItemSelectionModel::Rows);
                trackPane->zoomToSelection();
                Util::SetFocus(trackPane);
            }
        }
    }

    if (!loadOK)
        error(tr("Unable to import sample data.  Please verify the program installation."), tr("Import Sample Data"));
}

void MainWindow::firstRunHook()
{
    MainWindowBase::firstRunHook();

    // Create default configuration
    cfgDataWritable().setupDefaultConfig();

    // Ask user about loading sample data on first run.
    // TODO: for some reason, supplying the main window as the QMessageBox parent puts the message box on
    // the wrong screen in multi-monitor setups.  Unclear why.  Use nullptr instead, which centers it on
    // the primary display.
    const QMessageBox::StandardButton result =
            QMessageBox::question(nullptr, tr("Import Sample Data?"),
                                  tr("This is the first time ") + QApplication::applicationDisplayName() +
                                  tr(" has been run with this profile.  You can import some sample data to "
                                     "experiment with, or start with a clean slate.  Would you like to import "
                                     "the sample data?"),
                                  QMessageBox::Yes | QMessageBox::No,
                                  QMessageBox::Yes);

    if (result == QMessageBox::Yes)
        sampleSession();

    m_docDialog.show();  // show intro dialog
}

void MainWindow::commitData(QSessionManager& /*manager*/)
{
    sessionSave();
}

void MainWindow::updateActions()
{
    MainWindowBase::updateActions();

    const PaneBase*  focused       = focusedPane();
    const auto*      focusedDcp    = dynamic_cast<const DataColumnPane*>(focused);
    const MapPane*   lastMapPane   = findPane<MapPane>();
    const TrackPane* lastTrackPane = findPane<TrackPane>();

    const bool hasFocused      = (focused != nullptr);
    const bool hasSelection    = hasFocused && focused->hasSelection();
    const bool hasItems        = hasFocused && focused->hasItems();
    const bool hasSettingsFile = MainWindowBase::hasSettingsFile();
    const bool isOffline       = isOfflineMode();
    const bool hasDelete       = (focusedDcp != nullptr && focusedDcp->hasAction(MainAction::DeleteSelection));
    const bool hasZoom         = (focusedDcp != nullptr && focusedDcp->hasAction(MainAction::ZoomToSelection));
    const bool hasDuplicate    = (focusedDcp != nullptr && focusedDcp->hasAction(MainAction::DuplicateSelection));
    const bool hasSimplify     = (focusedDcp != nullptr && focusedDcp->hasAction(MainAction::SimplifySelection));
    const bool hasReverse      = (focusedDcp != nullptr && focusedDcp->hasAction(MainAction::ReverseSelection));
    const bool hasUnsetSpeed   = (focusedDcp != nullptr && focusedDcp->hasAction(MainAction::UnsetSpeed));
    const bool hasCurrentTrack = (lastTrackPane != nullptr && lastTrackPane->hasSelection());
    const bool hasAutoImport   = (cfgData().autoImportMode != CfgData::AutoImportMode::Disabled);
    const bool hasExport       = (app().trackModel().rowCount() > 0);

    ui->action_Add_Points_Mode->setEnabled(hasCurrentTrack);
    ui->action_Auto_Import->setEnabled(hasAutoImport);
    ui->action_Copy_Selected->setEnabled(hasSelection);
    ui->action_Delete_Selection->setEnabled(hasDelete && hasSelection);
    ui->action_Duplicate_Selection->setEnabled(hasDuplicate && hasSelection);
    ui->action_Export_Track->setEnabled(hasExport);
    ui->action_Live_GPSD_Capture->setEnabled(Gpsd::gpsdSupported());
    ui->action_Reload_Visible_Area->setEnabled(!isOffline);
    ui->action_Reverse_Selection->setEnabled(hasReverse && hasCurrentTrack);
    ui->action_Revert->setEnabled(hasSettingsFile);
    ui->action_Save_Settings->setEnabled(hasSettingsFile && !m_privateSession);
    ui->action_Save_Settings_As->setEnabled(!m_privateSession);
    ui->action_Select_Points_Mode->setEnabled(hasCurrentTrack);
    ui->action_Simplify_Selection->setEnabled(hasSimplify && hasSelection);
    ui->action_Size_Cols->setEnabled(hasItems);
    ui->action_View_Collapse_All->setEnabled(hasItems);
    ui->action_View_Expand_All->setEnabled(hasItems);
    ui->action_View_Select_All->setEnabled(hasItems);
    ui->action_View_Select_None->setEnabled(hasItems);
    ui->action_View_Show_All->setEnabled(hasItems);
    ui->action_Unset_Speed->setEnabled(hasUnsetSpeed && hasSelection);
    ui->action_Zoom_To_Selection->setEnabled(hasZoom && hasSelection);

    updateUndoActions(&app().undoMgr(), ui->action_Undo, ui->action_Redo, ui->action_Clear_Undo);
    updateUndoActions(lastMapPane != nullptr ? &lastMapPane->undoMgrView() : nullptr,
                      ui->action_Undo_View, ui->action_Redo_View);

    m_newPaneDialog.updateActions();

    // Kick off timer to perform slow actions, to avoid spamming it on each selection change.
    // Only worry about that if there is a selection.
    using namespace std::chrono_literals;
    m_actionUpdateTimer.start(hasSelection ? 250ms : 0ms);
}

// Handle actions which are a little expensive to update. E.g, if someone highlights 1000's of
// points in a PointPane, we must traverse the list to see if they're all segments or all points.
// We don't want to do that every single time the selection changes. Doing it incrementally
// would be better, but also a slight PITA, so this is a compromise. To do it better, see
// selectionChanged() and the selected/deselected sets, but it'll require tracking point and
// segment counts.
void MainWindow::updateActionsDeferred()
{
    const auto* focused = dynamic_cast<const DataColumnPane*>(focusedPane());

    const bool hasMerge = (focused != nullptr && focused->hasAction(MainAction::MergeSelection));

    ui->action_Merge_Selection->setEnabled(hasMerge);
}

// Let multiple consumers get this from a single place, rather than N different panes.
void MainWindow::selectionChanged(const QItemSelectionModel* selector,
                                  const QItemSelection& selected, const QItemSelection& deselected)
{
    if (ui == nullptr)
        return;

    if (selector != nullptr) {
        const QAbstractItemModel* model = Util::MapDown(selector->model());

        if (dynamic_cast<const PointModel*>(model) != nullptr)
            emit selectedPointsChanged(selector, selected, deselected);

        if (qobject_cast<const TrackModel*>(model) != nullptr)
            emit selectedTracksChanged(selector, selected, deselected);

        if (qobject_cast<const WaypointModel*>(model) != nullptr)
            emit selectedWaypointsChanged(selector, selected, deselected);
    }

    updateActions();
}

// This is a funnel point for all panes to report changed events to.
// Consumers can then get the result from here, rather than N different places.
void MainWindow::currentChanged(const QModelIndex& current)
{
    const QModelIndex modelIdx = Util::MapDown(current); // emit in model space, not filter space

    // Deselection of on focus change or deletion of all model data.
    if (!current.isValid())
    {
        cfgDataWritable().pointColorizer.setModel(CfgData::emptyPointModel);
        return;
    }

    const auto* trackModel = qobject_cast<const TrackModel*>(modelIdx.model());
    if (trackModel != nullptr) {
        emit currentTrackChanged(modelIdx);
        emit currentTrackPointChanged(QModelIndex());

        // Update colorizer for new model.
        const PointModel* pointModel = trackModel->geoPoints(modelIdx);
        cfgDataWritable().pointColorizer.setModel(pointModel);

        app().climbModel().deferredUpdate(modelIdx);
    }

    if (dynamic_cast<const PointModel*>(modelIdx.model()) != nullptr)
        emit currentTrackPointChanged(modelIdx);

    if (qobject_cast<const WaypointModel*>(modelIdx.model()) != nullptr)
        emit currentWaypointChanged(modelIdx);
}

void MainWindow::setupSignals()
{
    MainWindowBase::setupSignals();

    // Undo signals update dirty flag, and actions.  The view undoer doesn't update dirty.
    connect(&app().undoMgr(), &UndoMgr::dirtyStateChanged, this, &MainWindow::dirtyStateChanged);

    connect(&app().undoMgr(), &UndoMgr::undoAdded, this,
            static_cast<void(MainWindow::*)(void)>(&MainWindow::updateActions));

    connect(&app().undoMgr(), &UndoMgr::changeApplied, this, &MainWindow::postUndoActions);

    connect(&app().waypointModel(), &WaypointModel::rowsInserted, this, &MainWindow::visibleWaypointsChanged);
    connect(&app().waypointModel(), &WaypointModel::rowsRemoved,  this, &MainWindow::visibleWaypointsChanged);
    connect(&app().waypointModel(), &WaypointModel::dataChanged,  this, &MainWindow::visibleWaypointsChanged);

    // Auto-import signalling, since this is asynchronous.
    connect(&m_autoImportProcess, &QProcess::started,       this, &MainWindow::autoImportAsyncStart);
    connect(&m_autoImportProcess, &QProcess::errorOccurred, this, &MainWindow::autoImportAsyncError);
    connect(&m_autoImportProcess, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
            this, &MainWindow::autoImportAsyncFinish);
}

void MainWindow::setupEnableTimer()
{
    m_actionUpdateTimer.setSingleShot(true);

    connect(&m_actionUpdateTimer, &QTimer::timeout, this, &MainWindow::updateActionsDeferred);
}

void MainWindow::setupAutosave()
{
    if (cfgData().dataAutosaveInterval > 0) {
        m_autosaveTimer.setSingleShot(true);
        connect(&m_autosaveTimer, &QTimer::timeout, this, &MainWindow::sessionSave, Qt::UniqueConnection);
    } else {
        disconnect(&m_autosaveTimer, &QTimer::timeout, this, &MainWindow::sessionSave);
    }
}

void MainWindow::setupAutoImportTimeout()
{
    m_autoImportTimer.setSingleShot(true);
    connect(&m_autoImportTimer, &QTimer::timeout, this, &MainWindow::autoImportAsyncTimeout);
}

void MainWindow::incCaptureCount()
{
    ++m_gpsCaptureCount;
    updateStatus();
}

void MainWindow::decCaptureCount()
{
    --m_gpsCaptureCount;
    updateStatus();
}

QIcon MainWindow::mapModeIcon() const
{
    switch (mapMode()) {
    case MapMode::Move:   return ui->action_Map_Move_Mode->icon();
    case MapMode::Add:    return ui->action_Add_Points_Mode->icon();
    case MapMode::Select: return ui->action_Select_Points_Mode->icon();
    default:              return { };
    }
}

void MainWindow::setStat(MainWindow::Stat stat, const QString& txt, const QByteArray& fontStyle, const QColor& color)
{
    if (stat >= Stat::_Count)  // Safety.  Shouldn't happen.
        return;

    // Search for label widget in the list of widgets for this status entry
    if (auto* label = dynamic_cast<QLabel*>(m_status.at(unsigned(stat)).back()); label != nullptr) {
        label->setText(txt);
        Util::SetWidgetStyle(label, color, fontStyle);
    }
}

void MainWindow::setStat(MainWindow::Stat stat, const QString& txt, const QByteArray& fontStyle)
{
    setStat(stat, txt, fontStyle, QGuiApplication::palette().color(QPalette::Text));
}

void MainWindow::setStat(MainWindow::Stat stat, const QIcon& icon)
{
    if (auto* label = dynamic_cast<QLabel*>(m_status.at(unsigned(stat)).back()); label != nullptr) {
        const int size = QFontMetrics(statusBar()->font()).height();
        label->setPixmap(icon.pixmap(size, size));
    }
}

void MainWindow::updateStatus()
{
    setStat(Stat::Offline,  isOfflineMode() ? "[OFFLINE]" : "[ONLINE]", "normal",
            isOfflineMode() ? cfgData().uiColor[UiType::Warning] : cfgData().uiColor[UiType::Success]);

    setStat(Stat::Capture, hasLiveCaptures() ? Icons::get("media-record")
                                             : QIcon());

    setStat(Stat::MapMode, mapModeIcon());
}

void MainWindow::updateStatus(const SelectionSummary& selectionSummary)
{
    updateStatus();

    setStat(Stat::Total,    QString("<b>Tot:</b> %1").arg(selectionSummary.total));
    setStat(Stat::Visible,  QString("<b>Vis:</b> %1").arg(selectionSummary.visible));
    setStat(Stat::Selected, QString("<b>Sel:</b> %1").arg(selectionSummary.selected));
    setStat(Stat::Length,   cfgData().unitsTrkLength(selectionSummary.length));
    setStat(Stat::Duration, cfgData().unitsDuration(selectionSummary.duration));
    setStat(Stat::Ascent,   QString("%1 %2").arg(QChar(0x25B2), cfgData().unitsClimb(selectionSummary.ascent)));
    setStat(Stat::Descent,  QString("%1 %2").arg(QChar(0x25BC), cfgData().unitsClimb(selectionSummary.descent)));
}

// There's a Qt bug somewhere: the status bar doesn't refill part of its background
// if we remove and add flags of different widths.  To work around that, we will
// paint the flags into identically sized rectangles.  It's more effort, but avoids
// the visal artifacts.
void MainWindow::updateStatBarGeoPol(const Marble::GeoDataCoordinates& coords)
{
    // If region has changed, update status bar flags
    GeoPolRegionVec geopol = app().geoPolMgr().intersections(coords, GeoPolRegion::WorldOpt::IncludeIfNoOther);

    // if nothing has changed, don't bother.
    if (geopol == m_mouseGeoPol)
        return;

    m_mouseGeoPol.swap(geopol);

    // Remove old flags
    Util::ClearLayout(statWidget(Stat::Flags).back()->layout());

    // add new flags
    QString nestedName;
    QString nestedId;
    for (const auto* region : m_mouseGeoPol) {
        // build up a name to display
        nestedName += (nestedName.isEmpty() ? "" : "/") + region->name();
        nestedId = region->isoA2();

        // Size it appropriately for the text height;
        const int flagHeight = QFontMetrics(statusBar()->font()).height();
        const int flagAspect = 180;  // average aspect: we'll shrink to fit this.
        const QSize flagBoxSize(flagHeight * flagAspect / 100, flagHeight);
        const QSize flagSize(flagBoxSize - QSize(2, 2)); // margin

        const QIcon flagIcon(region->flagIcon());

        QRect flagBox;
        QPixmap flagPixMap(flagBoxSize);
        QPainter flagPainter(&flagPixMap);

        // Clear background.
        flagPixMap.fill(QGuiApplication::palette().color(statusBar()->backgroundRole()));

        // paint icon into pixmap
        flagBox.setSize(flagSize);  // allow for outline
        flagBox.moveCenter(flagPixMap.rect().center());
        flagIcon.paint(&flagPainter, flagBox);

        // Draw outline box to make flags easier to see against background
        flagBox.setSize(flagIcon.actualSize(flagSize));
        flagBox.moveCenter(flagPixMap.rect().center());
        flagPainter.setPen(MultiIconDelegate::outlinePen());
        flagPainter.drawRect(flagBox);

        auto* flagLabel = new QLabel();
        flagLabel->setPixmap(flagPixMap);
        statWidget(Stat::Flags).back()->layout()->addWidget(flagLabel);
    }

    // Update geopol hierarchical name
    setStat(Stat::GeoPolName, nestedName);
    setStat(Stat::GeoPolId,   nestedId);
}

// On mouse movement, update the position information.
void MainWindow::setGeoPositionStatus(const Marble::GeoDataCoordinates& coords)
{
    if (!statusBar()->isVisible())
        return;

    updateStatBarGeoPol(coords);

    // Format with config units
    const QString posText =
            cfgData().unitsLon.toString(coords.longitude(Marble::GeoDataCoordinates::Degree)) + ", " +
            cfgData().unitsLat.toString(coords.latitude(Marble::GeoDataCoordinates::Degree));

    setStat(Stat::GeoLocation, posText);
}

// We don't directly set the m_progressBar values, because it's very slow, and sometimes we get
// update spam.  Instead, we update locally cached values, and set the progress bar on a timer.
void MainWindow::progressTimerUpdate()
{
    // Avoid udpate spam: clamp to 1/8th sec update frequency
    if (m_progressTimer.elapsed() < 125)
        return;

    if (m_progressBar.maximum() != m_progressMax)
        m_progressBar.setMaximum(m_progressMax);

    if (m_progressBar.value() != m_progressValue)
        m_progressBar.setValue(m_progressValue);

    m_progressTimer.start();
}

void MainWindow::finishProgress()
{
    if (m_progressMax == std::numeric_limits<int>::max())
        return;

    m_progressMax   = std::numeric_limits<int>::max();
    m_progressValue = 1;

    progressTimerUpdate();
    m_progressBar.reset();
    m_progressBar.setFormat(tr("Done"));
}

void MainWindow::initProgress(int total)
{
    if (total == 0)
        return finishProgress();

    if (m_progressMax == std::numeric_limits<int>::max()) {
        m_progressBar.reset();
        m_progressValue = 0;
        m_progressMax   = total;
        m_progressBar.setFormat(tr("%p%")); // TODO: accept as parameter

        m_progressTimer.start();
    } else {
        m_progressMax = total;
    }
}

void MainWindow::failedProgress(int failed)
{
    const QString failedMsg = tr("ERROR: %d task(s) failed").arg(failed);
    statusMessage(UiType::Error, failedMsg);
}

void MainWindow::updateProgress(int done, bool add)
{
    m_progressValue = done + (add ? m_progressValue : 0);

    if (m_progressValue >= m_progressMax) {
        finishProgress();
    } else {
        // The timer is to protect against progress bar spam.  For very rapid operations, the
        // mere act of calling setValue (which updates the GUI) can be more expensive than the
        // operation itself.
        progressTimerUpdate();
    }
}

QAction* MainWindow::getPaneAction(PaneAction cc) const
{
    if (ui == nullptr)
        return nullptr;

    switch (cc) {
    case PaneAction::ShowAll:             return ui->action_View_Show_All;
    case PaneAction::ExpandAll:           return ui->action_View_Expand_All;
    case PaneAction::CollapseAll:         return ui->action_View_Collapse_All;
    case PaneAction::SelectAll:           return ui->action_View_Select_All;
    case PaneAction::SelectNone:          return ui->action_View_Select_None;
    case PaneAction::ResizeToFit:         return ui->action_Size_Cols;
    case PaneAction::SetFiltersVisible:   return ui->action_Show_Filters;
    case PaneAction::CopySelected:        return ui->action_Copy_Selected;
    case PaneAction::ViewAsTree:          return ui->action_View_AsTree;
    case PaneAction::PaneClose:           return ui->action_Pane_Close;
    case PaneAction::PaneAdd:             return nullptr; // it has sub-menus
    case PaneAction::PaneAddGroup:        return nullptr; // it has sub-menus
    case PaneAction::PaneReplace:         return nullptr; // it has sub-menus
    case PaneAction::PaneSplitH:          return ui->action_Pane_Split_Horizontal;
    case PaneAction::PaneSplitV:          return ui->action_Pane_Split_Vertical;
    case PaneAction::PaneLeft:            return ui->action_Pane_Left;
    case PaneAction::PaneRight:           return ui->action_Pane_Right;
    case PaneAction::PaneRowUp:           return ui->action_Pane_Group_Left;
    case PaneAction::PaneRowDown:         return ui->action_Pane_Group_Right;
    case PaneAction::PaneBalanceSiblings: return ui->action_Pane_Balance_Siblings;
    case PaneAction::PaneBalanceTab:      [[fallthrough]];
    case PaneAction::PaneAddTab:          [[fallthrough]];
    case PaneAction::PaneRenameTab:       [[fallthrough]];
    case PaneAction::PaneCloseTab:        return mainWindowTabs()->getPaneAction(cc);
    default:                              return nullptr;
    }
}

QAction* MainWindow::getMainAction(MainAction aa) const
{
    switch (aa) {
    case MainAction::ZoomToSelection:     return ui->action_Zoom_To_Selection;
    case MainAction::DeleteSelection:     return ui->action_Delete_Selection;
    case MainAction::DuplicateSelection:  return ui->action_Duplicate_Selection;
    case MainAction::MergeSelection:      return ui->action_Merge_Selection;
    case MainAction::SelectPerson:        return ui->action_Select_Person;
    case MainAction::SimplifySelection:   return ui->action_Simplify_Selection;
    case MainAction::ReverseSelection:    return ui->action_Reverse_Selection;
    case MainAction::UnsetSpeed:          return ui->action_Unset_Speed;
    }

    return nullptr;
}

QToolBar* MainWindow::getToolBar(CheckableMenu tb) const
{
    switch (tb) {
    case CheckableMenu::ToolBarMain:    return ui->mainToolBar;
    case CheckableMenu::ToolBarUI:      return ui->uiToolBar;
    case CheckableMenu::ToolBarPane:    return ui->paneToolBar;
    case CheckableMenu::ToolBarMapMode: return ui->mapModeToolBar;
    default: return nullptr;
    }
}

// Restore status color, or warning/etc colors stick for status tips
void MainWindow::statusChanged(const QString& /*txt*/) const
{
    changeStatusBarColor(QPalette::Text);
}

Pane::Container* MainWindow::containerFactory() const
{
    return Pane::factory<Pane::Container>(PaneClass::Group, const_cast<MainWindow&>(*this));
}

// Save status bar entry show/hide state
void MainWindow::saveStatusBarState(QSettings& settings) const
{
    settings.beginWriteArray("statusBar"); {
        bool ok;
        for (const auto* action : statActions()) {
            if (Stat stat = Stat(action->property(s_actionPropertyStat).toInt(&ok)); ok) {
                settings.setArrayIndex(int(stat));
                SL::Save(settings, "visible", action->isChecked());
            }
        }
    } settings.endArray();
}

bool MainWindow::settingsSaver(QSettings& settings) const
{
    SL::Save(settings, "importOpts", m_importOpts);
    SL::Save(settings, "exportOpts", m_exportOpts);
    SL::Save(settings, "trackSimplifyDialog", m_trackSimplifyDialog);
    SL::Save(settings, "gpsCaptureDialog", m_gpsCaptureDialog);
    SL::Save(settings, "mapDownloadDialog", m_mapDownloadDialog);
    SL::Save(settings, "newWaypointDialog", m_newWaypointDialog);
    SL::Save(settings, "newPaneDialog", m_newPaneDialog);
    SL::Save(settings, "newTrackDialog", m_newTrackDialog);
    SL::Save(settings, "person", m_person);
    SL::Save(settings, "offlineMode", isVisible(CheckableMenu::OfflineMode));
    SL::Save(settings, "statusBarVisible", isVisible(CheckableMenu::StatusBar));
    saveStatusBarState(settings);

    return true;
}

void MainWindow::save(QSettings& settings) const
{
    if (ui == nullptr || m_privateSession)
        return;

    MainWindowBase::saveUiConfig(settings);
    saveModels();

    const_cast<MainWindow&>(*this).markModified(false, true); // mark as not modified post-save
}

// Load status bar entry show/hide state
void MainWindow::loadStatusBarState(QSettings& settings)
{
    const int size = settings.beginReadArray("statusBar"); {
        int add = 0; // for insertion of new stats during load from prior formats

        for (int i = 0; i < size; ++i) {
            settings.setArrayIndex(i);

            // Stat::Capture added in v11
            if (cfgData().priorCfgDataVersion < 11 && i == int(Stat::Capture)) {
                setVisible(Stat(i), true, false);
                ++add;
            }

            // Stat::MapMode added in v24
            if (cfgData().priorCfgDataVersion < 24 && i == int(Stat::MapMode)) {
                setVisible(Stat(i), true, false);
                ++add;
            }

            setVisible(Stat(i + add), SL::Load(settings, "visible", true), false);
        }
    } settings.endArray();
}

bool MainWindow::settingsLoader(QSettings& settings)
{
    SL::Load(settings, "importOpts", m_importOpts);
    SL::Load(settings, "exportOpts", m_exportOpts);
    SL::Load(settings, "trackSimplifyDialog", m_trackSimplifyDialog);
    SL::Load(settings, "gpsCaptureDialog", m_gpsCaptureDialog);
    SL::Load(settings, "mapDownloadDialog", m_mapDownloadDialog);
    SL::Load(settings, "newWaypointDialog", m_newWaypointDialog);
    SL::Load(settings, "newPaneDialog", m_newPaneDialog);
    SL::Load(settings, "newTrackDialog", m_newTrackDialog);
    SL::Load(settings, "person", m_person);
    setVisible(CheckableMenu::OfflineMode, SL::Load(settings, "offlineMode", false));
    setVisible(CheckableMenu::StatusBar, SL::Load(settings, "statusBarVisible", true));

    loadStatusBarState(settings);

    return true;
}

bool MainWindow::loadInternal(QSettings& settings)
{
    if (ui == nullptr)
        return false;

    m_launchSplash.setStatus(tr("Restoring UI configuration..."));

    if (!loadUiConfig(settings))
        return false;

    m_launchSplash.setStatus(tr("Loading saved data..."));

    if (!loadModels())
        return false;

    app().trackModel().setPerson(m_person);  // Update power data for the selected person
    markModified(false, true);               // mark as not modified post-load
    undoMgr().clear();                       // also done in postLoadHook, but this helps the test suite

    return true;
}

void MainWindow::load(QSettings& settings)
{
    loadInternal(settings);
}

// This is executed post-load after the application returns to the main event loop, i.e, after the
// UI becomes visible.  It can be used to do things that depend on the existence of the UI.
void MainWindow::postLoadHook()
{
    // Use statusBar()->isVisible since parent class doesn't update this class's UI
    for (CheckableMenu cm = CheckableMenu::_ModeBegin; cm < CheckableMenu::_ModeEnd; Util::inc(cm))
        setVisible(cm, isVisible(cm), false);

    for (CheckableMenu cm = CheckableMenu::_ToolBarBegin; cm < CheckableMenu::_ToolBarEnd; Util::inc(cm))
        setVisible(cm, isVisible(cm), false);

    MainWindowBase::postLoadHook();

    // Startup auto-import, if configured (do AFTER post-loadhook, which resets undos)
    autoImport(CfgData::AutoImportMode::Startup);
}

void MainWindow::dirtyStateChanged(bool dirty)
{
    if (!dirty)
        return;

    markModified(dirty);

    // Set global modification flag to display in window title
    if (cfgData().dataAutosaveInterval > 0)
        m_autosaveTimer.start(cfgData().dataAutosaveInterval * 1000); // dataAutosaveInterval is in S, timer in mS
}

void MainWindow::newConfig(bool newValues)
{
    // If there's no person for power estimation, select one if available.
    if (m_person.isEmpty() || !cfgData().people.contains(m_person))
        if (cfgData().people.rowCount() > 0)
            m_person = cfgData().people.data(cfgData().people.index(0, PersonModel::Name), Util::RawDataRole).toString();

    // Refresh model display in case units changed
    {
        const ChangeTrackingModel::SignalBlocker block(app().trackModel());
        app().trackModel().setPerson(m_person);
        app().trackModel().refresh();
    }

    // TODO: this should be done with a signal/slot set up in the various dialogs
    m_areaDialog.newConfig();             // Refresh area dialog
    m_newPaneDialog.newConfig();          // Refresh new pane dialog
    app().undoMgr().newConfig(cfgData()); // Refresh undo manager
    app().climbModel().newConfig();       // Refresh climb analysis
    m_appConfig.newConfig();              // Refresh appConfig
    m_docDialog.newConfig();              // Refresh docDialog

    // let panes update from new config
    runOnPanels([](PaneBase* pane) { pane->newConfig(); });

    // New autosave timer, and trigger save if there's dirty data and timer went from zero to non-zero
    if (newValues) {
        setupAutosave();
        dirtyStateChanged(newValues);
    }

    // Update map undo managers with undo limits specific to them.
    runOnPanels<MapPane>([](MapPane* pane) { pane->undoMgrView().setLimits(cfgData().maxUndoCountView, 1L<<20); });

    updateActions(); // update enable/disable UI states
}

void MainWindow::viewAsTree(bool asTree)
{
    runOnFocusPane(&PaneBase::viewAsTree, asTree);

    ui->action_View_AsTree->setChecked(asTree);
    resizeColumnsAllPanes();
}

void MainWindow::newFocus(QObject* focus)
{
    MainWindowBase::newFocus(focus);

    if (PaneBase* focusPane = focusedPane(focus); focusPane != nullptr)
        ui->action_View_AsTree->setChecked(focusPane->viewIsTree());

    updateActions();
    updateStatus();
}

// Return name of the file to store the given model.
QString MainWindow::modelDataFile(App::Model model) const
{
    if (!hasSettingsFile())
        return { };

    const QFileInfo settingsInfo(currentSettingsFile());

    // dataAutosavePath can optionally override GPS data file path.  Otherwise, use settings file path.
    QString path = cfgData().dataAutosavePath.isEmpty() ? settingsInfo.path() : cfgData().dataAutosavePath;
    path.reserve(path.size() + 64);

    path += QDir::separator();
    path += settingsInfo.completeBaseName();
    path += App::modelDataSuffix(model);

    return path;
}

bool MainWindow::saveModels() const
{
    if (!hasSettingsFile() || m_privateSession)
        return false;

    const QString visited    = modelDataFile(App::Model::_LastSaved);
    const bool isNewLocation = (visited != m_modelVisited);

    m_modelVisited = visited;
    bool success = true;

    for (App::Model mtype = App::Model::_First; mtype != App::Model::_LastSaved; Util::inc(mtype)) {
        const ChangeTrackingModel& model = app().getModel(mtype);

        // don't save models that aren't dirty.  If the save format has changed, or the model is
        // being saved to a new location, always save though.
        if (!model.isDirty() && !isNewLocation && !model.isOldSaveFormat())
            continue;

        const QString dataFile = modelDataFile(mtype);

        if (!backupFile(dataFile, cfgData().backupDataCount))
            error(tr("Unable to create backup file: ") + dataFile, tr("Backup Save Data"));

        const QFileInfo fileInfo(dataFile);

        // Create save directory
        if (!QDir::root().mkpath(fileInfo.absolutePath())) {
            error(tr("Unable to create save directory: ") + fileInfo.absolutePath(), tr("Save Data"));
            continue;
        }

        if (!model.save(dataFile)) {
            error(dataFile, tr("Save Data Error"));
            success = false;
        }
    }

    return success;
}

bool MainWindow::loadModels()
{   
    if (!hasSettingsFile())
        return false;

    m_modelVisited = modelDataFile(App::Model::_LastSaved);

    const auto onError = [](const QString& file) {
        QMessageBox(QMessageBox::Critical, QObject::tr("Data load error"),
                    file, QMessageBox::Abort).exec();
        return false;
    };

    for (App::Model mtype = App::Model::_First; mtype != App::Model::_LastSaved; Util::inc(mtype)) {
        ChangeTrackingModel& model = app().getModel(mtype);

        const ChangeTrackingModel::SignalBlocker block(model);

        const QString dataFile = modelDataFile(mtype);

        // No error if no model data: it might be a new config.
        if (!QFileInfo::exists(dataFile))
            continue;

        if (!model.load(dataFile))
            return onError(dataFile);
    }

    // Don't refresh the model here: that'll happen in MainWindow::load()
    return true;
}

void MainWindow::setVisible(CheckableMenu menu, bool checked, bool setDirty)
{
    switch (menu)
    {
    case CheckableMenu::StatusBar:      setStatusBarVisible(checked);     break;
    case CheckableMenu::OfflineMode:    setOfflineMode(checked);          break;
    case CheckableMenu::ToolBarMain:    [[fallthrough]];
    case CheckableMenu::ToolBarUI:      [[fallthrough]];
    case CheckableMenu::ToolBarPane:    [[fallthrough]];
    case CheckableMenu::ToolBarMapMode: setToolBarVisible(menu, checked); break;
    default: assert(0); break;
    }

    if (setDirty)
        dirtyStateChanged(true);
}

bool MainWindow::isVisible(CheckableMenu menu) const
{
    switch (menu)
    {
    case CheckableMenu::StatusBar:      return ui->action_Show_Statusbar->isChecked();
    case CheckableMenu::OfflineMode:    return ui->action_Offline_Mode->isChecked();
    case CheckableMenu::ToolBarMain:    [[fallthrough]];
    case CheckableMenu::ToolBarUI:      [[fallthrough]];
    case CheckableMenu::ToolBarPane:    [[fallthrough]];
    case CheckableMenu::ToolBarMapMode: return getToolBar(menu)->isVisibleTo(this);
    default:                            assert(0); return false;
    }
}

void MainWindow::setMapMode(MainWindow::MapMode mapMode)
{
    m_mapMode = mapMode;

    ui->action_Add_Points_Mode->setChecked(m_mapMode == MapMode::Add);
    ui->action_Map_Move_Mode->setChecked(m_mapMode == MapMode::Move);
    ui->action_Select_Points_Mode->setChecked(m_mapMode == MapMode::Select);

    QString modeText;
    Qt::CursorShape cursor = Qt::ArrowCursor;

    switch (mapMode) {
    case MapMode::Move:   modeText = tr("Map move mode");      cursor = Qt::ArrowCursor; break;
    case MapMode::Add:    modeText = tr("Add points mode");    cursor = Qt::CrossCursor; break;
    case MapMode::Select: modeText = tr("Select points mode"); cursor = Qt::DragMoveCursor; break;
    }

    statusMessage(UiType::Info, modeText);
    updateStatus();

    // TODO: the MarbleMap input handler stomps on our cursor.  We can't supply our own
    // input handler, since there seems to be no way to create a MarbleAbstractPresenter.
    // The header is not present in the public includes, nor is there a way to get a pointer
    // out of the API, but one is required to create a MarbleWidgetInputHandler.
    setCursor(cursor);
}

void MainWindow::resetUI()
{
    m_appConfig.resetDefault();
    mainWindowTabs()->deleteTabs();
    closeSecondaryWindows();
    setupDefaultPanels();
}

bool MainWindow::autoImportPreTest()
{
    const QDir autoImportDir(cfgData().autoImportDir);

    // Verify that the auto-import path exists.
    if (!autoImportDir.exists() || cfgData().autoImportDir.isEmpty()) {
        error(tr("AutoImport directory does not exist: Import canceled."), autoImportError);
        return false;
    }

    if (!autoImportDir.isReadable()) {
        error(tr("AutoImport directory is not readable: Import canceled."), autoImportError);
        return false;
    }

    // Check data:
    const QFileInfo autoImportDirFile(cfgData().autoImportDir);
    QFileInfo backupDir(cfgData().autoImportBackupDir); // non-const since we must refresh it.

    switch (cfgData().autoImportPost) {
    case CfgData::AutoImportPost::Backup: [[fallthrough]];
    case CfgData::AutoImportPost::Delete:
        if (cfgData().autoImportBackupSuffix.isEmpty()) {
            error(tr("AutoImport backup suffix is empty: Import canceled."), autoImportError);
            return false;
        }

        if (!autoImportDirFile.isWritable()) {
            error(tr("AutoImport directory is not writable: Import canceled."), autoImportError);
            return false;
        }

        break;
    case CfgData::AutoImportPost::Move:
        if (cfgData().autoImportBackupDir.isEmpty()) {
            error(tr("AutoImport backup directory does not exist: Import canceled."), autoImportError);
            return false;
        }

        backupDir.refresh();  // refresh data after creation

        if (!backupDir.exists()) {
            if (!QDir::root().mkpath(cfgData().autoImportBackupDir)) {
                error(tr("AutoImport backup dir cannot be created: Import canceled."), autoImportError);
                return false;
            }
        }

        if (!backupDir.isReadable() || !backupDir.isWritable()) {
            error(tr("No permissions for AutoImport backup directory: Import canceled."), autoImportError);
            return false;
        }

        break;
    case CfgData::AutoImportPost::Leave:
        break;
    }

    return true;
}

void MainWindow::autoImport()
{
    // Do not execute if disabled.
    if (cfgData().autoImportMode == CfgData::AutoImportMode::Disabled) {
        statusMessage(UiType::Warning, tr("AutoImport disabled in configuration."));
        return;
    }

    if (!autoImportPreTest())
        return;

    autoImportLaunch();
}

void MainWindow::autoImport(CfgData::AutoImportMode mode)
{
    // Auto-import if not disabled, and if modes match
    if (mode != CfgData::AutoImportMode::Disabled && cfgData().autoImportMode == mode)
        autoImport();
}

void MainWindow::autoImportPost(const QFileInfoList& files, const QVector<bool>& rc) const
{
    assert(files.size() == rc.size());

    const auto move = [this](const QFile& file, const QString& newName) {
        if (QFileInfo::exists(newName))
            QDir::root().remove(newName);

        if (!QDir::root().rename(file.fileName(), newName)) {
            error(tr("Unable to rename: ") + file.fileName() + " -> " + newName,
                  autoImportError);
        }
    };

    for (int x = 0; x < files.size(); ++x) {
        // skip post-import handling for any files which failed to import for some reason
        if (!rc.at(x))
            continue;

        // Also skip the auto-import stdout file. It gets removed by the QTemporaryFile.
        if (!m_autoImportStdout.isNull() && files.at(x).absoluteFilePath() == m_autoImportStdout->fileName())
            continue;

        const QFile file(files.at(x).absoluteFilePath());

        switch (cfgData().autoImportPost) {
        case CfgData::AutoImportPost::Backup:
            move(file, file.fileName() + cfgData().autoImportBackupSuffix);
            break;
        case CfgData::AutoImportPost::Move:
            move(file, cfgData().autoImportBackupDir + QDir::separator() + files.at(x).fileName());
            break;
        case CfgData::AutoImportPost::Delete:
            if (!QDir::root().remove(file.fileName()))
                error(tr("Unable to delete: ") + file.fileName(), autoImportError);
            break;
        case CfgData::AutoImportPost::Leave:
            break; // Nothing to do.
        }
    }
}

void MainWindow::autoImportLaunch()
{
    if (cfgData().autoImportCommand.isEmpty()) {
        // If the autoImportCommand is empty, we use synchronous reading of existing files.
        autoImportSyncImport();
    } else {
        // If there is an autoImportCommand, we launch an async external process.
        autoImportLaunchASync();
    }
}

void MainWindow::autoImportSyncImport()
{
    const GeoLoadParams geoLoadParams(GeoIoFeature::AllTypes | GeoIoFeature::AllAux,
                                      cfgData().autoImportTags,
                                      { }, { }); // TODO: add route auto-import tag to config
    const QFileInfoList files = autoImportFiles();

    if (files.isEmpty())
        return statusMessage(UiType::Info, tr("No files found for AutoImport."));

    const QVector<bool> rc = importTracks(geoLoadParams, files, true, false);

    // handle post-import behavior for files which succeeded.
    autoImportPost(files, rc);
}

QFileInfoList MainWindow::autoImportFiles() const
{
    // Break autoImportPattern into list of strings at space boundaries
    const QStringList nameFilters =
            cfgData().autoImportPattern.split(" ", QtCompat::SplitBehavior::SkipEmptyParts);

    // This is the list of files matching the autoImportPattern
    QFileInfoList importFiles;

    // Test for empty filter list, because that matches everything, but we want it to match
    // nothing.
    if (!nameFilters.isEmpty())
        importFiles = QDir(cfgData().autoImportDir)
                      .entryInfoList(nameFilters, QDir::Files | QDir::Readable, QDir::Time);

    // If we're running asynchronously, and using a stdout tmp file, also read that.
    if (!m_autoImportStdout.isNull())
        importFiles.append(QFileInfo(m_autoImportStdout->fileName()));

    return importFiles;
}

void MainWindow::autoImportAsyncStart()
{
    statusMessage(UiType::Info, tr("AutoImport external command started."));
}

void MainWindow::autoImportAsyncError()
{
    m_autoImportTimer.stop();
    m_autoImportStdout.reset();
    error(tr("AutoImport external command error."), autoImportError);
}

bool MainWindow::autoImportAsyncKill()
{
    if (m_autoImportProcess.state() == QProcess::NotRunning)
        return true;

    // Block signals as we kill the process, or we trigger the error AND finish signals.
    const QSignalBlocker block(m_autoImportProcess);
    m_autoImportProcess.kill();  // It had its chance.
    m_autoImportStdout.reset();

    return m_autoImportProcess.waitForFinished(10 * 1000);
}

void MainWindow::autoImportAsyncTimeout()
{
    if (!autoImportAsyncKill()) {
        error(tr("AutoImport external command timed out, and unable to kill process."), autoImportError);
    } else {
        error(tr("AutoImport external command timed out."), autoImportError);
    }
}

void MainWindow::autoImportAsyncFinish(int rc, QProcess::ExitStatus exitStatus)
{
    m_autoImportTimer.stop();

    if (rc != 0 || exitStatus != QProcess::NormalExit)
        return error(tr("AutoImport external process failed."), autoImportError);

    // Now that we've finished the async process to produce files, use the normal
    // synchronous import process to read them.
    autoImportSyncImport();
    m_autoImportStdout.reset();
}


std::tuple<QString, QStringList> MainWindow::autoImportParseCmd() const
{
    // See comment in posixexpander.h for the features this provides.
    const Util::PosixExpander expander(cfgData().autoImportCommand);

    if (expander.rc() != 0) {
        statusMessage(UiType::Error, tr("Failed to parse Auto Import Command"));
        return { };
    }

    if (expander.argc() == 0) {
        statusMessage(UiType::Error, tr("Auto Import Command empty"));
        return { };
    }

    return { expander.cmd(), expander.args() };
}

void MainWindow::on_action_Show_Filters_triggered(bool checked)
{
    runOnPanels([checked](PaneBase* pane) {
        pane->setFiltersVisible(checked);
    });
}

void MainWindow::toolBarToggled(CheckableMenu tb, bool checked)
{
    if (QToolBar* toolBar = getToolBar(tb); toolBar != nullptr) {
        QAction* action = toolBar->toggleViewAction();
        const UndoMgr::ScopedUndo undoSet(app().undoMgr(), showHideMsg(checked, action->text()));
        app().undoMgr().add(new UndoUiMenu(*this, tb));

        setToolBarVisible(tb, checked);
    }
}

// For some reason, in Qt 5.15, PaneBase::paneToggled() works during load, but does not work
// during setupDefaultPanels() which happens at construct time.  Everything was fine <= 5.13,
// so I'm not sure what the story is, but this is a workaround.
void MainWindow::enablePaneChildren() const
{
    runOnPanels([](PaneBase* pane) { pane->paneToggled(pane->isChecked()); });
}

void MainWindow::on_action_Show_Statusbar_triggered(bool checked)
{
    const UndoMgr::ScopedUndo undoSet(app().undoMgr(), showHideMsg(checked, ui->action_Show_Statusbar->text()));
    app().undoMgr().add(new UndoUiMenu(*this, CheckableMenu::StatusBar));

    setVisible(CheckableMenu::StatusBar, checked);
}

void MainWindow::on_action_Pane_Balance_Siblings_triggered()
{
    balanceSiblingsInteractive();
}

// Replace pane with splitter containing pane, and new pane.
void MainWindow::on_action_Pane_Split_Horizontal_triggered()
{
    splitPaneInteractive(focusedPaneWarn(), Qt::Horizontal);
}

// Replace pane with splitter containing pane, and new pane.
void MainWindow::on_action_Pane_Split_Vertical_triggered()
{
    splitPaneInteractive(focusedPaneWarn(), Qt::Vertical);
}

void MainWindow::on_action_Copy_Selected_triggered()
{
    runOnFocusPane(&Pane::copySelected);
}

void MainWindow::on_action_View_Show_All_triggered()
{
    runOnFocusPane(&Pane::showAll);
}

void MainWindow::on_action_View_Expand_All_triggered()
{
    runOnFocusPane(&Pane::expandAll);
}

void MainWindow::on_action_View_Collapse_All_triggered()
{
    runOnFocusPane(&Pane::collapseAll);
}

void MainWindow::on_action_View_Select_All_triggered()
{
    runOnFocusPane(&Pane::selectAll);
}

void MainWindow::on_action_View_Select_None_triggered()
{
    runOnFocusPane(&Pane::selectNone);
}

void MainWindow::on_action_Pane_Close_triggered()
{
    const UndoWinCfg::ScopedUndo undoSet(*this, ui->action_Pane_Close->text());

    removePane(focusedPaneWarn());
}

void MainWindow::on_action_Pane_Left_triggered()
{
    const UndoWinCfg::ScopedUndo undoSet(*this, ui->action_Pane_Left->text());

    movePane(focusedPaneWarn(), -1);
}

void MainWindow::on_action_Pane_Right_triggered()
{
    const UndoWinCfg::ScopedUndo undoSet(*this, ui->action_Pane_Right->text());

    movePane(focusedPaneWarn(), +1);
}

void MainWindow::on_action_Pane_Group_Left_triggered()
{
    const UndoWinCfg::ScopedUndo undoSet(*this, ui->action_Pane_Group_Left->text());

    movePaneParent(focusedPaneWarn(), -1);
}

void MainWindow::on_action_Pane_Group_Right_triggered()
{
    const UndoWinCfg::ScopedUndo undoSet(*this, ui->action_Pane_Group_Right->text());

    movePaneParent(focusedPaneWarn(), +1);
}

void MainWindow::on_action_Reset_Ui_triggered()
{
    if (cfgData().warnOnRevert)
        if (warningDialog(tr("Reset UI Configuration"), tr("Reset UI configuration to defaults?")) != QMessageBox::Ok)
            return;

    const UndoWinCfg::ScopedUndo undoSet(*this, ui->action_Reset_Ui->text());
    resetUI();
    statusMessage(UiType::Success, tr("UI configuration reset."));
}

void MainWindow::on_action_Quit_without_Save_triggered()
{
    m_saveOnExit = false;
    close();
}

void MainWindow::on_action_Export_Track_triggered()
{
    exportTracks(m_exportOpts.getExportFileName(GeoSave::trackFileFilter));
}

// Import from defined options
QVector<bool> MainWindow::importTracks(const GeoLoadParams& loadParams, const ImportInfoList& importList,
                                       bool statMsg, bool errors)
{
    const UndoMgr::ScopedUndo undoSet(app().undoMgr(), UndoMgr::genName(tr("Import"), importList.size(), "File", "Files"));
    GeoLoad loader(this, app().trackModel(), app().waypointModel());

    return postImport(loader, loader.load(loadParams, importList), statMsg, errors);
}

// Import from dialog options
QVector<bool> MainWindow::importTracks(const ImportInfoList& importList, bool statMsg, bool errors)
{
    // No files
    if (importList.isEmpty()) {
        if (statMsg)
            statusMessage(UiType::Warning, tr("Canceled"));
        return { };
    }

    // Temporary loader for the purpose of querying allNative
    {
        GeoLoad loader(this, app().trackModel(), app().waypointModel());

        if (!loader.allNative(importList)) { // only display import dialog for non-native formats
            // Allow test suite to bypass this modal input. Dialog values set by test suite in that case.
            if (!appBase().testing() && m_importOpts.exec() != QDialog::Accepted) {
                if (statMsg)
                    statusMessage(UiType::Warning, tr("Canceled"));
                return { };
            }
        }
    }

    return importTracks(m_importOpts.geoLoadParams(), importList, statMsg, errors);
}

// Post data import hook
QVector<bool> MainWindow::postImport(const GeoLoad& loader, const QVector<bool>& rc, bool statMsg, bool errors)
{
    // If there are any failures
    if (GeoLoad::anyFailed(rc)) {
        if (errors)
            error(loader.errorString(), tr("Import File"));
        else if (statMsg)
            statusMessage(UiType::Error, loader.errorString());

        return rc;
    }

    static const constexpr auto selectOpt = QItemSelectionModel::Clear | QItemSelectionModel::SelectCurrent | QItemSelectionModel::Rows;

    const auto updatePane = [](DataColumnPane* pane, const QModelIndexList& imported) {
        pane->sort();
        pane->refreshFilter();
        pane->select(imported, selectOpt);
        pane->resizeToFit(200);
    };

    // For convenience: select the newly imported tracks.
    // Must refresh filter so we can set the current index just below.  Otherwise, the
    // newly added track won't have passed the active filter, so can't be selected.
    runOnPanels<TrackPane>([&](TrackPane* pane) { updatePane(pane, loader.importedTrk()); });
    runOnPanels<WaypointPane>([&](WaypointPane* pane) { updatePane(pane, loader.importedWpt()); });

    if (statMsg) {
        const int totalSkipped = loader.duplicateTrkCount() + loader.duplicateWptCount();
        const bool bothSkipped = (loader.duplicateTrkCount() > 0 && loader.duplicateWptCount() > 0);

        // Ungainly bit of text concatenation ahead.
        statusMessage(UiType::Success, tr("Track import completed.") +
                      ((totalSkipped > 0) ? tr(" Skipped ") : "") +
                      ((loader.duplicateTrkCount() > 0) ? QString::number(loader.duplicateTrkCount()) + tr(" duplicate tracks")
                                                        : QString()) +
                      (bothSkipped ? tr(" and ") : "") +
                      ((loader.duplicateWptCount() > 0) ? QString::number(loader.duplicateWptCount()) + tr(" duplicate waypoints")
                                                        : QString()));
    }

    return rc;
}

bool MainWindow::exportTracks(const QString& exportFile)
{
    // No export file
    if (exportFile.isNull()) {
        statusMessage(UiType::Warning, tr("Canceled"));
        return false;
    }

    if (!appBase().testing() && m_exportOpts.exec() != QDialog::Accepted) {
        statusMessage(UiType::Warning, tr("Canceled"));
        return false;
    }

    GeoSaveParams geoSaveParams = m_exportOpts.geoSaveParams();

    if (auto* trkPane = findPane<TrackPane>(); trkPane != nullptr)
        geoSaveParams.m_trkSelection = trkPane->getSelections();

    if (auto* wptPane = findPane<WaypointPane>(); wptPane != nullptr)
        geoSaveParams.m_wptSelection = wptPane->getSelections();

    if (!geoSaveParams.hasFeature(GeoIoFeature::AllTrk) && geoSaveParams.m_trkSelection.empty()) {
        const int ret = QMessageBox::question(m_exportOpts.parentWidget(),
                                              tr("Selection"),
                                              tr("No selection: export all tracks and routes?"),
                                              QMessageBox::Yes | QMessageBox::No,
                                              QMessageBox::Yes);

        if (ret != QMessageBox::Yes) {
            statusMessage(UiType::Warning, tr("Canceled"));
            return false;
        }

        geoSaveParams.m_feature |= (GeoIoFeature::AllTrk | GeoIoFeature::AllRte);
    }

    GeoSave saver(this, app().trackModel(), app().waypointModel(), geoSaveParams);

    if (saver.save(exportFile)) {
        statusMessage(UiType::Success, tr("Track export completed."));
        return true;
    } else {
        if (!saver.errorString().isEmpty())
            error(saver.errorString(), tr("Export Tracks"));
        else
            statusMessage(UiType::Warning, tr("Canceled"));

        return false;
    }
}

void MainWindow::showBoxSelectionDialog(const Marble::GeoDataLatLonBox& region)
{
    m_areaDialog.newRegion(region);
    m_areaDialog.show();
}

bool MainWindow::isOfflineMode() const
{
    return ui->action_Offline_Mode->isChecked();
}

void MainWindow::setOfflineMode(bool offline)
{
    ui->action_Offline_Mode->setChecked(offline);  // make sure UI state reflects actual

    runOnPanels<MapPane>([offline](MapPane* mapPane) { mapPane->setOfflineMode(offline); });
    updateStatus();   // Update status bar for offline indicator
    updateActions();  // Update enabled actions, for network refresh enable.
}

void MainWindow::setStatusBarVisible(bool checked)
{
    statusBar()->setVisible(checked);
    ui->action_Show_Statusbar->setChecked(checked);
}

void MainWindow::setToolBarVisible(CheckableMenu tb, bool checked)
{
    if (QToolBar* toolBar = getToolBar(tb); toolBar != nullptr) {
        toolBar->setVisible(checked);
        toolBar->toggleViewAction()->setChecked(checked);
    }
}

void MainWindow::reloadVisible()
{
    runOnPanels<MapPane>([](MapPane* mapPane) { mapPane->reloadVisible(); });
}

void MainWindow::resortAll()
{
    runOnPanels<PaneBase>([](PaneBase* pane) { pane->sort(); });
}

void MainWindow::expandPointPanes() const
{
    runOnPanels<PointPane>([](PaneBase* pane) { pane->expandAll(); });
}

QStringList MainWindow::recentTags(int maxTags, int maxTracks) const
{
    const TrackModel& trackModel = app().trackModel();

    QModelIndexList indexes;
    indexes.reserve(maxTracks);

    if (auto* trackPane = findPane<TrackPane>(); trackPane != nullptr) {
        // If a track pane is available, use those tracks
        indexes = trackPane->getAllIndexes(TrackModel::Tags);

        // Sort in model increasing order
        std::sort(indexes.begin(), indexes.end(), [](const QModelIndex& idx0, const QModelIndex& idx1) {
            return idx0.row() > idx1.row();
        });

        while (indexes.size() > maxTracks)
            indexes.pop_back();

        return trackModel.recentTags(indexes, maxTags);
    }

    // Otherwise, use most recent tracks from the trackModel
    return trackModel.recentTags(maxTags, maxTracks);
}

void MainWindow::on_action_Import_Track_triggered()
{
    importTracks(m_importOpts.getImportFilenames(GeoLoad::trackFileFilter), true, true);
}

void MainWindow::on_action_Open_Settings_triggered()
{
    openSettings();
}

void MainWindow::on_action_Save_Settings_As_triggered()
{
    saveSettingsAs();
}

void MainWindow::on_action_Save_Settings_triggered()
{
    saveSettings();
}

void MainWindow::on_action_Revert_triggered()
{
    revertSettings();
}

void MainWindow::on_action_Enlarge_Font_triggered()
{
    changeFontSize(1.1f);
}

void MainWindow::on_action_Shrink_Font_triggered()
{
    changeFontSize(0.9f);
}

void MainWindow::on_action_Size_Cols_triggered()
{
    resizeColumnsAllPanes();
}

void MainWindow::on_action_Reset_Font_triggered()
{
    setFontSize(m_startupFontSize);
}

void MainWindow::on_action_Delete_Selection_triggered()
{
    PaneBase* pane = focusedPaneWarn();

    if (auto* dcp = dynamic_cast<DataColumnPane*>(pane); dcp != nullptr) {
        if (cfgData().warnOnRemove && !appBase().testing()) {
            const int count = dcp->selectionModel()->selectedRows().size();
            const QString dialogName = UndoMgr::genName(tr("Delete"), count, dcp->getItemNameLower());
            const QString dialogText = UndoMgr::genName(tr("You are about to remove"), count, dcp->getItemName()) +
                    tr(".  Proceed?");

            if (warningDialog(dialogName, dialogText) == QMessageBox::Cancel)
                return;
        }

        dcp->deleteSelection();
    }
}

void MainWindow::on_action_Zoom_To_Selection_triggered()
{
    if (auto* dcp = dynamic_cast<DataColumnPane*>(focusedPaneWarn()); dcp != nullptr)
        dcp->zoomToSelection();
}

void MainWindow::on_action_Duplicate_Selection_triggered()
{
    if (auto* dcp = dynamic_cast<DataColumnPane*>(focusedPaneWarn()); dcp != nullptr)
        dcp->duplicateSelection();
}

void MainWindow::on_action_Merge_Selection_triggered()
{
    if (auto* dcp = dynamic_cast<DataColumnPane*>(focusedPaneWarn()); dcp != nullptr)
        dcp->mergeSelection();
}

void MainWindow::on_action_Simplify_Selection_triggered()
{
    if (auto* dcp = dynamic_cast<DataColumnPane*>(focusedPaneWarn()); dcp != nullptr)
        dcp->simplifySelection();
}

void MainWindow::on_action_Select_Person_triggered()
{
    m_personDialog.setValue(m_person);

    if (m_personDialog.exec() != QDialog::Accepted)
        return statusMessage(UiType::Warning, tr("Canceled"));

    app().trackModel().setPerson(m_person = m_personDialog.value());
}

void MainWindow::on_action_Import_from_Device_triggered()
{
    m_deviceDialog.show();
}

void MainWindow::on_action_New_Window_triggered()
{
    const UndoWinCfg::ScopedUndo undoSet(*this, tr("New Window"));

    newWindowInteractive();
}

void MainWindow::on_action_Offline_Mode_triggered(bool checked)
{
    const UndoMgr::ScopedUndo undoSet(app().undoMgr(), ui->action_Offline_Mode->text());
    app().undoMgr().add(new UndoUiMenu(*this, CheckableMenu::OfflineMode));

    setVisible(CheckableMenu::OfflineMode, checked);
}

void MainWindow::on_action_Donate_triggered()
{
    m_aboutDialog.showTab(AboutDialog::Donate);
}

void MainWindow::on_action_Reload_Visible_Area_triggered()
{
    reloadVisible();
}

// Things to do after an undo OR redo.
void MainWindow::postUndoActions()
{
    resortAll();      // Resort, since view autosort is disabled for perf reasons
    markModified(!app().undoMgr().atSavePoint());
    updateActions();
}

void MainWindow::on_action_Undo_triggered()
{
    // Display redo name, since we query it after the undo
    if (app().undoMgr().undo()) // undo
        statusMessage(UiType::Success, tr("Undone: ") + app().undoMgr().topRedoName());
}

void MainWindow::on_action_Unset_Speed_triggered()
{
    if (auto* dcp = dynamic_cast<DataColumnPane*>(focusedPaneWarn()); dcp != nullptr)
        dcp->unsetSpeed();
}

void MainWindow::on_action_Redo_triggered()
{
    // Display undo name, since we query it after the redo
    if (app().undoMgr().redo()) // redo
        statusMessage(UiType::Success, tr("Redone: ") + app().undoMgr().topUndoName());
}

void MainWindow::on_action_Clear_Undo_triggered()
{
    undoMgr().clear();
    updateActions();
}

void MainWindow::on_action_Undo_View_triggered()
{
    // previous viewpoint
    if (auto* mapPane = findPane<MapPane>(); mapPane != nullptr) {
        mapPane->undoMgrView().undo();
        postUndoActions();
    }
}

void MainWindow::on_action_Redo_View_triggered()
{
    // next viewpoint
    if (auto* mapPane = findPane<MapPane>(); mapPane != nullptr) {
        mapPane->undoMgrView().redo();
        postUndoActions();
    }
}

void MainWindow::on_action_Live_GPSD_Capture_triggered()
{
    m_gpsCaptureDialog.show();
}

void MainWindow::on_action_Configure_triggered()
{
   m_appConfig.show();
}

void MainWindow::on_action_About_ZT_triggered()
{
    m_aboutDialog.show();
}

void MainWindow::on_action_Show_Tutorial_triggered()
{
    m_docDialog.show();
}

void MainWindow::on_action_Download_Region_triggered()
{
    m_mapDownloadDialog.show();
}

void MainWindow::on_action_Supported_GPSD_Versions_triggered()
{
    m_gpsdVersionDialog.show();
}

void MainWindow::on_action_Pane_Dialog_triggered()
{
    m_newPaneDialog.show();
}

void MainWindow::on_action_Add_Points_Mode_triggered()
{
    setMapMode(MapMode::Add);
}

void MainWindow::on_action_Select_Points_Mode_triggered()
{
    setMapMode(MapMode::Select);
}

void MainWindow::on_action_Map_Move_Mode_triggered()
{
    setMapMode(MapMode::Move);
}

void MainWindow::on_action_Create_New_Track_triggered()
{
    const QModelIndex newTrack = m_newTrackDialog.exec(app().trackModel(), tr("Create new track"));
    if (!newTrack.isValid())
        return;

    if (auto* trkPane = findPane<TrackPane>(); trkPane != nullptr)
        trkPane->select(newTrack);

    statusMessage(UiType::Success, tr("Created Track: ") + m_newTrackDialog.trackName());
}

void MainWindow::on_action_Goto_Lat_Lon_triggered()
{
    auto* mapPane = findPane<MapPane>();
    if (mapPane == nullptr)
        return;

    m_gotoLatLonDialog.setPos(mapPane->center());

    if (m_gotoLatLonDialog.exec() != QDialog::Accepted)
        return statusMessage(UiType::Warning, tr("Canceled"));

    mapPane->zoomTo(m_gotoLatLonDialog.location());
}

void MainWindow::on_action_Visit_ZombieTrackerGPS_Web_Site_triggered()
{
    if (!QDesktopServices::openUrl(app().ZtgpsWWW(App::WWW::Main)))
        return statusMessage(UiType::Warning, tr("Unable to open web site in external browser."));
}

void MainWindow::on_action_Auto_Import_triggered()
{
    autoImport();
}

void MainWindow::on_action_Reverse_Selection_triggered()
{
    if (auto* dcp = dynamic_cast<DataColumnPane*>(focusedPaneWarn()); dcp != nullptr)
        dcp->reverseSelection();
}

// This is last in the file because the multiple returns from autoImportParseCmd
// screws up auto-indentation for the entire rest of the file in qtcreator. Gah.
void MainWindow::autoImportLaunchASync()
{
    if (m_autoImportProcess.state() != QProcess::NotRunning)
        return statusMessage(UiType::Warning, tr("AutoImport command already running."));

    // Add a variable for the autoImportDir, so it can be expanded by wordexp()
    // We set this both for the called-process, and temporarily for ourselves so that
    // wordexp(3) sees it.
    static const char* autoImportDirVar = "AutoImportDir";

    // Setup process parameters
    m_autoImportProcess.setWorkingDirectory(cfgData().autoImportDir);

    // Create args for the process. Must happen after env manipulation above
    qputenv(autoImportDirVar, qUtf8Printable(cfgData().autoImportDir));
    const auto [cmd, args] = autoImportParseCmd();
    qunsetenv(autoImportDirVar); // remove from our own environment

    if (cfgData().autoImportStdout) {
        m_autoImportStdout.reset(new QTemporaryFile(QDir::tempPath() + QDir::separator() + "AsyncImportStdout"));

        if (!m_autoImportStdout->open())
            return statusMessage(UiType::Error, tr("Unable to open temporary file."));

        m_autoImportProcess.setStandardOutputFile(m_autoImportStdout->fileName());
    } else {
        // Discard stdout if we're not going to read it.
        m_autoImportProcess.setStandardOutputFile(QProcess::nullDevice());
    }

    // Start the process.
    m_autoImportProcess.start(cmd, args);

    // Timer to kill the process if it runs too long.
    using namespace std::chrono_literals;
    m_autoImportTimer.start(1s * cfgData().autoImportTimeout);
}

