/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <tuple>

#include <QScopedPointer>
#include <QElapsedTimer>
#include <QTimer>
#include <QList>
#include <QLabel>
#include <QDir>
#include <QProgressBar>
#include <QMenu>
#include <QSessionManager>
#include <QString>
#include <QProcess>
#include <QFileInfoList>

#include <src/ui/windows/mainwindowbase.h>

#include "src/core/app.h"
#include "src/core/selectionsummary.h"
#include "src/ui/panes/pane.h"
#include "src/ui/windows/appconfig.h"
#include "src/ui/windows/aboutdialog.h"
#include "src/ui/windows/docdialog.h"
#include "src/ui/windows/launchsplash.h"
#include "src/ui/dialogs/exportdialog.h"
#include "src/ui/dialogs/importdialog.h"
#include "src/ui/dialogs/areadialog.h"
#include "src/ui/dialogs/persondialog.h"
#include "src/ui/dialogs/devicedialog.h"
#include "src/ui/dialogs/tracksimplifydialog.h"
#include "src/ui/dialogs/gpscapturedialog.h"
#include "src/ui/dialogs/mapdownloaddialog.h"
#include "src/ui/dialogs/newwaypointdialog.h"
#include "src/ui/dialogs/gpsdversiondialog.h"
#include "src/ui/dialogs/newpanedialog.h"
#include "src/ui/dialogs/newtrackdialog.h"
#include "src/ui/dialogs/gotolatlondialog.h"

enum class UiType;

class QItemSelectionModel;
class QMouseEvent;
class QTemporaryFile;
class GeoLoad;

class UndoCfgData;
template <typename T> class UndoUiBase;

namespace Ui {
class MainWindow;
} // namespace Ui

// Actions presented in main window, e.g, by toolbar
// When adding new ones, also check pane class hasAction(MainAction) methods
enum class MainAction {
    ZoomToSelection,
    DeleteSelection,
    DuplicateSelection,
    MergeSelection,
    SelectPerson,
    SimplifySelection,
    ReverseSelection,
    UnsetSpeed,
};

class MainWindow final : public MainWindowBase
{
    Q_OBJECT

    // helper to call static initializers before other object construction
    struct InitStatic {
        InitStatic(MainWindow&);
        ~InitStatic();
    } initStatic;

public:
    explicit MainWindow(QWidget *parent = nullptr, const CmdLine* cmdLineOverride = nullptr);
    ~MainWindow() override;

    // Menu entries whose state is saved
    enum class CheckableMenu {
        _ModeBegin,
        StatusBar = _ModeBegin,
        OfflineMode,
        _ModeEnd,

        _ToolBarBegin = 0x1000, // saved with window state, not explicitly.
        ToolBarMain = _ToolBarBegin,
        ToolBarUI,
        ToolBarPane,
        ToolBarMapMode,
        _ToolBarEnd,

        _Count = _ToolBarEnd
    };

    // Status bar items
    enum class Stat {
        _First = 0,
        Offline = _First,  // online/offline state
        MapMode,           // map movement mode icon
        Capture,           // live data capture state
        Total,             // total tracks loaded
        Visible,           // visible tracks (matching filter)
        Selected,          // selected tracks
        Length,            // selected track total length
        Duration,          // selected track total duration
        Ascent,            // selected track total ascent
        Descent,           // selected track total descent
        Flags,             // mouse cursor geopol flag
        GeoLocation,       // textual lat/lon location
        GeoPolId,          // 2 letter ID
        GeoPolName,        // textual geopol name
        _Count,
    };

    // Map interaction mode: applies to all map panes
    enum class MapMode {
        Move,              // normal mode: move the map, but no editing
        Add,               // add new points to current track
        Select,            // select points to current track
    };

    [[nodiscard]] QAction* getPaneAction(PaneAction) const override;
    [[nodiscard]] QAction* getMainAction(MainAction) const;

    void openAppConfig(AppConfig::Page);

    [[nodiscard]] QWidget* paneFactory(PaneClass_t paneClass = PaneClass_t(-1)) const override;
    template <class T = PaneBase> T* paneFactory(PaneClass paneClass) const;
    Pane::Container* containerFactory() const override;

    // Put this on the stack to handle progress bar updating
    class ProgressHandler {
    public:
        ProgressHandler(MainWindow* m, int total) : m(m) { if (nest++ == 0 && m != nullptr) m->initProgress(total); }
        ~ProgressHandler() { if (--nest == 0 && m != nullptr) m->finishProgress(); }
    private:
        MainWindow* m;
        static int nest;
    };

    // Status and Progress bar
    void updateStatus(const SelectionSummary&);
    void updateProgress(int done, bool add = false);
    void failedProgress(int failed);
    void finishProgress();
    void initProgress(int total);

    void showBoxSelectionDialog(const Marble::GeoDataLatLonBox& region);

    // Importing
    QVector<bool> importTracks(const ImportInfoList&, bool statMsg = true, bool errors = true);
    QVector<bool> importTracks(const GeoLoadParams&, const ImportInfoList&, bool statMsg = true, bool errors = true);

    // Exporting
    bool exportTracks(const QString&);

    // Map mode: global to all map panes
    [[nodiscard]] MapMode mapMode() const { return m_mapMode; }
    void setMapMode(MapMode);

    [[nodiscard]] bool isOfflineMode() const;

    void newConfig(bool newValues) override;

    // Capture count tracking
    void incCaptureCount();
    void decCaptureCount();
    [[nodiscard]] bool hasLiveCaptures() const { return m_gpsCaptureCount > 0; }

    [[nodiscard]] TrackSimplifyDialog& getTrackSimplifyDialog()   { return m_trackSimplifyDialog; }
    [[nodiscard]] NewWaypointDialog&   getNewWaypointDialog()     { return m_newWaypointDialog; }
    [[nodiscard]] NewTrackDialog&      getNewTrackDialog()        { return m_newTrackDialog; }
    [[nodiscard]] const AppConfig&     getAppConfigDialog() const { return m_appConfig; }

    void expandPointPanes() const;

    // Obtain list of recently used tags.
    // maxTags returns at most that many. maxTracks limits the search to N tracks.
    [[nodiscard]] QStringList recentTags(int maxTags = 6, int maxTracks = 25) const;

    static MainWindow* mainWindowStatic;  // there can be only one.

private:
    friend class UndoUiBase<CheckableMenu>;
    friend class UndoUiBase<Stat>;
    friend class UndoCfgData;

public slots:
    void viewAsTree(bool);
    void statusChanged(const QString&) const; // reset status line color
    void updateStatus();
    void selectionChanged(const QItemSelectionModel*,
                          const QItemSelection& selected, const QItemSelection& deselected) override;
    void currentChanged(const QModelIndex& current) override;
    void commitData(QSessionManager& manager);
    void setGeoPositionStatus(const Marble::GeoDataCoordinates&);
    void updateActions() override; // update action enable/disable state
    void updateActionsDeferred(); // expensive updates, e.g, which require traversing a selection list

signals:
    void currentTrackChanged(const QModelIndex& current);
    void currentWaypointChanged(const QModelIndex& current);
    void visibleTracksChanged();
    void visibleWaypointsChanged();
    void currentTrackPointChanged(const QModelIndex& current);
    void selectedPointsChanged(const QItemSelectionModel*, const QItemSelection&, const QItemSelection&);
    void selectedTracksChanged(const QItemSelectionModel*, const QItemSelection&, const QItemSelection&);
    void selectedWaypointsChanged(const QItemSelectionModel*, const QItemSelection&, const QItemSelection&);
    void trackQueryChanged(const QString&);
    void waypointQueryChanged(const QString&);

private slots:
    void newFocus(QObject*) override;
    void postLoadHook() override;

    void showStatusBarContextMenu(const QPoint& pos);
    void statusBarItemToggled(QAction*);
    void toolBarToggled(MainWindow::CheckableMenu, bool checked);
    void enablePaneChildren() const; // see comment in C++

    void dirtyStateChanged(bool) override;

    void autoImportAsyncStart();                              // auto-import started
    void autoImportAsyncError();                              // auto-import error
    void autoImportAsyncTimeout();                            // auto-import command timeout
    void autoImportAsyncFinish(int rc, QProcess::ExitStatus); // for auto-import async processes

    // TODO: ...
    //    void commitData(QSessionManager& manager);
    //    void egg();

    void on_action_About_ZT_triggered();
    void on_action_Add_Points_Mode_triggered();
    void on_action_Auto_Import_triggered();
    void on_action_Clear_Undo_triggered();
    void on_action_Configure_triggered();
    void on_action_Copy_Selected_triggered();
    void on_action_Create_New_Track_triggered();
    void on_action_Delete_Selection_triggered();
    void on_action_Donate_triggered();
    void on_action_Download_Region_triggered();
    void on_action_Duplicate_Selection_triggered();
    void on_action_Enlarge_Font_triggered();
    void on_action_Export_Track_triggered();
    void on_action_Goto_Lat_Lon_triggered();
    void on_action_Import_Track_triggered();
    void on_action_Import_from_Device_triggered();
    void on_action_Live_GPSD_Capture_triggered();
    void on_action_Map_Move_Mode_triggered();
    void on_action_Merge_Selection_triggered();
    void on_action_New_Window_triggered();
    void on_action_Offline_Mode_triggered(bool);
    void on_action_Open_Settings_triggered();
    void on_action_Pane_Balance_Siblings_triggered();
    void on_action_Pane_Close_triggered();
    void on_action_Pane_Dialog_triggered();
    void on_action_Pane_Group_Left_triggered();
    void on_action_Pane_Group_Right_triggered();
    void on_action_Pane_Left_triggered();
    void on_action_Pane_Right_triggered();
    void on_action_Pane_Split_Horizontal_triggered();
    void on_action_Pane_Split_Vertical_triggered();
    void on_action_Quit_without_Save_triggered();
    void on_action_Redo_View_triggered();
    void on_action_Redo_triggered();
    void on_action_Reload_Visible_Area_triggered();
    void on_action_Reset_Font_triggered();
    void on_action_Reset_Ui_triggered();
    void on_action_Reverse_Selection_triggered();
    void on_action_Revert_triggered();
    void on_action_Save_Settings_As_triggered();
    void on_action_Save_Settings_triggered();
    void on_action_Select_Person_triggered();
    void on_action_Select_Points_Mode_triggered();
    void on_action_Shrink_Font_triggered();
    void on_action_Show_Filters_triggered(bool);
    void on_action_Show_Statusbar_triggered(bool);
    void on_action_Show_Tutorial_triggered();
    void on_action_Simplify_Selection_triggered();
    void on_action_Size_Cols_triggered();
    void on_action_Supported_GPSD_Versions_triggered();
    void on_action_Undo_View_triggered();
    void on_action_Undo_triggered();
    void on_action_Unset_Speed_triggered();
    void on_action_View_Collapse_All_triggered();
    void on_action_View_Expand_All_triggered();
    void on_action_View_Select_All_triggered();
    void on_action_View_Select_None_triggered();
    void on_action_View_Show_All_triggered();
    void on_action_Visit_ZombieTrackerGPS_Web_Site_triggered();
    void on_action_Zoom_To_Selection_triggered();

private:
    friend class TestZtgps;  // tester

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

    void setupSession() const;
    void setupDefaultPanels();
    void setupMenus();
    void setupActionTooltips();  // set WhatsThis to match ToolTip
    void setupStatus();
    void setupProgress();
    void setupStatusMenus();
    void setupSignals();
    void setupEnableTimer();
    void setupAutosave();
    void setupAutoImportTimeout();
    void setupDefaults();
    void setupActionIcons();
    void checkGeoPolMgr();

    static void setupDefaultViewPresets();
    static void setupDefaultFilters();

    bool saveModels() const;  // save all binary models.  return success.
    bool loadModels();  // load all binary models.  return success.

    void mouseReleaseEvent(QMouseEvent*) override;
    void dragEnterEvent(QDragEnterEvent*) override;
    void dropEvent(QDropEvent*) override;

    void firstRunHook() override;
    void sampleSession();
    using MainWindowBase::recentSessionsChanged;
    void recentSessionsChanged();
    void progressTimerUpdate(); // see comment in C++
    [[nodiscard]] QVector<bool> postImport(const GeoLoad&, const QVector<bool>& rc,
                                           bool statMsg, bool errors); // post import cleanup: resize columns, etc.
    void setOfflineMode(bool offline);  // set offline mode
    void setStatusBarVisible(bool checked);
    void setToolBarVisible(CheckableMenu, bool checked);
    void reloadVisible(); // reload visible area of map panes
    void resortAll(); // resort all panes
    void postUndoActions(); // actions to perform after an undo
    void resetUI(); // reset UI to default

    void autoImport();  // Run auto-import process, unless it is disabled.
    void autoImport(CfgData::AutoImportMode); // Auto-import if mode matches.
    bool autoImportPreTest(); // Runs some basic dir/file tests before attempting autoImport
    void autoImportPost(const QFileInfoList&, const QVector<bool>& rc) const;
    void autoImportLaunch();       // Launch AutoImport, either sync or async.
    void autoImportSyncImport();   // Synchronous import
    void autoImportLaunchASync();  // Async external command.
    bool autoImportAsyncKill();    // kill any running auto-import process
    QFileInfoList autoImportFiles() const; // return list of files found to AutoImport
    [[nodiscard]] std::tuple<QString, QStringList> autoImportParseCmd() const;

    // load config data from these settings
    void loadCfgData(QSettings& settings) override { m_appConfig.load(settings); }
    bool loadInternal(QSettings&);

    bool settingsLoader(QSettings&) override;       // Load derived class settings data
    bool settingsSaver(QSettings&) const override;  // Save derived class settings data

    [[nodiscard]] QToolBar* getToolBar(CheckableMenu) const;
    [[nodiscard]] QString   modelDataFile(App::Model) const;

    // Functions to query menu check state
    using MainWindowBase::setVisible;
    void setVisible(CheckableMenu, bool checked, bool setDirty = true);
    void setVisible(Stat, bool visible, bool setDirty = true);
    [[nodiscard]] bool isVisible(CheckableMenu) const;
    [[nodiscard]] bool isVisible(Stat) const;

    // Status bar functions
    void setStat(Stat, const QString&, const QByteArray& fontStyle, const QColor&);
    void setStat(Stat, const QString&, const QByteArray& fontStyle = "normal");
    void setStat(Stat, const QIcon&);
    void updateStatBarGeoPol(const Marble::GeoDataCoordinates&);

    [[nodiscard]] const auto& statWidget(Stat stat) const { return m_status.at(unsigned(stat)); }
    [[nodiscard]] auto& statWidget(Stat stat) { return m_status.at(unsigned(stat)); }

    void saveStatusBarState(QSettings&) const;
    void loadStatusBarState(QSettings&);

    [[nodiscard]] auto statActions() const; // just the actions: skip the headers
    [[nodiscard]] QIcon mapModeIcon() const;

    [[nodiscard]] static QString showHideMsg(bool checked, QString name);

    Ui::MainWindow*        ui;

    // Data
    QString                m_person;      // for power estimations

    // Config: must be initialized after the models, which it uses.
    AppConfig              m_appConfig;

    // Dialogs
    AboutDialog            m_aboutDialog;
    DocDialog              m_docDialog;
    ExportDialog           m_exportOpts;
    ImportDialog           m_importOpts;
    AreaDialog             m_areaDialog;
    PersonDialog           m_personDialog;
    DeviceDialog           m_deviceDialog;
    TrackSimplifyDialog    m_trackSimplifyDialog;
    GpsCaptureDialog       m_gpsCaptureDialog;
    MapDownloadDialog      m_mapDownloadDialog;
    NewWaypointDialog      m_newWaypointDialog;
    GpsdVersionDialog      m_gpsdVersionDialog;
    NewPaneDialog          m_newPaneDialog;
    NewTrackDialog         m_newTrackDialog;
    GotoLatLonDialog       m_gotoLatLonDialog;
    LaunchSplash           m_launchSplash;

    // AutoImport async process
    QProcess               m_autoImportProcess;
    QTimer                 m_autoImportTimer;
    QScopedPointer<QTemporaryFile> m_autoImportStdout;

    // UI things
    QElapsedTimer          m_progressTimer;
    QTimer                 m_autosaveTimer;             // autosave when timer fires
    QTimer                 m_actionUpdateTimer;         // update some expensive enables
    QMenu                  m_statusBarMenu;
    int                    m_progressValue   = 1;       // see progressTimerUpdate comment
    int                    m_progressMax     = 1;       // ...
    int                    m_gpsCaptureCount = 0;       // active GPSD captures
    MapMode                m_mapMode = MapMode::Move;   // current map mode (move, edit, etc)

    std::array<QVector<QWidget*>, int(Stat::_Count)> m_status;       // status bar widgets
    GeoPolRegionVec                                  m_mouseGeoPol;  // holds geopol region for mouse

    mutable QString                                  m_modelVisited; // last model location visited

    static const QString autoImportError;

    // Status bar QAction menu entry to hold a Stat value
    static const constexpr char* s_actionPropertyStat = "stat";
    static const constexpr char* s_statSeparator = "|";

    // Splash screen logo
    static const constexpr char* LogoHtml = R"logo(<p align="center"><img src=":art/logos/projects/ZTGPS-Large-360x263.jpg"></img></p>)logo";
};

template <class T> T* MainWindow::paneFactory(PaneClass paneClass) const
{
    return dynamic_cast<T*>(paneFactory(PaneClass_t(int(paneClass))));
}

#endif // MAINWINDOW_H
