/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "src/ui/panes/pane.h"
#include "src/ui/windows/mainwindow.h"
#include "src/ui/windows/appconfig.h"
#include "docdialog.h"

// Fields: indent level, heading text, HTML file
DocDialog::DocDialog(MainWindow& mainWindow) :
    DocDialogBase(&mainWindow),
    m_mainWindow(mainWindow)
{
    // Util::SetupWhatsThis(this); base class does this
}

DocDialog::~DocDialog()
{
}

void DocDialog::setupTOC()
{
    if (TOCList::isSetup()) // avoid redundant setups
        return;

    const AppConfig& appConf = m_mainWindow.getAppConfigDialog();

    setupTOC({
        { 0, tr("Introduction"),                  "Intro.html",               "configure" },
        { 0, tr("User Interface") },
           { 1,    tr("UI Basics"),               "Interface.html",           "interface" },
           { 1,    tr("New Pane Dialog"),         "NewPaneDialog.html",       "window-new" },
           { 1,    tr("Data Pane Features"),      "DataPanes.html",           "server-database" },
           { 1,    tr("Track Tags"),              "TrackTags.html",           "tag" },
           { 1,    tr("Map Modes"),               "MapModes.html",            "transform-move" },
           { 1,    tr("Status Bar"),              "StatusBar.html",           "kt-show-statusbar" },
           { 1,    tr("ToolBars"),                "Toolbars.html",            "configure-toolbars" },
           { 1,    tr("Color Themes"),            "Themes.html",              "preferences-desktop-plasma-theme" },
           { 1,    tr("Undo/Redo"),               "UndoRedo.html",            "edit-undo" },
           { 1,    tr("Rich Text"),               "RichText.html",            "text-enriched" },
        { 0, tr("Importing GPS Data") },
           { 1,    tr("From Files"),              "ImportFile.html",          "document-open" },
           { 1,    tr("From GPS Devices"),        "ImportDevice.html",        "kstars_satellites" },
           { 1,    tr("Live Capture (GPSD)"),     "ImportLive.html",          "kstars_satellites" },
           { 1,    tr("Auto Import"),             "AutoImport.html",          appConf.pageIcon(AppConfig::Page::AutoImport) },
        { 0, tr("Exporting GPS Data"),            "ExportFile.html",          "document-save" },
        { 0, tr("Queries") },
           { 1,    tr("Data Queries"),            "DataQueries.html",         "edit-find" },
           { 1,    tr("Area Searches"),           "AreaSearch.html",          "map-globe" },
           { 1,    tr("Map Queries"),             "GeoLocSearch.html",        "globe" },
        { 0, tr("Track Display") },
           { 1,    tr("Colors"),                  "TrackColorization.html",   "color-profile" },
           { 1,    tr("Notes"),                   "TrackNotes.html",          "edit-entry" },
           { 1,    tr("Flags"),                   "TrackFlags.html",          "flag" },
        { 0, tr("Panes") },
           { 1,    tr("Activity Summary"),        "ActivitySummaryPane.html", Pane::iconFile(PaneClass::ActivitySummary) },
           { 1,    tr("Climb Analysis"),          "ClimbAnalysisPane.html",   Pane::iconFile(PaneClass::ClimbAnalysis) },
           { 1,    tr("Filters"),                 "FilterPane.html",          Pane::iconFile(PaneClass::Filter) },
           { 1,    tr("GPS Device"),              "GpsDevPane.html",          Pane::iconFile(PaneClass::GpsDevice) },
           { 1,    tr("Line Chart"),              "LinePane.html",            Pane::iconFile(PaneClass::LineChart) },
           { 1,    tr("Map"),                     "MapPane.html",             Pane::iconFile(PaneClass::Map) },
           { 1,    tr("Simple View"),             "SimpleViewPane.html",      Pane::iconFile(PaneClass::SimpleView) },
           { 1,    tr("Track Comparison"),        "TrackCmpPane.html",        Pane::iconFile(PaneClass::CmpChart) },
           { 1,    tr("Track Details"),           "TrackDetailPane.html",     Pane::iconFile(PaneClass::TrackDetail) },
           { 1,    tr("Track Points"),            "PointPane.html",           Pane::iconFile(PaneClass::Points) },
           { 1,    tr("Training Zones"),          "ZonePane.html",            Pane::iconFile(PaneClass::ZoneSummary) },
           { 1,    tr("Track"),                   "TrackPane.html",           Pane::iconFile(PaneClass::Track) },
           { 1,    tr("View"),                    "ViewPane.html",            Pane::iconFile(PaneClass::View) },
           { 1,    tr("Waypoint"),                "WaypointPane.html",        Pane::iconFile(PaneClass::Waypoint) },
        { 0, tr("Track Editing") },
           { 1,    tr("Track Merging"),           "EditMerge.html",           "merge" },
           { 1,    tr("Segment Splitting"),       "EditSegments.html",        "split" },
           { 1,    tr("Simplifying"),             "EditSimplify.html",        "path-simplify" },
           { 1,    tr("Deleting Points"),         "EditDelete.html",          "delete" },
           { 1,    tr("Reversing Tracks"),        "EditReverse.html",         "path-reverse" },
           { 1,    tr("Changing Speed"),          "EditSpeed.html",           "speedometer" },
           { 1,    tr("Creating New Tracks"),     "NewTrack.html",            "createpath" },
        { 0, tr("Configuration") },
           { 1,    tr("Offline Mode"),            "OfflineMode.html",         "offline" },
           { 1,    tr("Tags"),                    "ConfigTags.html",          appConf.pageIcon(AppConfig::Page::Tags) },
           { 1,    tr("Units"),                   "ConfigUnits.html",         appConf.pageIcon(AppConfig::Page::UnitsMisc) },
           { 1,    tr("People"),                  "ConfigPeople.html",        appConf.pageIcon(AppConfig::Page::People) },
           { 1,    tr("AutoImport"),              "ConfigAutoImport.html",    appConf.pageIcon(AppConfig::Page::AutoImport) },
           { 1,    tr("Colorizers"),              "ConfigColorizer.html",     appConf.pageIcon(AppConfig::Page::TrackColorizer) },
        { 0, tr("Bugs & Limitations"),            "Bugs.html",                "script-error" },
        { 0, tr("Roadmap"),                       "Roadmap.html",             "content-loading-symbolic" },
    });
}
