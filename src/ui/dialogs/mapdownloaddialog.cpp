/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <chrono>

#include <marble/GeoDataCoordinates.h>
#include <marble/GeoDataLatLonBox.h>
#include <marble/GeoDataLatLonAltBox.h>
#include <marble/TileCoordsPyramid.h>

#include <QPoint>
#include <QRect>
#include <QColor>
#include <QMessageBox>

#include <src/util/icons.h>
#include <src/util/ui.h>

#include "src/ui/windows/mainwindow.h"
#include "src/ui/panes/mappane.h"
#include "src/core/app.h"

#include "mapdownloaddialog.h"
#include "ui_mapdownloaddialog.h"

MapDownloadDialog::MapDownloadDialog(const MainWindow& mainWindow) :
    QDialog(const_cast<MainWindow*>(&mainWindow)),
    m_mainWindow(mainWindow),
    m_lastMapPane(nullptr),
    ui(new Ui::MapDownloadDialog)
{
    ui->setupUi(this);

    setupActionIcons();
    setupTimers();
    Util::SetupWhatsThis(this);
}

MapDownloadDialog::~MapDownloadDialog()
{
    delete ui;
}

void MapDownloadDialog::setupActionIcons()
{
    Icons::defaultIcon(ui->levelRefresh, "view-refresh");
}

void MapDownloadDialog::setupTimers()
{
    m_tileCountTimer.setSingleShot(true);
    connect(&m_tileCountTimer, &QTimer::timeout, this, &MapDownloadDialog::updateTileCount);
}

void MapDownloadDialog::updateWidgets()
{
    if (ui == nullptr)
        return;

    const bool enableBounds = ui->btnSpecifiedArea->isChecked();

    ui->degN->setEnabled(enableBounds);
    ui->degS->setEnabled(enableBounds);
    ui->degW->setEnabled(enableBounds);
    ui->degE->setEnabled(enableBounds);

    ui->degN->setMinimum(ui->degS->value() + delta);
    ui->degS->setMaximum(ui->degN->value() - delta);
    ui->degE->setMinimum(ui->degW->value() + delta);
    ui->degW->setMaximum(ui->degE->value() - delta);

    ui->tileMin->setRange(minTileLevelFromMap(), ui->tileMax->value());
    ui->tileMax->setRange(ui->tileMin->value(), maxTileLevelFromMap());

    // Zoom control bounds, if there's a map pane
    if (auto* mapPane = m_mainWindow.findPane<MapPane>(); mapPane != nullptr) {
        ui->levelRefresh->setEnabled(true);
    } else {
        ui->levelRefresh->setEnabled(false);
    }
}

void MapDownloadDialog::resetWidgetBounds()
{
    ui->degN->setRange(-90.0, 90.0);
    ui->degS->setRange(-90.0, 90.0);
    ui->degW->setRange(-180.0, 180.0);
    ui->degE->setRange(-180.0, 180.0);

    ui->tileMin->setRange(minTileLevel, maxTileLevel);
    ui->tileMax->setRange(minTileLevel, maxTileLevel);
}

QPoint MapDownloadDialog::pointFromCoords(double latDeg, double lonDeg, int level)
{
    // This is taken from Marble's TileId::fromCoordinates, with some changes.  It seems quite an awkward
    // interface that exposes implementation details, but this is what MarbleMap::downloadRegion() requires.
    // It's not well documented in the Marble docs, so this code was lifted from the KDE Marble source.

    if (level < 0)
        return { };

    const int size = 1000000;
    const int maxLat = 90 * size;
    const int maxLon = 180 * size;

    int lat = Marble::GeoDataCoordinates::normalizeLat(latDeg, Marble::GeoDataCoordinates::Degree) * size;
    int lon = Marble::GeoDataCoordinates::normalizeLon(lonDeg, Marble::GeoDataCoordinates::Degree) * size;
    int x = 0;
    int y = 0;

    for (int i=0; i<level; ++i) {
        const int deltaLat = maxLat >> i;
        if (lat <= (maxLat - deltaLat)) {
            y += 1<<(level-i-1);
            lat += deltaLat;
        }
        const int deltaLon = maxLon >> i;
        if (lon >= (maxLon - deltaLon)) {
            x += 1<<(level-i-1);
        } else {
            lon += deltaLon;
        }
    }

    return { x, y };
}

QRect MapDownloadDialog::rectFromArea(int level) const
{
    return { pointFromCoords(ui->degN->value(), ui->degW->value(), level),
             pointFromCoords(ui->degS->value(), ui->degE->value(), level) };
}

auto MapDownloadDialog::requestedRegion() const
{
    QVector<Marble::TileCoordsPyramid> region; // this is the format Marble wants for MarbleMap::downloadRegion

    Marble::TileCoordsPyramid coordsPyramid(ui->tileMin->value(), ui->tileMax->value());
    const QRect coordsRect(rectFromArea(ui->tileMax->value()));
    coordsPyramid.setBottomLevelCoords(coordsRect);

    if (coordsRect.isValid())
        region.push_back(coordsPyramid);

    return region;
}

qint64 MapDownloadDialog::tileCount(const QVector<Marble::TileCoordsPyramid>& region)
{
    qint64 count = 0;
    for (const auto& tcp : region)
        count += tcp.tilesCount();

    return count;
}

bool MapDownloadDialog::download()
{
    // Coordinates to download, in Marble's "list of pyramidal integer coordinates" scheme.
    const auto region = requestedRegion();

    const int count = tileCount(region);
    if (count >= maxTilesCount) {
        QMessageBox(QMessageBox::Critical, tr("Tile count error"),
                    tr("Please download fewer tiles."), QMessageBox::Ok).exec();
        return false;
    }

    if (count >= warnTilesCount) {
        if (QMessageBox(QMessageBox::Warning, tr("Tile count warning"),
                        tr("Downloading many tiles creates a high load on the service. Do you wish to continue?"),
                        QMessageBox::Ok | QMessageBox::Cancel).exec() != QMessageBox::Ok)
            return false;
    }

    // start the download
    if (auto* mapPane = m_mainWindow.findPane<MapPane>(); mapPane != nullptr) {
        mapPane->downloadTiles(region);
        return true;
    }

    return false;
}

void MapDownloadDialog::updateTileCount()
{
    const qint64 count = tileCount(requestedRegion());

    QColor color = QGuiApplication::palette().color(QPalette::Text);

    if (count >= maxTilesCount) {
        ui->downloadTileCount->setText(tr("Excessive"));
        color = cfgData().uiColor[UiType::Critical].name();
    } else {
        if (count >= warnTilesCount) color = cfgData().uiColor[UiType::Error].name();
        else if (count >= noteTilesCount) color = cfgData().uiColor[UiType::Warning].name();

        ui->downloadTileCount->setNum(int(count));
    }

    Util::SetWidgetStyle(ui->downloadTileCount, color);
}

void MapDownloadDialog::updateSignals()
{
    disconnectSignals();

    if (m_lastMapPane = m_mainWindow.findPane<MapPane>(); m_lastMapPane != nullptr)
        connect(m_lastMapPane, &MapPane::visibleAreaChanged, this, &MapDownloadDialog::mapVisibleAreaChanged);
}

void MapDownloadDialog::disconnectSignals()
{
    if (m_lastMapPane != nullptr)
        disconnect(m_lastMapPane, nullptr, nullptr, nullptr);

    m_lastMapPane = nullptr;
}

void MapDownloadDialog::updateToMapBounds()
{
    if (auto* mapPane = m_mainWindow.findPane<MapPane>(); mapPane != nullptr)
        setBounds(mapPane->bounds());
}

void MapDownloadDialog::on_degN_valueChanged(double lat)
{
    ui->degS->setMaximum(lat - delta);
    deferredTileCountRecalc();
}

void MapDownloadDialog::on_degS_valueChanged(double lat)
{
    ui->degN->setMinimum(lat + delta);
    deferredTileCountRecalc();
}

void MapDownloadDialog::on_degW_valueChanged(double lon)
{
    ui->degE->setMinimum(lon + delta);
    deferredTileCountRecalc();
}

void MapDownloadDialog::on_degE_valueChanged(double lon)
{
    ui->degW->setMaximum(lon - delta);
    deferredTileCountRecalc();
}

void MapDownloadDialog::on_tileMin_valueChanged(int value)
{
    ui->tileMax->setMinimum(std::max(value, minTileLevelFromMap()));
    deferredTileCountRecalc();
}

void MapDownloadDialog::on_tileMax_valueChanged(int value)
{
    ui->tileMin->setMaximum(std::min(value, maxTileLevelFromMap()));
    deferredTileCountRecalc();
}

void MapDownloadDialog::on_btnVisibleRegion_clicked()
{
    updateWidgets();
    deferredTileCountRecalc();
}

void MapDownloadDialog::on_btnSpecifiedArea_clicked()
{
    updateToMapBounds();
    updateWidgets();
    deferredTileCountRecalc();
}

void MapDownloadDialog::save(QSettings& settings) const
{
    SL::Save(settings, "degN", ui->degN);
    SL::Save(settings, "degS", ui->degS);
    SL::Save(settings, "degW", ui->degW);
    SL::Save(settings, "degE", ui->degE);
    SL::Save(settings, "tileMin", ui->tileMin);
    SL::Save(settings, "tileMax", ui->tileMax);
    SL::Save(settings, "visibleRegion", ui->btnVisibleRegion);
    SL::Save(settings, "specifiedArea", ui->btnSpecifiedArea);
}

void MapDownloadDialog::load(QSettings& settings)
{
    resetWidgetBounds();

    SL::Load(settings, "degN", ui->degN);
    SL::Load(settings, "degS", ui->degS);
    SL::Load(settings, "degW", ui->degW);
    SL::Load(settings, "degE", ui->degE);
    SL::Load(settings, "tileMin", ui->tileMin);
    SL::Load(settings, "tileMax", ui->tileMax);
    SL::Load(settings, "visibleRegion", ui->btnVisibleRegion);
    SL::Load(settings, "specifiedArea", ui->btnSpecifiedArea);

    updateWidgets();
}

void MapDownloadDialog::setBounds(const Marble::GeoDataLatLonBox& bounds)
{
    if (ui == nullptr)
        return;

    resetWidgetBounds();

    ui->degN->setValue(bounds.north(Marble::GeoDataCoordinates::Degree));
    ui->degS->setValue(bounds.south(Marble::GeoDataCoordinates::Degree));
    ui->degW->setValue(bounds.west(Marble::GeoDataCoordinates::Degree));
    ui->degE->setValue(bounds.east(Marble::GeoDataCoordinates::Degree));

    updateWidgets();
    deferredTileCountRecalc();
}

void MapDownloadDialog::mapVisibleAreaChanged(const Marble::GeoDataLatLonAltBox& bounds)
{
    if (ui->btnVisibleRegion->isChecked())
        setBounds(Marble::GeoDataLatLonBox(bounds));
}

void MapDownloadDialog::updateCurrentTileZoom()
{
    // Default tile min/max
    if (auto* mapPane = m_mainWindow.findPane<MapPane>(); mapPane != nullptr) {
        const int currentTileLevel = std::clamp(mapPane->currentTileLevel(), minTileLevel, maxTileLevel);
        ui->tileMin->setMaximum(currentTileLevel);
        ui->tileMax->setMinimum(currentTileLevel);
        ui->tileMin->setValue(currentTileLevel);
        ui->tileMax->setValue(currentTileLevel);
    }
}

void MapDownloadDialog::deferredTileCountRecalc()
{
    using namespace std::chrono_literals;

    if (isVisible())
        m_tileCountTimer.start(250ms);
}

void MapDownloadDialog::showEvent(QShowEvent* event)
{
    QDialog::showEvent(event);

    updateSignals();

    // Update tile zoom if visible region is selected
    if (ui->btnVisibleRegion->isChecked() || ui->tileMax->value() == 1)
        updateCurrentTileZoom();

    // Update bounds on dialog show if that's selected
    if (ui->btnVisibleRegion->isChecked() || ui->degN->value() == 0.0)
        updateToMapBounds();

    updateWidgets();
}

void MapDownloadDialog::hideEvent(QHideEvent* event)
{
    QDialog::hideEvent(event);

    disconnectSignals();
}

int MapDownloadDialog::maxTileLevelFromMap() const
{
    if (auto* mapPane = m_mainWindow.findPane<MapPane>(); mapPane != nullptr)
        return std::min(maxTileLevel, mapPane->maximumTileLevel());

    return maxTileLevel;
}

int MapDownloadDialog::minTileLevelFromMap() const
{
    if (auto* mapPane = m_mainWindow.findPane<MapPane>(); mapPane != nullptr)
        return std::max(minTileLevel, mapPane->minimumTileLevel());

    return minTileLevel;
}

void MapDownloadDialog::on_levelRefresh_clicked()
{
    updateCurrentTileZoom();
    deferredTileCountRecalc();
}

void MapDownloadDialog::on_buttonBox_clicked(QAbstractButton* button)
{
    if (m_mainWindow.findPane<MapPane>() == nullptr) {
        QMessageBox(QMessageBox::Critical, tr("Map download error"),
                    tr("No map pane found"), QMessageBox::Ok).exec();
        return;
    }

    if (ui->buttonBox->buttonRole(button) == QDialogButtonBox::AcceptRole) {
        if (download())
            accept();
    } else if (ui->buttonBox->buttonRole(button) == QDialogButtonBox::RejectRole) {
        reject();
    } else if (ui->buttonBox->buttonRole(button) == QDialogButtonBox::ApplyRole) {
        download();
    }
}
