/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef IMPORTDIALOG_H
#define IMPORTDIALOG_H

#include <QDialog>
#include <src/core/settings.h>

#include "src/dev-io/importinfo.h"
#include "src/geo-io/geoioparams.h"

namespace Ui {
class ImportDialog;
} // namespace Ui

class QShowEvent;
class TagSelector;
class MainWindow;
enum class GeoIoFeature;

class ImportDialog final : public QDialog, public Settings
{
    Q_OBJECT

public:
    explicit ImportDialog(MainWindow&);
    ~ImportDialog() override;

    GeoLoadParams geoLoadParams() const;

    ImportInfoList getImportFilenames(const char* filter);

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

    static const constexpr char* defaultRouteTag = "Route";

private slots:
    void askTrackColor();
    void selectRouteTag();
    void updateActions();

private:
    friend class TestZtgps; // test hook

    void          showEvent(QShowEvent*) override;
    GeoIoFeature  features()    const;
    QStringList   tags()        const;
    const QString routeTag()    const;
    QColor        trackColor()  const; // returns invalid color if no override
    bool          deduplicate() const;
    void          setRouteTag(const QString&);

    static const QString&      filterTrk();
    static const QString&      filterWpt();
    static Qt::CaseSensitivity filterCase();

    void setupSignals();

    Ui::ImportDialog *ui;
    const MainWindow& m_mainWindow;
    TagSelector*      m_tagSelector; // UI adopts, so we don't free it.
    QString           m_routeTag;    // route tag to use
    QString           defaultDir;    // which directory to show
};

#endif // IMPORTDIALOG_H
