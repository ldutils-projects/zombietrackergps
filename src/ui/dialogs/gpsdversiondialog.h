/*
    Copyright 2021-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GPSDVERSIONDIALOG_H
#define GPSDVERSIONDIALOG_H

#include <QDialog>
#include <QHeaderView>
#include <QStandardItemModel>

namespace Ui {
class GpsdVersionDialog;
} // namespace Ui

class MainWindow;

class GpsdVersionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit GpsdVersionDialog(MainWindow&);
    ~GpsdVersionDialog() override;

private:
    friend class TestZtgps;
    void showEvent(QShowEvent*) override;
    void setup();
    void setupList();
    void populateList();

    Ui::GpsdVersionDialog *ui;
    QHeaderView           m_headerView;
    QStandardItemModel    m_gpsdVersions;
};

#endif // GPSDVERSIONDIALOG_H
