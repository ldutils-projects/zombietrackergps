/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QColorDialog>
#include <QDir>
#include <QFileDialog>
#include <QFileInfo>

#include <src/util/util.h>
#include <src/util/ui.h>

#include "importdialog.h"
#include "ui_importdialog.h"

#include "src/geo-io/geoiodecls.h"     // for GeoIoFeature
#include "src/ui/windows/mainwindow.h"
#include "src/ui/dialogs/tagselectordialog.h"
#include "src/ui/widgets/tagselector.h"


ImportDialog::ImportDialog(MainWindow& mainWindow) :
    QDialog(&mainWindow),
    ui(new Ui::ImportDialog),
    m_mainWindow(mainWindow),
    m_tagSelector(new TagSelector(mainWindow)),
    m_routeTag(defaultRouteTag),
    defaultDir(QDir::home().absolutePath())
{
    ui->setupUi(this);
    ui->autoAssignTagsGroupLayout->insertWidget(0, m_tagSelector);

    Util::SetTBColor(ui->trackColor, QRgb(0x00b155));

    setupSignals();
    setRouteTag(defaultRouteTag);
    Util::SetupWhatsThis(this);
}

ImportDialog::~ImportDialog()
{
    delete ui;
    // m_tagSelector is adopted by the UI, so we don't delete it ourselves.
}

GeoLoadParams ImportDialog::geoLoadParams() const
{
    bool useRouteTag = (ui == nullptr ? false : ui->applyRouteTag->isChecked());

    return GeoLoadParams(features(), tags(),
                         useRouteTag ? QStringList(routeTag()) + tags() : tags(),
                         tags(), // use track tags for waypoints too
                         trackColor(), deduplicate(),
                         filterTrk(), filterWpt(), filterCase());
}

GeoIoFeature ImportDialog::features() const
{
    if (ui == nullptr)
        return GeoIoFeature::None;

    return (ui->btnTrk->isChecked()    ? GeoIoFeature::Trk :    GeoIoFeature::None) |
           (ui->btnRte->isChecked()    ? GeoIoFeature::Rte :    GeoIoFeature::None) |
           (ui->btnWpt->isChecked()    ? GeoIoFeature::Wpt :    GeoIoFeature::None) |
           (ui->btnAuxRte->isChecked() ? GeoIoFeature::AuxRte : GeoIoFeature::None) |
           (ui->btnAuxTrk->isChecked() ? GeoIoFeature::AuxTrk : GeoIoFeature::None);
}

void ImportDialog::setupSignals()
{
    connect(ui->overrideTagColors, &QCheckBox::stateChanged, this, &ImportDialog::updateActions);
    connect(ui->applyRouteTag, &QCheckBox::stateChanged, this, &ImportDialog::updateActions);

    connect(ui->trackColor, &QToolButton::clicked, this, &ImportDialog::askTrackColor);
    connect(ui->routeTag, &QToolButton::clicked, this, &ImportDialog::selectRouteTag);
}

QStringList ImportDialog::tags() const
{
    return m_tagSelector->tags();
}

const QString ImportDialog::routeTag() const
{
    return m_routeTag;
}

QColor ImportDialog::trackColor() const
{
    if (ui == nullptr)
        return QColor();

    return ui->overrideTagColors->isChecked() ? Util::GetTBColor(ui->trackColor) : QColor();
}

bool ImportDialog::deduplicate() const
{
    if (ui == nullptr)
        return false;

    return ui->deduplicate->isChecked();
}

void ImportDialog::setRouteTag(const QString& tag)
{
    if (ui == nullptr)
        return;

    // If tag isn't found, display broken icon. User must re-select a valid one.
    if (!cfgData().tags.contains(tag)) {
        ui->routeTag->setIcon(QIcon(cfgData().brokenIcon));
        return;
    }

    m_routeTag = tag;
    ui->routeTag->setIcon(QIcon(cfgData().tags.tagIconName(tag)));
}

const QString& ImportDialog::filterTrk()
{
    static const QString empty;
    return empty;
}

const QString& ImportDialog::filterWpt()
{
    static const QString empty;
    return empty;
}

Qt::CaseSensitivity ImportDialog::filterCase()
{
    return Qt::CaseInsensitive;
}

ImportInfoList ImportDialog::getImportFilenames(const char* filter)
{
    // Test hook: bypass modal dialog under test suite.
    if (appBase().testing())
        return ImportInfoList();

    const QStringList trackFiles =
            QFileDialog::getOpenFileNames(parentWidget(),
                                          tr("Import GPS Track File(s)"), defaultDir,
                                          filter,  nullptr, QFileDialog::ReadOnly);

    if (!trackFiles.isEmpty())
        defaultDir = QFileInfo(trackFiles.front()).path();

    return ImportInfoList(trackFiles);
}

void ImportDialog::askTrackColor()
{
    Util::SetTBColor(ui->trackColor,
                     QColorDialog::getColor(trackColor(), this, tr("Track Override Color")));
}

void ImportDialog::selectRouteTag()
{
    TagSelectorDialog tagSelector(m_mainWindow, this);
    tagSelector().setMaxTags(1).setTags({routeTag()});

    if (tagSelector.exec() == QDialog::Accepted)
        if (!tagSelector().tags().empty())
            setRouteTag(tagSelector().tags().first());
}

void ImportDialog::updateActions()
{
    ui->trackColor->setEnabled(ui->overrideTagColors->isChecked());
    ui->routeTag->setEnabled(ui->applyRouteTag->isChecked());
}

void ImportDialog::showEvent(QShowEvent* event)
{
    updateActions();
    setRouteTag(m_routeTag);
    QDialog::showEvent(event);
}

void ImportDialog::save(QSettings& settings) const
{
    MemberSave(settings, defaultDir);

    if (ui == nullptr)
        return;

    SL::Save(settings, "trackColor",    trackColor());
    SL::Save(settings, "deduplicate",   ui->deduplicate);
    SL::Save(settings, "importTrk",     ui->btnTrk);
    SL::Save(settings, "importRte",     ui->btnRte);
    SL::Save(settings, "importWpt",     ui->btnWpt);
    SL::Save(settings, "auxRte",        ui->btnAuxRte);
    SL::Save(settings, "auxTrk",        ui->btnAuxTrk);
    SL::Save(settings, "applyRouteTag", ui->applyRouteTag);
    SL::Save(settings, "routeTag",      routeTag());

    // Don't save ui->overrrideTagColors; that's auto-generated on load from
    // the valid or invalid color.
}

void ImportDialog::load(QSettings& settings)
{
    MemberLoad(settings, defaultDir);

    if (ui != nullptr) {
        const QColor trackColor = SL::Load(settings, "trackColor", QColor());

        Util::SetTBColor(ui->trackColor, trackColor);
        ui->overrideTagColors->setChecked(trackColor.isValid());
        SL::Load(settings, "deduplicate",   ui->deduplicate);
        SL::Load(settings, "importTrk",     ui->btnTrk);
        SL::Load(settings, "importRte",     ui->btnRte);
        SL::Load(settings, "importWpt",     ui->btnWpt);
        SL::Load(settings, "auxRte",        ui->btnAuxRte);
        SL::Load(settings, "auxTrk",        ui->btnAuxTrk);
        SL::Load(settings, "applyRouteTag", ui->applyRouteTag);
        setRouteTag(SL::Load(settings, "routeTag", QString(defaultRouteTag)));

        updateActions();
    }
}
