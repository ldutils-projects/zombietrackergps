/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAPTHEMEDIALOG_H
#define MAPTHEMEDIALOG_H

#include <QDialog>
#include <marble/MapThemeManager.h>

namespace Ui {
class MapThemeDialog;
} // namespace Ui

class MainWindow;

class MapThemeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MapThemeDialog(MainWindow&, QWidget* parent = nullptr);
    ~MapThemeDialog() override;

signals:
    void themeSelected(const QString& mapTheme);

private slots:
    void selectThemeIndex(const QModelIndex& index);

private:
    void setupView();
    void setupSignals();

    Ui::MapThemeDialog*     ui;
    MainWindow&             m_mainWindow;
    Marble::MapThemeManager m_mapThemeManager;
};

#endif // MAPTHEMEDIALOG_H
