/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QColorDialog>
#include <QLocale>
#include <QDateTime>

#include <src/util/util.h>
#include <src/util/ui.h>
#include <src/undo/undomgr.h>

#include "src/ui/windows/mainwindow.h"
#include "src/core/app.h"
#include "src/core/mapdatamodel.h"
#include "src/core/pointmodel.h"

#include "newtrackdialog.h"
#include "ui_newtrackdialog.h"

NewTrackDialog::NewTrackDialog(MainWindow& mainWindow) :
    QDialog(&mainWindow),
    ui(new Ui::NewTrackDialog),
    m_tagSelector(new TagSelector(mainWindow)),
    m_mainWindow(mainWindow)
{
    ui->setupUi(this);
    ui->autoAssignTagsGroupLayout->insertWidget(0, m_tagSelector);

    setupSignals();
    defaultTrackName();
    Util::SetupWhatsThis(this);

    Util::SetTBColor(ui->trackColor, QRgb(0x00b155));
}

NewTrackDialog::~NewTrackDialog()
{
    delete ui;
    // tagSelector is adopted by the UI, so we don't delete it ourselves.
}

void NewTrackDialog::setupSignals()
{
    connect(ui->overrideTagColors, &QCheckBox::stateChanged, ui->trackColor, &QWidget::setEnabled);

    connect(ui->trackColor, &QToolButton::clicked, this, &NewTrackDialog::askTrackColor);
}

QStringList NewTrackDialog::tags() const
{
    return m_tagSelector->tags();
}

QColor NewTrackDialog::trackColor() const
{
    return ui->overrideTagColors->isChecked() ? Util::GetTBColor(ui->trackColor) : QColor();
}

void NewTrackDialog::askTrackColor()
{
    Util::SetTBColor(ui->trackColor,
                     QColorDialog::getColor(trackColor(), this, tr("Track Override Color")));
}

QString NewTrackDialog::trackName() const
{
    return ui->trackName->text();
}

TrackType NewTrackDialog::trackType() const
{
    if (ui == nullptr)
        return TrackType::Trk;

    return ui->btnTrk->isChecked() ? TrackType::Trk : TrackType::Rte;
}

void NewTrackDialog::setTrackName(const QString& name)
{
    ui->trackName->setText(name);
}

void NewTrackDialog::defaultTrackName()
{
    const QString candidateName =
            tr("Current Track: ") + QLocale().toString(QDateTime::currentDateTime(),
                                                       "dd MMM yyyy HH:mm");

    setTrackName(candidateName);
}

QModelIndex NewTrackDialog::appendRow(TrackModel& trackModel, const QString& undoName) const
{
    // Don't create undo scope until after handling cancel above.
    const UndoMgr::ScopedUndo undoSet(app().undoMgr(), undoName);

    // Create a new track to capture to
    const QModelIndex newRow = trackModel.appendRow(trackName(), trackType(), tags(), trackColor());

    if (PointModel* pm = trackModel.geoPoints(newRow); pm != nullptr) {
        pm->insertRows(0, 1); // insert single empty segment
        return newRow;
    }

    m_mainWindow.statusMessage(UiType::Error, tr("Error adding track"));
    return { };
}

QModelIndex NewTrackDialog::exec(TrackModel& trackModel, const QString& undoName)
{
    defaultTrackName(); // set for current date

    const int rc = exec();

    if (rc != QDialog::Accepted) {
        m_mainWindow.statusMessage(UiType::Warning, tr("Canceled"));
        return { };
    }

    return appendRow(trackModel, undoName);
}

void NewTrackDialog::save(QSettings& settings) const
{
    if (ui != nullptr) {
        SL::Save(settings, "trackColor",  trackColor());
        SL::Save(settings, "createTracks", ui->btnTrk);
    }
}

void NewTrackDialog::load(QSettings& settings)
{
    if (ui != nullptr) {
        const QColor trackColor = SL::Load(settings, "trackColor", QColor());

        Util::SetTBColor(ui->trackColor, trackColor);
        ui->overrideTagColors->setChecked(trackColor.isValid());

        SL::Load(settings, "createTracks", ui->btnTrk);
    }
}
