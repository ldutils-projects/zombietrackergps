/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NEWVIEWDIALOG_H
#define NEWVIEWDIALOG_H

#include <QDialog>
#include <src/ui/dialogs/iconselector.h>

#include "src/core/viewmodel.h"
#include "src/core/viewparams.h"

namespace Ui {
class NewViewDialog;
} // namespace Ui

class QString;
class MainWindow;
class QRadioButton;

class NewViewDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewViewDialog(MainWindow&, const ViewParams&, QWidget *parent = nullptr);
    ~NewViewDialog() override;

    QString getViewName() const;  // user supplied name
    QString getViewIcon() const;  // which flag to use

private slots:
    void selectIcon();
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

private:
    void populateFlagChoices();
    void setupSignals();

    Ui::NewViewDialog* ui;

    MainWindow&    m_mainWindow;   // main window
    QRadioButton*  m_checked;      // which radio button is checked
    IconSelector   m_iconSelector; // icon selector
    int            m_selectRc;     // icon delector dialog accept state
    bool           m_userEdited;   // set when user edits
    ViewParams     m_viewParams;   // view parameters

    static const constexpr char* flagNameProperty = "FlagName";
};

#endif // NEWVIEWDIALOG_H
