/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>

#include <QDateTime>
#include <QMessageBox>

#include <src/util/ui.h>
#include <src/util/icons.h>
#include <src/undo/undomgr.h>
#include <src/ui/dialogs/iconselector.h>

#include "src/ui/windows/mainwindow.h"
#include "src/ui/panes/waypointpane.h" // for iconSelectorPaths()
#include "src/core/cfgdata.h"
#include "src/core/geopolmgr.h"
#include "src/core/waypointitem.h"
#include "src/core/app.h"

#include "newwaypointdialog.h"
#include "ui_newwaypointdialog.h"

NewWaypointDialog::NewWaypointDialog(MainWindow& mainWindow) :
    QDialog(&mainWindow),
    m_origLat(0.0),
    m_origLon(0.0),
    m_origEle(0.0),
    m_iconSelector(nullptr),
    m_mainWindow(mainWindow),
    ui(new Ui::NewWaypointDialog)
{
    ui->setupUi(this);

    setupSymbols();
    setupSignals();
    setupActionIcons();

    Util::SetupWhatsThis(this);
}

NewWaypointDialog::~NewWaypointDialog()
{
    delete ui;
}

void NewWaypointDialog::setupSymbols()
{
    const auto& symToPictogram = WaypointItem::pictogramMap(WaypointModel::Symbol);

    for (auto it = symToPictogram.begin(); it != symToPictogram.end(); ++it)
        if (!it.value().isEmpty())
            ui->wptSymbol->addItem(QIcon(it.value()), WaypointItem::symbolCase(it.key()));
}

void NewWaypointDialog::setupSignals()
{
    connect(ui->wptSymbol, &QComboBox::currentTextChanged, this, &NewWaypointDialog::updateWptIcon);
    connect(ui->wptType,   &QLineEdit::textChanged, this, &NewWaypointDialog::updateWptIcon);
}

void NewWaypointDialog::setupActionIcons()
{
    Icons::defaultIcon(ui->btnRefreshName, "view-refresh");
    Icons::defaultIcon(ui->btnRefreshPos,  "view-refresh");
}

void NewWaypointDialog::updateSuggestedName()
{
    const QStringList names = app().geoPolMgr()
                              .intersectionNames(std::make_pair(ui->wptLon->value(), ui->wptLat->value()),
                                                 GeoPolRegion::IncludeIfNoOther);

    ui->wptName->setText(names.join('/'));
}

int NewWaypointDialog::exec(double lat, double lon, double ele, bool genName)
{
    return exec({ Marble::GeoDataCoordinates(lon, lat, ele, Marble::GeoDataCoordinates::Degree) }, genName);
}

int NewWaypointDialog::exec(const QVector<Marble::GeoDataCoordinates>& points, bool genName)
{
    if (points.empty())
        return QDialog::Rejected;

    m_points = points;

    const Units& eleUnits = cfgData().unitsElevation;

    ui->wptLat->setValue(m_origLat = points[0].latitude(Marble::GeoDataCoordinates::Degree));
    ui->wptLon->setValue(m_origLon = points[0].longitude(Marble::GeoDataCoordinates::Degree));
    ui->wptEle->setValue(m_origEle = eleUnits.to(points[0].altitude()).toDouble());

    // Use configured units
    ui->wptEle->setSuffix(QString(" ") + eleUnits.suffix(m_origEle));

    // Hide position widgets if there are multiple input points
    const bool showPosWidgets = (points.size() == 1);
    ui->posWidget->setVisible(showPosWidgets);
    ui->labelPos->setVisible(showPosWidgets);

    if (genName)
        updateSuggestedName();

    return QDialog::exec();
}

QString NewWaypointDialog::wptName() const
{
    return ui->wptName->text();
}

QString NewWaypointDialog::wptSymbol() const
{
    return ui->wptSymbol->currentText();
}

QString NewWaypointDialog::wptIconName() const
{
    return m_iconPath;
}

QStringList NewWaypointDialog::wptTags() const
{
    (void)this; // temporary, to make clang-tidy happy until this is implemented
    return { }; // TODO: ...
}

void NewWaypointDialog::on_wptIcon_clicked()
{
    m_iconSelector->setGeometry(Util::MapOnScreen(ui->wptIcon, ui->wptIcon->pos(), m_iconSelector->size()));
    m_iconSelector->setCurrentPath(m_iconPath);

    if (m_iconSelector->exec() == QDialog::Accepted)
        setIconPath(m_iconSelector->iconFile());
}

void NewWaypointDialog::on_btnRefreshName_clicked()
{
    updateSuggestedName();
}

void NewWaypointDialog::on_btnRefreshPos_clicked()
{
    if (m_points.empty())
        return;

    ui->wptLat->setValue(m_origLat);
    ui->wptLon->setValue(m_origLon);
    ui->wptEle->setValue(m_origEle);
}

void NewWaypointDialog::updateWptIcon()
{
    if (!ui->wptIconFromSymbol->isChecked())  // don't update if not asked to
        return;

    if (const QString iconPath = WaypointItem::guessIcon(ui->wptSymbol->currentText(), ui->wptType->text());
        !iconPath.isNull()) {
        setIconPath(iconPath);
    }
}

void NewWaypointDialog::setIconPath(const QString& path)
{
    m_iconPath = path;

    if (m_iconSelector != nullptr) {
        m_iconSelector->setCurrentPath(m_iconPath);
        ui->wptIcon->setIcon(m_iconSelector->icon());
    }
}

void NewWaypointDialog::showEvent(QShowEvent* event)
{
    if (m_iconSelector == nullptr)
        m_iconSelector = IconSelector::iconSelector(WaypointPane::iconSelectorPathsStatic(),
                                                    WaypointPane::noColorNames);

    if (m_iconSelector == nullptr) {
        assert(0 && "failed to create IconSelector");
        return;
    }

    if (ui->wptIconFromSymbol->isChecked())
        updateWptIcon();
    else
        setIconPath(m_iconPath);

    QDialog::showEvent(event);
}

void NewWaypointDialog::save(QSettings& settings) const
{
    SL::Save(settings, "wptName", ui->wptName);
    SL::Save(settings, "wptSymbol", ui->wptSymbol);
    SL::Save(settings, "wptType", ui->wptType);
    SL::Save(settings, "wptIconFromSymbol", ui->wptIconFromSymbol);
    SL::Save(settings, "iconpath", m_iconPath);
}

void NewWaypointDialog::load(QSettings& settings)
{
    SL::Load(settings, "wptName", ui->wptName);
    SL::Load(settings, "wptSymbol", ui->wptSymbol);
    SL::Load(settings, "wptType", ui->wptType);
    SL::Load(settings, "wptIconFromSymbol", ui->wptIconFromSymbol);
    SL::Load(settings, "iconpath", m_iconPath);
}

void NewWaypointDialog::on_buttonBox_accepted()
{
    if (wptName().isEmpty() || m_iconSelector == nullptr)
        return on_buttonBox_rejected();

    // Prompt on possible excess
    if (m_points.size() > 10) {
        if (Util::WarningDialog(m_mainWindow, tr("Confirm waypoint creation"),
                                tr("You are creating many waypoints at once.  Proceed?")) == QMessageBox::Cancel)
            return on_buttonBox_rejected();
    }

    // Friendly undo name
    const QString undoName = UndoMgr::genName(tr("Add"), m_points.size(), WaypointModel::getItemNameStatic());
    const UndoMgr::ScopedUndo undoSet(app().undoMgr(), undoName);

    // Add all waypoints
    for (const auto& pt : m_points)
        app().waypointModel().appendRow(wptName(),
                                        pt.latitude(Marble::GeoDataCoordinates::Degree),
                                        pt.longitude(Marble::GeoDataCoordinates::Degree),
                                        pt.altitude(),
                                        QStringList(), QString(), QString(),
                                        wptSymbol(), tr("Interactive"),
                                        QDateTime::currentDateTime(),
                                        wptIconName());

    m_mainWindow.statusMessage(UiType::Success, undoName);

    accept();
}

void NewWaypointDialog::on_buttonBox_rejected()
{
    m_mainWindow.statusMessage(UiType::Warning, tr("Canceled"));
    reject();
}

void NewWaypointDialog::on_wptIconFromSymbol_clicked()
{
    updateWptIcon();
}
