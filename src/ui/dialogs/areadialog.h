/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef AREADIALOG_H
#define AREADIALOG_H

#include <marble/GeoDataLatLonBox.h>
#include <QDialog>
#include <QVector>
#include <QPersistentModelIndex>
#include <QItemSelectionModel>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>
#include <QHeaderView>

#include "src/ui/widgets/tagselectordelegate.h"
#include "src/core/pointmodel.h"

namespace Ui {
class AreaDialog;
} // namespace Ui

class MainWindow;
class TrackModel;
class QRadioButton;

class AreaDialog final : public QDialog
{
    Q_OBJECT

public:
    explicit AreaDialog(MainWindow&);
    ~AreaDialog() override;

    void newRegion(const Marble::GeoDataLatLonBox& region);
    void closeEvent(QCloseEvent* event) override;
    void newConfig();

private slots:
    void dataColChanged(ModelType, bool state);
    void extremeChanged(bool state);
    void on_zoomToButton_clicked();
    void doubleClicked(const QModelIndex&);
    void processCurrentChanged(const QModelIndex&);
    void processSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected);
    void processRowsAboutToBeRemoved(const QModelIndex&, int first, int last);
    void processDataChanged(const QModelIndex&, const QModelIndex&, const QVector<int>&);

private:
    enum { Name = 0, Index = Name, Tags, Val, _Count };

    const MainWindow& mainWindow() const { return m_mainWindow; }
    MainWindow& mainWindow() { return m_mainWindow; }
    TrackModel& model() { return m_model; }
    const TrackModel& constModel() const { return m_model; }

    const MainWindow& mainWindowConst() const { return m_mainWindow; }
    void setupDataView();
    void setupButtons();
    void setupSignals();
    void showData();
    void updateRow(const QModelIndex& trackIdx, int row, bool updateValue, const PointModel* geoPoints = nullptr);
    void selectExtreme();
    void updateStatus();

    QPersistentModelIndex trackModelIdx(int modelRow) const;
    QPersistentModelIndex trackModelIdx(const QModelIndex& proxyIdx) const;

    Ui::AreaDialog*           ui;
    MainWindow&               m_mainWindow;
    TagSelectorDelegate       m_tagDelegate;
    Marble::GeoDataLatLonBox  m_region;
    QStandardItemModel        m_data;
    QHeaderView               m_header;
    QSortFilterProxyModel     m_filter;
    ModelType                 m_currentColumn;
    PointModel::Extreme       m_extreme;  // pick high, low, or average data
    int                       m_tagKeyRole;
    int                       m_selectedCount;
    const QVector<ModelType>  m_radioData;
    QVector<QRadioButton*>    m_buttons;
    TrackModel&               m_model;
};

#endif // AREADIALOG_H
