/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QString>
#include <QRadioButton>
#include <QImage>
#include <QVariant>
#include <QBoxLayout>
#include <QLineEdit>

#include <src/util/icons.h>
#include <src/util/ui.h>
#include <src/undo/undomgr.h>

#include "src/ui/windows/mainwindow.h"
#include "src/core/app.h"
#include "src/core/geopolmgr.h"

#include "newviewdialog.h"
#include "ui_newviewdialog.h"

NewViewDialog::NewViewDialog(MainWindow& mainWindow, const ViewParams& viewParams, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewViewDialog),
    m_mainWindow(mainWindow),
    m_checked(nullptr),
    m_iconSelector({":art/tags"}),
    m_selectRc(QDialog::Rejected),
    m_userEdited(false),
    m_viewParams(viewParams)
{
    ui->setupUi(this);
    ui->viewName->setText(tr("My Map View"));

    populateFlagChoices();
    setupSignals();
    Util::SetupWhatsThis(this);

    setWindowTitle(tr("Add View Preset"));
}

NewViewDialog::~NewViewDialog()
{
    delete ui;
}

void NewViewDialog::setupSignals()
{
    // Track whether text has been user edited.  TODO: maybe there's an easier way.
    connect(ui->viewName, &QLineEdit::textEdited, this, [this](const QString&) {
        m_userEdited = true;
    });

    connect(ui->selectIcon, &QPushButton::pressed, this, &NewViewDialog::selectIcon);
}

// Suggest flags near center point
void NewViewDialog::populateFlagChoices()
{
    const GeoPolRegionVec flagChoices =
            app().geoPolMgr().intersections(m_viewParams, GeoPolRegion::WorldOpt::IncludeIfNoOther);

    ui->flagRadioNone->setChecked(true);
    m_checked = ui->flagRadioNone;

    auto* radioLayout = dynamic_cast<QBoxLayout*>(ui->flagRadio->layout());

    assert(radioLayout);
    if (radioLayout == nullptr)
        return;

    // Add radio buttons for each flag
    for (const auto* region : flagChoices) {
        if (region != nullptr) {
            if (const QString& flagName = region->flag(); !flagName.isNull()) {
                auto* button = new QRadioButton();

                button->setIcon(QIcon(flagName));
                button->setIconSize(QImage(flagName).size());
                button->setProperty(flagNameProperty, flagName);

                const QString tooltip = QString("<p><b><u><nobr><big>") +
                        region->name() +
                        "</big></nobr></u></b></p>" +
                        "<table border=0.5 cellspacing=0 cellpadding=2>" +
                        region->tooltipTableRow() +
                        "</table><br/>";

                button->setToolTip(tooltip);

                // Track button presses, and update text
                connect(button, &QRadioButton::pressed, this, [this, button, region]() {
                    m_checked = button; // keep track of currently checked QRadioButton

                    if (!m_userEdited)
                        ui->viewName->setText(region->name());
                });

                radioLayout->addWidget(button, 10);
            }
        }
    }
}

void NewViewDialog::selectIcon()
{
    ui->flagRadioSelect->setChecked(true);
    m_selectRc = m_iconSelector.exec();
    m_checked = ui->flagRadioSelect;
}

QString NewViewDialog::getViewName() const
{
    return ui->viewName->text();
}

QString NewViewDialog::getViewIcon() const
{
    // Get property for the icon name.  The no flag button has no property to fetch.
    if (m_checked != nullptr && m_checked->isChecked()) {
        if (m_checked == ui->flagRadioSelect && m_selectRc == QDialog::Accepted)
            return m_iconSelector.iconFile();

        if (const QVariant flagNameVar = m_checked->property(flagNameProperty); flagNameVar.isValid())
            return flagNameVar.toString();
    }

    return { };
}

void NewViewDialog::on_buttonBox_accepted()
{
    if (getViewName().isEmpty())
        return on_buttonBox_rejected();

    // Friendly undo name
    const UndoMgr::ScopedUndo undoSet(app().undoMgr(), tr("Add View: ") + getViewName());

    app().viewModel().appendRow(getViewName(), m_viewParams, getViewIcon());

    m_mainWindow.statusMessage(UiType::Success, tr("Added view preset: ") + getViewName());

    accept();
}

void NewViewDialog::on_buttonBox_rejected()
{
    m_mainWindow.statusMessage(UiType::Warning, tr("Canceled"));
    reject();
}
