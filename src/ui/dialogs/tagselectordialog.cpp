/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>

#include <src/util/ui.h>
#include <src/util/util.h>

#include "tagselectordialog.h"
#include "ui_tagselectordialog.h"

#include "src/ui/widgets/tagselector.h"

TagSelectorDialog::TagSelectorDialog(const MainWindow& mainWindow, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TagSelectorDialog),
    m_tagSelector(new TagSelector(mainWindow))
{
    ui->setupUi(this);
    Util::SetupWhatsThis(this);

    ui->verticalLayout->insertWidget(0, m_tagSelector);
}

TagSelectorDialog::~TagSelectorDialog()
{
    delete ui;
    // tagSelector is adopted by the UI, so we don't delete it ourselves.
}

TagSelector& TagSelectorDialog::operator()()
{
    assert(m_tagSelector);
    return *m_tagSelector;
}

const TagSelector& TagSelectorDialog::operator()() const
{
    assert(m_tagSelector);
    return *m_tagSelector;
}
