/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef EXPORTDIALOG_H
#define EXPORTDIALOG_H

#include <QDialog>
#include <src/core/settings.h>

#include "src/geo-io/geoioparams.h"

namespace Ui {
class ExportDialog;
} // namespace Ui

enum class GeoIoFeature;

class ExportDialog final : public QDialog, public Settings
{
    Q_OBJECT

public:
    explicit ExportDialog(QWidget *parent = nullptr);
    ~ExportDialog() override;

    GeoSaveParams geoSaveParams() const;

    QString   getExportFileName(const char* filter = nullptr);

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

private slots:
    void updateActions();

private:
    friend class TestZtgps; // test hook

    void         showEvent(QShowEvent*) override;
    void         setupSignals();

    GeoFormat    exportFormat()   const; // format to export
    GeoIoFeature features()       const; // build features mask from export params
    int          indentLevel()    const; // indent level for formatted output

    static const QString&      filterTrk();
    static const QString&      filterWpt();
    static Qt::CaseSensitivity filterCase();

    Ui::ExportDialog* ui;
    QString           defaultDir;  // which directory to show
};

#endif // EXPORTDIALOG_H
