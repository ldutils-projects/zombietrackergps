/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <chrono>

#include <QItemSelectionModel>
#include <QVector>

#include <src/util/ui.h>

#include "src/core/app.h"
#include "src/core/cfgdata.h"

#include "tracksimplifydialog.h"
#include "ui_tracksimplifydialog.h"

TrackSimplifyDialog::TrackSimplifyDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TrackSimplifyDialog),
    m_selections(nullptr),
    m_model(nullptr)
{
    ui->setupUi(this);

    // Call setSimplifyType twice in a row to ensure there's a state change to trigger toggled() signals
    setSimplifyType(SimplifyType::ByDist);
    setSimplifyType(SimplifyType::Adaptive);
    setupSuffixes();
    setupSignals();
    Util::SetupWhatsThis(this);
}

TrackSimplifyDialog::~TrackSimplifyDialog()
{
    delete ui;
}

TrackSimplifyDialog::SimplifyType TrackSimplifyDialog::simplifyType() const
{
    if (ui->byDistRadio->isChecked())
        return SimplifyType::ByDist;

    if (ui->byTimeRadio->isChecked())
        return SimplifyType::ByTime;

    if (ui->byAdaptiveRadio->isChecked())
        return SimplifyType::Adaptive;

    assert(0);
    return SimplifyType::Invalid;
}

Dur_t TrackSimplifyDialog::timeS() const
{
    const QTime time = ui->filterTime->time();

    return Dur_t(time.msecsSinceStartOfDay() / 1000);
}

Dist_t TrackSimplifyDialog::distM() const
{
    return cfgData().unitsLegLength.fromDouble(ui->filterDist->value());
}

Dist_t TrackSimplifyDialog::thresholdM() const
{
    return cfgData().unitsLegLength.fromDouble(ui->filterAdaptive->value());
}

void TrackSimplifyDialog::showEvent(QShowEvent* showEvent)
{
    setupSuffixes();  // reset this if settings have changed
    updatePreview();

    QDialog::showEvent(showEvent);
}

void TrackSimplifyDialog::setSimplifyType(TrackSimplifyDialog::SimplifyType type)
{
    switch (type) {
    case SimplifyType::ByDist:   ui->byDistRadio->setChecked(true); break;
    case SimplifyType::Adaptive: ui->byAdaptiveRadio->setChecked(true); break;
    case SimplifyType::ByTime:   ui->byTimeRadio->setChecked(true); break;
    case SimplifyType::Invalid:
        assert(0);
        ui->byAdaptiveRadio->setChecked(true); // default to adaptive
        break;
    }
}

void TrackSimplifyDialog::setTimeS(Dur_t timeS)
{
    ui->filterTime->setTime(QTime(0, 0, 0).addSecs(int(timeS)));
}

void TrackSimplifyDialog::setDistM(Dist_t distM)
{
    ui->filterDist->setValue(cfgData().unitsLegLength.toDouble(Dist_t::base_type(distM)));
}

void TrackSimplifyDialog::setThresholdM(Dist_t thresholdM)
{
    ui->filterAdaptive->setValue(cfgData().unitsLegLength.toDouble(qreal(thresholdM)));
}

void TrackSimplifyDialog::save(QSettings& settings) const
{
    if (ui == nullptr)
        return;

    SL::Save(settings, "type", simplifyType());
    SL::Save(settings, "timeS", Dur_t::base_type(timeS()));
    SL::Save(settings, "distM", Dist_t::base_type(distM()));
    SL::Save(settings, "thresholdM", Dist_t::base_type(thresholdM()));
}

void TrackSimplifyDialog::load(QSettings& settings)
{
    if (ui == nullptr)
        return;

    setSimplifyType(SL::Load(settings, "type", SimplifyType::Adaptive));
    setTimeS(Dur_t(SL::Load(settings, "timeS", 5)));
    setDistM(SL::Load(settings, "distM", 10.0));
    setThresholdM(SL::Load(settings, "thresholdM", 1.0));
}

TrackSimplifyDialog::Params TrackSimplifyDialog::simplifyParams(Mode mode) const
{
    switch (simplifyType()) {
    case SimplifyType::Adaptive: return Params::Adaptive(thresholdM(), mode);
    case SimplifyType::ByTime:   return Params::Time(timeS(), mode);
    case SimplifyType::ByDist:   return Params::Dist(distM(), mode);
    case SimplifyType::Invalid:  assert(0); break;
    }

    return { };
}

int TrackSimplifyDialog::exec(SimplifiableModel& model, const QModelIndexList& selections)
{
    // Things to do at stack unwind.
    struct onExit_t {
        onExit_t(TrackSimplifyDialog& ts) : m_ts(ts) { }
        ~onExit_t() { m_ts.setSelections(nullptr, nullptr); }
        TrackSimplifyDialog& m_ts;
    } onExit(*this);

    setSelections(&model, &selections);

    return QDialog::exec();
}

void TrackSimplifyDialog::setSelections(SimplifiableModel* model,
                                        const QModelIndexList* selections)
{
    m_model      = model;
    m_selections = selections;
}

void TrackSimplifyDialog::setupSignals()
{
    using namespace std::chrono_literals;

    if (ui == nullptr)
        return;

    m_previewTimer.setSingleShot(true);
    m_previewTimer.setInterval(100ms);

    // Timer notifies us to update UI
    connect(&m_previewTimer, &QTimer::timeout, this, &TrackSimplifyDialog::updatePreview);

    // Sim0plify on accept.
    connect(this, &QDialog::accepted, this, &TrackSimplifyDialog::simplify);

    // Start timer on UI element changes
    for (auto* btn : findChildren<QRadioButton*>())
        connect(btn, &QRadioButton::released,
                &m_previewTimer, static_cast<void(QTimer::*)()>(&QTimer::start));

    for (auto* btn : findChildren<QDoubleSpinBox*>())
        connect(btn, static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),
                &m_previewTimer, static_cast<void(QTimer::*)()>(&QTimer::start));

    for (auto* btn : findChildren<QTimeEdit*>())
        connect(btn, &QTimeEdit::timeChanged,
                &m_previewTimer, static_cast<void(QTimer::*)()>(&QTimer::start));
}

void TrackSimplifyDialog::setupSuffixes()
{
    ui->filterDist->setSuffix(QString(" ") + cfgData().unitsLegLength.suffix(1.0));
    ui->filterAdaptive->setSuffix(QString(" ") + cfgData().unitsLegLength.suffix(1.0));
}

SimplifiableModel::Count TrackSimplifyDialog::simplifiedPointCount() const
{
    if (m_model != nullptr && m_selections != nullptr)
        return m_model->simplify(*m_selections, simplifyParams(Mode::Preview));

    return { };
}

void TrackSimplifyDialog::updatePreview()
{
    if (ui == nullptr)
        return;

    const auto count = simplifiedPointCount();

    ui->previewText->setText(tr("%1 points in %2 tracks %3 %4 points")
                             .arg(count.m_beforePoints)
                             .arg(count.m_tracks)
                             .arg(QChar(0x21E8))
                             .arg(count.m_afterPoints));
}

void TrackSimplifyDialog::simplify()
{
    if (m_selections == nullptr || m_model == nullptr)
        return;

    const QString undoName = UndoMgr::genName(tr("Simplify"), m_selections->size(),
                                              m_model->getItemName());
    const UndoMgr::ScopedUndo undoSet(app().undoMgr(), undoName);

    (void)m_model->simplify(*m_selections, simplifyParams());
}
