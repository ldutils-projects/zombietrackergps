/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NEWTRACKDIALOG_H
#define NEWTRACKDIALOG_H

#include <QDialog>
#include <QModelIndex>
#include <src/core/settings.h>

#include "src/ui/widgets/tagselector.h"

class TagSelector;
class TrackModel;
class MainWindow;

enum TrackType : uint8_t;

namespace Ui {
class NewTrackDialog;
} // namespace Ui

class NewTrackDialog final : public QDialog, public Settings
{
    Q_OBJECT

public:
    explicit NewTrackDialog(MainWindow&);
    ~NewTrackDialog() override;

    QStringList tags() const;
    QColor  trackColor() const; // returns invalid color if no override
    QString trackName() const;
    TrackType trackType() const;
    void setTrackName(const QString&);
    void defaultTrackName(); // name by current date

    QModelIndex appendRow(TrackModel&, const QString& undoName) const;  // append new track to given model

    using QDialog::exec;
    QModelIndex exec(TrackModel&, const QString& undoName);

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

private slots:
    void askTrackColor();

private:
    void setupSignals();

    Ui::NewTrackDialog* ui;
    TagSelector*        m_tagSelector; // UI adopts, so we don't free it.
    MainWindow&         m_mainWindow;
};

#endif // NEWTRACKDIALOG_H
