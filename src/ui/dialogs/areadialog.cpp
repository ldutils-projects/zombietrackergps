/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QRadioButton>
#include <QList>

#include <src/util/util.h>
#include <src/util/ui.h>

#include "src/ui/windows/mainwindow.h"
#include "src/ui/panes/trackpane.h"
#include "src/ui/panes/pointpane.h"
#include "src/ui/panes/mappane.h"
#include "src/core/app.h"

#include "areadialog.h"
#include "ui_areadialog.h"

AreaDialog::AreaDialog(MainWindow& mainWindow) :
    QDialog(&mainWindow),
    ui(new Ui::AreaDialog),
    m_mainWindow(mainWindow),
    m_tagDelegate(mainWindow, "", true, cfgData().tags.keyRole()),
    m_header(Qt::Horizontal, this),
    m_filter(this),
    m_currentColumn(PointModel::Speed),
    m_extreme(PointModel::Extreme::High),
    m_tagKeyRole(cfgData().tags.keyRole()),
    m_selectedCount(0),
    m_radioData({ PointModel::Speed,
                  PointModel::Grade,
                  PointModel::Ele,
                  PointModel::Duration,
                  PointModel::Hr,
                  PointModel::Cad,
                  PointModel::Power,
                  PointModel::Temp, }),
    m_model(app().trackModel())
{
    ui->setupUi(this);

    setupDataView();
    setupButtons();
    setupSignals();
    Util::SetupWhatsThis(this);
}

AreaDialog::~AreaDialog()
{
    delete ui;
}

void AreaDialog::setupDataView()
{
    m_filter.setSourceModel(&m_data);
    ui->dataView->setModel(&m_filter);
    ui->dataView->setHeader(&m_header);

    m_filter.setSortRole(Util::RawDataRole);
    m_filter.setDynamicSortFilter(false);
    m_filter.setFilterKeyColumn(-1);
    m_filter.setFilterRole(Util::CopyRole);
    newConfig();

    m_header.setSectionResizeMode(QHeaderView::Interactive);
    m_header.setDefaultAlignment(Qt::AlignRight);
    m_header.setSectionsMovable(false);

    selectExtreme();

    ui->dataView->setItemDelegateForColumn(Tags, &m_tagDelegate);
    Util::SetFocus(ui->dataView);
}

void AreaDialog::setupButtons()
{
    m_buttons.clear();
    m_buttons.reserve(m_radioData.size());
    
    for (const auto d : m_radioData) {
        m_buttons.append(new QRadioButton(PointModel::mdName(d)));
        ui->dataRadios->addWidget(m_buttons.back());
        m_buttons.back()->setChecked(d == m_currentColumn);

        connect(m_buttons.back(), &QRadioButton::clicked,
                this, [this, d](bool state) { dataColChanged(d, state); });
    }
}

void AreaDialog::setupSignals()
{
    connect(ui->dataView, &QTreeView::doubleClicked, this, &AreaDialog::doubleClicked);
    connect(ui->dataView->selectionModel(), &QItemSelectionModel::currentChanged,
            this, &AreaDialog::processCurrentChanged);

    connect(ui->dataView->selectionModel(), &QItemSelectionModel::selectionChanged,
            this, &AreaDialog::processSelectionChanged);

    connect(ui->dataHigh, &QRadioButton::clicked, this, &AreaDialog::extremeChanged);
    connect(ui->dataAvg, &QRadioButton::clicked, this, &AreaDialog::extremeChanged);
    connect(ui->dataLow, &QRadioButton::clicked, this, &AreaDialog::extremeChanged);

    // Changes from main model
    connect(&model(), &TrackModel::rowsAboutToBeRemoved, this, &AreaDialog::processRowsAboutToBeRemoved);
    connect(&model(), &TrackModel::dataChanged, this, &AreaDialog::processDataChanged);

    // Line edit regex
    connect(ui->filterText, &QLineEdit::textChanged,
            &m_filter, static_cast<void (QSortFilterProxyModel::*)(const QString&)>(&QSortFilterProxyModel::setFilterRegExp));
}

void AreaDialog::newConfig()
{
    m_filter.setSortCaseSensitivity(cfgData().getSortCaseSensitivity());
    m_filter.setFilterCaseSensitivity(cfgData().getFilterCaseSensitivity());
}

void AreaDialog::closeEvent(QCloseEvent* event)
{
    QDialog::closeEvent(event);

    m_data.clear();
}

QPersistentModelIndex AreaDialog::trackModelIdx(int modelRow) const
{
    return m_data.data(m_data.index(modelRow, Index), Util::PrivateDataRole).value<QPersistentModelIndex>();
}

QPersistentModelIndex AreaDialog::trackModelIdx(const QModelIndex& proxyIdx) const
{
    return trackModelIdx(Util::MapDown(proxyIdx).row());
}

void AreaDialog::processDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector<int>&)
{
    for (int modelRow = 0; modelRow < m_data.rowCount(); ++modelRow)
        if (const QModelIndex trackIdx = trackModelIdx(modelRow); 
            trackIdx.row() >= topLeft.row() && trackIdx.row() <= bottomRight.row())
            updateRow(trackIdx, modelRow, false);
}

void AreaDialog::processRowsAboutToBeRemoved(const QModelIndex&, int first, int last)
{
    for (int modelRow = m_data.rowCount() - 1; modelRow >= 0; --modelRow)
        if (const int trackRow = trackModelIdx(modelRow).row(); trackRow >= first && trackRow <= last)
            m_data.removeRow(modelRow);
}

void AreaDialog::processCurrentChanged(const QModelIndex& idx)
{
    if (!idx.isValid())
        return;

    auto* trackPane = mainWindow().findPane<TrackPane>();
    if (trackPane == nullptr)
        return;

    auto* pointPane = mainWindow().findPane<PointPane>();
    const PointPane::InstantUpdate instantUpdate(pointPane);  // force instant updates on selection in point pane

    const QModelIndex trackIdx = trackModelIdx(idx);
    if (!trackPane->select(trackIdx, QItemSelectionModel::Current | QItemSelectionModel::Rows))
        return;

    if (pointPane == nullptr)
        return;

    const PointModel* geoPoints = constModel().geoPoints(trackIdx);
    if (geoPoints == nullptr)
        return;

    const QModelIndex extremeIdx = geoPoints->areaExtreme(*this);
    pointPane->select(extremeIdx, QItemSelectionModel::Current | QItemSelectionModel::Rows);
}

void AreaDialog::processSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected)
{
    auto* trackPane = mainWindow().findPane<TrackPane>();
    if (trackPane == nullptr)
        return;

    // Map selection into TrackModel's index space.
    const auto changeSelection = [this, &trackPane](const QItemSelection& selection,
            QItemSelectionModel::SelectionFlags flags) {

        for (const QItemSelectionRange& range : selection) {
            m_selectedCount += (range.bottom() - range.top() + 1) * (bool(flags & QItemSelectionModel::Select) ? 1 : -1);

            for (int row = range.top(); row <= range.bottom(); ++row)
                trackPane->select(trackModelIdx(range.model()->index(row, 0)), flags);
        }
    };

    changeSelection(deselected, QItemSelectionModel::Deselect | QItemSelectionModel::Rows);
    changeSelection(selected, QItemSelectionModel::Select | QItemSelectionModel::Rows);

    updateStatus();
}

void AreaDialog::updateStatus()
{
    ui->statusText->setText(tr(" <b>Tracks with points in area:</b> ") +
                            QString::number(m_data.rowCount()) +
                            tr(" <b>Selected:</b> ") +
                            QString::number(m_selectedCount));
}

void AreaDialog::doubleClicked(const QModelIndex&)
{
    auto* trackPane = mainWindow().findPane<TrackPane>();
    if (trackPane == nullptr)
        return;

    // Map our selections to trackmodel selections
    QModelIndexList selections = ui->dataView->selectionModel()->selectedRows();
    for (auto& s : selections)
        s = trackModelIdx(s);

    trackPane->gotoSelection(selections);
}

void AreaDialog::newRegion(const Marble::GeoDataLatLonBox& region)
{
    m_region = region;
    model().setAreaSelected(*this, false);
    model().selectPointsWithin(*this, region);
    showData();
    updateStatus();
}

void AreaDialog::updateRow(const QModelIndex& trackIdx, int row, bool updateValue, const PointModel* geoPoints)
{
    if (!trackIdx.isValid())
        return;

    if (geoPoints == nullptr)
        if (geoPoints = constModel().geoPoints(trackIdx); geoPoints == nullptr)
            return;

    const QString trackName = model().data(TrackModel::Name, trackIdx).toString();
    const QStringList trackTags = model().data(TrackModel::Tags, trackIdx, m_tagKeyRole).toStringList();

    m_data.setData(m_data.index(row, Name),  trackName);
    m_data.setData(m_data.index(row, Name),  trackName, Util::CopyRole);
    m_data.setData(m_data.index(row, Index), trackIdx, Util::PrivateDataRole); // same as Name column
    m_data.setData(m_data.index(row, Tags),  trackTags, m_tagKeyRole);
    m_data.setData(m_data.index(row, Tags),  trackTags.join(", "), Util::CopyRole);

    if (updateValue) {
        const auto [extreme, count] = geoPoints->extremeVal(*this, m_currentColumn, m_extreme);
        if (count == 0)
            return;

        m_data.setData(m_data.index(row, Val), PointModel::mdUnits(m_currentColumn)(extreme));
        m_data.setData(m_data.index(row, Val), extreme, Util::RawDataRole);
    }
}

// Populate our wee model with the data extremes from selected area
void AreaDialog::showData()
{
    m_data.clear();
    m_selectedCount = 0;

    if (auto* trackPane = mainWindow().findPane<TrackPane>(); trackPane != nullptr)
        trackPane->updateVisibility();

    QList<QStandardItem*> rowData;

    for (int i = 0; i < _Count; ++i)
        rowData.append(nullptr);

    m_data.setHorizontalHeaderLabels(
       { TrackModel::mdName(TrackModel::Name),
         TrackModel::mdName(TrackModel::Tags),
         PointModel::mdName(m_currentColumn) });

    std::bitset<PointModel::_Count> hasData;

    Util::Recurse(model(), [this, &rowData, &hasData](const QModelIndex& idx) {
        if (!model().isAreaSelected(*this, idx) || !model().isVisible(idx))
            return true;

        const PointModel* geoPoints = constModel().geoPoints(idx);
        if (geoPoints == nullptr)
            return true;

        for (int i = 0; i < _Count; ++i)
            rowData[i] = new QStandardItem();
        m_data.appendRow(rowData);

        updateRow(idx, m_data.rowCount() - 1, true, geoPoints);
        geoPoints->accumulateHasData(*this, hasData);
        return true;
    });

    // Enable or disable radio buttons
    for (int i = 0; i < m_radioData.size(); ++i)
        m_buttons[i]->setEnabled(hasData.test(size_t(m_radioData[i])));

    // Sort by new value
    m_header.setSortIndicator(Val, (m_extreme == PointModel::Extreme::Low) ?
                                  Qt::AscendingOrder : Qt::DescendingOrder);

    // Select first point, if any (the maximally extreme point)
    if (m_data.rowCount() > 0)
        ui->dataView->setCurrentIndex(m_filter.index(0, 0));

    Util::ResizeViewForData(*ui->dataView);
}

void AreaDialog::dataColChanged(ModelType mt, bool state)
{
    if (state) {
        m_currentColumn = mt;
        showData();
    }
}

void AreaDialog::extremeChanged(bool state)
{
    if (state) {
        selectExtreme();
        showData();
    }
}

// Select which extreme to show based on the UI state
void AreaDialog::selectExtreme()
{
    m_extreme = ui->dataHigh->isChecked() ? PointModel::Extreme::High :
                ui->dataAvg->isChecked()  ? PointModel::Extreme::Avg :
                                            PointModel::Extreme::Low;
}

void AreaDialog::on_zoomToButton_clicked()
{
    auto* mapPane = mainWindow().findPane<MapPane>();
     
    if (mapPane == nullptr) {
        mainWindow().statusMessage(UiType::Warning, tr("No Map Pane found."));
        return;
    }
   
    mapPane->zoomTo(m_region);
}
