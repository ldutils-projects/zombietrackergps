/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TAGSELECTORDIALOG_H
#define TAGSELECTORDIALOG_H

#include <QDialog>

namespace Ui {
class TagSelectorDialog;
} // namespace Ui

class TagSelector;
class MainWindow;

class TagSelectorDialog final : public QDialog
{
    Q_OBJECT

public:
    explicit TagSelectorDialog(const MainWindow&, QWidget *parent = nullptr);
    ~TagSelectorDialog() override;

    TagSelector& operator()();
    const TagSelector& operator()() const;

private:
    Ui::TagSelectorDialog* ui;
    TagSelector*           m_tagSelector; // UI adopts, so we don't free it!
};

#endif // TAGSELECTORDIALOG_H
