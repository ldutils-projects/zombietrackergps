/*
    Copyright 2019-2020 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QAbstractButton>
#include <QPushButton>
#include <QItemSelectionModel>

#include <src/core/app.h>
#include <src/util/ui.h>
#include <src/undo/undomgr.h>

#include "src/ui/panes/gpsdevicepane.h"
#include "src/ui/windows/mainwindow.h"

#include "devicedialog.h"
#include "ui_devicedialog.h"

DeviceDialog::DeviceDialog(MainWindow& mainWindow) :
    QDialog(&mainWindow),
    ui(new Ui::DeviceDialog),
    m_mainWindow(mainWindow),
    m_gpsDeviceList(new GpsDevicePane(mainWindow, false)) // disable context menus
{
    ui->setupUi(this);
    setupSignals();
    Util::SetupWhatsThis(this);

    ui->verticalLayout->insertWidget(0, m_gpsDeviceList);

    m_gpsDeviceList->paneToggled(false);  // default to filter bar hidden
    m_gpsDeviceList->showAllColumns();    // show all columns by default

    processSelectionChanged();
}

DeviceDialog::~DeviceDialog()
{
    delete ui;
    // m_gpsDeviceList is adopted by the UI, so we don't delete it ourselves.
}

void DeviceDialog::setupSignals()
{
    connect(m_gpsDeviceList->treeView()->selectionModel(), &QItemSelectionModel::selectionChanged,
            this, &DeviceDialog::processSelectionChanged);

    connect(m_gpsDeviceList->treeView(), &QTreeView::doubleClicked, this, &DeviceDialog::processDoubleClick);
}

void DeviceDialog::deviceImport()
{
    const UndoMgr::ScopedUndo undoSet(app().undoMgr(), tr("Import Tracks from Device"));

    hide();
    m_gpsDeviceList->import();
}

void DeviceDialog::processSelectionChanged()
{
    ui->importButton->setEnabled(m_gpsDeviceList->treeView()->selectionModel()->hasSelection());
}

void DeviceDialog::processDoubleClick()
{
    deviceImport();
}

void DeviceDialog::on_importButton_clicked()
{
    deviceImport();
}
