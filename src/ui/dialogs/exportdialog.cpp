/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QDir>
#include <QFileDialog>
#include <QFileInfo>

#include <src/util/ui.h>
#include <src/core/appbase.h>

#include "src/geo-io/geoio.h"    // for GeoIoFeture
#include "src/geo-io/geoiogpx.h" // just for the default name

#include "exportdialog.h"
#include "ui_exportdialog.h"

ExportDialog::ExportDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ExportDialog)
{
    ui->setupUi(this);

    setupSignals();
    Util::SetupWhatsThis(this);

    ui->exportFormat->addItems(GeoSave::formatNames());
    ui->exportFormat->setCurrentText(GeoLoadGpx::nameStatic);

    defaultDir = QDir::home().absolutePath();
}

ExportDialog::~ExportDialog()
{
    delete ui;
}

void ExportDialog::setupSignals()
{
    connect(ui->exportFormat, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this, &ExportDialog::updateActions);
}

GeoSaveParams ExportDialog::geoSaveParams() const
{
    return GeoSaveParams(exportFormat(), features(), indentLevel(),
                         filterTrk(), filterWpt(), filterCase());
}

GeoFormat ExportDialog::exportFormat() const
{
    return GeoFormat(ui->exportFormat->currentIndex() + 1);  // +1 skips native, which we don't offer.
}

GeoIoFeature ExportDialog::features() const
{
    return (!ui->writeNoTrk->isChecked() ? GeoIoFeature::Trk : GeoIoFeature::None) |
           (!ui->writeNoRte->isChecked() ? GeoIoFeature::Rte : GeoIoFeature::None) |
           (!ui->writeNoWpt->isChecked() ? GeoIoFeature::Wpt : GeoIoFeature::None) |
           (ui->writeZtgpsExtensions->isChecked() ? GeoIoFeature::ExtZtgps : GeoIoFeature::None) |
           (ui->writeFormatted->isChecked() ? GeoIoFeature::Formatted : GeoIoFeature::None) |
           (ui->writeSpaces->isChecked() ? GeoIoFeature::Spaces : GeoIoFeature::None) |
           (ui->writeAllTrk->isChecked() ? GeoIoFeature::AllTrk : GeoIoFeature::None) |
           (ui->writeAllRte->isChecked() ? GeoIoFeature::AllRte : GeoIoFeature::None) |
           (ui->writeAllWpt->isChecked() ? GeoIoFeature::AllWpt : GeoIoFeature::None);
}

int ExportDialog::indentLevel() const
{
    return ui->indentLevel->value();
}

const QString& ExportDialog::filterTrk()
{
    static const QString empty;
    return empty;
}

const QString& ExportDialog::filterWpt()
{
    static const QString empty;
    return empty;
}

Qt::CaseSensitivity ExportDialog::filterCase()
{
    return Qt::CaseInsensitive;
}

QString ExportDialog::getExportFileName(const char* filter)
{
    // Test hook: don't use modal dialog during testing
    if (appBase().testing())
        return QString();

    const QString trackFile =
            QFileDialog::getSaveFileName(parentWidget(),
                                         tr("Export GPS Track File"), defaultDir,
                                         filter);

    if (!trackFile.isEmpty())
        defaultDir = QFileInfo(trackFile).path();

    return trackFile;
}

void ExportDialog::save(QSettings& settings) const
{
    MemberSave(settings, defaultDir);

    if (ui == nullptr)
        return;

    SL::Save(settings, "exportFormat",         ui->exportFormat->currentText());
    SL::Save(settings, "indentLevel",          indentLevel());

    SL::Save(settings, "writeSpaces",          ui->writeSpaces);
    SL::Save(settings, "writeTabs",            ui->writeTabs);
    SL::Save(settings, "writeFormatted",       ui->writeFormatted);
    SL::Save(settings, "writeZtgpsExtensions", ui->writeZtgpsExtensions);
    SL::Save(settings, "exportAllTrk",         ui->writeAllTrk);        // track buttons
    SL::Save(settings, "exportSelTrk",         ui->writeSelectedTrk);   // ...
    SL::Save(settings, "exportNoTrk",          ui->writeNoTrk);         // ...
    SL::Save(settings, "exportAllRte",         ui->writeAllRte);        // route buttons
    SL::Save(settings, "exportSelRte",         ui->writeSelectedRte);   // ...
    SL::Save(settings, "exportNoRte",          ui->writeNoRte);         // ...
    SL::Save(settings, "exportAllWpt",         ui->writeAllWpt);        // waypoint buttons
    SL::Save(settings, "exportSelWpt",         ui->writeSelectedWpt);   // ...
    SL::Save(settings, "exportNoWpt",          ui->writeNoWpt);         // ...
}

void ExportDialog::load(QSettings& settings)
{
    MemberLoad(settings, defaultDir);

    if (ui == nullptr)
        return;

    ui->exportFormat->setCurrentText(SL::Load(settings, "exportFormat", GeoLoadGpx::nameStatic));
    ui->indentLevel->setValue(SL::Load(settings, "indentLevel", 2));

    SL::Load(settings, "writeSpaces",          ui->writeSpaces);
    SL::Load(settings, "writeTabs",            ui->writeTabs);
    SL::Load(settings, "writeFormatted",       ui->writeFormatted);
    SL::Load(settings, "writeZtgpsExtensions", ui->writeZtgpsExtensions);
    SL::Load(settings, "exportAllTrk",         ui->writeAllTrk);        // track buttons
    SL::Load(settings, "exportSelTrk",         ui->writeSelectedTrk);   // ...
    SL::Load(settings, "exportNoTrk",          ui->writeNoTrk);         // ...
    SL::Load(settings, "exportAllRte",         ui->writeAllRte);        // route buttons
    SL::Load(settings, "exportSelRte",         ui->writeSelectedRte);   // ...
    SL::Load(settings, "exportNoRte",          ui->writeNoRte);         // ...
    SL::Load(settings, "exportAllWpt",         ui->writeAllWpt);        // waypoint buttons
    SL::Load(settings, "exportSelWpt",         ui->writeSelectedWpt);   // ...
    SL::Load(settings, "exportNoWpt",          ui->writeNoWpt);         // ...
}

void ExportDialog::showEvent(QShowEvent* event)
{
    updateActions();
    QDialog::showEvent(event);
}

void ExportDialog::updateActions()
{
    const bool isGpx = ui->exportFormat->currentText() == GeoLoadGpx::nameStatic;
    const bool isBinary = GeoLoad::isBinary(ui->exportFormat->currentText());

    ui->writeFormatted->setEnabled(!isBinary);
    ui->writeSpaces->setEnabled(!isBinary);
    ui->writeTabs->setEnabled(!isBinary);
    ui->indentLevel->setEnabled(!isBinary);

    ui->writeZtgpsExtensions->setEnabled(isGpx);
}
