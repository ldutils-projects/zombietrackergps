/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <algorithm>
#include <limits>

#include <QApplication>

#include <src/util/math.h>
#include <src/util/variantcmp.h>

#include "geolocfilter.h"

#include "src/core/geolocmodel.inl.h"

const uint32_t GeoLocFilter::filterMaskDefault =
        (1U<<int(GeoLocEntry::Feature::Region)) |
        (1U<<int(GeoLocEntry::Feature::City)) |
        (1U<<int(GeoLocEntry::Feature::Forest)) |
        (1U<<int(GeoLocEntry::Feature::Park)) |
        (1U<<int(GeoLocEntry::Feature::Water)) |
        (1U<<int(GeoLocEntry::Feature::Mountain));

GeoLocFilter::GeoLocFilter(const GeoLocModel& model) :
    m_model(model),
    m_enabled(false),
    m_currColumn(GeoLocModel::Name),
    m_currOrder(Qt::AscendingOrder),
    m_prevColumn(GeoLocModel::Dist),
    m_prevOrder(Qt::AscendingOrder)
{
    // For most purpose we are fine with a const model, but the overridden method
    // takes a non-const.
    GeoLocFilter::setSourceModel(const_cast<GeoLocModel*>(&model));
}

inline QModelIndex GeoLocFilter::fromSource(const QModelIndex& idx) const
{
    return createIndex(idx.row(), idx.column(), idx.internalId());
}

inline QModelIndex GeoLocFilter::toSource(const QModelIndex& idx) const
{
    return model().createIndex(idx.row(), idx.column(), idx.internalId());
}

QModelIndex GeoLocFilter::mapFromSource(const QModelIndex& sourceIndex) const
{
    // Non-leafs map straight over: we reflect the hierarchy above the leaf nodes.
    if (!m_enabled || !sourceIndex.isValid())
        return fromSource(sourceIndex);

    const auto& entry = map(sourceIndex.parent());

    if (int proxyRow = entry->toProxy(sourceIndex.row()); proxyRow >= 0)
        return createIndex(proxyRow, sourceIndex.column(), sourceIndex.internalId());

    assert(0 && "invalid source to proxy mapping");
    return { }; // error; no reverse mapping found.
}

QModelIndex GeoLocFilter::mapToSource(const QModelIndex& proxyIndex) const
{
    if (!m_enabled || !proxyIndex.isValid())
        return toSource(proxyIndex);

    const int srcRow = map(proxyIndex.parent())->toSource(proxyIndex.row());

    return model().createIndex(srcRow, proxyIndex.column(), proxyIndex.internalId());
}

int GeoLocFilter::rowCount(const QModelIndex& proxyIndex) const
{
    if (model().loadFinished() && !GeoLocModel::Node::isLeaf(proxyIndex))
        return map(proxyIndex)->rowCount();

    return 0;
}

int GeoLocFilter::columnCount(const QModelIndex& proxyIndex) const
{
    return model().columnCount(mapToSource(proxyIndex));
}

QModelIndex GeoLocFilter::index(int row, int column, const QModelIndex &parent) const
{
    if (model().loadFinished()) {
        const GeoLocModel::Node* node = model().getNode(parent);
        const auto entry = map(parent);
        const auto sourceParentRow = entry->toSource(row);

        if (node->isLeaf(sourceParentRow)) {
            return createIndex(row, column, quintptr(node) | GeoLocModel::Node::leafMarker);
        }

        node = node->child(uint32_t(sourceParentRow));;
        return createIndex(row, column, quintptr(node));
    }

    return { };
}

QModelIndex GeoLocFilter::parent(const QModelIndex& idx) const
{
    if (!idx.isValid())
        return { };

    const GeoLocModel::Node* childNode = model().getNode(idx);
    const GeoLocModel::Node* parentNode = GeoLocModel::Node::isLeaf(idx) ? childNode : childNode->parent();

    if (const GeoLocModel::Node* gpNode = parentNode->parent(); gpNode != nullptr) {
        if (const auto gpIt = m_map.find(quintptr(gpNode)); gpIt != m_map.end()) {
            const int parentRow = gpIt->toProxy(int(parentNode->childNumber()));
            return createIndex(parentRow, 0, quintptr(parentNode));
        }
    }

    return { }; // it should remain in the map
}

void GeoLocFilter::setSourceModel(QAbstractItemModel* newSourceModel)
{
    beginResetModel();

    // We have to translate data change events to our own index space and re-forward
    if (sourceModel() != newSourceModel) {
        if (sourceModel() != nullptr) {
            disconnect(sourceModel(), &QAbstractItemModel::modelAboutToBeReset, this, &GeoLocFilter::processModelAboutToBeReset);
            disconnect(sourceModel(), &QAbstractItemModel::modelReset, this, &GeoLocFilter::processModelReset);
        }

        QAbstractProxyModel::setSourceModel(newSourceModel);

        if (sourceModel() != nullptr) {
            connect(sourceModel(), &QAbstractItemModel::modelAboutToBeReset, this, &GeoLocFilter::processModelAboutToBeReset);
            connect(sourceModel(), &QAbstractItemModel::modelReset, this, &GeoLocFilter::processModelReset);
        }
    }

    clear();
    endResetModel();
}

// Return comparison result for single model column in the given order.
inline std::optional<bool>
GeoLocFilter::BiMap::cmpCol(int lhs, int rhs, int column, Qt::SortOrder order) const
{
    const QVariant lhsVal = m_flt->model().data(m_flt->model().index(lhs, column, sourceParent()), Util::RawDataRole);
    const QVariant rhsVal = m_flt->model().data(m_flt->model().index(rhs, column, sourceParent()), Util::RawDataRole);

    // All valid items before invalid.
    if (lhsVal.isValid() != rhsVal.isValid())
        return lhsVal.isValid() && !rhsVal.isValid();

    if (lhsVal != rhsVal)
        return (QtCompat::lt(lhsVal, rhsVal)) ^ (order != Qt::AscendingOrder);

    return { };
}

void GeoLocFilter::BiMap::sort()
{
    std::sort(m_proxyToSource.begin(), m_proxyToSource.end(), [&](const auto& lhs, const auto& rhs) {
        // Primary sort order
        if (const auto diff0 = cmpCol(lhs, rhs, m_flt->m_currColumn, m_flt->m_currOrder); diff0)
            return diff0.value();

        // Secondary (previously set) sort
        if (m_flt->m_prevColumn != m_flt->m_currColumn)
            if (const auto diff1 = cmpCol(lhs, rhs, m_flt->m_prevColumn, m_flt->m_prevOrder); diff1)
                return diff1.value();

        // Last resort: name
        if (const auto diff2 = cmpCol(lhs, rhs, GeoLocModel::Name, Qt::AscendingOrder); diff2)
            return diff2.value();

        return false;
    });

    // Rebuild source to proxy
    m_sourceToProxy.clear();
    for (int i = 0; i < m_proxyToSource.size(); ++i)
        m_sourceToProxy[m_proxyToSource.at(i)] = i;
}

inline void GeoLocFilter::BiMap::append(int child)
{
   m_sourceToProxy.insert(child, m_proxyToSource.size());
   m_proxyToSource.append(child);
}

void GeoLocFilter::sort(int column, Qt::SortOrder order)
{
    beginResetModel();

    if (column != m_currColumn) {
        m_prevColumn = m_currColumn;
        m_prevOrder  = m_currOrder;
    }

    m_currColumn = column;
    m_currOrder  = order;

    for (auto& map : m_map)
        map.sort();

    endResetModel();
}

void GeoLocFilter::processModelAboutToBeReset()
{
    beginResetModel();
    clear();
}

void GeoLocFilter::processModelReset()
{
    clear();
    endResetModel();
}

void GeoLocFilter::clear()
{
    m_map.clear();
}

void GeoLocFilter::setFilterEnabled(bool e)
{
    m_enabled = e;

    if (!e)
        clear();
}

decltype(GeoLocFilter::m_map)::const_iterator GeoLocFilter::map(const QModelIndex& idx) const
{
    const GeoLocModel::Node* node = model().getNode(idx);

    // See if we already mapped it.
    if (const auto it = m_map.constFind(quintptr(node)); it != m_map.cend()) {
        // Move to most recently used position
        if (const auto orderIt = std::find(m_order.begin(), m_order.end(), quintptr(node));
            orderIt != m_order.end())
            m_order.erase(orderIt);
        m_order.push_back(quintptr(node));
        return it;
    }

    qApp->setOverrideCursor(Qt::WaitCursor);

    // Remove older buckets
    while (m_order.size() >= maxBuckets) {
        m_map.remove(m_order.front());
        m_order.pop_front();
    }

    m_order.push_back(quintptr(node));

    // If not, create map entry.
    const auto parentEntry = m_map.insert(quintptr(node), BiMap(this, idx));

    // Add child rows
    for (int child = 0; child < int(node->childCount()); ++child)
        parentEntry->append(child);

    // Map all the leaf entries that satisfy filter criterea
    for (uint32_t rec = 0; rec < node->leafCount(); ++rec) {
        const int row = node->leafToRow(rec);

        if (m_currFilter.accepts(model().locDataFor(node, row)))
            parentEntry->append(row);
    }

    // Sort newly inserted data by current sort criteria.
    parentEntry->sort();

    qApp->restoreOverrideCursor();

    return { parentEntry };
}

// Update filter settings.
bool GeoLocFilter::update(const GeoPolRegionVec& geopol,                            // geopol
                          const Marble::GeoDataCoordinates& center, float maxDistM, // center+dist
                          const Marble::GeoDataLatLonBox& bounds,                   // bounds
                          uint32_t featureMask)                                     // features
{
    beginResetModel(); {
        m_currFilter = { geopol, center, bounds, maxDistM, featureMask };

        clear();  // clear and re-filter on next request

        m_prevFilter = m_currFilter;
        setFilterEnabled(true);  // always enable in case of sorting
    } endResetModel();

    return true;
}

// Given a name, find closest matching entry using haversine distance on sphere surface.
QModelIndex GeoLocFilter::findClosest(const QString& name) const
{
    const auto [node, srcParent] = model().parentFor(name);

    if (node == nullptr)
        return { };

    if (name.size() <= GeoLocModel::treeChars)
        return mapFromSource(srcParent);

    // We can't easily use std::lower_bound here since QModelIndex aren't iterators.
    const int srcRows = model().rowCount(srcParent);

    const QString lowerName = name.toLower();
    int count = srcRows;
    int first = 0;
    int pos, step;

    while (count > 0) {
        step = count / 2;
        pos = first + step;
        const QModelIndex idx = model().index(pos, GeoLocModel::Name, srcParent);
        if (model().data(idx, Util::RawDataRole).toString().toLower() < lowerName) {
            first = ++pos;
            count -= step + 1;
        } else {
            count = step;
        }
    }

    if (first >= srcRows)
        return { };

    // Search through all matches for this name and return closest.
    double minDist = std::numeric_limits<double>::max();
    QModelIndex closest;

    for (int row = first; row < srcRows; ++row) {
        const QModelIndex srcIdx = model().index(row, GeoLocModel::Dist, srcParent);
        const QModelIndex fltIdx = mapFromSource(srcIdx);

        // No more matches for this name.
        if (model().data(model().index(row, GeoLocModel::Name, srcParent), Util::RawDataRole).toString().toLower() != lowerName)
            break;

        const double dist = model().data(srcIdx, Util::RawDataRole).toDouble();
        if (dist < minDist && fltIdx.isValid()) {
            minDist = dist;
            closest = fltIdx;
        }
    }

    return closest;
}

bool GeoLocFilter::FilterParams::nonLocChange(const GeoLocFilter::FilterParams& rhs) const
{
    return (m_geoPol != rhs.m_geoPol) ||
            !Math::almost_equal(m_maxDistM, rhs.m_maxDistM) ||
            m_featureMask != rhs.m_featureMask;
}

// Return true if the filter accepts this location.
bool GeoLocFilter::FilterParams::accepts(const GeoLocEntry& loc) const
{
    const Marble::GeoDataCoordinates coords(double(loc.lonRad()), double(loc.latRad()));

    // These short-circuit if disabled by special values (e.g, empty bounds, zero distances)
    const bool passDist = m_maxDistM == 0.0f || loc.greatCircleDist(float(m_center.latitude()), float(m_center.longitude())) <= m_maxDistM;
    const bool passBounds = m_bounds.isEmpty() || m_bounds.contains(coords);
    const bool passFeature = bool((1U << int(loc.feature())) & m_featureMask);
    const bool passGeoPol = m_geoPol.empty() || m_geoPol.back()->intersects(coords);

    // Add to filter if it satisfies all criteria.
    return passDist && passBounds && passFeature && passGeoPol;
}
