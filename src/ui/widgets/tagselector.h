/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TAGSELECTOR_H
#define TAGSELECTOR_H

#include <QWidget>
#include <QButtonGroup>

#include <src/ui/filters/contentfilter.h>
#include "src/core/tagmodel.h"

namespace Ui {
class TagSelector;
} // namespace Ui

class MainWindow;
class QShowEvent;

class TagSelector final : public QWidget
{
    Q_OBJECT

public:
    explicit TagSelector(const MainWindow&, QWidget *parent = nullptr);
    ~TagSelector() override;

    [[nodiscard]] QStringList  tags() const;       // get active tags after user selection
    TagSelector& setTags(const QStringList& tags); // set initial active tags
    TagSelector& clearTags();                      // clear active tags
    TagSelector& setMaxTags(int maxTags);          // set max tags to select

private slots:
    void activeDoubleClicked();
    void availDoubleClicked();
    void recentTagClicked(QAbstractButton*);
    void on_action_Left_triggered();
    void on_action_Right_triggered();
    void on_action_Up_triggered();
    void on_action_Down_triggered();

private:
    void showEvent(QShowEvent*) override;
    void setupActionIcons();
    void setupTagSelectors();
    void setupSignals();
    void setupButtons();
    void moveSelections(int rel);
    void toActive();
    void toAvail();
    void setVisible(bool visible) override;
    void update(); // refresh sizes, filters, and so forth
    void setRecentTags(const QStringList& tags); // tags to show in recent list
    void updateActions(); // update enable states
    int  numActiveTags() const; // count of active tags

    Ui::TagSelector   *ui;

    const MainWindow&  m_mainWindow;       // main window reference
    TagModel           m_activeTags;       // these are the active tags we're editing.
    ContentFilter      m_activeFilter;     // filter out items from active set
    QButtonGroup       m_recentTagButtons; // button group for recent tags
    int                m_maxTags;          // max tags to allow selection of

    static const constexpr char* tagProperty    = "zt-tag";
};

#endif // TAGSELECTOR_H
