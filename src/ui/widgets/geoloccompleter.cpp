/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cstdlib>
#include <array>

#include <QTableView>
#include <QHeaderView>
#include <QFontMetrics>
#include <QApplication>

#include "geoloccompleter.h"

#include <src/util/units.h>
#include <src/util/roles.h>

#include "src/core/geolocmodel.h"

GeoLocCompleter::GeoLocCompleter(QAbstractItemModel* model, QObject* parent) :
    QCompleter(model, parent),
    m_flagDelegate(this, tr("Flags"), true, Util::RawDataRole),
    m_completionView(new QTableView())
{
    setupView();
    setupDelegates();
}

GeoLocCompleter::~GeoLocCompleter()
{
}

void GeoLocCompleter::setupView()
{
    model()->fontSizeChanged();

    m_completionView->verticalHeader()->setVisible(false);

    const QFontMetrics fontMetric(QApplication::font());

    const int em  = fontMetric.size(Qt::TextSingleLine, "M").width();
    const int lat = fontMetric.size(Qt::TextSingleLine, model()->mdUnits(GeoLocModel::Lat)(-88.88888888f)).width();
    const int lon = fontMetric.size(Qt::TextSingleLine, model()->mdUnits(GeoLocModel::Lon)(-179.88888888f)).width();
    const int dist = fontMetric.size(Qt::TextSingleLine, model()->mdUnits(GeoLocModel::Dist)(8888888.88)).width();
    const int type = GeoLocEntry::longestFeatureName(fontMetric);

    QHeaderView& header = *m_completionView->horizontalHeader();

    header.setSectionsMovable(true);
    header.setSortIndicator(GeoLocModel::Dist, Qt::AscendingOrder);

    std::array<int, GeoLocModel::_Count> widths = { };
    widths[GeoLocModel::Name]  = em * 20;
    widths[GeoLocModel::Flags] = int(fontMetric.height() * 2.5 * 2);
    widths[GeoLocModel::TZ]    = em * 8;
    widths[GeoLocModel::Type]  = type + em;
    widths[GeoLocModel::Dist]  = dist + em;
    widths[GeoLocModel::Lat]   = lat + em;
    widths[GeoLocModel::Lon]   = lon + em;

    static const int tableRowSpace = 6; // TODO: how to query this?

    int totalWidth = 0;
    for (uint32_t w = 0; w < widths.size(); ++w) {
        totalWidth += widths.at(w) + tableRowSpace;
        header.resizeSection(int(w), widths.at(w));
    }

    m_completionView->setMinimumWidth(totalWidth);
    m_completionView->setMinimumHeight(header.height() + fontMetric.lineSpacing() * 15);
    m_completionView->setAlternatingRowColors(true);
    m_completionView->setSortingEnabled(true);

    setPopup(m_completionView);
    setWrapAround(false);

    setCaseSensitivity(Qt::CaseInsensitive);
    setMaxVisibleItems(16); // TODO: adjust for window height
}

void GeoLocCompleter::setupDelegates()
{
    m_completionView->setItemDelegateForColumn(GeoLocModel::Flags, &m_flagDelegate);
}

// Because there may be 10+ million completion possibilities, for performance reasons
// we build a hierarchy for the first few characters of the path.
QStringList GeoLocCompleter::splitPath(const QString& path) const
{
    QStringList paths;

    m_lastIndex = QModelIndex();

    for (int i = 0; i < std::min(GeoLocModel::treeChars, path.size()); ++i)
        paths.append(path.left(i+1));

    // Force completion in initial single-char levels
    if (path.size() < GeoLocModel::treeChars)
        paths.append(paths.back());
    else
        paths.append(path);

    return paths;
}

// It's difficult to get the index (as opposed to the string) from the completer
// after it's activated, so we overload this function to remember them as they
// go by.
QString GeoLocCompleter::pathFromIndex(const QModelIndex& idx) const
{
    m_lastIndex = idx;
    return QCompleter::pathFromIndex(idx);
}

void GeoLocCompleter::save(QSettings& settings) const
{
    SL::Save(settings, "header", *m_completionView->horizontalHeader());
}

void GeoLocCompleter::load(QSettings& settings)
{
    SL::Load(settings, "header", *m_completionView->horizontalHeader());
}
