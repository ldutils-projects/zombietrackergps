/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "src/core/cfgdata.h"
#include "src/core/geopolmgr.h"
#include "src/core/app.h"
#include "flagdelegate.h"

FlagDelegate::FlagDelegate(QObject* /*parent*/,
                           const QString &winTitle, bool winBorders, int role) :
    MultiIconDelegate(nullptr, winTitle, OutlineMode::Box, winBorders, role)
{
}

QWidget* FlagDelegate::createEditor(QWidget*, const QStyleOptionViewItem&, const QModelIndex&) const
{
    return nullptr;
}

void FlagDelegate::setModelData(QWidget*, QAbstractItemModel*, const QModelIndex&) const
{
}

QIcon FlagDelegate::iconFor(const QString& name) const
{
    if (const GeoPolRegion* region = app().geoPolMgr()[name]; region != nullptr)
        return region->flagIcon();

    return { };
}

QSize FlagDelegate::iconSize() const
{
    return cfgData().flagSizeTrack;
}

int FlagDelegate::maxIcons() const
{
    return cfgData().maxTrackPaneFlags;
}
