/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TAGSELECTORDELEGATE_H
#define TAGSELECTORDELEGATE_H

#include "src/ui/widgets/multiicondelegate.h"

class MainWindow;

class TagSelectorDelegate final : public MultiIconDelegate
{
public:
    TagSelectorDelegate(const MainWindow&, const QString& winTitle,
                        bool winBorders = true, int role = Util::RawDataRole);

    QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& idx) const override;
    void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& idx) const override;

private:
    QIcon iconFor(const QString&) const override;
    QSize iconSize() const override; // query icon size
    int   maxIcons() const override; // query icon count

    const MainWindow& m_mainWindow;
};

#endif // TAGSELECTORDELEGATE_H
