/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QList>
#include <QIcon>
#include <QMap>

#include "tagselector.h"
#include "ui_tagselector.h"

#include <algorithm>
#include <cassert>
#include <src/util/util.h>
#include <src/util/icons.h>
#include <src/util/ui.h>

#include "src/core/cfgdata.h"
#include "src/core/app.h"
#include "src/ui/windows/mainwindow.h"

TagSelector::TagSelector(const MainWindow& mainWindow, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TagSelector),
    m_mainWindow(mainWindow),
    m_activeFilter(m_activeTags, TagModel::Name, Util::RawDataRole, this),
    m_maxTags(64)
{
    ui->setupUi(this);

    setupActionIcons();
    setupTagSelectors();
    setupSignals();
    setupButtons();

    Util::SetupWhatsThis(this);
}

TagSelector::~TagSelector()
{
    delete ui;
}

void TagSelector::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Left,  "arrow-left");
    Icons::defaultIcon(ui->action_Right, "arrow-right");
    Icons::defaultIcon(ui->action_Up,    "arrow-up");
    Icons::defaultIcon(ui->action_Down,  "arrow-down");
}

void TagSelector::setupTagSelectors()
{
    QTreeView& availTagView  = *ui->availableTags;
    QTreeView& trackTagView  = *ui->activeTags;

    // This is really read-only, but the Qt API only accepts non-const pointers, so we cast the
    // constness away.
    m_activeFilter.setSourceModel(const_cast<TagModel*>(&cfgData().tags));

    availTagView.setModel(&m_activeFilter);
    trackTagView.setModel(&m_activeTags);

    availTagView.setIconSize(cfgData().iconSizeTag);
    trackTagView.setIconSize(cfgData().iconSizeTag);

    // Hide some columns
    for (ModelType mt = TagModel::_First; mt < TagModel::_Count; ++mt) {
        const bool hidden = !(mt == TagModel::Name || mt == TagModel::Color || mt == TagModel::Icon);

        availTagView.setColumnHidden(mt, hidden);
        trackTagView.setColumnHidden(mt, hidden);
    }
}

void TagSelector::setupSignals()
{
    connect(ui->availableTags,  &QTreeView::doubleClicked, this, &TagSelector::availDoubleClicked);
    connect(ui->activeTags,     &QTreeView::doubleClicked, this, &TagSelector::activeDoubleClicked);

    // idClicked signal isn't present on versions of Qt before 5.15.
    connect(&m_recentTagButtons, static_cast<void(QButtonGroup::*)(QAbstractButton*)>(&QButtonGroup::buttonClicked),
            this, &TagSelector::recentTagClicked);
}

void TagSelector::setupButtons()
{
    ui->tagLeft->setDefaultAction(ui->action_Left);
    ui->tagRight->setDefaultAction(ui->action_Right);
    ui->tagUp->setDefaultAction(ui->action_Up);
    ui->tagDown->setDefaultAction(ui->action_Down);
}

QStringList TagSelector::tags() const
{
    QStringList tags;
    tags.reserve(m_activeTags.rowCount());

    for (int row = 0; row < m_activeTags.rowCount(); ++row)
        tags.append(m_activeTags.data(m_activeTags.index(row, TagModel::Name),
                                      m_activeTags.keyRole()).toString());

    return tags;
}

TagSelector& TagSelector::setTags(const QStringList& tags)
{
    m_activeTags.clear();

    // find index from source and insert them in order.
    for (const auto& tag : tags)
        if (QModelIndex srcIdx = cfgData().tags.keyIdx(tag); srcIdx.isValid())
            m_activeTags.insertRow(cfgData().tags, srcIdx, m_activeTags.rowCount());

    update();
    return *this;
}

TagSelector& TagSelector::clearTags()
{
    return setTags(QStringList());
}

TagSelector& TagSelector::setMaxTags(int maxTags)
{
    m_maxTags = maxTags;

    // Remove excess
    if (const int excess = numActiveTags() - m_maxTags; excess > 0)
        m_activeTags.removeRows(m_activeTags.rowCount() - excess, excess);

    update();
    return *this;
}

void TagSelector::setRecentTags(const QStringList& tags)
{
    // tagMoveButtons group holds these items:
    //   0..N-1: recent tag buttons
    //   ....
    //   N: QSpacer
    //   N+1...N+4 movement buttons

    static const int moveButtonCount = 4;
    [[maybe_unused]] static const int spacerCount = 1;

    // Remove buttons from button group
    for (auto* button : m_recentTagButtons.buttons()) {
       m_recentTagButtons.removeButton(button);
       ui->tagMoveButtons->removeWidget(button);
       delete button;
    }

    assert(ui->tagMoveButtons->count() == moveButtonCount + spacerCount);

    QSize iconSize(16,16);
    if (auto* button = dynamic_cast<QAbstractButton*>(ui->tagMoveButtons->layout()->itemAt(0)->widget()); button != nullptr)
        iconSize = button->iconSize();

    int buttonId = 0;
    for (const auto& tag : tags) {
        auto* tb = new QToolButton;
        tb->setIcon(QIcon(cfgData().tags.tagIconName(tag)));
        tb->setIconSize(iconSize);
        tb->setToolTip(tag);
        tb->setProperty(tagProperty, tag);
        ui->tagMoveButtons->insertWidget(ui->tagMoveButtons->count() - moveButtonCount - 1, tb);

        m_recentTagButtons.addButton(tb, buttonId++);
    }
}

void TagSelector::toActive()
{
    const QModelIndexList selection = Util::MapDown(ui->availableTags->selectionModel()->selectedRows());
    const QModelIndex currentIdxDst = ui->activeTags->selectionModel()->currentIndex();

    const QModelIndex insertParent = (currentIdxDst.isValid() ? m_activeTags.parent(currentIdxDst) : QModelIndex());
    int               insertPos    = (currentIdxDst.isValid() ? currentIdxDst.row() : m_activeTags.rowCount());

    // Insert selected tags, but do not exceed m_maxTags active at once.
    for (const auto& idx : selection)
        if (idx.column() == 0)
            if (!m_activeTags.contains(cfgData().tags.data(TagModel::Name, idx)) &&
                !m_activeTags.isCategory(idx) &&
                numActiveTags() < m_maxTags)
                m_activeTags.insertRow(cfgData().tags, idx, insertPos++, insertParent);

    // For convenience, select them.
    QItemSelectionModel* selectModel = ui->activeTags->selectionModel();
    selectModel->reset(); // don't emit selection changed

    for (const auto& idx : selection)
        if (idx.column() == 0)
            if (const QModelIndex trackIdx = m_activeTags.keyIdx(cfgData().tags.data(TagModel::Name, idx));
                    trackIdx.isValid())
                selectModel->select(trackIdx, QItemSelectionModel::Select | QItemSelectionModel::Rows);

    update();
}

void TagSelector::toAvail()
{
    // We can simply remove selected entries from the active tags list.
    Util::RemoveRows(m_activeTags, ui->activeTags->selectionModel());

    update();
}

void TagSelector::setVisible(bool visible)
{
    if (visible) {
        ui->availableTags->expandAll();
        update();
    }

    QWidget::setVisible(visible);
}

void TagSelector::updateActions()
{
    ui->action_Left->setEnabled(numActiveTags() < m_maxTags);
}

int TagSelector::numActiveTags() const
{
    return m_activeTags.rowCount();
}

void TagSelector::update()
{
    m_activeFilter.invalidate(); // invalidate filter

    Util::ResizeViewForData(*ui->activeTags);
    Util::ResizeViewForData(*ui->availableTags);

    updateActions(); // update enable states
}

void TagSelector::moveSelections(int rel)
{
    QItemSelectionModel* selectModel = ui->activeTags->selectionModel();

    const QModelIndexList selection = selectModel->selectedRows();
    QList<QPersistentModelIndex> sorted;
    QPersistentModelIndex current = selectModel->currentIndex();

    sorted.reserve(selection.size());
    for (const auto& idx : selection)
        sorted.append(idx);

    // Choose order depending on whether it's moving up or down.  Otherwise we step on ones
    // we've already moved.
    std::sort(sorted.begin(), sorted.end(), [&rel](const QModelIndex& l, const QModelIndex& r) {
        return (rel < 0) ? (l.row() < r.row()) : (l.row() > r.row());
    });

    for (const auto& idx : sorted) {
        const QModelIndex parent = m_activeTags.parent(idx);
        const int newRow = idx.row() + rel;
        // Moving down is increment of 2: see comment in docs for QAbstractItemModel::beginMoveRows.
        if (newRow >= 0 && newRow <= (m_activeTags.rowCount(parent)+1))
            m_activeTags.moveRow(parent, idx.row(), parent, newRow);
    }

    selectModel->reset(); // don't emit selection changed
    selectModel->setCurrentIndex(current, QItemSelectionModel::Current | QItemSelectionModel::Rows);

    for (const auto& idx : sorted)
        selectModel->select(idx, QItemSelectionModel::Select | QItemSelectionModel::Rows);
}

void TagSelector::activeDoubleClicked()
{
    toAvail();
}

void TagSelector::availDoubleClicked()
{
    toActive();
}

void TagSelector::recentTagClicked(QAbstractButton* button)
{
    auto* tb = qobject_cast<QToolButton*>(button);
    if (tb == nullptr)
        return;

    const QString tagName = tb->property(tagProperty).toString();

    // Search in active and available tag sets
    const QModelIndex activeIdx = m_activeTags.findRow(QModelIndex(), tagName, TagModel::Name, Util::RawDataRole);
    const QModelIndex availIdx  = Util::MapUp(&m_activeFilter,
                                              cfgData().tags.findRow(QModelIndex(), tagName,
                                                                     TagModel::Name, Util::RawDataRole));

    static const QItemSelectionModel::SelectionFlags selectMode = QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows;

    // Move the requested tag into the active or available set
    if (activeIdx.isValid()) {
        ui->activeTags->selectionModel()->select(activeIdx, selectMode);
        toAvail();
    } else if (availIdx.isValid()) {
        ui->availableTags->selectionModel()->select(availIdx, selectMode);
        toActive();
    }
}

void TagSelector::on_action_Left_triggered()
{
    toActive();
}

void TagSelector::on_action_Right_triggered()
{
    toAvail();
}

void TagSelector::on_action_Up_triggered()
{
    moveSelections(-1);
}

void TagSelector::on_action_Down_triggered()
{
    // Moving down is 2: see comment in docs for QAbstractItemModel::beginMoveRows.
    moveSelections(2);
}

void TagSelector::showEvent(QShowEvent* event)
{
    // Populate recent tags
    setRecentTags(m_mainWindow.recentTags());

    QWidget::showEvent(event);
}
