/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TRACKMAP_H
#define TRACKMAP_H

#include <QPersistentModelIndex>
#include <QPixmap>
#include <QTimer>
#include <QPoint>
#include <QMap>
#include <marble/MarbleWidget.h>

#include <src/fwddecl.h>
#include <src/core/geotypes.h>
#include <src/util/roles.h>

#include "src/core/pointitem.h"
#include "src/core/viewmodel.h"
#include "src/core/viewparams.h"

namespace Marble {
class GeoPainter;
class GeoDataCoordinates;
class MarbleWidgetInputHandler;
} // namespace Marble

class MainWindow;
class TrackModel;
class WaypointModel;
class QMouseEvent;
class MapPane;
class Pane;
class UndoMgr;

// This class inherits from MarbleWidget to paint some data on top of the map,
// where otherwise we would need a prohibitive number of Placemarks.  Allocating
// all those on the fly is very slow.  This is a higher performance way to draw
// icons on track points, for example.
class TrackMap final : public Marble::MarbleWidget
{
    Q_OBJECT

public:
    TrackMap(MainWindow&, MapPane&);

    void newConfig();

    void setOfflineMode(bool offline);

    void startSelectRegion(const QPoint&);
    void endSelectRegion(const QPoint&);

    ViewParams viewParams() const;
    void gotoView(const ViewParams&, bool addUndo = true);

    Marble::GeoDataCoordinates center() const; // return center of view
    Marble::GeoDataLatLonBox bounds() const;   // bounding box for screen view

    Marble::GeoDataCoordinates mouseGeoCoordinates() const; // current mouse coordinates

    // registration functions for tracking GPSD live points
    void registerGpsdPoint(const Pane&);              // register GPSD point
    void setGpsdPoint(const Pane&, const PointItem&); // set point for pane
    void unregisterGpsdPoint(const Pane&);            // unregister GPSD point

protected:
    void customPaint(Marble::GeoPainter* painter) override;

signals:
    void mouseMoveGeoCoords(const Marble::GeoDataCoordinates&);
    void viewMoveIdle(); // when map stops moving

public slots:
    void mapThemeSelected(const QString&);

private slots:
    void currentTrackChanged(const QModelIndex&);
    void currentWaypointChanged(const QModelIndex&);
    void currentPointChanged(const QModelIndex&);
    void pointDataChanged(const QModelIndex&);
    void pointModelReset();
    void visibleTracksChanged();
    void visibleWaypointsChanged();
    void selectedPointsChanged();
    void newViewContext(Marble::ViewContext newViewContext);
    void processRegionSelected(const QList<double>&);
    void hqUpdate();
    void lqUpdate();
    void handleProgress(int active, int queued);
    void handleJobRemoved();
    void trackViewMove();

private:
    friend class UndoMapBase;
    friend class TestZtgps;

    MainWindow& mainWindow() { return m_mainWindow; }
    const MainWindow& mainWindow() const { return m_mainWindow; }

    inline TrackModel& trackModel();
    inline const TrackModel& trackModel() const;

    inline WaypointModel& waypointModel();
    inline const WaypointModel& waypointModel() const;

    inline const MapPane& mapPane() const { return m_mapPane; }
    inline MapPane& mapPane() { return m_mapPane; }

    UndoMgr& undoMgrView();
    UndoMgr& undoMgr();
    const UndoMgr& undoMgr() const;

    void addViewMove(const ViewParams&, bool addUndo);

    // Mouse events
    void mousePressEvent(QMouseEvent*) override;
    void mouseReleaseEvent(QMouseEvent*) override;
    void mouseMoveEvent(QMouseEvent*) override;

    // Return lon/lat in radians
    inline std::tuple<Lon_t, Lat_t> mouseGeoCoordinates(QMouseEvent*, Marble::GeoDataCoordinates::Unit);

    // Move-mode mouse events
    bool mousePressEventMove(QMouseEvent*);
    bool mouseReleaseEventMove(QMouseEvent*);
    bool mouseMoveEventMove(QMouseEvent*);

    // Add-mode mouse events
    bool mousePressEventAdd(QMouseEvent*);
    bool mouseReleaseEventAdd(QMouseEvent*);
    bool mouseMoveEventAdd(QMouseEvent*);

    // Select-mode mouse events
    bool mousePressEventSelect(QMouseEvent*);
    bool mouseReleaseEventSelect(QMouseEvent*);
    bool mouseMoveEventSelect(QMouseEvent*);

    bool updatePointAtMouse(QMouseEvent*, bool update); // used during interactive dragging

    void resizeEvent(QResizeEvent *event) override; // low quality during resize events

    template <typename T> T get(int mt, const QModelIndex& idx, int role = Util::RawDataRole) const;

    // Drawing is skipped if draw=false.  Proximity (threshold) detection is skipped if force=true
    inline void drawSinglePt(Marble::GeoPainter*, const PointItem&, qreal threshold, const QPixmap&,
                             bool draw, bool force);
    void drawSingleTrack(Marble::GeoPainter*, const QModelIndex&);
    void drawSingleWaypoint(Marble::GeoPainter*, const QModelIndex&);
    void drawTrackPts(Marble::GeoPainter*);
    void drawTrackLines(Marble::GeoPainter*);
    void drawTrackWaypoints(Marble::GeoPainter*);
    void drawSelectionBox(Marble::GeoPainter*);
    void selectPoint(const QModelIndex&, bool add);

    // Factor out redundancy between drawing various models. E.g, tracks, or waypoints.
    void drawModel(Marble::GeoPainter*, void (TrackMap::*drawFn)(Marble::GeoPainter*, const QModelIndex&),
                   const QAbstractItemModel& model,
                   const QPersistentModelIndex& currentIdx,
                   bool drawWhileMoving);

    inline static void drawTrackSeg(Marble::GeoPainter*, const QColor&, float trackWidth,
                             const TrackSegLines&);
    bool isCurrentTrack(const QModelIndex& idx) const { return m_currentTrackIdx == idx; }
    bool isCurrentWaypoint(const QModelIndex& idx) const { return m_currentWaypointIdx == idx; }

    bool hasGpsdLivePoints() const { return !m_gpsdPoints.isEmpty(); }

    void setupIcons();
    void setupSignals();
    void setupTimers();
    void deferredUpdate(int mSec = 500);
    auto waypointIcon(const QModelIndex&);

    static QPixmap pixmapFromIcon(const QIcon& icon, int newSize);

    void setViewContext(Marble::ViewContext);
    Marble::GeoDataCoordinates widgetGeoCoords(const QPoint&) const;

//  MarbleWidget::regionSelected (for some reason!) has different definitions between
//  the same apparent version (libmarblewidget-qt5.so.28) between different flavors of
//  Linux.  Unclear why, but it means we can't use that symbol from the shared library
//  and successfully run in all environments.

//  To work around that, we draw our own selection box, which is very awkward in the
//  MarbleWidget API, and doesn't erase itself correctly in all situations :(.

//  Ubuntu and Debian, from objdump -C -T <lib> | grep -i regionSelect:
//    Marble::MarbleWidget::regionSelected(QList<double> const&)
//    Marble::MarbleAbstractPresenter::regionSelected(QList<double> const&)

//  OpenSUSE, same library version:
//    Marble::MarbleAbstractPresenter::regionSelected(Marble::GeoDataLatLonBox const&)
//    Marble::MarbleWidget::regionSelected(Marble::GeoDataLatLonBox const&)

    bool regionSelectionStarted() const { return !m_selectionStartCoordinate.isNull(); }
    bool regionSelectionInProgress() const { return !m_selectionStartCoordinate.isNull() && !m_selectionEndCoordinate.isNull(); }

    MainWindow&            m_mainWindow;

    QPoint                 m_selectionStartCoordinate;
    QPoint                 m_selectionEndCoordinate;
    QPoint                 m_currentMouseCoordinates;

    // For mapping numeric tokens to data from GpsCapturePanes (live GPSD data).
    QMap<PaneId_t, PointItem>  m_gpsdPoints;     // map paneId to GPS point

    // for tracking which track we've set to active.  This is here because setting
    // the track as active may change it in multiple map views, which all share the
    // same GeoDataStyle pointer, which we want to change once, not once per map.
    QPersistentModelIndex  m_currentTrackIdx;          // reflect last current track
    QPersistentModelIndex  m_currentPointIdx;          // reflect last current point
    QPersistentModelIndex  m_currentWaypointIdx;       // reflect last current waypoint
    bool                   m_dragging         = false; // true while dragging
    bool                   m_dragThresholdMet = false; // true if we exceed the drag threshold
    bool                   m_selectingPoints  = false; // true during point selection
    PointItem::Lon_t       m_dragInitLon      = 0.0;   // initial drag lon
    PointItem::Lat_t       m_dragInitLat      = 0.0;   // initial drag lat
    QPoint                 m_dragInitPoint;            // threshold test, so we can click without dragging

    QPixmap                m_defaultPointIcon;         // for default track points
    QPixmap                m_selectedPointIcon;        // for selected track points
    QPixmap                m_currentPointIcon;         // for current track points
    QPixmap                m_gpsdLivePointIcon;        // for GPSD live points
    QMap<QString, QPixmap> m_waypointIcons;            // indexed by icon name

    QTimer                 m_hqUpdateTimer;            // high quality update
    QTimer                 m_lqUpdateTimer;            // low quality update
    QTimer                 m_mapIdleTimer;             // timer for map idleness

    qreal                  m_prevX            = 0.0;   // for point proximity culling
    qreal                  m_prevY            = 0.0;   // ...

    MapPane&               m_mapPane;                  // pane we're managed by
    ViewParams             m_prevView;                 // for view undoing
};

#endif // TRACKMAP_H
