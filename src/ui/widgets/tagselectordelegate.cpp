/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <src/core/cfgdata.h>

#include "src/ui/windows/mainwindow.h"
#include "src/core/tagmodel.h"
#include "src/core/app.h"
#include "src/ui/dialogs/tagselectordialog.h"
#include "tagselectordelegate.h"

TagSelectorDelegate::TagSelectorDelegate(const MainWindow& mainWindow,
                                         const QString &winTitle, bool winBorders, int role) :
    MultiIconDelegate(nullptr, winTitle, OutlineMode::None, winBorders, role),
    m_mainWindow(mainWindow)
{
}

QWidget* TagSelectorDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& /*option*/, const QModelIndex& idx) const
{
    auto* editor = new TagSelectorDialog(m_mainWindow, parent);
    if (editor == nullptr)
        return nullptr;

    if (const auto* model = idx.model(); model != nullptr) {
        (*editor)().setTags(model->data(idx, role).value<QStringList>());
        setPopup(editor);
    }

    return editor;
}

void TagSelectorDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& idx) const
{
    updateResultFromEditor(editor, model, idx, [&](bool& accepted) {
        auto* dialog = dynamic_cast<TagSelectorDialog*>(editor);

        accepted = (dialog->result() == QDialog::Accepted);
        return (*dialog)().tags();
    });
}

QIcon TagSelectorDelegate::iconFor(const QString& name) const
{
    if (const QVariant icon = cfgData().tags.value(name, TagModel::Icon, Qt::DecorationRole);
            icon.type() == QVariant::Icon)
        return icon.value<QIcon>();

    return { };
}

QSize TagSelectorDelegate::iconSize() const
{
    return cfgData().iconSizeTrack;
}

int TagSelectorDelegate::maxIcons() const
{
    return cfgData().maxTrackPaneIcons;
}
