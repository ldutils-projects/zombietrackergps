/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <limits>
#include <cassert>
#include <chrono>

#include <QIcon>
#include <QPen>
#include <QMouseEvent>
#include <QRect>
#include <QMutexLocker>
#include <QCursor>
#include <QPoint>
#include <QPointF>

#include <marble/GeoPainter.h>
#include <marble/MarbleWidgetInputHandler.h>
#include <marble/GeoDataLatLonBox.h>
#include <marble/MarbleModel.h>
#include <marble/HttpDownloadManager.h>
#include <marble/ViewportParams.h>
#include <marble/GeoDataCoordinates.h>

#include <src/util/math.h>
#include <src/util/util.h>
#include <src/undo/undomgr.h>
#include <src/core/app.h>

#include "src/core/trackmodel.h"
#include "src/core/waypointmodel.h"
#include "src/core/cfgdata.h"
#include "src/core/pointmodel.h"
#include "src/ui/windows/mainwindow.h"
#include "src/ui/panes/pane.h"
#include "src/ui/panes/pointpane.h"
#include "src/ui/panes/mappane.h"
#include "src/undo/undomap.h"

#include "trackmap.h"

TrackMap::TrackMap(MainWindow& mainWindow, MapPane& mapPane) :
    m_mainWindow(mainWindow),
    m_hqUpdateTimer(this),
    m_lqUpdateTimer(this),
    m_mapPane(mapPane)
{
    setupIcons();
    setupSignals();
    setupTimers();

    using namespace std::chrono_literals;
    m_mapIdleTimer.start(1s); // create initial map movement point, for undo
}

void TrackMap::newConfig()
{
    inputHandler()->setInertialEarthRotationEnabled(cfgData().mapInertialMovement);

    setupIcons();
    update();
}

inline TrackModel& TrackMap::trackModel()
{
    // We use const vs non-const to return a const or non-const model. This bugs clang-tidy,
    // so we mention 'this' so it doesn't suggest making this a static method.
    (void)this;
    return app().trackModel();
}

inline const TrackModel& TrackMap::trackModel() const
{
    // We use const vs non-const to return a const or non-const model. This bugs clang-tidy,
    // so we mention 'this' so it doesn't suggest making this a static method.
    (void)this;
    return app().trackModel();
}

inline WaypointModel& TrackMap::waypointModel()
{
    // We use const vs non-const to return a const or non-const model. This bugs clang-tidy,
    // so we mention 'this' so it doesn't suggest making this a static method.
    (void)this;
    return app().waypointModel();
}

inline const WaypointModel& TrackMap::waypointModel() const
{
    // We use const vs non-const to return a const or non-const model. This bugs clang-tidy,
    // so we mention 'this' so it doesn't suggest making this a static method.
    (void)this;
    return app().waypointModel();
}

QPixmap TrackMap::pixmapFromIcon(const QIcon& icon, int newSize)
{
    const QSize actualSize = icon.actualSize(QSize(newSize, newSize));
    return icon.pixmap(actualSize).scaledToWidth(newSize, Qt::SmoothTransformation);
}

void TrackMap::setupIcons()
{
    m_defaultPointIcon  = pixmapFromIcon(QIcon(cfgData().defaultPointIcon),  cfgData().defaultPointIconSize);
    m_selectedPointIcon = pixmapFromIcon(QIcon(cfgData().selectedPointIcon), cfgData().selectedPointIconSize);
    m_currentPointIcon  = pixmapFromIcon(QIcon(cfgData().currentPointIcon),  cfgData().currentPointIconSize);
    m_gpsdLivePointIcon = pixmapFromIcon(QIcon(cfgData().gpsdLivePointIcon), cfgData().gpsdLivePointIconSize);

    // Clear the waypoint icon cache so it repopulates
    m_waypointIcons.clear();
}

void TrackMap::setupSignals()
{
    connect(&mainWindow(), &MainWindow::currentTrackChanged, this, &TrackMap::currentTrackChanged);
    connect(&mainWindow(), &MainWindow::currentTrackPointChanged, this, &TrackMap::currentPointChanged);
    connect(&mainWindow(), &MainWindow::currentWaypointChanged, this, &TrackMap::currentWaypointChanged);
    connect(&mainWindow(), &MainWindow::selectedPointsChanged, this, &TrackMap::selectedPointsChanged);
    connect(&mainWindow(), &MainWindow::visibleTracksChanged, this, &TrackMap::visibleTracksChanged);
    connect(&mainWindow(), &MainWindow::visibleWaypointsChanged, this, &TrackMap::visibleWaypointsChanged);

    connect(this, &TrackMap::viewContextChanged, this, &TrackMap::newViewContext);

    // Progress bar
    connect(model()->downloadManager(), &Marble::HttpDownloadManager::progressChanged, this, &TrackMap::handleProgress);
    connect(model()->downloadManager(), &Marble::HttpDownloadManager::jobRemoved, this, &TrackMap::handleJobRemoved);
}

void TrackMap::setupTimers()
{
    // Wait a little bit after the last active container change, and then resize columns.
    m_hqUpdateTimer.setSingleShot(true);
    connect(&m_hqUpdateTimer, &QTimer::timeout, this, &TrackMap::hqUpdate);

    m_lqUpdateTimer.setSingleShot(true);
    connect(&m_lqUpdateTimer, &QTimer::timeout, this, &TrackMap::lqUpdate);

    // Map idleness
    m_mapIdleTimer.setSingleShot(true);
    connect(&m_mapIdleTimer, &QTimer::timeout, this, &TrackMap::trackViewMove);
}

void TrackMap::setOfflineMode(bool offline)
{
    model()->downloadManager()->setDownloadEnabled(!offline);
}

template <typename T> T TrackMap::get(int mt, const QModelIndex& idx, int role) const
{
    return trackModel().data(mt, idx, role).value<T>();
}

void TrackMap::handleProgress(int active, int queued)
{
    mainWindow().initProgress(active + queued);
}

void TrackMap::handleJobRemoved()
{
    mainWindow().updateProgress(1, true);
}

// Parameters that define the view
ViewParams TrackMap::viewParams() const
{
    return ViewParams({ centerLatitude(), centerLongitude(), heading(), zoom() });
}

void TrackMap::gotoView(const ViewParams& vp, bool addUndo)
{
    if (vp.hasHeading())
        setHeading(vp.heading());

    if (vp.hasBounds()) {
        centerOn(vp.bounds(), false);
    } else {
        if (vp.hasZoom())
            setZoom(vp.zoom(), Marble::Instant);

        centerOn(vp.centerLon(), vp.centerLat(), false);
    }

    addViewMove(vp, addUndo);
}

Marble::GeoDataCoordinates TrackMap::center() const
{
    return { Math::toRad(centerLongitude()), Math::toRad(centerLatitude()) };
}

Marble::GeoDataLatLonBox TrackMap::bounds() const
{
    return viewport()->viewLatLonAltBox();
}

Marble::GeoDataCoordinates TrackMap::mouseGeoCoordinates() const
{
    return widgetGeoCoords(m_currentMouseCoordinates);
}

void TrackMap::registerGpsdPoint(const Pane& pane)
{
    assert(m_gpsdPoints.find(pane.paneId()) == m_gpsdPoints.end());
    m_gpsdPoints.insert(pane.paneId(), PointItem());
}

void TrackMap::setGpsdPoint(const Pane& pane, const PointItem& point)
{
    m_gpsdPoints.insert(pane.paneId(), point);
    setViewContext(Marble::Animation);
    update();
}

void TrackMap::unregisterGpsdPoint(const Pane& pane)
{
    m_gpsdPoints.remove(pane.paneId());
    deferredUpdate();
}

// Build the view history
void TrackMap::addViewMove(const ViewParams& newView, bool addUndo)
{
    // Track view history, but only if it's not identical to the last one.
    if (m_prevView != newView) {
        if (addUndo && m_prevView.isValid()) {
            const UndoMgr::ScopedUndo undoSet(undoMgrView(), tr("Move Map"));
            undoMgrView().add(new UndoMapView(*this, m_prevView, newView));
        }

        emit viewMoveIdle();
    }

    m_prevView = newView;
}

// When the map has been idle for a little while
void TrackMap::trackViewMove()
{
    addViewMove(viewParams(), true);
}

UndoMgr& TrackMap::undoMgrView()
{
    return mapPane().undoMgrView();
}

UndoMgr& TrackMap::undoMgr()
{
    // We use const vs non-const to return a const or non-const model. This bugs clang-tidy,
    // so we mention 'this' so it doesn't suggest making this a static method.
    (void)this;
    return app().undoMgr();
}

const UndoMgr& TrackMap::undoMgr() const
{
    // We use const vs non-const to return a const or non-const model. This bugs clang-tidy,
    // so we mention 'this' so it doesn't suggest making this a static method.
    (void)this;
    return app().undoMgr();
}

void TrackMap::mapThemeSelected(const QString& themeName)
{
    const UndoMgr::ScopedUndo undoSet(undoMgr(), tr("Set Map Theme"));
    undoMgr().add(new UndoMapTheme(*this, mapThemeId(), themeName));

    Marble::MarbleWidget::setMapThemeId(themeName);
}

void TrackMap::customPaint(Marble::GeoPainter* painter)
{
    if (painter == nullptr)
        return;

    drawTrackLines(painter);
    drawTrackWaypoints(painter);
    drawTrackPts(painter);
    drawSelectionBox(painter);
}

void TrackMap::newViewContext(Marble::ViewContext newViewContext)
{
    if (newViewContext == Marble::Still) {
        update();  // map doesn't always do this when it should.

        // small interval for stillness
        m_mapIdleTimer.start(int(cfgData().mapUndoStill * 1000));
    }
}

void TrackMap::hqUpdate()
{
    setViewContext(Marble::Still);
    update();  // force refresh
}

void TrackMap::lqUpdate()
{
    setViewContext(Marble::Animation);
    update();  // force refresh
}

void TrackMap::resizeEvent(QResizeEvent* event)
{
    using namespace std::chrono_literals;

    setViewContext(Marble::Animation);
    m_hqUpdateTimer.start(1500ms);

    Marble::MarbleWidget::resizeEvent(event);
}

void TrackMap::drawModel(Marble::GeoPainter* painter, void (TrackMap::*drawFn)(Marble::GeoPainter*, const QModelIndex&),
                         const QAbstractItemModel& model, const QPersistentModelIndex& currentIdx, bool drawWhileMoving)
{
    painter->setCompositionMode(QPainter::CompositionMode_SourceOver);

    const bool still   = (viewContext() == Marble::Still);

    if (!still && !drawWhileMoving)
        return;

    if (still || cfgData().mapMoveMode == CfgData::MapMoveMode::AllTracks) {
        Util::Recurse(model, [this, &drawFn, &currentIdx, painter](const QModelIndex& idx) {
            if (idx != currentIdx) // we'll draw current track last
               (this->*drawFn)(painter, idx);
            return true;
        });
    }

    // Paint current track last so it isn't buried.
    if (still ||
        cfgData().mapMoveMode == CfgData::MapMoveMode::ActiveTrack ||
        cfgData().mapMoveMode == CfgData::MapMoveMode::AllTracks)
        if (currentIdx.isValid())
            (this->*drawFn)(painter, currentIdx);
}

// Draw track points for current track
void TrackMap::drawTrackLines(Marble::GeoPainter* painter)
{
    drawModel(painter, &TrackMap::drawSingleTrack, trackModel(), m_currentTrackIdx, true);
}

// Draw track points for current track
void TrackMap::drawTrackWaypoints(Marble::GeoPainter* painter)
{
    drawModel(painter, &TrackMap::drawSingleWaypoint, waypointModel(), m_currentWaypointIdx,
              cfgData().mapMoveWaypoints);
}

// Helper to draw an entire track segment once in a given color and pen width.
inline void TrackMap::drawTrackSeg(Marble::GeoPainter* painter, const QColor& color, float trackWidth,
                                   const TrackSegLines& trackSegLines)
{
    QPen trackPen(color);
    trackPen.setWidthF(qreal(trackWidth));

    painter->setPen(trackPen);

    for (const auto& geoLine : trackSegLines)
        painter->drawPolyline(geoLine);
}

void TrackMap::drawSingleTrack(Marble::GeoPainter* painter, const QModelIndex& idx)
{
    static const float movingTrackWidth = 1.0f;

    const TrackSegLines& trackSegLines = trackModel().trackLines(*this, idx);

    if (!trackModel().isVisible(idx))
        return;

    // Magic 0.2 is experimentally derived from zooming the map.
    static const qreal zoomFactor = 0.2;
    const float zoomInterp = std::clamp(float(distance() * zoomFactor), 0.0f, 1.0f);

    const auto trackColor = get<QColor>(TrackModel::Color, idx, Qt::BackgroundRole);

    const bool current = isCurrentTrack(idx);
    const bool still   = (viewContext() == Marble::Still);

    const int alpha = current ?
                int(Math::mix(float(cfgData().currentTrackAlphaC),
                              float(cfgData().currentTrackAlphaF), zoomInterp)) :
                int(Math::mix(float(cfgData().defaultTrackAlphaC),
                              float(cfgData().defaultTrackAlphaF), zoomInterp));

    const float currentWidthForZoom    = Math::mix(cfgData().currentTrackWidthC,
                                                   cfgData().currentTrackWidthF, zoomInterp);

    const float defaultWidthForZoom    = Math::mix(cfgData().defaultTrackWidthC,
                                                   cfgData().defaultTrackWidthF, zoomInterp);

    const float defaultWidthForContext = still ? defaultWidthForZoom : movingTrackWidth;

    static const float minOutlineWidth = 2.5;

    const float outlineWidth           = current ? cfgData().currentTrackWidthO :
                                         (defaultWidthForContext >= minOutlineWidth) ? cfgData().defaultTrackWidthO : 0.0;

    const float trackWidthForDraw      = current ? currentWidthForZoom : defaultWidthForContext;

    const QColor trackColorDraw   = QColor(trackColor.red(), trackColor.green(), trackColor.blue(), alpha);
    const QColor outlineColorDraw = QColor(cfgData().outlineTrackColor.red(),
                                           cfgData().outlineTrackColor.green(),
                                           cfgData().outlineTrackColor.blue(), alpha);

    // Draw outline first, and main color on top.
    if (outlineWidth > 0.1f)
        drawTrackSeg(painter, outlineColorDraw, outlineWidth + trackWidthForDraw, trackSegLines);

    // Now draw main track in its color.
    drawTrackSeg(painter, trackColorDraw, trackWidthForDraw, trackSegLines);
}

auto TrackMap::waypointIcon(const QModelIndex& idx)
{
    QString iconName = waypointModel().data(WaypointModel::Icon, idx, Util::IconNameRole).toString();
    int     iconSize = cfgData().waypointIconSize;

    // If no icon from the waypoint itself, use the default.
    if (iconName.isNull()) {
        iconName = cfgData().waypointDefaultIcon;
        iconSize = cfgData().waypointDefaultIconSize;
    }

    // return existing icon if we've made one.
    if (const auto iconIt = m_waypointIcons.find(iconName); iconIt != m_waypointIcons.end())
        return iconIt;

    if (!iconName.isEmpty()) {
        const QPixmap resizedIcon = pixmapFromIcon(QIcon(iconName), iconSize);
        return m_waypointIcons.insert(iconName, resizedIcon);
    }

    return m_waypointIcons.end();
}

void TrackMap::drawSingleWaypoint(Marble::GeoPainter* painter, const QModelIndex& idx)
{
    if (!waypointModel().isVisible(idx))
        return;

    const Marble::GeoDataCoordinates coords = waypointModel().coords(idx);

    if (const auto iconIt = waypointIcon(idx); iconIt != m_waypointIcons.end()) {
        painter->drawPixmap(coords, *iconIt);

        if (idx == m_currentWaypointIdx) {
            painter->setPen(QPen(QColor(180, 0, 0), 2));
            painter->drawRect(coords, iconIt->width()+2, iconIt->height()+2);
        }
    }
}

inline void TrackMap::drawSinglePt(Marble::GeoPainter* painter, const PointItem& pt,
                                   qreal screenPointThreshold, const QPixmap& pixmap, bool draw,
                                   bool force)
{
    qreal screenX, screenY;

    if (screenCoordinates(double(pt.lon()), double(pt.lat()), screenX, screenY)) {
        if (!force && !std::isnan(m_prevX) && Math::distSqr(m_prevX - screenX, m_prevY - screenY) < screenPointThreshold)
            return;

        const Marble::GeoDataCoordinates coords = pt.as<Marble::GeoDataCoordinates>();
        if (draw)
            painter->drawPixmap(coords, pixmap);
        m_prevX = screenX;
        m_prevY = screenY;
    }
}

// Draw track points for current track
void TrackMap::drawTrackPts(Marble::GeoPainter* painter)
{
    static const qreal NaN  = std::numeric_limits<qreal>::quiet_NaN();

    const bool still = (viewContext() == Marble::Still);
    const bool editing = (mainWindow().mapMode() == MainWindow::MapMode::Select);
    const bool selectingPoints = m_selectingPoints;

    m_selectingPoints = false;

    if (!still && !editing && !selectingPoints) {
        // Avoid drawing under movement if not asked to draw active track
        if (!(cfgData().mapMoveMode == CfgData::MapMoveMode::AllTracks ||
              cfgData().mapMoveMode == CfgData::MapMoveMode::ActiveTrack))
            return;

        // Avoid drawing under movement if asked not to draw points
        if (!cfgData().mapMovePoints)
            return;
    }

    const PointModel* geoPoints = trackModel().geoPoints(m_currentTrackIdx);

    if (geoPoints == nullptr || !m_currentTrackIdx.isValid() || !trackModel().isVisible(m_currentTrackIdx))
        return;

    const qreal screenPointThreshold = Math::sqr(cfgData().defaultPointIconProx);

    const int currentRow = m_currentPointIdx.row(); // for performance

    const PointItem* currentPointItem = nullptr;
    bool  hasSelection = false;

    // To avoid overdrawing the active point with other ones, we render the points in three layers:
    //   Bottom: normal points
    //   Middle: selected points
    //   Top:    current point

    // First, normal points
    for (const auto& trkseg : *geoPoints) {
        m_prevX = m_prevY = NaN;  // to avoid drawing points too close together in screenspace.
        int ptNum = 0;
        for (const auto& pt : trkseg) {
            // Attempt to short circuit calling geoPoints.is() if possible, because this is part of the
            // render loop and performance matters.
            const bool isCurrent  = (ptNum++ == currentRow) && geoPoints->is(*this, m_currentPointIdx, pt);
            const bool isSelected = pt.test(PointItem::Flags::Select);
            hasSelection = hasSelection || isSelected;

            drawSinglePt(painter, pt, screenPointThreshold, m_defaultPointIcon, !isCurrent && !isSelected, false);

            if (isCurrent)
                currentPointItem = &pt;
        }
    }

    // Second, selected points, only if there are any.
    if (hasSelection) {
        for (const auto& trkseg : *geoPoints) {
            m_prevX = m_prevY = NaN;  // to avoid drawing points too close together in screenspace.
            for (const auto& pt : trkseg) {
                // The pointer comparison is to avoid calling geoPoints.is() on every single item, because
                // this happens in the draw loop and performance matters here.
                const bool isCurrent  = (&pt == currentPointItem);
                const bool isSelected = pt.test(PointItem::Flags::Select);

                drawSinglePt(painter, pt, screenPointThreshold, m_selectedPointIcon, !isCurrent && isSelected, false);
            }
        }
    }

    // Third, GPSD points
    if (hasGpsdLivePoints()) {
        for (const auto& pt : m_gpsdPoints)
            drawSinglePt(painter, pt, screenPointThreshold, m_gpsdLivePointIcon, true, true);
    }

    // Last, draw current point on top of any other points, so it's easily visible.
    if (currentPointItem != nullptr)
        drawSinglePt(painter, *currentPointItem, screenPointThreshold, m_currentPointIcon, true, true);
}

void TrackMap::mousePressEvent(QMouseEvent* event)
{
    bool accepted = false;

    m_dragThresholdMet = false;
    m_dragInitPoint = event->pos();

    switch (mainWindow().mapMode()) {
    case MainWindow::MapMode::Move:    accepted = mousePressEventMove(event);   break;
    case MainWindow::MapMode::Add:     accepted = mousePressEventAdd(event);    break;
    case MainWindow::MapMode::Select:  accepted = mousePressEventSelect(event); break;
    }

    if (accepted)
        event->accept();
    else
        Marble::MarbleWidget::mousePressEvent(event);
}

void TrackMap::mouseReleaseEvent(QMouseEvent* event)
{
    bool accepted = false;
    switch (mainWindow().mapMode()) {
    case MainWindow::MapMode::Move:    accepted = mouseReleaseEventMove(event);   break;
    case MainWindow::MapMode::Add:     accepted = mouseReleaseEventAdd(event);    break;
    case MainWindow::MapMode::Select:  accepted = mouseReleaseEventSelect(event); break;
    }

    if (accepted)
        event->accept();
    else
        Marble::MarbleWidget::mouseReleaseEvent(event);
}


void TrackMap::mouseMoveEvent(QMouseEvent* event)
{
    // Signal in coordinate space, which the Marble map doesn't provide.
    emit mouseMoveGeoCoords(widgetGeoCoords(m_currentMouseCoordinates = event->pos()));

    // Start dragging after exceeding threshold
    if ((m_dragInitPoint - event->pos()).manhattanLength() > appBase().startDragDistance())
        m_dragThresholdMet = true;

    bool accepted = false;
    switch (mainWindow().mapMode()) {
    case MainWindow::MapMode::Move:    accepted = mouseMoveEventMove(event);   break;
    case MainWindow::MapMode::Add:     accepted = mouseMoveEventAdd(event);    break;
    case MainWindow::MapMode::Select:  accepted = mouseMoveEventSelect(event); break;
    }

    if (accepted)
        event->accept();
    else
        Marble::MarbleWidget::mouseMoveEvent(event);
}

bool TrackMap::updatePointAtMouse(QMouseEvent* event, bool update)
{
    if (!update)
        return false;

    // Ensure it belongs to the current model
    PointModel* geoPoints = trackModel().geoPoints(m_currentTrackIdx);
    if (geoPoints == nullptr || m_currentPointIdx.model() != geoPoints)
        return false;

    const auto [lonDeg, latDeg] = mouseGeoCoordinates(event, Marble::GeoDataCoordinates::Degree);

    geoPoints->setData(PointModel::Lon, m_currentPointIdx, PointItem::Lon_t::base_type(lonDeg), Util::RawDataRole);
    geoPoints->setData(PointModel::Lat, m_currentPointIdx, PointItem::Lat_t::base_type(latDeg), Util::RawDataRole);

    return true;
}

inline std::tuple<Lon_t, Lat_t> TrackMap::mouseGeoCoordinates(QMouseEvent* event, Marble::GeoDataCoordinates::Unit unit)
{
    qreal lon, lat;
    geoCoordinates(event->pos().x(), event->pos().y(), lon, lat, unit);

    return std::make_tuple(Lon_t(lon), Lat_t(lat));
}

bool TrackMap::mousePressEventMove(QMouseEvent* event)
{
    if (event->modifiers() == Qt::ControlModifier) {
        startSelectRegion(event->pos());
        return true;
    }

    return false;
}

bool TrackMap::mouseReleaseEventMove(QMouseEvent* event)
{
    if (regionSelectionStarted()) {
        endSelectRegion(event->pos());
        return true;
    }

    return false;
}

bool TrackMap::mouseMoveEventMove(QMouseEvent* event)
{
    if (regionSelectionStarted()) {
        m_selectionEndCoordinate = event->pos();
        return true;
    }

    return false;
}

bool TrackMap::mousePressEventAdd(QMouseEvent*)
{
    // Preserve this as a non-static method for stylistic reasons, without clang-tidy complaints.
    (void)this;
    return true;
}

void TrackMap::selectPoint(const QModelIndex& idx, bool add)
{
    auto* pointPane = mainWindow().findPane<PointPane>();
    if (pointPane == nullptr)
        return;

    const QItemSelectionModel::SelectionFlags selectFlags =
            QItemSelectionModel::Rows | QItemSelectionModel::SelectCurrent |
            (add ? QItemSelectionModel::NoUpdate : QItemSelectionModel::Clear);

    m_currentPointIdx = idx;
    pointPane->select(idx, selectFlags);
}

bool TrackMap::mouseReleaseEventAdd(QMouseEvent* event)
{
    if (m_dragThresholdMet) // if we dragged the map, don't add the point on release
        return mouseReleaseEventMove(event);

    PointModel* geoPoints = trackModel().geoPoints(m_currentTrackIdx);
    if (geoPoints == nullptr || (m_currentPointIdx.isValid() && m_currentPointIdx.model() != geoPoints))
        return false;

    const UndoMgr::ScopedUndo undoSet(app().undoMgr(), tr("Interactive add point"));

    // If the track has no segments, add one, to add our point to
    if (geoPoints->isEmpty())
        geoPoints->insertRows(0, 1);

    QModelIndex parent;  // Parent of new point
    int beforeRow;       // row to add before

    // Figure out where to insert the point
    if (m_currentPointIdx.isValid()) {
        if (geoPoints->isSegment(m_currentPointIdx)) {
            parent = m_currentPointIdx;                  // segment level: use segment as parent
            beforeRow = geoPoints->rowCount(parent);
        } else {
            parent = m_currentPointIdx.parent();         // point: use segment as parent
            beforeRow = m_currentPointIdx.row() + 1;
        }
    } else {
        parent = geoPoints->index(geoPoints->rowCount() - 1, 0);  // last extant segment
        beforeRow = geoPoints->rowCount(parent);
    }

    const auto [lonDeg, latDeg] = mouseGeoCoordinates(event, Marble::GeoDataCoordinates::Degree);

    const QModelIndex newPointIdx = geoPoints->addInterpolatedPoint(latDeg, lonDeg, parent, beforeRow);
    selectPoint(newPointIdx, false);

    return true;
}

bool TrackMap::mouseMoveEventAdd(QMouseEvent* event)
{
    if (m_dragThresholdMet) // if we dragged the map, act like move mode
        return mouseMoveEventMove(event);

    return false;
}

bool TrackMap::mousePressEventSelect(QMouseEvent* event)
{
    // Select closest point in current track
    PointModel* geoPoints = trackModel().geoPoints(m_currentTrackIdx);
    if (geoPoints == nullptr)
        return false;

    const auto [lonRad, latRad] = mouseGeoCoordinates(event, Marble::GeoDataCoordinates::Radian);

    QModelIndex closest;
    qreal minDist = std::numeric_limits<qreal>::max();

    // clicks must lie within this radius squared to matter.
    static const qreal maxClickDistSqr = 25*25; // TODO: do something besides just making this up.

    qreal screenX, screenY;
    for (const auto& trkseg : *geoPoints) {
        for (const auto& pt : trkseg) {
            if (screenCoordinates(double(pt.lon()), double(pt.lat()), screenX, screenY)) {
                // Only accept if it lies within a certain radius in screen space from the click position
                if (Math::distSqr(screenX - qreal(event->pos().x()), screenY - qreal(event->pos().y())) < maxClickDistSqr) {
                    if (const qreal dist = pt.greatCircleDist(latRad, lonRad); dist <= minDist) {
                        closest = geoPoints->modelIndexFor(trkseg, pt);
                        minDist = dist;
                    }
                }
            }
        }
    }

    if (!closest.isValid())  // if no close point, try to act like move mode
      return mousePressEventMove(event);

    // Disable the default input handler. We'd rather supply our own, but see the comment in MainWindow::setMapMode.
    setInputEnabled(false);

    const bool addPoint = event->modifiers() == Qt::ControlModifier ||
                          event->modifiers() == Qt::ShiftModifier;

    selectPoint(closest, addPoint);

    geoPoints->incDontTrack(); // don't track undos until we finish dragging
    m_dragging         = true;
    m_dragInitLon      = geoPoints->getItem(closest)->lon();
    m_dragInitLat      = geoPoints->getItem(closest)->lat();

    return true;
}

bool TrackMap::mouseReleaseEventSelect(QMouseEvent* event)
{
    if (!m_dragging) // if no close point, try to act like move mode
        return mouseReleaseEventMove(event);

    if (PointModel* geoPoints = trackModel().geoPoints(m_currentTrackIdx); geoPoints != nullptr && m_dragging) {
        // Do this before resetting the dontTrackCount, so that we create a single, proper undo from the original position
        if (m_dragThresholdMet) {
            geoPoints->setData(PointModel::Lon, m_currentPointIdx, PointItem::Lon_t::base_type(m_dragInitLon), Util::RawDataRole);
            geoPoints->setData(PointModel::Lat, m_currentPointIdx, PointItem::Lat_t::base_type(m_dragInitLat), Util::RawDataRole);
        }

        geoPoints->decDontTrack();
    }

    // Do this after resetting the undo dontTrack count, so we create a single undo after the move
    const bool update = m_dragging && m_dragThresholdMet;
    const UndoMgr::ScopedUndo undoSet(app().undoMgr(), tr("Interactive drag point"), update);
    updatePointAtMouse(event, update);

    setInputEnabled(true);
    m_dragThresholdMet = m_dragging = false;
    deferredUpdate();
    return true;
}

bool TrackMap::mouseMoveEventSelect(QMouseEvent* event)
{
    if (!m_dragging)   // if no close point, try to act like move mode
        return mouseMoveEventMove(event);

    if (!updatePointAtMouse(event, m_dragging && m_dragThresholdMet))
        return false;

    update();
    return true;
}

Marble::GeoDataCoordinates TrackMap::widgetGeoCoords(const QPoint& point) const
{
    qreal lon, lat;
    if (!geoCoordinates(point.x(), point.y(), lon, lat, Marble::GeoDataCoordinates::Radian))
        return { };

    return { lon, lat, 0.0, Marble::GeoDataCoordinates::Radian };
}

void TrackMap::drawSelectionBox(Marble::GeoPainter* painter)
{
    if (!regionSelectionInProgress())
        return;

    const QRect screenRect(m_selectionStartCoordinate, m_selectionEndCoordinate);

    const Marble::GeoDataCoordinates centerCoord = widgetGeoCoords(screenRect.center());
    if (!centerCoord.isValid())
        return;

    QPen boxPen(QColor(QRgb(0xff008040)));
    boxPen.setWidth(2);

    painter->setPen(boxPen);
    painter->setBrush(QBrush());

    painter->drawRect(centerCoord, screenRect.width(), screenRect.height(), false);
}

void TrackMap::startSelectRegion(const QPoint& point)
{
    m_selectionStartCoordinate = point;
    m_selectionEndCoordinate = QPoint();
}

void TrackMap::endSelectRegion(const QPoint& point)
{
    const Marble::GeoDataCoordinates start = widgetGeoCoords(m_selectionStartCoordinate);
    const Marble::GeoDataCoordinates end = widgetGeoCoords(point);

    const Marble::GeoDataLatLonBox region(std::max(start.latitude(), end.latitude()),
                                          std::min(start.latitude(), end.latitude()),
                                          std::max(start.longitude(), end.longitude()),
                                          std::min(start.longitude(), end.longitude()),
                                          Marble::GeoDataCoordinates::Radian);

    m_selectionStartCoordinate = QPoint();  // reset to no drag selection in progress.
    m_selectionEndCoordinate = QPoint();

    mainWindow().showBoxSelectionDialog(region);
}

void TrackMap::deferredUpdate(int mSec)
{
    // Low quality updates
    using namespace std::chrono_literals;
    m_lqUpdateTimer.start(5ms);

    // trigger delayed high quality refresh
    m_hqUpdateTimer.start(mSec);
}

void TrackMap::setViewContext(Marble::ViewContext vc)
{
    // don't use high quality mode if drawing GPS live points
    const Marble::ViewContext actualVc = hasGpsdLivePoints() ? Marble::Animation : vc;

    if (viewContext() != actualVc)
        Marble::MarbleWidget::setViewContext(actualVc);
}

void TrackMap::currentTrackChanged(const QModelIndex& idx)
{
    // Disconnect signals from prior current track
    if (m_currentTrackIdx.isValid())
        if (PointModel* pm = trackModel().geoPoints(m_currentTrackIdx); pm != nullptr)
            disconnect(pm, nullptr, this, nullptr);

    m_currentTrackIdx = trackModel().rowSibling(0, Util::MapDown(idx));

    // Update map on data changes
    if (m_currentTrackIdx.isValid()) {
        if (PointModel* pm = trackModel().geoPoints(m_currentTrackIdx); pm != nullptr) {
            connect(pm, &PointModel::dataChanged, this, &TrackMap::pointDataChanged, Qt::UniqueConnection);
            connect(pm, &PointModel::modelReset, this, &TrackMap::pointModelReset, Qt::UniqueConnection);
        }
    }

    deferredUpdate();
}

void TrackMap::currentWaypointChanged(const QModelIndex& idx)
{
    m_currentWaypointIdx = waypointModel().rowSibling(0, Util::MapDown(idx));
    deferredUpdate();
}

void TrackMap::visibleTracksChanged()
{
    deferredUpdate();
}

void TrackMap::visibleWaypointsChanged()
{
    deferredUpdate();
}

void TrackMap::currentPointChanged(const QModelIndex& idx)
{
    m_selectingPoints = true;
    m_currentPointIdx = idx;
    deferredUpdate();
}

void TrackMap::pointDataChanged(const QModelIndex&)
{
    deferredUpdate();
}

void TrackMap::pointModelReset()
{
    deferredUpdate();
}

void TrackMap::selectedPointsChanged()
{
    m_selectingPoints = true;
    deferredUpdate();
}

// TODO: At some point, the regionSelected signal from MarbleWidget changed
// between a list of doubles, and a GeoDataLatLonBox.  This should change
// accordingly (the connect() should fail to compile when that happens).
void TrackMap::processRegionSelected(const QList<double>& coords)
{
    const Marble::GeoDataLatLonBox region(coords[1], coords[3], coords[2], coords[0],
            Marble::GeoDataCoordinates::Degree);

    mainWindow().showBoxSelectionDialog(region);
}
