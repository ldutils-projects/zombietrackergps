/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GPSDEVICEPANE_H
#define GPSDEVICEPANE_H

#include "datacolumnpane.h"

namespace Ui {
class GpsDevicePane;
} // namespace Ui

class MainWindow;
class QModelIndex;
class QTreeView;
class GeoLoadParams;

class GpsDevicePane : public DataColumnPane, public NamedItem
{
    Q_OBJECT

public:
    explicit GpsDevicePane(MainWindow&, bool contextMenus = true, QWidget* parent = nullptr);
    ~GpsDevicePane() override;

    void import();  // interactive import (prompt for parameters)
    void import(const GeoLoadParams&);  // noninteractive import

    QTreeView* treeView() const;

private slots:
    void on_GpsDevicePane_toggled(bool checked) { paneToggled(checked); }
    void showContextMenu(const QPoint&);
    void on_action_Import_Data_triggered();
    void on_action_Refresh_triggered();
    void resizeOnChange();
    void showEvent(QShowEvent*) override;

private:
    void setupActionIcons();
    void setupContextMenus();
    void setupSignals();
    const DefColumns& defColumnView() const override;

    bool hasAction(MainAction) const override { return false; }

    // Obtain model associated with pane
    TreeModel* model() override;
    const TreeModel* model() const override;

    Ui::GpsDevicePane* ui;
    QPoint             menuPos;
};

#endif // GPSDEVICEPANE_H
