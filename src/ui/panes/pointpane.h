/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef POINTPANE_H
#define POINTPANE_H

#include <QPersistentModelIndex>
#include <QTimer>

#include <src/ui/widgets/doublespindelegate.h>
#include <src/ui/widgets/spindelegate.h>
#include <src/ui/widgets/datetimedelegate.h>
#include <src/ui/widgets/texteditordelegate.h>
#include <src/ui/widgets/lineeditdelegate.h>

#include "src/core/selectionsummary.h"
#include "datacolumnpane.h"

namespace Ui {
class PointPane;
} // namespace Ui

class MainWindow;
class QModelIndex;
class PointModel;
class TrackModel;

class PointPane final :
        public DataColumnPane,
        public NamedItem
{
    Q_OBJECT

public:
    explicit PointPane(MainWindow&, QWidget *parent = nullptr);
    ~PointPane() override;

    void newConfig() override;

    void zoomToSelection() override;
    void deleteSelection() override;

    // Select a range of indices
    void selectRange(const QModelIndex&, const QModelIndex&,
                     QItemSelectionModel::SelectionFlags =
                         QItemSelectionModel::Rows | QItemSelectionModel::Select | QItemSelectionModel::Clear);

    // Put on stack to use instant updates on current track changes instead of deferred updates
    class InstantUpdate {
    public:
        InstantUpdate(PointPane* p) : p(p) { if (p != nullptr) ++p->m_instantCount; }
        ~InstantUpdate() { if (p != nullptr) --p->m_instantCount; }
    private:
        PointPane* p;
    };

protected:
    // *** begin Settings API
    using DataColumnPane::save;
    void load(QSettings&) override;
    // *** end Settings API

private slots:
    void on_PointPane_toggled(bool checked) { paneToggled(checked); }
    void doubleClicked(const QModelIndex&);
    void showContextMenu(const QPoint&);
    void currentTrackChanged(const QModelIndex&);
    void deferredUpdate();
    void filterTextChanged(const QString&) override;
    void processSelectionChanged(const QItemSelection&, const QItemSelection&);
    void processRowsAboutToBeRemoved(const QModelIndex&, int first, int last);
    void processModelAboutToBeReset();
    void processSelectedPointsChanged();
    void on_action_Split_Segments_triggered();
    void on_action_Create_Waypoint_triggered();

private:
    friend class InstantUpdate;
    friend class TestZtgps; // test hook

    void focusIn() override;

    PointModel* currentPoints();
    const PointModel* currentPoints() const;

    void setupActionIcons();
    void setupContextMenus();
    void setupSignals();
    void setupTimers();
    void setupDelegates();
    void gotoSelection(const QModelIndexList&) const;
    bool hasAction(MainAction) const override;
    void unsetIndex(QPersistentModelIndex&, const QModelIndex&, int first, int last);
    const DefColumns& defColumnView() const override;

    // Candidate name for data merging
    std::tuple<QString, bool> mergedName(const QModelIndexList&, const QString& undoName) const override;

    // Obtain model associated with pane
    TreeModel* model() override;
    const TreeModel* model() const override;

    Ui::PointPane*         ui;
    QPersistentModelIndex  m_currentTrackIdx;   // index of track whose points we should display
    QPersistentModelIndex  m_nextTrackIdx;      // for deferred updating
    QTimer                 m_updateTimer;       // for deferred update with track change

    DateTimeDelegate       m_dateTimeDelegate;  // for point editing...
    DoubleSpinDelegate     m_latDelegate;       // ...
    DoubleSpinDelegate     m_lonDelegate;       // ...
    DoubleSpinDelegate     m_eleDelegate;       // ...
    DoubleSpinDelegate     m_tempDelegate;      // ...
    DoubleSpinDelegate     m_speedDelegate;     // ...
    DoubleSpinDelegate     m_depthDelegate;     // ...
    SpinDelegate           m_hrDelegate;        // ...
    SpinDelegate           m_cadDelegate;       // ...
    LineEditDelegate       m_nameDelegate;      // ...
    TextEditorDelegate     m_commentDelegate;   // ...
    TextEditorDelegate     m_descDelegate;      // ...
    LineEditDelegate       m_symbolDelegate;    // ...
    LineEditDelegate       m_typeDelegate;      // ...

    int                    m_instantCount;      // instant update counter
    static PointModel*     m_empty;             // empty model, for when there's no current track

    // These are to avoid processing the entire selection list each time it changes.
    SelectionSummary       m_selectionSummary;
};

#endif // POINTPANE_H
