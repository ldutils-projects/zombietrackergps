/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <chrono>

#include <QItemSelection>

#include <src/util/util.h>
#include <src/util/icons.h>
#include <src/util/ui.h>
#include <src/ui/panes/datacolumnpanebase.inl.h>

#include "src/ui/windows/mainwindow.h"
#include "src/ui/panes/mappane.h"
#include "src/core/app.h"
#include "src/core/pointmodel.h"
#include "src/core/trackmodel.h"

#include "pointpane.h"
#include "ui_pointpane.h"

PointModel* PointPane::m_empty = nullptr;

PointPane::PointPane(MainWindow& mainWindow, QWidget *parent) :
    DataColumnPane(mainWindow, PaneClass::Points, parent),
    NamedItem(PointModel::getItemNameStatic()),
    ui(new Ui::PointPane),
    m_dateTimeDelegate(this),
    m_latDelegate(this, -90.0, 90.0, 12, 1),
    m_lonDelegate(this, -180, 179.99999999999, 12, 1),
    m_eleDelegate(this, -10000, 360000, 2, 1),
    m_tempDelegate(this, -100, 400, 2, 1),
    m_speedDelegate(this, 0, 100000, 2, 1),
    m_depthDelegate(this, -360000, 360000, 2, 1),
    m_hrDelegate(this, 0, 255),
    m_cadDelegate(this, 0, 255),
    m_nameDelegate(this),
    m_commentDelegate(this),
    m_descDelegate(this),
    m_symbolDelegate(this),
    m_typeDelegate(this),
    m_instantCount(0)
{
    ui->setupUi(this);

    if (m_empty == nullptr)
        m_empty = new PointModel(nullptr);

    setupView(ui->pointView, m_empty);
    setWidgets<PointModel>(defColumnView(),
                           ui->filterPoints, nullptr, ui->showColumns, ui->filterCtrl, ui->filterIsValid);

    // Don't display top (segment) level in flattened view.
    setFlattenPredicate([](const QAbstractItemModel*, const QModelIndex& idx) {
        return idx.parent().isValid();
    });

    setupActionIcons();
    setupContextMenus();
    setupSignals();
    setupTimers();
    setupDelegates();

    Util::SetupWhatsThis(this);
}

PointPane::~PointPane()
{
    deleteUI(ui);
}

void PointPane::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Split_Segments, "split");
    Icons::defaultIcon(ui->action_Create_Waypoint, "mark-location-symbolic");
}

void PointPane::setupContextMenus()
{
    paneMenu.addAction(mainWindow().getMainAction(MainAction::ZoomToSelection));
    paneMenu.addAction(mainWindow().getMainAction(MainAction::DeleteSelection));
    paneMenu.addAction(ui->action_Split_Segments);
    paneMenu.addAction(mainWindow().getMainAction(MainAction::MergeSelection));
    paneMenu.addAction(mainWindow().getMainAction(MainAction::SimplifySelection));
    paneMenu.addSeparator();
    paneMenu.addAction(mainWindow().getMainAction(MainAction::UnsetSpeed));
    paneMenu.addSeparator();
    paneMenu.addAction(ui->action_Create_Waypoint);

    setupActionContextMenu(paneMenu);
    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(this, &PointPane::customContextMenuRequested, this, &PointPane::showContextMenu);
}

void PointPane::setupSignals()
{
    DataColumnPane::setupSignals();

    // Notify ourselves and the filter about current container changes
    connect(&mainWindow(), &MainWindow::currentTrackChanged, this, &PointPane::currentTrackChanged);

    // Selection changes
    connect(selectionModel(), &QItemSelectionModel::selectionChanged, this, &PointPane::processSelectionChanged);

    // Row deletion from model
    connect(&app().trackModel(), &TrackModel::rowsAboutToBeRemoved, this, &PointPane::processRowsAboutToBeRemoved);
    // Model reset
    connect(&app().trackModel(), &TrackModel::modelAboutToBeReset, this, &PointPane::processModelAboutToBeReset);

    // Double click to zoom-to
    connect(ui->pointView, &QTreeView::doubleClicked, this, &PointPane::doubleClicked);

    // Handle point selection change
    connect(&mainWindow(), &MainWindow::selectedPointsChanged, this, &PointPane::processSelectedPointsChanged);
}

void PointPane::setupTimers()
{
    m_updateTimer.setSingleShot(true);
    connect(&m_updateTimer, &QTimer::timeout, this, &PointPane::deferredUpdate);
}

void PointPane::setupDelegates()
{
    initDelegates({{ PointModel::Time,    &m_dateTimeDelegate },
                   { PointModel::Lat,     &m_latDelegate },
                   { PointModel::Lon,     &m_lonDelegate },
                   { PointModel::Ele,     &m_eleDelegate },
                   { PointModel::Temp,    &m_tempDelegate },
                   { PointModel::Speed,   &m_speedDelegate },
                   { PointModel::Depth,   &m_depthDelegate },
                   { PointModel::Hr,      &m_hrDelegate },
                   { PointModel::Cad,     &m_cadDelegate },
                   { PointModel::Name,    &m_nameDelegate },
                   { PointModel::Comment, &m_commentDelegate },
                   { PointModel::Desc,    &m_descDelegate },
                   { PointModel::Symbol,  &m_symbolDelegate },
                   { PointModel::Type,    &m_typeDelegate } });
}

TreeModel* PointPane::model()
{
    return currentPoints();
}

const TreeModel* PointPane::model() const
{
    return currentPoints();
}

const PointPane::DefColumns& PointPane::defColumnView() const
{
    static const DefColumns cols = {
        { PointModel::Index,    true  },
        { PointModel::Time,     false },
        { PointModel::Elapsed,  true  },
        { PointModel::Lon,      false },
        { PointModel::Lat,      false },
        { PointModel::Ele,      true  },
        { PointModel::Length,   false },
        { PointModel::Distance, true  },
        { PointModel::Vert,     false },
        { PointModel::Grade,    true  },
        { PointModel::Duration, false },
        { PointModel::Temp,     false },
        { PointModel::Depth,    false },
        { PointModel::Speed,    true  },
        { PointModel::Hr,       false },
        { PointModel::Cad,      false },
        { PointModel::Power,    false },
        { PointModel::Course,   false },
        { PointModel::Bearing,  false },
        { PointModel::Name,     false },
        { PointModel::Comment,  false },
        { PointModel::Desc,     false },
        { PointModel::Symbol,   false },
        { PointModel::Type,     false },
    };

    // Verify we added everything.  It's not critical, but better practice.
    for (ModelType md = PointModel::_First; md < PointModel::_Count; ++md)
        assert(cols.findData(md) >= 0);

    return cols;
}

std::tuple<QString, bool> PointPane::mergedName(const QModelIndexList&, const QString&) const
{
    return { QString(), true }; // no name needed for segment or point merging
}

void PointPane::newConfig()
{
    ui->pointView->expandAll();

    m_eleDelegate.setSuffix(QString(" ") + cfgData().unitsElevation.suffix(1000.0));
    m_speedDelegate.setSuffix(QString(" ") + cfgData().unitsSpeed.suffix(10.0));
    m_hrDelegate.setSuffix(QString(" ") + cfgData().unitsHr.suffix(10.0));
    m_cadDelegate.setSuffix(QString(" ") + cfgData().unitsCad.suffix(100.0));
    m_tempDelegate.setSuffix(QString(" ") + cfgData().unitsTemp.suffix(100.0));
    m_dateTimeDelegate.setFormat(cfgData().unitsPointDate.dateFormat());
}

void PointPane::filterTextChanged(const QString& regex)
{
    DataColumnPane::filterTextChanged(regex);

    if (PointModel* points = currentPoints(); points != nullptr) {
        m_selectionSummary.refresh(points, topFilter(), selectionModel());
        mainWindow().updateStatus(m_selectionSummary);
    }
}

// For performance reasons (selections are needed within the TrackMap render loop), we cache the results of
// the selection in the model.  This approach also lets us avoid rebuilding the entire selection set upon
// each change, and instead do it incrementally.
void PointPane::processSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected)
{
    PointModel* points = currentPoints();
    if (points == nullptr)
        return;

    const auto modSelection = [this, points](const QItemSelection& selection, bool newState) {
        // map through our filter stack
        for (const auto& sr : selection)
            for (int r = sr.top(); r <= sr.bottom(); ++r)
                points->select(*this, Util::MapDown(sr.model()->index(r, 0, sr.parent())), newState);
    };

    modSelection(selected,   true);
    modSelection(deselected, false);

    m_selectionSummary.update(*points, topFilter(), selectionModel(), selected, deselected);
    mainWindow().updateStatus(m_selectionSummary);
}

void PointPane::unsetIndex(QPersistentModelIndex& idx, const QModelIndex& parent, int first, int last)
{
    if (idx.parent() == parent && idx.row() >= first && idx.row() <= last) {
        idx = QModelIndex();
        setSourceModel(m_empty);
    }
}

void PointPane::processRowsAboutToBeRemoved(const QModelIndex& parent, int first, int last)
{
    if (!m_currentTrackIdx.isValid())
        return;

    unsetIndex(m_currentTrackIdx, parent, first, last);
    unsetIndex(m_nextTrackIdx, parent, first, last);
}

void PointPane::processModelAboutToBeReset()
{
    m_currentTrackIdx = QModelIndex();
    m_nextTrackIdx    = QModelIndex();
    setSourceModel(m_empty);
}

void PointPane::processSelectedPointsChanged()
{
    const bool hasItems     = PointPane::hasItems();
    const bool hasSelection = PointPane::hasSelection();
    const bool isTypeTrk    = app().trackModel().data(TrackModel::Type, m_currentTrackIdx, Util::RawDataRole).value<TrackType>() ==
                              TrackType::Trk;

    ui->action_Split_Segments->setEnabled(hasItems && hasSelection && isTypeTrk);
    ui->action_Create_Waypoint->setEnabled(hasSelection);
}

void PointPane::currentTrackChanged(const QModelIndex& current)
{
    using namespace std::chrono_literals;

    // Return if no row change.
    if (m_currentTrackIdx.model() == current.model() && m_currentTrackIdx.row() == current.row())
        return;

    m_nextTrackIdx = current.sibling(current.row(), 0);  // save index for deferredUPdate()

    if (appBase().testing() || m_instantCount > 0)
        deferredUpdate();         // Under test hook, don't defer the update
    else
        m_updateTimer.start(250ms); // If interactive, defer to avoid slowing down current track changes
}

void PointPane::deferredUpdate()
{
    if (PointModel* oldPoints = currentPoints(); oldPoints != nullptr)
        oldPoints->unselectAll(*this);

    m_currentTrackIdx = m_nextTrackIdx;
    m_nextTrackIdx    = QModelIndex();

    PointModel* newPoints = currentPoints();
    setSourceModel(newPoints != nullptr ? newPoints : m_empty);

    expandAll();
}

void PointPane::showContextMenu(const QPoint& pos)
{
    paneMenu.exec(mapToGlobal(pos));
}

void PointPane::focusIn()
{
    DataColumnPane::focusIn();

    if (PointModel* points = currentPoints(); points != nullptr) {
        m_selectionSummary.refresh(points, topFilter(), selectionModel());
        mainWindow().updateStatus(m_selectionSummary);
        points->select(selectionModel()->selectedRows());
    }
}

PointModel* PointPane::currentPoints()
{
    if (!m_currentTrackIdx.isValid())
        return nullptr;

    return app().trackModel().geoPoints(m_currentTrackIdx);
}

const PointModel* PointPane::currentPoints() const
{
    return const_cast<const PointModel*>(const_cast<PointPane*>(this)->currentPoints());
}

void PointPane::gotoSelection(const QModelIndexList& selection) const
{
    auto* mapPane = mainWindow().findPane<MapPane>();

    if (ui == nullptr || ui->pointView == nullptr || mapPane == nullptr || selection.isEmpty())
        return;

    if (const PointModel* points = currentPoints(); points != nullptr)
        mapPane->zoomTo(points->boundsBox(selection));
}

bool PointPane::hasAction(MainAction action) const
{
    switch (action)
    {
    case MainAction::ZoomToSelection:    [[fallthrough]];
    case MainAction::DeleteSelection:    [[fallthrough]];
    case MainAction::SimplifySelection:  [[fallthrough]];
    case MainAction::UnsetSpeed:         return true;
    case MainAction::MergeSelection:
        if (selectionModel() != nullptr) {
            if (const QModelIndexList selection = Util::MapDown(selectionModel()->selectedRows()); selection.count() > 1)
                return PointModel::allSegments(selection) || PointModel::sameSegment(selection);
        }
        return false;

    default:                             return false;
    }
}

void PointPane::doubleClicked(const QModelIndex& idx)
{
    // If it's an editable column edit, rather than view the selection
    if (TrackModel::mdIsEditable(idx.column()))
        return;

    gotoSelection(getSelections());
}

void PointPane::zoomToSelection()
{
    gotoSelection(getSelections());
}

void PointPane::deleteSelection()
{
    int segments = 0, points = 0;
    for (const auto& idx : getSelections()) {
        if (idx.parent().isValid())
            ++points;
        else
           ++segments;
    }

    if (PointModel* pm = currentPoints(); pm != nullptr)
        Util::RemoveRows(*pm, selectionModel(), &topFilter());

    selectionModel()->clearSelection();

    mainWindow().statusMessage(UiType::Success, tr("Deleted ") +
                               (segments > 0 ? QString::number(segments) + tr(" segments") : "") +
                               ((segments > 0 && points > 0) ? tr(" and ") : "") +
                               (points > 0 ? QString::number(points) + tr(" points") : "") + ".");

    // TODO: set current item to something that makes sense
}

void PointPane::selectRange(const QModelIndex& idx0, const QModelIndex& idx1,
                            QItemSelectionModel::SelectionFlags flags)
{
    const PointModel* points = currentPoints();
    if (points == nullptr)
        return;

    // Sort the indexes in case they are reversed
    const bool reverse = points->data(idx0.sibling(idx0.row(), PointModel::Index), Util::RawDataRole).toInt() >
                         points->data(idx1.sibling(idx1.row(), PointModel::Index), Util::RawDataRole).toInt();

    const QModelIndex first = reverse ? idx1 : idx0;
    const QModelIndex last  = reverse ? idx0 : idx1;

    assert(points->isSegment(first) == points->isSegment(last));

    if (bool(flags & QItemSelectionModel::Clear)) {
        selectionModel()->clearSelection();
        flags &= ~QItemSelectionModel::Clear; // don't redo clears below
    }

    // For performance, we must add everything to a QItemSelection and select it all at once
    QItemSelection selection;

    const auto mapUpSelect = [this, &selection](const QModelIndex& idx) {
        const QModelIndex proxyIdx = Util::MapUp(&topFilter(), idx);
        selection.select(proxyIdx, proxyIdx);
    };

    if (points->isSegment(first)) {
        // Segment level selection
        for (int segId = first.row(); segId <= last.row(); ++segId)
            mapUpSelect(points->index(segId, 0));
    } else {
        // Point level selection
        for (int segId = first.parent().row(); segId <= last.parent().row(); ++segId) {
            const QModelIndex segIdx = points->index(segId, 0);

            const int firstPtId = (segId == first.parent().row()) ? first.row() : 0;
            const int lastPtId = (segId == last.parent().row()) ? last.row() : points->rowCount(segIdx) - 1;

            for (int ptId = firstPtId; ptId <= lastPtId; ++ptId)
                mapUpSelect(points->index(ptId, 0, segIdx));
        }
    }

    selectionModel()->select(selection, flags);
}

void PointPane::load(QSettings& settings)
{
    DataColumnPane::load(settings);

    // New fields in version 26.
    if (cfgData().priorCfgDataVersion < 26) {
        for (ModelType mt : { PointModel::Name, PointModel::Comment, PointModel::Desc,
                              PointModel::Symbol, PointModel::Type })
            setColumnHidden(mt, !defColumnView().defaultShown(mt));
    }
}

void PointPane::on_action_Split_Segments_triggered()
{
    const UndoMgr::ScopedUndo undoSet(app().undoMgr(), tr("Split Segments"));

    if (PointModel* pm = currentPoints(); pm != nullptr)
        pm->splitSegments(selectionModel()->selectedRows());

    selectionModel()->clearSelection();
}

void PointPane::on_action_Create_Waypoint_triggered()
{
    const PointModel* points = currentPoints();
    if (points == nullptr)
        return;

    const QModelIndexList selections = getSelections();

    QVector<Marble::GeoDataCoordinates> coords;
    coords.reserve(selections.size());

    for (const auto& idx : selections)
        coords.append(points->as<Marble::GeoDataCoordinates>(idx));

    if (mainWindow().getNewWaypointDialog().exec(coords) == QDialog::Rejected)
        return;

    if (auto* mapPane = mainWindow().findPane<MapPane>(); mapPane != nullptr)
        mapPane->zoomTo(points->boundsBox(selections));
}
