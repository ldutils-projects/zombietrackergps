/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <numeric>
#include <chrono>

#include <QFontMetrics>
#include <QInputDialog>
#include <QScrollBar>
#include <QList>
#include <QToolTip>
#include <QCursor>
#include <QSignalBlocker>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QHorizontalBarSeries>
#include <QBarSet>
#include <QBarCategoryAxis>
#include <QValueAxis>

#include <src/util/variantcmp.h>
#include <src/util/util.h>
#include <src/util/ui.h>
#include <src/util/icons.h>
#include <src/util/units.h>
#include <src/core/modelmetadata.inl.h>

#include "src/ui/windows/mainwindow.h"
#include "src/core/trackmodel.h"
#include "src/core/app.h"

#include "trackpane.h"
#include "trackcmppane.h"
#include "ui_trackcmppane.h"

TrackCmpPane::TrackCmpPane(MainWindow& mainWindow, QWidget *parent) :
    BarChartBase(mainWindow, PaneClass::CmpChart, parent),
    ui(new Ui::TrackCmpPane),
    m_currentTimer(this),
    m_axisY(new QtCharts::QBarCategoryAxis()),
    m_axisX(new QtCharts::QValueAxis()),
    m_currentRow(-1),
    m_prevRow(-1)
{
    ui->setupUi(this);

    setupActionIcons();
    setPaneFilterBar(ui->filterCtrl, ui->graphData);
    setupDataSelector();
    setupTimers();
    setupChart();
    setupSignals();
    setupMenus();
    setupCompleter();
    Util::SetupWhatsThis(this);

    setLockToQuery(false);
}

TrackCmpPane::~TrackCmpPane()
{
    // We don't delete the axes, since the chart owns them (we just track a pointer).

    if (ui != nullptr)
        ui->graphScroll->takeWidget();  // We own the ChartView, the scroll doesn't.

    deleteUI(ui);
}

void TrackCmpPane::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Show_Axes,       "labplot-axis-vertical");
    Icons::defaultIcon(ui->action_Show_Legend,     "description");
    Icons::defaultIcon(ui->action_Animated,        "motion_path_animations");
    Icons::defaultIcon(ui->action_Set_Bar_Width,   "resizerow");
    Icons::defaultIcon(ui->action_Show_Bar_Values, "format-precision-more");
    Icons::defaultIcon(ui->action_Page_Up,         "arrow-up-double");
    Icons::defaultIcon(ui->action_Page_Down,       "arrow-down-double");
}

void TrackCmpPane::setupTimers()
{
    m_currentTimer.setSingleShot(true);
    connect(&m_currentTimer, &QTimer::timeout, this, &TrackCmpPane::highlightCurrent);
}

void TrackCmpPane::setupChart()
{
    BarChartBase::setupChart();

    if (m_chart == nullptr)
        return;

    m_barSeries = new QtCharts::QHorizontalBarSeries();

    m_chart->addSeries(m_barSeries);
    m_chart->legend()->setVisible(false);
    m_chart->setAnimationDuration(500);
    m_barSeries->setUseOpenGL(true); // not yet supported for bar charts, but someday?

    m_axisY->setLabelsColor(m_labelNormal);
    m_axisY->append("");
    m_axisY->setVisible(false);

    m_axisX->setLabelsColor(m_labelNormal);
    m_axisX->setGridLinePen(m_majorGridPen);
    m_axisX->setMinorGridLinePen(m_minorGridPen);

    m_barSeries->setLabelsPosition(QtCharts::QHorizontalBarSeries::LabelsInsideEnd);
    m_barSeries->setBarWidth(1.0);

    m_chart->addAxis(m_axisX, Qt::AlignTop);
    m_chart->addAxis(m_axisY, Qt::AlignLeft);
    m_barSeries->attachAxis(m_axisX);
    m_barSeries->attachAxis(m_axisY);

    ui->graphScroll->setWidget(&m_chartView);
    setFocusProxy(ui->graphScroll);

    setPlotColumn(0);  // default: must happen after setupDataSelector()
}

void TrackCmpPane::setupMenus()
{
    // We don't add pageup/dn actions, because the widget itself provides keyboard controls for that.
    // Our actions are only for the menu.
    for (auto* action : {
         ui->action_Show_Axes,
         ui->action_Show_Bar_Values })
    {
        action->setShortcutContext(Qt::WidgetWithChildrenShortcut);
        addAction(action);
    }

    paneMenu.addActions({ ui->action_Page_Up,
                          ui->action_Page_Down });
    paneMenu.addSeparator();

    paneMenu.addActions({ ui->action_Animated,
                          ui->action_Show_Bar_Values,
                          ui->action_Show_Axes,
                          ui->action_Set_Bar_Width });

    ui->action_Animated->setChecked(false);
    ui->action_Show_Bar_Values->setChecked(false);
    ui->action_Show_Axes->setChecked(true);

    setupActionContextMenu(paneMenu);
    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(&m_chartView, &ChartViewZoom::customContextMenuRequested, this, &TrackCmpPane::showContextMenu);
}

void TrackCmpPane::setupSignals()
{
    BarChartBase::setupSignals();

    connect(&mainWindow(), &MainWindow::currentTrackChanged, this, &TrackCmpPane::currentTrackChanged);

    connect(m_barSeries, &QtCharts::QHorizontalBarSeries::hovered, this, &TrackCmpPane::hovered);
    connect(m_barSeries, &QtCharts::QHorizontalBarSeries::clicked, this, &TrackCmpPane::clicked);
}

void TrackCmpPane::setAxesShown(bool shown) const
{
    BarChartBase::setAxesShown(shown);
    ui->action_Show_Axes->setChecked(shown);

    m_axisY->setVisible(false);  // always hide Y axis, no matter what.
}

bool TrackCmpPane::axesShown() const
{
    return ui != nullptr && ui->action_Show_Axes->isChecked();
}

void TrackCmpPane::setSortDirection(bool state)
{
    ui->sortDirection->setIcon(state ? Icons::get("view-sort-ascending") :
                                       Icons::get("view-sort-descending"));
    refreshChart();
}

// This is called from a timer to change the color of the current item
void TrackCmpPane::highlightCurrent()
{
    if (m_barSeries == nullptr)
        return;

    const TrackModel& model = app().trackModel();
    const auto barSets = m_barSeries->barSets();

    // Reset old bar to color it should be
    if (m_prevRow >= 0 && m_prevRow < m_rowToChart.size())
        if (const int chartIdx = m_rowToChart.at(m_prevRow); chartIdx >= 0 && chartIdx < barSets.count())
            setBarColorFromModel(model, barSets.at(chartIdx), m_prevRow);

    // Highlight current
    if (m_currentRow >= 0 && m_currentRow < m_rowToChart.size())
        if (const int chartIdx = m_rowToChart.at(m_currentRow); chartIdx >= 0 && chartIdx < barSets.count())
            barSets.at(chartIdx)->setColor(cfgData().trkPtMarkerColor);

    m_prevRow = m_currentRow;
}

// Unfortunately, changing a bar color is very slow, likely because it causes
// a complete repaint of the chart, and Qt can't use GL for that as of 5.9.
// As a workaround, we trigger that on a timer to avoid spamming it.  It's not
// as nice, but it means the current track can change far more rapidly.
void TrackCmpPane::currentTrackChanged(const QModelIndex& current)
{
    using namespace std::chrono_literals;

    m_currentRow = current.isValid() ? current.row() : -1;
    m_currentTimer.start(250ms);
}

void TrackCmpPane::updateActions()
{
    const QScrollBar* vScroller = ui->graphScroll->verticalScrollBar();

    ui->action_Page_Up->setEnabled(vScroller->sliderPosition() > vScroller->minimum());
    ui->action_Page_Down->setEnabled(vScroller->sliderPosition() < vScroller->maximum());
}

void TrackCmpPane::hovered(bool status, int /*index*/, QtCharts::QBarSet* barSet)
{
    if (!status) {
        m_chart->setToolTip(QString()); // unset tooltip
    } else {
        const TrackModel& model = app().trackModel();
        const int row = barSet->property(rowProperty).toInt();

        if (row >= model.rowCount())
            return;

        m_chart->setToolTip(model.tooltip(model.index(row, TrackModel::Name)));
    }

    updateActions();
}

// On click, make it the current item in a recent track pane
void TrackCmpPane::clicked(int /*index*/, QtCharts::QBarSet* barSet)
{
    const TrackModel& model = app().trackModel();
    const int row = barSet->property(rowProperty).toInt();

    if (row >= model.rowCount())
        return;

    auto* trackPane = mainWindow().findPane<TrackPane>();
    if (trackPane == nullptr)
        return;

    trackPane->select(model.index(row, TrackModel::Name),
                      QItemSelectionModel::Current | QItemSelectionModel::Rows);
}

// There's a QBarSet for each data column we're to display (e.g, distance)
// Each item in the QBarSet corresponds to the column data from a selected TrackItem

void TrackCmpPane::updateChart()
{
    const TrackModel& model = app().trackModel();

    if (ui == nullptr)
        return;

    m_currentRow = -1; // reset

    m_barWidth = std::clamp(m_barWidth, 4, 256);
    m_chart->setAnimationOptions(ui->action_Animated->isChecked() ? QtCharts::QChart::SeriesAnimations :
                                                                  QtCharts::QChart::NoAnimation);
    m_barSeries->setLabelsVisible(ui->action_Show_Bar_Values->isChecked());

    const int modelRowCount = model.rowCount();
    if (modelRowCount == 0 || m_plotColumn < 0) {
        m_barSeries->clear();
        return;
    }

    // Add matching rows
    {
        QVector<int> rows;
        rows.reserve(modelRowCount);
        for (int row = 0; row < modelRowCount; ++row)
            if (m_queryRoot->match(model, QModelIndex(), row))
                rows.append(row);

        const bool sortAscending = ui->sortDirection->isChecked();

        // Sort data
        std::sort(rows.begin(), rows.end(), [this, &sortAscending, &model](const int r0, const int r1) {
            const QModelIndex lhs = model.index(r0, m_plotColumn);
            const QModelIndex rhs = model.index(r1, m_plotColumn);
            const QVariant lhsData = model.data(lhs, Util::RawDataRole);
            const QVariant rhsData = model.data(rhs, Util::RawDataRole);

            return sortAscending ? QtCompat::gt(lhsData, rhsData) : QtCompat::lt(lhsData, rhsData);
        });

        // Early out if nothing has changed since last time
        if (rows == m_chartToRow)
            return;

        // Swap into the displayedRows vector, and abandon our own.
        rows.swap(m_chartToRow);
    }

    // Reset selection summary
    m_selectionSummary.clear(model.rowCount(), m_chartToRow.size());

    if (m_chartToRow.size() == 0) {  // ensure we have at least one entry
        m_barSeries->clear();
        return;
    }

    // Clear old graph
    m_barSeries->clear();
    m_barSeries->detachAxis(m_axisX);

    QVector<double> values;
    values.reserve(m_chartToRow.size());

    double maxVal = -std::numeric_limits<decltype(maxVal)>::max();
    double unitsXMul;

    // Collect data
    for (const auto row : m_chartToRow) {
        const QModelIndex plotIdx = model.index(row, m_plotColumn);
        const double value = model.data(plotIdx, Util::RawDataRole).toDouble();
        values.append(value);
        maxVal = std::fmax(maxVal, value);

        m_selectionSummary.accumulate(model, plotIdx, 1); // for status update
    }
    
    // initial width resize so that the plotArea is sane
    m_chart->resize(QSizeF(size().width(), m_chart->size().height()));

    // Calculate normalization values and axis ranges
    {
        const Units& xAxisUnits = TrackModel::mdUnits(m_plotColumn);

        const int padWidth   = QApplication::fontMetrics().size(Qt::TextSingleLine, "M").width() * 2;
        const QString labelForWidth = xAxisUnits.chartLabelFormat(1000.0);
        const int labelWidth = QApplication::fontMetrics().size(Qt::TextSingleLine, labelForWidth).width() + padWidth;

        int tickCount = m_chart->plotArea().width() / std::max(labelWidth, 1);
        maxVal = std::fmax(maxVal, 1.0);

        double unitsMin = xAxisUnits.toDouble(0.0);
        double unitsMax = xAxisUnits.toDouble(maxVal);

        looseNiceNumbers(unitsMin, unitsMax, tickCount);

        unitsXMul = xAxisUnits.toDouble(1.0); // so we can just multiply later on.

        m_axisX->setTickCount(std::min(tickCount, 5)); // the calculated tick count makes labels too dense
        m_axisX->setMinorTickCount(4);
        m_axisX->setLabelFormat(xAxisUnits.chartLabelFormat(unitsMax));
        m_axisX->setRange(unitsMin, unitsMax);

        m_barSeries->setLabelsFormat(QString("@value " + xAxisUnits.suffix(unitsMax)));
    }

    // Scale values to the display unit (there's no built-in display-time scaling)
    assert(m_chartToRow.size() == values.size());

    // create bar sets
    QList<QtCharts::QBarSet*> barSets;
    barSets.reserve(m_chartToRow.size());

    // Create rowToIndex map from the indexToRow map
    m_rowToChart.resize(modelRowCount);
    m_rowToChart.fill(-1);

    int rowPos = 0;
    for (const auto row : m_chartToRow) {
        auto* barSet = new QtCharts::QBarSet("");

        barSets.append(barSet);

        barSet->append(values.at(rowPos) * unitsXMul);
        setBarColorFromModel(model, barSet, row);
        barSet->setProperty(rowProperty, row);

        m_rowToChart[row] = rowPos++;
    }

    // Add bar sets to the graph
    m_barSeries->append(barSets);   // append the list of new barsets we created
    m_barSeries->attachAxis(m_axisX); // put X axis back

    // Set size of chart to keep bars the same height no matter how many there are (there's a scroll area)
    const int height = std::max(m_chartToRow.size() * m_barWidth, 5) +
                       (axesShown() ? fontMetrics().height() : 0) +
                       (m_chart->size().height() - m_chart->plotArea().height());

    ui->graphScroll->widget()->resize(ui->graphScroll->contentsRect().width(), height);

    mainWindow().updateStatus(m_selectionSummary);
}

void TrackCmpPane::resizeEvent(QResizeEvent *event)
{
    BarChartBase::resizeEvent(event);

    refreshChart();
}

// Force removal of old chart data and refresh of new.
void TrackCmpPane::refreshChart(int delayMs)
{
    m_chartToRow.clear();
    m_barSeries->clear();

    if (delayMs > 0)
        m_updateTimer.start(delayMs);
    else
        updateChart();
}

void TrackCmpPane::clearChart()
{
    m_barSeries->clear();
}

void TrackCmpPane::on_lockToQuery_toggled(bool checked)
{
    setLockToQuery(checked);
}

void TrackCmpPane::on_sortDirection_toggled(bool checked)
{
    setSortDirection(checked);
}

void TrackCmpPane::setBarValuesShown(bool show)
{
    if (ui == nullptr)
        return;

    ui->action_Show_Bar_Values->setChecked(show);
    refreshChart(10);
}

void TrackCmpPane::on_action_Set_Bar_Width_triggered()
{
    const int val = QInputDialog::getInt(this, tr("Set Bar Width"), tr("Width (px)"), m_barWidth, 4, 256, 1);

    setBarWidth(val);
}

void TrackCmpPane::on_action_Show_Bar_Values_triggered(bool shown)
{
    setBarValuesShown(shown);
}

void TrackCmpPane::on_action_Page_Up_triggered()
{
    ui->graphScroll->verticalScrollBar()->triggerAction(QScrollBar::SliderPageStepSub);
    updateActions();
}

void TrackCmpPane::on_action_Page_Down_triggered()
{
    ui->graphScroll->verticalScrollBar()->triggerAction(QScrollBar::SliderPageStepAdd);
    updateActions();
}

void TrackCmpPane::on_action_Show_Axes_triggered(bool shown) const
{
    setAxesShown(shown);
}

void TrackCmpPane::save(QSettings& settings) const
{
    BarChartBase::save(settings);

    if (ui != nullptr) {
        MemberSave(settings, ui->trackQuery);
        MemberSave(settings, ui->graphData);

        SL::Save(settings, "barWidth", m_barWidth);
        SL::Save(settings, "lockToTrackQuery", ui->lockToQuery->isChecked());
        SL::Save(settings, "sortAscending", ui->sortDirection->isChecked());
        SL::Save(settings, "animated", ui->action_Animated->isChecked());
        SL::Save(settings, "barValues", ui->action_Show_Bar_Values->isChecked());
        SL::Save(settings, "showAxes", axesShown());
    }
}

void TrackCmpPane::load(QSettings& settings)
{
    const QSignalBlocker block(ui->trackQuery);

    BarChartBase::load(settings);

    if (ui != nullptr) {
        MemberLoad(settings, ui->trackQuery);
        MemberLoad(settings, ui->graphData);
        setQuery(ui->trackQuery->text());
        setPlotColumn(ui->graphData->currentIndex());

        setBarWidth(SL::Load(settings, "barWidth", 20));
        setLockToQuery(SL::Load(settings, "lockToTrackQuery", false));
        setSortDirection(SL::Load(settings, "sortAscending", false));
        setBarValuesShown(SL::Load(settings, "barValues", false));
        setAxesShown(SL::Load(settings, "showAxes", true));
        ui->action_Animated->setChecked(SL::Load(settings, "animated", true));
    }
}
