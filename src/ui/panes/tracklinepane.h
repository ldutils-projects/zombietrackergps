/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TRACKLINECHART_H
#define TRACKLINECHART_H

#include <array>
#include <QVector>
#include <QTimer>
#include <QStandardItemModel>
#include "chartbase.h"

#include <src/core/modelmetadata.h>
#include <src/ui/widgets/chartviewzoom.h>

namespace Ui {
class TrackLinePane;
} // namespace Ui

class QStandardItem;
class MainWindow;
class PointModel;
class Units;
class QPointF;

namespace QtCharts {
class QValueAxis;
class QLineSeries;
class QAbstractAxis;
} // namespace QtCharts

class TrackLinePane final : public ChartBase
{
    Q_OBJECT

public:
    explicit TrackLinePane(MainWindow& mainWindow, QWidget *parent = nullptr);
    ~TrackLinePane() override;

//  void selectAll() override;
//  void selectNone() override;
    void showAll() override;
    void newConfig() override;

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

private slots:
    void processRowsAboutToBeRemoved(const QModelIndex &parent, int first, int last);
    void processModelAboutToBeReset();
    void currentTrackChanged(const QModelIndex&);
    void currentPointChanged(const QModelIndex&);
    void pointDataChanged();
    void selectedPointsChanged(const QItemSelectionModel*, const QItemSelection&, const QItemSelection&);
    void toggleColumn(QStandardItem*);
    void updateAxes();
    void showContextMenu(const QPoint& pos);
    void pan(QMouseEvent*, const QPoint& rel);
    void mouseMove(QMouseEvent*);
    void endPan();
    void mouseRelease(QMouseEvent*);
    void on_TrackLinePane_toggled(bool checked) { paneToggled(checked); }
    void on_action_Show_Axes_toggled(bool);
    void on_action_Show_Legend_toggled(bool);
    void on_action_Reset_Zoom_triggered();
    void on_action_Zoom_In_X_triggered();
    void on_action_Zoom_Out_X_triggered();
    void on_action_Pan_Left_triggered();
    void on_action_Pan_Right_triggered();
    void on_action_Page_Left_triggered();
    void on_action_Page_Right_triggered();
    void on_action_Zoom_to_Range_triggered();
    void on_action_Adjust_Axes_triggered();

private:
    // markers for drawing current point item and selected point ranges
    enum class Marker {
        _First = 0,
        Current = _First, // current point
        SelectionTop,     // show point range
        SelectionBottom,  // ...
        SelectionLeft,    // ...
        SelectionRight,   // ...
        _Count
    };

    PointModel* currentPointModel();
    const PointModel* currentPointModel() const;

    void pan(const QPoint& rel);
    void pan(qreal relX, qreal relY) { return pan(QPoint(relX, relY)); }

    void enableActions(); // enable proper set of actions for current state
    void setupActionIcons();
    void setupDataSelector();
    void setupChart();
    void setupMenus();
    void setupSignals();
    void setupTimers();
    void updateChart() override;
    void clearChart();
    void refreshChart();
    void updateYAxis();
    void initYAxis(QtCharts::QLineSeries*, const Units&, qreal yMin, qreal yMax);
    void initXAxis(QtCharts::QLineSeries*);
    void initMarkers();
    void updateXAxis();
    void updateXRange();
    void wheelEvent(QWheelEvent*) override;
    void resizeEvent(QResizeEvent*) override;
    void setZoom(float);
    void resetPanZoom();
    void zoomIn(float);
    void zoomOut(float);
    void setAxesShown(bool shown) const override;
    void setLegendShown(bool shown) const override;
    bool axesShown() const override;
    bool legendShown() const override;
    bool isChecked(int col) const;
    bool hasSelection() const override;
    qreal xAxisDistance(int xPos) const;  // distance into track for a given chart position
    QtCharts::QLineSeries* newSeries(int col);
    QPointF positionForIdx(const QModelIndex&) const;
    void setCurrent(const QModelIndex&) const;

    static QString singleMarkerText(const PointModel*, const QColor&, ModelType, const QVariant&,
                                    const QString& pre="", const QString& post="");
    void updateMarkers();
    void drawMarker(Marker, qreal xPosL, qreal xPosR = -1.0);
    void clearMarker(Marker);
    void clearSelectionMarkers();
    void drawMarkerText(qreal xPos);
    void drawSelectionMarkers();

    void setCurrentTrack(const QModelIndex& pointModelIdx);

    QtCharts::QLineSeries* getMarker(Marker m) const { return m_marker.at(int(m)); }

    inline qreal xViewExtentInUnits() const;
    inline qreal xAxisStart() const;
    inline qreal trackTotalDistanceUnits() const;

    Ui::TrackLinePane*     ui;
    QPersistentModelIndex  m_currentTrackIdx;  // current selection
    QStandardItemModel     m_graphDataModel;   // for show-column combo box
    QVector<int>           m_columnMap;        // map PointModel column to combobox position
    QPersistentModelIndex  m_rangeLIdx;        // for redrawing election ranges
    QPersistentModelIndex  m_rangeRIdx;        // ...

    const float            m_zoomStepWheel;    // zoom step multiplier for wheel events
    const float            m_zoomStepMenu;     // zoom step multiplier for kb/menu events
    float                  zoomLevel;          // current zoom
    const float            m_initZoom;         // init zoom level
    const float            m_minZoom;          // min zoom level
    const float            m_maxZoom;          // max zoom level
    float                  xBegin;             // X axis start point, [0..1]

    QTimer                 m_axisUpdateTimer;  // avoid axis update spam

    std::array<QtCharts::QLineSeries*, int(Marker::_Count)> m_marker = { }; // current/range markers
    QVector<ModelType>     m_valueColumn;      // columns for labels.
};

#endif // TRACKLINECHART_H
