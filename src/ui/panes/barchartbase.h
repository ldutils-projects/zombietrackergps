/*
    Copyright 2021-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BARCHARTBASE_H
#define BARCHARTBASE_H

#include <memory>  // for std::unqiue_ptr
#include <QPixmap>
#include <QTimer>
#include <QStandardItemModel>
#include <QVector>

#include <src/core/modelmetadata.h>
#include <src/core/query.h>
#include <src/ui/widgets/chartviewzoom.h>
#include <src/ui/misc/querybar.h>

#include "src/core/selectionsummary.h"

#include "chartbase.h"

namespace QtCharts {
class QBarSet;
class QAbstractBarSeries;
class QBarCategoryAxis;
class QValueAxis;
} // namespace QtCharts

class QLineEdit;
class QLabel;
class QGroupBox;
class QHBoxLayout;
class QToolButton;
class QComboBox;

class MainWindow;
class TrackModel;

class BarChartBase :
        public ChartBase,
        public QueryBar
{
public:
    BarChartBase(MainWindow&, PaneClass, QWidget *parent = nullptr);
    ~BarChartBase() override;

    // *** begin Pane API
    void showAll() override;
    void newConfig() override;
    void focusIn() override;
    // *** end Pane API

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

    virtual void setBarWidth(int val);

protected slots:
    virtual void setPlotColumn(int index);
    virtual void queryTextChanged(const QString&);       // signal from this widget
    virtual void externQueryTextChanged(const QString&); // signal from other widgets
    virtual void processRowsInserted(const QModelIndex& parent, int first, int last);
    virtual void processRowsRemoved(const QModelIndex& parent, int first, int last);
    virtual void processDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector<int>&);

protected:
    void setupTimers();
    void setupCompleter();
    void setupDataSelector();
    void setupSignals();
    void setPaneFilterBar(QHBoxLayout*, QComboBox*);
    void dragEnterEvent(QDragEnterEvent*) override;
    void dropEvent(QDropEvent*) override;
    void setQuery(const QString&) override;

    virtual void clearChart() = 0;
    virtual void refreshChart(int delayMs = 500); // 0 to avoid deferring
    virtual void updateActions() = 0;
    virtual void showContextMenu(const QPoint&);
    virtual void setBarColorFromModel(const TrackModel&, QtCharts::QBarSet*, int modelRow);
    virtual void setLockToQuery(bool checked);

    bool isEmptyQuery() const override;
    bool isValidQuery() const override;

    QStandardItemModel              m_graphDataModel;    // for show-column combo box
    QVector<int>                    m_columnMap;         // map combobox position to column
    QTimer                          m_updateTimer;       // avoid update spam

    QtCharts::QAbstractBarSeries*   m_barSeries;         // we only need one.

    Query::Context                  m_queryCtx;          // for query language
    Query::query_uniqueptr_t        m_queryRoot;         // root of node filter tree

    ModelType                       m_plotColumn;        // which column we should plot
    int                             m_barWidth;          // bar width

    SelectionSummary                m_selectionSummary;  // for status bar info

private:
    QLineEdit*                      m_trackQuery;
    QToolButton*                    m_lockToQuery;
    QComboBox*                      m_graphData;
};

#endif // BARCHARTBASE_H
