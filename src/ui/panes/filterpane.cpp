/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QInputDialog>

#include "filterpane.h"
#include "ui_filterpane.h"

#include <src/util/util.h>
#include <src/util/icons.h>
#include <src/util/ui.h>
#include <src/undo/undomgr.h>
#include <src/ui/panes/datacolumnpanebase.inl.h>

#include "src/ui/windows/mainwindow.h"
#include "src/ui/panes/trackpane.h"
#include "src/core/filtermodel.h"
#include "src/core/app.h"

FilterPane::FilterPane(MainWindow& mainWindow, QWidget *parent) :
    DataColumnPane(mainWindow, PaneClass::Filter, parent),
    NamedItem(FilterModel::getItemNameStatic()),
    ui(new Ui::FilterPane),
    m_queryCtx(&app().trackModel()),
    m_textDelegate(this),
    m_queryDelegate(m_queryCtx, this),
    m_model(app().filterModel())
{
    ui->setupUi(this);

    setupView(ui->viewCategories, &m_model);
    setWidgets<FilterModel>(defColumnView(),
                            ui->filterCategories, nullptr, ui->showColumns, ui->filterCtrl, ui->filterIsValid);

    setupActionIcons();
    setupContextMenus();
    setupSignals();
    setupDelegates();
    Util::SetupWhatsThis(this);

    newConfig();
}

FilterPane::~FilterPane()
{
    delete ui;
}

void FilterPane::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Edit_Query,        "edit-entry");
    Icons::defaultIcon(ui->action_Activate,          "view-filter");
    Icons::defaultIcon(ui->action_Set_Icon,          "image-x-icon");
    Icons::defaultIcon(ui->action_Edit_Name,         "edit-rename");
    Icons::defaultIcon(ui->action_Unset_Icon,        "edit-clear");
    Icons::defaultIcon(ui->action_Deactivate,        "edit-clear");
    Icons::defaultIcon(ui->action_Update_Filter,     "view-refresh");
    Icons::defaultIcon(ui->action_Create_New_Filter, "bookmark-new");
}

void FilterPane::setupContextMenus()
{
    paneMenu.addActions({ ui->action_Activate,
                          ui->action_Deactivate });
    paneMenu.addSeparator();
    paneMenu.addActions({ ui->action_Create_New_Filter,
                          mainWindow().getMainAction(MainAction::DuplicateSelection),
                          mainWindow().getMainAction(MainAction::DeleteSelection) });
    paneMenu.addSeparator();
    paneMenu.addActions({ ui->action_Edit_Name,
                          ui->action_Edit_Query,
                          ui->action_Update_Filter,
                          ui->action_Set_Icon,
                          ui->action_Unset_Icon });

    setupActionContextMenu(paneMenu);

    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(this, &FilterPane::customContextMenuRequested, this, &FilterPane::showContextMenu);
}

void FilterPane::setupSignals()
{
    DataColumnPane::setupSignals();

    QTreeView& view = *ui->viewCategories;
    connect(&view, &QTreeView::activated, this, &FilterPane::filterSelected);
    connect(&view, &QTreeView::clicked, this, &FilterPane::filterSelected);
}

void FilterPane::setupDelegates()
{
    initDelegates({{ FilterModel::Name,  &m_textDelegate },
                   { FilterModel::Query, &m_queryDelegate }});
}

void FilterPane::newConfig()
{
    DataColumnPane::newConfig();
    ui->viewCategories->setIconSize(cfgData().iconSizeFilter);
}

const FilterPane::DefColumns& FilterPane::defColumnView() const
{
    static const DefColumns cols = {
        { FilterModel::Name,  true  },
        // FilterModel::Icon is in Name column.
        { FilterModel::Query, false },
    };

    // Verify we added everything.  It's not critical, but better practice.
    for (ModelType md = FilterModel::_First; md < FilterModel::_Count; ++md)
        assert(cols.findData(md) >= 0);

    return cols;
}

void FilterPane::showContextMenu(const QPoint& pos)
{
    menuIdx = clickPosIndex(pos);

    const bool hasSelection = this->hasSelection();

    ui->action_Activate->setEnabled(hasSelection);
    ui->action_Edit_Query->setEnabled(hasSelection);
    ui->action_Edit_Name->setEnabled(hasSelection);
    ui->action_Set_Icon->setEnabled(hasSelection);

    paneMenu.exec(mapToGlobal(pos));
}

void FilterPane::addFilterInteractive(const QString& query)
{
    if (ui == nullptr)
        return;

    if (!m_queryCtx.isValidQuery(query)) {
        mainWindow().statusMessage(UiType::Warning, tr("Invalid Query"));
        return;
    }

    bool ok;
    const QString newName = QInputDialog::getText(this, tr("Filter Name"),
                                                  tr("Filter name:"), QLineEdit::Normal,
                                                  "My Track Filter", &ok);

    if (!ok || newName.isEmpty()) {
        mainWindow().statusMessage(UiType::Warning, tr("Canceled"));
        return;
    }

    const UndoMgr::ScopedUndo undoSet(app().undoMgr(), tr("Add Filter: ") + newName);

    // Add current query the model.
    m_model.appendRow({ newName, query });

    mainWindow().statusMessage(UiType::Success, tr("Added filter: ") + newName);
}

TrackPane* FilterPane::getFilterPane() const
{
    if (auto* pane = mainWindow().findPane<TrackPane>(); pane != nullptr)
        return pane;

    mainWindow().statusMessage(UiType::Warning, tr("No Pane found for filter"));
    return nullptr;
}

void FilterPane::unsetFilter(PaneBase* pane)
{
    if (pane == nullptr)
        return;

    pane->setQuery("");
}

void FilterPane::unsetFilter()
{
    return unsetFilter(getFilterPane());
}

// True if anything is selected in a node's subtree, but NOT including the
// node itself.
bool FilterPane::hasSubtreeSelection(const QModelIndex& parent) const
{
    for (int row = 0; row < m_model.rowCount(parent); ++row) {
        const QModelIndex idx = m_model.index(row, 0, parent);
        if (isSelected(idx) || hasSubtreeSelection(idx))
            return true;
    }

    return false;
}

// Absolutely dreadfully inefficient due to dual recursions, but performance doesn't matter here: this
// only happens when the user applies the filter (rarely), not when the filter is used (often).
void FilterPane::buildFilterString(QString& aggregateNames, QString& aggregateQuery, const QModelIndex& idx, bool firstAtDepth)
{
    const bool subtreeSelected = hasSubtreeSelection(idx);
    const bool nodeSelected = isSelected(idx);

    if (!nodeSelected && !subtreeSelected)
        return;

    if (idx.isValid()) {
        const QString name  = m_model.data(FilterModel::Name, idx, Util::RawDataRole).toString();
        const QString query = m_model.data(FilterModel::Query, idx, Util::RawDataRole).toString();

        if (!aggregateNames.isEmpty())
            aggregateNames.append("+");
        aggregateNames.append(name);

        if (!firstAtDepth)
            aggregateQuery.append(" | ");
    
        aggregateQuery.append(query);

        if (subtreeSelected)
            aggregateQuery.append(" & ( ");
    }

    firstAtDepth = true;
    const int queryStrSize = aggregateQuery.size();

    for (int row = 0; row < m_model.rowCount(idx); ++row) {
        buildFilterString(aggregateNames, aggregateQuery, m_model.index(row, 0, idx), firstAtDepth);
        if (aggregateQuery.size() > queryStrSize)
            firstAtDepth = false;
    }

    if (idx.isValid())
        if (subtreeSelected)
            aggregateQuery.append(" )");
}

TreeModel* FilterPane::model()
{
    return &m_model;
}

const TreeModel* FilterPane::model() const
{
    return &m_model;
}

void FilterPane::setFilter()
{
    setFilter(getFilterPane());
}

void FilterPane::setFilter(PaneBase* pane)
{
    if (pane == nullptr)
        return;

    QString aggregateNames;
    QString aggregateQuery;

    aggregateNames.reserve(128);
    aggregateQuery.reserve(256);

    buildFilterString(aggregateNames, aggregateQuery);

    pane->setQuery(aggregateQuery);

    mainWindow().statusMessage(UiType::Info, tr("Using filter(s): ") + aggregateNames);
}

void FilterPane::filterSelected(const QModelIndex&)
{
    setFilter();
}

void FilterPane::on_action_Create_New_Filter_triggered()
{
    PaneBase* pane = getFilterPane();
    if (pane == nullptr)
        return;

    addFilterInteractive(pane->getQuery());
}

void FilterPane::on_action_Update_Filter_triggered()
{
    PaneBase* pane = getFilterPane();
    if (pane == nullptr)
        return;

    const auto selections = getSelections();

    // Friendly undo name
    const QString undoName = UndoMgr::genName(tr("Update"), selections.size(), tr("Filter"), tr("Filters"));
    const UndoMgr::ScopedUndo undoSet(app().undoMgr(), undoName);

    int count = 0;
    for (const auto& idx : selections) {
        m_model.setData(FilterModel::Query, idx, pane->getQuery(), Util::RawDataRole);
        ++count;
    }

    mainWindow().statusMessage(UiType::Success, undoName);
}

void FilterPane::on_action_Activate_triggered()
{
    setFilter();
}

bool FilterPane::hasAction(MainAction action) const
{
    switch (action)
    {
    case MainAction::DeleteSelection:    [[fallthrough]];
    case MainAction::DuplicateSelection: return true;
    default:                             return false;
    }
}

void FilterPane::on_action_Edit_Name_triggered()
{
    if (menuIdx.isValid()) {
        setColumnHidden(FilterModel::Name, false);
        ui->viewCategories->edit(menuIdx.sibling(menuIdx.row(), FilterModel::Name));
    }
}

void FilterPane::on_action_Edit_Query_triggered()
{
    if (menuIdx.isValid()) {
        setColumnHidden(FilterModel::Query, false);
        ui->viewCategories->edit(menuIdx.sibling(menuIdx.row(), FilterModel::Query));
    }
}

void FilterPane::on_action_Set_Icon_triggered()
{
    setIcon(FilterModel::Icon);
}

void FilterPane::on_action_Unset_Icon_triggered()
{
    clearIcon(FilterModel::Icon);
}

void FilterPane::on_action_Deactivate_triggered()
{
    unsetFilter();
}
