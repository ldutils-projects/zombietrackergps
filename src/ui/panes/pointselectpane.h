/*
    Copyright 2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef POINTSELECTPANE_H
#define POINTSELECTPANE_H

#include <QItemSelectionModel>
#include <QIdentityProxyModel>
#include <QModelIndex>

#include "src/core/selectionsummary.h"

class MainWindow;
class PointModel;
class QItemSelection;

// This is a base class for panes which maintain their own set of selected points in a
// QItemSelectionModel. They can copy this to a PointPane, and on pane focus, they can
// select points in the PointModel to make the selection visible in a MapPane.
class PointSelectPane
{
protected:
    PointSelectPane(MainWindow&);
    ~PointSelectPane();

    void currentTrackChanged(const QModelIndex&);
    void processModelAboutToBeReset();
    bool processRowsAboutToBeRemoved(const QModelIndex&, int first, int last);
    void focusIn();
    void refreshPointSelection();
    void select(const QItemSelection&, QItemSelectionModel::SelectionFlags = QItemSelectionModel::ClearAndSelect);
    void clearSelection();

    QModelIndex currentTrack() const { return m_currentTrack; }  // just for subclass asserts
    QModelIndex mapUp(const QModelIndex&) const;

    PointModel* getGeoPoints();

private:
    MainWindow&            m_mainWindow;
    QModelIndex            m_currentTrack;          // currently selected track
    QIdentityProxyModel    m_identityProxy;         // since we don't subset the points
    QItemSelectionModel    m_pointSelectionModel;   // for selecting points in various training zones
    SelectionSummary       m_pointSelectionSummary; // for selecting points by clicking on pie slices
};

#endif // POINTSELECTPANE_H
