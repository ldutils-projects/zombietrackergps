/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef VIEWPANE_H
#define VIEWPANE_H

#include <src/ui/widgets/lineeditdelegate.h>

#include "datacolumnpane.h"

namespace Ui {
class ViewPane;
} // namespace Ui

class MainWindow;
class QModelIndex;

class ViewPane final : public DataColumnPane, public NamedItem
{
    Q_OBJECT

public:
    explicit ViewPane(MainWindow&, QWidget *parent = nullptr);
    ~ViewPane() override;

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

private slots:
    void on_ViewPane_toggled(bool checked) { paneToggled(checked); }
    void on_action_Goto_triggered();
    void on_action_New_triggered();
    void on_action_Edit_triggered();
    void on_action_Update_triggered();
    void showContextMenu(const QPoint&);
    void viewSelected(const QModelIndex&);
    void on_action_Set_Icon_triggered();
    void on_action_Unset_Icon_triggered();

private:
    friend class TestZtgps;

    void setupActionIcons();
    void setupContextMenus();
    void setupDelegates();
    void setupSignals();
    void gotoIndex(const QModelIndex&) const;
    const DefColumns& defColumnView() const override;
    void newConfig() override;
    bool hasAction(MainAction) const override;

    // Obtain model associated with pane
    TreeModel* model() override;
    const TreeModel* model() const override;

    // Helper
    QString nameFromTopIndex(const QModelIndex& idx) const;

    Ui::ViewPane*    ui;
    LineEditDelegate m_textDelegate;
    QPoint           m_menuPos;
};

#endif // VIEWPANE_H
