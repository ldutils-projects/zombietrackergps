/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <chrono>

#include <QSet>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QInputDialog>

#include <src/util/icons.h>
#include <src/util/util.h>
#include <src/util/roles.h>
#include <src/util/ui.h>
#include <src/ui/panes/datacolumnpanebase.inl.h>

#include "src/ui/windows/mainwindow.h"
#include "src/core/trackmodel.h"
#include "src/core/trackitem.h"
#include "src/core/mapdatamodel.h"

#include "trackpane.h"
#include "filterpane.h"
#include "mapdatapane.inl.h"
#include "ui_trackpane.h"

TrackPane::TrackPane(MainWindow& mainWindow, QWidget *parent) :
    MapDataPane(mainWindow, PaneClass::Track, std::get<0>(TrackModel::getItemNameStatic()), parent),
    NamedItem(TrackModel::getItemNameStatic()),
    ui(new Ui::TrackPane),
    m_colorDelegate(this, false, tr("Track Color"), true, Util::RawDataRole),
    m_trackTypeDelegate(this, { MapDataModel::trackTypeName(TrackType::Trk),
                                MapDataModel::trackTypeName(TrackType::Rte) }),
    m_model(app().trackModel())
{
    ui->setupUi(this);

    setupView(ui->trackView, &app().trackModel());
    setWidgets<TrackModel>(defColumnView(),
                           ui->filterTrack, ui->filterColumn, ui->showColumns, ui->filterCtrl, ui->filterIsValid);

    setupActionIcons();
    setupContextMenus();
    setupSignals();
    setupDelegates();
    setupTimers();
    Util::SetupWhatsThis(this);

    newConfig();
}

TrackPane::~TrackPane()
{
    deleteUI(ui);
}

void TrackPane::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Reset_Track_Color,        "gkt-restore-defaults");
    Icons::defaultIcon(ui->action_Reset_Track_Note,         "gkt-restore-defaults");
    Icons::defaultIcon(ui->action_Edit,                     "document-edit");
    Icons::defaultIcon(ui->action_Set_Icon,                 "preferences-desktop-filetype-association");
    Icons::defaultIcon(ui->action_Change_Tags,              "tag");
    Icons::defaultIcon(ui->action_Select_Duplicates,        "edit-select");
    Icons::defaultIcon(ui->action_Select_All_Duplicates,    "edit-select");
    Icons::defaultIcon(ui->action_Create_Filter_from_Query, "view-filter");
    Icons::defaultIcon(ui->action_Unset_Icon,               "edit-clear");
}

void TrackPane::setupContextMenus()
{
    paneMenu.addActions({ mainWindow().getMainAction(MainAction::ZoomToSelection),
                          mainWindow().getMainAction(MainAction::SelectPerson) });
    paneMenu.addSeparator();
    paneMenu.addAction(ui->action_Create_Filter_from_Query);
    paneMenu.addSeparator();
    paneMenu.addActions({ ui->action_Edit,
                          ui->action_Set_Icon,
                          ui->action_Change_Tags });
    paneMenu.addSeparator();
    paneMenu.addActions({ ui->action_Unset_Icon,
                          ui->action_Reset_Track_Color,
                          ui->action_Reset_Track_Note });
    paneMenu.addSeparator();
    paneMenu.addActions({ mainWindow().getMainAction(MainAction::DeleteSelection),
                          mainWindow().getMainAction(MainAction::DuplicateSelection),
                          mainWindow().getMainAction(MainAction::MergeSelection),
                          mainWindow().getMainAction(MainAction::SimplifySelection),
                          mainWindow().getMainAction(MainAction::ReverseSelection),
                          mainWindow().getMainAction(MainAction::UnsetSpeed) });
    paneMenu.addSeparator();
    paneMenu.addActions({ ui->action_Select_Duplicates,
                          ui->action_Select_All_Duplicates });

    setupActionContextMenu(paneMenu);
    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(this, &TrackPane::customContextMenuRequested, this, &TrackPane::showContextMenu);

    // Tie our config button to main window action
    ui->selectPerson->setDefaultAction(mainWindow().getMainAction(MainAction::SelectPerson));
}

void TrackPane::setupSignals()
{
    MapDataPane::setupSignals();

    // Double click
    QTreeView& view = *ui->trackView;
    connect(&view, &QTreeView::doubleClicked, this, &TrackPane::doubleClicked);

    // Selection changes
    connect(selectionModel(), &QItemSelectionModel::selectionChanged, this, &TrackPane::processSelectionChanged);
    connect(&mainWindow(), &MainWindow::selectedTracksChanged, this, &TrackPane::processSelectedTracksChanged);
}

void TrackPane::addFilterInteractive()
{
    auto* filterPane = mainWindow().findPane<FilterPane>();

    if (filterPane == nullptr) {
        mainWindow().statusMessage(UiType::Warning, tr("No Filter Pane found"));
        return;
    }

    filterPane->addFilterInteractive(getQuery());
}

void TrackPane::resizeToFit(int defer)
{
    MapDataPane::resizeToFit(defer);
    TrackItem::setupSmallFont();
}

void TrackPane::setupDelegates()
{
    initDelegates({{ TrackModel::Name,  &m_textDelegate },
                   { TrackModel::Type,  &m_trackTypeDelegate },
                   { TrackModel::Color, &m_colorDelegate },
                   { TrackModel::Notes, &m_noteDelegate },
                   { TrackModel::Tags,  &m_tagDelegate },
                   { TrackModel::Flags, &m_flagDelegate }});

    m_colorDelegate.setPadSize(6, 6);
    m_colorDelegate.setMaxSize(72, -1);
}

const TrackPane::DefColumns& TrackPane::defColumnView() const
{
    static const DefColumns cols = {
        { TrackModel::Name,         true  },
        // TrackModel::Icon is in Name column
        { TrackModel::Type,         false },
        { TrackModel::Tags,         true  },
        { TrackModel::Color,        true  },
        { TrackModel::Notes,        true  },
        { TrackModel::Flags,        true  },
        { TrackModel::Keywords,     false },
        { TrackModel::Source,       false },
        { TrackModel::Length,       true  },
        { TrackModel::BeginDate,    true  },
        { TrackModel::EndDate,      false },
        { TrackModel::BeginTime,    false },
        { TrackModel::EndTime,      false },
        { TrackModel::StoppedTime,  false },
        { TrackModel::MovingTime,   true  },
        { TrackModel::TotalTime,    true  },
        { TrackModel::MinElevation, false },
        { TrackModel::AvgElevation, false },
        { TrackModel::MaxElevation, true  },
        { TrackModel::MinSpeed,     false },
        { TrackModel::AvgOvrSpeed,  false },
        { TrackModel::AvgMovSpeed,  true  },
        { TrackModel::MaxSpeed,     true  },
        { TrackModel::MinGrade,     false },
        { TrackModel::AvgGrade,     false },
        { TrackModel::MaxGrade,     true  },
        { TrackModel::MinCad,       false },
        { TrackModel::AvgMovCad,    false },
        { TrackModel::MaxCad,       false },
        { TrackModel::MinPower,     false },
        { TrackModel::AvgMovPower,  true  },
        { TrackModel::MaxPower,     true  },
        { TrackModel::Energy,       true  },
        { TrackModel::Ascent,       true  },
        { TrackModel::Descent,      false },
        { TrackModel::BasePeak,     true  },
        { TrackModel::Segments,     false },
        { TrackModel::Points,       false },
        { TrackModel::Area,         false },
        { TrackModel::MinTemp,      false },
        { TrackModel::AvgTemp,      false },
        { TrackModel::MaxTemp,      false },
        { TrackModel::MinHR,        false },
        { TrackModel::AvgHR,        true  },
        { TrackModel::MaxHR,        true  },
        { TrackModel::MinHRPct,     false },
        { TrackModel::AvgHRPct,     false },
        { TrackModel::MaxHRPct,     false },
        { TrackModel::Laps,         false },
        { TrackModel::MinLon,       false },
        { TrackModel::MinLat,       false },
        { TrackModel::MaxLon,       false },
        { TrackModel::MaxLat,       false },
    };

    // Verify we added everything.  It's not critical, but better practice.
    for (ModelType md = TrackModel::_First; md < TrackModel::_Count; ++md)
        assert(cols.findData(md) >= 0);

    return cols;
}

std::tuple<QString, bool> TrackPane::mergedName(const QModelIndexList& selections, const QString& undoName) const
{
    if (model() == nullptr || selections.isEmpty())
        return { QString(), false };

    // Candidate name
    QString newName = model()->rowSibling(TrackModel::Name, selections.front()).data(Util::RawDataRole).toString();

    // Query user for name, supplying candidate.  The appended tabs are a cheesy way to
    // force a larger width for the dialog. There's no reason for this dialog to be non-const,
    // but the Qt method requires a non-const parent widget for positioning, so we const_cast it.
    bool ok;
    newName = QInputDialog::getText(const_cast<TrackPane*>(this), undoName,
                                    tr("Merged Track Name") + QString(5, '\t'),
                                    QLineEdit::Normal, newName, &ok);

    return { newName, ok };
}

TreeModel* TrackPane::model()
{
    return &m_model;
}

const TreeModel* TrackPane::model() const
{
    return &m_model;
}

// For performance reasons (selections are needed within the TrackMap render loop), we cache some results of
// the selection.
void TrackPane::processSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected)
{
    m_selectionSummary.update(m_model, topFilter(), selectionModel(), selected, deselected);
    mainWindow().updateStatus(m_selectionSummary);
}

void TrackPane::processSelectedTracksChanged()
{
    const bool hasSelection = selectionModel()->hasSelection();
    const bool hasVisible   = topFilter().rowCount() > 0;

    // Update which UI actions are enabled, given the selection state.
    ui->action_Reset_Track_Color->setEnabled(hasSelection);
    ui->action_Reset_Track_Note->setEnabled(hasSelection);
    ui->action_Set_Icon->setEnabled(hasSelection);
    ui->action_Unset_Icon->setEnabled(hasSelection);
    ui->action_Change_Tags->setEnabled(hasSelection);
    ui->action_Select_Duplicates->setEnabled(hasSelection);
    ui->action_Select_All_Duplicates->setEnabled(hasVisible);
}

void TrackPane::showContextMenu(const QPoint& pos)
{
    // Enable menus as appropriate
    m_menuIdx = clickPosIndex(pos);

    ui->action_Create_Filter_from_Query->setEnabled(!ui->filterTrack->text().isEmpty());
    ui->action_Edit->setEnabled(TrackModel::mdIsEditable(m_menuIdx.column()));

    paneMenu.exec(mapToGlobal(pos));
}

bool TrackPane::hasAction(MainAction action) const
{
    switch (action)
    {
    case MainAction::ZoomToSelection:    [[fallthrough]];
    case MainAction::DeleteSelection:    [[fallthrough]];
    case MainAction::DuplicateSelection: [[fallthrough]];
    case MainAction::SimplifySelection:  [[fallthrough]];
    case MainAction::ReverseSelection:   [[fallthrough]];
    case MainAction::UnsetSpeed:         return true;

    case MainAction::MergeSelection:
        if (selectionModel() != nullptr)
            return selectionModel()->selectedRows().count() > 1;
        return false;

    default:                             return false;
    }
}

void TrackPane::newConfig()
{
    MapDataPane::newConfig();
    ui->trackView->setIconSize(cfgData().iconSizeTrack);
}

void TrackPane::dragEnterEvent(QDragEnterEvent* event)
{
    // Allow drop from filters
    if (app().filterModel().isStreamMagic(event->mimeData()))
        event->acceptProposedAction();

    MapDataPane::dragEnterEvent(event);
}

void TrackPane::dropEvent(QDropEvent* event)
{
    // Drop from filter
    if (app().filterModel().isStreamMagic(event->mimeData())) {
        const auto srcIndexes = app().filterModel().getDropIndices(event->mimeData());
        assert(srcIndexes.size() == 1);

        if (auto* filterPane = mainWindow().findPane<FilterPane>(); filterPane != nullptr) {
            filterPane->setFilter(this);
            event->accept();
            return;
        }
    }

    MapDataPane::dropEvent(event);
}

void TrackPane::on_action_Reset_Track_Color_triggered()
{
    resetDataAction(TrackModel::Color, ui->action_Reset_Track_Color);
}

void TrackPane::on_action_Reset_Track_Note_triggered()
{
    resetDataAction(TrackModel::Notes, ui->action_Reset_Track_Note);
}

void TrackPane::updateVisibility()
{
    MapDataPane::updateVisibilityImpl<TrackModel>();
    emit mainWindow().visibleTracksChanged(); // send notification
}

void TrackPane::filterTextChanged(const QString& query)
{
    emit mainWindow().trackQueryChanged(query);

    MapDataPane::filterTextChanged(query);

    using namespace std::chrono_literals;
    m_mapUpdateTimer.start(500ms);
}

void TrackPane::load(QSettings& settings)
{
    MapDataPane::load(settings);

    // Version 4 added the Flags column.  It's unsaved (auto-gen on load) but when we first load
    // a config withou it, we will move it into the desired visual position.  User can move it from there.
    if (cfgData().priorCfgDataVersion < 4) {
        if (const int defFlagPos = defColumnView().findData(TrackModel::Flags); defFlagPos >= 0 && m_treeView != nullptr) {
            moveSection(visualIndex(TrackModel::Flags), defFlagPos);
            refreshHiddenColumns();
        }
    }

    // Version 7 added new Source column.
    insertColumnOnLoad(7, TrackModel::Source, TrackModel::_Count);

    // Version 13 added new HRPct columns
    if (cfgData().priorCfgDataVersion < 13)
        for (ModelType mt : { TrackModel::MinHRPct, TrackModel::AvgHRPct, TrackModel::MaxHRPct } )
            setColumnHidden(visualIndex(mt), !defColumnView().defaultShown(mt));

    // Version 18 added AvgGrade and AvgElevation
    insertColumnOnLoad(18, TrackModel::AvgElevation, TrackModel::_Count);
    insertColumnOnLoad(18, TrackModel::AvgGrade, TrackModel::_Count);
}

void TrackPane::on_action_Edit_triggered()
{
    if (m_menuIdx.isValid())
        ui->trackView->edit(m_menuIdx);
}

void TrackPane::on_action_Set_Icon_triggered()
{
    setIcon(TrackModel::Icon);
}

void TrackPane::on_action_Unset_Icon_triggered()
{
    clearIcon(TrackModel::Icon);
}

void TrackPane::on_action_Change_Tags_triggered()
{
    if (m_menuIdx.isValid())
        ui->trackView->edit(m_menuIdx.sibling(m_menuIdx.row(), TrackModel::Tags));
}

void TrackPane::on_action_Select_Duplicates_triggered()
{
    selectDuplicates(false);
}

void TrackPane::on_action_Select_All_Duplicates_triggered()
{
    selectDuplicates(true);
}

void TrackPane::on_action_Create_Filter_from_Query_triggered()
{
    addFilterInteractive();
}
