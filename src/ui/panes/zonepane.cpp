/*
    Copyright 2022-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <array>
#include <chrono>

#include <QPieSeries>
#include <QPieSlice>
#include <QLegend>
#include <QLegendMarker>
#include <QModelIndexList>
#include <QList>

#include <src/core/modelmetadata.inl.h>
#include <src/util/icons.h>
#include <src/util/ui.h>

#include "src/ui/windows/mainwindow.h"
#include "src/ui/panes/trackpane.h"
#include "src/core/pointitem.inl.h"
#include "src/core/trackmodel.h"
#include "src/core/app.h"

#include "zonepane.h"
#include "ui_zonepane.h"

ZonePane::ZonePane(MainWindow& mainWindow, QWidget* parent) :
    ChartBase(mainWindow, PaneClass::ZoneSummary, parent),
    PointSelectPane(mainWindow),
    ui(new Ui::ZonePane),
    m_updateTimer(this),
    m_pieSeries(nullptr)
{
    ui->setupUi(this);
    setPaneFilterBar(ui->filterCtrl);

    setupActionIcons();
    setupChart();
    setupMenus();
    setupSignals();
    setupTimers();
    Util::SetupWhatsThis(this);
}

ZonePane::~ZonePane()
{
    deleteUI(ui);
}

void ZonePane::setupChart()
{
    ChartBase::setupChart();

    if (m_chart == nullptr)
        return;

    m_pieSeries = new QtCharts::QPieSeries();

    m_chart->addSeries(m_pieSeries);
    m_chart->setAnimationDuration(300);

    m_chart->legend()->setAlignment(Qt::AlignLeft);

    m_chart->setAnimationOptions(QtCharts::QChart::SeriesAnimations);

    // m_pieSeries->setUseOpenGL(true); // not yet supported for pie charts, but someday?

    // Set up pie series signals
    connect(m_pieSeries, &QtCharts::QPieSeries::hovered, this, &ZonePane::sliceHovered, Qt::UniqueConnection);
    connect(m_pieSeries, &QtCharts::QPieSeries::doubleClicked, this, &ZonePane::sliceDoubleClicked, Qt::UniqueConnection);

    ui->verticalLayout->addWidget(&m_chartView);

    setLegendShown(true);
    setLegendDetailsShown(true);
}

void ZonePane::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Show_Legend,         "description");
    Icons::defaultIcon(ui->action_Open_Configuration,  "configure");
    Icons::defaultIcon(ui->action_Show_Legend_Details, "view-list-details");
}

void ZonePane::setupMenus()
{
    ui->btnLegend->setDefaultAction(ui->action_Show_Legend);
    ui->btnConfig->setDefaultAction(ui->action_Open_Configuration);
    ui->btnDetails->setDefaultAction(ui->action_Show_Legend_Details);

    ui->action_Show_Legend->setChecked(true);

    paneMenu.addActions({ ui->action_Show_Legend,
                          ui->action_Show_Legend_Details });
    paneMenu.addSeparator();
    paneMenu.addActions({ ui->action_Open_Configuration });

    setupActionContextMenu(paneMenu);
    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(&m_chartView, &ChartViewZoom::customContextMenuRequested, this, &ZonePane::showContextMenu);
}

void ZonePane::setupSignals()
{
    connect(&mainWindow(), &MainWindow::selectedTracksChanged, this, &ZonePane::selectedTracksChanged);
    connect(&mainWindow(), &MainWindow::currentTrackChanged, this, &ZonePane::currentTrackChanged);

    // Row deletion from model
    connect(&app().trackModel(), &TrackModel::rowsAboutToBeRemoved, this, &ZonePane::processRowsAboutToBeRemoved);
    // Model reset
    connect(&app().trackModel(), &TrackModel::modelAboutToBeReset, this, &ZonePane::processModelAboutToBeReset);
    connect(&app().trackModel(), &TrackModel::modelReset, this, &ZonePane::processModelReset);
}

void ZonePane::setupTimers()
{
    m_updateTimer.setSingleShot(true);
    connect(&m_updateTimer, &QTimer::timeout, this, &ZonePane::updateChart);
}

void ZonePane::newConfig()
{
    ChartBase::newConfig();

    refreshChart();
}

void ZonePane::save(QSettings& settings) const
{
    ChartBase::save(settings);

    if (ui != nullptr) {
        SL::Save(settings, "method", ui->method->currentIndex());
        SL::Save(settings, "legendDetails", legendDetailsShown());
    }
}

void ZonePane::load(QSettings& settings)
{
    ChartBase::load(settings);

    if (ui != nullptr) {
        ui->method->setCurrentIndex(SL::Load(settings, "method", 0));
        setLegendDetailsShown(SL::Load(settings, "legendDetails", true));
    }
}

void ZonePane::clearChart()
{
    if (m_pieSeries != nullptr)
        m_pieSeries->clear();

    m_zoneData.clear();

    hideChart();
}

void ZonePane::hideChart()
{
    m_chartView.hide();

    if (ui != nullptr)
        ui->labelStatus->show();
}

void ZonePane::focusIn()
{
    PointSelectPane::focusIn();
}

void ZonePane::showChart()
{
    if (m_pieSeries != nullptr && m_chart != nullptr) {
        m_chartView.show();

        if (ui != nullptr && ui->labelStatus != nullptr)
            ui->labelStatus->hide();
    }
}

void ZonePane::statusMessage(const QString& message, bool mainWindowMsg)
{
    hideChart();

    if (ui != nullptr && ui->labelStatus != nullptr) {
        ui->labelStatus->setText(message);

        if (mainWindowMsg)
            mainWindow().statusMessage(UiType::Info, Pane::name() + tr(": ") + message);

    }
}

QItemSelectionModel* ZonePane::selectionModel() const
{
    if (auto* trackPane = mainWindow().findPane<TrackPane>(); trackPane != nullptr)
        return trackPane->selectionModel();

    return nullptr;
}

ZonePane::ZoneAccum_t& ZonePane::ZoneAccum_t::operator+=(const ZoneAccum_t& rhs)
{
    m_duration      += rhs.m_duration;
    m_length        += rhs.m_length;
    m_wattMsec      += rhs.m_wattMsec;
    m_powerDuration += rhs.m_powerDuration;
    m_hrDuration    += rhs.m_hrDuration;
    m_hrMSec        += rhs.m_hrMSec;
    m_ascent        += rhs.m_ascent;
    m_descent       += rhs.m_descent;

    return *this;
}

void ZonePane::ZoneAccum_t::accumulate(const PowerData& pd, const PointItem* prevPt, const PointItem& pt)
{
    if (prevPt == nullptr)
        return;

    // Accumulate total milliseconds spent in each percentile
    if (prevPt->hasDuration(&pt)) {
        const PointItem::Dur_t mSec = prevPt->duration(pt);
        m_duration += mSec;

        if (prevPt->hasPower(PointItem::Either, &pt)) {
            if (PointItem::Power_t power = prevPt->power(pd, pt); power > 5.0) {
                m_wattMsec      += power * mSec;
                m_powerDuration += mSec;
            }
        }

        if (pt.hasHr()) {
            if (PointItem::Hr_t hr = pt.hr(); hr > 20) {
                m_hrMSec     += hr * mSec;
                m_hrDuration += mSec;
            }
        }
    }

    if (prevPt->hasLength(&pt))
        m_length += prevPt->length(pt);

    if (prevPt->hasVert(&pt)) {
        const PointItem::Ele_t vert = prevPt->vert(pt);
        m_ascent  += (vert > 0.0) ? vert : 0.0;
        m_descent += (vert < 0.0) ? -vert : 0.0;
    }
}

QString ZonePane::ZoneAccum_t::toolTip() const
{
    QString toolTip = QString("<p><table border=0.5 cellspacing=0 cellpadding=2>");

    static const double rateConv = (1.0 / 60.0);

    const auto genHdr = [](ModelType mt, const QString& value) {
        return QString("<tr><td><b>") + TrackModel::mdName(mt) + "</b></td><td>" + value + "</td></tr>";
    };

    if (m_duration > 0)
        toolTip += genHdr(TrackModel::TotalTime, cfgData().unitsDuration(Dur_t::base_type(m_duration * mStonS)));

    if (m_length > 0.0)
        toolTip += genHdr(TrackModel::Length, cfgData().unitsTrkLength(qreal(m_length)));

    if (avgPower() > 0.0)
        toolTip += genHdr(TrackModel::AvgMovPower,  cfgData().unitsPower(qreal(avgPower())));

    if (avgHr() > 0)
        toolTip += genHdr(TrackModel::AvgHR, cfgData().unitsHr(qreal(avgHr() * rateConv)));

    if (m_ascent > 0.0)
        toolTip += genHdr(TrackModel::Ascent, cfgData().unitsClimb(qreal(m_ascent)));

    if (m_descent > 0.0)
        toolTip += genHdr(TrackModel::Descent, cfgData().unitsClimb(qreal(m_descent)));

    toolTip += "</table></p>";

    return toolTip;
}

inline float ZonePane::dataPct(ModelType calcMethod, const PowerData& pd, const PointItem* prevPt, const PointItem& pt, float maxData)
{
    switch (calcMethod) {
    case ZoneModel::MaxHRPct:
        if (pt.hasHr()) // skip if no Hr or no previous point
            if (const float hrPct = float(pt.hr()) / maxData; hrPct > 0.0f)
                return hrPct;
        break;
    case ZoneModel::FTPPct:
        if (prevPt != nullptr && prevPt->hasPower(PointItem::Either, &pt))
            if (PointItem::Power_t power = prevPt->power(pd, pt); power > 0.0)
                if (const auto ftpPct = power / maxData; ftpPct > 0.0f)
                    return float(ftpPct);
    default:
        break;
    }

    return -1.0f;
}

inline int ZonePane::bucket(ModelType calcMethod, const PowerData& pd, const PointItem* prevPt, const PointItem& pt, float maxData)
{
    return int(dataPct(calcMethod, pd, prevPt, pt, maxData) * 100.0f);
}

QString ZonePane::ZoneData_t::label() const
{
    return cfgData().zones.data(ZoneModel::Name, m_idx).toString();
}

QString ZonePane::ZoneData_t::toolTip() const
{
    return QString("<p><b><u><nobr><big>") + label() + "</big></nobr></u></b>" +
            tr(" (zone ") + QString::number(m_idx.row()) + ")" +
            "</p>" +
            m_accum.toolTip();
}

QtCharts::QPieSlice* ZonePane::ZoneData_t::slice(int zoneId, const ZonePane& zonePane) const
{
    if (m_accum.m_duration == 0) // don't add slices without data
        return nullptr;

    auto* slice = new QtCharts::QPieSlice(label(), qreal(m_accum.m_duration));

    slice->setLabelColor(zonePane.m_labelNormal);
    slice->setPen(zonePane.m_majorGridPen);
    slice->setLabelArmLengthFactor(0.08); // try to avoid clipping labels on top or bottom of chart area
    slice->setExplodeDistanceFactor(0.12);
    slice->setColor(cfgData().zones.data(ZoneModel::Color, m_idx, Util::RawDataRole).value<QColor>());
    slice->setProperty(zoneIdProperty, zoneId);

    return slice;
}

float ZonePane::calcMaxData(const QModelIndex& trackIdx)
{
    const TrackModel& trackModel = app().trackModel();

    const QModelIndex personIdx = trackModel.personIdx();
    if (!personIdx.isValid()) {
        statusMessage(tr("No person defined for training zone analysis."));
        return -1.0f;;
    }

    const QString personName = cfgData().people.data(PersonModel::Name, personIdx).toString();

    // TODO: if only one track selected, use date from that track. Else, use
    // current date (or maybe median?)

    // Get Max BPM or FTP for that person
    switch (calcMethod()) {
    case ZoneModel::MaxHRPct:
        if (!trackIdx.isValid()) {
            statusMessage(tr("Invalid track selected."));
            break;
        }

        if (const QVariant maxHrVar = trackModel.data(TrackModel::MaxHR, trackIdx, Util::RawDataRole);
            maxHrVar.isValid() && maxHrVar.toFloat() >= 1.0)
            return maxHrVar.toFloat() * 60.0; // constant because HR is in Hz

        statusMessage(tr("No valid BPM data available for ") + personName);
        break;
    case ZoneModel::FTPPct:
        if (float maxData = float(cfgData().people.ftp(personIdx)); maxData >= 10.0f)
            return maxData;
        statusMessage(tr("No valid FTP data available for ") + personName);
        break;
    default:
        statusMessage(tr("Invalid calculation method."), true);
        break;
    }

    return -1.0f;
}

void ZonePane::selectPointsInZone(const ZoneData_t& zone)
{
    if (m_currentRows.isEmpty())
        return;

    PointSelectPane::clearSelection();

    const ModelType dataColumn = calcMethod();

    const float maxData = calcMaxData(currentTrack());
    if (maxData <= 0.0)
        return;

    PointModel* geoPoints = getGeoPoints();
    if (geoPoints == nullptr)
        return;

    // Select all points in current track which are in the selected zone
    const PowerData& pd = geoPoints->powerData();

    // For performance, we must add everything to a QItemSelection and select it all at once
    QItemSelection selected;

    for (int segId = 0; segId < geoPoints->size(); ++segId) {
        const auto& trkSeg = geoPoints->at(segId);
        const QModelIndex segIdx = geoPoints->index(segId, 0);

        for (int ptId = 1; ptId < trkSeg.size(); ptId++) {
            if (const float pct = dataPct(dataColumn, pd, &trkSeg.at(ptId-1), trkSeg.at(ptId), maxData); zone.inZone(pct)) {
                const QModelIndex ptIdx = geoPoints->index(ptId, 0, segIdx);
                const QModelIndex proxyIdx = PointSelectPane::mapUp(ptIdx);
                selected.select(proxyIdx, proxyIdx);
            }
        }
    }

    PointSelectPane::select(selected);
}

ModelType ZonePane::calcMethod() const
{
    if (ui == nullptr)
        return ModelType(-1);

    switch (ui->method->currentIndex()) {
    case 0:  return ZoneModel::MaxHRPct;
    case 1:  return ZoneModel::FTPPct;
    default: return ModelType(-1);
    }
}

void ZonePane::updateChart()
{
    const TrackModel& trackModel = app().trackModel();

    if (ui == nullptr || m_pieSeries == nullptr || selectionModel() == nullptr)
        return;

    const QModelIndexList rows = Util::MapDown(selectionModel()->selectedRows());

    if (rows == m_currentRows) // nothing to do
        return;

    m_currentRows = rows;

    clearChart(); // clear old chart data in preparation for repopulating

    const ModelType dataColumn = calcMethod();

    if (rows.isEmpty())
        return statusMessage(tr("No selected tracks."));

    // Collect zone model data for rapid access
    m_zoneData.reserve(cfgData().zones.rowCount());

    Util::Recurse(cfgData().zones, [this, dataColumn](const QModelIndex& idx) {
        m_zoneData.append(ZoneData_t(cfgData().zones.data(idx.sibling(idx.row(), dataColumn), Util::RawDataRole).toFloat(),
                                     idx));
        return true;
    });

    if (m_zoneData.size() == 0)
        return statusMessage(tr("No training zone data defined."));

    // Fill in ceilings from next zones
    for (int z = 0; z < (m_zoneData.size() - 1); ++z)
        m_zoneData[z].m_pctCeil = m_zoneData[z+1].m_pctFloor;

    m_zoneData.back().m_pctCeil = 100000.0f; // last bucket extends beyond possible percentages

    // One bucket per percentile, to a max of 400%
    std::array<ZoneAccum_t, 400> buckets;  // percentile buckets

    // Collect data from tracks into percentile sized buckets
    for (const auto& row : rows) {
        const float maxData = calcMaxData(row);
        if (maxData <= 0.0) {
            m_zoneData.clear();
            return;
        }

        if (const PointModel* geoPoints = trackModel.geoPoints(row); geoPoints != nullptr) {
            const PowerData& pd = geoPoints->powerData();

            for (const auto& trkSeg : *geoPoints) {
                const PointItem* prevPt = nullptr;

                for (const auto& pt : trkSeg) {
                    if (const int bucketId = bucket(dataColumn, pd, prevPt, pt, maxData);
                        bucketId >= 0 && bucketId < int(buckets.size())) {
                        buckets.at(bucketId).accumulate(pd, prevPt, pt);
                    }

                    prevPt = &pt;
                }
            }
        }
    }

    // Run through the percentile buckets in order and accumulate data into per-zone data.
    int zone = 0;
    bool haveZoneData = false;
    for (size_t b = 0; b < buckets.size(); ++b) {
        if (buckets.at(b).m_duration > 0) {
            // If bucket percentile exceeds start point of next zone, increment the zone to use
            while (zone < (m_zoneData.size() - 1) && float(b * 0.01f) > m_zoneData.at(zone).m_pctCeil)
                ++zone;

            // Only add it if this bucket meets or exceeds the zone floor percentile
            if (float(b * 0.01f) >= m_zoneData.at(zone).m_pctFloor) {
                m_zoneData[zone].m_accum += buckets.at(b);
                haveZoneData = true;
            }
        }
    }

    if (!haveZoneData)
        return statusMessage(tr("No training zone data in selected tracks."));

    // Find total duration
    PointItem::Dur_t totalDuration = Dur_t(0);
    for (const auto& zone : m_zoneData)
        totalDuration += zone.m_accum.m_duration;

    // Add slices into the pie
    for (int z = 0; z < m_zoneData.size(); ++z) {
        m_zoneData[z].m_fraction = qreal(m_zoneData[z].m_accum.m_duration) / qreal(totalDuration);

        if (QtCharts::QPieSlice* slice = m_zoneData[z].slice(z, *this); slice != nullptr)
            m_pieSeries->append(slice);
    }

    updateLabels();
    showChart();
}

void ZonePane::updateLabels(QtCharts::QPieSlice* activeSlice)
{
    if (m_chart == nullptr || m_pieSeries == nullptr)
        return;

    const auto slices = m_pieSeries->slices();
    const auto markers = m_chart->legend()->markers();

    assert(markers.size() == slices.size());

    // Add duration info to labels
    for (int sliceId = 0; sliceId < slices.size(); ++sliceId) {
        const int zoneId = slices[sliceId]->property(zoneIdProperty).toInt();

        const QString detail = QString("<br/><small>") +
                               QChar(0x29D7) +
                               " " +
                               cfgData().unitsDuration(Dur_t::base_type(m_zoneData.at(zoneId).m_accum.m_duration * mStonS)) +
                               " " +
                               QChar(0x2192) +
                               " " +
                               cfgData().unitsTrkLength(qreal(m_zoneData.at(zoneId).m_accum.m_length)) +
                               "</small>";

        markers[sliceId]->setLabel(QString("<u>") +
                                   (slices.at(sliceId) == activeSlice ? "<b>" : "") +
                                   m_zoneData.at(zoneId).label() +
                                   (slices.at(sliceId) == activeSlice ? "</b>" : "") +
                                   "</u>" +
                                   QString(" (%1%)").arg(m_zoneData.at(zoneId).m_fraction * 100.0, 0, 'f', 0) +
                                   (legendDetailsShown() ? detail : QString()) );

        // TODO: we should add a config setting for this marker color and width
        markers[sliceId]->setPen(QPen(QColor(), 1.9f, Qt::SolidLine));
    }

    // As of Qt 5.15.3, calling setLabel() on an existing marker doesn't update it on screen. This forces a refresh.
    if (legendShown()) {
        setLegendShown(false);
        setLegendShown(true);
    }
}

void ZonePane::showContextMenu(const QPoint& pos)
{
    paneMenu.exec(m_chartView.mapToGlobal(pos));
}

void ZonePane::refreshChart(int timer)
{
    m_currentRows.clear(); // force update
    clearChart();

    if (timer <= 0)
        updateChart();
    else
        m_updateTimer.start(timer);
}

bool ZonePane::legendShown() const
{
    return ui != nullptr && ui->action_Show_Legend->isChecked();
}

void ZonePane::setLegendDetailsShown(bool shown)
{
    if (ui != nullptr)
        ui->action_Show_Legend_Details->setChecked(shown);

    updateLabels();
}

bool ZonePane::legendDetailsShown() const
{
    return ui != nullptr && ui->action_Show_Legend_Details->isChecked();
}

void ZonePane::sliceHovered(QtCharts::QPieSlice* slice, bool state)
{
    if (slice == nullptr)
        return;

    slice->setLabelVisible(state);
    slice->setExploded(state);

    if (state) {
        if (const int zoneId = slice->property(zoneIdProperty).toInt(); validZoneId(zoneId)) {
            m_chart->setToolTip(m_zoneData.at(zoneId).toolTip());
            updateLabels(slice);
        }
    } else {
        m_chart->setToolTip(QString()); // unset tooltip
        updateLabels(nullptr);
    }
}

void ZonePane::sliceDoubleClicked(QtCharts::QPieSlice* slice)
{
    if (const int zoneId = slice->property(zoneIdProperty).toInt(); validZoneId(zoneId))
        selectPointsInZone(m_zoneData.at(slice->property(zoneIdProperty).toInt()));
}

void ZonePane::processRowsAboutToBeRemoved(const QModelIndex& parent, int first, int last)
{
    bool refresh = PointSelectPane::processRowsAboutToBeRemoved(parent, first, last);

    // Search for the range in the currently displayed set of rows, and refresh if we find it.
    for (const auto& row : m_currentRows) {
        if (row.row() >= first && row.row() <= last)
            refresh = true;

        if (refresh)
            break;
    }

    if (refresh)
        refreshChart();
}

void ZonePane::processModelAboutToBeReset()
{
    PointSelectPane::processModelAboutToBeReset();
}

void ZonePane::processModelReset()
{
    refreshChart();
}

void ZonePane::selectedTracksChanged(const QItemSelectionModel*, const QItemSelection&, const QItemSelection&)
{
    using namespace std::chrono_literals;
    m_updateTimer.start(250ms);  // avoid update spam
}

void ZonePane::currentTrackChanged(const QModelIndex& current)
{
    PointSelectPane::currentTrackChanged(current);
}

void ZonePane::on_action_Show_Legend_toggled(bool shown)
{
    setLegendShown(shown);
}

void ZonePane::on_action_Open_Configuration_triggered()
{
    mainWindow().openAppConfig(AppConfig::Page::Zones);
}

void ZonePane::on_method_currentIndexChanged(int)
{
    refreshChart(10);
}

void ZonePane::on_action_Show_Legend_Details_toggled(bool checked)
{
    setLegendDetailsShown(checked);
}
