/*
    Copyright 2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <src/util/util.h>

#include "src/ui/windows/mainwindow.h"
#include "src/ui/panes/pointpane.h"

#include "pointselectpane.h"

PointSelectPane::PointSelectPane(MainWindow& mainWindow) :
    m_mainWindow(mainWindow)
{
}

PointSelectPane::~PointSelectPane()
{
}

QModelIndex PointSelectPane::mapUp(const QModelIndex& idx) const
{
    return Util::MapUp(&m_identityProxy, idx);
}

void PointSelectPane::refreshPointSelection()
{
    PointModel* geoPoints = getGeoPoints();

    // Mirror selection to most recent point pane, if any. Else, select points directly.
    if (auto* pointPane = m_mainWindow.findPane<PointPane>(); pointPane != nullptr)
        pointPane->select(m_pointSelectionModel, QItemSelectionModel::ClearAndSelect);
    else if (geoPoints != nullptr)
        geoPoints->select(m_pointSelectionModel.selectedRows());

    m_pointSelectionSummary.refresh(geoPoints, m_identityProxy, &m_pointSelectionModel);

    // Only set the model if it's changed; otherwise this clears the selections.
    if (geoPoints != m_pointSelectionModel.model()) {
        m_pointSelectionModel.setModel(geoPoints);
        m_identityProxy.setSourceModel(geoPoints);
    }

    m_mainWindow.updateStatus(m_pointSelectionSummary);
    m_mainWindow.selectionChanged(&m_pointSelectionModel, QItemSelection(), QItemSelection());
}

void PointSelectPane::select(const QItemSelection& selected, QItemSelectionModel::SelectionFlags flags)
{
    m_pointSelectionModel.select(selected, flags);
    refreshPointSelection();
}

void PointSelectPane::clearSelection()
{
    m_pointSelectionSummary.clear(0, 0); // Reset selection summary
    m_pointSelectionModel.clear();
}

PointModel* PointSelectPane::getGeoPoints()
{
    return app().trackModel().geoPoints(m_currentTrack);
}

void PointSelectPane::currentTrackChanged(const QModelIndex& current)
{
    if (m_currentTrack == current)
        return;

    m_currentTrack = current;
    clearSelection();
    refreshPointSelection();
}

void PointSelectPane::processModelAboutToBeReset()
{
    currentTrackChanged(QModelIndex());
}

bool PointSelectPane::processRowsAboutToBeRemoved(const QModelIndex&, int first, int last)
{
    if (m_currentTrack.isValid() && m_currentTrack.row() >= first && m_currentTrack.row() <= last) {
        currentTrackChanged(QModelIndex());
        return true;
    }

    return false;
}

void PointSelectPane::focusIn()
{
    refreshPointSelection();
}
