/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <src/util/roles.h>
#include <src/util/util.h>
#include <src/core/removablemodel.h>
#include <src/core/duplicablemodel.h>

#include "src/ui/windows/mainwindow.h"
#include "src/core/mergeablemodel.h"
#include "src/core/app.h"
#include "src/core/simplifiablemodel.h"
#include "src/core/reverseablemodel.h"
#include "src/core/speededitmodel.h"
#include "datacolumnpane.h"

DataColumnPane::DataColumnPane(MainWindow& mainWindow, PaneClass paneClass, QWidget *parent, bool useFlattener) :
    DataColumnPaneBase(mainWindow, PaneClass_t(int(paneClass)), parent, useFlattener)
{
}

// This is needed to make the MOC hapy
DataColumnPane::~DataColumnPane()
{
}

const MainWindow& DataColumnPane::mainWindow() const
{
    return static_cast<const MainWindow&>(DataColumnPaneBase::mainWindow());
}

MainWindow& DataColumnPane::mainWindow()
{
    return static_cast<MainWindow&>(DataColumnPaneBase::mainWindow());
}

// Reject names that match a color-name pattern.  Pass all others.
bool DataColumnPane::noColorNames(const QString& name)
{
    static const QRegularExpression nameRe(PointColorNameRe ".*");

    return !nameRe.match(name).hasMatch();
}

IconSelector* DataColumnPane::iconSelectorFactory() const
{
    return new IconSelector(iconSelectorPaths(), nullptr, noColorNames);
}

// Path for icon selector
const QStringList& DataColumnPane::iconSelectorPaths() const
{
    static const QStringList paths = {":art/tags", "[Points]:art/points", "[US NPS Symbols]:art/us-nps-symbols"};
    return paths;
}

void DataColumnPane::duplicateSelection()
{
    PaneAction<DuplicableModel> action(*this, tr("Duplicate"),
               tr("No items selected to duplicate."));

    if (action.ok()) {
        action.model()->duplicate(action.selections());
        sort();  // resort items in view
    }
}

void DataColumnPane::deleteSelection()
{
    PaneAction<RemovableModel> action(*this, tr("Delete"),
               tr("No items selected to delete."));

    if (action.ok())
        action.model()->remove(action.selections());
}

void DataColumnPane::mergeSelection()
{
    PaneAction<MergeableModel> action(*this, tr("Merge"),
               tr("No items selected to merge."));

    if (!action.ok())
        return;

    const auto [newName, nameOk] = mergedName(action.selections(), action.undoName());

    if (nameOk) {
        const auto [newItem, mergeOk] = action.model()->merge(action.selections(), newName);
        sort();  // resort per active criteria

        if (!mergeOk) {
            action.setError();
            return mainWindow().statusMessage(UiType::Error, std::get<0>(getItemName(action.selections())) + tr(" merge failed"));
        }

        select(newItem, QItemSelectionModel::SelectCurrent | QItemSelectionModel::Rows);
    } else {
        action.setCanceled();
    }
}

void DataColumnPane::simplifySelection()
{
    PaneAction<SimplifiableModel> action(*this, tr("Simplify"),
               tr("No items selected for simplify operation."));

    if (action.ok()) {
        if (mainWindow().getTrackSimplifyDialog().exec(*action.model(), action.selections())
            == QDialog::Accepted)
            mainWindow().expandPointPanes();
        else
            action.setCanceled();
    }
}

void DataColumnPane::reverseSelection()
{
    PaneAction<ReverseableModel> action(*this, tr("Reverse"),
              tr("No items selected for reverse operation."));

    if (action.ok()) {
        action.model()->reverse(action.selections());
        sort();  // resort per active criteria
    }
}

void DataColumnPane::unsetSpeed()
{
    PaneAction<SpeedEditModel> action(*this, tr("Unset Speed"),
              tr("No items selected for Unset Speed operation."));

    if (action.ok())
        action.model()->unsetSpeed(action.selections());
}

std::tuple<QString, bool> DataColumnPane::mergedName(const QModelIndexList&, const QString&) const
{
    return { QString(), false };
}

template<class MODEL>
DataColumnPane::PaneAction<MODEL>::PaneAction(DataColumnPane& dcp,
                                              const QString& actionName,
                                              const QString& warning,
                                              bool reqSelections) :
    m_dcp(dcp),
    m_model(m_dcp.modelAs<MODEL>()),
    m_reqSelections(reqSelections),
    m_selections(m_dcp.getSelections()),
    m_undoName(UndoMgr::genName(actionName, m_model, m_selections)),
    m_undoSet(app().undoMgr(), m_undoName, ok())
{
    if (!ok())
        m_dcp.mainWindow().statusMessage(UiType::Warning, warning);
}

template<class MODEL>
DataColumnPane::PaneAction<MODEL>::~PaneAction<MODEL>()
{
    if (m_canceled)
        m_dcp.mainWindow().statusMessage(UiType::Warning, QObject::tr("Canceled"));
    else if (!m_error)
        m_dcp.mainWindow().statusMessage(UiType::Success, m_undoName);
}
