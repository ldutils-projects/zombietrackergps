/*
    Copyright 2019-2021 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QApplication>
#include "chartbase.h"
#include "src/ui/windows/mainwindow.h"

ChartBase::ChartBase(MainWindow& mainWindow, PaneClass pc, QWidget *parent) :
    Pane(mainWindow, pc, parent),
    m_chart(nullptr),
    m_chartView(nullptr),
    m_labelHighlight(QApplication::palette().highlight().color()),
    m_labelNormal(QApplication::palette().text().color()),
    m_labelSelected(QApplication::palette().link().color()),
    m_majorGridPen(QApplication::palette().light(), 2, Qt::SolidLine),
    m_minorGridPen(QApplication::palette().midlight(), 1, Qt::DashLine)
{
}

ChartBase::~ChartBase()
{
}

void ChartBase::setupChart(QtCharts::QChart* chart)
{
    m_chart = (chart != nullptr) ? chart : new QtCharts::QChart();

    m_chartView.setChart(m_chart);  // takes ownership of chart
    m_chartView.setRenderHint(QPainter::Antialiasing);

    m_chart->setBackgroundVisible(true);
    m_chart->setPlotAreaBackgroundVisible(true);
    m_chart->setPlotAreaBackgroundBrush(QApplication::palette().base());
    m_chart->setBackgroundBrush(QApplication::palette().window());

    m_chart->setMargins(QMargins(0,0,0,0));
    m_chart->legend()->setContentsMargins(0,0,0,0);
    m_chart->legend()->setAlignment(Qt::AlignTop);
    m_chart->legend()->setLabelColor(m_labelNormal);

    disableToolTipsFor(&m_chartView);
}

void ChartBase::setAxesShown(bool shown) const
{
    if (m_chart == nullptr)
        return;

    for (auto& yAxis : m_chart->axes())
        yAxis->setVisible(shown);
}

void ChartBase::setLegendShown(bool shown) const
{
    if (m_chart == nullptr)
        return;

    m_chart->legend()->setVisible(shown);
}

QtCharts::QAbstractAxis* ChartBase::axisX(const QtCharts::QChart* chart, QtCharts::QAbstractSeries* series)
{
    const auto axes = chart->axes(Qt::Horizontal, series);
    return axes.empty() ? nullptr : axes.first();
}

void ChartBase::save(QSettings& settings) const
{
    Pane::save(settings);

    SL::Save(settings, "showAxes",   axesShown());
    SL::Save(settings, "showLegend", legendShown());
}

void ChartBase::load(QSettings& settings)
{
    Pane::load(settings);

    setAxesShown(SL::Load(settings, "showAxes", true));
    setLegendShown(SL::Load(settings, "showLegend", true));
}
