/*
    Copyright 2021-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CLIMBANALYSISPANE_H
#define CLIMBANALYSISPANE_H

#include <QIdentityProxyModel>
#include <QItemSelectionModel>
#include <QModelIndex>

#include "src/core/selectionsummary.h"
#include "src/ui/panes/pointselectpane.h"

#include "datacolumnpane.h"

namespace Ui {
class ClimbAnalysisPane;
} // namespace Ui

class MainWindow;
class QModelIndex;
class ClimbModel;

class ClimbAnalysisPane final :
        public DataColumnPane,
        public PointSelectPane,
        public NamedItem
{
    Q_OBJECT

public:
    explicit ClimbAnalysisPane(MainWindow&, QWidget *parent = nullptr);
    ~ClimbAnalysisPane() override;

    void newConfig() override;

private slots:
    void processRowsAboutToBeRemoved(const QModelIndex&, int first, int last);
    void processModelAboutToBeReset();
    void on_ClimbAnalysisPane_toggled(bool checked) { paneToggled(checked); }
    void zoomToSelection() override;
    void doubleClicked(const QModelIndex&);
    void showContextMenu(const QPoint&);
    void filterTextChanged(const QString&) override;
    void selectionChanged();
    void currentTrackChanged(const QModelIndex&);

private:
    void setupActionIcons();
    void setupContextMenus();
    void setupSignals();
    void gotoSelection(const QModelIndexList&) const;
    void focusIn() override;
    bool hasAction(MainAction) const override;
    void deferredUpdate(const QModelIndex&);
    const DefColumns& defColumnView() const override;

    TreeModel* model() override;
    const TreeModel* model() const override;

    Ui::ClimbAnalysisPane* ui;
};

#endif // CLIMBANALYSISPANE_H
