/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TRACKPANE_H
#define TRACKPANE_H

#include <src/ui/widgets/colordelegate.h>
#include <src/ui/widgets/comboboxdelegate.h>
#include "mapdatapane.h"

namespace Ui {
class TrackPane;
} // namespace Ui

class TrackModel;

class TrackPane final : public MapDataPane, public NamedItem
{
    Q_OBJECT

public:
    explicit TrackPane(MainWindow&, QWidget *parent = nullptr);
    ~TrackPane() override;

    bool hasAction(MainAction) const override;
    void addFilterInteractive();

public slots:
    void updateVisibility() override;
    void resizeToFit(int defer = -1) override;

private slots:
    void on_TrackPane_toggled(bool checked) { paneToggled(checked); }
    void showContextMenu(const QPoint&);
    void on_action_Reset_Track_Color_triggered();
    void on_action_Reset_Track_Note_triggered();
    void on_action_Set_Icon_triggered();
    void on_action_Edit_triggered();
    void filterTextChanged(const QString&) override;
    void processSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected);
    void processSelectedTracksChanged();
    void on_action_Change_Tags_triggered();
    void on_action_Select_Duplicates_triggered();
    void on_action_Select_All_Duplicates_triggered();
    void on_action_Create_Filter_from_Query_triggered();
    void on_action_Unset_Icon_triggered();

private:
    friend class TestZtgps;

    // *** begin Settings API
    using MapDataPane::save;
    void load(QSettings&) override;
    // *** end Settings API

    void setupActionIcons();
    void setupContextMenus();
    void setupDelegates();
    void setupSignals();
    void dragEnterEvent(QDragEnterEvent*) override;
    void dropEvent(QDropEvent*) override;

    void newConfig() override;

    const DefColumns& defColumnView() const override;

    // Candidate name for data merging
    std::tuple<QString, bool> mergedName(const QModelIndexList&, const QString& undoName) const override;

    // Obtain model associated with pane
    TreeModel* model() override;
    const TreeModel* model() const override;

    Ui::TrackPane*      ui;
    ColorDelegate       m_colorDelegate;
    ComboBoxDelegate    m_trackTypeDelegate;
    TrackModel&         m_model;
};

#endif // TRACKPANE_H
