/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef WAYPOINTPANE_H
#define WAYPOINTPANE_H

#include "mapdatapane.h"

namespace Ui {
class WaypointPane;
} // namespace Ui

class WaypointModel;

class WaypointPane :
        public MapDataPane,
        public NamedItem
{
    Q_OBJECT

public:
    explicit WaypointPane(MainWindow& mainWindow, QWidget *parent = nullptr);
    ~WaypointPane() override;

    bool hasAction(MainAction) const override;

    static const QStringList& iconSelectorPathsStatic();
    static std::tuple<const QString, const QString> getItemNameStatic();

public slots:
    void updateVisibility() override;

private slots:
    void on_WaypointPane_toggled(bool checked) { paneToggled(checked); }
    void showContextMenu(const QPoint&);
    void on_action_Reset_Waypoint_Note_triggered();
    void on_action_Set_Icon_triggered();
    void on_action_Edit_triggered();
    void filterTextChanged(const QString&) override;
    void processSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected);
    void processSelectedWaypointsChanged();
    void on_action_Change_Tags_triggered();
    void on_action_Guess_Icon_triggered();
    void on_action_Select_Duplicates_triggered();
    void on_action_Select_All_Duplicates_triggered();
    void on_action_Unset_Icon_triggered();
    void on_action_New_Waypoint_triggered();

private:
    void setupActionIcons();
    void setupContextMenus();
    void setupDelegates();
    void setupSignals();

    void newConfig() override;

    const DefColumns& defColumnView() const override;

    const QStringList& iconSelectorPaths() const override; // subclasses can override

    // Obtain model associated with pane
    TreeModel* model() override;
    const TreeModel* model() const override;

    Ui::WaypointPane*   ui;
    WaypointModel&      m_model;
};

#endif // WAYPOINTPANE_H
