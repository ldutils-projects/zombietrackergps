/*
    Copyright 2021-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <tuple>
#include <chrono>

#include <QInputDialog>
#include <QScrollBar>
#include <QScrollArea>
#include <QToolTip>
#include <QCursor>
#include <QFont>
#include <QFontMetrics>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QSignalBlocker>
#include <QStackedBarSeries>
#include <QBarSeries>
#include <QBarSet>
#include <QBarCategoryAxis>
#include <QValueAxis>

#include <src/util/util.h>
#include <src/util/ui.h>
#include <src/util/icons.h>
#include <src/util/units.h>
#include <src/core/modelmetadata.inl.h>
#include <src/ui/widgets/checklistitemdelegate.h>

#include "src/ui/windows/mainwindow.h"
#include "src/ui/panes/trackpane.h"
#include "src/core/trackmodel.h"

#include "activitysummarypane.h"
#include "ui_activitysummarypane.h"

ActivitySummaryPane::ActivitySummaryPane(MainWindow& mainWindow, QWidget *parent) :
    BarChartBase(mainWindow, PaneClass::ActivitySummary, parent),
    ui(new Ui::ActivitySummaryPane),
    m_refreshSizeTimer(this),
    m_axisYL(new QtCharts::QValueAxis()),
    m_axisYR(new QtCharts::QValueAxis()),
    m_axisX(new QtCharts::QBarCategoryAxis()),
    m_dateSpan(Span::Month),
    m_hoveredBarSet(nullptr),
    m_hoveredIndex(-1),
    m_prevHoveredBarSet(nullptr),
    m_prevHoveredIndex(-1)
{
    ui->setupUi(this);

    setupActionIcons();
    setPaneFilterBar(ui->filterCtrl, ui->graphData);
    setupDataSelector();
    setupSpanSelector();
    setupChart();
    setupTimers();
    setupSignals();
    setupMenus();
    setupCompleter();
    setupFilterStatusIcons();
    setupDefaults();
    Util::SetupWhatsThis(this);
}

ActivitySummaryPane::~ActivitySummaryPane()
{
    // We don't delete the axes, since the chart owns them (we just track a pointer).

    if (ui != nullptr)
        ui->graphScroll->takeWidget();  // We own the ChartView, the scroll doesn't.

    deleteUI(ui);
}

void ActivitySummaryPane::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Show_Axes,         "labplot-axis-vertical");
    Icons::defaultIcon(ui->action_Show_Legend,       "description");
    Icons::defaultIcon(ui->action_Animated,          "motion_path_animations");
    Icons::defaultIcon(ui->action_Set_Bar_Width,     "resizerow");
    Icons::defaultIcon(ui->action_Show_Bar_Values,   "format-precision-more");
    Icons::defaultIcon(ui->action_Page_Left,         "arrow-left-double");
    Icons::defaultIcon(ui->action_Page_Right,        "arrow-right-double");
    Icons::defaultIcon(ui->action_Show_Empty_Spans,  "gnumeric-column-hide");
    Icons::defaultIcon(ui->action_Zoom_to_Tracks,    "zoom-fit-selection");
    Icons::defaultIcon(ui->action_Zero_Based_Y_Axis, "labplot-axis-vertical");
}

void ActivitySummaryPane::setupTimers()
{
    m_refreshSizeTimer.setSingleShot(true);
    connect(&m_refreshSizeTimer, &QTimer::timeout, this, &ActivitySummaryPane::updateChartSize);
}

void ActivitySummaryPane::setupSpanSelector()
{
    // Add possible spans: Week, Month, and Year
    for (Span span = Span::Week; span != Span::_Count; Util::inc(span)) {
        QString name;
        switch (span) {
        case Span::Week:   name = tr("Week");  break;
        case Span::Month:  name = tr("Month"); break;
        case Span::Year:   name = tr("Year");  break;
        case Span::_Count: name = tr(""); assert(0); break;
        }

        auto* item = new QStandardItem(name);
        item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

        m_spanDataModel.appendRow(item);
    }

    ui->dateSpan->setModel(&m_spanDataModel);
    ui->dateSpan->setItemDelegate(new ChecklistItemDelegate(ui->dateSpan));
    ui->dateSpan->setMaxVisibleItems(int(Span::Year) - int(Span::Week) + 1);

    connect(ui->dateSpan, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this, [this](int span) { setDateSpan(Span(span)); });
}

void ActivitySummaryPane::setupChart()
{
    BarChartBase::setupChart();

    if (m_chart == nullptr)
        return;

    m_chart->setAnimationDuration(500);
    m_chart->legend()->setVisible(true);
    m_chart->legend()->setAlignment(Qt::AlignBottom);

    m_axisYL->setLabelsColor(m_labelNormal);
    m_axisYL->setGridLinePen(m_majorGridPen);
    m_axisYL->setMinorGridLinePen(m_minorGridPen);

    m_axisYR->setLabelsColor(m_labelNormal);
    m_axisYR->setGridLineVisible(false);
    m_axisYR->setMinorGridLineVisible(false);

    m_axisX->setLabelsColor(m_labelNormal);
    m_axisX->setGridLinePen(m_majorGridPen);
    m_axisX->setVisible(true);

    m_chart->addAxis(m_axisYL, Qt::AlignLeft);
    m_chart->addAxis(m_axisYR, Qt::AlignRight);
    m_chart->addAxis(m_axisX, Qt::AlignBottom);

    ui->graphScroll->setWidget(&m_chartView);
    setFocusProxy(ui->graphScroll);
}

void ActivitySummaryPane::setupMenus()
{
    for (auto* action : {
         ui->action_Page_Left,
         ui->action_Page_Right,
         ui->action_Show_Axes,
         ui->action_Show_Bar_Values,
         ui->action_Show_Empty_Spans,
         ui->action_Show_Legend,
         ui->action_Zoom_to_Tracks })
    {
        action->setShortcutContext(Qt::WidgetWithChildrenShortcut);
        addAction(action);
    }

    paneMenu.addActions({ ui->action_Page_Left,
                          ui->action_Page_Right,
                          ui->action_Zoom_to_Tracks });
    paneMenu.addSeparator();

    paneMenu.addActions({ ui->action_Animated,
                          ui->action_Show_Bar_Values,
                          ui->action_Show_Axes,
                          ui->action_Show_Legend,
                          ui->action_Show_Empty_Spans,
                          ui->action_Zero_Based_Y_Axis,
                          ui->action_Set_Bar_Width });

    setupActionContextMenu(paneMenu);
    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(&m_chartView, &ChartViewZoom::customContextMenuRequested, this, &ActivitySummaryPane::showContextMenu);
}

void ActivitySummaryPane::setupSignals()
{
    BarChartBase::setupSignals();
}

void ActivitySummaryPane::setupDefaults()
{
    setLockToQuery(false);
    setPlotColumn(0);         // default: must happen after setupDataSelector()
    setDateSpan(Span::Month); // default: must happen after setupSpanSelector();
}

int ActivitySummaryPane::barWidthForLabels() const
{
    // If not showing axes, use user-set bar width.
    if (!axesShown())
        return m_barWidth;

    // Otherwise, use max of user width and size based on font and label contents.
    QDateTime sampleDate;
    sampleDate.setDate(QDate(2088, 3, 30));

    const int labelBarWidth = fontMetrics().boundingRect(spanName(sampleDate)).width() +
            fontMetrics().boundingRect('M').width() * 2;

    return std::max(m_barWidth, labelBarWidth);
}

bool ActivitySummaryPane::axesShown() const
{
    return ui != nullptr && ui->action_Show_Axes->isChecked();
}

bool ActivitySummaryPane::legendShown() const
{
    return ui != nullptr && ui->action_Show_Legend->isChecked();
}

void ActivitySummaryPane::setDateSpan(Span span)
{
    m_dateSpan = span;
    ui->dateSpan->setCurrentIndex(int(span));

    refreshChart(10);
}

QString ActivitySummaryPane::getToolTip(int index, QtCharts::QBarSet* barSet) const
{
    if (m_barSeries == nullptr || m_axisX == nullptr || m_axisX->count() <= index)
        return "";

    const Units& yAxisUnits = TrackModel::mdUnits(m_plotColumn);

    // Awkwardness: We store nS internally, but have converted to hours for duration formats
    const double timeConvert = (yAxisUnits.isDateFormat() ? 3.6e12 : 1.0);
    const double unitsYMul = yAxisUnits.fromDouble(1.0) * timeConvert; // units multiplier for Units conversion

    QString toolTip = QString("<p><b><u><nobr><big>") +
                      m_axisX->at(index) +
                      "</big></nobr></u></b></p><p>" +
                      "<table border=0.5 cellspacing=0 cellpadding=2>";

    qreal valTotal = 0.0;
    int   countTotal = 0;
    qreal lengthTotal = 0.0;
    qint64 movingTotal = 0;

    static const QChar markerChar = QChar(0x25CF); // unicode circle

    const int em = fontMetrics().boundingRect(markerChar).width();

    const auto genHdr = [](ModelType mt) {
        return QString("<th align=\"right\">") + TrackModel::mdName(mt) + "</th>";
    };

    const auto genRow = [](ModelType mt, qreal value, bool isBold, bool isItalic) {
        const QString bold       = (isBold ? "<b>" : "");
        const QString unbold     = (isBold ? "</b>" : "");
        const QString italic     = (isItalic ? "<i>" : "");
        const QString unitalic   = (isItalic ? "</i>" : "");

        return QString("<td align=\"right\">") +
                bold + italic + TrackModel::mdUnits(mt)(value) + unitalic + unbold + "</td>";
    };

    // Create header
    {
        toolTip += QString("<tr><th></th><th align=\"left\">Tag</th>") + genHdr(m_plotColumn);
        if (m_plotColumn != TrackModel::Length)
            toolTip += genHdr(TrackModel::Length);

        if (m_plotColumn != TrackModel::MovingTime)
            toolTip += genHdr(TrackModel::MovingTime);

        toolTip += "<td align=\"right\">#</tr>";
    }

    // Top down, to match bar visual order
    for (int b = m_barSeries->count() - 1; b >= 0; --b) {
        const QtCharts::QBarSet* barN = m_barSeries->barSets().at(b);

        // Don't add items with no data
        // TODO: use initial seed value
        if ((*barN)[index] != 0.0) {
            const QString bold       = (barN == barSet ? "<b>" : "");
            const QString unbold     = (barN == barSet ? "</b>" : "");
            const QString activeMark = (barN == barSet ? QChar(markerChar) : ' ');
            const QString tag        = barN->label();

            // For whatever reason, QBarSet::color() is not marked as a const method, so we must cast it.
            const QColor  barColor  = const_cast<QtCharts::QBarSet*>(barN)->color();
            const QColor& cellColor = barColor;
            const int     count     = m_barData[tag][index].m_trackCount;
            const qreal   length    = m_barData[tag][index].m_length;
            const qint64  moving    = m_barData[tag][index].m_movingTime;

            toolTip += QString("<tr><td align=\"center\" width=") +
                       QString::number(em * 2) +
                       " bgcolor=\"" +
                       cellColor.name() +
                       "\">" + activeMark + "</td><td>" +
                       bold + tag + unbold + "</td>";

            toolTip += genRow(m_plotColumn, (*barN)[index] * unitsYMul, barN == barSet, false);

            if (m_plotColumn != TrackModel::Length)
                toolTip += genRow(TrackModel::Length, length, barN == barSet, false);

            if (m_plotColumn != TrackModel::MovingTime)
                toolTip += genRow(TrackModel::MovingTime, moving, barN == barSet, false);

            toolTip += "<td align=\"right\">" + bold + QString::number(count) + unbold + "</tr>";

            valTotal    += (*barN)[index];
            countTotal  += count;
            lengthTotal += length;
            movingTotal += moving;
        }
    }

    // Add total, if needed
    if (isStackedSeries()) {
        toolTip +="<tr><td>+</td><td><i>Total</i></td>";
        toolTip += genRow(m_plotColumn, valTotal * unitsYMul, false, true);

        if (m_plotColumn != TrackModel::Length)
            toolTip += genRow(TrackModel::Length, lengthTotal, false, true);

        if (m_plotColumn != TrackModel::MovingTime)
            toolTip += genRow(TrackModel::MovingTime, movingTotal, false, true);

        toolTip += "<td align=\"right\"><i>" + QString::number(countTotal) + "</i></tr>";
    }

    toolTip += "</table></p>";

    return toolTip;
}

void ActivitySummaryPane::hovered(bool status, int index, QtCharts::QBarSet* barSet)
{
    if (!status) {
        m_chart->setToolTip(QString()); // unset tooltip
    } else {
        m_chart->setToolTip(getToolTip(index, barSet));

        m_hoveredBarSet = barSet;
        m_hoveredIndex  = index;
    }

    updateActions();
}

void ActivitySummaryPane::selectTracks(int index, QtCharts::QBarSet* barSet, bool allTags, bool select, bool zoom)
{
    // Verify track pane
    auto* trackPane = mainWindow().findPane<TrackPane>();
    if (trackPane == nullptr || barSet == nullptr || index < 0)
        return;

    const QString tag = barSet->label(); // tag of interest

    // Search for bar we clicked in the date ranges. We must find it and our index to proceed.
    if (index >= m_barDateRanges.size())
        return;

    const TrackModel& model     = app().trackModel();
    const QDateTime   beginDate = m_barDateRanges.at(index);
    const QDateTime   endDate   = nextDateTime(beginDate);

    QModelIndexList selection; // what to select

    // Find matching indexes
    const int modelRowCount = model.rowCount();
    for (int row = 0; row < modelRowCount; ++row) {
        const QModelIndex dateIdx   = model.index(row, TrackModel::BeginDate);
        const QModelIndex tagIdx    = model.index(row, TrackModel::Tags);
        const QDateTime   trackDate = model.data(dateIdx, Util::RawDataRole).toDateTime();
        const QStringList tags      = model.data(tagIdx, Util::RawDataRole).toStringList();

        if (beginDate <= trackDate && trackDate < endDate && (allTags || tags.contains(tag)))
            selection.append(dateIdx);
    }

    // Select them in the model
    if (select)
        trackPane->select(selection, QItemSelectionModel::Select | QItemSelectionModel::Rows | QItemSelectionModel::Clear);

    if (zoom)
        trackPane->gotoSelection(selection);
}

void ActivitySummaryPane::clicked(int index, QtCharts::QBarSet* barSet)
{
    selectTracks(index, barSet, bool(app().keyboardModifiers() & Qt::ShiftModifier), true, false);
}

void ActivitySummaryPane::doubleClicked(int index, QtCharts::QBarSet* barSet)
{
    selectTracks(index, barSet, bool(app().keyboardModifiers() & Qt::ShiftModifier), true, true);
}

void ActivitySummaryPane::on_lockToQuery_toggled(bool checked)
{
    setLockToQuery(checked);
}

void ActivitySummaryPane::on_action_Set_Bar_Width_triggered()
{
    const int val = QInputDialog::getInt(this, tr("Set Bar Width"), tr("Minimum width (px)"), m_barWidth, 4, 256, 1);

    setBarWidth(val);
}

void ActivitySummaryPane::on_action_Show_Bar_Values_triggered(bool checked)
{
    setBarValuesShown(checked);
}

void ActivitySummaryPane::on_action_Page_Left_triggered()
{
    ui->graphScroll->horizontalScrollBar()->triggerAction(QScrollBar::SliderPageStepSub);
    updateActions();
}

void ActivitySummaryPane::on_action_Page_Right_triggered()
{
    ui->graphScroll->horizontalScrollBar()->triggerAction(QScrollBar::SliderPageStepAdd);
    updateActions();
}

void ActivitySummaryPane::on_action_Show_Axes_triggered(bool shown) const
{
    setAxesShown(shown);
}

void ActivitySummaryPane::on_action_Show_Legend_triggered(bool shown) const
{
    setLegendShown(shown);
}

void ActivitySummaryPane::on_action_Show_Empty_Spans_triggered(bool shown)
{
    setShowEmptySpans(shown);
}


void ActivitySummaryPane::on_ActivitySummaryPane_toggled(bool checked)
{
    paneToggled(checked);
    refreshChartSize(10);
}

void ActivitySummaryPane::on_action_Zoom_to_Tracks_triggered()
{
    // We must do this because activating the context menu un-hovers.
    QtCharts::QBarSet* hoveredBarSet = m_hoveredBarSet;
    int                hoveredIndex  = m_hoveredIndex;

    if (m_hoveredBarSet == nullptr) {
        hoveredBarSet = m_prevHoveredBarSet;
        hoveredIndex  = m_prevHoveredIndex;
    }

    selectTracks(hoveredIndex, hoveredBarSet, bool(app().keyboardModifiers() & Qt::ShiftModifier), false, true);
}

void ActivitySummaryPane::on_action_Zero_Based_Y_Axis_triggered(bool checked)
{
    setZeroBasedYAxis(checked);
}

void ActivitySummaryPane::refreshChartSize(int delayMs)
{
    if (delayMs > 0)
        m_refreshSizeTimer.start(delayMs);
    else
        updateChartSize();
}

qreal ActivitySummaryPane::spanInitValue() const
{
    return TrackModel::mdAccumInit(m_plotColumn).toDouble();
}

QDateTime ActivitySummaryPane::getMinDateTime(int row) const
{
    const TrackModel& model = app().trackModel();

    const QModelIndex firstTrackDateIndex = model.index(row, TrackModel::BeginDate);

    if (!firstTrackDateIndex.isValid())  // bad date for some reason
        return { };

    QDateTime firstTrackDateTime = model.data(firstTrackDateIndex, Util::RawDataRole).toDateTime();

    if (!firstTrackDateTime.isValid())  // bad date for some reason
        return { };

    // Calculate begin date, as rounded down to midnight on starting week/month/year
    firstTrackDateTime.setTime(QTime(0, 0, 0));
    QDate firstTrackDate = firstTrackDateTime.date();

    switch (m_dateSpan) {
    case Span::Week:
        firstTrackDate = firstTrackDate.addDays(-firstTrackDate.dayOfWeek() + 1);  // prior monday
        break;
    case Span::Month:
        firstTrackDate.setDate(firstTrackDate.year(), firstTrackDate.month(), 1); // 1st of month
        break;
    case Span::Year:
        firstTrackDate.setDate(firstTrackDate.year(), 1, 1);  // jan 1st of year
        break;
    default:
        assert(0); break;
    }

    firstTrackDateTime.setDate(firstTrackDate);

    return firstTrackDateTime;
}

QDateTime ActivitySummaryPane::nextDateTime(const QDateTime& dateTime) const
{
    switch (m_dateSpan) {
    case Span::Week:  return dateTime.addDays(7);
    case Span::Month: return dateTime.addMonths(1);
    case Span::Year:  return dateTime.addYears(1);
    default: assert(0); return { };
    }
}

QString ActivitySummaryPane::spanName(const QDateTime& dateTime) const
{
    Units weekUnits(Format::DateddMMMyyyy);
    Units monthUnits(Format::DateMMMyyyy);
    Units yearUnits(Format::Dateyyyy);

    switch (m_dateSpan) {
    case Span::Week:  return weekUnits.toString(dateTime);
    case Span::Month: return monthUnits.toString(dateTime);
    case Span::Year:  return yearUnits.toString(dateTime);
    default: assert(0); return "";
    }
}

void ActivitySummaryPane::clearChart()
{
    if (m_barSeries != nullptr) {
        m_barSeries->clear();
        // Clear old graph
        for (auto* axis : m_barSeries->attachedAxes())
            m_barSeries->detachAxis(axis);
    }

    m_barData.clear();
    m_axisYL->setRange(0.0, 0.0);
    m_axisYR->setRange(0.0, 0.0);
    m_axisX->clear();
    m_barDateRanges.clear();
    m_hoveredBarSet = nullptr;
    m_prevHoveredBarSet = nullptr;
}

bool ActivitySummaryPane::isStackedSeries() const
{
    return TrackModel::mdAccumSummed(m_plotColumn);
}

void ActivitySummaryPane::newSeries()
{
    m_chart->removeAllSeries(); // This deletes the series, so we no longer own it.

    // Create new series of proper type (stacked or side by side)
    if (isStackedSeries())
        m_barSeries = new QtCharts::QStackedBarSeries();
    else
        m_barSeries = new QtCharts::QBarSeries();

    m_barSeries->setUseOpenGL(true); // not yet supported for bar charts, but someday?
    m_barSeries->setLabelsVisible(false);
    m_barSeries->setLabelsPosition(QtCharts::QAbstractBarSeries::LabelsInsideEnd);
    m_barSeries->setBarWidth(float(std::clamp(cfgData().asBarWidth, 5, 100)) / 100.0);

    m_chart->addSeries(m_barSeries);

    connect(m_barSeries, &QtCharts::QAbstractBarSeries::hovered, this, &ActivitySummaryPane::hovered);
    connect(m_barSeries, &QtCharts::QAbstractBarSeries::clicked, this, &ActivitySummaryPane::clicked);
    connect(m_barSeries, &QtCharts::QAbstractBarSeries::doubleClicked, this, &ActivitySummaryPane::doubleClicked);
}

void ActivitySummaryPane::updateChart()
{
    const TrackModel& model = app().trackModel();

    if (ui == nullptr)
        return;

    clearChart();
    newSeries();

    m_chart->setAnimationOptions(ui->action_Animated->isChecked() ? QtCharts::QChart::SeriesAnimations :
                                                                    QtCharts::QChart::NoAnimation);
    m_barSeries->setLabelsVisible(ui->action_Show_Bar_Values->isChecked());

    const int modelRowCount = model.rowCount();
    if (modelRowCount == 0 || m_plotColumn < 0)
        return;

    QVector<int> rows; // this will be a date sorted list of rows in the TrackModel
    rows.reserve(modelRowCount);

    // Sort rows by date
    {
        // Add rows that match our query, and also have a valid BeginDate and plot column
        for (int row = 0; row < modelRowCount; ++row)
            if (m_queryRoot->match(model, QModelIndex(), row) &&
                model.data(model.index(row, m_plotColumn), Util::RawDataRole).isValid() &&
                model.data(model.index(row, TrackModel::BeginDate), Util::RawDataRole).toDateTime().isValid())
                rows.append(row);

        // Sort them by increasing date
        std::sort(rows.begin(), rows.end(), [&model](const int r0, const int r1) {
            const QModelIndex lhs     = model.index(r0, TrackModel::BeginDate);
            const QModelIndex rhs     = model.index(r1, TrackModel::BeginDate);
            const QDateTime   lhsData = model.data(lhs, Util::RawDataRole).toDateTime();
            const QDateTime   rhsData = model.data(rhs, Util::RawDataRole).toDateTime();

            return lhsData < rhsData;
        });
    }

    m_chart->resize(QSizeF(m_chart->size().width(), size().height()));

    // Reset selection summary
    m_selectionSummary.clear(model.rowCount(), rows.size());

    if (rows.size() == 0)  // ensure we have at least one entry
        return;

    QDateTime spanStartDateTime = getMinDateTime(rows.at(0));
    if (!spanStartDateTime.isValid())  // bad date for some reason
        return;

    QMap<QString, QtCharts::QBarSet*> barSets; // our barsets
    QStringList xAxisLabels; // much faster than repeatedly adding to an QBarCategoryAxis!

    // much faster than repeatedly editing a QBarSet!
    // The tuple holds the accumulation values, and the number of track entries for each
    QMap<QString, QList<qreal>> barValues;
    xAxisLabels.reserve(128);
    m_barDateRanges.reserve(128);

    const double unitsYMul = TrackModel::mdUnits(m_plotColumn).toDouble(1.0);

    // Accumulate over date spans
    for (const auto row : rows) {
        const QModelIndex plotIdx   = model.index(row, m_plotColumn);
        const QModelIndex tagsIdx   = model.index(row, TrackModel::Tags);
        const QModelIndex dateIdx   = model.index(row, TrackModel::BeginDate);
        const QModelIndex lengthIdx = model.index(row, TrackModel::Length);
        const QModelIndex movingIdx = model.index(row, TrackModel::MovingTime);

        const qreal       value   = model.data(plotIdx,   Util::RawDataRole).toDouble();
        const QStringList tags    = model.data(tagsIdx,   Util::RawDataRole).toStringList();
        const QDateTime   date    = model.data(dateIdx,   Util::RawDataRole).toDateTime();
        const qreal       length  = model.data(lengthIdx, Util::RawDataRole).toDouble();
        const qint64      moving  = model.data(movingIdx, Util::RawDataRole).toLongLong();

        // TODO: use first power tag from TrackItem? Need to pass through from TrackModel
        const QString     tag     = tags.isEmpty() ? tr("N/A") : tags.front();

        // Create a QBarSet for this tag if we haven't already
        if (!barValues.contains(tag)) {
            QList<qreal> values;
            auto* barSet = new QtCharts::QBarSet(tag);
            setBarColorFromModel(model, barSet, row);

            // Expand to be as big as the others we've already added.  We must test for
            // emptiness before adding our new QBarSet below.
            if (!barValues.isEmpty()) {
                const int count = barValues.first().count();
                values.reserve(std::max(count, 128));
                m_barData[tag].resize(count);

                for (int x = 0; x < count; ++x)
                    values.append(spanInitValue());
            }

            m_barData[tag];               // create if it doesn't exist
            barValues[tag].swap(values);  // do this after isEmpty test above

            barSets.insert(tag, barSet);  // add the new QBarSet to the map
        }

        // We've finished one span: add data for next span across all tag indices
        // Use a while loop in case the data skips multiple spans.
        while (date >= spanStartDateTime) {
            const QDateTime spanNextDateTime = nextDateTime(spanStartDateTime);

            if (ui->action_Show_Empty_Spans->isChecked() || date < spanNextDateTime) {
                for (auto& barVal : barValues)
                    barVal.append(spanInitValue());

                for (auto& barData : m_barData)
                    barData.append(BarData());

                m_barDateRanges.append(spanStartDateTime);
                xAxisLabels.append(spanName(spanStartDateTime));

                // Limit number of rows per config setting
                while (m_barDateRanges.count() > cfgData().asMaxDateSpans) {
                    for (auto& barVal : barValues)
                        barVal.pop_front();

                    for (auto& barData : m_barData)
                        barData.pop_front();

                    m_barDateRanges.pop_front();
                    xAxisLabels.pop_front();
                }
            }

            // Next span time
            spanStartDateTime = spanNextDateTime;
        }

        QList<qreal>&     barVal    = barValues[tag]; // we know we have one, since we added it up above
        QVector<BarData>& barData   = m_barData[tag]; // for creating the tooltip

        const int valCount   = barVal.count();   // get number of entries, since we'll modify the most recent one.
        [[maybe_unused]] const int trkCount   = barData.count();
        [[maybe_unused]] const int dateCount  = m_barDateRanges.count();
        [[maybe_unused]] const int xAxisCount = xAxisLabels.count();
        assert(valCount > 0 && valCount == trkCount && valCount == dateCount && valCount == xAxisCount);

        const qreal unitsVal = value * unitsYMul;

        // accumulate
        barVal[valCount - 1] = TrackModel::mdAccum(m_plotColumn, barVal[valCount - 1], unitsVal).toDouble();

        barData[valCount - 1].m_trackCount++;
        barData[valCount - 1].m_length += length;
        barData[valCount - 1].m_movingTime += moving;

        m_selectionSummary.accumulate(model, plotIdx, 1); // for status update
    }

    qreal minVal = 1e38;  // for axis min/max range
    qreal maxVal = -1e38;

    // Build QBarSets from value lists, and find numeric bounds
    {
        // Finalize values
        int count = 0;
        for (auto keyIt = barValues.keyBegin(); keyIt != barValues.keyEnd(); ++keyIt) {
            count = barValues[*keyIt].size();
            for (int x = 0; x < count; ++x)
                barValues[*keyIt][x] = TrackModel::mdAccumFinal(m_plotColumn, barValues[*keyIt][x], m_barData[*keyIt][x].m_trackCount).toDouble();
        }

        // Find min/max for each tag's bar
        const bool stacked = isStackedSeries();
        for (int x = 0; x < count; ++x) {
            qreal setSum = 0.0;
            for (auto& bv : barValues) {
                setSum = (stacked ? setSum : 0) + bv[x];

                if (!stacked) {
                    minVal = std::min(minVal, setSum);
                    maxVal = std::max(maxVal, setSum);
                }
            }

            if (stacked) {
                minVal = std::min(minVal, setSum);
                maxVal = std::max(maxVal, setSum);
            }
        }

        // Add values to the bar sets
        for (auto it = barValues.begin(); it != barValues.end(); ++it)
            barSets[it.key()]->append(it.value());

        // This is unfortunately slow in Qt if there are many bars.
        m_axisX->setCategories(xAxisLabels);
    }

    // Set axis values
    {
        const Units& yAxisUnits = TrackModel::mdUnits(m_plotColumn);

        int tickCount = 2; // TODO: react to height
        double niceMin = minVal, niceMax = maxVal;

        // Optionally use zero based aaxis
        if (ui->action_Zero_Based_Y_Axis->isChecked()) {
            if (niceMax < 0.0)
                niceMax = 0.0;
            else
                niceMin = 0.0;
        }

        looseNiceNumbers(niceMin, niceMax, tickCount);

        for (auto* yAxis : { m_axisYL, m_axisYR }) {
            yAxis->setTickCount(tickCount);
            yAxis->setMinorTickCount(4);
            yAxis->setRange(niceMin, niceMax); // do this last: it seems to be better that way for Qt perf
            yAxis->setLabelFormat(yAxisUnits.isDateFormat() ? yAxisUnits.dateFormat()
                                                            : yAxisUnits.chartLabelFormat(maxVal));
            m_barSeries->attachAxis(yAxis); // put Y axis back
        }

        m_barSeries->attachAxis(m_axisX);  // put X axis back
    }

    // Add all bar sets to the graph.  The barSeries will own the barSets.  The barSets map above holds
    // a pointer to the QBarSets, so it's OK for it to run its destructor.  Ownership is with the series now.
    for (QtCharts::QBarSet* barSet : barSets)
        m_barSeries->append(barSet);

    updateChartSize(); // Set size of chart to keep bars the same width no matter how many there are (there's a scroll area)

    // Scroll to most recent entries
    QScrollBar* hScroll = ui->graphScroll->horizontalScrollBar();
    hScroll->setSliderPosition(hScroll->maximum());

    mainWindow().updateStatus(m_selectionSummary);
    updateActions();
}

void ActivitySummaryPane::updateChartSize() const
{
    const int width = std::max(std::max(4, m_axisX->count()) * barWidthForLabels(), 25) +
                      (m_chart->size().width() - m_chart->plotArea().width());

    const bool needHScroll = width >= ui->graphScroll->contentsRect().width();

    // account for scroll bar in our height resize, if it's needed.
    const int height = ui->graphScroll->contentsRect().height() -
                       (needHScroll ? ui->graphScroll->horizontalScrollBar()->height() : 0);

    ui->graphScroll->widget()->resize(width, height);
    m_chart->resize(ui->graphScroll->widget()->size());
}

void ActivitySummaryPane::resizeEvent(QResizeEvent* event)
{
    BarChartBase::resizeEvent(event);

    refreshChartSize();
}

void ActivitySummaryPane::wheelEvent(QWheelEvent* event)
{
    // Zoom bars on control wheel
    if (bool(event->modifiers() & Qt::ControlModifier)) {
        setBarWidth(barWidthForLabels() + 2 * (event->angleDelta().y() > 0 ? 1 : -1));
        event->accept();
        return;
    }

    // Move the chart horizontally with wheel events.
    // TODO: there must be a better way to do this, but I couldn't find a scroll area or bar method
    // to use with connect() to turn normal wheel Y axis events to X axis scrolling. This kludge is
    // to do that.
    if (event->angleDelta().y() != 0 && event->modifiers() == Qt::NoModifier) {
        if (event->angleDelta().y() > 0)
            ui->graphScroll->horizontalScrollBar()->triggerAction(QScrollBar::SliderSingleStepAdd);
        else
            ui->graphScroll->horizontalScrollBar()->triggerAction(QScrollBar::SliderSingleStepSub);
        event->accept();
        return;
    }

    BarChartBase::wheelEvent(event);
}

void ActivitySummaryPane::updateActions()
{
    const QScrollBar* hScroller = ui->graphScroll->horizontalScrollBar();

    ui->action_Zoom_to_Tracks->setEnabled(m_hoveredBarSet != nullptr);
    ui->action_Page_Left->setEnabled(hScroller->sliderPosition() > hScroller->minimum());
    ui->action_Page_Right->setEnabled(hScroller->sliderPosition() < hScroller->maximum());

    m_prevHoveredBarSet = m_hoveredBarSet;
    m_prevHoveredIndex  = m_hoveredIndex;
}

void ActivitySummaryPane::setShowEmptySpans(bool shown)
{
    if (ui == nullptr)
        return;

    ui->action_Show_Empty_Spans->setChecked(shown);
    refreshChart(10);
}

void ActivitySummaryPane::setZeroBasedYAxis(bool checked)
{
    if (ui == nullptr)
        return;

    ui->action_Zero_Based_Y_Axis->setChecked(checked);
    refreshChart(10);
}

void ActivitySummaryPane::setAxesShown(bool shown) const
{
    BarChartBase::setAxesShown(shown);
    ui->action_Show_Axes->setChecked(shown);

    m_axisYL->setVisible(shown);
    m_axisYR->setVisible(shown);
    m_axisX->setVisible(shown);

    updateChartSize();
}

void ActivitySummaryPane::setLegendShown(bool shown) const
{
    BarChartBase::setLegendShown(shown);
    ui->action_Show_Legend->setChecked(shown);

    m_chart->legend()->setVisible(shown);

    updateChartSize();
}

void ActivitySummaryPane::setBarValuesShown(bool show)
{
    if (ui == nullptr)
        return;

    ui->action_Show_Bar_Values->setChecked(show);
    refreshChart(10);
}

void ActivitySummaryPane::save(QSettings& settings) const
{
    BarChartBase::save(settings);

    if (ui != nullptr) {
        MemberSave(settings, ui->trackQuery);
        MemberSave(settings, ui->graphData);

        SL::Save(settings, "dateSpan", std::underlying_type<Span>::type(m_dateSpan));
        SL::Save(settings, "lockToTrackQuery", ui->lockToQuery->isChecked());
        SL::Save(settings, "animated", ui->action_Animated->isChecked());
        SL::Save(settings, "barValues", ui->action_Show_Bar_Values->isChecked());
        SL::Save(settings, "showAxes", axesShown());
        SL::Save(settings, "showLegend", legendShown());
        SL::Save(settings, "showEmptySpans", ui->action_Show_Empty_Spans->isChecked());
        SL::Save(settings, "zeroBasedYAxis", ui->action_Zero_Based_Y_Axis->isChecked());
    }
}

void ActivitySummaryPane::load(QSettings& settings)
{
    const QSignalBlocker block(ui->trackQuery);

    BarChartBase::load(settings);

    if (ui != nullptr) {
        MemberLoad(settings, ui->trackQuery);
        MemberLoad(settings, ui->graphData);
        setQuery(ui->trackQuery->text());
        setPlotColumn(ui->graphData->currentIndex());

        setDateSpan(Span(SL::Load(settings, "dateSpan", std::underlying_type<Span>::type(Span::Month))));
        setLockToQuery(SL::Load(settings, "lockToTrackQuery", false));
        ui->action_Animated->setChecked(SL::Load(settings, "animated", true));
        setBarValuesShown(SL::Load(settings, "barValues", false));
        setAxesShown(SL::Load(settings, "showAxes", true));
        setLegendShown(SL::Load(settings, "showLegend", true));
        setShowEmptySpans(SL::Load(settings, "showEmptySpans", false));
        setZeroBasedYAxis(SL::Load(settings, "zeroBasedYAxis", false));
    }
}
