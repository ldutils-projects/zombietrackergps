/*
    Copyright 2021-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DATESUMMARYPANE_H
#define DATESUMMARYPANE_H

#include <QDateTime>
#include <QMap>

#include "barchartbase.h"

namespace Ui {
class ActivitySummaryPane;
} // namespace Ui

class ActivitySummaryPane final : public BarChartBase
{
    Q_OBJECT

public:
    explicit ActivitySummaryPane(MainWindow&, QWidget *parent = nullptr);
    ~ActivitySummaryPane() override;

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

    void setAxesShown(bool shown) const override;
    void setLegendShown(bool /*shown*/) const override;
    void setBarValuesShown(bool show);

    bool axesShown() const override;
    bool legendShown() const override;

private:
    // Ranges to summarize activity over. Must be in ui->dateSpan index order for the moment.
    enum Span {
        Week,
        Month,
        Year,
        _Count,
    };

private slots:
    void setDateSpan(ActivitySummaryPane::Span);
    void on_ActivitySummaryPane_toggled(bool checked);
    void hovered(bool status, int index, QtCharts::QBarSet*);
    void clicked(int index, QtCharts::QBarSet*);
    void doubleClicked(int index, QtCharts::QBarSet*);
    void on_lockToQuery_toggled(bool checked);
    void on_action_Set_Bar_Width_triggered();
    void on_action_Page_Left_triggered();
    void on_action_Page_Right_triggered();
    void on_action_Show_Axes_triggered(bool shown) const;
    void on_action_Show_Bar_Values_triggered(bool checked);
    void on_action_Show_Legend_triggered(bool shown) const;
    void on_action_Show_Empty_Spans_triggered(bool shown);
    void on_action_Zoom_to_Tracks_triggered();
    void on_action_Zero_Based_Y_Axis_triggered(bool checked);

protected:
    virtual QString getToolTip(int index, QtCharts::QBarSet*) const;

private:
    friend class TestZtgps; // test hook

    void setupActionIcons();
    void setupTimers();
    void setupSpanSelector();
    void setupChart();
    void setupMenus();
    void setupSignals();
    void setupDefaults();
    void updateChart() override;
    void clearChart() override;
    void refreshChartSize(int delayMs = 500); // 0 to avoid deferring
    void resizeEvent(QResizeEvent*) override;
    void wheelEvent(QWheelEvent*) override;
    void setShowEmptySpans(bool);
    void setZeroBasedYAxis(bool);
    int  barWidthForLabels() const;
    void updateChartSize() const;
    void updateActions() override;
    void selectTracks(int index, QtCharts::QBarSet*, bool allTags, bool select, bool zoom);
    void newSeries();
    bool isStackedSeries() const;

    QDateTime getMinDateTime(int row) const;             // obtain chart min date, rounded to week/month/year
    QDateTime nextDateTime(const QDateTime&) const;      // increment to next week/month/year
    qreal     spanInitValue() const;                     // return span start value before accumulation
    QString   spanName(const QDateTime&) const;          // span name, for bar chart axis labels

    Ui::ActivitySummaryPane*        ui;
    QStandardItemModel              m_spanDataModel;     // for date span combo box
    QTimer                          m_refreshSizeTimer;  // avoid size refresh spam

    QtCharts::QValueAxis*           m_axisYL;            // Y value axis, left
    QtCharts::QValueAxis*           m_axisYR;            // Y value axis, right
    QtCharts::QBarCategoryAxis*     m_axisX;             // X category axis

    // Vector of begin dates, so we can select tracks by clicking on the bar.
    // Single click selects all matching tag, double click selects all in the date range.
    QVector<QDateTime>              m_barDateRanges;

    // This structure holds data used to generate the tooltip from a bar and index
    struct BarData {
        qreal  m_length     = 0;
        qint64 m_movingTime = 0;
        int    m_trackCount = 0;
    };

    QMap<QString, QVector<BarData>> m_barData;           // per-tag, per-index data used to create the tooltip

    Span                            m_dateSpan;          // date span from enum above
    QtCharts::QBarSet*              m_hoveredBarSet;     // for zoom menu
    int                             m_hoveredIndex;      // ...
    QtCharts::QBarSet*              m_prevHoveredBarSet; // for zoom menu
    int                             m_prevHoveredIndex;  // ...
};

#endif // DATESUMMARYPANE_H
