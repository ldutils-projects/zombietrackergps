/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAPDATAPANE_INL_H
#define MAPDATAPANE_INL_H

#include "src/ui/windows/mainwindow.h"
#include "mapdatapane.h"

template <class MODEL>
void MapDataPane::updateVisibilityImpl()
{
    // Let the MapPane know which are visible in our view.
    MODEL* model = modelAs<MODEL>();

    // Only do this if we're the most recently focused track pane, to avoid multiple
    // track panes fighting for whose visibility will win.  This also helps loading behavior,
    // so that later loaded panes don't stop on earlier loaded panes's selections.
    if (!m_mostRecentFocus || model == nullptr)
        return;

    model->setAllVisible(false);

    Util::Recurse(topFilter(), [this, model](const QModelIndex& idx) {
        model->setVisible(Util::MapDown(idx), true);
        return true;
    });

    m_selectionSummary.refresh(model, topFilter(), selectionModel());
    mainWindow().updateStatus(m_selectionSummary);
    sort(); // resort our own view
}

#endif // MAPDATAPANE_INL_H
