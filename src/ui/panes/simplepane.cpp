/*
    Copyright 2021-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "simplepane.h"
#include "ui_simplepane.h"

#include <QTimer>
#include <src/util/icons.h>
#include <src/util/ui.h>

#include "src/ui/panes/pane.h"
#include "src/ui/windows/mainwindow.h"
#include "src/core/app.h"

static const char* initText =
R"INIT(<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body>
<table border="0" style=" border-style:none; margin-top:2px; margin-bottom:2px; margin-left:2px; margin-right:2px;" align="center" cellspacing="0" cellpadding="2">
<tr>
<td width="48%">
<p align="center">${Tags}</p></td>
<td width="48%">
<p align="center">${Flags}</p></td></tr></table>
<p style="-qt-paragraph-type:empty;  "><br /></p>
<table border="1" style=" margin-top:2px; margin-bottom:2px; margin-left:2px; margin-right:2px;" align="center" cellspacing="0" cellpadding="2">
<tr>
<td colspan="2">
<p align="center"><span style="font-size:12pt;"><b>${Name}</b></span></p></td></tr>
<tr>
<td width="48%">
<p align="center"><b>Length</b><br/>${Length}</p></td>
<td width="48%">
<p align="center"><b>Ascent</b><br/>${Ascent}</p></td></tr>
<tr>
<td>
<p align="center"><b>Moving Time</b><br/>${Moving_Time}</p></td>
<td>
<p align="center"><b>Moving Speed</b><br/>${Moving_Spd}</p></td></tr>
<tr>
<td colspan="2">
<p align="center"><span style="font-size:12pt;"><b>Exercise</b></span></p></td></tr>
<tr>
<td>
<p align="center"><b>Moving Power</b><br/>${Moving_Pow}</p></td>
<td>
<p align="center"><b>Max Power</b><br/>${Max_Pow}</p></td></tr>
<tr>
<td>
<p align="center"><b>Energy</b><br/>${Energy}</p></td>
<td>
<p align="center"><b>Moving Cad</b><br/>${Moving_Cad}</p></td></tr>
<tr>
<td>
<p align="center"><b>Avg HR</b><br/>${Avg_HR} ${Avg_HR_%}</p></td>
<td>
<p align="center"><b>Max HR</b><br/>${Max_HR} ${Max_HR_%}</p></td></tr>
</table></body></html>
)INIT";

SimplePane::SimplePane(MainWindow& mainWindow, QWidget *parent) :
    Pane(mainWindow, PaneClass::SimpleView, parent),
    ui(new Ui::SimplePane),
    m_textEditor(app().getModel(App::Model::Track), true, parent),
    m_rawText(initText)
{
    ui->setupUi(this);

    setupContextMenus();
    setupActionIcons();
    setupSignals();
    Util::SetupWhatsThis(this);
}

SimplePane::~SimplePane()
{
    deleteUI(ui);
}

void SimplePane::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Configure_Display, "document-edit");
}

void SimplePane::setupContextMenus()
{
    paneMenu.addActions({ ui->action_Configure_Display });
    setupActionContextMenu(paneMenu);

    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(this, &SimplePane::customContextMenuRequested, this, &SimplePane::showContextMenu);
}

void SimplePane::setupSignals()
{
    // Notify about track changes
    connect(&mainWindow(), &MainWindow::currentTrackChanged, this, &SimplePane::currentTrackChanged);

    // Row deletion from model
    connect(&app().trackModel(), &TrackModel::rowsAboutToBeRemoved, this, &SimplePane::processRowsAboutToBeRemoved);
    // Model reset
    connect(&app().trackModel(), &TrackModel::modelAboutToBeReset, this, &SimplePane::processModelAboutToBeReset);

    // Handle changes in text to display
    connect(&m_textEditor, &ModelTextEditDialog::textApplied, this, &SimplePane::applyText);
}

void SimplePane::showContextMenu(const QPoint& pos)
{
    paneMenu.exec(mapToGlobal(pos));
}

void SimplePane::currentTrackChanged(const QModelIndex& current)
{
    m_currentTrackIdx = current;
    updateVars();
}

void SimplePane::processRowsAboutToBeRemoved(const QModelIndex& parent, int first, int last)
{
    if (!m_currentTrackIdx.isValid())
        return;

    if (m_currentTrackIdx.parent() == parent && m_currentTrackIdx.row() >= first && m_currentTrackIdx.row() <= last)
        m_currentTrackIdx = QModelIndex();

    updateVars();
}

void SimplePane::processModelAboutToBeReset()
{
    m_currentTrackIdx = QModelIndex();
    updateVars();
}

void SimplePane::updateVars()
{
    if (!m_currentTrackIdx.isValid() || ui == nullptr) {
        if (ui != nullptr)
            ui->html->clear();
        return;
    }

    ui->html->setText(m_varExpander.expandHtml(m_rawText, m_currentTrackIdx));
}

void SimplePane::applyText()
{
    m_rawText = m_textEditor.toHtml();
    updateVars();
}

void SimplePane::save(QSettings& settings) const
{
    Pane::save(settings);

    SL::Save(settings, "rawText", m_rawText);
    SL::Save(settings, "editorState", m_textEditor);
}

void SimplePane::load(QSettings& settings)
{
    Pane::load(settings);

    SL::Load(settings, "rawText", m_rawText);
    SL::Load(settings, "editorState", m_textEditor);
    updateVars();
}

void SimplePane::on_action_Configure_Display_triggered()
{
    m_textEditor.setHtml(m_rawText);

    if (m_textEditor.exec() != QDialog::Accepted) {
        mainWindow().statusMessage(UiType::Warning, tr("Canceled"));
        return;
    }

    applyText();
}
