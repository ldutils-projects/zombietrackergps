/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <algorithm>
#include <QGuiApplication>
#include <QClipboard>
#include <QList>
#include <QSignalBlocker>
#include <QMimeType>
#include <QMimeData>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QFontMetrics>
#include <QTreeView>
#include <marble/AbstractFloatItem.h>
#include <marble/GeoDataCoordinates.h>
#include <marble/MarbleWidgetPopupMenu.h>

#include <src/core/app.h>
#include <src/core/treemodel.h>
#include <src/util/roles.h>
#include <src/util/icons.h>
#include <src/util/util.h>
#include <src/util/ui.h>
#include <src/undo/undomgr.h>
#include <src/ui/widgets/checklistitemdelegate.h>

#include "src/core/viewmodel.h"
#include "src/ui/windows/mainwindow.h"
#include "src/ui/widgets/trackmap.h"
#include "src/ui/widgets/distancespinbox.h"
#include "src/ui/widgets/geoloccompleter.h"
#include "src/ui/dialogs/newviewdialog.h"
#include "mappane.h"
#include "ui_mappane.h"

Q_DECLARE_METATYPE(Marble::MapQuality)

MapPane::MapPane(MainWindow& mainWindow, QWidget* parent) :
    Pane(mainWindow, PaneClass::Map, parent),
    ui(new Ui::MapPane),
    m_undoMgrView(25, 1L<<20, UndoMgr::ModType::Modifying),
    m_geoLocFilter(app().geoLocModel()),
    m_mapWidget(new TrackMap(mainWindow, *this)),
    m_mapThemeDialog(nullptr),
    m_filterFeatureMask(GeoLocFilter::filterMaskDefault),
    m_filterFeatureMenu(tr("Feature Settings"))
{
    ui->setupUi(this);
    setPaneFilterBar(ui->filterCtrl);

    // Set view stack handling
    undoMgrView().setMiddleMode(UndoMgr::MiddleMode::XferAndSkip);

    setupMapWidget();
    setupFilterDistance();
    setupFilterFeature();
    setupFilterMode();
    setupSignals();
    setupCompleter();
    setupContextMenus();
    setupActionIcons();
    refreshMenus();
    Util::SetupWhatsThis(this);

    setOfflineMode(mainWindow.isOfflineMode()); // initialize to current offline mode
}

MapPane::~MapPane()
{
    delete m_mapThemeDialog;
    deleteUI(ui);
}

void MapPane::setupActionIcons()
{
    if (ui == nullptr)
        return;

    Icons::defaultIcon(ui->action_Show_Overview,         "application-x-marble");
    Icons::defaultIcon(ui->action_Show_ScaleBar,         "tool-measure");
    Icons::defaultIcon(ui->action_Show_Compass,          "compass");
    Icons::defaultIcon(ui->action_Orient_to_North,       "arrow-up");
    Icons::defaultIcon(ui->action_Location_to_Clipboard, "edit-copy");
    Icons::defaultIcon(ui->action_Add_View_Preset,       "mark-location-symbolic");

    Icons::defaultIcon(ui->action_Feature_Select_All,    "edit-select-all");
    Icons::defaultIcon(ui->action_Feature_Select_None,   "edit-select-none");
    Icons::defaultIcon(ui->action_Feature_Toggle,        "edit-select-invert");
    Icons::defaultIcon(ui->action_Feature_Default,       "kt-restore-defaults");
    Icons::defaultIcon(ui->action_Create_Waypoint,       "mark-location-symbolic");
}

void MapPane::setupMapWidget()
{
    if (m_mapWidget == nullptr)
        return;

    static const QString defaultThemeId = "earth/openstreetmap/openstreetmap.dgml";

    const Marble::GeoDataCoordinates home(-100.0, 40.0, 0.0, Marble::GeoDataCoordinates::Degree);

    m_mapWidget->setMapThemeId(defaultThemeId);
    m_mapWidget->centerOn(home);
    m_mapWidget->setShowAtmosphere(true);
    m_mapWidget->setShowBorders(true);
    m_mapWidget->setShowCompass(false);
    m_mapWidget->setShowCities(false);
    m_mapWidget->setShowCrosshairs(false);
    m_mapWidget->setShowGrid(true);
    m_mapWidget->setShowOtherPlaces(true);
    m_mapWidget->setShowOverviewMap(true);
    m_mapWidget->setShowPlaces(true);
    m_mapWidget->setShowRelief(true);
    m_mapWidget->setShowSunShading(false);
    m_mapWidget->setShowScaleBar(true);
    m_mapWidget->setShowTerrain(true);
    m_mapWidget->setProjection(Marble::Spherical);
    m_mapWidget->setMapQualityForViewContext(Marble::PrintQuality, Marble::Still);
    m_mapWidget->setAnimationsEnabled(false);

    for (Marble::AbstractFloatItem* floatItem : m_mapWidget->floatItems()) {
        if (floatItem == nullptr)
            continue;

        // TODO: get size from preferences
        if (floatItem->nameId() == "compass")
            floatItem->setContentSize(QSize(75, 75));

        if (floatItem->nameId() == "license" ||
            floatItem->nameId() == "navigation")
            floatItem->hide();
    }

    // add mapp widget to layout
    ui->verticalLayout->addWidget(m_mapWidget);
}

void MapPane::setupFilterDistance()
{
    m_filterDistance = new DistanceSpinBox();
    if (m_filterDistance == nullptr)
        return;

    m_filterDistance->setObjectName("distanceSpinBox");

    ui->filterCtrl->insertWidget(3, m_filterDistance, 0);

    m_filterDistance->setToolTip(tr("<html><body>The maximum distance from the center of the view "
                                    "to search for matches.</body></html>"));
    m_filterDistance->setWhatsThis(m_filterDistance->toolTip());
}

void MapPane::setupFilterMode()
{
    auto* model = ui->filterMode->model();

    const QFontMetrics fontMetric(QApplication::font());
    const int iconSize = fontMetric.lineSpacing() * 12 / 10;

    ui->filterMode->setIconSize(QSize(iconSize, iconSize));

    for (FilterMode f = FilterMode::_First; f < FilterMode::_Count; Util::inc(f)) {
        const char* iconName = nullptr;

        switch (f) {
        case World:          iconName = ":art/tags/Misc/Earth-02.svg"; break;
        case VisibleArea:    iconName = ":art/tags/Misc/Magnifier.svg"; break;
        case Distance:       iconName = ":art/tags/Misc/Distance.svg"; break;
        case GeoPolCountry:  iconName = ":art/tags/Flags/Organizations/Political/United_Nations.jpg"; break;
        case GeoPolRegion:   iconName = ":art/tags/Misc/Map.svg"; break;
        case _Count:         break;
        }

        if (iconName != nullptr) {
            model->setData(model->index(int(f), 0), QIcon(iconName), Qt::DecorationRole);
        }
    }
}

void MapPane::setupFilterFeature()
{
    m_filterFeatureModel.appendRow({new QStandardItem(tr("Features")),
                                    new QStandardItem() });

    const QFontMetrics fontMetric(QApplication::font());
    const int iconSize = fontMetric.lineSpacing() * 14 / 10;

    for (GeoLocEntry::Feature f = GeoLocEntry::Feature::_First; f < GeoLocEntry::Feature::_Count; Util::inc(f)) {
        if (f != GeoLocEntry::Feature::None) {
            auto* item = new QStandardItem(GeoLocEntry::featureName(f));

            item->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
            item->setCheckState(bool(m_filterFeatureMask & uint32_t(f)) ? Qt::Checked : Qt::Unchecked);
            item->setData(GeoLocEntry::tooltip(f), Qt::ToolTipRole);
            item->setData(GeoLocEntry::whatsthis(f), Qt::WhatsThisRole);

            auto* icon = new QStandardItem();
            icon->setSelectable(false);

            if (const char* iconName = GeoLocEntry::featureIcon(f); iconName != nullptr)
                icon->setData(QIcon(iconName), Qt::DecorationRole);

            m_filterFeatureModel.appendRow({item, icon});
        }
    }

    auto* treeView = new QTreeView(ui->filterFeature);
    treeView->header()->hide();
    treeView->setRootIsDecorated(false);
    treeView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->filterFeature->setView(treeView);

    ui->filterFeature->setIconSize(QSize(iconSize, iconSize));
    ui->filterFeature->setModel(&m_filterFeatureModel);
    ui->filterFeature->setItemDelegate(new ChecklistItemDelegate(ui->filterFeature));

    const int minWidth = ui->filterFeature->minimumSizeHint().width() + 25 + iconSize; // TODO: kludge to account for checkbox
    ui->filterFeature->view()->setMinimumWidth(minWidth);

    // Update filter when feature selection changes
    connect(&m_filterFeatureModel, &QStandardItemModel::itemChanged, this, &MapPane::toggleFilterFeature);

    // Update filter when distance changes
    connect(m_filterDistance, static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), this, &MapPane::filterDistChanged);
}

void MapPane::setupSignals()
{
    if (m_mapWidget == nullptr)
        return;

    connect(m_mapWidget, &TrackMap::mouseMoveGeoCoords, &mainWindow(), &MainWindow::setGeoPositionStatus);

    // Notify main window when our view undo manager adds or applies an undo.
    connect(&undoMgrView(), &UndoMgr::undoAdded, &mainWindow(),
            static_cast<void(MainWindow::*)(void)>(&MainWindow::updateActions));

    // Update geoloc filter when view changes
    connect(m_mapWidget, &TrackMap::viewMoveIdle, this, &MapPane::viewMoveIdle);

    // Pass signal through.  A little inefficient but it doesn't really matter.
    connect(m_mapWidget, &TrackMap::visibleLatLonAltBoxChanged, this, &MapPane::handleMapAreaChanged);
}

// Annoyingly, the map widget has its own menu class and does not accept QMenus.  We add the parent menus
// directly instead of in sub-menus.
void MapPane::setupContextMenus()
{
    if (ui == nullptr || m_mapWidget == nullptr)
        return;

    const auto addSep = []() {
        auto* sep = new QAction();
        sep->setSeparator(true);
        return sep;
    };

    setContextMenuPolicy(Qt::CustomContextMenu);

    Marble::MarbleWidgetPopupMenu* menu = m_mapWidget->popupMenu();

    menu->addAction(Qt::RightButton, ui->action_Orient_to_North);
    menu->addAction(Qt::RightButton, ui->action_Add_View_Preset);
    menu->addAction(Qt::RightButton, ui->action_Create_Waypoint);
    menu->addAction(Qt::RightButton, ui->action_Location_to_Clipboard);
    menu->addAction(Qt::RightButton, addSep());

    // Unfortunately, we can only add actions, not menus, to the MarbleWidgetPopupMenu.  This leaves us
    // with a redundant layer of menu "Pane/Pane", but there's little I can find to do about that, so we'll
    // just have to live with it for now.
    auto* paneMenu       = new QMenu("Pane");
    auto* paneMenuAction = new QAction("Pane");

    setupPaneContextMenu(*paneMenu);
    paneMenuAction->setMenu(paneMenu);
    menu->addAction(Qt::RightButton, paneMenuAction);

    // Filter feature context menus
    m_filterFeatureMenu.addActions({ ui->action_Feature_Select_All,
                                     ui->action_Feature_Select_None,
                                     ui->action_Feature_Toggle,
                                     ui->action_Feature_Default,
                                   });

    ui->filterFeature->setContextMenuPolicy(Qt::CustomContextMenu);

    connect(ui->filterFeature, &QComboBox::customContextMenuRequested, this, &MapPane::showFilterContextMenu);
    connect(&m_filterFeatureMenu, &QMenu::triggered, this, &MapPane::execFilterFeatureMenu);
}

void MapPane::setupCompleter()
{
    ui->filterMap->setCompleter(new GeoLocCompleter(&m_geoLocFilter, this));

    // The QAbstractItemView::activated signal doesn't work for completers.
    connect(ui->filterMap->completer()->popup(), &QAbstractItemView::clicked,
            this, &MapPane::filterNameActivated);
}

void MapPane::toggleFilterFeature(QStandardItem* item)
{
    const uint32_t bit = 1U << item->index().row();

    if (item->checkState() == Qt::Checked)
        m_filterFeatureMask |= bit;
    else
        m_filterFeatureMask &= ~bit;

    updateFilter();
}

void MapPane::refreshMenus()
{
    if (ui == nullptr || m_mapWidget == nullptr)
        return;

    ui->action_Show_Compass->setChecked(m_mapWidget->showCompass());
    ui->action_Show_Overview->setChecked(m_mapWidget->showOverviewMap());
    ui->action_Show_ScaleBar->setChecked(m_mapWidget->showScaleBar());
}

void MapPane::showContextMenu(const QPoint& pos)
{
    if (m_mapWidget == nullptr)
        return;

    paneMenu.exec(mapToGlobal(m_menuPos = pos));
}

void MapPane::newConfig()
{
    // TODO: filter case sensitivity
    m_mapWidget->newConfig();

    if (m_filterDistance != nullptr)
        m_filterDistance->setSuffix(QString(" ") + cfgData().unitsTrkLength.suffix(100.0));
}

// This is a workaround for a map widget defect which fails to apply higher render quality sometimes.
void MapPane::mapZoomWorkaround()
{
    m_mapWidget->zoomOut();
    m_mapWidget->zoomIn();
}

void MapPane::postLoadHook()
{
    Pane::postLoadHook();

    undoMgrView().clear();  // empty out the undos.
}

void MapPane::gotoView(const ViewParams& vp, const QString& name)
{
    if (m_mapWidget == nullptr)
        return;

    m_mapWidget->gotoView(vp);

    mapZoomWorkaround();
    mainWindow().statusMessage(UiType::Info, tr("Showing: ") + name);
}

// Zoom to a view preset
void MapPane::gotoView(const QModelIndex& idx)
{
    if (!idx.isValid())
        return;

    const auto& views = app().viewModel();
    const auto name   = views.as<QString>(idx, ViewModel::Name);

    gotoView(views.viewParams(idx), name);
}

void MapPane::zoomTo(const Marble::GeoDataLatLonBox& extent)
{
    if (m_mapWidget == nullptr)
        return;

//    For unknown reasons, this test fails (false positives) on OpenSUSE
//    if (extent.isEmpty())  // unset.
//        return;

    if (extent.isNull()) {  // singularity
        m_mapWidget->centerOn(extent.center());
    } else {
        m_mapWidget->centerOn(extent);
    }

    mapZoomWorkaround();
}

void MapPane::zoomTo(const Marble::GeoDataCoordinates& coords)
{
    if (m_mapWidget == nullptr)
        return;

    m_mapWidget->centerOn(coords);
    mapZoomWorkaround();
}

void MapPane::zoomTo(const PointItem& point)
{
    if (m_mapWidget == nullptr)
        return;

    m_mapWidget->centerOn(point.as<Marble::GeoDataCoordinates>());

    mapZoomWorkaround();
}

void MapPane::save(QSettings& settings) const
{
    if (ui == nullptr || m_mapWidget == nullptr)
        return;

    Pane::save(settings);

    // TODO: add first class support for hash save/load in settings.h
    // Serialize floating item state for the map (size/colors of overview map, etc)
    settings.beginWriteArray("floatItem"); {
        int itemNo = 0;
        for (const Marble::AbstractFloatItem* floatItem : m_mapWidget->floatItems()) {
            if (floatItem == nullptr)
                continue;

            settings.setArrayIndex(itemNo++);
            settings.setValue("floatItemNameId", floatItem->nameId());
            settings.setValue("floatItemSettings", floatItem->settings());
            settings.setValue("floatItemVisible", floatItem->visible());
        }
    } settings.endArray();

    SL::Save(settings, "zoom",              m_mapWidget->zoom());
    SL::Save(settings, "centerLat",         m_mapWidget->centerLatitude());
    SL::Save(settings, "centerLon",         m_mapWidget->centerLongitude());
    SL::Save(settings, "mapQualityStill",   m_mapWidget->mapQuality(Marble::Still));
    SL::Save(settings, "mapQualityAnim",    m_mapWidget->mapQuality(Marble::Animation));
    SL::Save(settings, "mapTheme",          m_mapWidget->mapThemeId());
    SL::Save(settings, "heading",           m_mapWidget->heading());
    SL::Save(settings, "showCompass",       m_mapWidget->showCompass());
    SL::Save(settings, "showOverviewMap",   m_mapWidget->showOverviewMap());
    SL::Save(settings, "showScaleBar",      m_mapWidget->showScaleBar());

    // Filter settings
    SL::Save(settings, "filterMap",         ui->filterMap);
    SL::Save(settings, "filterMode",        ui->filterMode);
    SL::Save(settings, "filterDist",        m_filterDistanceDSB);
    SL::Save(settings, "filterFeatureMask", m_filterFeatureMask);

    if (completer() != nullptr)
        SL::Save(settings, "completer",         *completer());
}

void MapPane::load(QSettings& settings)
{
    if (ui == nullptr || m_mapWidget == nullptr)
        return;

    Pane::load(settings);

    // TODO: add first class support for hash save/load in settings.h
    // Serialize floating item state for the map (size/colors of overview map, etc)
    const int floatItemCount = settings.beginReadArray("floatItem"); {
        for (int fi = 0; fi < floatItemCount; ++fi) {
            settings.setArrayIndex(fi);

            const QString nameId = settings.value("floatItemNameId").toString();

            // Re-assign it to the proper thing.  This has an obnoxious time complexity, but
            // that doesn't matter for our tiny list size.
            for (Marble::AbstractFloatItem* floatItem : m_mapWidget->floatItems()) {
                 if (floatItem != nullptr && floatItem->nameId() == nameId) {
                     floatItem->setSettings(settings.value("floatItemSettings").value<QHash<QString, QVariant>>());
                     floatItem->setVisible(settings.value("floatItemVisible").value<bool>());
                 }
            }
        }
    } settings.endArray();

    m_mapWidget->setZoom(SL::Load<int>(settings, "zoom", 1500));
    m_mapWidget->setCenterLatitude(SL::Load<qreal>(settings, "centerLat", 40.0));
    m_mapWidget->setCenterLongitude(SL::Load<qreal>(settings, "centerLon", -100.0));
    m_mapWidget->setMapQualityForViewContext(SL::Load<Marble::MapQuality>(settings, "mapQualityStill", Marble::HighQuality),
                                           Marble::Still);
    m_mapWidget->setMapQualityForViewContext(SL::Load<Marble::MapQuality>(settings, "mapQualityAnim", Marble::HighQuality),
                                           Marble::Animation);
    m_mapWidget->setMapThemeId(SL::Load<QString>(settings, "mapTheme", m_mapWidget->mapThemeId()));
    m_mapWidget->setHeading(SL::Load<qreal>(settings, "heading", 0.0));
    m_mapWidget->setShowCompass(SL::Load<bool>(settings, "showCompass", true));
    m_mapWidget->setShowOverviewMap(SL::Load<bool>(settings, "showOverviewMap", true));
    m_mapWidget->setShowScaleBar(SL::Load<bool>(settings, "showScaleBar", true));

    SL::Load(settings, "filterMap",  ui->filterMap);
    SL::Load(settings, "filterMode", ui->filterMode);
    SL::Load(settings, "filterDist", m_filterDistanceDSB);
    SL::Load(settings, "filterFeatureMask", m_filterFeatureMask);

    if (completer() != nullptr)
        SL::Load(settings, "completer",  *completer());

    refreshMenus();
    updateFilter();

    // This is a workaround for a map widget defect which fails to apply higher render quality sometimes.
    m_mapWidget->zoomOut();
    m_mapWidget->zoomIn();
}

void MapPane::updateViewPreset(const QModelIndex& idx)
{
    if (m_mapWidget == nullptr || !idx.isValid())
        return;

    ViewModel& vm = app().viewModel();

    const QString presetName = vm.data(ViewModel::Name, idx).toString();

    // Nicely named undo
    const UndoMgr::ScopedUndo undoSet(app().undoMgr(), tr("Update View: ") + presetName);

    vm.setRow(presetName, m_mapWidget->viewParams(), idx);

    mainWindow().statusMessage(UiType::Success, tr("View preset updated: ") + presetName);
}

void MapPane::updateFilter()
{
    m_filterDistance->setEnabled(ui->filterMode->currentIndex() == FilterMode::Distance);

    QSignalBlocker block(&m_filterFeatureModel);
    for (int r = 1; r < m_filterFeatureModel.rowCount(); ++r)
        m_filterFeatureModel.item(r)->setCheckState(bool(m_filterFeatureMask & (1U<<r)) ? Qt::Checked : Qt::Unchecked);

    viewMoveIdle(); // update the filter's params
}

GeoLocCompleter* MapPane::completer()
{
    if (ui == nullptr || ui->filterMap == nullptr)
        return nullptr;

    return static_cast<GeoLocCompleter*>(ui->filterMap->completer());
}

const GeoLocCompleter* MapPane::completer() const
{
    return static_cast<const GeoLocCompleter*>(ui->filterMap->completer());
}

void MapPane::viewMoveIdle()
{
    if (m_mapWidget == nullptr)
        return;

    // Set model center (but don't block on loading)
    app().geoLocModel().setCenter(m_mapWidget->center());

    // requested filter parameters
    float                            maxDistM = 0.0f;
    Marble::GeoDataLatLonBox         bounds;
    GeoPolRegionVec                  geoPol;
    const Marble::GeoDataCoordinates center = m_mapWidget->center();

    switch (ui->filterMode->currentIndex()) {
    case FilterMode::Distance:
        maxDistM = cfgData().unitsTrkLength.from(m_filterDistance->value()).toFloat();
        break;
    case FilterMode::VisibleArea:
        bounds = m_mapWidget->bounds();
        break;
    case FilterMode::GeoPolRegion:
        geoPol = app().geoPolMgr().intersections(center, GeoPolRegion::IncludeNever);
        break;
    case FilterMode::GeoPolCountry:
        geoPol = app().geoPolMgr().intersections(center, GeoPolRegion::IncludeNever);
        if (!geoPol.isEmpty())
            geoPol.resize(1);
        break;
    }

    // Update the filter
    m_geoLocFilter.update(geoPol, center, maxDistM, bounds, m_filterFeatureMask);
}

void MapPane::setOfflineMode(bool offline)
{
    if (m_mapWidget != nullptr)
        m_mapWidget->setOfflineMode(offline);
}

void MapPane::reloadVisible()
{
    if (m_mapWidget != nullptr)
        m_mapWidget->reloadMap();
}

void MapPane::downloadTiles(const QVector<Marble::TileCoordsPyramid>& region)
{
    if (m_mapWidget != nullptr)
        m_mapWidget->downloadRegion(region);
}

Marble::GeoDataLatLonBox MapPane::bounds() const
{
    if (m_mapWidget != nullptr)
        return m_mapWidget->bounds();

    return { };
}

Marble::GeoDataCoordinates MapPane::center() const
{
    if (m_mapWidget != nullptr)
        return m_mapWidget->center();

    return { };
}

int MapPane::minimumTileLevel() const
{
    (void)this; // temporary, to make clang-tidy happy until this is implemented
    return 1;  // TODO: can we get this dynamically from marble?
}

int MapPane::maximumTileLevel() const
{
    (void)this; // temporary, to make clang-tidy happy until this is implemented
    return 18; // TODO: can we get this dynamically from marble?
}

int MapPane::currentTileLevel() const
{
    return (m_mapWidget != nullptr) ? m_mapWidget->tileZoomLevel() : 1;
}

void MapPane::registerGpsdPoint(const Pane& pane)
{
    if (m_mapWidget != nullptr)
        m_mapWidget->registerGpsdPoint(pane);
}

void MapPane::setGpsdPoint(const Pane& pane, const PointItem& point)
{
    if (m_mapWidget != nullptr)
        m_mapWidget->setGpsdPoint(pane, point);
}

void MapPane::unregisterGpsdPoint(const Pane& pane)
{
    if (m_mapWidget != nullptr)
        m_mapWidget->unregisterGpsdPoint(pane);
}

void MapPane::resizeToFit(int)
{
    if (completer() != nullptr)
        completer()->fontResized();
}

void MapPane::addViewPresetInteractive()
{
    if (m_mapWidget == nullptr)
        return;

    NewViewDialog newViewDialog(mainWindow(), m_mapWidget->viewParams(), this);

    newViewDialog.exec();
}

void MapPane::on_mapThemeButton_clicked()
{
    // Lazy construction: only make it if somebody uses it.
    if (m_mapThemeDialog == nullptr)
        m_mapThemeDialog = new MapThemeDialog(mainWindow());

    if (m_mapThemeDialog == nullptr || m_mapWidget == nullptr)
        return;

    // If it's already open, close it.
    if (m_mapThemeDialog->isVisible()) {
        m_mapThemeDialog->close();
        return;
    }

    // Map dialog location to global coordinates
    const QPoint popLocation = mapToGlobal(QPoint(ui->mapThemeButton->x() - m_mapThemeDialog->width(),
                                                  ui->mapThemeButton->y() + ui->mapThemeButton->height()));

    m_mapThemeDialog->setGeometry(QRect(popLocation, m_mapThemeDialog->size()));
    m_mapThemeDialog->setWindowFlags(Qt::Popup);
    m_mapThemeDialog->show();

    connect(m_mapThemeDialog, &MapThemeDialog::themeSelected, m_mapWidget, &TrackMap::mapThemeSelected,
            Qt::UniqueConnection);
}

void MapPane::on_action_Show_Compass_triggered(bool checked)
{
    if (m_mapWidget != nullptr)
        m_mapWidget->setShowCompass(checked);
}

void MapPane::on_action_Show_ScaleBar_triggered(bool checked)
{
    if (m_mapWidget != nullptr)
        m_mapWidget->setShowScaleBar(checked);
}

void MapPane::on_action_Show_Overview_triggered(bool checked)
{
    if (m_mapWidget != nullptr)
        m_mapWidget->setShowOverviewMap(checked);
}

void MapPane::on_action_Orient_to_North_triggered()
{
    if (m_mapWidget != nullptr)
        m_mapWidget->setHeading(0.0);
}

void MapPane::on_action_Location_to_Clipboard_triggered()
{
    if (m_mapWidget == nullptr)
        return;

    qreal lon, lat;
    if (!m_mapWidget->geoCoordinates(m_menuPos.x(), m_menuPos.y(), lon, lat)) {
        QGuiApplication::clipboard()->setText(tr("N/A"));
        return;
    }

    QGuiApplication::clipboard()->setText(QString("%1 %2")
                                          .arg(lat, 0, 'f', 8)
                                          .arg(lon, 0, 'f', 8));
}

void MapPane::on_action_Add_View_Preset_triggered()
{
    addViewPresetInteractive();
}

void MapPane::on_filterMode_currentIndexChanged(int)
{
    updateFilter();
}

void MapPane::on_filterFeature_currentIndexChanged(int)
{
    updateFilter();
}

void MapPane::filterDistChanged(double)
{
    updateFilter();
}

void MapPane::showFilterContextMenu(const QPoint& pos)
{
    m_filterFeatureMenu.exec(ui->filterFeature->mapToGlobal(pos));
}

void MapPane::execFilterFeatureMenu(QAction* action)
{
    if (action == ui->action_Feature_Select_All) {
        m_filterFeatureMask = GeoLocFilter::filterMaskAll;
    } else if (action == ui->action_Feature_Select_None) {
        m_filterFeatureMask = GeoLocFilter::filterMaskNone;
    } else if (action == ui->action_Feature_Toggle) {
        m_filterFeatureMask = ~m_filterFeatureMask;
    } else if (action == ui->action_Feature_Default) {
        m_filterFeatureMask = GeoLocFilter::filterMaskDefault;
    }

    updateFilter();
}

void MapPane::useModel()
{
    app().geoLocModel().finishLoad();
    app().geoLocModel().setCenter(m_mapWidget->center());
}

void MapPane::dragEnterEvent(QDragEnterEvent* event)
{
    // Allow drop from views
    if (app().viewModel().isStreamMagic(event->mimeData()) &&
        app().viewModel().getDropIndices(event->mimeData()).size() == 1) // only one view can be dropped
        event->acceptProposedAction();

    // Allow drop from MapDataModels
    for (App::Model mtype = App::Model::_First; mtype != App::Model::_LastSaved; Util::inc(mtype)) {
        const auto* mapModel = dynamic_cast<const MapDataModel*>(&app().getModel(mtype));
        const auto* model    = dynamic_cast<const TreeModel*>(&app().getModel(mtype));

        if (mapModel != nullptr && model != nullptr)
            event->acceptProposedAction();
    }

    Pane::dragEnterEvent(event);
}

void MapPane::dropEvent(QDropEvent* event)
{
    // Drop from view
    if (app().viewModel().isStreamMagic(event->mimeData())) {
        const auto srcIndexes = app().viewModel().getDropIndices(event->mimeData());
        assert(srcIndexes.size() == 1);

        gotoView(srcIndexes.front());
        event->accept();
        return;
    }

    // Drop from MapDataModels
    for (App::Model mtype = App::Model::_First; mtype != App::Model::_LastSaved; Util::inc(mtype)) {
        const auto* mapModel = dynamic_cast<const MapDataModel*>(&app().getModel(mtype));
        const auto*    model    = dynamic_cast<const TreeModel*>(&app().getModel(mtype));

        if (mapModel != nullptr && model != nullptr && model->isStreamMagic(event->mimeData())) {
            const auto srcIndexes = model->getDropIndices(event->mimeData());
            assert(srcIndexes.size() > 0);

            zoomTo(mapModel->boundsBox(srcIndexes));
            event->accept();
            return;
        }
    }

    Pane::dropEvent(event);
}

void MapPane::on_filterMap_textEdited(const QString&)
{
    useModel();
}

void MapPane::on_filterMap_returnPressed()
{
    if (const QModelIndex selected = completer()->lastSelectedIndex(); selected.isValid()) {
        // use last selected index, if available
        filterNameActivated(selected);
    } else {
        useModel();

        // Search for matching names (sans case), and use closest one to current position
        const QString name = ui->filterMap->text();
        const QModelIndex closest = m_geoLocFilter.findClosest(name);

        if (closest.isValid()) {
            filterNameActivated(closest);
        } else {
            mainWindow().statusMessage(UiType::Warning, tr("Location not found: ") + name);
        }
    }
}

void MapPane::filterNameActivated(const QModelIndex& filterIdx)
{
    if (!filterIdx.isValid())
        return;

    const QModelIndex idx = Util::MapDown(filterIdx);
    const QString name = app().geoLocModel().data(GeoLocModel::Name, idx, Util::RawDataRole).toString();

    completer()->setCompletionPrefix(name);
    completer()->resetLastSelectedIndex();
    ui->filterMap->setText(name);

    gotoView(app().geoLocModel().viewParams(idx), name);
}

void MapPane::handleMapAreaChanged(const Marble::GeoDataLatLonAltBox& area)
{
    emit visibleAreaChanged(area);
}

void MapPane::on_action_Create_Waypoint_triggered()
{
    mainWindow().getNewWaypointDialog().exec({ m_mapWidget->mouseGeoCoordinates() });
}
