/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>

#include "gpsdevicepane.h"
#include "ui_gpsdevicepane.h"

#include <src/util/icons.h>
#include <src/util/ui.h>
#include <src/ui/panes/datacolumnpanebase.inl.h>

#include "src/core/app.h"
#include "src/ui/windows/mainwindow.h"
#include "src/dev-io/gpsmodel.h"

GpsDevicePane::GpsDevicePane(MainWindow& mainWindow, bool contextMenus, QWidget* parent) :
    DataColumnPane(mainWindow, PaneClass::GpsDevice, parent, false),
    NamedItem(GpsModel::getItemNameStatic()),
    ui(new Ui::GpsDevicePane)
{
    ui->setupUi(this);
    setupView(ui->treeView, &app().gpsModel());
    setWidgets<GpsModel>(GpsDevicePane::defColumnView(),
                         ui->queryText, nullptr, ui->showColumns, ui->filterCtrl, ui->queryIsValid);

    if (contextMenus) // don't do for standalone use.
        setupContextMenus();

    m_treeView->setUniformRowHeights(true); // we don't have many entries, so this isn't painful

    setupActionIcons();
    setupSignals();
    Util::SetupWhatsThis(this);
}

GpsDevicePane::~GpsDevicePane()
{
    deleteUI(ui);
}

void GpsDevicePane::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Import_Data, "document-import");
    Icons::defaultIcon(ui->action_Refresh,     "view-refresh");
}

void GpsDevicePane::setupContextMenus()
{
    paneMenu.addActions({ ui->action_Import_Data,
                          ui->action_Refresh });

    setupActionContextMenu(paneMenu);
    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(this, &GpsDevicePane::customContextMenuRequested, this, &GpsDevicePane::showContextMenu);
}

void GpsDevicePane::setupSignals()
{
    DataColumnPane::setupSignals();

    connect(&app().gpsModel(), &GpsModel::rowsInserted, this, &GpsDevicePane::resizeOnChange);
}

TreeModel* GpsDevicePane::model()
{
    return &app().gpsModel();
}

const TreeModel* GpsDevicePane::model() const
{
    return &app().gpsModel();
}

const GpsDevicePane::DefColumns& GpsDevicePane::defColumnView() const
{
    static const DefColumns cols = {
        { GpsModel::Make,       true  },
        { GpsModel::Model,      true  },
        { GpsModel::Device,     false },
        { GpsModel::Image,      true, },
        { GpsModel::MountPoint, false },
    };

    // Verify we added everything.  It's not critical, but better practice.
    for (ModelType md = GpsModel::_First; md < GpsModel::_Count; ++md)
        assert(cols.findData(md) >= 0);

    return cols;
}

void GpsDevicePane::showContextMenu(const QPoint& pos)
{
    ui->action_Import_Data->setEnabled(hasSelection());

    paneMenu.exec(mapToGlobal(menuPos = pos));
}

void GpsDevicePane::import(const GeoLoadParams& loadParams)
{
    mainWindow().importTracks(loadParams, app().gpsModel().importFiles(getSelections()));
}

void GpsDevicePane::import()
{
    mainWindow().importTracks(app().gpsModel().importFiles(Util::MapDown(getSelections())));
}

QTreeView* GpsDevicePane::treeView() const
{
    return ui->treeView;
}

void GpsDevicePane::on_action_Refresh_triggered()
{
    (void)this; // Preserve as a non-static method for stylistic reasons. This placates clang-tidy.
    app().gpsModel().refresh();
}

void GpsDevicePane::resizeOnChange()
{
    resizeToFit(100); // small delay to avoid spam
}

void GpsDevicePane::showEvent(QShowEvent* event)
{
    app().gpsModel().refresh();
    DataColumnPane::showEvent(event);
}

void GpsDevicePane::on_action_Import_Data_triggered()
{
    import();
}
