/*
    Copyright 2021-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SIMPLEPANE_H
#define SIMPLEPANE_H

#include <QString>
#include <QPersistentModelIndex>

#include <src/core/modelvarexpander.h>

#include "src/ui/dialogs/modeltexteditdialog.h"
#include "pane.h"

namespace Ui {
class SimplePane;
} // namespace Ui

class MainWindow;

class SimplePane : public Pane
{
    Q_OBJECT

public:
    explicit SimplePane(MainWindow&, QWidget* parent = nullptr);
    ~SimplePane() override;

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

private slots:
    void on_SimplePane_toggled(bool checked) { paneToggled(checked); }
    void showContextMenu(const QPoint&);
    void currentTrackChanged(const QModelIndex&);
    void processRowsAboutToBeRemoved(const QModelIndex&, int first, int last);
    void processModelAboutToBeReset();
    void updateVars();
    void on_action_Configure_Display_triggered();

private:
    void setupActionIcons();
    void setupContextMenus();
    void setupSignals();

    void applyText();

    Ui::SimplePane*       ui;
    ModelTextEditDialog   m_textEditor;      // configure the display text
    ModelVarExpander      m_varExpander;     // expand variable values in HTML
    QPersistentModelIndex m_currentTrackIdx; // current track
    QString               m_rawText;         // raw (non-variable-expanded) text
};

#endif // SIMPLEPANE_H
