/*
    Copyright 2019-2022` Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>

#include <QHash>

#include "viewpane.h"
#include "ui_viewpane.h"

#include <src/util/util.h>
#include <src/util/ui.h>
#include <src/ui/panes/datacolumnpanebase.inl.h>

#include <src/util/icons.h>
#include "src/ui/windows/mainwindow.h"
#include "src/core/viewmodel.h"
#include "src/core/app.h"
#include "mappane.h"

ViewPane::ViewPane(MainWindow& mainWindow, QWidget *parent) :
    DataColumnPane(mainWindow, PaneClass::View, parent),
    NamedItem(ViewModel::getItemNameStatic()),
    ui(new Ui::ViewPane)
{
    ui->setupUi(this);

    setupView(ui->viewView, &app().viewModel());
    setWidgets<ViewModel>(defColumnView(),
                          ui->filterView, nullptr, ui->showColumns, ui->filterCtrl, ui->filterIsValid);

    setupActionIcons();
    setupContextMenus();
    setupDelegates();
    setupSignals();
    newConfig();
    Util::SetupWhatsThis(this);
}

ViewPane::~ViewPane()
{
    deleteUI(ui);
}

void ViewPane::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Goto,       "mark-location-symbolic");
    Icons::defaultIcon(ui->action_Edit,       "document-edit");
    Icons::defaultIcon(ui->action_Update,     "mark-location-symbolic");
    Icons::defaultIcon(ui->action_New,        "mark-location-symbolic");
    Icons::defaultIcon(ui->action_Set_Icon,   "image-x-icon");
    Icons::defaultIcon(ui->action_Unset_Icon, "edit-clear");
}

void ViewPane::setupContextMenus()
{
    paneMenu.addActions({ ui->action_Goto,
                          ui->action_New,
                          ui->action_Edit,
                          ui->action_Set_Icon,
                          ui->action_Unset_Icon });
    paneMenu.addSeparator();
    paneMenu.addActions({ ui->action_Update,
                          mainWindow().getMainAction(MainAction::DeleteSelection),
                          mainWindow().getMainAction(MainAction::DuplicateSelection) });

    setupActionContextMenu(paneMenu);
    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(this, &ViewPane::customContextMenuRequested, this, &ViewPane::showContextMenu);
}

void ViewPane::setupSignals()
{
    DataColumnPane::setupSignals();

    QTreeView& view = *ui->viewView;
    connect(&view, &QTreeView::activated, this, &ViewPane::viewSelected);
    connect(&view, &QTreeView::clicked, this, &ViewPane::viewSelected);
}

void ViewPane::setupDelegates()
{
    initDelegates({{ ViewModel::Name, &m_textDelegate }});
}

void ViewPane::newConfig()
{
    DataColumnPane::newConfig();
    ui->viewView->setIconSize(cfgData().iconSizeView);
}

void ViewPane::showContextMenu(const QPoint& pos)
{
    paneMenu.exec(mapToGlobal(m_menuPos = pos));
}

void ViewPane::viewSelected(const QModelIndex& idx)
{
    gotoIndex(idx);
}

void ViewPane::gotoIndex(const QModelIndex& idx) const
{
    auto* mapPane = mainWindow().findPane<MapPane>();

    if (ui == nullptr || ui->viewView == nullptr || mapPane == nullptr || !idx.isValid())
        return;

    mapPane->gotoView(Util::MapDown(idx));
}

TreeModel* ViewPane::model()
{
    return &app().viewModel();
}

const TreeModel* ViewPane::model() const
{
    return &app().viewModel();
}

const ViewPane::DefColumns& ViewPane::defColumnView() const
{
    static const DefColumns cols = {
        { ViewModel::Name,      true  },
        // ViewModel::Icon in name column
        { ViewModel::CenterLat, false },
        { ViewModel::CenterLon, false },
        { ViewModel::Heading,   false },
        { ViewModel::Zoom,      false },
    };

    // Verify we added everything.  It's not critical, but better practice.
    for (ModelType md = ViewModel::_First; md < ViewModel::_Count; ++md)
        assert(cols.findData(md) >= 0);

    return cols;
}

void ViewPane::on_action_Goto_triggered()
{
    gotoIndex(ui->viewView->currentIndex());
}

bool ViewPane::hasAction(MainAction action) const
{
    switch (action)
    {
    case MainAction::ZoomToSelection:    [[fallthrough]];
    case MainAction::DeleteSelection:    [[fallthrough]];
    case MainAction::DuplicateSelection: return true;
    default:                             return false;
    }
}

void ViewPane::on_action_Edit_triggered()
{
    const QModelIndex idx = clickPosIndex(m_menuPos);

    if (idx.isValid())
        ui->viewView->edit(idx);
}

void ViewPane::on_action_Update_triggered()
{
    auto* mapPane = mainWindow().findPane<MapPane>();

    if (ui == nullptr || ui->viewView == nullptr || mapPane == nullptr)
        return;

    const QModelIndex idx = Util::MapDown(ui->viewView->currentIndex());

    if (!idx.isValid())
        return;

    mapPane->updateViewPreset(idx);
}

void ViewPane::on_action_New_triggered()
{
    if (auto* mapPane = mainWindow().findPane<MapPane>())
        mapPane->addViewPresetInteractive();
}

void ViewPane::on_action_Set_Icon_triggered()
{
    setIcon(ViewModel::Icon);
}

void ViewPane::on_action_Unset_Icon_triggered()
{
    clearIcon(ViewModel::Icon);
}

// Helper.  There's probably a better way.
QString ViewPane::nameFromTopIndex(const QModelIndex& idx) const
{
    return topFilter().data(topFilter().index(idx.row(), ViewModel::Name, idx.parent()),
                            Util::RawDataRole).toString();
}

void ViewPane::save(QSettings& settings) const
{
    DataColumnPane::save(settings);

    // Save expansion state for each header node in the detail view.
    settings.beginGroup("Expanded"); {
        Util::Recurse(topFilter(), [this, &settings](const QModelIndex& idx) {
            if (topFilter().hasChildren(idx))
                SL::Save(settings, nameFromTopIndex(idx), ui->viewView->isExpanded(idx));
            return true;
        });
    } settings.endGroup();
}

void ViewPane::load(QSettings& settings)
{
    DataColumnPane::load(settings);

    QHash<QString, bool> expandState;

    // Restore expansion state for each header node in the detail view.
    settings.beginGroup("Expanded"); {
        const QStringList keys = settings.childKeys();

        for (const auto& key : keys) {
            bool expanded = true;
            SL::Load(settings, key, expanded);
            expandState[key] = expanded;
        }
    } settings.endGroup();

    Util::Recurse(topFilter(), [this, &expandState](const QModelIndex& idx) {
        if (topFilter().hasChildren(idx))
            if (const auto it = expandState.find(nameFromTopIndex(idx)); it != expandState.end())
                ui->viewView->setExpanded(idx, it.value());
        return true;
    });
}

