/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QGuiApplication>
#include <QRegularExpression>
#include <QInputDialog>

#include <src/undo/undomgr.h>
#include <src/util/icons.h>
#include <src/util/ui.h>

#include "src/core/app.h"
#include "src/ui/windows/mainwindow.h"
#include "src/ui/panes/mappane.h"
#include "src/ui/dialogs/newtrackdialog.h"
#include "src/core/trackmodel.h"
#include "gpscapturepane.h"
#include "ui_gpscapturepane.h"

GpsCapturePane::GpsCapturePane(MainWindow& mainWindow, QWidget *parent) :
    Pane(mainWindow, PaneClass::GpsCapture, parent),
    m_offline(Icons::get("content-loading-symbolic")),
    m_mode(Mode::Track),
    m_firstPoint(true),
    ui(new Ui::GpsCapturePane)
{
    ui->setupUi(this);

    setPaneFilterBar(ui->filterCtrl);

    setupActionIcons();
    setupSignals();
    setupLabelStyle();
    setupGpsdSupported();
    Util::SetupWhatsThis(this);

    setRunStatus(false, false);
    clearUi();
}

GpsCapturePane::~GpsCapturePane()
{
    if (m_gpsd.isRunning()) {
        m_gpsd.endCollection();  // halt collection before UI teardown
        runStatusChanged(false); // since signals aren't fired after we're being deleted
    }

    deleteUI(ui);
}

void GpsCapturePane::setupActionIcons()
{
    Icons::defaultIcon(ui->btnTrack,    "media-playback-start");
    Icons::defaultIcon(ui->btnCapture,  "media-record");
    Icons::defaultIcon(ui->btnPause,    "media-playback-pause");
    Icons::defaultIcon(ui->btnStop,     "media-playback-stop");
    Icons::defaultIcon(ui->btnGoto,     "view-restore");
    Icons::defaultIcon(ui->btnWaypoint, "mark-location-symbolic");
}

void GpsCapturePane::setupSignals()
{
    connect(&m_gpsd, &Gpsd::paused,  this, &GpsCapturePane::pauseStatusChanged);
    connect(&m_gpsd, &Gpsd::running, this, &GpsCapturePane::runStatusChanged);
    connect(&m_gpsd, &Gpsd::data,    this, &GpsCapturePane::receiveData);
    connect(&m_gpsd, &Gpsd::status,  this, &GpsCapturePane::statusMsg);
}

void GpsCapturePane::setupLabelStyle()
{
    static const QRegularExpression labelRe("label.*");
    static const QRegularExpression valueRe("value.*");

    for (auto* label : findChildren<QLabel*>(labelRe)) {
        QFont font(label->font());
        font.setItalic(true);
        label->setFont(font);
    }

    for (auto* label : findChildren<QLabel*>(valueRe)) {
        QFont font(label->font());
        font.setPointSizeF(font.pointSizeF() * 1.1f);
        label->setFont(font);
    }
}

void GpsCapturePane::setupGpsdSupported()
{
    ui->gpsdSupportSwitch->setCurrentIndex(Gpsd::gpsdSupported() ? 0 : 1);
}

void GpsCapturePane::startCollection(Mode mode)
{
    m_mode = mode;

    m_gpsd.startCollection(ui->hostname->text(),
                           ui->port->value(),
                           ui->device->text());
}

PointModel* GpsCapturePane::capturePointModel()
{
    if (m_mode != Mode::Capture || !m_appendTo.isValid())
        return nullptr;

    return app().trackModel().geoPoints(m_appendTo);
}

template <typename FN>
void GpsCapturePane::forwardToMap(const FN& fn) const
{
    mainWindow().runOnPanels<MapPane>(fn);
}

void GpsCapturePane::registerGpsdPoint()
{
    m_firstPoint = true;

    mainWindow().incCaptureCount();

    forwardToMap([this](MapPane* mapPane) {
        mapPane->registerGpsdPoint(*this);
    });
}

void GpsCapturePane::unregisterGpsdPoint()
{
    mainWindow().decCaptureCount();

    forwardToMap([this](MapPane* mapPane) {
        mapPane->unregisterGpsdPoint(*this);
    });
}

void GpsCapturePane::setGpsdPoint(const Gpsd::GpsData& gpsData)
{
    forwardToMap([this, &gpsData](MapPane* mapPane) {
        mapPane->setGpsdPoint(*this, gpsData.m_point);
        if (m_firstPoint)
            mapPane->zoomTo(gpsData.m_point);
    });

    m_firstPoint = false;
}

bool GpsCapturePane::appendToTrack(const Gpsd::GpsData& gpsData)
{
    // capturePointModel() will return null if not capturing.
    PointModel* pm = capturePointModel();
    if (pm == nullptr) // no model?
        return false;

    const int segCount = pm->rowCount();
    if (segCount == 0) // no segment?
        return false;

    const QModelIndex segIdx = pm->index(segCount - 1, 0);
    return pm->appendPoint(gpsData.m_point, segIdx);
}

QIcon GpsCapturePane::statusIcon(Gpsd::Rc rc)
{
    switch (rc) {
    case Gpsd::Rc::Success:        return Icons::get("state-ok");
    case Gpsd::Rc::BadVersion:     return Icons::get("state-error");
    case Gpsd::Rc::Partial:        return Icons::get("state-warning");
    case Gpsd::Rc::NoGpsd:         return Icons::get("state-error");
    case Gpsd::Rc::GpsdError:      return Icons::get("state-error");
    case Gpsd::Rc::NoDevice:       return Icons::get("state-warning");
    case Gpsd::Rc::ReadError:      return Icons::get("state-error");
    case Gpsd::Rc::NotRunning:     return Icons::get("state-information");
    case Gpsd::Rc::AlreadyRunning: return Icons::get("state-information");
    case Gpsd::Rc::Idle:           return Icons::get("state-offline");
    case Gpsd::Rc::Paused:         return Icons::get("state-pause");
    case Gpsd::Rc::Acquiring:      return Icons::get("state-sync");
    case Gpsd::Rc::Running:        return Icons::get("state-download");
    }

    return Icons::get("state-error");
}

void GpsCapturePane::setRunStatus(bool running, bool msg)
{
    // Update button enabled states
    ui->btnTrack->setEnabled(!running);
    ui->btnCapture->setEnabled(!running);
    ui->btnPause->setEnabled(running);
    ui->btnStop->setEnabled(running);
    ui->btnGoto->setEnabled(m_lastFix.hasLoc());
    ui->btnWaypoint->setEnabled(m_lastFix.hasLoc());

    if (msg) {
        if (running) {
            mainWindow().statusMessage(UiType::Success, tr("GPSD capture started"));
        } else {
            mainWindow().statusMessage(UiType::Info, tr("GPSD capture finished"));
            clearUi();
        }
    }
}

void GpsCapturePane::runStatusChanged(bool running)
{
    setRunStatus(running, true);

    // messages from Gpsd: one per startup or shutdown
    if (running) {
        registerGpsdPoint();

        if (PointModel* pm = capturePointModel(); pm != nullptr)
            pm->incDontTrack(); // avoid spamming undo manager
    } else {
        unregisterGpsdPoint();

        if (PointModel* pm = capturePointModel(); pm != nullptr)
            pm->decDontTrack();

        m_appendTo = QModelIndex();
    }
}

void GpsCapturePane::pauseStatusChanged(bool paused)
{
    // Update pause/continue button
    ui->btnPause->setIcon(Icons::get(paused ? "media-playback-start" : "media-playback-pause"));
    ui->btnPause->setText(paused ? tr("Continue") : tr("Pause"));
}

void GpsCapturePane::receiveData(const Gpsd::GpsData& gpsData)
{
    updateUi(gpsData);
    setGpsdPoint(gpsData);
    appendToTrack(gpsData);
}

void GpsCapturePane::statusMsg(Gpsd::Rc rc)
{
    const int height = ui->labelSat->height() * 5 / 3;

    // Status
    ui->iconStatus->setPixmap(statusIcon(rc).pixmap(height));
    ui->valueStatus->setText(Gpsd::statusStr(rc));

    switch (rc) {
    case Gpsd::Rc::NoGpsd:    [[fallthrough]];
    case Gpsd::Rc::GpsdError: [[fallthrough]];
    case Gpsd::Rc::NoDevice:  [[fallthrough]];
    case Gpsd::Rc::ReadError:
        mainWindow().statusMessage(UiType::Error, Gpsd::statusStr(rc));
        break;
    default:
        break;
    }
}

void GpsCapturePane::updateUi(const Gpsd::GpsData& gpsData)
{
    m_lastFix = gpsData.m_point;  // remember for goto button

    ui->btnGoto->setEnabled(m_lastFix.hasLoc());
    ui->btnWaypoint->setEnabled(m_lastFix.hasLoc());

    ui->valueSat->setText(QString::number(gpsData.m_satellites));

    const int height = ui->labelSat->height();
    const QPixmap offline = m_offline.pixmap(QSize(height, height));

    // Easier indexing into flag labels
    const std::array<QLabel*, 3> valueGeoPol = {
        ui->valueGeoPol0, ui->valueGeoPol1, ui->valueGeoPol2
    };

    // Time stamp
    if (gpsData.m_point.hasTime())
        ui->valueTime->setText(cfgData().unitsPointDate.toString(gpsData.m_point.time()));
    else
        ui->valueTime->setPixmap(offline);

    // Elevation
    if (gpsData.m_point.hasEle())
        ui->valueAlt->setText(cfgData().unitsElevation.toString(PointItem::Ele_t::base_type(gpsData.m_point.ele())));
    else
        ui->valueAlt->setPixmap(offline);

    // Lat/lon and region
    if (gpsData.m_point.hasLoc()) {
        ui->valueLon->setText(cfgData().unitsLon.toString(PointItem::Lon_t::base_type(gpsData.m_point.lon())));
        ui->valueLat->setText(cfgData().unitsLat.toString(PointItem::Lat_t::base_type(gpsData.m_point.lat())));

        const auto coords = gpsData.m_point.as<Marble::GeoDataCoordinates>();
        const GeoPolRegionVec geopol = app().geoPolMgr().intersections(coords, GeoPolRegion::WorldOpt::IncludeIfNoOther);

        ui->valueGeoPolName->setText(geopol.last()->isoA2());

        // Set flags
        int flagNum = 0;
        for (const auto* region : geopol) {
            valueGeoPol.at(flagNum)->setPixmap(region->flagPixmap(height + 4));
            valueGeoPol.at(flagNum)->setToolTip(region->name());
            if (++flagNum >= int(valueGeoPol.size()))
                break;
        }

    } else {
        ui->valueLon->setPixmap(offline);
        ui->valueLat->clear();

        for (auto* l : valueGeoPol) {
            l->clear();
            if (l == valueGeoPol.at(0))
                l->setPixmap(offline);
        }

        ui->valueGeoPolName->clear();
    }

    // speed
    if (gpsData.m_point.hasSpeed(PointItem::Measured))
        ui->valueSpeed->setText(cfgData().unitsSpeed.toString(Speed_t::base_type(gpsData.m_point.speed())));
    else
        ui->valueSpeed->setPixmap(offline);
}

void GpsCapturePane::on_btnTrack_clicked()
{
    startCollection(Mode::Track);
}

void GpsCapturePane::on_btnCapture_clicked()
{
    NewTrackDialog& trackDialog = mainWindow().getNewTrackDialog();

    m_appendTo = trackDialog.exec(app().trackModel(), tr("Capture track from live data"));
    if (!m_appendTo.isValid())
        return;

    startCollection(Mode::Capture);
}

void GpsCapturePane::on_btnPause_clicked()
{
    m_gpsd.command(Gpsd::Cmd::TogglePause);
}

void GpsCapturePane::on_btnStop_clicked()
{
    m_gpsd.endCollection();
}

void GpsCapturePane::on_btnGoto_clicked()
{
    if (m_lastFix.hasLoc()) {
        forwardToMap([this](MapPane* mapPane) {
            mapPane->zoomTo(m_lastFix);
        });
    } else {
        mainWindow().statusMessage(UiType::Warning, tr("No fix"));
    }
}

void GpsCapturePane::on_btnWaypoint_clicked()
{
    if (m_lastFix.hasLoc()) {
        if (mainWindow().getNewWaypointDialog().exec({ m_lastFix.as<Marble::GeoDataCoordinates>() }) == QDialog::Rejected)
            return;
    } else {
        mainWindow().statusMessage(UiType::Warning, tr("No fix"));
    }
}

void GpsCapturePane::on_GpsCapturePane_toggled(bool checked)
{
    paneToggled(checked);
    setRunStatus(m_gpsd.isRunning(), false); // refresh after this is damaged by reenableChildren()
}

void GpsCapturePane::save(QSettings& settings) const
{
    if (ui == nullptr)
        return;

    Pane::save(settings);

    SL::Save(settings, "hostname", ui->hostname);
    SL::Save(settings, "port",     ui->port);
    SL::Save(settings, "device",   ui->device);
}

void GpsCapturePane::load(QSettings& settings)
{
    if (ui == nullptr)
        return;

    Pane::load(settings);

    SL::Load(settings, "hostname", ui->hostname);
    SL::Load(settings, "port",     ui->port);
    SL::Load(settings, "device",   ui->device);

    setRunStatus(m_gpsd.isRunning(), false);
}
