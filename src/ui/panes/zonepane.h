/*
    Copyright 2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ZONEPANE_H
#define ZONEPANE_H

#include <tuple>

#include <QTimer>
#include <QModelIndexList>
#include <QVector>

#include "chartbase.h"
#include "src/fwddecl.h"
#include "src/core/pointitem.h"
#include "src/ui/panes/pointselectpane.h"

namespace Ui {
class ZonePane;
} // namespace Ui

namespace QtCharts {
class QPieSeries;
class QPieSlice;
} // namespace QtCharts

class MainWindow;
class QMouseEvent;

class ZonePane :
        public ChartBase,
        public PointSelectPane
{
    Q_OBJECT

public:
    explicit ZonePane(MainWindow&, QWidget *parent = nullptr);
    ~ZonePane() override;

    void newConfig() override;

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

private slots:
    void processRowsAboutToBeRemoved(const QModelIndex&, int first, int last);
    void processModelAboutToBeReset();
    void processModelReset();
    void selectedTracksChanged(const QItemSelectionModel*, const QItemSelection&, const QItemSelection&);
    void currentTrackChanged(const QModelIndex&);
    void on_ZonePane_toggled(bool checked) { paneToggled(checked); }
    void updateChart() override;
    void showContextMenu(const QPoint& pos);
    void sliceHovered(QtCharts::QPieSlice*, bool state);
    void sliceDoubleClicked(QtCharts::QPieSlice*);
    void on_action_Show_Legend_toggled(bool);
    void on_action_Show_Legend_Details_toggled(bool checked);
    void on_action_Open_Configuration_triggered();
    void on_method_currentIndexChanged(int index);

private:
    friend class TestZtgps; // test hook

    // Data we accumulate per zone
    struct ZoneAccum_t {
        ZoneAccum_t() { }

        inline ZoneAccum_t& operator+=(const ZoneAccum_t& rhs);

        inline void accumulate(const PowerData&, const PointItem* prevPt, const PointItem& pt);

        PointItem::Power_t avgPower() const {
            return m_wattMsec / std::max(m_powerDuration, PointItem::Dur_t(1));
        }

        PointItem::Hr_t avgHr() const {
            return m_hrMSec / std::max(m_hrDuration, PointItem::Dur_t(1));
        }

        QString toolTip() const;             // construct tooltip table for this data

        PointItem::Dur_t   m_duration      = Dur_t(0);  // accumulated duration for the zone
        PointItem::Dist_t  m_length        = 0;         // accumulated length for the zone
        EnergyAccum_t      m_wattMsec      = 0;         // accumulated power * time, to be time-averaged
        PointItem::Dur_t   m_powerDuration = Dur_t(0);  // milliseconds with >0 power
        PointItem::Dur_t   m_hrDuration    = Dur_t(0);  // milliseconds with >0 hr
        HrAccum_t          m_hrMSec        = 0;         // accumulated hr * time, to be time-averaged
        PointItem::Ele_t   m_ascent        = 0;         // accumulated ascent
        PointItem::Ele_t   m_descent       = 0;         // accumulated descent
    };

    struct ZoneData_t {
        ZoneData_t(float pctFloor = 0.0f, const QModelIndex& idx = QModelIndex()) :
            m_pctFloor(pctFloor),
            m_idx(idx)
        { }

        QtCharts::QPieSlice* slice(int zoneId, const ZonePane&) const;  // create pie slice for this zone
        bool inZone(float pct) const { return pct >= m_pctFloor && pct < m_pctCeil; }

        QString label() const;
        QString toolTip() const;

        float       m_pctFloor;         // percent floor for this zone
        float       m_pctCeil  = -1.0f; // percent ceiling for this zone
        float       m_fraction =  0.0f; // [0..1] fraction of total pie
        QModelIndex m_idx;              // index into ZoneModel
        ZoneAccum_t m_accum;            // accumulated data
    };

    static inline int   bucket(ModelType calcMethod, const PowerData&, const PointItem* prevPt, const PointItem& pt, float maxData);
    static inline float dataPct(ModelType calcMethod, const PowerData&, const PointItem* prevPt, const PointItem& pt, float maxData);

    void setupActionIcons();
    void setupSignals();
    void setupTimers();
    void setupMenus();
    void setupChart();
    void clearChart();
    void showChart();
    void hideChart();
    void updateLabels(QtCharts::QPieSlice* activeSlice = nullptr);                          // add descriptive text to marker labels
    void focusIn() override;
    bool axesShown() const override { return false; }
    bool legendShown() const override;
    void setLegendDetailsShown(bool shown);
    bool legendDetailsShown() const;
    void refreshChart(int timer = 400);           // force refresh
    void statusMessage(const QString&, bool mainWindowMsg = false);
    float calcMaxData(const QModelIndex& trackIdx);
    void selectPointsInZone(const ZoneData_t&);
    bool validZoneId(int z) const { return z >= 0 && z < m_zoneData.size(); }
    ModelType calcMethod() const;                 // ZoneModel column to use - e.g, PctMaxHR

    QItemSelectionModel* selectionModel() const override;

    Ui::ZonePane*          ui;
    QTimer                 m_updateTimer;           // avoid update spam
    QtCharts::QPieSeries*  m_pieSeries;             // pie chart
    QModelIndexList        m_currentRows;           // what chart is currently showing
    QVector<ZoneData_t>    m_zoneData;              // per-zone accumulation data

    static const constexpr Dur_t mStonS         = Dur_t(1000000);  // mS to nS
    static const constexpr char* zoneIdProperty = "ZoneID";
};

#endif // ZONEPANE_H
