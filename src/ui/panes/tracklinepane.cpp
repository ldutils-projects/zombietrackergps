/*
    Copyright 2019-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cmath>
#include <chrono>

#include <QtMath>
#include <QLineSeries>
#include <QChart>
#include <QValueAxis>
#include <QLegendMarker>
#include <QApplication>
#include <QGuiApplication>
#include <QFontMetrics>
#include <QPalette>
#include <QItemSelectionModel>
#include <QItemSelection>
#include <QItemSelectionRange>

#include <src/core/modelmetadata.inl.h>
#include <src/util/icons.h>
#include <src/util/ui.h>

#include "src/ui/windows/mainwindow.h"
#include "src/core/pointmodel.inl.h"
#include "src/core/trackmodel.h"
#include "src/core/app.h"

#include "tracklinepane.h"
#include "pointpane.h"
#include "mappane.h"
#include "ui_tracklinepane.h"

TrackLinePane::TrackLinePane(MainWindow& mainWindow, QWidget* parent) :
    ChartBase(mainWindow, PaneClass::LineChart, parent),
    ui(new Ui::TrackLinePane),
    m_graphDataModel(this),
    m_columnMap(PointModel::_Count, -1),
    m_zoomStepWheel(1.1f),
    m_zoomStepMenu(1.5f),
    zoomLevel(1.0f),
    m_initZoom(zoomLevel),
    m_minZoom(1.0f / float(pow(double(m_zoomStepWheel), 50.0))),
    m_maxZoom(1.25f),
    xBegin(0.0f),
    m_axisUpdateTimer(this)
{
    ui->setupUi(this);
    setPaneFilterBar(ui->filterCtrl);

    setupActionIcons();
    setupDataSelector();
    setupChart();
    setupMenus();
    setupSignals();
    setupTimers();
    Util::SetupWhatsThis(this);
}

TrackLinePane::~TrackLinePane()
{
    deleteUI(ui);
}

void TrackLinePane::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Show_Axes,     "labplot-axis-vertical");
    Icons::defaultIcon(ui->action_Show_Legend,   "description");
    Icons::defaultIcon(ui->action_Reset_Zoom,    "zoom-fit-best");
    Icons::defaultIcon(ui->action_Zoom_to_Range, "zoom-fit-selection");
    Icons::defaultIcon(ui->action_Zoom_In_X,     "x-zoom-in");
    Icons::defaultIcon(ui->action_Zoom_Out_X,    "x-zoom-out");
    Icons::defaultIcon(ui->action_Adjust_Axes,   "zoom-best-fit");
    Icons::defaultIcon(ui->action_Pan_Left,      "arrow-left");
    Icons::defaultIcon(ui->action_Pan_Right,     "arrow-right");
    Icons::defaultIcon(ui->action_Page_Left,     "arrow-left-double");
    Icons::defaultIcon(ui->action_Page_Right,    "arrow-right-double");
}

void TrackLinePane::setupChart()
{
    ChartBase::setupChart();

    if (m_chart == nullptr)
        return;
    m_chart->addAxis(new QtCharts::QValueAxis(), Qt::AlignBottom);

    ui->verticalLayout->addWidget(&m_chartView);

    updateAxes();
}

void TrackLinePane::setupMenus()
{
    // Add actions to our widget, so key sequences work when we're focused or when the
    // pane header bar is hidden.
    for (auto* action : {
         ui->action_Page_Left,
         ui->action_Page_Right,
         ui->action_Pan_Left,
         ui->action_Pan_Right,
         ui->action_Zoom_In_X,
         ui->action_Zoom_Out_X,
         ui->action_Zoom_to_Range,
         ui->action_Adjust_Axes,
         ui->action_Reset_Zoom,
         ui->action_Show_Legend,
         ui->action_Show_Axes })
    {
        action->setShortcutContext(Qt::WidgetWithChildrenShortcut);
        addAction(action);
    }

    ui->showAxes->setDefaultAction(ui->action_Show_Axes);
    ui->showLegend->setDefaultAction(ui->action_Show_Legend);
    ui->xZoomIn->setDefaultAction(ui->action_Zoom_In_X);
    ui->xZoomOut->setDefaultAction(ui->action_Zoom_Out_X);
    ui->panLeft->setDefaultAction(ui->action_Pan_Left);
    ui->panRight->setDefaultAction(ui->action_Pan_Right);

    ui->action_Show_Axes->setChecked(true);
    ui->action_Show_Legend->setChecked(true);

    paneMenu.addActions({ ui->action_Pan_Left,
                          ui->action_Pan_Right,
                          ui->action_Page_Left,
                          ui->action_Page_Right });
    paneMenu.addSeparator();

    paneMenu.addActions({ ui->action_Zoom_In_X,
                          ui->action_Zoom_Out_X,
                          ui->action_Zoom_to_Range,
                          ui->action_Adjust_Axes,
                          ui->action_Reset_Zoom });
    paneMenu.addSeparator();

    paneMenu.addActions({ ui->action_Show_Legend,
                          ui->action_Show_Axes });

    setupActionContextMenu(paneMenu);
    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(&m_chartView, &ChartViewZoom::customContextMenuRequested, this, &TrackLinePane::showContextMenu);
}

void TrackLinePane::setupSignals()
{
    // Notify ourselves when the current point, track, or point selection changes
    connect(&mainWindow(), &MainWindow::currentTrackChanged,    this, &TrackLinePane::currentTrackChanged);
    connect(&mainWindow(), &MainWindow::currentTrackPointChanged, this, &TrackLinePane::currentPointChanged);
    connect(&mainWindow(), &MainWindow::selectedPointsChanged, this, &TrackLinePane::selectedPointsChanged);

    // Mouse panning
    connect(&m_chartView, &ChartViewZoom::mouseMove, this, &TrackLinePane::mouseMove);
    connect(&m_chartView, &ChartViewZoom::mousePan, this,
            static_cast<void(TrackLinePane::*)(QMouseEvent*, const QPoint&)>(&TrackLinePane::pan));
    connect(&m_chartView, &ChartViewZoom::mouseEndPan, this, &TrackLinePane::endPan);
    connect(&m_chartView, &ChartViewZoom::mouseRelease, this, &TrackLinePane::mouseRelease);

    // Row deletion from model
    connect(&app().trackModel(), &TrackModel::rowsAboutToBeRemoved, this, &TrackLinePane::processRowsAboutToBeRemoved);
    // Model reset
    connect(&app().trackModel(), &TrackModel::modelAboutToBeReset, this, &TrackLinePane::processModelAboutToBeReset);
}

void TrackLinePane::setupTimers()
{
    // Wait a little bit after the last active container change, and then resize columns.
    m_axisUpdateTimer.setSingleShot(true);
    connect(&m_axisUpdateTimer, &QTimer::timeout, this, &TrackLinePane::updateAxes);
}

void TrackLinePane::setupDataSelector()
{
    // Populate combobox for column selection
    m_graphDataModel.appendRow(new QStandardItem(tr("Chart Data")));

    ModelMetaData::setupComboBox<PointModel>(*ui->graphData, m_graphDataModel, nullptr,
                                           [this](ModelType mt, QStandardItem* item) {
                                                item->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
                                                item->setCheckState(mt == PointModel::Ele ? Qt::Checked : Qt::Unchecked);
                                                m_columnMap[mt] = m_graphDataModel.rowCount();
                                                return item;
                                           },
                                           [](ModelType mt) { return PointModel::mdIsChartable(mt); });

    // connect column select behavior
    connect(&m_graphDataModel, &QStandardItemModel::itemChanged, this, &TrackLinePane::toggleColumn);
}

bool TrackLinePane::hasSelection() const
{
    const QtCharts::QLineSeries* selectL = getMarker(Marker::SelectionLeft);
    const QtCharts::QLineSeries* selectR = getMarker(Marker::SelectionRight);

    return selectL != nullptr && selectR != nullptr &&
            selectL->count() > 0 && selectR->count() > 0;
}

// enable proper set of actions for current state
void TrackLinePane::enableActions()
{
    ui->action_Pan_Left->setEnabled(xBegin < 1.0f);
    ui->action_Page_Left->setEnabled(xBegin < 1.0f);
    ui->action_Pan_Right->setEnabled(xBegin > 0.0f);
    ui->action_Page_Right->setEnabled(xBegin > 0.0f);
    ui->action_Zoom_In_X->setEnabled(zoomLevel > m_minZoom);
    ui->action_Zoom_Out_X->setEnabled(zoomLevel < m_maxZoom);
    ui->action_Zoom_to_Range->setEnabled(hasSelection());
}

void TrackLinePane::showContextMenu(const QPoint& pos)
{
    enableActions();
    paneMenu.exec(m_chartView.mapToGlobal(pos));
}

inline qreal TrackLinePane::trackTotalDistanceUnits() const
{
    const PointModel* model = currentPointModel();
    if (model == nullptr)
        return 1.0;

    return cfgData().unitsTrkLength.toDouble(model->trackTotalDistance());
}

inline qreal TrackLinePane::xViewExtentInUnits() const
{
    return trackTotalDistanceUnits() * qreal(zoomLevel);
}

inline qreal TrackLinePane::xAxisStart() const
{
    return qreal(xBegin) * trackTotalDistanceUnits();
}

void TrackLinePane::initXAxis(QtCharts::QLineSeries* series)
{
    series->attachAxis(axisX(m_chart));
}

void TrackLinePane::initYAxis(QtCharts::QLineSeries* series, const Units& yAxisUnits, qreal yMin, qreal yMax)
{
    auto* yAxis = new QtCharts::QValueAxis();

    const int minorTickCount = (m_chart->plotArea().height() >= 160) ? 4 : 0;

    yAxis->setMinorTickCount(minorTickCount);  // ...
    yAxis->setLabelsColor(series->color().rgb()); // rgb() to avoid alpha channel for labels.
    yAxis->setLinePen(m_majorGridPen);
    yAxis->setGridLinePen(m_majorGridPen);
    yAxis->setMinorGridLinePen(m_minorGridPen);
    yAxis->setLabelFormat(yAxisUnits.chartLabelFormat(yMax));
    yAxis->setLabelsVisible(true);
    m_chart->addAxis(yAxis, Qt::AlignLeft);

    // see comment above looseNiceNumbers for motivation to avoid QValueAxis::applyNiceNumbers()
    int tickCount = 2;       // TODO: react to height
    looseNiceNumbers(yMin, yMax, tickCount);

    yAxis->setTickCount(tickCount);
    yAxis->setRange(yMin, yMax);
    series->attachAxis(yAxis);
}

// Create series for drawing the markers
void TrackLinePane::initMarkers()
{
    for (int m = 0; m < int(Marker::_Count); ++m) {
        if (m_marker.at(m) != nullptr)
            continue;

        m_marker.at(m) = new QtCharts::QLineSeries();
        m_marker.at(m)->setUseOpenGL(false); // using GL interferes with mouse drag

        float width = 1.0;
        QColor color;
        Qt::PenStyle style = Qt::SolidLine;

        switch (Marker(m)) {
        case Marker::Current:
            color = cfgData().trkPtMarkerColor;
            width = cfgData().trkPtMarkerWidth;
            break;
        case Marker::SelectionTop:   [[fallthrough]];
        case Marker::SelectionBottom:
            color = cfgData().trkPtRangeColor;
            width = cfgData().trkPtRangeWidth;
            break;
        case Marker::SelectionLeft:  [[fallthrough]];
        case Marker::SelectionRight:
            color = cfgData().trkPtRangeColor;
            style = Qt::DashLine;
            width = 1.0;
            break;

        default: assert(0);
        }

        m_marker.at(m)->setPen(QPen(color, width, style));
        m_marker.at(m)->setPointsVisible(true);
        m_chart->addSeries(m_marker.at(m));

        // Disable legend marker for the marker
        m_chart->legend()->markers().back()->setVisible(false);
    }
}

QtCharts::QLineSeries* TrackLinePane::newSeries(int col)
{
    auto* series = new QtCharts::QLineSeries();
    series->setUseOpenGL(true); // without this, performance is catastrophic.
    series->setName(currentPointModel()->headerData(col, Qt::Horizontal).toString());

    const QPen seriesPen(cfgData().trkPtColor[col], qreal(cfgData().trkPtLineWidth));
    series->setPen(seriesPen);

    series->setPointsVisible(false);
    m_chart->addSeries(series); // add series to chart
    initXAxis(series);        // we don't recreate the X axis each time.

    return series;
}

qreal TrackLinePane::xAxisDistance(int xPos) const
{
    const Units& xAxisUnits = cfgData().unitsTrkLength;
    return std::max(xAxisUnits.fromDouble(m_chart->mapToValue(QPointF(xPos, 0.5)).x()), 0.0);
}

void TrackLinePane::setCurrent(const QModelIndex& idx) const
{
    const PointModel* points = currentPointModel();
    if (points == nullptr) // return if no point model found
        return;

    // Move map view to center on this point
    if (auto* mapPane = mainWindow().findPane<MapPane>(); mapPane != nullptr)
        mapPane->zoomTo(points->boundsBox({idx}));

    // Select in most recent point pane
    if (auto* pointPane = mainWindow().findPane<PointPane>(); pointPane != nullptr)
        pointPane->select(idx);
}

// On non-panning click, select this point
void TrackLinePane::mouseRelease(QMouseEvent* event)
{
    const PointModel* points = currentPointModel();
    if (points == nullptr) // return if no point model found
        return;

    const QModelIndex closestIdx = points->closestPoint(xAxisDistance(event->x()));
    if (!closestIdx.isValid())  // no closest point found
        return;

    if (bool(event->modifiers() & Qt::ShiftModifier)) {
        // Range select on shift-click
        if (auto* pointPane = mainWindow().findPane<PointPane>(); pointPane != nullptr) {
            const QModelIndex currentIdx = Util::MapDown(pointPane->selectionModel()->currentIndex());
            if (!currentIdx.isValid()) {
                setCurrent(currentIdx);  // no current item: just set one
            } else {
                pointPane->selectRange(closestIdx, currentIdx);
            }
        }
    } else {
        // set current on non-shift click
        setCurrent(closestIdx);
    }
}

bool TrackLinePane::isChecked(int col) const
{
    return m_columnMap.at(col) >= 0 &&
        m_graphDataModel.item(m_columnMap.at(col))->checkState() == Qt::Checked;
}

void TrackLinePane::updateChart()
{
    const PointModel* model = currentPointModel();
    if (model == nullptr || model->trackTotalPoints() < 2) {
        clearChart();
        return;
    }

    const Units& xAxisUnits = cfgData().unitsTrkLength;

    const auto seriesList     = m_chart->series();
    const bool existingSeries = !seriesList.isEmpty();

    // Remove old Y axes
    for (auto& yAxis : m_chart->axes(Qt::Vertical)) {
        m_chart->removeAxis(yAxis);
        delete yAxis;
    }

    QVector<QPointF> points;
    points.reserve(model->trackTotalPoints());

    m_valueColumn.clear();

    int pos = 0;
    // Go backwards, to draw higher priority columns over lower.
    for (int col = PointModel::_Count - 1; col >= PointModel::_First; --col) {
        if (!isChecked(col))  // Skip if not plotted, or unchartable
            continue;

        m_valueColumn.append(col);
        const Units& yAxisUnits = model->units(col);

        int   row  = 0;
        qreal yMin = std::numeric_limits<qreal>::max();
        qreal yMax = -std::numeric_limits<qreal>::max();

        points.clear(); // clear vector between columns

        // Add track point data to the chart.
        // Per Qt documentation, QXYSeries::replace() with a vector argument is much faster than
        // repeated appends.  So that's what we'll do here.
        const PointItem* prevPt = nullptr;
        for (const auto& trkSeg : *model) {
            for (const auto& pt : trkSeg) {
                if (prevPt != nullptr && prevPt->hasDistance() && prevPt->hasData(col, &pt)) {
                    const auto yVal        = model->ptData<qreal>(*prevPt, &pt, col, row++, true);
                    const qreal yValInUnits = yAxisUnits.toDouble(yVal);
                    const qreal distInUnits = xAxisUnits.toDouble(double(pt.distance()));

                    yMin = std::fmin(yMin, yValInUnits);
                    yMax = std::fmax(yMax, yValInUnits);

                    points.append(QPointF(distInUnits, yValInUnits));
                }

                prevPt = &pt;
            }
        }

        // reuse existing series if possible to avoid dynamic allocations
        QtCharts::QLineSeries* series = 
            existingSeries ? static_cast<decltype(series)>(seriesList.at(pos++)) : newSeries(col);

        series->replace(points);  // add our data to series
        initYAxis(series, yAxisUnits, yMin, yMax);
    }

    initMarkers();
    updateAxes();
}

void TrackLinePane::clearChart()
{
    // Remove current series
    m_chart->removeAllSeries();
    m_marker.fill(nullptr);  // these are deleted by removeAllSeries() just above
    m_valueColumn.clear();
}

void TrackLinePane::refreshChart()
{
    clearChart();
    updateChart();
}

void TrackLinePane::setAxesShown(bool shown) const
{
    ChartBase::setAxesShown(shown);
    ui->action_Show_Axes->setChecked(shown);
}

void TrackLinePane::setLegendShown(bool shown) const
{
    ChartBase::setLegendShown(shown);
    ui->action_Show_Legend->setChecked(shown);
}

bool TrackLinePane::axesShown() const
{
    return ui != nullptr && ui->action_Show_Axes->isChecked();
}

bool TrackLinePane::legendShown() const
{
    return ui != nullptr && ui->action_Show_Legend->isChecked();
}

void TrackLinePane::updateXAxis()
{
    const PointModel* model = currentPointModel();
    if (m_chart == nullptr || model == nullptr || model->trackTotalPoints() == 0)
        return;

    const Units& xAxisUnits = cfgData().unitsTrkLength;

    const qreal distanceMeters = model->trackTotalDistance() * qreal(zoomLevel);
    const qreal distanceUnits  = xViewExtentInUnits();
    const QString labelFormat  = xAxisUnits.chartLabelFormat(distanceUnits);

    // Calculate how many label ticks to display.
    // Allow for a bit of padding between labels
    // TODO: should probably round distanceMeters to match label rounding.
    const int padWidth   = QApplication::fontMetrics().size(Qt::TextSingleLine, "M").width() * 4;
    const QString labelForWidth = xAxisUnits(distanceMeters);
    const int labelWidth = QApplication::fontMetrics().size(Qt::TextSingleLine, labelForWidth).width() + padWidth;

    const int minorTickCount = (labelWidth > 40) ? 4 : 0;

    auto* xAxis = qobject_cast<QtCharts::QValueAxis*>(axisX(m_chart));

    // See comment above looseNiceNumbers for motivation of not using QValueAxis::applyNiceNumbers()
    int tickCount  = int(m_chart->plotArea().width() / std::max(labelWidth, 1));
    qreal minVal = xAxisStart();
    qreal maxVal = minVal + qreal(distanceUnits);
    looseNiceNumbers(minVal, maxVal, tickCount);

    xAxis->setTickCount(tickCount);
    xAxis->setMinorTickCount(minorTickCount);
    xAxis->setRange(minVal, maxVal);
    xAxis->setLabelsVisible(true);
    xAxis->setLinePen(m_majorGridPen);
    xAxis->setGridLinePen(m_majorGridPen);
    xAxis->setMinorGridLinePen(m_minorGridPen);
    xAxis->setLabelsColor(m_labelNormal);
    xAxis->setLabelFormat(labelFormat);
}

void TrackLinePane::updateYAxis()
{
    // No dynamic changes: everything is handled during creation.
}

void TrackLinePane::updateAxes()
{
    updateXAxis();
    updateYAxis();
    setAxesShown(axesShown());
    updateMarkers();
}

void TrackLinePane::resizeEvent(QResizeEvent* event)
{
    ChartBase::resizeEvent(event);

    using namespace std::chrono_literals;
    m_axisUpdateTimer.start(100ms);
}

void TrackLinePane::wheelEvent(QWheelEvent* event)
{
    if (event->angleDelta().y() > 0)
        zoomIn(m_zoomStepWheel);
    else if (event->angleDelta().y() < 0)
        zoomOut(m_zoomStepWheel);
}

void TrackLinePane::zoomIn(float val)
{
    setZoom(zoomLevel / val);
}

void TrackLinePane::zoomOut(float val)
{
    setZoom(zoomLevel * val);
}

void TrackLinePane::setZoom(float val)
{
    if (m_chart == nullptr)
        return;

    const decltype(zoomLevel) prevZoom = zoomLevel;
    zoomLevel = Util::Clamp(val, m_minZoom, m_maxZoom);

    // Update chart start position to zoom in around cursor
    if (const QtCharts::QLineSeries* cursor = getMarker(Marker::Current); cursor != nullptr && cursor->count() > 0)
        xBegin += (prevZoom - zoomLevel) * cursor->at(0).x();

    updateXRange();
}

void TrackLinePane::updateXRange()
{
    xBegin = std::clamp(xBegin, 0.0f, 1.0f);

    auto* xAxis = qobject_cast<QtCharts::QValueAxis*>(axisX(m_chart));
    xAxis->setRange(xAxisStart(), xAxisStart() + xViewExtentInUnits());

    updateMarkers();
}

void TrackLinePane::resetPanZoom()
{
    zoomLevel = 1.0;
    xBegin    = 0.0;
}

void TrackLinePane::pan(const QPoint& rel)
{
    const PointModel* model = currentPointModel();
    if (model == nullptr)
        return;

    auto* xAxis = qobject_cast<QtCharts::QValueAxis*>(axisX(m_chart));

    // Reset zoom level to whatever it currently is.
    zoomLevel = float((xAxis->max() - xAxis->min()) / trackTotalDistanceUnits());

    // X: ...
    xBegin += float(-rel.x()) * zoomLevel / float(std::max(m_chartView.width(), 1));

    updateXRange();
}

void TrackLinePane::pan(QMouseEvent* event, const QPoint& rel)
{
    // Only left click drag
    if ((event->buttons() & Qt::LeftButton) == 0)
        return;

    pan(rel);
}

void TrackLinePane::updateMarkers()
{
    // Update current marker and text, if active
    if (const QtCharts::QLineSeries* series = getMarker(Marker::Current); series != nullptr) {
        if (series != nullptr && series->count() > 0) {
            const qreal markerXPos = series->at(0).x() * m_chart->plotArea().width() + m_chart->plotArea().x();
            drawMarkerText(markerXPos);
        }
    }

    // Update selection range markers
    drawSelectionMarkers();
}

void TrackLinePane::endPan()
{
    updateMarkers();
    enableActions();
}

void TrackLinePane::mouseMove(QMouseEvent* event)
{
    if (m_chart->plotArea().contains(event->localPos()))
        drawMarker(Marker::Current, event->x());
    else
        clearMarker(Marker::Current);
}

void TrackLinePane::toggleColumn(QStandardItem*)
{
    refreshChart();
}

void TrackLinePane::processRowsAboutToBeRemoved(const QModelIndex& parent, int first, int last)
{
    if (!m_currentTrackIdx.isValid())
        return;

    if (m_currentTrackIdx.parent() == parent && m_currentTrackIdx.row() >= first && m_currentTrackIdx.row() <= last) {
        setCurrentTrack(QModelIndex());
        updateChart();
    }
}

QString TrackLinePane::singleMarkerText(const PointModel* model, const QColor& color,
                                        ModelType mt, const QVariant& val,
                                        const QString& pre, const QString& post)
{
    if (!val.isValid())
        return { };

    // TODO: if perf mattered, this could be cached since it doesn't change much
    const QColor defTextColor = QGuiApplication::palette().color(QPalette::Text);
    const Units& yAxisUnits = model->units(mt);
    const QColor colorToDraw = color.isValid() ? color : defTextColor;

    return pre + QString("<font color=\"") + colorToDraw.name() + "\">" + yAxisUnits(val) + "</font>" + post;
}

void TrackLinePane::drawMarkerText(qreal xPos)
{
    const PointModel* model = currentPointModel();
    if (model == nullptr)
        return;

    // Test suite crash in debug build without this, but why?  Should be handled by
    // currentChanged signal.  Perhaps a timing problem around which signal fires first.
    cfgDataWritable().pointColorizer.setModel(model);

    // Find distance of the marker, in native distance units (meters)
    const qreal distance = xAxisDistance(xPos);
    const QModelIndex closestIdx = model->closestPoint(distance);

    if (!closestIdx.isValid())
        return;

    // Helpfully add time and distance, if not plotted.
    const QVector<std::tuple<ModelType, const char*, QLabel*, QLabel*>> auxData = {
        { PointModel::Distance, "ruler", ui->distIcon, ui->distLabel },
        { PointModel::Elapsed,  "clock", ui->timeIcon, ui->timeLabel },
    };

    // Add distance and elapsed time indication
    for (const auto& aux : auxData) {
        const ModelType md   = std::get<ModelType>(aux);
        const char* iconName = std::get<const char*>(aux);
        QLabel* iconLabel    = std::get<2>(aux);
        QLabel* dataLabel    = std::get<3>(aux);

        dataLabel->clear();
        iconLabel->clear(); // clear old icon, in case we don't draw it

        if (!m_valueColumn.contains(md)) {
            const QVariant val = model->interpolate(distance, md);
            dataLabel->setText(singleMarkerText(model, cfgData().trkPtColor[md], md, val, "", " |"));

            if (val.isValid()) {
                const QIcon icon = Icons::get(iconName);
                const int size = QFontMetrics(dataLabel->font()).height();
                iconLabel->setPixmap(icon.pixmap(icon.actualSize(QSize(size, size))));
            }
        }
    }

    QString labelText;
    for (ModelType mt : m_valueColumn) {
        const auto val = model->interpolate<QVariant>(distance, mt);

        labelText += singleMarkerText(model, cfgData().trkPtColor[mt], mt, val);

        // Helpfully add grade if elevation is being plotted
        if (mt == PointModel::Ele && !m_valueColumn.contains(PointModel::Grade)) {
            const QVariant grade = model->interpolate(distance, PointModel::Grade);
            // Colorize the grade with the point colorizer
            const auto color = cfgData().pointColorizer.colorize(model->rowSibling(PointModel::Grade, closestIdx),
                                                                 Qt::ForegroundRole).value<QColor>();

            if (grade.isValid())
                labelText += singleMarkerText(model, color, PointModel::Grade, grade, " (", ")");
        }

        if (val.isValid())
            labelText += " | ";
    }

    if (labelText.count() > 2)
        labelText.resize(labelText.count() - 3); // remove trailing separator
    ui->dataLabel->setText(labelText);
}

void TrackLinePane::clearMarker(Marker marker)
{
    if (getMarker(marker) != nullptr)
        getMarker(marker)->clear();
}

void TrackLinePane::drawMarker(Marker marker, qreal xPosL, qreal xPosR)
{
    if (getMarker(marker) == nullptr || m_chart == nullptr)
        return;

    // Remove old marker line
    clearMarker(marker);

    QtCharts::QLineSeries* series = getMarker(marker);

    // Draw in unit axis from [0..1]
    const qreal xPos01L = (xPosL - m_chart->plotArea().x()) / m_chart->plotArea().width();
    const qreal xPos01R = (xPosR - m_chart->plotArea().x()) / m_chart->plotArea().width();

    switch (marker) {
    case Marker::Current:
        // Draw vertical line at the horizontal position of this item.
        series->append(xPos01L, 0.0);
        series->append(xPos01L, 1.0);
        drawMarkerText(xPosL);
        break;
    case Marker::SelectionTop:
        series->append(xPos01L, 1.0);
        series->append(xPos01R, 1.0);
        break;
    case Marker::SelectionBottom:
        series->append(xPos01L, 0.0);
        series->append(xPos01R, 0.0);
        break;
    case Marker::SelectionLeft:
        series->append(xPos01L, 0.0);
        series->append(xPos01L, 1.0);
        break;
    case Marker::SelectionRight:
        series->append(xPos01R, 0.0);
        series->append(xPos01R, 1.0);
        break;
    default:
        assert(0);
        break;
    }
}

QPointF TrackLinePane::positionForIdx(const QModelIndex& idx) const
{
    const PointModel* model = currentPointModel();
    if (m_chart == nullptr || model == nullptr)
        return { };

    const auto series = m_chart->series();
    if (series.isEmpty())
        return { };

    const Units& xAxisUnits   = cfgData().unitsTrkLength;
    const qreal distanceM     = model->data(PointModel::Distance, Util::MapDown(idx), Util::RawDataRole).toDouble();
    const qreal distanceUnits = xAxisUnits.toDouble(distanceM);

    return m_chart->mapToPosition(QPointF(distanceUnits, 0.0), series.front());
}

void TrackLinePane::clearSelectionMarkers()
{
    clearMarker(Marker::SelectionTop);
    clearMarker(Marker::SelectionBottom);
    clearMarker(Marker::SelectionLeft);
    clearMarker(Marker::SelectionRight);

    m_rangeLIdx = QModelIndex();
    m_rangeRIdx = QModelIndex();

    enableActions();
}

void TrackLinePane::setCurrentTrack(const QModelIndex& pointModelIdx)
{
    // Disconnect data changed signal from old track
    if (PointModel* pm = currentPointModel(); pm != nullptr)
        disconnect(pm, nullptr,  this, nullptr);

    m_currentTrackIdx = pointModelIdx;

    // Connect model reset signal from current track point model
    if (PointModel* pm = currentPointModel(); pm != nullptr)
        connect(pm, &PointModel::modelReset,   this, &TrackLinePane::pointDataChanged);
}

void TrackLinePane::pointDataChanged()
{
    // If any point data changes, we rebuild the whole chart, rather than faffing about
    // trying to work out exactly what happened.
    updateChart();
}

void TrackLinePane::currentTrackChanged(const QModelIndex& current)
{
    // Return if no row change.
    if (m_currentTrackIdx.model() == current.model() && m_currentTrackIdx.row() == current.row())
        return;

    setCurrentTrack(current.sibling(current.row(), 0));

    clearSelectionMarkers();
    resetPanZoom();
    updateChart();
}

void TrackLinePane::currentPointChanged(const QModelIndex& current)
{
    drawMarker(Marker::Current, positionForIdx(current).x());
}

void TrackLinePane::selectedPointsChanged(const QItemSelectionModel* selectionModel,
                                          const QItemSelection&, const QItemSelection&)
{
    // Init and clear old markers
    initMarkers();
    clearSelectionMarkers();

    const PointModel* model = currentPointModel();

    if (m_chart == nullptr || model == nullptr || selectionModel == nullptr)
        return;

    const auto& ranges = selectionModel->selection();
    if (ranges.isEmpty())
        return;

    m_rangeLIdx = ranges.front().topLeft();
    m_rangeRIdx = ranges.back().bottomRight();

    drawSelectionMarkers();
    enableActions();
}

void TrackLinePane::drawSelectionMarkers()
{
    // We can't use topLeft/bottomRight since those return only the first tree level
    const qreal xPosL = positionForIdx(m_rangeLIdx).x();
    const qreal xPosR = positionForIdx(m_rangeRIdx).x();

    // Don't draw nearly empty ranges
    if (std::abs(xPosR - xPosL) < 1.0)
        return;

    drawMarker(Marker::SelectionTop,    xPosL, xPosR);
//  drawMarker(Marker::SelectionBottom, xPosL, xPosR);  // skip bottom for aesthetics
    drawMarker(Marker::SelectionLeft,   xPosL, xPosR);
    drawMarker(Marker::SelectionRight,  xPosL, xPosR);
}

void TrackLinePane::processModelAboutToBeReset()
{
    setCurrentTrack(QModelIndex());
    updateChart();
}

// TODO: share with PointPane
PointModel* TrackLinePane::currentPointModel()
{
    if (!m_currentTrackIdx.isValid())
        return nullptr;

    return app().trackModel().geoPoints(m_currentTrackIdx);
}

// TODO: share with PointPane
const PointModel* TrackLinePane::currentPointModel() const
{
    return const_cast<const PointModel*>(const_cast<TrackLinePane*>(this)->currentPointModel());
}

void TrackLinePane::newConfig()
{
    ChartBase::newConfig();

    refreshChart();
    setZoom(zoomLevel);

    // This seems necessary to refresh the legend labels.  Unclear why.
    m_chart->legend()->setVisible(false);
    m_chart->legend()->setVisible(axesShown());
    m_chart->legend()->setAlignment(Qt::AlignBottom);
}

void TrackLinePane::showAll()
{
    resetPanZoom();
    updateAxes();
}

void TrackLinePane::on_action_Show_Axes_toggled(bool shown)
{
    setAxesShown(shown);
}

void TrackLinePane::on_action_Show_Legend_toggled(bool shown)
{
    setLegendShown(shown);
}

void TrackLinePane::on_action_Reset_Zoom_triggered()
{
    showAll();
    enableActions();
}

void TrackLinePane::on_action_Zoom_In_X_triggered()
{
    zoomIn(m_zoomStepMenu);
    enableActions();
}

void TrackLinePane::on_action_Zoom_Out_X_triggered()
{
    zoomOut(m_zoomStepMenu);
    enableActions();
}

void TrackLinePane::on_action_Pan_Left_triggered()
{
    pan(-m_chart->plotArea().width() * 0.1, 0.0);
    endPan();
}

void TrackLinePane::on_action_Pan_Right_triggered()
{
    pan(m_chart->plotArea().width() * 0.1, 0.0);
    endPan();
}

void TrackLinePane::on_action_Page_Left_triggered()
{
    pan(-m_chart->plotArea().width() * 0.8, 0.0);
    endPan();
}

void TrackLinePane::on_action_Page_Right_triggered()
{
    pan(m_chart->plotArea().width() * 0.8, 0.0);
    endPan();
}

void TrackLinePane::save(QSettings& settings) const
{
    ChartBase::save(settings);

    MemberSave(settings, zoomLevel);
    MemberSave(settings, xBegin);

    if (ui != nullptr) {
        settings.beginWriteArray("graphData"); {
            // Row 0 is the header
            for (int row = 1; row < m_graphDataModel.rowCount(); ++row) {
                settings.setArrayIndex(row);
                SL::Save(settings, "checked", m_graphDataModel.item(row)->checkState() == Qt::Checked);
                SL::Save(settings, "name", m_graphDataModel.data(m_graphDataModel.index(row, 0)).toString());
            }
        } settings.endArray();
    }
}

void TrackLinePane::load(QSettings& settings)
{
    ChartBase::load(settings);

    MemberLoad(settings, zoomLevel);
    MemberLoad(settings, xBegin);

    if (ui != nullptr) {
        const int eleRow = m_columnMap.at(PointModel::Ele);  // default check the elevation entry
        const int rows = std::min(m_graphDataModel.rowCount(), settings.beginReadArray("graphData")); {
            for (int row = 0; row < rows; ++row) {
                settings.setArrayIndex(row);
                const auto name = SL::Load<QString>(settings, "name", "");
                // String search, in case the displayed columns differ between save state and model.
                if (const auto items = m_graphDataModel.findItems(name); !items.isEmpty())
                    items.front()->setCheckState(SL::Load(settings, "checked", row == eleRow) ?
                                                     Qt::Checked : Qt::Unchecked);
            }
        } settings.endArray();
    }
}

void TrackLinePane::on_action_Zoom_to_Range_triggered()
{
    const PointModel* model = currentPointModel();

    if (!hasSelection() || model == nullptr)
        return;

    const qreal distanceL = model->data(PointModel::Distance, Util::MapDown(m_rangeLIdx), Util::RawDataRole).toDouble();
    const qreal distanceR = model->data(PointModel::Distance, Util::MapDown(m_rangeRIdx), Util::RawDataRole).toDouble();
    const qreal trackDistance = model->trackTotalDistance();

    xBegin    = distanceL / trackDistance;
    zoomLevel = distanceR / trackDistance - xBegin;

    updateXRange();
}

void TrackLinePane::on_action_Adjust_Axes_triggered()
{
    updateAxes();
}
