/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <chrono>

#include <QSet>
#include <QModelIndexList>

#include <src/util/icons.h>
#include <src/util/util.h>
#include <src/util/ui.h>
#include <src/util/roles.h>
#include <src/ui/panes/datacolumnpanebase.inl.h>

#include "src/ui/windows/mainwindow.h"
#include "src/ui/panes/mappane.h"
#include "src/core/app.h"
#include "src/core/waypointmodel.h"
#include "src/core/waypointitem.h"

#include "waypointpane.h"
#include "mapdatapane.inl.h"
#include "ui_waypointpane.h"

WaypointPane::WaypointPane(MainWindow& mainWindow, QWidget *parent) :
    MapDataPane(mainWindow, PaneClass::Waypoint, std::get<0>(WaypointModel::getItemNameStatic()), parent),
    NamedItem(WaypointModel::getItemNameStatic()),
    ui(new Ui::WaypointPane),
    m_model(app().waypointModel())
{
    ui->setupUi(this);

    setupView(ui->waypointView, &app().waypointModel());
    setWidgets<WaypointModel>(WaypointPane::defColumnView(),
                              ui->filterWaypoint, ui->filterColumn, ui->showColumns, ui->filterCtrl, ui->filterIsValid);

    setupActionIcons();
    setupContextMenus();
    setupSignals();
    setupDelegates();
    setupTimers();
    Util::SetupWhatsThis(this);

    WaypointPane::newConfig();
}

WaypointPane::~WaypointPane()
{
    deleteUI(ui);
}

void WaypointPane::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Reset_Waypoint_Note,     "gkt-restore-defaults");
    Icons::defaultIcon(ui->action_Edit,                    "document-edit");
    Icons::defaultIcon(ui->action_Set_Icon,                "preferences-desktop-filetype-association");
    Icons::defaultIcon(ui->action_Guess_Icon,              "insert-image");
    Icons::defaultIcon(ui->action_Change_Tags,             "tag");
    Icons::defaultIcon(ui->action_Select_Duplicates,       "edit-select");
    Icons::defaultIcon(ui->action_Select_All_Duplicates,   "edit-select");
    Icons::defaultIcon(ui->action_Unset_Icon,              "edit-clear");
    Icons::defaultIcon(ui->action_New_Waypoint,            "mark-location-symbolic");
}

void WaypointPane::setupContextMenus()
{
    paneMenu.addActions({ mainWindow().getMainAction(MainAction::ZoomToSelection) });
    paneMenu.addSeparator();
    paneMenu.addActions({ ui->action_New_Waypoint });
    paneMenu.addSeparator();
    paneMenu.addActions({ ui->action_Edit,
                          ui->action_Set_Icon,
                          ui->action_Guess_Icon,
                          ui->action_Change_Tags });
    paneMenu.addSeparator();
    paneMenu.addActions({ ui->action_Unset_Icon,
                          ui->action_Reset_Waypoint_Note });
    paneMenu.addSeparator();
    paneMenu.addActions({ mainWindow().getMainAction(MainAction::DeleteSelection),
                          mainWindow().getMainAction(MainAction::DuplicateSelection) });
    paneMenu.addSeparator();
    paneMenu.addActions({ ui->action_Select_Duplicates,
                          ui->action_Select_All_Duplicates });

    setupActionContextMenu(paneMenu);
    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(this, &WaypointPane::customContextMenuRequested, this, &WaypointPane::showContextMenu);
}

void WaypointPane::setupSignals()
{
    MapDataPane::setupSignals();

    // Double click
    QTreeView& view = *ui->waypointView;
    connect(&view, &QTreeView::doubleClicked, this, &WaypointPane::doubleClicked);

    // Selection changes
    connect(selectionModel(), &QItemSelectionModel::selectionChanged, this, &WaypointPane::processSelectionChanged);
    connect(&mainWindow(), &MainWindow::selectedWaypointsChanged, this, &WaypointPane::processSelectedWaypointsChanged);
}

void WaypointPane::setupDelegates()
{
    initDelegates({{ WaypointModel::Name,  &m_textDelegate },
                   { WaypointModel::Notes, &m_noteDelegate },
                   { WaypointModel::Tags,  &m_tagDelegate },
                   { WaypointModel::Flags, &m_flagDelegate }});
}

const WaypointPane::DefColumns& WaypointPane::defColumnView() const
{
    static const DefColumns cols = {
        { WaypointModel::Name,         true  },
        // WaypointModel::Icon is in Name column
        { WaypointModel::Tags,         true  },
        { WaypointModel::Notes,        true  },
        { WaypointModel::Flags,        true  },
        { WaypointModel::Lat,          false },
        { WaypointModel::Lon,          false },
        { WaypointModel::Ele,          false },
        { WaypointModel::Time,         false },
        { WaypointModel::Type,         true  },
        { WaypointModel::Symbol,       false },
        { WaypointModel::Source,       false },
    };

    // Verify we added everything.  It's not critical, but better practice.
    for (ModelType md = WaypointModel::_First; md < WaypointModel::_Count; ++md)
        assert(cols.findData(md) >= 0);

    return cols;
}

const QStringList& WaypointPane::iconSelectorPathsStatic()
{
    static const QStringList path = {":art/points", "[US NPS Symbols]:art/us-nps-symbols" };
    return path;
}

const QStringList& WaypointPane::iconSelectorPaths() const
{
    return iconSelectorPathsStatic();
}

TreeModel* WaypointPane::model()
{
    return &m_model;
}

const TreeModel* WaypointPane::model() const
{
    return &m_model;
}

// For performance reasons (selections are needed within the TrackMap render loop), we cache some results of
// the selection.
void WaypointPane::processSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected)
{
    m_selectionSummary.update(m_model, topFilter(), selectionModel(), selected, deselected);
    mainWindow().updateStatus(m_selectionSummary);
}

void WaypointPane::processSelectedWaypointsChanged()
{
    const bool hasSelection = selectionModel()->hasSelection();
    const bool hasVisible   = topFilter().rowCount() > 0;

    // Update which UI actions are enabled, given the selection state.
    ui->action_Reset_Waypoint_Note->setEnabled(hasSelection);
    ui->action_Set_Icon->setEnabled(hasSelection);
    ui->action_Unset_Icon->setEnabled(hasSelection);
    ui->action_Change_Tags->setEnabled(hasSelection);
    ui->action_Select_Duplicates->setEnabled(hasSelection);
    ui->action_Select_All_Duplicates->setEnabled(hasVisible);
}

void WaypointPane::showContextMenu(const QPoint& pos)
{
    // Enable menus as appropriate
    m_menuIdx = clickPosIndex(pos);

    const bool foundMapPane = (mainWindow().findPane<MapPane>() != nullptr);

    ui->action_Edit->setEnabled(WaypointModel::mdIsEditable(m_menuIdx.column()));
    ui->action_New_Waypoint->setEnabled(foundMapPane);

    paneMenu.exec(mapToGlobal(pos));
}

bool WaypointPane::hasAction(MainAction action) const
{
    switch (action)
    {
    case MainAction::ZoomToSelection:    [[fallthrough]];
    case MainAction::DeleteSelection:    [[fallthrough]];
    case MainAction::DuplicateSelection: return true;
    default:                             return false;
    }
}

void WaypointPane::newConfig()
{
    MapDataPane::newConfig();
    ui->waypointView->setIconSize(cfgData().iconSizeTrack);
}

void WaypointPane::on_action_Reset_Waypoint_Note_triggered()
{
    resetDataAction(WaypointModel::Notes, ui->action_Reset_Waypoint_Note);
}

void WaypointPane::updateVisibility()
{
    MapDataPane::updateVisibilityImpl<WaypointModel>();
    emit mainWindow().visibleWaypointsChanged(); // send notification
}

void WaypointPane::filterTextChanged(const QString& query)
{
    emit mainWindow().waypointQueryChanged(query);

    MapDataPane::filterTextChanged(query);

    using namespace std::chrono_literals;
    m_mapUpdateTimer.start(500ms);
}

void WaypointPane::on_action_Edit_triggered()
{
    if (m_menuIdx.isValid())
        ui->waypointView->edit(m_menuIdx);
}

void WaypointPane::on_action_Set_Icon_triggered()
{
    setIcon(WaypointModel::Icon);
}

void WaypointPane::on_action_Unset_Icon_triggered()
{
    clearIcon(WaypointModel::Icon);
}

void WaypointPane::on_action_Change_Tags_triggered()
{
    if (m_menuIdx.isValid())
        ui->waypointView->edit(m_menuIdx.sibling(m_menuIdx.row(), WaypointModel::Tags));
}

void WaypointPane::on_action_Select_Duplicates_triggered()
{
    selectDuplicates(false);
}

void WaypointPane::on_action_Select_All_Duplicates_triggered()
{
    selectDuplicates(true);
}

void WaypointPane::on_action_Guess_Icon_triggered()
{
    const QModelIndexList selections = getSelections();

    const QString undoName =  UndoMgr::genNameX(ui->action_Guess_Icon->text(), selections.size());
    const UndoMgr::ScopedUndo undoSet(app().undoMgr(), undoName);

    m_model.guessIcons(selections, false);
}

void WaypointPane::on_action_New_Waypoint_triggered()
{
    if (auto* mapPane = mainWindow().findPane<MapPane>(); mapPane != nullptr)
        mainWindow().getNewWaypointDialog().exec({ mapPane->center() });
}
