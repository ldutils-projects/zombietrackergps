/*
    Copyright 2021-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <chrono>

#include <QToolTip>
#include <QCursor>
#include <QLabel>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QBarSet>

#include <src/util/util.h>
#include <src/util/ui.h>
#include <src/util/icons.h>
#include <src/ui/misc/querycompleter.h>

#include "src/ui/windows/mainwindow.h"

#include "filterpane.h"
#include "trackpane.h"
#include "barchartbase.h"

BarChartBase::BarChartBase(MainWindow& mainWindow, PaneClass paneClass, QWidget *parent) :
    ChartBase(mainWindow, paneClass, parent),
    QueryBar(parent),
    m_graphDataModel(this),
    m_updateTimer(this),
    m_barSeries(nullptr),
    m_queryRoot(new Query::All()),
    m_plotColumn(-1),
    m_barWidth(25),
    m_trackQuery(nullptr),
    m_lockToQuery(nullptr),
    m_graphData(nullptr)
{
    setupTimers();
}

BarChartBase::~BarChartBase()
{
}

void BarChartBase::setupTimers()
{
    m_updateTimer.setSingleShot(true);
    connect(&m_updateTimer, &QTimer::timeout, this, &BarChartBase::updateChart);
}

void BarChartBase::setupSignals()
{
    connect(&app().trackModel(), &TrackModel::rowsInserted, this, &BarChartBase::processRowsInserted);
    connect(&app().trackModel(), &TrackModel::rowsRemoved, this, &BarChartBase::processRowsRemoved);
    connect(&app().trackModel(), &TrackModel::dataChanged, this, &BarChartBase::processDataChanged);

    connect(m_trackQuery, &QLineEdit::textChanged, this, &BarChartBase::queryTextChanged);
}

void BarChartBase::setupCompleter()
{
    if (m_trackQuery == nullptr)
        return;

    QueryBase::setSourceModel(&app().trackModel());
    m_queryCtx.setModel(&app().trackModel(), cfgData().getFilterCaseSensitivity());

    // the QLineEdit widget takes ownership of the completer: we don't delete or keep track of it.
    auto* completer = new QueryCompleter(m_queryCtx, m_trackQuery);
    m_trackQuery->setCompleter(completer);
}

void BarChartBase::setupDataSelector()
{
    ModelMetaData::setupComboBox<TrackModel>(*m_graphData, m_graphDataModel, nullptr,
                                             [this](ModelType mt, QStandardItem* item) {
                                                item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
                                                m_columnMap.append(mt);
                                                return item;
                                             },
                                             [](ModelType mt) { return TrackModel::mdIsChartable(mt); });

    // connect column select behavior
    connect(m_graphData, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this, &BarChartBase::setPlotColumn);
}

void BarChartBase::setPaneFilterBar(QHBoxLayout* filterCtrl, QComboBox* graphData)
{
    ChartBase::setPaneFilterBar(filterCtrl);
    m_graphData = graphData;

    if (m_paneFilterLayout != nullptr) {
        m_filterValid = filterCtrl->parentWidget()->findChild<QLabel*>();
        m_trackQuery  = filterCtrl->parentWidget()->findChild<QLineEdit*>();
        m_lockToQuery = filterCtrl->parentWidget()->findChild<QToolButton*>("lockToQuery");
    }

    assert(m_filterValid != nullptr && m_trackQuery != nullptr && m_lockToQuery != nullptr &&
           m_graphData != nullptr);
}

void BarChartBase::dragEnterEvent(QDragEnterEvent* event)
{
    // Allow drop from filters
    if (app().filterModel().isStreamMagic(event->mimeData()))
        event->acceptProposedAction();

    ChartBase::dragEnterEvent(event);
}

void BarChartBase::dropEvent(QDropEvent* event)
{
    // Drop from filter
    if (app().filterModel().isStreamMagic(event->mimeData())) {
        const auto srcIndexes = app().filterModel().getDropIndices(event->mimeData());
        assert(srcIndexes.size() == 1);

        if (auto* filterPane = mainWindow().findPane<FilterPane>(); filterPane != nullptr) {
            filterPane->setFilter(this);
            event->accept();
            return;
        }
    }

    ChartBase::dropEvent(event);
}

bool BarChartBase::isEmptyQuery() const
{
    return m_trackQuery->text().isEmpty();
}

bool BarChartBase::isValidQuery() const
{
    return m_queryRoot->isValid();
}

void BarChartBase::refreshChart(int delayMs)
{
    clearChart();

    if (delayMs > 0)
        m_updateTimer.start(delayMs);
    else
        updateChart();
}

void BarChartBase::showContextMenu(const QPoint& pos)
{
    updateActions();
    paneMenu.exec(m_chartView.mapToGlobal(pos));
}

void BarChartBase::setQuery(const QString& query)
{
    externQueryTextChanged(query);
}

void BarChartBase::setBarColorFromModel(const TrackModel& model, QtCharts::QBarSet* barSet, int modelRow)
{
    barSet->setColor(model.data(model.index(modelRow, TrackModel::Color), Qt::BackgroundRole).value<QColor>());
}

void BarChartBase::setLockToQuery(bool checked)
{
    if (checked) {
        connect(&mainWindow(), &MainWindow::trackQueryChanged, this, &BarChartBase::externQueryTextChanged, Qt::UniqueConnection);
        connect(m_trackQuery, &QLineEdit::textChanged, &mainWindow(), &MainWindow::trackQueryChanged, Qt::UniqueConnection);

        // Copy current track query so it gets copied to our filter text
        if (auto* trackPane = mainWindow().findPane<TrackPane>(); trackPane != nullptr)
            setQuery(trackPane->getQuery());
    } else {
        disconnect(&mainWindow(), &MainWindow::trackQueryChanged, this, &BarChartBase::externQueryTextChanged);
        disconnect(m_trackQuery, &QLineEdit::textChanged, &mainWindow(), &MainWindow::trackQueryChanged);
    }

    m_lockToQuery->setIcon(checked ? Icons::get("object-locked") :
                                     Icons::get("object-unlocked"));

    m_lockToQuery->setChecked(checked);
}

void BarChartBase::setBarWidth(int val)
{
    if (val == m_barWidth)
        return;

    m_barWidth = std::clamp(val, 4, 256);
    refreshChart(10);
}

void BarChartBase::setPlotColumn(int index)
{
    m_plotColumn = (index < m_columnMap.size()) ? m_columnMap.at(index) : -1;

    if (m_graphData != nullptr && m_plotColumn >= 0)
        m_graphData->setCurrentText(TrackModel::mdName(m_plotColumn));

    refreshChart(10);
}

void BarChartBase::queryTextChanged(const QString& query)
{
    emit mainWindow().trackQueryChanged(query);

    m_queryRoot = m_queryCtx.parse(query);
    QueryBase::updateFilter(query);

    using namespace std::chrono_literals;
    m_updateTimer.start(250ms);
}

void BarChartBase::externQueryTextChanged(const QString& query)
{
    const QSignalBlocker block(m_trackQuery);
    const QSignalBlocker mainWindowBlock(&mainWindow());

    m_trackQuery->setText(query);

    queryTextChanged(query);
}

void BarChartBase::processRowsInserted(const QModelIndex& /*parent*/, int /*first*/, int /*last*/)
{
    refreshChart();
}

void BarChartBase::processRowsRemoved(const QModelIndex& /*parent*/, int /*first*/, int /*last*/)
{
    refreshChart();
}

void BarChartBase::processDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector<int>&)
{
    // nothing to do unless modified index range includes dates, tags, colors, or the plot column
    if (!((topLeft.column() <= m_plotColumn          && m_plotColumn          <= bottomRight.column()) ||
          (topLeft.column() <= TrackModel::Tags      && TrackModel::Tags      <= bottomRight.column()) ||
          (topLeft.column() <= TrackModel::Color     && TrackModel::Color     <= bottomRight.column()) ||
          (topLeft.column() <= TrackModel::BeginDate && TrackModel::BeginDate <= bottomRight.column())))
        return;

    refreshChart();
}

void BarChartBase::showAll()
{
    m_trackQuery->clear();
}

void BarChartBase::newConfig()
{
    ChartBase::newConfig();
    QueryBase::newConfig();

    m_queryCtx.setCaseSensitivity(cfgData().getFilterCaseSensitivity());

    using namespace std::chrono_literals;
    m_updateTimer.start(10ms);
}

void BarChartBase::focusIn()
{
    mainWindow().updateStatus(m_selectionSummary);
}

void BarChartBase::save(QSettings& settings) const
{
    ChartBase::save(settings);

    SL::Save(settings, "barWidth", m_barWidth);
}

void BarChartBase::load(QSettings& settings)
{
    ChartBase::load(settings);

    setBarWidth(SL::Load(settings, "barWidth", 20));
}

