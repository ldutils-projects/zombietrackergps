/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAPPEDDATAPANE_H
#define MAPPEDDATAPANE_H

#include <QTimer>
#include <QModelIndexList>

#include <src/core/modelmetadata.h>
#include "src/core/selectionsummary.h"
#include "src/ui/widgets/lineeditdelegate.h"
#include <src/ui/widgets/modeltexteditordelegate.h>
#include "src/ui/widgets/tagselectordelegate.h"
#include "src/ui/widgets/flagdelegate.h"
#include "datacolumnpane.h"

class QAction;
class MainWindow;
class MapDataModel;

// Common base for panes (Track, Waypoint, etc) which handle map visible data.
class MapDataPane : public DataColumnPane
{
    Q_OBJECT

public:
    void         zoomToSelection() override;
    virtual void gotoSelection(const QModelIndexList&) const;

public slots:
    virtual void updateVisibility() = 0;

protected slots:
    void doubleClicked(const QModelIndex&);

protected:
    MapDataPane(MainWindow&, PaneClass, const QString& itemName, QWidget *parent = nullptr);
    ~MapDataPane() override;

    void setupTimers();
    void postLoadHook() override;
    void selectDuplicates(bool all);
    void focusIn() override;

    void resetDataAction(ModelType, const QAction*);

    template <class MODEL> void updateVisibilityImpl();

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

    LineEditDelegate         m_textDelegate;
    ModelTextEditorDelegate  m_noteDelegate;
    TagSelectorDelegate      m_tagDelegate;
    FlagDelegate             m_flagDelegate;
    QTimer                   m_mapUpdateTimer;
    QModelIndex              m_menuIdx;
    bool                     m_mostRecentFocus; // true if we're the most recently focused *track pane*

    // These are to avoid processing the entire selection list each time it changes.
    SelectionSummary    m_selectionSummary;
};

#endif // MAPPEDDATAPANE_H
