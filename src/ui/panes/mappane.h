/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAPPANE_H
#define MAPPANE_H

#include <QPoint>
#include <QStandardItemModel>
#include <QMenu>
#include <QAction>

#include <src/undo/undomgr.h>

#include "pane.h"
#include "src/ui/dialogs/mapthemedialog.h"
#include "src/ui/filters/geolocfilter.h"

namespace Ui {
class MapPane;
} // namespace Ui

namespace Marble {
class GeoDataLatLonBox;
class TileCoordsPyramid;
} // namespace Marble

class TrackMap;
class ViewModel;
class DistanceSpinBox;
class ViewParams;
class GeoLocCompleter;
class PointItem;

class MapPane final : public Pane
{
    Q_OBJECT

public:
    explicit MapPane(MainWindow&, QWidget* parent = nullptr);
    ~MapPane() override;

    void gotoView(const QModelIndex&);
    void gotoView(const ViewParams&, const QString& name);
    void zoomTo(const Marble::GeoDataLatLonBox&);
    void zoomTo(const Marble::GeoDataCoordinates&);
    void zoomTo(const PointItem&);
    void addViewPresetInteractive();
    void updateViewPreset(const QModelIndex&);
    void setOfflineMode(bool offline);
    void reloadVisible();
    void downloadTiles(const QVector<Marble::TileCoordsPyramid>&);
    Marble::GeoDataLatLonBox bounds() const;   // bounding box for screen view
    Marble::GeoDataCoordinates center() const; // center of pane
    int minimumTileLevel() const;
    int maximumTileLevel() const;
    int currentTileLevel() const;

    // Our map specific view undo manager
    [[nodiscard]] UndoMgr& undoMgrView() { return m_undoMgrView; }
    [[nodiscard]] const UndoMgr& undoMgrView() const { return m_undoMgrView; }

    // registration functions for tracking GPSD live points
    void registerGpsdPoint(const Pane&);              // register GPSD point
    void setGpsdPoint(const Pane&, const PointItem&); // set point for pane
    void unregisterGpsdPoint(const Pane&);            // unregister GPSD point

    // TODO: ... implement some of these by passing through to TrackPane
//    void showAll() override;
//    void expandAll() override;
//    void collapseAll() override;
//    void selectAll() override;
//    void selectNone() override;
    void resizeToFit(int defer = -1) override;
//    void copySelected() const override;
//    void viewAsTree(bool) override;
    void newConfig() override;

signals:
    void visibleAreaChanged(const Marble::GeoDataLatLonAltBox&);

private slots:
    void on_action_Orient_to_North_triggered();
    void on_action_Add_View_Preset_triggered();
    void on_action_Location_to_Clipboard_triggered();
    void on_action_Show_Compass_triggered(bool checked);
    void on_action_Show_Overview_triggered(bool checked);
    void on_action_Show_ScaleBar_triggered(bool checked);
    void on_MapPane_toggled(bool checked) { paneToggled(checked); }
    void on_mapThemeButton_clicked();
    void on_filterMode_currentIndexChanged(int index);
    void on_filterFeature_currentIndexChanged(int index);
    void on_filterMap_textEdited(const QString&);
    void on_filterMap_returnPressed();
    void on_action_Create_Waypoint_triggered();

    void toggleFilterFeature(QStandardItem* item);
    void showContextMenu(const QPoint&);
    void setupCompleter();
    void viewMoveIdle(); // received from map when view stops moving
    void filterDistChanged(double val);
    void showFilterContextMenu(const QPoint&);
    void execFilterFeatureMenu(QAction*);
    void filterNameActivated(const QModelIndex&);
    void handleMapAreaChanged(const Marble::GeoDataLatLonAltBox&);

private:
    friend class TestZtgps;

    enum FilterMode {
        _First,
        World = _First,
        VisibleArea,
        Distance,
        GeoPolCountry,
        GeoPolRegion,
        _Count,
    };

    friend class UndoMapBase;

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

    void setupActionIcons();
    void setupMapWidget();
    void setupSignals();
    void setupContextMenus();
    void setupFilterFeature();
    void setupFilterMode();
    void setupFilterDistance();
    void refreshMenus();
    void mapZoomWorkaround();
    void postLoadHook() override;
    void updateFilter();
    void useModel();
    void dragEnterEvent(QDragEnterEvent*) override;
    void dropEvent(QDropEvent*) override;

    GeoLocCompleter* completer();
    const GeoLocCompleter* completer() const;

    Ui::MapPane*         ui;                         // map pane UI

    UndoMgr              m_undoMgrView;              // view undoing for this pane
    GeoLocFilter         m_geoLocFilter;             // location filter
    TrackMap*            m_mapWidget;                // primary map display
    MapThemeDialog*      m_mapThemeDialog;           // for selecting the map theme
    union {
        DistanceSpinBox* m_filterDistance = nullptr; // distance filter spinner
        QDoubleSpinBox*  m_filterDistanceDSB;        // view as a double spinbox, for save/load
    };
    QStandardItemModel   m_filterFeatureModel;       // for combobox
    uint32_t             m_filterFeatureMask;        // which features to select
    QPoint               m_menuPos;                  // remember context menu position

    QMenu                m_filterFeatureMenu;        // context menu for filter features
};

#endif // MAPPANE_H
