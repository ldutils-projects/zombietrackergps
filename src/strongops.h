/*
    Copyright 2022-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef STRONGCONVERSIONS_H
#define STRONGCONVERSIONS_H

#include "fwddecl.h"

// Helper macro to save some boilerplate verbosity in declaring these ops.
#define STRONGOP_TYPE(RESULT, OP, ARG0, ARG0T, ARG1) \
    inline RESULT operator OP(ARG0 arg0, ARG1 arg1) \
    { \
        return RESULT(ARG0T(arg0) OP ARG1::base_type(arg1)); \
    }

#define STRONGOP(RESULT, OP, ARG0, ARG1) \
    STRONGOP_TYPE(RESULT, OP, ARG0, ARG0::base_type, ARG1)

// Define some known strong type conversions
STRONGOP(EnergyAccum_t, *, Power_t, Dur_t)  // energy = power * time
STRONGOP(Speed_t, /, Dist_t, Dur_t)         // speed = distance / time
STRONGOP(Dur_t, -, Elaps_t, Elaps_t)        // duration = time - time
STRONGOP(Hr_t, /, HrAccum_t, Dur_t)         // hr = hraccum / time
STRONGOP(Cad_t, /, CadAccum_t, Dur_t)       // cad = cadaccum / time

STRONGOP(HrAccum_t, *, Hr_t, Dur_t)         // hraccum = hr * time
STRONGOP(CadAccum_t, *, Cad_t, Dur_t)       // cadaccum = hr * time
STRONGOP(TempAccum_t, *, Temp_t, Dur_t)     // tempaccum = hr * time
STRONGOP(Dur_t, /, Dist_t, Speed_t)         // time = dist / speed

STRONGOP_TYPE(Power_t, /, EnergyAccum_t, double, Dur_t)  // power = energy / time
STRONGOP_TYPE(Temp_t, /, TempAccum_t, float, Dur_t)      // temp = tempaccum / time

#undef STRONGOP
#undef STRONGOP_TYPE

#endif // STRONGCONVERSIONS_H
