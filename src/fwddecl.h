/*
    Copyright 2020-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FWDDECL_H
#define FWDDECL_H

#include <QtGlobal>
#include <src/fwddeclbase.h>

// Declare some strongly typed numeric types for use throughout the project.
REGISTER_STRONGNUM(double,     Lat_t);
REGISTER_STRONGNUM(double,     Lon_t);
REGISTER_STRONGNUM(float,      Ele_t);
REGISTER_STRONGNUM(float,      Power_t);
REGISTER_STRONGNUM(float,      Grade_t);
REGISTER_STRONGNUM(float,      Dist_t);
REGISTER_STRONGNUM_EXP(float,  Speed_t);
REGISTER_STRONGNUM_EXP(float,  Accel_t);
REGISTER_STRONGNUM(uint8_t,    Hr_t);
REGISTER_STRONGNUM(uint8_t,    Cad_t);

// These don't need save/load support. They are converted to more compact types for that purpose,
// or are recalculated rather than saved.
REGISTER_STRONGNUM_INTERNAL(float,   Temp_t);        // temperature
REGISTER_STRONGNUM_INTERNAL(qint64,  EnergyAccum_t); // for watt*msec accumulations
REGISTER_STRONGNUM_INTERNAL(qint64,  HrAccum_t);     // for bpm*msec accumulations
REGISTER_STRONGNUM_INTERNAL(qint64,  CadAccum_t);    // for rpm*msec accumulations
REGISTER_STRONGNUM_INTERNAL(qint64,  TempAccum_t);   // for temp*msec accumulations

// We can't use std::int64_t for these, due to the interface with
// QDataStream. That generates a lot of ambiguous operator errors
// with 64 bit int types, unlike 8, 16, or 32 bit int types.
REGISTER_STRONGNUM_INTERNAL(qint64,   Elaps_t);
REGISTER_STRONGNUM_INT_EXP(qint64,    Dur_t);

#endif // FWDDECL_H
