/*
    Copyright 2020-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <cstdlib>
#include <array>
#include <bitset>

#include <QtGlobal>
#include <QProcess>
#include <QByteArray>
#include <QRegularExpression>
#include <QMenu>
#include <QAction>
#include <QTreeView>
#include <QTextBrowser>
#include <QToolButton>
#include <QCheckBox>
#include <QSpinBox>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QTimeEdit>
#include <QStandardItemModel>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QToolBar>
#include <QClipboard>
#include <QTimer>
#include <QPieSeries>
#include <QPieSlice>
#include <QBarSeries>
#include <QBarSet>
#include <QStackedWidget>
#include <QScrollArea>
#include <QLine>
#include <QFrame>
#include <QDir>

#include <src/util/qtcompat.h>
#include <src/util/resources.h>
#include <src/util/util.h>
#include <src/util/ui.h>
#include <src/core/newpanemodel.h>
#include <src/core/modelvarexpander.h>
#include <src/ui/dialogs/modeltexteditdialog.h>

#include "src/core/app.h"
#include "src/core/builddate.h"
#include "src/core/geopolmgr.h"
#include "src/core/geopolregion.h"
#include "src/core/geolocheader.h"
#include "src/core/viewparams.h"
#include "src/ui/filters/geolocfilter.h"
#include "src/ui/windows/mainwindow.h"
#include "src/ui/windows/docdialog.h"
#include "src/ui/windows/aboutdialog.h"
#include "src/ui/windows/appconfig.h"
#include "src/ui/widgets/tagselector.h"
#include "src/ui/widgets/trackmap.h"
#include "src/ui/dialogs/gpsdversiondialog.h"
#include "src/ui/dialogs/newpanedialog.h"
#include "src/ui/dialogs/newtrackdialog.h"
#include "src/ui/dialogs/importdialog.h"
#include "src/ui/panes/panegroup.h"
#include "src/ui/panes/viewpane.h"
#include "src/ui/panes/mappane.h"
#include "src/ui/panes/gpsdevicepane.h"
#include "src/ui/panes/trackpane.h"
#include "src/ui/panes/trackdetailpane.h"
#include "src/ui/panes/pointpane.h"
#include "src/ui/panes/zonepane.h"
#include "src/ui/panes/activitysummarypane.h"
#include "src/ui/panes/filterpane.h"
#include "src/ui/panes/emptypane.h"
#include "src/ui/panes/climbanalysispane.h"
#include "src/util/cmdline.h"
#include "src/geo-io/geoio.h"

#include "testztgps.h"
#include "resultcheck.h"
#include "models.h"

QString TestZtgps::m_stdout;  // stdout capture for cmd line tests
QString TestZtgps::m_stderr;  // stderr capture for cmd line tests

const QString TestZtgps::m_testQuery =
        "( ( Name ~ foo | Name ~ bar | Tags ~ \"test flags\" ) | !( Ele > 40m & Ele < 260m ) ) &"
        "( ( ( Flags ~ Kansas ) ) )";


// Old config file data for test which exercise loading these
decltype(TestZtgps::cfgDataVersions) TestZtgps::cfgDataVersions = {
    { "0x1000/cfgDataVersion.13", 0x1000U, 13U, 3U, 0U },
    { "0x1001/cfgDataVersion.16", 0x1001U, 16U, 3U, 23U },
    { "0x1001/cfgDataVersion.19", 0x1001U, 19U, 7U, 23U },
    { "0x1002/cfgDataVersion.20", 0x1002U, 20U, 7U, 23U },
    { "0x1002/cfgDataVersion.23", 0x1002U, 23U, 7U, 23U },
    { "0x1002/cfgDataVersion.25", 0x1002U, 25U, 7U, 23U },
    { "0x1003/cfgDataVersion.26", 0x1003U, 26U, 7U, 23U },
};

TestZtgps::TestZtgps() :
    m_privateSessionCL("testztgps", "1.0", BuildDate, { "testztgps", "--no-first-run", "--private-session", "--conf",
                       m_tmpDir.filePath("new-session-benchmark.conf") }),
    m_mainWindow_private(nullptr)
{
}

TestZtgps::~TestZtgps()
{
    deleteMainWindowPrivate(m_mainWindow_private);
}

void TestZtgps::captureOutErr(QtMsgType type, const QMessageLogContext& context, const QString& msg)
{
    switch (type) {
    case QtInfoMsg:     m_stdout += msg; m_stdout += "\n"; break;
    case QtDebugMsg:    [[fallthrough]];
    case QtWarningMsg:  [[fallthrough]];
    case QtCriticalMsg: m_stderr += msg; m_stderr += "\n"; break;
    default:            CmdLineBase::forwardMessage(0, type, context, msg);
    }
}

void TestZtgps::clearOutErr()
{
    m_stdout.clear();
    m_stderr.clear();
}

QString TestZtgps::testOutErrRc(const QString& stdout, const QString& stderrFromTest,
                                const QStringList& args, int rcProg)
{
    // For some infernal reason, when running external commands using QProcess::start, something goes
    // sideways with finding color schemes, resulting in stderr messages from Qt that have nothing to
    // do with ZTGPS. We trim those out here to avoid false positives. This is probably specific to
    // English locales, unfortunately.

    QString stderr = stderrFromTest;

    const QRegularExpression colorSchemeRe("(.*)(^.*not find color scheme.*$)(.*)",
                                           QRegularExpression::DotMatchesEverythingOption);

    const QRegularExpressionMatch match = colorSchemeRe.match(stderr);

    if (match.hasMatch()) {
        // Remove line about color scheme
        stderr = match.captured(1) + match.captured(3);
    }

    const char* rcTest = RcOK;

    QFETCH(int, rc);
    QFETCH(bool, regex);

    if (rcProg != rc)
        rcTest = RcBadRc;

    const QString goldOutFile = gold(".out.txt");
    const QString goldErrFile = gold(".err.txt");

    const std::string go = goldOutFile.toStdString();
    const std::string ge = goldErrFile.toStdString();

    if (rcTest == RcOK && (goldOutFile.isEmpty() || goldErrFile.isEmpty()))
        rcTest = RcResource;

    const auto goldOut = Util::ReadFile<QString>(goldOutFile, 1<<16, false);
    const auto goldErr = Util::ReadFile<QString>(goldErrFile, 1<<16, false);

    if (rcTest == RcOK) {
        if (regex) {
            const QRegularExpression stdoutRe(goldOut, QRegularExpression::DotMatchesEverythingOption);
            const QRegularExpression stderrRe(goldErr, QRegularExpression::DotMatchesEverythingOption);
            if (!goldOut.isEmpty() && !stdoutRe.match(stdout).hasMatch())
                rcTest = RcGoldOut;
            else if (!goldErr.isEmpty() && !stderrRe.match(stderr).hasMatch())
                rcTest = RcGoldErr;
        } else {
            if (goldOut != stdout)
                rcTest = RcGoldOut;
            else if (goldErr != stderr)
                rcTest = RcGoldErr;
        }
    }

    if (rcTest != RcOK) {
        const QString infoTxt = QString("argv: %1\n").arg(args.join(' ')) +
                                QString("rc: %1\n").arg(rcProg) +
                                QString("regex: %1\n").arg(regex ? "true" : "false") +
                                QString("issue: %1\n").arg(rcTest);

        writeTestFailInfo(infoTxt, goldOut, stdout, goldErr, stderr);
    }

    return rcTest;
}

void TestZtgps::writeTestFailInfo(const QString& infoTxt,
                                  const QString& stdoutGoldTxt, const QString& stdoutProgTxt,
                                  const QString& stderrGoldTxt, const QString& stderrProgTxt)
{
    QFile testInfo  ("/tmp/testInfo.txt");
    QFile stdoutProg("/tmp/stdoutProg.txt");
    QFile stderrProg("/tmp/stderrProg.txt");
    QFile stdoutGold("/tmp/stdoutGold.txt");
    QFile stderrGold("/tmp/stderrGold.txt");

    testInfo.open(QFile::WriteOnly | QFile::Truncate);
    stdoutProg.open(QFile::WriteOnly | QFile::Truncate);
    stderrProg.open(QFile::WriteOnly | QFile::Truncate);
    stdoutGold.open(QFile::WriteOnly | QFile::Truncate);
    stderrGold.open(QFile::WriteOnly | QFile::Truncate);

    testInfo.write(QString("Test: %1::%2\n").arg(QTest::currentTestFunction(), QTest::currentDataTag()).toUtf8());
    testInfo.write(infoTxt.toUtf8());

    stdoutProg.write(stdoutProgTxt.toUtf8());
    stderrProg.write(stderrProgTxt.toUtf8());
    stdoutGold.write(stdoutGoldTxt.toUtf8());
    stderrGold.write(stderrGoldTxt.toUtf8());
}

QString TestZtgps::testProcess()
{
    QFETCH(QStringList, argv);

    QProcess ztgps;
    ztgps.start(exe(), argv);

    if (!ztgps.waitForFinished())
        return RcProcess;

    const QByteArray stdout = ztgps.readAllStandardOutput();
    const QByteArray stderr = ztgps.readAllStandardError();

    if (ztgps.exitStatus() != QProcess::NormalExit)
        return RcProcess;

    return testOutErrRc(stdout, stderr, argv, ztgps.exitCode());
}

QString TestZtgps::testCmdLine()
{
    const CmdLine::SaveMessageHandler handler(captureOutErr);
    clearOutErr();

    QFETCH(QStringList, argv);

    argv.prepend(Appname);

    int rcProg;
    CmdLine opts(Appname, Version, BuildDate, argv);
    if ((rcProg = Exit::code(opts.processArgs())) == 0)
         rcProg = Exit::code(opts.processArgsPostApp());

    return testOutErrRc(m_stdout, m_stderr, argv, rcProg);
}

void TestZtgps::usage_data()
{
    QTest::addColumn<int>("rc");
    QTest::addColumn<bool>("regex");
    QTest::addColumn<QStringList>("argv");

    QTest::newRow("help")                      << 0 << false << QStringList({ "--help" });
    QTest::newRow("help-batch")                << 0 << false << QStringList({ "--help-batch" });
    QTest::newRow("help-filter")               << 0 << false << QStringList({ "--help-filter" });
    QTest::newRow("help-formats-input")        << 0 << false << QStringList({ "--help-formats-input" });
    QTest::newRow("help-formats-output")       << 0 << false << QStringList({ "--help-formats-output" });
    QTest::newRow("help-fields.trk")           << 0 << false << QStringList({ "--help-fields", "trk" });
    QTest::newRow("help-fields.wpt")           << 0 << false << QStringList({ "--help-fields", "wpt" });
    QTest::newRow("help-units.trk.ascent")     << 0 << false << QStringList({ "--help-units", "trk", "Ascent" });
    QTest::newRow("help-units.trk.moving_pow") << 0 << false << QStringList({ "--help-units", "trk", "moving_POW" });
    QTest::newRow("help-units.wpt.elevation")  << 0 << false << QStringList({ "--help-units", "wpt", "ELEVATION" });
    QTest::newRow("version")                   << 0 << true  << QStringList({ "--version" });
    QTest::newRow("pubkey")                    << 0 << false << QStringList({ "--pubkey" });
    QTest::newRow("err.invalid_opt")           << 5 << false << QStringList({ "--invalid_opt" });
    QTest::newRow("err.help-fields.invalid")   << 1 << false << QStringList({ "--help-fields", "ZZZ" });
}

void TestZtgps::usage()
{
    QCOMPARE(testCmdLine(), QString(RcOK));
}

void TestZtgps::batch_err_data() const
{
    QTest::addColumn<int>("rc");
    QTest::addColumn<bool>("regex");
    QTest::addColumn<QStringList>("argv");

    const QString garbageGpx   = QFINDTESTDATA(DATAROOT "gps/garbage.not.a.gpx");
    const QString mysticGpx    = QFINDTESTDATA(DATAROOT "gps/mystic_basin_trail.gpx");
    const QString noClobberGpx = tmp("noclobber.gpx");
    const QString badDirGpx    = tmp("noSuchDir/noSuchFile.gpx");
    const QString sameFileGpx1 = tmp("sameFile.gpx");
    const QString sameFileGpx2 = tmp("././sameFile.gpx");
    const QString newGpx1      = tmp("newGpx1.gpx");

    QVERIFY(m_tmpDir.isValid());
    QVERIFY(QFile(mysticGpx).copy(sameFileGpx1));

    QFile(noClobberGpx).open(QFile::WriteOnly|QFile::Append);  // create it

    QTest::newRow("err.garbage-input")       << 5 << true  << QStringList({ "--clobber", "--input", garbageGpx, "--output", "/dev/null", "--format", "tcx" });
    QTest::newRow("err.unknown-output")      << 5 << false << QStringList({ "--clobber", "--input", mysticGpx, "--output", "/dev/null" });
    QTest::newRow("err.output-exists")       << 5 << false << QStringList({ "--format", "gpx", "--input",mysticGpx, "--output", "/dev/null" });
    QTest::newRow("err.io-size-mismatch")    << 5 << false << QStringList({ "--input", mysticGpx, mysticGpx, "--output", "/dev/null/out.gpx" });
    QTest::newRow("err.concat-multi-out")    << 5 << false << QStringList({ "--concat", "--input", mysticGpx, "--output", "/dev/null/out.gpx", "/dev/null/out2.gpx" });
    QTest::newRow("err.out-format-required") << 5 << false << QStringList({ "--concat", "-i", mysticGpx, "--dir", "/dev/null" });
    QTest::newRow("err.bad-output-dir")      << 5 << false << QStringList({ "--concat", "-i", mysticGpx, "--dir", "/dev/null", "--format", "gpx" });
    QTest::newRow("err.bad-format")          << 5 << false << QStringList({ "--concat", "-i", mysticGpx, "--dir", "/tmp", "--format", "BAD" });
    QTest::newRow("err.noclobber")           << 5 << true  << QStringList({ "-i", mysticGpx, "-o", noClobberGpx });
    QTest::newRow("err.bad-outdir")          << 5 << true  << QStringList({ "-i", mysticGpx, "-o", badDirGpx });
    QTest::newRow("err.same-file")           << 5 << true  << QStringList({ "--clobber", "-i", sameFileGpx1, "-o", sameFileGpx2 });
    QTest::newRow("err.stdout-count")        << 5 << false << QStringList({ "--clobber", "-i", mysticGpx, mysticGpx, "-o", "-", "-", "--format", "gpx" });
    QTest::newRow("err.unknown-type")        << 5 << false << QStringList({ "--type", "BAD", "-i", mysticGpx, "-o", newGpx1 });
    QTest::newRow("err.badfilter-trk.1")     << 5 << false << QStringList({ "--filter-track", "NoField : Bad", "-i", mysticGpx, "-o", newGpx1 });
    QTest::newRow("err.badfilter-trk.2")     << 5 << false << QStringList({ "--filter-track", "Name : Good & & badOperator", "-i", mysticGpx, "-o", newGpx1 });
}

void TestZtgps::batch_err()
{
    QCOMPARE(testCmdLine(), QString(RcOK));
}

QString TestZtgps::testConvert()
{
    QFETCH(ResultCheckAll, check);

    if (QString result = testCmdLine(); result != RcOK)
        return result;

    return check().value_or(RcOK);
}

void TestZtgps::convert_data() const
{
    const QString mysticGpx     = QFINDTESTDATA(DATAROOT "gps/mystic_basin_trail.gpx");
    const QString mysticGpxTags = QFINDTESTDATA(DATAROOT "gps/mystic_basin_trail-tags.gpx");
    const QString plougasouGpx  = QFINDTESTDATA(DATAROOT "gps/Plougasou-plestin-parcours.gpx");
    const QString achiresFrGpx  = QFINDTESTDATA(DATAROOT "gps/archies_fr.filtered.gpx");
    const QString sampleTcx     = QFINDTESTDATA(DATAROOT "gps/sample_file.tcx");

    QTest::addColumn<bool>("regex");
    QTest::addColumn<int>("rc");
    QTest::addColumn<QStringList>("argv");
    QTest::addColumn<ResultCheckAll>("check");

    // ******** GPX -> GPX tests ********
    QTest::newRow("gpx1-gpx1.quiet")           << false << 0
                                               << QStringList({ "-i", mysticGpx, "-o", tmp("test1.gpx") })
                                               << ResultCheckAll( { { ResultCheck::isGpx },
                                                                    { "<trk>", 3 },
                                                                    { "<trkpt ", 285 } },
                                                                  tmp("test1.gpx"));
    QTest::newRow("gpx1-gpx1.quiet.clobber")   << false << 0
                                               << QStringList({ "--clobber", "-i", mysticGpx, "-o", tmp("test1.gpx") })
                                               << ResultCheckAll({ { ResultCheck::isGpx } },
                                                                 tmp("test1.gpx") );
    QTest::newRow("gpx1-gpx1.verbose-1")       << false << 0
                                               << QStringList({ "--clobber", "--verbose", "-i", mysticGpx, "-o", tmp("test1.gpx") })
                                               << ResultCheckAll({ { ResultCheck::isGpx } },
                                                                 tmp("test1.gpx") );
    QTest::newRow("gpx1-gpx1.verbose-2")       << false << 0
                                               << QStringList({ "--clobber", "-vv", "-i", mysticGpx, "-o", tmp("test1.gpx") })
                                               << ResultCheckAll({ { ResultCheck::isGpx } },
                                                                 tmp("test1.gpx") );
    QTest::newRow("gpx1-gpx1.verbose-3")       << false << 0
                                               << QStringList({ "-vv", "-i", plougasouGpx, "-o", tmp("test2.gpx") })
                                               << ResultCheckAll({ { ResultCheck::isGpx },
                                                                   { "<trkpt ", 934 },
                                                                   { "<trk>", 1 } },
                                                                 tmp("test2.gpx"));
    QTest::newRow("gpx3-gpx1.dedup")           << false << 0
                                               << QStringList({ "-vv", "--stat", "--concat", "-i", mysticGpx, mysticGpx, mysticGpx, "-o", tmp("test_dedup.gpx") })
                                               << ResultCheckAll( { { ResultCheck::isGpx },
                                                                    { "<trk>", 3 },
                                                                    { "<trkpt ", 285 } },
                                                                  tmp("test_dedup.gpx"));
    QTest::newRow("gpx1-gpx1.verbose-3-stats") << true << 0
                                               << QStringList({ "--clobber", "--stats", "-vvv", "-i", mysticGpx, "-o", tmp("test1.gpx") })
                                               << ResultCheckAll({ { ResultCheck::isGpx } },
                                                                 tmp("test1.gpx") );
    QTest::newRow("gpx1-gpx1.pretty.1")        << false << 0
                                               << QStringList({ "--clobber", "--pretty", "-i", mysticGpx, "-o", tmp("test1.gpx") })
                                               << ResultCheckAll({ { ResultCheck::isGpx },
                                                                   { "\n  <trk>\n", 3 },
                                                                   { "\n  </trk>\n", 3 },
                                                                   { "\n  <wpt lat[^\n]*lon[^\n]*>\n", 23 },
                                                                   { "\n  </wpt>\n", 23 } },
                                                                 tmp("test1.gpx") );
    QTest::newRow("gpx1-gpx1.pretty.2")        << false << 0
                                               << QStringList({ "--clobber", "--indent", "3", "--pretty", "-i", mysticGpx, "-o", tmp("test1.gpx") })
                                               << ResultCheckAll({ { ResultCheck::isGpx },
                                                                   { "\n   <trk>\n", 3 },
                                                                   { "\n   </trk>\n", 3 },
                                                                   { "\n   <wpt lat[^\n]*lon[^\n]*>\n", 23 },
                                                                   { "\n   </wpt>\n", 23 } },
                                                                 tmp("test1.gpx") );
    QTest::newRow("gpx1-gpx1.wpt")            << false << 0
                                              << QStringList({ "--stat", "-vv", "-i", achiresFrGpx, "-o", tmp("test3.gpx") })
                                              << ResultCheckAll({ { ResultCheck::isGpx },
                                                                  { "<trk>", 0 },
                                                                  { "<wpt ", 62 } },
                                                                tmp("test3.gpx") );
    QTest::newRow("gpx1-gpx1.concat")         << false << 0
                                              << QStringList({ "--concat", "--stat", "-vv", "-i", mysticGpx, plougasouGpx, achiresFrGpx, "-o", tmp("test4.gpx") })
                                              << ResultCheckAll({ { ResultCheck::isGpx },
                                                                  { "<trk>", (3 + 1) },
                                                                  { "<rte>", (2 + 0) },
                                                                  { "<trkpt ", (285 + 934) },
                                                                  { "<wpt ", (23 + 62) },
                                                                  { "Mystic River Basin Trails", 5 },
                                                                  { "plestin-les-greves-plougasnou", 1 },
                                                                  { "Trebeurden", 4 } },
                                                                tmp("test4.gpx") );
    QTest::newRow("gpx1-gpx1.concat.trk")     << false << 0
                                              << QStringList({ "--concat", "--clobber", "--type", "trk", "--stat", "-vv", "-i", mysticGpx, plougasouGpx, achiresFrGpx, "-o", tmp("test4.gpx") })
                                              << ResultCheckAll({ { ResultCheck::isGpx },
                                                                  { "<trk>", (3 + 1) },
                                                                  { "<trkpt ", (285 + 934) },
                                                                  { "<wpt ", 0 } },
                                                                tmp("test4.gpx"));
    QTest::newRow("gpx1-gpx1.concat.wpt")     << false << 0
                                              << QStringList({ "--concat", "--clobber", "--type", "wpt", "--stat", "-vv", "-i", mysticGpx, plougasouGpx, achiresFrGpx, "-o", tmp("test4.gpx") })
                                              << ResultCheckAll({ { ResultCheck::isGpx },
                                                                  { "<trk>", 0 },
                                                                  { "<trkpt ", 0 },
                                                                  { "<wpt ", (23 + 62) } },
                                                                tmp("test4.gpx") );

    QTest::newRow("gpx1-gpx1.concat.trkwpt")  << false << 0
                                              << QStringList({ "--concat", "--clobber", "--type", "wpt", "trk", "--stat", "-vv", "-i", mysticGpx, plougasouGpx, achiresFrGpx, "-o", tmp("test4.gpx") })
                                              << ResultCheckAll({ { ResultCheck::isGpx },
                                                                  { "<trk>", (3 + 1) },
                                                                  { "<trkpt ", (285 + 934) },
                                                                  { "<wpt ", (23 + 62) } },
                                                                tmp("test4.gpx") );

    QTest::newRow("gpx1-gpx1.filter-trk.name") << false << 0
                                               << QStringList({ "--filter-trk", "Name : Tufts", "--stat", "-vv", "-i", mysticGpx, "-o", tmp("test5.gpx") })
                                               << ResultCheckAll({ { ResultCheck::isGpx },
                                                                   { "<trk>", 1 },
                                                                   { "<wpt ", 23 },
                                                                   { "TUFTS CONNECT", 1 } },
                                                                 tmp("test5.gpx") );
    QTest::newRow("gpx1-gpx1.filter-trk.flags") << false << 0
                                                << QStringList({ "--clobber", "--filter-trk", "Flags ~ United.*Mass", "--stat", "-vv", "-i", mysticGpx, "-o", tmp("test5.gpx") })
                                                << ResultCheckAll({ { ResultCheck::isGpx },
                                                                    { "<trk>", 3 },
                                                                    { "<wpt ", 23 } },
                                                                  tmp("test5.gpx") );
    QTest::newRow("gpx1-gpx1.filter-wpt.1")   << false << 0
                                              << QStringList({ "--clobber", "--filter-wpt", "Name == ASSEMBLYSQ", "--stat", "-vv", "-i", mysticGpx, "-o", tmp("test5.gpx") })
                                              << ResultCheckAll({ { ResultCheck::isGpx },
                                                                  { "<trk>", 3 },
                                                                  { "<wpt ", 1 },
                                                                  { "ASSEMBLYSQ", 1 } },
                                                                tmp("test5.gpx") );

    QTest::newRow("gpx1-gpx1.displaycolor.1") << false << 0
                                              << QStringList({ "-i", plougasouGpx, "-o", tmp("test7.gpx") })
                                              << ResultCheckAll({ { ResultCheck::isGpx },
                                                                  { "<gpxx:TrackExtension>", 1 },
                                                                  { "</gpxx:TrackExtension>", 1 },
                                                                  { "<ztgpstx:TrackExtension>", 1 },
                                                                  { "</ztgpstx:TrackExtension>", 1 },
                                                                  { "<extensions>", 1 },
                                                                  { "</extensions>", 1 },
                                                                  { "<ztgpstx:color>#808080</ztgpstx:color>", 1},
                                                                  { "<ztgpstx:tags>", 0},
                                                                  { "</ztgpstx:tags>", 0},
                                                                  { "<gpxx:DisplayColor>DarkGray</gpxx:DisplayColor>", 1} },
                                                                tmp("test7.gpx") );

    QTest::newRow("gpx1-gpx1.displaycolor.2") << false << 0
                                              << QStringList({ "-i", plougasouGpx, "-o", tmp("test8.gpx"), "--no-ztgps-extensions" })
                                              << ResultCheckAll({ { ResultCheck::isGpx },
                                                                  { "<gpxx:TrackExtension>", 1 },
                                                                  { "</gpxx:TrackExtension>", 1 },
                                                                  { "<ztgpstx:TrackExtension>", 0 },
                                                                  { "</ztgpstx:TrackExtension>", 0 },
                                                                  { "<extensions>", 1 },
                                                                  { "</extensions>", 1 },
                                                                  { "<ztgpstx:tags>", 0},
                                                                  { "</ztgpstx:tags>", 0},
                                                                  { "<ztgpstx:color>#808080</ztgpstx:color>", 0},
                                                                  { "<gpxx:DisplayColor>DarkGray</gpxx:DisplayColor>", 1} },
                                                                tmp("test8.gpx") );

    QTest::newRow("gpx1-gpx1.ztgps-ext.1")    << false << 0
                                              << QStringList({ "--clobber", "-i", plougasouGpx, "-o", tmp("test8.gpx"), "--ztgps-extensions" })
                                              << ResultCheckAll({ { ResultCheck::isGpx },
                                                                  { "<gpxx:TrackExtension>", 1 },
                                                                  { "</gpxx:TrackExtension>", 1 },
                                                                  { "<ztgpstx:TrackExtension>", 1 },
                                                                  { "</ztgpstx:TrackExtension>", 1 },
                                                                  { "<extensions>", 1 },
                                                                  { "</extensions>", 1 },
                                                                  { "<ztgpstx:tags>", 0},
                                                                  { "</ztgpstx:tags>", 0},
                                                                  { "<ztgpstx:color>#808080</ztgpstx:color>", 1},
                                                                  { "<gpxx:DisplayColor>DarkGray</gpxx:DisplayColor>", 1} },
                                                                tmp("test8.gpx") );

    QTest::newRow("gpx1-gpx1.ztgps-ext.2")    << false << 0
                                              << QStringList({ "--clobber", "-i", mysticGpxTags, "-o", tmp("test8.gpx") })
                                              << ResultCheckAll({ { ResultCheck::isGpx },
                                                                  { "<gpxx:TrackExtension>", 0 },
                                                                  { "</gpxx:TrackExtension>", 0 },
                                                                  { "<ztgpstx:TrackExtension>", 2 },
                                                                  { "</ztgpstx:TrackExtension>", 2 },
                                                                  { "<extensions>", 2 },
                                                                  { "</extensions>", 2 },
                                                                  { "<ztgpstx:tags>Bikepack, Road</ztgpstx:tags>", 1},
                                                                  { "<ztgpstx:tags>Bikepack, Commute</ztgpstx:tags>", 1},
                                                                  { "<ztgpstx:color>#ab4574</ztgpstx:color>", 1},
                                                                  { "<gpxx:DisplayColor>DarkGray</gpxx:DisplayColor>", 0} },
                                                                tmp("test8.gpx") );

    // Try some dogfood
    QTest::newRow("gpx1-gpx1.dogfood")        << false << 0
                                              << QStringList({ "--stat", "-vv", "-i", tmp("test5.gpx"), "-o", tmp("test6.gpx") })
                                              << ResultCheckAll({ { ResultCheck::isGpx },
                                                                  { "<trk>", 3 },
                                                                  { "<wpt ", 1 },
                                                                  { "ASSEMBLYSQ", 1 } },
                                                                tmp("test6.gpx") );

    // ******** TCX -> GPX tests ********
    QTest::newRow("tcx1-gpx1.verbose-2")       << false << 0
                                               << QStringList({ "-vv", "-i", sampleTcx, "-o", tmp("test10.gpx") })
                                               << ResultCheckAll({ { ResultCheck::isGpx },
                                                                   { "<trk>", 1 },
                                                                   { "<trkseg>", 6 },
                                                                   { "<trkpt ", 260 } },
                                                                 tmp("test10.gpx") );

    // ******** GPX -> TCX tests ********
    QTest::newRow("gpx1-tcx1.verbose-2")       << false << 0
                                               << QStringList({ "-vv", "-i", mysticGpx, "-o", tmp("test10.tcx") })
                                               << ResultCheckAll({ { ResultCheck::isTcx },
                                                                   { "<Activity ", 5 },
                                                                   { "<Trackpoint>", 311 } },
                                                                 tmp("test10.tcx") );

    // ******** GPX -> FIT tests ********
    QTest::newRow("gpx1-fit1.verbose-2")       << false << 0
                                               << QStringList({ "-vv", "--filter-trk", "Name : \"LONG TRACK\"", "-i", mysticGpx, "-o", tmp("test10.fit") })
                                               << ResultCheckAll();


    // ******** FIT -> GPX tests ********
    QTest::newRow("fit1-gpx1.verbose-2")       << false << 0
                                               << QStringList({ "-vv", "-i", tmp("test10.fit"), "-o", tmp("test11.gpx") })
                                               << ResultCheckAll({ { ResultCheck::isGpx },
                                                                   { "<trk>", 1 },
                                                                   { "<trkpt ", 166 } },
                                                                 tmp("test11.gpx") );

    // ******** GPX -> KML tests ********
    QTest::newRow("gpx1-kml1.verbose-2")       << false << 0
                                               << QStringList({ "-vv", "-i", mysticGpx, "-o", tmp("test1.kml") })
                                               << ResultCheckAll({ { ResultCheck::isKml },
                                                                   { "<Placemark", 5 },
                                                                   { "<when>", 311 } },
                                                                 tmp("test1.kml") );

    // ******** KML -> GPX tests ********
    QTest::newRow("kml1-gpx1.verbose-2")       << false << 0
                                               << QStringList({ "-vv", "-i", tmp("test1.kml"), "-o", tmp("test20.gpx") })
                                               << ResultCheckAll({ { ResultCheck::isGpx },
                                                                   { "<trk>", 5 },
                                                                   { "<trkpt ", 311 } },
                                                                 tmp("test20.gpx") );
}

void TestZtgps::convert()
{
    QVERIFY(app().geoPolMgr().finishLoad());
    QCOMPARE(TestZtgps::testConvert(), QString(RcOK));
}

void TestZtgps::convert_cmd_data() const
{
    QTest::addColumn<bool>("regex");
    QTest::addColumn<int>("rc");
    QTest::addColumn<QStringList>("argv");
    QTest::addColumn<ResultCheckAll>("check");

    const QString mysticGpx    = QFINDTESTDATA(DATAROOT "gps/mystic_basin_trail.gpx");

    // ******** GPX -> GPX tests ********
    QTest::newRow("gpx1-gpx1.verbose-2")       << false << 0
                                               << QStringList({ "--clobber", "-vv", "-i", mysticGpx, "-o", tmp("test1_cmd.gpx") })
                                               << ResultCheckAll({ { ResultCheck::isGpx } },
                                                                 tmp("test1_cmd.gpx") );
}

void TestZtgps::convert_cmd()
{
    QVERIFY(app().geoPolMgr().finishLoad());
    QCOMPARE(testProcess(), QString(RcOK));
}

std::optional<QString> TestZtgps::testOldDataFile(TreeModel* treeModel, const QString& suffix)
{
    auto* model = dynamic_cast<ChangeTrackingModel*>(treeModel);
    if (model == nullptr)
        return QString("Bad model");

    QFETCH(uint32_t, data_ver);
    QFETCH(uint32_t, conf_ver);

    const QString nameBase = "ZTGPS-Sample";
    const QString dir = QString("%1oldformat/0x%2/cfgDataVersion.%3")
            .arg(DATAROOT)
            .arg(data_ver, 0, 16)
            .arg(conf_ver);

    const ChangeTrackingModel::SignalBlocker block(*model);
    const QString dataFile = QFINDTESTDATA(dir + QDir::separator() + nameBase + suffix);

    if (!QFile(dataFile).exists())
        return QString("File not found: ") + dataFile;

    if (!model->load(dataFile))
        return QString("Unable to load: ") + dataFile;

    return std::nullopt;
}

bool TestZtgps::modelContains(const TreeModel* model, const QString& queryStr)
{
    const Query::Context queryCtx(model);
    const auto query = queryCtx.parse(queryStr);

    return model->findRow(QModelIndex(), [&](const QModelIndex& idx) { return queryCtx.match(query, idx); }).isValid();
}

QString TestZtgps::testOldDataFile()
{
    QFETCH(uint32_t, data_ver);

    // There is a problem serializing QIcon objects in Qt 5.9 and unserializing them in 5.15.
    // (maybe 5.14 too, not sure).  This has been worked around in Ztgps binary save format 0x1002,
    // but if we're reading an old data format on a newer Qt build, we have a problem.  All we
    // can do is pretend things are OK (although they're really not).
    if (data_ver < 0x1002 && QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
        return RcOK;

    QVector<bool> hasModel;
    hasModel.fill(true, int(App::Model::_LastSaved));
    hasModel[int(App::Model::Waypoint)] = data_ver >= 0x1001;

    QVector<QStringList> queries;
    queries.resize(int(App::Model::_LastSaved));
    queries[int(App::Model::Track)]    += { "Name : SHORT", "Name : LONG", "Name : TUFTS" };
    queries[int(App::Model::Filter)]   += { "Name : \"Big Climbs\"", "Name : Biking" };
    queries[int(App::Model::View)]     += { "Name : Canada", "Name : Arizona" };
    queries[int(App::Model::Waypoint)] += { "Name : EARHARTDAM", "Name : \"FISH DOCK\"" };

    Models models;

    for (App::Model mtype = App::Model::_First; mtype != App::Model::_LastSaved; Util::inc(mtype)) {
        if (!hasModel.at(int(mtype)))
            continue;

        // Load
        if (const auto result = testOldDataFile(models(mtype), App::modelDataSuffix(mtype)); result)
            return result.value();

        // Test
        for (const QString& query : queries.at(int(mtype)))
            if (!modelContains(models(mtype), query))
                return QString("Data not found in model: ") + query;
    }

    return RcOK;
}

QString TestZtgps::testOldConfFile(bool loadModels)
{
    QFETCH(uint32_t, data_ver);
    QFETCH(uint32_t, conf_ver);
    QFETCH(uint32_t, expected_trk);
    QFETCH(uint32_t, expected_wpt);

    // There is a problem serializing QIcon objects in Qt 5.9 and unserializing them in 5.15.
    // (maybe 5.14 too, not sure).  This has been worked around in Ztgps binary save format 0x1002,
    // but if we're reading an old data format on a newer Qt build, we have a problem.  All we
    // can do is pretend things are OK (although they're really not).
    if (data_ver < 0x1002 && QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
        return RcOK;

    const QString confFile = sampleConfFile(data_ver, conf_ver);

    if (!QFile(confFile).exists())
        return QString("File not found: ") + confFile;

    // Marble::MarbleWidget plugins report some "QObject::disconnect: Unexpected null parameter" warnings on
    // destruction.  This is to silence it for the test suite.  It does the same if you just run
    // "marble" from the command line with the plugin package installed, and I haven't found a way to remove
    // the warnings from outside libmarble.
    const CmdLine::SaveMessageHandler handler(captureOutErr);
    clearOutErr();

    QSettings settings(confFile, QSettings::IniFormat);
    CmdLine loadOld("testztgps", "1.0", BuildDate, { "testztgps", "--private-session", "--conf", confFile });
    loadOld.processArgs();   // set new session file

    MainWindow mainWindow(nullptr, &loadOld);

    bool success = false;
    const QDateTime modTime = QFileInfo(confFile).lastModified();

    if (loadModels) {
        // Load models as well as conf
        success     = mainWindow.loadInternal(settings);
    } else {
        // Just test loading the UI configuration
        app().newSession(); // don't use MainWindow newSession(), because that clears the current session file
        success = mainWindow.loadUiConfig(settings);
    }

    if (mainWindow.findChild<QAction*>("action_Save_Settings")->isEnabled() ||
        mainWindow.findChild<QAction*>("action_Save_Settings_As")->isEnabled())
        return { "Save actions not disabled in private session." };

    if (success) {
        const uint tracks    = app().getModel(App::Model::Track).rowCount();
        const uint waypoints = app().getModel(App::Model::Waypoint).rowCount();

        if (tracks != expected_trk)
            return QString("Failed to find expected track count after load: expected %1 found %2")
                    .arg(expected_trk).arg(tracks);
        if (waypoints != expected_wpt)
            return QString("Failed to find expected waypoint count after load: expected %1 found %2")
                    .arg(expected_wpt).arg(waypoints);

        if (undoMgr().undoCount() != 0)
            return QString("Bad undo count after load: expected %1 found %2 (models = %3)")
                    .arg(0).arg(undoMgr().undoCount()).arg(loadModels ? "true" : "false");

        if (undoMgr().redoCount() != 0)
            return QString("Bad redo count after load: expected %1 found %2")
                    .arg(0).arg(undoMgr().redoCount());

        // It's a private session, so saving better fail
        if (mainWindow.uiSave(confFile))
            return { "Save of private session suceeded and should have failed" };

        if (modTime != QFileInfo(confFile).lastModified())
            return { "Mod time changed after saving private session" };

    } else {
        return QString("config load error: ") + confFile;
    }

    return RcOK;
}

void TestZtgps::old_formats_data()
{
    QTest::addColumn<uint32_t>("data_ver");
    QTest::addColumn<uint32_t>("conf_ver");
    QTest::addColumn<uint32_t>("expected_trk");
    QTest::addColumn<uint32_t>("expected_wpt");

    for (const auto& verData : cfgDataVersions)
        QTest::addRow(verData.path, "") << verData.data_ver << verData.conf_ver << verData.expected_trk << verData.expected_wpt;
}

void TestZtgps::old_formats()
{
    QCOMPARE(testOldDataFile(), QString(RcOK));
    QCOMPARE(testOldConfFile(), QString(RcOK));
}

void TestZtgps::gps_device_data() const
{
    createMainWindowPrivate(m_mainWindow_private);

    QTest::addColumn<QString>("make");
    QTest::addColumn<QString>("model");
    QTest::addColumn<QStringList>("trackNames");

    QTest::addRow("Garmin/Edge500")   << "Garmin" << "Edge 500"
                                      << QStringList({"Sample"});
    QTest::addRow("Garmin/Dakota20")  << "Garmin" << "Dakota 20" <<
                                         QStringList({"Mystic River Basin Trails: LONG TRACK",
                                                      "Mystic River Basin Trails: SHORT TRACK",
                                                      "Mystic River Basin Trails: TUFTS CONNECT"});
    QTest::newRow("");  // mark end of sequence
}

void TestZtgps::gps_device() const
{
    createMainWindowPrivate(m_mainWindow_private);
    app().newSession();

    GpsModel& gpsModel = app().gpsModel();

    if (QTest::currentDataTag()[0] == '\0') {
        delete m_mainWindow_private;
        m_mainWindow_private = nullptr;
        return;
    }

    const QString mountRoot =
            QFileInfo(QFINDTESTDATA(QString(DATAROOT "devices") + QDir::separator() + QTest::currentDataTag())).canonicalFilePath();

    QFETCH(QString, make);
    QFETCH(QString, model);
    QFETCH(QStringList, trackNames);

    QVERIFY(QDir(mountRoot).exists());

    // Create fake root
    gpsModel.m_testMounts = [mountRoot](GpsModel& gps) {
        if (!gps.m_volumes.contains(mountRoot)) {
            gps.m_volumes.append(mountRoot);
            return true;
        }
        return false;
    };

    // Find action to show import dialog
    auto* importFromDevice = m_mainWindow_private->findChild<QAction*>("action_Import_from_Device");
    QVERIFY(importFromDevice != nullptr);

    // Find device dialog
    auto* gpsDevicePane = m_mainWindow_private->findChild<GpsDevicePane*>();
    QVERIFY(gpsDevicePane != nullptr);

    auto* refresh = gpsDevicePane->findChild<QAction*>("action_Refresh");
    QVERIFY(refresh);

    refresh->trigger();

    // Find our fake mount root
    QVERIFY(gpsModel.rowCount() >= 1);
    const QModelIndex idx = gpsModel.findRow(QModelIndex(), mountRoot, GpsModel::MountPoint);
    QVERIFY(idx.isValid());

    QCOMPARE(gpsModel.data(GpsModel::Make, idx).toString(),  make);
    QCOMPARE(gpsModel.data(GpsModel::Model, idx).toString(), model);

    // Verify track model does not contain these names
    for (const QString& name : trackNames)
        QVERIFY(!app().trackModel().findRow(QModelIndex(), name, TrackModel::Name).isValid());

    // Find import action
    auto* importData = gpsDevicePane->findChild<QAction*>("action_Import_Data");
    QVERIFY(importData != nullptr);

    // Don't use the import action: that shows a modal dialog.  Use non-interactive form.
    gpsDevicePane->select(idx);  // select our device
    gpsDevicePane->import(GeoLoadParams());

    // Verify it contains our track names
    for (const QString& name : trackNames)
        QVERIFY(app().trackModel().findRow(QModelIndex(), name, TrackModel::Name).isValid());
}

template <class D>
void testDialog(QWidget& mainWindow, QWidget& actionWidget, const char* actionName, bool modal = false)
{
    auto* action = actionWidget.findChild<QAction*>(actionName);
    QVERIFY2(action != nullptr, actionName);

    D* dialog = mainWindow.findChild<D*>();
    QVERIFY2(dialog != nullptr, actionName);

    if (modal)
        QTimer::singleShot(5, dialog, &D::reject);   // to close the modal dialogs

    action->trigger();

    // modal dialogs close before the event loop returns to us, so it's OK if they're not visible.
    if (!modal && dialog != nullptr)
        QVERIFY2(dialog->isVisible(), actionName);

    dialog->close();
    QVERIFY2(!dialog->isVisible(), actionName);
}

// Test opening some dialogs

void TestZtgps::save_load() const
{
    const CmdLine::SaveMessageHandler handler(captureOutErr);
    clearOutErr();

    const GeoLoadParams loadParams(GeoIoFeature::Trk | GeoIoFeature::Wpt | GeoIoFeature::AuxTrk, { "Road", "Recreation" });
    const QString mysticGpx    = QFINDTESTDATA(DATAROOT "gps/mystic_basin_trail.gpx");

    const auto test = [&](const MainWindow& mw, int tracks, int waypoints, int undoCount, int redoCount, bool dirty) {
        QCOMPARE(app().trackModel().rowCount(), tracks);
        QCOMPARE(app().waypointModel().rowCount(), waypoints);
        QCOMPARE(undoMgr().undoCount(), undoCount);
        QCOMPARE(undoMgr().redoCount(), redoCount);
        QCOMPARE(mw.isWindowModified(), dirty);
    };

    const auto testView = [&](const MainWindow& mw, const QString& name, double centerLat, double centerLon,
                              int undoCount, int redoCount) {
        auto* viewPane = mw.findPane<ViewPane>();
        QVERIFY(viewPane != nullptr && viewPane->topFilter().rowCount() > 2);

        auto* mapPane = mw.findPane<MapPane>();
        QVERIFY(mapPane != nullptr);

        if (!name.isEmpty()) {
            const QModelIndex idx = app().viewModel().findRow(QModelIndex(), name, ViewModel::Name);
            QVERIFY(idx.isValid());
            viewPane->gotoIndex(idx);
        }

        const ViewParams view = mapPane->m_mapWidget->viewParams();

        QVERIFY(std::abs(view.centerLat() - centerLat) < 0.5);
        QVERIFY(std::abs(view.centerLon() - centerLon) < 0.5);

        QCOMPARE(mapPane->undoMgrView().undoCount(), undoCount);
        QCOMPARE(mapPane->undoMgrView().redoCount(), redoCount);
    };

    // New session, and import from file
    {
        app().newSession(); // don't use MainWindow newSession(), because that clears the current session file
        cfgDataWritable().reset();
        CmdLine saveload("testztgps", "1.0", BuildDate, { "testztgps", "--no-first-run", "--conf", m_tmpDir.filePath("saveload-test.conf") });
        saveload.processArgs();   // set new session file
        MainWindow mainWindow(nullptr, &saveload);
        mainWindow.sampleSession();  // create the default sample session

        mainWindow.findChild<QAction*>("action_Save_Settings")->trigger();
        undoMgr().clear();

        test(mainWindow, 3, 0, 0, 0, false);    // post import: T=3, W=23, one undo, modified

        auto* trackPane = mainWindow.findPane<TrackPane>();
        QVERIFY(trackPane != nullptr);

        auto* undo = mainWindow.findChild<QAction*>("action_Undo");
        auto* redo = mainWindow.findChild<QAction*>("action_Redo");
        QVERIFY(undo);
        QVERIFY(redo);

        // Import
        QVERIFY(!GeoLoad::anyFailed(mainWindow.importTracks(loadParams, { { mysticGpx } })));
        test(mainWindow, 3, 23, 1, 0, true);    // post import: T=3, W=23, one undo, modified
        undo->trigger();
        test(mainWindow, 3, 0, 0, 1, false);    // post undo: T=3, W=0, back to unmodified
        redo->trigger();
        test(mainWindow, 3, 23, 1, 0, true);    // post redo, back to post-import state

        // Import again, to verify deduplication
        QVERIFY(!GeoLoad::anyFailed(mainWindow.importTracks(loadParams, { { mysticGpx } })));
        test(mainWindow, 3, 23, 1, 0, true);    // state unchanged due to deduplication
        QCOMPARE(trackPane->topFilter().rowCount(), 3);

        trackPane->m_queryChangedTimer.setInterval(0);  // shorten normal query defer timer

        // Set a track filter
        auto* filterTrack = trackPane->findChild<QLineEdit*>("filterTrack");
        QVERIFY(filterTrack != nullptr);

        QTest::keyClicks(filterTrack, "Name : ");
        QTest::qWait(1); // allow deferred update to complete

        QVERIFY(QtCompat::LabelPixmap(trackPane->m_filterValid).cacheKey() == trackPane->m_filterInvalidIcon.cacheKey());  // invalid so far

        QTest::keyClicks(filterTrack, "tufts");
        QTest::qWait(1); // allow deferred update to complete

        QVERIFY(QtCompat::LabelPixmap(trackPane->m_filterValid).cacheKey() == trackPane->m_filterValidIcon.cacheKey());  // valid now
        QCOMPARE(trackPane->topFilter().rowCount(), 1);
    }

    // Restore session
    {
        app().newSession(); // don't use MainWindow newSession(), because that clears the current session file
        cfgDataWritable().reset();
        CmdLine saveload("testztgps", "1.0", BuildDate, { "testztgps", "--conf", m_tmpDir.filePath("saveload-test.conf") });
        saveload.processArgs();   // set new session file
        MainWindow mainWindow(nullptr, &saveload);
        mainWindow.postLoadHook();  // since we bypassed normal initialization

        auto* trackPane = mainWindow.findPane<TrackPane>();
        QVERIFY(trackPane != nullptr);

        auto* filterTrack = trackPane->findChild<QLineEdit*>("filterTrack");
        QVERIFY(filterTrack != nullptr);

        test(mainWindow, 3, 23, 0, 0, false);  // fresh load, no undos, not modified

        // Import again, to verify deduplication after session reload
        QVERIFY(!GeoLoad::anyFailed(mainWindow.importTracks(loadParams, { { mysticGpx } })));
        test(mainWindow, 3, 23, 0, 0, false);  // fresh load, no undos, not modified

        QCOMPARE(trackPane->topFilter().rowCount(), 1);  // tracks should still be filtered
        trackPane->updateFilter(""); // unset filter
        QCOMPARE(trackPane->topFilter().rowCount(), 3);

        // test offline mode
        {
            auto* offlineMode = mainWindow.findChild<QAction*>("action_Offline_Mode");
            QVERIFY(offlineMode != nullptr);

            QVERIFY(!offlineMode->isChecked());
            QVERIFY(!mainWindow.isOfflineMode());

            offlineMode->trigger();
            QVERIFY(offlineMode->isChecked());
            QVERIFY(mainWindow.isOfflineMode());
        }

        // Test filter pane
        auto* filterPane = mainWindow.findPane<FilterPane>();
        {
            QVERIFY(filterPane != nullptr);
            QVERIFY(filterPane->topFilter().rowCount() > 3);

            const QModelIndex bikeIdx = app().filterModel().findRow(QModelIndex(), "Bike", FilterModel::Name);
            QVERIFY(bikeIdx.isValid());
            filterPane->select({bikeIdx});
            filterPane->setFilter();
            QVERIFY(filterTrack->text().contains("Cross"));
            QVERIFY(filterTrack->text().contains("Road"));
            QVERIFY(filterTrack->text().contains("Mountain"));
            QVERIFY(filterTrack->text().contains("Gravel"));
            QVERIFY(filterTrack->text().contains("WinterBike"));
        }

        // Test view pane and view undo/redo
        {
            auto* viewPane = mainWindow.findPane<ViewPane>();
            QVERIFY(viewPane != nullptr);
            QVERIFY(viewPane->topFilter().rowCount() > 2);

            testView(mainWindow, "Minnesota", 46.15787512003232, -92.86976379630738, 0, 0);
            testView(mainWindow, "Normandie", 49.26, -0.19, 1, 0);
            testView(mainWindow, "Canada", 57.98525257396584, -93.8615056166874, 2, 0);

            auto* undoView = mainWindow.findChild<QAction*>("action_Undo_View");
            auto* redoView = mainWindow.findChild<QAction*>("action_Redo_View");
            QVERIFY(undoView != nullptr);
            QVERIFY(redoView != nullptr);

            undoView->trigger();  // undo back to Normandia
            testView(mainWindow, QString(), 49.26, -0.19, 1, 1);

            undoView->trigger();  // undo back to Minnesota
            testView(mainWindow, QString(), 46.15787512003232,  -92.86976379630738, 0, 2);

            redoView->trigger();  // redo back to Normandia
            testView(mainWindow, QString(), 49.26, -0.19, 1, 1);

            redoView->trigger();  // redo back to Canada
            testView(mainWindow, QString(), 57.98525257396584, -93.8615056166874, 2, 0);
        }

        // Test some menu driven dialogs
        auto* mapPane = mainWindow.findPane<MapPane>();
        QVERIFY(mapPane != nullptr);

        // We can't focus normally since we haven't shown any windows.
        {
            trackPane->setFocus();
            mainWindow.newFocus(trackPane);
            trackPane->select(app().trackModel().index(0,0));

            // Due to modal inputs, it's difficult to exercise some dialogs here.
            testDialog<AboutDialog>(mainWindow, mainWindow, "action_About_ZT");
            testDialog<AppConfig>(mainWindow, mainWindow, "action_Configure");
            testDialog<DeviceDialog>(mainWindow, mainWindow, "action_Import_from_Device");
            testDialog<DocDialog>(mainWindow, mainWindow, "action_Show_Tutorial");
            testDialog<GpsCaptureDialog>(mainWindow, mainWindow, "action_Live_GPSD_Capture");
            testDialog<GpsdVersionDialog>(mainWindow, mainWindow, "action_Supported_GPSD_Versions");
            testDialog<ImportDialog>(mainWindow, mainWindow, "action_Import_Track", true);
            testDialog<ExportDialog>(mainWindow, mainWindow, "action_Export_Track", true);
            testDialog<MapDownloadDialog>(mainWindow, mainWindow, "action_Download_Region");
            testDialog<NewTrackDialog>(mainWindow, mainWindow, "action_Create_New_Track", true);
            testDialog<NewWaypointDialog>(mainWindow, *mapPane, "action_Create_Waypoint", true);
            testDialog<PersonDialog>(mainWindow, mainWindow, "action_Select_Person", true);
            testDialog<TrackSimplifyDialog>(mainWindow, mainWindow, "action_Simplify_Selection", true);
        }

        auto* selectAll = mainWindow.findChild<QAction*>("action_View_Select_All");
        auto* selectNone = mainWindow.findChild<QAction*>("action_View_Select_None");

        // Test selection menus
        {
            trackPane->setFocus();
            mainWindow.newFocus(trackPane);
            trackPane->select(app().trackModel().index(0,0));

            QVERIFY(selectAll != nullptr);
            QVERIFY(selectNone != nullptr);

            QCOMPARE(app().trackModel().rowCount(), 3);
            selectAll->trigger();
            QCOMPARE(trackPane->getSelections().size(), 3);
            selectNone->trigger();
            QCOMPARE(trackPane->getSelections().size(), 0);
        }

        {
            // Test duplicate
            selectAll->trigger();
            QCOMPARE(trackPane->getSelections().size(), 3);
            auto* duplicate = mainWindow.findChild<QAction*>("action_Duplicate_Selection");
            QVERIFY(duplicate != nullptr);
            duplicate->trigger();
            QCOMPARE(app().trackModel().rowCount(), 6);
            QCOMPARE(trackPane->getSelections().size(), 3);
            QVERIFY(undoMgr().undo());
            QCOMPARE(app().trackModel().rowCount(), 3);
        }

        // Toolbars
        {
            for (MainWindow::CheckableMenu tb = MainWindow::CheckableMenu::_ToolBarBegin; tb < MainWindow::CheckableMenu::_ToolBarEnd; Util::inc(tb))
                QVERIFY(mainWindow.isVisible(tb));

            auto* showToolbar = mainWindow.findChild<QMenu*>("menu_Show_Toolbar");
            QVERIFY(showToolbar);

            for (QAction* a : showToolbar->actions())
                QVERIFY(a->isChecked());

            // Disable them all
            for (QAction* a : showToolbar->actions())
                a->trigger();

            // They should no longer be visible
            for (MainWindow::CheckableMenu tb = MainWindow::CheckableMenu::_ToolBarBegin; tb < MainWindow::CheckableMenu::_ToolBarEnd; Util::inc(tb))
                QVERIFY(!mainWindow.getToolBar(tb)->isVisibleTo(&mainWindow));

            // Re-enable
            for (QAction* a : showToolbar->actions())
                a->trigger();

            // They should be visible again
            for (MainWindow::CheckableMenu tb = MainWindow::CheckableMenu::_ToolBarBegin; tb < MainWindow::CheckableMenu::_ToolBarEnd; Util::inc(tb))
                QVERIFY(mainWindow.getToolBar(tb)->isVisibleTo(&mainWindow));
        }
    }

}

void TestZtgps::newpane_dialog() const
{
    createMainWindowPrivate(m_mainWindow_private);
    QVERIFY(m_mainWindow_private);

    MainWindow& mainWindow = *m_mainWindow_private;
    mainWindow.sampleSession();  // create the sample session
    mainWindow.resetUI();
    undoMgr().clear();

    QVERIFY(!undoMgr().hasUndos());

    auto* viewPane = mainWindow.findPane<ViewPane>();
    QVERIFY(viewPane != nullptr);
    Util::SetFocus(viewPane);
    mainWindow.trackFocus(viewPane); // because otherwise the focus event is deferred by Qt

    auto* openPaneDialog = mainWindow.findChild<QAction*>("action_Pane_Dialog");
    QVERIFY(openPaneDialog != nullptr);

    openPaneDialog->trigger();  // open the new pane dialog

    // Find new pane dialog
    auto* newPaneDialog = mainWindow.findChild<NewPaneDialog*>();
    QVERIFY(newPaneDialog != nullptr);

    auto* replacePane = newPaneDialog->findChild<QAction*>("action_Replace_Pane");
    QVERIFY(replacePane != nullptr);

    auto* paneView = newPaneDialog->findChild<QTreeView*>();
    const QModelIndex emptyPaneRow = app().newPaneModel().findRow(QModelIndex(), Pane::name(PaneClass::Empty),
                                                                  NewPaneModel::Name);
    QVERIFY(emptyPaneRow.isValid());
    QVERIFY(emptyPaneRow.row() == int(PaneClass::Empty));
    paneView->setCurrentIndex(Util::MapUp(&newPaneDialog->topFilter(), emptyPaneRow));

    replacePane->trigger();

    QVERIFY(mainWindow.findPane<ViewPane>() == nullptr); // we just replaced this
    QVERIFY(mainWindow.findPane<EmptyPane>() != nullptr); // we just added this
    QVERIFY(undoMgr().hasUndos());  // Look for undo set

    undoMgr().undo();
    QVERIFY(mainWindow.findPane<ViewPane>() != nullptr); // post-undo: back to orginal config
    QVERIFY(mainWindow.findPane<EmptyPane>() == nullptr); // post-undo: back to orginal config

    undoMgr().redo();
    QVERIFY(mainWindow.findPane<ViewPane>() == nullptr); // post-redo: we just replaced this
    QVERIFY(mainWindow.findPane<EmptyPane>() != nullptr); // post-redo: we just added this

    auto* addPane = newPaneDialog->findChild<QAction*>("action_Add_New_Pane");
    QVERIFY(addPane != nullptr);

    const QModelIndex filterPaneRow = app().newPaneModel().findRow(QModelIndex(), Pane::name(PaneClass::Filter),
                                                                   NewPaneModel::Name);
    QVERIFY(filterPaneRow.isValid());
    QVERIFY(filterPaneRow.row() == int(PaneClass::Filter));

    paneView->setCurrentIndex(Util::MapUp(&newPaneDialog->topFilter(), filterPaneRow));

    // Add the new filter pane
    addPane->trigger();
    QVERIFY(mainWindow.findPane<ViewPane>() == nullptr);   // we just replaced this
    QVERIFY(mainWindow.findPane<EmptyPane>() != nullptr);  // should still be here after the add
    QVERIFY(mainWindow.findPane<FilterPane>() != nullptr); // newly added pane


    auto* closePane = mainWindow.findChild<QAction*>("action_Pane_Close");
    QVERIFY(closePane != nullptr);

    // because otherwise the focus event is deferred by Qt
    mainWindow.trackFocus(mainWindow.findPane<FilterPane>());

    // Close what we just added
    closePane->trigger();
    QVERIFY(mainWindow.findPane<ViewPane>() == nullptr);   // we just replaced this
    QVERIFY(mainWindow.findPane<EmptyPane>() != nullptr);  // should still be here after the add
    QVERIFY(mainWindow.findPane<FilterPane>() == nullptr); // newly added pane
}

void TestZtgps::clipboard_data() const
{
    createMainWindowPrivate(m_mainWindow_private);
    m_mainWindow_private->sampleSession();  // create the sample session
    m_mainWindow_private->resetUI();

    auto* trackPane = m_mainWindow_private->findPane<TrackPane>();
    QVERIFY(trackPane != nullptr);

    // Add other panes we mean to test.
    m_mainWindow_private->addPane(m_mainWindow_private->paneFactory(PaneClass::Points), nullptr, true, trackPane);
    m_mainWindow_private->addPane(m_mainWindow_private->paneFactory(PaneClass::TrackDetail), nullptr, true, trackPane);

    QTest::addColumn<PaneClass>("paneClass");
    QTest::addColumn<bool>("all");
    QTest::addColumn<QString>("colSep");
    QTest::addColumn<QString>("rowSep");

    QTest::addRow("TrackPane_all")        << PaneClass::Track       << true  << QString() << QString();
    QTest::addRow("TrackPane_none")       << PaneClass::Track       << false << QString() << QString();
    QTest::addRow("PointPane_all")        << PaneClass::Points      << true  << QString() << QString();
    QTest::addRow("PointPane_none")       << PaneClass::Points      << false << QString() << QString();
    QTest::addRow("TrackDetailPane_all")  << PaneClass::TrackDetail << true  << QString() << QString();
    QTest::addRow("TrackDetailPane_none") << PaneClass::TrackDetail << false << QString() << QString();
    QTest::addRow("TrackPane_s0_all")     << PaneClass::Track       << true  << "," << "\n\n";
    QTest::addRow("PointPane_s0_all")     << PaneClass::Points      << true  << ":-:" << QString();
    QTest::addRow("__END__")              << PaneClass::Track       << true  << QString() << QString();
}

QString TestZtgps::removeAbsPath(const QString& re, const QString& repl, const QString& string)
{
    QString newStr = string;

    return newStr.replace(QRegularExpression(re, QRegularExpression::MultilineOption), repl);
}

void TestZtgps::clipboard() const
{
    QFETCH(PaneClass, paneClass);
    QFETCH(bool, all);
    QFETCH(QString, colSep);
    QFETCH(QString, rowSep);

    if (qstrcmp(QTest::currentDataTag(), "__END__") == 0) {
        m_mainWindow_private->newSession(); // close out old session
        return;
    }

    cfgDataWritable().reset();

    if (!colSep.isEmpty())
        cfgDataWritable().colSeparator = colSep;
    if (!rowSep.isEmpty())
        cfgDataWritable().rowSeparator = rowSep;

    auto* trackPane = m_mainWindow_private->findPane<TrackPane>();
    QVERIFY(trackPane != nullptr);
    const QModelIndex track0 = app().trackModel().index(0, 0);

    auto* pane = m_mainWindow_private->findPane<PaneBase>([paneClass](PaneBase* p) {
                 return p->paneClass() == PaneClass_t(int(paneClass)); });
    QVERIFY(pane != nullptr);

    // Because we don't have enough bits and bobs in the test framework for the event loop to be working,
    // we manually poke the panes to process the track change notification
    if (auto* pp = dynamic_cast<PointPane*>(pane); pp != nullptr) {
        pp->currentTrackChanged(track0);
    } else if (auto* tdp = dynamic_cast<TrackDetailPane*>(pane); tdp != nullptr)
        tdp->currentTrackChanged(track0);

    if (all)
        pane->selectAll();
    else
        pane->selectNone();

    pane->copySelected();  // copy to clipboard

    const QString clipText    = removeAbsPath(R"(^([^\s]*\s*)(.*)(mystic_basin_trail\.gpx)$)",
                                              R"(\1\3)",
                                              app().clipboard()->text());

    const QString goldOutFile = gold(".txt");
    const auto goldOut        = Util::ReadFile<QString>(goldOutFile, 1<<16, false);

    if (clipText != goldOut) {
        writeTestFailInfo("", goldOut, clipText);
    }

    QCOMPARE(clipText, goldOut);
}

void TestZtgps::model_load_benchmark_data()
{
    QTest::addColumn<uint32_t>("data_ver");
    QTest::addColumn<uint32_t>("conf_ver");
    QTest::addColumn<uint32_t>("expected_trk");
    QTest::addColumn<uint32_t>("expected_wpt");

    QTest::addRow(TestZtgps::cfgDataVersions.back().path, "")
            << TestZtgps::cfgDataVersions.back().data_ver
            << TestZtgps::cfgDataVersions.back().conf_ver
            << TestZtgps::cfgDataVersions.back().expected_trk
            << TestZtgps::cfgDataVersions.back().expected_wpt;
}

void TestZtgps::model_load_benchmark()
{
    QBENCHMARK {
        QCOMPARE(testOldDataFile(), QString(RcOK));
    }
}

void TestZtgps::uiload_benchmark_data()
{
    QTest::addColumn<uint32_t>("data_ver");
    QTest::addColumn<uint32_t>("conf_ver");
    QTest::addColumn<uint32_t>("expected_trk");
    QTest::addColumn<uint32_t>("expected_wpt");

    QTest::addRow(TestZtgps::cfgDataVersions.back().path, "")
            << TestZtgps::cfgDataVersions.back().data_ver
            << TestZtgps::cfgDataVersions.back().conf_ver
            << 0U  // 0 tracks and waypoints since we don't test data here, just the UI
            << 0U; // ...
}

void TestZtgps::uiload_benchmark()
{
    QBENCHMARK {
        QCOMPARE(testOldConfFile(false), QString(RcOK));
    }
}

void TestZtgps::mainwin_creation_benchmark() const
{
    const CmdLine::SaveMessageHandler handler(captureOutErr);

    m_privateSessionCL.processArgs();

    QBENCHMARK {
        MainWindow mainWindow(nullptr, &m_privateSessionCL);
    }
}

void TestZtgps::doc_dialog() const
{
    DocDialog docDialog(*m_mainWindow_private);
    docDialog.setup();  // normally happens with show(), but we don't want to show.

    auto*  toc     = docDialog.findChild<QTreeView*>("toc");
    auto*  browser = docDialog.findChild<QTextBrowser*>();
    auto*  btnNext = docDialog.findChild<QToolButton*>("btnNext");
    auto*  btnPrev = docDialog.findChild<QToolButton*>("btnPrev");

    QVERIFY(toc     != nullptr);
    QVERIFY(browser != nullptr);
    QVERIFY(btnNext != nullptr);
    QVERIFY(btnPrev != nullptr);

    auto* tocModel = dynamic_cast<SubTreeFilter*>(toc->model());
    QVERIFY(tocModel != nullptr);
    if (tocModel == nullptr)   // not needed due to QVERIFY above, but keeps static analyzer happy
        return;

    // Some extra tests here to placate the static analyzer
    QVERIFY(btnPrev != nullptr && !btnPrev->isEnabled());
    QVERIFY(btnNext != nullptr && !btnNext->isEnabled());

    // QVERIFY(!tocModel->findItems("Introduction").isEmpty());
    toc->setCurrentIndex(tocModel->index(0,0));
    QCOMPARE(browser->source(), QUrl("Intro.html"));

    // TODO: localization
    QVERIFY(browser->document()->toHtml().contains("manage collections of GPS tracks"));

    QTest::keyClick(toc, Qt::Key_Down);
    QTest::keyClick(toc, Qt::Key_Down);
    QCOMPARE(browser->source(), QUrl("Interface.html"));
    QVERIFY(browser->document()->toHtml().contains("User Interface")); // TODO: localization
    QVERIFY(btnPrev != nullptr && btnPrev->isEnabled());
    QVERIFY(btnNext != nullptr && !btnNext->isEnabled());

    QTest::mouseClick(btnPrev, Qt::LeftButton);
    QCOMPARE(browser->source(), QUrl("Intro.html"));
    QVERIFY(btnPrev != nullptr && !btnPrev->isEnabled());
    QVERIFY(btnNext != nullptr && btnNext->isEnabled());

    QTest::mouseClick(btnNext, Qt::LeftButton);
    QCOMPARE(browser->source(), QUrl("Interface.html"));
    QVERIFY(btnPrev != nullptr && btnPrev->isEnabled());
    QVERIFY(btnNext != nullptr && !btnNext->isEnabled());

    // Test that we have icons for everything
    bool foundNoIcon = false;
    Util::Recurse(*tocModel, [&docDialog, &foundNoIcon](const QModelIndex& idx) {
        if (docDialog.tocResource(idx).isValid() &&
            docDialog.tocIcon(idx).isEmpty())
            foundNoIcon = true;
        return true;
    });

    QVERIFY(!foundNoIcon);

    // Test searching
    {
        auto* tocQuery = docDialog.findChild<QLineEdit*>("tocQuery");
        QVERIFY(tocQuery != nullptr);

        const auto allIndexes = Util::RowIndexes(*tocModel);
        tocQuery->setText("ztgps");

        QVERIFY(Util::RowIndexes(*tocModel).count() < allIndexes.count());

        tocQuery->setText("");

        QVERIFY(Util::RowIndexes(*tocModel).count() == allIndexes.count());
    }
}

void TestZtgps::tag_selector() const
{
    cfgDataWritable().setupDefaultConfig();  // to get the default tag set

    createMainWindowPrivate(m_mainWindow_private);
    QVERIFY(m_mainWindow_private);

    const QString confFile = sampleConfFile();

    QSettings settings(confFile, QSettings::IniFormat);
    CmdLine loadOld("testztgps", "1.0", BuildDate, { "testztgps", "--private-session", "--conf", confFile });
    loadOld.processArgs();   // set new session file

    MainWindow mainWindow(nullptr, &loadOld);

    TagSelector tagSelector(mainWindow);

    auto* availableTags = tagSelector.findChild<QTreeView*>("availableTags");
    auto* activeTags    = tagSelector.findChild<QTreeView*>("activeTags");

    QVERIFY(availableTags != nullptr);
    QVERIFY(activeTags != nullptr);

    auto* tagLeft  = tagSelector.findChild<QToolButton*>("tagLeft");
    auto* tagRight = tagSelector.findChild<QToolButton*>("tagRight");
    auto* tagUp    = tagSelector.findChild<QToolButton*>("tagUp");
    auto* tagDown  = tagSelector.findChild<QToolButton*>("tagDown");

    QVERIFY(tagLeft != nullptr);
    QVERIFY(tagRight != nullptr);
    QVERIFY(tagUp != nullptr);
    QVERIFY(tagDown != nullptr);

    QVERIFY(tagSelector.tags().isEmpty());
    tagSelector.show(); // to populate tag list
    tagSelector.hide();
    QVERIFY(tagSelector.tags().isEmpty());

    QString name_00;

    {
        const QModelIndex item_0    = availableTags->model()->index(0, 0);
        const QModelIndex item_00   = availableTags->model()->index(0, 0, item_0);

        name_00 = availableTags->model()->data(item_00).toString();

        QCOMPARE(name_00, QString("Bikepack"));
        availableTags->selectionModel()->select(item_00, QItemSelectionModel::SelectCurrent | QItemSelectionModel::Rows);

        QTest::mouseClick(tagLeft, Qt::LeftButton);
        QCOMPARE(tagSelector.tags().size(), 1);
        QCOMPARE(tagSelector.tags().constFirst(), name_00);
    }

    // Test recent buttons
    {
        auto* tagMoveButtons = tagSelector.findChild<QVBoxLayout*>("tagMoveButtons");
        QVERIFY(tagMoveButtons != nullptr);

        if (tagMoveButtons != nullptr) { // this is unneeded, here to placate the static analyzer
            QVERIFY(tagMoveButtons->count() >= 11); // 6 tags + 4 movement + spacer

            for (int b = 0; b < 5; ++b)
                QVERIFY(tagMoveButtons->itemAt(b)->widget() != nullptr);
        }

        QWidget* btnRecreation = tagMoveButtons->itemAt(0)->widget();
        QWidget* btnRoad       = tagMoveButtons->itemAt(1)->widget();

        QVERIFY(!tagSelector.tags().contains("Recreation"));
        QVERIFY(!tagSelector.tags().contains("Road"));

        QTest::mouseClick(btnRecreation, Qt::LeftButton); // Recreation

        QVERIFY(tagSelector.tags().contains("Recreation"));
        QVERIFY(!tagSelector.tags().contains("Road"));

        QTest::mouseClick(btnRoad, Qt::LeftButton); // Road

        QVERIFY(tagSelector.tags().contains("Recreation"));
        QVERIFY(tagSelector.tags().contains("Road"));

        QTest::mouseClick(btnRecreation, Qt::LeftButton); // Recreation

        QVERIFY(!tagSelector.tags().contains("Recreation"));
        QVERIFY(tagSelector.tags().contains("Road"));

        QTest::mouseClick(btnRoad, Qt::LeftButton); // Road

        QVERIFY(!tagSelector.tags().contains("Recreation"));
        QVERIFY(!tagSelector.tags().contains("Road"));
    }

    {
        const QModelIndex item_0    = availableTags->model()->index(0, 0);
        const QModelIndex item_00   = availableTags->model()->index(0, 0, item_0);
        availableTags->selectionModel()->select(item_00, QItemSelectionModel::SelectCurrent | QItemSelectionModel::Rows);
        QTest::mouseClick(tagLeft, Qt::LeftButton);
        QCOMPARE(tagSelector.tags().size(), 2);
        QCOMPARE(tagSelector.tags().constFirst(), name_00);
    }

    {
        const QModelIndex item_0    = activeTags->model()->index(0, 0);
        activeTags->selectionModel()->select(item_0, QItemSelectionModel::SelectCurrent | QItemSelectionModel::Rows);
        QTest::mouseClick(tagDown, Qt::LeftButton);
        QCOMPARE(tagSelector.tags().size(), 2);
        QCOMPARE(tagSelector.tags().constLast(), name_00);
        QTest::mouseClick(tagUp, Qt::LeftButton);
        QCOMPARE(tagSelector.tags().constFirst(), name_00);
    }

    {
        activeTags->selectAll();
        QTest::mouseClick(tagRight, Qt::LeftButton);
        QVERIFY(tagSelector.tags().isEmpty());
    }
}

template <class W> QString testTooltips(const QObject* group)
{
    // Everything should have a tooltip except the hidden QLIneEdit Qt adds to QSpinboxes,
    // and similar Qt details.

    for (const auto* w : group->findChildren<W*>()) {
        if (isQtInternal(w))
            continue;

        // Everything should have a tooltip except the hidden QLIneEdit Qt adds to QSpinboxes
        if (w->toolTip().isEmpty())
            return QString("Widget has empty tooltip: ") +
                    w->metaObject()->className() + ": " + w->objectName();

        if (w->whatsThis().isEmpty())
            return QString("Widget has empty whatsThis: ") +
                    w->metaObject()->className() + ": " + w->objectName();
    }

    return { };
}

template <class W> void testUISearch(AppConfig& appConfig, SubTreeFilter* tocModel, const W* w)
{
    auto* tocQuery = appConfig.findChild<QLineEdit*>("tocQuery");
    QVERIFY(tocQuery != nullptr);
    QVERIFY(w != nullptr);

    if (Util::PlainText(w).isEmpty()) // avoid things with no labels or titles to search in
        return;

    tocQuery->setText(QString());

    const auto allIndexes = Util::RowIndexes(*tocModel);

    tocQuery->setText(QString("* == \"") + Util::PlainText(w) + "\"");

    QVERIFY(Util::RowIndexes(*tocModel).size() < allIndexes.size());

    if (w->styleSheet().isEmpty()) {
        QWARN(qUtf8Printable(w->objectName() + " (" + Util::PlainText(w) + ")"));
        QWARN(qUtf8Printable(tocQuery->text()));
    }

    QVERIFY(!w->styleSheet().isEmpty());

    tocQuery->setText(QString());

    QVERIFY(w->styleSheet().isEmpty());
    QVERIFY(Util::RowIndexes(*tocModel).size() == allIndexes.size());
}

template <class W> void testUISearch(AppConfig& appConfig, QWidget* root, SubTreeFilter* tocModel)
{
    for (const auto* w : root->findChildren<W>())
        testUISearch(appConfig, tocModel, w);
}

void TestZtgps::app_config() const
{
    const CmdLine::SaveMessageHandler handler(captureOutErr);
    clearOutErr();

    MainWindow mainWindow(nullptr, &m_privateSessionCL);
    AppConfig appConfig(&mainWindow);
    appConfig.show();
    appConfig.hide();

    app().newSession();
    m_privateSessionCL.processArgs();

    auto*  toc        = appConfig.findChild<QTreeView*>("toc");
    auto*  appCfgTabs = appConfig.findChild<QStackedWidget*>("appCfgTabs");

    QVERIFY(toc        != nullptr);
    QVERIFY(appCfgTabs != nullptr);
    auto* tocModel = dynamic_cast<SubTreeFilter*>(toc->model());
    QVERIFY(tocModel != nullptr);
    if (tocModel == nullptr)   // not needed due to QVERIFY above, but keeps static analyzer happy
        return;

    auto* caseSensitiveFilters = appConfig.findChild<QCheckBox*>("caseSensitiveFilters");
    auto* defaultTrackAlphaC = appConfig.findChild<QSpinBox*>("defaultTrackAlphaC");
    QVERIFY(caseSensitiveFilters);
    QVERIFY(defaultTrackAlphaC);

    const auto edit1 = [&]() {
        QTest::mouseClick(caseSensitiveFilters, Qt::LeftButton);
        QTest::keyClick(defaultTrackAlphaC, Qt::Key_Up);
    };

    const auto edit2 = [&]() {
        QTest::keyClick(defaultTrackAlphaC, Qt::Key_Up);
    };

    const auto check = [&](int uiDefaultTrackAlphaC, bool uiCaseFilter,
                           int cfgDefaultTrackAlphaC, bool cfgCaseFilter,
                           int undoCount, int redoCount)
    {
        QCOMPARE(defaultTrackAlphaC->value(), uiDefaultTrackAlphaC);
        QCOMPARE(caseSensitiveFilters->isChecked(), uiCaseFilter);
        QCOMPARE(cfgData().defaultTrackAlphaC, cfgDefaultTrackAlphaC);
        QCOMPARE(cfgData().caseSensitiveFilters, cfgCaseFilter);

        QCOMPARE(undoMgr().undoCount(), undoCount);
        QCOMPARE(undoMgr().redoCount(), redoCount);
    };

    if (QString result = testTooltips<QComboBox>(appCfgTabs); !result.isEmpty())
        QVERIFY2(result.isEmpty(), qUtf8Printable(result));

    if (QString result = testTooltips<QSpinBox>(appCfgTabs); !result.isEmpty())
        QVERIFY2(result.isEmpty(), qUtf8Printable(result));

    if (QString result = testTooltips<QDoubleSpinBox>(appCfgTabs); !result.isEmpty())
        QVERIFY2(result.isEmpty(), qUtf8Printable(result));

    if (QString result = testTooltips<QToolButton>(appCfgTabs); !result.isEmpty())
        QVERIFY2(result.isEmpty(), qUtf8Printable(result));

    if (QString result = testTooltips<QPushButton>(appCfgTabs); !result.isEmpty())
        QVERIFY2(result.isEmpty(), qUtf8Printable(result));

    if (QString result = testTooltips<QLineEdit>(appCfgTabs); !result.isEmpty())
        QVERIFY2(result.isEmpty(), qUtf8Printable(result));

    if (QString result = testTooltips<QCheckBox>(appCfgTabs); !result.isEmpty())
        QVERIFY2(result.isEmpty(), qUtf8Printable(result));

    const int dtac = cfgData().defaultTrackAlphaC;
    QCOMPARE(dtac > 100 && dtac < 150, true);
    QCOMPARE(cfgData().caseSensitiveFilters, false);

    edit1();                                   // make some UI changes
    check(dtac+1, true, dtac, false, 0, 0);    // UI should have changed, cfgData shouldn't.

    appConfig.resetDefault(); // reset cfgData to defaults: bypass the modal message box
    check(dtac, false, dtac, false, 0, 0);     // UI and cfgData should match defaults again

    edit1();           // make some changes
    check(dtac+1, true, dtac, false, 0, 0);    // UI should have changed, cfgData shouldn't.

    appConfig.resetPrevious();                 // reset cfgData to previous: bypass the modal message box
    check(dtac, false, dtac, false, 0, 0);     // UI and cfgData should match defaults again

    edit1();                                   // make some UI changes
    appConfig.applyCurrent();                  // apply our changes: they should now be reflected in cfgData
    check(dtac+1, true, dtac+1, true, 0, 0);   // cfgData should reflect UI values, but no undos set since no apply

    appConfig.resetPrevious();                 // reset cfgData to previous: bypass the modal message box
    check(dtac, false, dtac, false, 0, 0);     // UI and cfgData should match defaults again

    edit1();                                   // make some UI changes
    check(dtac+1, true, dtac, false, 0, 0);    // UI should have changed, cfgData shouldn't.
    appConfig.accept();                        // accept changes
    check(dtac+1, true, dtac+1, true, 1, 0);   // UI and cfgData should both have changed, with 1 undo

    appConfig.onShow();                        // fake-show UI again
    edit2();                                   // more edits
    check(dtac+2, true, dtac+1, true, 1, 0);   //
    appConfig.resetPrevious();                 // reset cfgData to previous: bypass the modal message box
    check(dtac+1, true, dtac+1, true, 1, 0);   // verify it's the previous, not the defaults!

    edit2();                                   // edit again
    appConfig.accept();                        // accept changes
    check(dtac+2, true, dtac+2, true, 2, 0);   // should have two undos now

    undoMgr().undo();                          // Undo the first cfgData change
    check(dtac+2, true, dtac+1, true, 1, 1);   // should have one more undo, and one redo

    undoMgr().undo();                          // Undo the other cfgData change
    check(dtac+2, true, dtac, false, 0, 2);    // should have no undos, two redos

    undoMgr().redo();                          // Redo
    check(dtac+2, true, dtac+1, true, 1, 1);   // should have one more undo, and one redo

    undoMgr().redo();                          // Redo other change
    check(dtac+2, true, dtac+2, true, 2, 0);   // should have two undos now

    undoMgr().undo();                          // Undo
    check(dtac+2, true, dtac+1, true, 1, 1);   // should have one more undo, and one redo
    appConfig.onShow();                        // fake-show UI again
    check(dtac+1, true, dtac+1, true, 1, 1);   // UI should reflect current state

    // Test that we have icons for everything
    bool foundNoIcon = false;
    bool foundNoPage = false;
    bool foundNoAppCfg = false;

    Util::Recurse(*tocModel, [&](const QModelIndex& idx) {
        const int page = appConfig.tocResource(idx).toInt();

        if (page >= 0) {
            if (appConfig.tocIcon(idx).isEmpty())
                foundNoIcon = true;

            appConfig.changePage(idx);

            const auto* appCfgTabs = appConfig.findChild<const QStackedWidget*>("appCfgTabs");
            if (appCfgTabs == nullptr)
                foundNoAppCfg = true;

            if (appCfgTabs != nullptr && appCfgTabs->currentIndex() != page)
                foundNoPage = true;
        }
        return true;
    });

    QVERIFY(!foundNoIcon);
    QVERIFY(!foundNoPage);
    QVERIFY(!foundNoAppCfg);

    // Test tocQuery searching. Do this a page at a time to avoid page-switching issues where
    // the first hit for a given string is on a prior page, so wouldn't get colorized.
    for (int page = 0; page < appCfgTabs->count(); ++page) {
        QWidget* pageRoot = appCfgTabs->widget(page);
        QVERIFY(pageRoot != nullptr);

        appConfig.changePage(appConfig.index(QVariant(page)));

        testUISearch<QLabel*>(appConfig, pageRoot, tocModel);
        testUISearch<QCheckBox*>(appConfig, pageRoot, tocModel);
        testUISearch<QAbstractButton*>(appConfig, pageRoot, tocModel);
    }

    appConfig.hide();
}

void TestZtgps::about_dialog()
{
    static const QRegularExpression buildRe("build:.*2[0-9][0-9][0-9]");

    AboutDialog about;
    about.setup();

    const QLabel* buildDate = about.findChild<QLabel*>("buildDate");

    QVERIFY(buildDate != nullptr);

    if (buildDate != nullptr)  // not needed, here to placate the static analyzer
        QVERIFY(buildRe.match(buildDate->text()).hasMatch());

    const QLabel* ztTitle1 = about.findChild<QLabel*>("aboutZTTitle_1");
    const QLabel* ztTitle2 = about.findChild<QLabel*>("aboutZTTitle_2");

    QVERIFY(ztTitle1 != nullptr && ztTitle1->text().contains(Apptitle));
    QVERIFY(ztTitle2 != nullptr && ztTitle2->text().contains(Apptitle));

    const QTextBrowser* license = about.findChild<QTextBrowser*>("aboutTabLicense");
    QVERIFY(license != nullptr && license->toHtml().contains("GNU GENERAL PUBLIC LICENSE"));

    const QTextBrowser* privacy = about.findChild<QTextBrowser*>("aboutTabPrivacy");
    QVERIFY(privacy != nullptr && privacy->toHtml().contains("Privacy Policy")); // TODO: localization

    auto* tabs = about.findChild<QTabWidget*>("aboutTabs");
    QVERIFY(tabs != nullptr && tabs->count() > 4);

    for (int i = 0; i < tabs->count(); ++i) {
        tabs->setCurrentIndex(i);
        QVERIFY(!tabs->tabIcon(i).isNull());
    }

    const auto* tabSupport = about.findChild<const QTextBrowser*>("aboutTabSupport");
    QVERIFY(tabSupport);
    if (tabSupport == nullptr) // unneeded, but makes static analyzer happy
        return;

    QVERIFY(tabSupport->toHtml().contains("zombietrackergps.net"));
}

void TestZtgps::gpsd_version_dialog() const
{
    createMainWindowPrivate(m_mainWindow_private);

    GpsdVersionDialog verDialog(*m_mainWindow_private);
    verDialog.show();

    const QTreeView* gpsdVersions = verDialog.findChild<QTreeView*>("gpsdVersions");
    const QAbstractItemModel* model = gpsdVersions->model();

    QVERIFY(model != nullptr && model->rowCount() >= 5);
    QVERIFY(model != nullptr && model->columnCount() == 3);

    const QRegularExpression gpsdLibRegex("libgps[.]so[.][0-9]+", QRegularExpression::DotMatchesEverythingOption);
    const QRegularExpression gpsdApiRegex("[0-9]+", QRegularExpression::DotMatchesEverythingOption);

    for (int r = 0; r < model->rowCount(); ++r) {
        QVERIFY(gpsdLibRegex.match(model->data(model->index(r, 0)).toString()).hasMatch());
        QVERIFY(gpsdApiRegex.match(model->data(model->index(r, 1)).toString()).hasMatch());
    }
}

bool isQtInternal(const QObject* w)
{
    // Qt adds its own sub-widgets to several classes, which may not always be visible. That's fine.
    return QString(w->parent()->metaObject()->className()) == "QLineEdit" ||
            QString(w->parent()->metaObject()->className()) == "QSpinBox" ||
            QString(w->parent()->metaObject()->className()) == "QDoubleSpinBox";
}

template <class W> QString testSubWidgets(const TabWidget* secondaryWin)
{
    auto* pane = secondaryWin->findChild<PaneBase*>();

    if (pane == nullptr)
        return "Unable to find PaneBase";

    auto* filterCtrl = pane->findChild<QObject*>("filterCtrl");

    if (!pane->isEnabled())
        QString("Pane should be enabled: ") + pane->name();

    // Several pane classes don't have a filterCtrl, but most do.
    if (pane->paneClass() == int(PaneClass::SimpleView) ||
        pane->paneClass() == int(PaneClass::Empty))
        return { };

    if (filterCtrl == nullptr)
        return QString("Unable to find filterCtrl: ") + pane->name();

    if (!pane->isCheckable())
        return QString("Pane should be checkable: ") + pane->name();

    if (QString result = testTooltips<W>(filterCtrl); !result.isEmpty())
        return result;

    for (const auto* w : filterCtrl->findChildren<W*>()) {
        if (isQtInternal(w))
            continue;

        if (w->isVisibleTo(secondaryWin) != pane->isChecked())
            return QString("Widget visible state should match pane checked state: ") +
                    w->metaObject()->className() + ": " +
                    w->objectName() + ": " +
                    (w->isVisibleTo(secondaryWin) ? "true" : "false") + " " +
                    (pane->isChecked() ? "true" : "false");
    }

    return { };
}

QString testWidgetEnables(TabWidget* secondaryWin, bool check)
{
    auto* pane = secondaryWin->findChild<PaneBase*>();

    if (pane == nullptr)
        return "Unable to find PaneBase";

    pane->setChecked(check);

    if (QString result = testSubWidgets<QComboBox>(secondaryWin); !result.isEmpty())
        return result;

    if (QString result = testSubWidgets<QSpinBox>(secondaryWin); !result.isEmpty())
        return result;

    if (QString result = testSubWidgets<QDoubleSpinBox>(secondaryWin); !result.isEmpty())
        return result;

    if (QString result = testSubWidgets<QToolButton>(secondaryWin); !result.isEmpty())
        return result;

    if (QString result = testSubWidgets<QPushButton>(secondaryWin); !result.isEmpty())
        return result;

    if (QString result = testSubWidgets<QLineEdit>(secondaryWin); !result.isEmpty())
        return result;

    if (QString result = testSubWidgets<QCheckBox>(secondaryWin); !result.isEmpty())
        return result;

    return { };
}


QString TestZtgps::testPaneCreate() const
{
    QFETCH(int, pane_class);

    // Start by closing all top level windows, in case some prior test failed and left one open.
    for (auto* window : QApplication::topLevelWidgets())
        window->close();

    QTest::qWait(1); // tabs are closed under deleteLater, so let event loop run, so they go away

    undoMgr().clear();

    const auto countTopWindows = []() {
        int count = 0;
        for (auto* window : QApplication::topLevelWidgets())
            if (dynamic_cast<TabWidget*>(window) != nullptr)
                ++count;

        return count;
    };

    const auto findSecondaryWin = []() -> TabWidget* {
        for (auto* window : QApplication::topLevelWidgets())
            if (auto* tabWidget = dynamic_cast<TabWidget*>(window))
                return tabWidget;
        return nullptr;
    };

    m_mainWindow_private->paneClassWinActions().at(pane_class)->trigger();

    if (countTopWindows() != 1)
        return QString("Wrong number of top level widgets after opening pane in secondary window: %1")
                .arg(countTopWindows());

    appBase().cfgData().warnOnClose = false; // for our non-interactive test

    TabWidget* secondaryWin = findSecondaryWin();

    if (secondaryWin == nullptr)
        return { "Secondary window not found" };

    // Test check enable/disable
    {
        // Show controls
        if (QString result = testWidgetEnables(secondaryWin, true); !result.isEmpty())
            return result;

        // Hide controls
        if (QString result = testWidgetEnables(secondaryWin, false); !result.isEmpty())
            return result;

        // Show controls again
        if (QString result = testWidgetEnables(secondaryWin, true); !result.isEmpty())
            return result;
    }

    // This test is expensive: only do it for a single pane class
    if (pane_class == int(PaneClass::View)) {
        if (secondaryWin->count() != 2) // two because of new-tab tab
            return { "Expected one tab" };

        secondaryWin->addTabInteractive();

        if (secondaryWin->count() != 3)
            return { "Expected two tabs" };

        const PaneGroup* group = dynamic_cast<PaneGroup*>(secondaryWin->widget(1));

        if (group == nullptr)
            return { "Expected pane group" };

        if (dynamic_cast<EmptyPane*>(group->widget(0)) == nullptr)
            return { "Expected empty pane" };

        if (secondaryWin->currentIndex() != 1)
            return QString("Expected current index == 1: found %1").arg(secondaryWin->currentIndex());

        undoMgr().undo();  // undo adding pane
        QTest::qWait(1); // tabs are closed under deleteLater, so let even loop run, so they go away

        secondaryWin = findSecondaryWin();

        if (secondaryWin->count() != 2) // two because of new-tab tab
            return { "Expected one tab after undo" };

        // Verify the original opened tab is still there
        group = dynamic_cast<PaneGroup*>(secondaryWin->widget(0));
        if (group == nullptr)
            return { "Expected pane group" };

        const PaneBase* pane0 = dynamic_cast<PaneBase*>(group->widget(0));

        if (pane0 == nullptr)
            return { "Expected pane" };

        if (pane0->paneClass() != PaneClass_t(pane_class))
            return { "Wrong pane class after undo tab open" };

        undoMgr().redo(); // redo adding pane
        QTest::qWait(1);
        secondaryWin = findSecondaryWin();

        if (secondaryWin->count() != 3)
            return QString("Expected three tabs after redo: found %1").arg(secondaryWin->count());
    }

    if (!secondaryWin->close())
        return QString("Secondary window close failed: ") + secondaryWin->windowTitle();

    QTest::qWait(1); // tabs are closed under deleteLater, so let event loop run, so they go away

    if (countTopWindows() != 0)
        return QString("Wrong number of top level widgets after closing secondary window: %1")
                .arg(countTopWindows());

    return RcOK;
}

void TestZtgps::create_all_pane_classes_data() const
{
    createMainWindowPrivate(m_mainWindow_private);

    QTest::addColumn<int>("pane_class");

    // This leaks memory, but it's just a test suite, so not worth the effort to fix.
    for (PaneClass pc = PaneClass::_First; pc < PaneClass::_Count; Util::inc(pc))
        QTest::newRow(strdup(qUtf8Printable(Pane::name(pc)))) << int(pc);

    QTest::newRow("cleanup") << -1;  // cleanup
}

void TestZtgps::createMainWindowPrivate(MainWindow*& mainWindow) const
{
    if (mainWindow != nullptr) {
        mainWindow->newSession();
        return;
    }

    CmdLine::pushMessageHandler(captureOutErr);
    clearOutErr();
    m_privateSessionCL.processArgs();   // set new session file
    mainWindow = new MainWindow(nullptr, &m_privateSessionCL);
    mainWindow->newSession();
}

void TestZtgps::deleteMainWindowPrivate(MainWindow*& mainWindow)
{
    delete mainWindow;
    mainWindow = nullptr;
}

void TestZtgps::create_all_pane_classes()
{
    QFETCH(int, pane_class);

    if (pane_class >= 0) {
        QCOMPARE(testPaneCreate(), QString(RcOK));
    } else {
        delete m_mainWindow_private;
        m_mainWindow_private = nullptr;
    }
}

void TestZtgps::model_tooltips() const
{
    app().newSession();
    app().cfgData().setupDefaultConfig(); // default config options

    Models models;

    const QString mysticGpx = QFINDTESTDATA(DATAROOT "gps/mystic_basin_trail.gpx");
    GeoLoad loader(nullptr, models.m_trkModel, models.m_wptModel);
    QVERIFY(loader.load(mysticGpx));
    QVERIFY(models(App::Model::Track)->rowCount() == 5);

    // Test TrackModel tooltips
    {
        const TrackModel& model = models.m_trkModel;

        for (int row = 0; row < model.rowCount(); ++row) {
            app().cfgData().tooltipInlineLimit = 512;
            app().cfgData().tooltipFullLimit = 4096;

            for (ModelType col : { TrackModel::Name, TrackModel::Length, TrackModel::BeginDate,
                                   TrackModel::Ascent, TrackModel::MaxSpeed }) {
                const QString data = model.data(model.index(row, col), Qt::ToolTipRole).toString();

                // Look for certain column data in the tooltip
                for (ModelType ttcol : { TrackModel::Name, TrackModel::Length, TrackModel::Notes,
                                         TrackModel::MaxElevation, TrackModel::Ascent})
                    QVERIFY(data.contains(model.data(model.index(row, ttcol), Qt::DisplayRole).toString()));
            }

            const QString notesData = model.data(model.index(row, TrackModel::Notes), Util::RawDataRole).toString();

            // Verify notes tracktip
            {
                const QString notesTT = model.data(model.index(row, TrackModel::Notes), Qt::ToolTipRole).toString();
                QVERIFY(notesTT.contains(notesData));
            }

            // Set small size for inline limit and re-test.  Track tooltip should not contain notes, but notes tooltip should.
            {
                app().cfgData().tooltipInlineLimit = 4;
                app().cfgData().tooltipFullLimit = 4096;

                const QString notesTT = model.data(model.index(row, TrackModel::Notes), Qt::ToolTipRole).toString();
                const QString mainTT  = model.data(model.index(row, TrackModel::Name), Qt::ToolTipRole).toString();

                QVERIFY(!mainTT.contains(notesData));
                QVERIFY(notesTT.contains(notesData));
                QVERIFY(mainTT.contains(app().cfgData().trackNoteIcon));
            }

            // Set small size for both limits
            {
                app().cfgData().tooltipInlineLimit = 4;
                app().cfgData().tooltipFullLimit = 8;

                const QString notesTT = model.data(model.index(row, TrackModel::Notes), Qt::ToolTipRole).toString();
                const QString mainTT  = model.data(model.index(row, TrackModel::Name), Qt::ToolTipRole).toString();

                QVERIFY(!mainTT.contains(notesData));
                QVERIFY(notesTT.isEmpty());
                QVERIFY(mainTT.contains(app().cfgData().trackNoteIcon));
            }
        }
    }

    // Test PointModel tooltips
    {
        TrackModel& model = models.m_trkModel;
        PointModel* pm = model.geoPoints(model.index(0, 0));
        QVERIFY(pm != nullptr);

        const QModelIndex idx00Desc = pm->index(0, PointModel::Desc, pm->index(0, 0));
        const QModelIndex idx00Name = pm->index(0, PointModel::Name, pm->index(0, 0));

        app().cfgData().tooltipInlineLimit = 512;
        app().cfgData().tooltipFullLimit = 4096;

        // large sizes
        {
            const QString descData = pm->data(idx00Desc, Util::RawDataRole).toString();
            const QString descTT = pm->data(idx00Desc, Qt::ToolTipRole).toString();
            const QString mainTT = pm->data(idx00Name, Qt::ToolTipRole).toString();

            QVERIFY(descData.contains("Starting point"));
            QVERIFY(descTT.contains(descData));
            QVERIFY(mainTT.contains(descData));
            QVERIFY(!mainTT.isEmpty());
        }

        // small inline limit
        {
            app().cfgData().tooltipInlineLimit = 4;
            app().cfgData().tooltipFullLimit = 4096;

            const QString descData = pm->data(idx00Desc, Util::RawDataRole).toString();
            const QString descTT = pm->data(idx00Desc, Qt::ToolTipRole).toString();
            const QString mainTT = pm->data(idx00Name, Qt::ToolTipRole).toString();

            QVERIFY(descData.contains("Starting point"));
            QVERIFY(descTT.contains(descData));
            QVERIFY(!mainTT.contains(descData));
            QVERIFY(mainTT.contains(app().cfgData().trackNoteIcon));
            QVERIFY(!mainTT.isEmpty());
        }

        // both small limits
        {
            app().cfgData().tooltipInlineLimit = 4;
            app().cfgData().tooltipFullLimit = 4;

            const QString descData = pm->data(idx00Desc, Util::RawDataRole).toString();
            const QString descTT = pm->data(idx00Desc, Qt::ToolTipRole).toString();
            const QString mainTT = pm->data(idx00Name, Qt::ToolTipRole).toString();

            QVERIFY(descData.contains("Starting point"));
            QVERIFY(!descTT.contains(descData));
            QVERIFY(!mainTT.contains(descData));
            QVERIFY(descTT.isEmpty());
            QVERIFY(mainTT.contains(app().cfgData().trackNoteIcon));
        }

        app().cfgData().tooltipInlineLimit = 512;
        app().cfgData().tooltipFullLimit = 4096;
    }
}

Q_DECLARE_METATYPE(App::Model)

bool TestZtgps::testQuery(const TreeModel* model)
{
    if (model == nullptr)
        return false;

    QFETCH(QString, query);

    return Query::Context(model).parse(query)->isValid();
}

void TestZtgps::query_data()
{
    QTest::addColumn<bool>("valid");
    QTest::addColumn<App::Model>("modelType");
    QTest::addColumn<QString>("query");

    QTest::newRow("err.track.bad_column")     << false << App::Model::Track << "NotAColumn : 5";
    QTest::newRow("err.track.bad_open_paren") << false << App::Model::Track << ") Name : abc";
    QTest::newRow("err.track.bad_nesting-1")  << false << App::Model::Track << "( Name : abc";
    QTest::newRow("err.track.bad_nesting-2")  << false << App::Model::Track << "( ( Name : abc )";
    QTest::newRow("err.track.ascent.suffix")  << false << App::Model::Track << "Ascent > 40 BAD";
    QTest::newRow("err.track.bad_operator-1") << false << App::Model::Track << "Name : abc * name : def";
    QTest::newRow("err.track.begin_date-1")   << false << App::Model::Track << "begin_date > 2018-BAD-01";
    QTest::newRow("err.track.begin_date-2")   << false << App::Model::Track << "begin_date > 2018-BAD-44";
    QTest::newRow("err.track.begin_date-3")   << false << App::Model::Track << "begin_date > 2000-Mar-44";
    QTest::newRow("err.track.begin_date-4")   << false << App::Model::Track << "begin_date > 2000-Mar--3";
    QTest::newRow("err.track.begin_date-5")   << false << App::Model::Track << "begin_date > 2044-Feb-2f";
    QTest::newRow("err.track.begin_date-6")   << false << App::Model::Track << "begin_date > 204444-Feb-02";
    QTest::newRow("err.track.bad_operator-2") << false << App::Model::Track << "Name # abc";
    QTest::newRow("err.track.no-column-lt")   << false << App::Model::Track << "< abc";
    QTest::newRow("err.track.no-column-eq")   << false << App::Model::Track << "== abc";
    QTest::newRow("err.track.no-column-ne")   << false << App::Model::Track << "!= abc";
    QTest::newRow("err.track.no-column-re")   << false << App::Model::Track << "~ abc";
    QTest::newRow("err.track.bad-seq-and-1")  << false << App::Model::Track << "&& Name : abc";
    QTest::newRow("err.track.bad-seq-and-2")  << false << App::Model::Track << "& Name : abc";
    QTest::newRow("err.track.bad-seq-and-3")  << false << App::Model::Track << "Name : abc &&";
    QTest::newRow("err.track.bad-seq-and-4")  << false << App::Model::Track << "Name : abc &";
    QTest::newRow("err.track.bad-seq-and-5")  << false << App::Model::Track << "Name : abc & &";
    QTest::newRow("err.track.bad-seq-and-6")  << false << App::Model::Track << "Name : abc & |";
    QTest::newRow("err.track.bad-seq-and-7")  << false << App::Model::Track << "&&";
    QTest::newRow("err.track.bad-seq-or-1")   << false << App::Model::Track << "|| Name : abc";
    QTest::newRow("err.track.bad-seq-or-2")   << false << App::Model::Track << "| Name : abc";
    QTest::newRow("err.track.bad-seq-or-3")   << false << App::Model::Track << "Name : abc ||";
    QTest::newRow("err.track.bad-seq-or-4")   << false << App::Model::Track << "Name : abc |";
    QTest::newRow("err.track.bad-seq-or-5")   << false << App::Model::Track << "|";
    QTest::newRow("err.track.bad-seq-xor-1")  << false << App::Model::Track << "^ Name : abc";
    QTest::newRow("err.track.bad-seq-xor-2")  << false << App::Model::Track << "Name : abc ^";
    QTest::newRow("err.track.bad-seq-xor-3")  << false << App::Model::Track << "Name : abc ^ Name : xyz ^";
    QTest::newRow("err.track.bad-not-1")      << false << App::Model::Track << "!";
    QTest::newRow("err.track.bad-not-2")      << false << App::Model::Track << "Name : abc && !";
    QTest::newRow("err.view.bad_column")      << false << App::Model::View  << "Ascent > 40 km";
    QTest::newRow("err.view.bad_regex-1")     << false << App::Model::View  << "Name =~ fhi[";
    QTest::newRow("err.view.bad_regex-2")     << false << App::Model::View  << "Name =~ fh(i";
    QTest::newRow("err.view.bad_regex-3")     << false << App::Model::View  << "Name =~ *fhi";
    QTest::newRow("err.view.bad_paren-1")     << false << App::Model::View  << "Name ~ ( abc )";
    QTest::newRow("err.view.bad_paren-2")     << false << App::Model::View  << "( Name ~ ( abc ) )";
    QTest::newRow("err.point.elapsed-1")      << false << App::Model::Point << "elapsed > 1h 40m 10q";
    QTest::newRow("err.point.elapsed-2")      << false << App::Model::Point << "elapsed > 4";
    QTest::newRow("err.point.dist.suffix")    << false << App::Model::Point << "distance > 30bpm";
    QTest::newRow("err.point.bad-suffix-1")   << false << App::Model::Point << "distance > 30 foo";
    QTest::newRow("err.point.bad-suffix-2")   << false << App::Model::Point << "distance > 30foo";
    QTest::newRow("err.point.bad-cmp-1")      << false << App::Model::Point << "> 30";
    QTest::newRow("err.point.bad-cmp-2")      << false << App::Model::Point << "elapsed >";
    QTest::newRow("err.point.bad-cmp-3")      << false << App::Model::Point << "elapsed ==";

    QTest::newRow("track.empty-query")        << true  << App::Model::Track << "";
    QTest::newRow("track.op==")               << true  << App::Model::Track << "Name == abc";
    QTest::newRow("track.op!=")               << true  << App::Model::Track << "Name != abc";
    QTest::newRow("track.op<")                << true  << App::Model::Track << "Name < abc";
    QTest::newRow("track.op>")                << true  << App::Model::Track << "Name > abc";
    QTest::newRow("track.op<=")               << true  << App::Model::Track << "Name <= abc";
    QTest::newRow("track.op>=")               << true  << App::Model::Track << "Name >= abc";
    QTest::newRow("track.op:")                << true  << App::Model::Track << "Name : abc";
    QTest::newRow("track.op=~")               << true  << App::Model::Track << "Name =~ abc";
    QTest::newRow("track.op~")                << true  << App::Model::Track << "Name ~ abc";
    QTest::newRow("track.op!~")               << true  << App::Model::Track << "Name !~ abc";
    QTest::newRow("track.op~")                << true  << App::Model::Track << "Name ~ abc";
    QTest::newRow("track.op&")                << true  << App::Model::Track << "Name : abc & Name : def";
    QTest::newRow("track.op&&")               << true  << App::Model::Track << "Name : abc && Name : def";
    QTest::newRow("track.op^")                << true  << App::Model::Track << "Name : abc ^ Name : def";
    QTest::newRow("track.op|")                << true  << App::Model::Track << "Name : abc | Name : def";
    QTest::newRow("track.op||")               << true  << App::Model::Track << "Name : abc || Name : def";
    QTest::newRow("track.op,")                << true  << App::Model::Track << "Name : abc , Name : def";
    QTest::newRow("track.op!-1")              << true  << App::Model::Track << "! Name : abc";
    QTest::newRow("track.op!-2")              << true  << App::Model::Track << "! ( Name : abc )";
    QTest::newRow("track.any_column")         << true  << App::Model::Track << "abc";
    QTest::newRow("track.leading_spaces")     << true  << App::Model::Track << "   Name : abc";
    QTest::newRow("track.trailing_spaces")    << true  << App::Model::Track << "Name : abc   ";
    QTest::newRow("track.surrounding_spaces") << true  << App::Model::Track << " Name : abc  ";
    QTest::newRow("track.paren_1")            << true  << App::Model::Track << "( abc )";
    QTest::newRow("track.paren_2")            << true  << App::Model::Track << "( Name == abc )";
    QTest::newRow("track.paren_3")            << true  << App::Model::Track << "( ( Name == abc ) )";
    QTest::newRow("track.paren_4")            << true  << App::Model::Track << "( ( ! Name == abc ) )";
    QTest::newRow("track.multi-negation")     << true  << App::Model::Track << "! ! ! Name == abc";
    QTest::newRow("track.quoted-string")      << true  << App::Model::Track << "Name == \"string with spaces\"";
    QTest::newRow("track.multi-op-1")         << true  << App::Model::Track << "Name : a & Name : b | Name : c & Name : d";
    QTest::newRow("track.multi-op-2")         << true  << App::Model::Track << "( Name : a | Name : b ) & ( Name : c | Name : d )";
    QTest::newRow("track.column-*-1")         << true  << App::Model::Track << "* : abc";
    QTest::newRow("track.column-*-2")         << true  << App::Model::Track << "* < 5";
    QTest::newRow("track.suffix-1")           << true  << App::Model::Track << "Moving_Pow > 40 watts";
    QTest::newRow("track.suffix-2")           << true  << App::Model::Track << "Moving_Pow > 1 hp";
    QTest::newRow("track.suffix-3")           << true  << App::Model::Track << "Moving_Pow > 0.3 hp";
    QTest::newRow("track.suffix-3.clause")    << true  << App::Model::Track << "Moving_Pow > 0.3 hp & Max_HR > 150 bpm";
    QTest::newRow("track.suffix-3.clause")    << true  << App::Model::Track << "Moving_Pow > 0.3 hp & Max_HR > 150 bpm";
    QTest::newRow("track.flags-1")            << true  << App::Model::Track << "Flags : Norway";
    QTest::newRow("track.begin_date-1")       << true  << App::Model::Track << "begin_date > 2018-Jan-01";
    QTest::newRow("track.begin_date-2")       << true  << App::Model::Track << "begin_date > 2018-02-03";
    QTest::newRow("track.begin_date-3")       << true  << App::Model::Track << "begin_date > 2018-Jul-04 08:22:33";
    QTest::newRow("track.begin_date-4")       << true  << App::Model::Track << "begin_date > 04-Jul-2020";

    QTest::newRow("point.grade.%")            << true  << App::Model::Point << "Grade > 40 %";
    QTest::newRow("point.length.suffix-1")    << true  << App::Model::Point << "Length < 40 kilometers";
    QTest::newRow("point.length.case")        << true  << App::Model::Point << "LENGTH < 40 kilOMETERs";
    QTest::newRow("point.elapsed-1")          << true  << App::Model::Point << "elapsed > 1h";
    QTest::newRow("point.elapsed-2")          << true  << App::Model::Point << "elapsed > 1h40m";
    QTest::newRow("point.elapsed-3")          << true  << App::Model::Point << "elapsed > 1h 40m";
    QTest::newRow("point.elapsed-4")          << true  << App::Model::Point << "elapsed > 1h 40m 10s";
    QTest::newRow("point.elapsed-5")          << true  << App::Model::Point << "elapsed > 2days";
    QTest::newRow("point.elapsed-6")          << true  << App::Model::Point << "elapsed > 200 seconds";
    QTest::newRow("point.elapsed-7")          << true  << App::Model::Point << "elapsed > 200sec";
    QTest::newRow("point.temp-1")             << true  << App::Model::Point << "temp > 30C";
    QTest::newRow("point.temp-2")             << true  << App::Model::Point << "temp > 30F";
}

void TestZtgps::query()
{
    const Models models;

    QFETCH(bool, valid);
    QFETCH(App::Model, modelType);

    QCOMPARE(testQuery(models(modelType)), valid);
}

void TestZtgps::query_parse_benchmark()
{
    Models models;

    QBENCHMARK {
        (void)Query::Context(&models.m_wptModel).parse(m_testQuery);
    }
}

void TestZtgps::query_match_benchmark()
{
    Models  models;
    GeoLoad loader(nullptr, models.m_trkModel, models.m_wptModel);

    const QString mysticGpx    = QFINDTESTDATA(DATAROOT "gps/mystic_basin_trail.gpx");
    QVERIFY(loader.load(mysticGpx));
    const auto q = Query::Context(&models.m_wptModel).parse(m_testQuery);
    QVERIFY(q);

    // Try match against every waypoint (23)
    QBENCHMARK {
        for (int row = 0; row < models.m_wptModel.rowCount(); ++row)
            [[maybe_unused]] bool matched = q->match(models.m_wptModel, QModelIndex(), row);
    }
}

void TestZtgps::undo_view()
{
    ViewModel model;

    undoMgr().clear(); // start with fresh undo state

    QCOMPARE(app().undoMgr().undoCount(), 0);
    QCOMPARE(app().undoMgr().redoCount(), 0);
    QCOMPARE(app().undoMgr().atSavePoint(), true);

    // Add and verify a row
    model.appendRow("myView 1", ViewParams(30.0, 80.0, 0.0, 100), QFINDTESTDATA(DATAROOT "icons/beer1.svg"));
    QCOMPARE(model.rowCount(), 1);
    QCOMPARE(app().undoMgr().undoCount(), 1);
    QCOMPARE(app().undoMgr().redoCount(), 0);
    QCOMPARE(app().undoMgr().atSavePoint(), false);
    QModelIndex idx = model.index(0, 0);
    QCOMPARE(model.data(ViewModel::Name,      idx, Util::RawDataRole).toString(), QString("myView 1"));
    QCOMPARE(model.data(ViewModel::CenterLat, idx, Util::RawDataRole).toDouble(), 30.0);
    QCOMPARE(model.data(ViewModel::CenterLon, idx, Util::RawDataRole).toDouble(), 80.0);
    QCOMPARE(model.data(ViewModel::Heading,   idx, Util::RawDataRole).toDouble(), 0.0);
    QCOMPARE(model.data(ViewModel::Zoom,      idx, Util::RawDataRole).toInt(),    100);

    // Change name and CenterLon
    model.setData(idx.sibling(idx.row(), ViewModel::Name), "myView 1.mod", Util::RawDataRole);
    QCOMPARE(app().undoMgr().undoCount(), 2);
    QCOMPARE(app().undoMgr().redoCount(), 0);
    model.setData(idx.sibling(idx.row(), ViewModel::CenterLat), -10.0, Util::RawDataRole);
    QCOMPARE(app().undoMgr().undoCount(), 3);
    QCOMPARE(app().undoMgr().redoCount(), 0);
    QCOMPARE(model.data(ViewModel::Name,      idx, Util::RawDataRole).toString(), QString("myView 1.mod"));
    QCOMPARE(model.data(ViewModel::CenterLat, idx, Util::RawDataRole).toDouble(), -10.0);

    QCOMPARE(undoMgr().topUndoName(), QString("Set Lat"));
    QCOMPARE(undoMgr().undo(), true);  // undo the set lat
    QCOMPARE(model.data(ViewModel::CenterLat, idx, Util::RawDataRole).toDouble(), 30.0);
    QCOMPARE(app().undoMgr().undoCount(), 2);
    QCOMPARE(app().undoMgr().redoCount(), 1);
    QCOMPARE(app().undoMgr().atSavePoint(), false);
    QCOMPARE(model.data(ViewModel::Name,      idx, Util::RawDataRole).toString(), QString("myView 1.mod"));
    QCOMPARE(model.data(ViewModel::CenterLat, idx, Util::RawDataRole).toDouble(), 30.0);

    QCOMPARE(undoMgr().redo(), true);  // redo the set lat
    QCOMPARE(model.data(ViewModel::CenterLat, idx, Util::RawDataRole).toDouble(), -10.0);
    QCOMPARE(model.data(ViewModel::Name,      idx, Util::RawDataRole).toString(), QString("myView 1.mod"));
    QCOMPARE(app().undoMgr().undoCount(), 3);
    QCOMPARE(app().undoMgr().redoCount(), 0);
    QCOMPARE(app().undoMgr().atSavePoint(), false);

    QCOMPARE(undoMgr().undo(), true);  // undo the set lat
    QCOMPARE(model.data(ViewModel::CenterLat, idx, Util::RawDataRole).toDouble(), 30.0);
    QCOMPARE(model.data(ViewModel::Name,      idx, Util::RawDataRole).toString(), QString("myView 1.mod"));
    QCOMPARE(app().undoMgr().undoCount(), 2);
    QCOMPARE(app().undoMgr().redoCount(), 1);
    QCOMPARE(app().undoMgr().atSavePoint(), false);

    QCOMPARE(undoMgr().undo(), true);  // undo the set name
    QCOMPARE(model.data(ViewModel::CenterLat, idx, Util::RawDataRole).toDouble(), 30.0);
    QCOMPARE(model.data(ViewModel::Name,      idx, Util::RawDataRole).toString(), QString("myView 1"));
    QCOMPARE(app().undoMgr().undoCount(), 1);
    QCOMPARE(app().undoMgr().redoCount(), 2);
    QCOMPARE(app().undoMgr().atSavePoint(), false);

    QCOMPARE(undoMgr().redo(), true);  // undo the set name
    QCOMPARE(model.data(ViewModel::CenterLat, idx, Util::RawDataRole).toDouble(), 30.0);
    QCOMPARE(model.data(ViewModel::Name,      idx, Util::RawDataRole).toString(), QString("myView 1.mod"));
    QCOMPARE(app().undoMgr().undoCount(), 2);
    QCOMPARE(app().undoMgr().redoCount(), 1);
    QCOMPARE(app().undoMgr().atSavePoint(), false);

    QCOMPARE(undoMgr().undoToSave(), true);  // undo until the save point
    QCOMPARE(app().undoMgr().undoCount(), 0);
    QCOMPARE(app().undoMgr().redoCount(), 3);
    QCOMPARE(app().undoMgr().atSavePoint(), true);
    QCOMPARE(model.rowCount(), 0);

    QCOMPARE(undoMgr().redoAll(), true);  // redo everything
    idx = model.index(0, 0);  // need a new index, since the row was deleted and restored
    QCOMPARE(model.data(ViewModel::CenterLat, idx, Util::RawDataRole).toDouble(), -10.0);
    QCOMPARE(model.data(ViewModel::Name,      idx, Util::RawDataRole).toString(), QString("myView 1.mod"));
    QCOMPARE(app().undoMgr().undoCount(), 3);
    QCOMPARE(app().undoMgr().redoCount(), 0);
    QCOMPARE(app().undoMgr().atSavePoint(), false);
    QCOMPARE(model.rowCount(), 1);

    undoMgr().clear(); // start with fresh undo state
    QCOMPARE(app().undoMgr().undoCount(), 0);
    QCOMPARE(app().undoMgr().redoCount(), 0);
    QCOMPARE(app().undoMgr().atSavePoint(), true);
    QCOMPARE(model.rowCount(), 1);
}

Q_DECLARE_METATYPE(GeoPolRegion::WorldOpt)

void TestZtgps::geopol_data()
{
    QTest::addColumn<double>("lat");
    QTest::addColumn<double>("lon");
    QTest::addColumn<QString>("isoA2");
    QTest::addColumn<int>("level");
    QTest::addColumn<QStringList>("names");
    QTest::addColumn<GeoPolRegion::WorldOpt>("worldOpt");

    QTest::newRow("US.NV") << 37.30 << -115.2 << "US.NV" << 1 << QStringList({ "United States", "Nevada" })
                           << GeoPolRegion::WorldOpt::IncludeIfNoOther;
    QTest::newRow("CA.MB") << 51.50 << -100.2 << "CA.MB" << 1 << QStringList({ "Canada", "Manitoba" })
                           << GeoPolRegion::WorldOpt::IncludeIfNoOther;
    QTest::newRow("FR.ND") << 49.10 << -1.5 << "FR.ND" << 1 << QStringList({ "France", "Normandie" })
                           << GeoPolRegion::WorldOpt::IncludeIfNoOther;
    QTest::newRow("World.FR.ND") << 49.10 << -1.5 << "FR.ND" << 1 << QStringList({ "World", "France", "Normandie" })
                           << GeoPolRegion::WorldOpt::IncludeAlways;
    QTest::newRow("MN")    << 45.0 << 105.5 << "MN" << 0 << QStringList({ "Mongolia" })
                           << GeoPolRegion::WorldOpt::IncludeIfNoOther;
    QTest::newRow("NZ")    << -43.0 << 171.0 << "NZ" << 0 << QStringList({ "New Zealand" })
                           << GeoPolRegion::WorldOpt::IncludeIfNoOther;
    QTest::newRow("World") << -34.0 << 173.0 << "" << -1 << QStringList({ "World" })
                           << GeoPolRegion::WorldOpt::IncludeIfNoOther;

    QTest::newRow("World.US.NV") << 37.30 << -115.2 << "US.NV" << 1 << QStringList({ "World", "United States", "Nevada" })
                           << GeoPolRegion::WorldOpt::IncludeIfOther;
}

template <typename T> const GeoPolRegion* intersection(const T& pos, GeoPolRegion::WorldOpt worldOpt) {
    return app().geoPolMgr().intersection(pos, worldOpt);
}

void TestZtgps::geopol()
{
    QVERIFY(app().geoPolMgr().finishLoad());

    QFETCH(double, lat);
    QFETCH(double, lon);
    QFETCH(QString, isoA2);
    QFETCH(int, level);
    QFETCH(QStringList, names);
    QFETCH(GeoPolRegion::WorldOpt, worldOpt);

    QCOMPARE(app().geoPolMgr().intersectionNames(GeoLocEntry(lat, lon), worldOpt), names);
    QCOMPARE(intersection(GeoLocEntry(lat, lon), worldOpt)->isoA2(), isoA2);
    QCOMPARE(intersection(GeoLocEntry(lat, lon), worldOpt)->level(), level);
    QCOMPARE(intersection(Marble::GeoDataCoordinates(lon, lat, 0.0, Marble::GeoDataCoordinates::Degree), worldOpt)->isoA2(), isoA2);
    QCOMPARE(intersection(ViewParams(lat, lon, 0), worldOpt)->isoA2(), isoA2);
}

void TestZtgps::geopol_benchmark()
{
    QBENCHMARK {
        (void)intersection(GeoLocEntry(37.30, -115.2), GeoPolRegion::WorldOpt::IncludeIfNoOther)->isoA2();
    }
}

void TestZtgps::geoloc_data()
{
    QTest::addColumn<QString>("flags");
    QTest::addColumn<GeoLocEntry::Feature>("type");
    QTest::addColumn<double>("centerLat");
    QTest::addColumn<double>("centerLon");
    QTest::addColumn<double>("minDistKm");
    QTest::addColumn<double>("maxDistKm");

    QTest::addRow("fairbaNKS")
            << "World/United States/Alaska"
            << GeoLocEntry::Feature::City
            << 64.5  << -147.5 << 0.0 << 100.0;
    QTest::addRow("Strasbourg")
            << "World/France/Grand Est"
            << GeoLocEntry::Feature::City
            << 48.5  <<    7.5 << 0.0 << 100.0;
    QTest::addRow("Mount Rainier")
            << "World/United States/Washington"
            << GeoLocEntry::Feature::Mountain
            << 46.5  << -121.0 << 0.0 << 100.0;
    QTest::addRow("Yellowstone National Park")
            << "World/United States/Wyoming"
            << GeoLocEntry::Feature::Park
            << 44.5  << -110.0 << 0.0 << 100.0;
    QTest::addRow("Australia")
            << "World/Australia/Northern Territory"
            << GeoLocEntry::Feature::Region
            << -25.0 <<  135.0 << 0.0 << 100.0;
}

QString TestZtgps::testGeoLoc()
{
    QFETCH(QString, flags);
    QFETCH(GeoLocEntry::Feature, type);
    QFETCH(double, centerLat);
    QFETCH(double, centerLon);
    QFETCH(double, minDistKm);
    QFETCH(double, maxDistKm);

    const Marble::GeoDataCoordinates center(centerLon, centerLat, 0.0, Marble::GeoDataCoordinates::Degree);
    app().geoLocModel().setCenter(center);
    GeoLocFilter geoLocFilter(app().geoLocModel());

//    geoLocFilter.update(gpr, center, 16000.0, Marble::GeoDataLatLonBox(), GeoLocFilter::filterMaskAll);

    const QString name(QTest::currentDataTag());

    const QModelIndex idx    = geoLocFilter.mapToSource(geoLocFilter.findClosest(name));

    const QString     modelName  = app().geoLocModel().data(GeoLocModel::Name,  idx, Util::RawDataRole).toString();
    const QStringList modelFlags = app().geoLocModel().data(GeoLocModel::Flags, idx, Util::RawDataRole).toStringList();
    const auto modelType  =
            app().geoLocModel().data(GeoLocModel::Type,  idx, Util::RawDataRole).value<GeoLocEntry::Feature>();
    const double      modelDistKm = app().geoLocModel().data(GeoLocModel::Dist,  idx, Util::RawDataRole).toDouble() / 1000.0;

    if (name.compare(modelName, Qt::CaseInsensitive) != 0)
        return QString("Name mismatch: Expected %1, Found %2").arg(name, modelName);

    if (type != modelType)
        return QString("Type mismatch: Expected %1, Found %2")
                .arg(GeoLocEntry::featureName(type),
                     GeoLocEntry::featureName(modelType));

    if (flags.compare(modelFlags.back(), Qt::CaseInsensitive) != 0)
        return QString("Flags mismatch: Expected %1, Found %2").arg(flags, modelFlags.back());

    if (modelDistKm < minDistKm || modelDistKm > maxDistKm)
        return QString("Bad distance: found %1: Range [%2-%3]").arg(modelDistKm, minDistKm, maxDistKm);

    return RcOK;
}

void TestZtgps::geoloc()
{
    QVERIFY(app().geoLocModel().finishLoad()); // make sure this is finished.
    QVERIFY(app().geoPolMgr().finishLoad());

    QCOMPARE(testGeoLoc(), QString(RcOK));
}

void TestZtgps::geoloc_benchmark()
{
    QVERIFY(app().geoLocModel().finishLoad()); // make sure this is finished.
    QVERIFY(app().geoPolMgr().finishLoad());

    GeoLocFilter geoLocFilter(app().geoLocModel());

    QBENCHMARK {
        const QModelIndex idx = geoLocFilter.mapToSource(geoLocFilter.findClosest("Boston"));
        app().geoLocModel().data(GeoLocModel::Name, idx);
    }
}

void TestZtgps::model_var_expand_data()
{
    QTest::addColumn<int>("row");
    QTest::addColumn<double>("length");
    QTest::addColumn<QString>("srcHtml");
    QTest::addColumn<QRegularExpression>("successRegex");

    const int testRow = 2;

    QTest::addRow("expand_name")
            << testRow << -1.0
            << QString("<p>Name: ${Name}</p>")
            << QRegularExpression(R"REGEX(<p[^>]*>Name: Mystic River Basin Trails: LONG TRACK</p>.*</body></html>)REGEX");

    QTest::addRow("expand_name_and_length")
            << testRow << -1.0
            << QString("<p>Name: ${Name} = ${length}</p>")
            << QRegularExpression(R"REGEX(<p[^>]*>Name: Mystic River Basin Trails: LONG TRACK = 7\.82 Km</p>.*</body></html>)REGEX");

    QTest::addRow("unclosed_var")
            << testRow << -1.0
            << QString("<p>Name: ${Name = ${length}</p>")
            << QRegularExpression(R"REGEX(<p[^>]*>Name: \$\{Name = 7\.82 Km</p>.*</body></html>)REGEX");

    QTest::addRow("bad_column")
            << testRow << -1.0
            << QString("<p>Name: ${Blee} = ${length}</p>")
            << QRegularExpression(R"REGEX(<p[^>]*>Name: <b>N/A</b> = 7\.82 Km</p>.*</body></html>)REGEX");

    QTest::addRow("colorize_long")
            << testRow << 100.0 * 1000.0
            << QString("<p>${Max_grade} = ${length}</p>")
            << QRegularExpression(R"REGEX(<p[^>]*><span style="color:#00[fb][fb][a7][a7];">3 %</span> = <span style="color:#[fc][fc]0000;">100.00 Km</span></p>.*</body></html>)REGEX");

    QTest::addRow("notes_image")
            << testRow << -1.0
            << QString("<p>${Notes}</p>")
            << QRegularExpression(R"REGEX(<p[^>]*><img[\s]+height="[\d]+"[\s]+src="file::icons/hicolor/actions/[\d]+/note.svg"/></p>)REGEX");

    QTest::addRow("flags")
            << testRow << -1.0
            << QString("<p>${Flags}</p>")
            << QRegularExpression(R"REGEX(<p[^>]*><img[\s]+height="[\d]+"[\s]+src="file::art/tags/Flags/Countries/United_States\.jpg"/>\&nbsp;<img[\s]+height="[\d]+"[\s]+src="file::art/tags/Flags/Regions/United_States/Massachusetts\.jpg"/></p>)REGEX");

    QTest::addRow("tags")
            << testRow << -1.0
            << QString("<p>${Tags}</p>")
            << QRegularExpression(R"REGEX(<p[^>]*><img[\s]+height="[\d]+"[\s]+src="[^"]+"/>\&nbsp;<img[\s]+height="[\d]+"[\s]+src="[^"]+"/></p>)REGEX");
}

// ModelTextEditDialog is from ldutils: we test here to have access to a TrackModel
void TestZtgps::model_var_expand()
{
    cfgDataWritable().reset(); // so we get colorization

    QFETCH(int, row);
    QFETCH(double, length);
    QFETCH(QString, srcHtml);
    QFETCH(QRegularExpression, successRegex);

    Models models;
    GeoLoad loader(nullptr, models.m_trkModel, models.m_wptModel);

    const QString mysticGpx    = QFINDTESTDATA(DATAROOT "gps/mystic_basin_trail.gpx");
    QVERIFY(loader.load(mysticGpx));

    // Apply tags so we can test tag icon functionality. Must use default ones here.
    models.m_trkModel.setData(models.m_trkModel.index(2, TrackModel::Tags), QStringList({"Road", "Recreation"}),
                              Util::RawDataRole);

    const QModelIndex rowIdx = models.m_trkModel.index(row, 0);

    if (length >= 0.0)
        models.m_trkModel.setData(rowIdx.sibling(row, TrackModel::Length), length, Util::RawDataRole);

    ModelTextEditDialog editor(models.m_trkModel, true);
    const ModelVarExpander expander;

    editor.setHtml(srcHtml);

    const QString expandedHtml = expander.expandHtml(editor.toHtml(), rowIdx);

    const bool hasMatch = successRegex.match(expandedHtml).hasMatch();

    QVERIFY2(hasMatch, qUtf8Printable(expandedHtml));
}

void TestZtgps::model_text_edit_data()
{
    QTest::addColumn<int>("column");
    QTest::addColumn<QRegularExpression>("successRegex");

    QTest::addRow("Name")
            << int(TrackModel::Name)
            << QRegularExpression(R"REGEX(\$\{Name\})REGEX");

    QTest::addRow("Min_Cad")
            << int(TrackModel::MinCad)
            << QRegularExpression(R"REGEX(\$\{Min_Cad\})REGEX");

    QTest::addRow("Avg_Grade")
            << int(TrackModel::AvgGrade)
            << QRegularExpression(R"REGEX(\$\{Avg_Grade\})REGEX");
}

void TestZtgps::model_text_edit()
{
    QFETCH(int, column);
    QFETCH(QRegularExpression, successRegex);

    Models models;
    ModelTextEditDialog editor(models.m_trkModel, true);

    auto* btnInsert = editor.findChild<QToolButton*>("btnInsert");
    QVERIFY(btnInsert);

    auto* modelColumns = editor.findChild<QTreeView*>("modelColumns");
    QVERIFY(modelColumns);

    auto* columnModel = qobject_cast<QStandardItemModel*>(modelColumns->model());
    QVERIFY(columnModel);

    modelColumns->selectionModel()->setCurrentIndex(columnModel->index(column, 0),
                                                    QItemSelectionModel::SelectCurrent);

    btnInsert->click();

    const bool hasMatch = successRegex.match(editor.toHtml()).hasMatch();
    QVERIFY2(hasMatch, qUtf8Printable(editor.toHtml()));
}

void TestZtgps::save_formats_data()
{
    QTest::addColumn<GeoFormat>("format");
    QTest::addColumn<QString>("filename");

    QTest::newRow("GPX") << GeoFormat::Gpx << "save_formats-1.gpx";
    QTest::newRow("KML") << GeoFormat::Gpx << "save_formats-1.kml";
    QTest::newRow("TCX") << GeoFormat::Gpx << "save_formats-1.tcx";
    QTest::newRow("FIT") << GeoFormat::Gpx << "save_formats-1.fit";
}

void TestZtgps::save_formats()
{
    QFETCH(GeoFormat, format);
    QFETCH(QString,   filename);

    Models models;
    GeoLoad loader(nullptr, models.m_trkModel, models.m_wptModel);

    const QString mysticGpx = QFINDTESTDATA(DATAROOT "gps/mystic_basin_trail.gpx");
    QVERIFY(loader.load(mysticGpx));

    // Test saving as gpx
    {
        const QString gpx_1 = m_tmpDir.filePath(filename);
        GeoSave saver(nullptr, models.m_trkModel, models.m_wptModel, GeoSaveParams(format));

        QVERIFY(saver.save(gpx_1));

        Models models_reloaded;
        GeoLoad loader(nullptr, models_reloaded.m_trkModel, models_reloaded.m_wptModel);
        QVERIFY(loader.load(gpx_1));

        QVERIFY(models.m_trkModel.rowCount() == models_reloaded.m_trkModel.rowCount());

        for (int r = 0; r < models.m_trkModel.rowCount(); ++r) {
            QVERIFY(models.m_trkModel.isDuplicate(models.m_trkModel.index(r, 0),
                                                   models_reloaded.m_trkModel.index(r, 0)));
        }

        for (int r = 0; r < models.m_wptModel.rowCount(); ++r) {
            QVERIFY(models.m_wptModel.isDuplicate(models.m_wptModel.index(r, 0),
                                                   models_reloaded.m_wptModel.index(r, 0)));
        }
    }
}

void TestZtgps::route_point_aux()
{
    Models  models;
    GeoLoad loader(nullptr, models.m_trkModel, models.m_wptModel,
                   GeoLoadParams(GeoIoFeature::AllTypes | GeoIoFeature::AuxRte));

    const QString mysticGpx    = QFINDTESTDATA(DATAROOT "gps/mystic_basin_trail.gpx");

    QVERIFY(loader.load(mysticGpx));
    QVERIFY(models.m_trkModel.rowCount() == 5);

    PointModel* rtePm = models.geoPoints(models.m_trkModel.index(0, 0));
    PointModel* trkPm = models.geoPoints(models.m_trkModel.index(2, 0));
    QVERIFY(rtePm != nullptr);
    QVERIFY(trkPm != nullptr);

    const auto testAux = [](const PointModel* pm, bool wantAll, bool wantAny) {
        bool auxDataAll = true;
        bool auxDataAny = false;
        Util::Recurse(*pm, [&auxDataAll, &auxDataAny, pm](const QModelIndex& idx) {
            if (const PointItem* pt = pm->getItem(idx); pt != nullptr) {
                auxDataAny |= pt->hasAuxData();
                auxDataAll &= pt->hasAuxData();
            }
            return true;
        });

        QCOMPARE(auxDataAll, wantAll);
        QCOMPARE(auxDataAny, wantAny);
    };

    // Test aux data after import
    testAux(rtePm, true,  true);
    testAux(trkPm, false, false);

    // Set some aux data, then retest
    trkPm->setData(PointModel::Name, trkPm->index(0, 0, trkPm->index(0,0)), "NameTest", Util::RawDataRole);
    trkPm->setData(PointModel::Desc, trkPm->index(0, 0, trkPm->index(0,0)), "DescTest", Util::RawDataRole);
    testAux(trkPm, false, true);

    // Reset Name: should still have auxdata
    trkPm->setData(PointModel::Name, trkPm->index(0, 0, trkPm->index(0,0)), "", Util::RawDataRole);
    testAux(trkPm, false, true);

    // Reset Desc: aux data should be gone from track
    trkPm->setData(PointModel::Desc, trkPm->index(0, 0, trkPm->index(0,0)), "", Util::RawDataRole);
    testAux(trkPm, false, false);
}

void TestZtgps::import_dialog_data()
{
    createMainWindowPrivate(m_mainWindow_private);

    QTest::addColumn<QString>("rteTag");
    QTest::addColumn<QList<int>>("tagsToSelect");
    QTest::addColumn<bool>("importRoutes");
    QTest::addColumn<bool>("importTracks");
    QTest::addColumn<bool>("deduplicate");
    QTest::addColumn<bool>("applyRouteTag");
    QTest::addColumn<bool>("overrideTagColors");

    QTest::newRow("trk-rte-dedupe")
            << "Route"
            << QList<int>()
            << true   // import routes
            << true   // import tracks
            << true   // deduplicate
            << true   // apply route tag
            << false; // override tag colors

    QTest::newRow("trk-rte-dedupe-no-rte-tag")
            << "Route"
            << QList<int>()
            << true   // import routes
            << true   // import tracks
            << true   // deduplicate
            << false  // apply route tag
            << false; // override tag colors

    QTest::newRow("trk-rte-dedupe-3tags")
            << "Route"
            << QList<int>({0, 1, 2})
            << true   // import routes
            << true   // import tracks
            << true   // deduplicate
            << true   // apply route tag
            << false; // override tag colors

    QTest::newRow("trk-rte-dedupe-custom-color")
            << "Route"
            << QList<int>({0, 1})
            << true   // import routes
            << true   // import tracks
            << true   // deduplicate
            << true   // apply route tag
            << true;  // override tag colors

    QTest::newRow("trk-rte")
            << "Route"
            << QList<int>()
            << true   // import routes
            << true   // import tracks
            << false  // deduplicate
            << true   // apply route tag
            << false; // override tag colors

    QTest::newRow("trk-dedupe")
            << "Route"
            << QList<int>()
            << false  // import routes
            << true   // import tracks
            << true   // deduplicate
            << true   // apply route tag
            << false; // override tag colors

    QTest::newRow("trk")
            << "Route"
            << QList<int>()
            << false  // import routes
            << true   // import tracks
            << false  // deduplicate
            << true   // apply route tag
            << false; // override tag colors

    QTest::newRow("trk-no-rte-tag")
            << "Route"
            << QList<int>()
            << false  // import routes
            << true   // import tracks
            << false  // deduplicate
            << false  // apply route tag
            << false; // override tag colors

    QTest::newRow("rte-dedupe")
            << "Route"
            << QList<int>()
            << true   // import routes
            << false  // import tracks
            << true   // deduplicate
            << true   // apply route tag
            << false; // override tag colors

    QTest::newRow("rte")
            << "Route"
            << QList<int>()
            << true   // import routes
            << false  // import tracks
            << false  // deduplicate
            << true   // apply route tag
            << false; // override tag colors

    QTest::newRow("rte-no-rte-tag")
            << "Route"
            << QList<int>()
            << true   // import routes
            << false  // import tracks
            << false  // deduplicate
            << false  // apply route tag
            << false; // override tag colors

    QTest::newRow("rte-dedupe-no-rte-tag")
            << "Route"
            << QList<int>()
            << true   // import routes
            << false  // import tracks
            << true   // deduplicate
            << false  // apply route tag
            << false; // override tag colors
}

void TestZtgps::import_dialog()
{
    QFETCH(QString, rteTag);
    QFETCH(QList<int>, tagsToSelect);
    QFETCH(bool, importRoutes);
    QFETCH(bool, importTracks);
    QFETCH(bool, deduplicate);
    QFETCH(bool, applyRouteTag);
    QFETCH(bool, overrideTagColors);

    const QString mysticGpx = QFINDTESTDATA(DATAROOT "gps/mystic_basin_trail.gpx");
    const QColor customTrkColor = QColor(0xffa33b74);

    app().newSession();
    cfgDataWritable().setupDefaultConfig();

    // Find action to show import dialog
    auto* importTrack = m_mainWindow_private->findChild<QAction*>("action_Import_Track");
    QVERIFY(importTrack != nullptr);

    // It's difficult to directly use the import action in this test suite because it shows
    // a modal file selector.  Instead, we'll exercise the dialog directly.
    ImportDialog& importDialog = m_mainWindow_private->m_importOpts;

    auto* btnTrackColor        = importDialog.findChild<QToolButton*>("trackColor");
    auto* btnRouteTag          = importDialog.findChild<QToolButton*>("routeTag");
    auto* btnDeduplicate       = importDialog.findChild<QCheckBox*>("deduplicate");
    auto* btnOverrideTagColors = importDialog.findChild<QCheckBox*>("overrideTagColors");
    auto* btnRte               = importDialog.findChild<QCheckBox*>("btnRte");
    auto* btnTrk               = importDialog.findChild<QCheckBox*>("btnTrk");
    auto* btnWpt               = importDialog.findChild<QCheckBox*>("btnWpt");
    auto* btnApplyRouteTag     = importDialog.findChild<QCheckBox*>("applyRouteTag");
    auto* availableTags        = importDialog.findChild<QTreeView*>("availableTags");
    auto* activeTags           = importDialog.findChild<QTreeView*>("activeTags");
    auto* tagLeft              = importDialog.findChild<QToolButton*>("tagLeft");
    auto* tagSelector          = importDialog.findChild<TagSelector*>();

    QVERIFY(btnTrackColor        != nullptr);
    QVERIFY(btnRouteTag          != nullptr);
    QVERIFY(btnDeduplicate       != nullptr);
    QVERIFY(btnOverrideTagColors != nullptr);
    QVERIFY(btnRte               != nullptr);
    QVERIFY(btnTrk               != nullptr);
    QVERIFY(btnWpt               != nullptr);
    QVERIFY(btnApplyRouteTag     != nullptr);
    QVERIFY(availableTags        != nullptr);
    QVERIFY(activeTags           != nullptr);
    QVERIFY(tagLeft              != nullptr);
    QVERIFY(tagSelector          != nullptr);

    QCOMPARE(undoMgr().undoCount(), 0);

    {
        // Setup dialog box params
        importDialog.setRouteTag(rteTag);
        btnRte->setChecked(importRoutes);
        btnTrk->setChecked(importTracks);
        btnDeduplicate->setChecked(deduplicate);
        btnApplyRouteTag->setChecked(applyRouteTag);
        btnOverrideTagColors->setChecked(overrideTagColors);
        tagSelector->clearTags();

        if (overrideTagColors)
            Util::SetTBColor(btnTrackColor, customTrkColor);

        QStringList trkTagList;

        // Select track tag(s) to apply
        const QModelIndex item_0    = availableTags->model()->index(0, 0);
        for (auto row : tagsToSelect) {
            const QModelIndex tagIdx = availableTags->model()->index(row, 0, item_0);
            const QString tagName = availableTags->model()->data(tagIdx).toString();

            trkTagList.append(tagName);
            availableTags->selectionModel()->select(tagIdx, QItemSelectionModel::Select | QItemSelectionModel::Rows);
        }

        QTest::mouseClick(tagLeft, Qt::LeftButton); // move the tags to the active set

        // Calculate expected route and track tags that we should see post-import.
        const QStringList rteTagList =
                (applyRouteTag ? QStringList(rteTag) : QStringList()) + trkTagList;

        importDialog.show();
        m_mainWindow_private->importTracks(ImportInfoList({{"File", mysticGpx}}), false, false);
        importDialog.hide();
        QCOMPARE(undoMgr().undoCount(), 1);

        int rteCount = 0;
        int trkCount = 0;
        for (int row = 0; row < app().trackModel().rowCount(); ++row) {
            // Test tags
            const QStringList tags = app().trackModel().index(row, TrackModel::Tags).data(Util::RawDataRole).toStringList();
            if (app().trackModel().index(row, TrackModel::Type).data(Util::RawDataRole).value<TrackType>() == TrackType::Rte) {
                QCOMPARE(tags, rteTagList);
                ++rteCount;
            } else {
                ++trkCount;
                QCOMPARE(tags, trkTagList);
            }

            // Test color
            const QColor color = app().trackModel().index(row, TrackModel::Color).data(Util::RawDataRole).value<QColor>();
            if (overrideTagColors) {
                QVERIFY(color == customTrkColor);
            } else {
                QVERIFY(color != customTrkColor);
            }
        }

        const int expectedRoutes = importRoutes ? 2 : 0;
        const int expectedTracks = importTracks ? 3 : 0;
        QCOMPARE(rteCount, expectedRoutes);
        QCOMPARE(trkCount, expectedTracks);

        // Test deduplication
        importDialog.show();
        m_mainWindow_private->importTracks(ImportInfoList({{"File", mysticGpx}}), false, false);
        importDialog.hide();

        QCOMPARE(app().trackModel().rowCount(), (expectedRoutes + expectedTracks) * (deduplicate ? 1 : 2));

        while (undoMgr().hasUndos())
            QVERIFY(undoMgr().undo());
        QCOMPARE(app().trackModel().rowCount(), 0);
    }
}

Q_DECLARE_METATYPE(GeoIoFeature)

void TestZtgps::export_dialog_data()
{
    createMainWindowPrivate(m_mainWindow_private);
    app().newSession();
    cfgDataWritable().setupDefaultConfig();

    QTest::addColumn<QString>("exportFormat");
    QTest::addColumn<bool>("writeFormatted");
    QTest::addColumn<bool>("writeZtgpsExtensions");
    QTest::addColumn<bool>("writeSpaces");
    QTest::addColumn<int>("indentLevel");
    QTest::addColumn<bool>("writeAllTrk");
    QTest::addColumn<bool>("writeAllRte");
    QTest::addColumn<bool>("writeAllWpt");
    QTest::addColumn<bool>("writeNoTrk");
    QTest::addColumn<bool>("writeNoRte");
    QTest::addColumn<bool>("writeNoWpt");
    QTest::addColumn<bool>("writeSelectedTrk");
    QTest::addColumn<bool>("writeSelectedRte");
    QTest::addColumn<bool>("writeSelectedWpt");
    QTest::addColumn<GeoFormat>("expectedExportFormat");
    QTest::addColumn<GeoIoFeature>("expectedFeatures");

    QTest::newRow("gpx-indent1-ztgps-all")
            << "GPX" // exportFormat
            << true  // writeFormatted
            << true  // writeZtgpsExtensions
            << true  // writeSpaces
            << 1     // indentLevel
            << true  // writeAllTrk
            << true  // writeAllRte
            << true  // writeAllWpt
            << false // writeNoTrk
            << false // writeNoRte
            << false // writeNoWpt
            << false // writeSelectedTrk
            << false // writeSelectedRte
            << false // writeSelectedWpt
            << GeoFormat::Gpx           // exportFormat
            << (GeoIoFeature::AllTypes | GeoIoFeature::ExtZtgps | GeoIoFeature::Formatted |
                GeoIoFeature::Spaces | GeoIoFeature::AllData)
               ;

    QTest::newRow("gpx-indent1-all")
            << "GPX" // exportFormat
            << true  // writeFormatted
            << false // writeZtgpsExtensions
            << true  // writeSpaces
            << 1     // indentLevel
            << true  // writeAllTrk
            << true  // writeAllRte
            << true  // writeAllWpt
            << false // writeNoTrk
            << false // writeNoRte
            << false // writeNoWpt
            << false // writeSelectedTrk
            << false // writeSelectedRte
            << false // writeSelectedWpt
            << GeoFormat::Gpx           // exportFormat
            << (GeoIoFeature::AllTypes | GeoIoFeature::Formatted | GeoIoFeature::Spaces |
                GeoIoFeature::AllData)
               ;

    QTest::newRow("gpx-nofmt-all")
            << "GPX" // exportFormat
            << false // writeFormatted
            << false // writeZtgpsExtensions
            << true  // writeSpaces
            << 1     // indentLevel
            << true  // writeAllTrk
            << true  // writeAllRte
            << true  // writeAllWpt
            << false // writeNoTrk
            << false // writeNoRte
            << false // writeNoWpt
            << false // writeSelectedTrk
            << false // writeSelectedRte
            << false // writeSelectedWpt
            << GeoFormat::Gpx           // exportFormat
            << (GeoIoFeature::AllTypes | GeoIoFeature::Spaces | GeoIoFeature::AllData)
               ;

    QTest::newRow("gpx-tabs-all")
            << "GPX" // exportFormat
            << true  // writeFormatted
            << false // writeZtgpsExtensions
            << false // writeSpaces
            << 3     // indentLevel
            << true  // writeAllTrk
            << true  // writeAllRte
            << true  // writeAllWpt
            << false // writeNoTrk
            << false // writeNoRte
            << false // writeNoWpt
            << false // writeSelectedTrk
            << false // writeSelectedRte
            << false // writeSelectedWpt
            << GeoFormat::Gpx           // exportFormat
            << (GeoIoFeature::AllTypes | GeoIoFeature::Formatted | GeoIoFeature::AllData)
               ;

    QTest::newRow("gpx-tabs-trk")
            << "GPX" // exportFormat
            << true  // writeFormatted
            << false // writeZtgpsExtensions
            << false // writeSpaces
            << 1     // indentLevel
            << true  // writeAllTrk
            << false // writeAllRte
            << false // writeAllWpt
            << false // writeNoTrk
            << true  // writeNoRte
            << true  // writeNoWpt
            << false // writeSelectedTrk
            << false // writeSelectedRte
            << false // writeSelectedWpt
            << GeoFormat::Gpx           // exportFormat
            << (GeoIoFeature::Trk | GeoIoFeature::Formatted | GeoIoFeature::AllTrk)
               ;

    QTest::newRow("gpx-tabs-rte")
            << "GPX" // exportFormat
            << true  // writeFormatted
            << false // writeZtgpsExtensions
            << false // writeSpaces
            << 1     // indentLevel
            << false // writeAllTrk
            << true  // writeAllRte
            << false // writeAllWpt
            << true  // writeNoTrk
            << false // writeNoRte
            << true  // writeNoWpt
            << false // writeSelectedTrk
            << false // writeSelectedRte
            << false // writeSelectedWpt
            << GeoFormat::Gpx           // exportFormat
            << (GeoIoFeature::Rte | GeoIoFeature::Formatted | GeoIoFeature::AllRte)
               ;

    QTest::newRow("gpx-tabs-wpt")
            << "GPX" // exportFormat
            << true  // writeFormatted
            << false // writeZtgpsExtensions
            << false // writeSpaces
            << 1     // indentLevel
            << false // writeAllTrk
            << false // writeAllRte
            << true  // writeAllWpt
            << true  // writeNoTrk
            << true  // writeNoRte
            << false // writeNoWpt
            << false // writeSelectedTrk
            << false // writeSelectedRte
            << false // writeSelectedWpt
            << GeoFormat::Gpx           // exportFormat
            << (GeoIoFeature::Wpt | GeoIoFeature::Formatted | GeoIoFeature::AllWpt)
               ;

    QTest::newRow("gpx-tabs-trkwpt")
            << "GPX" // exportFormat
            << true  // writeFormatted
            << false // writeZtgpsExtensions
            << false // writeSpaces
            << 1     // indentLevel
            << true  // writeAllTrk
            << false // writeAllRte
            << true  // writeAllWpt
            << false // writeNoTrk
            << true  // writeNoRte
            << false // writeNoWpt
            << false // writeSelectedTrk
            << false // writeSelectedRte
            << false // writeSelectedWpt
            << GeoFormat::Gpx           // exportFormat
            << (GeoIoFeature::Trk | GeoIoFeature::Wpt | GeoIoFeature::Formatted |
                GeoIoFeature::AllTrk | GeoIoFeature::AllWpt)
               ;

    QTest::newRow("kml-tabs-all")
            << "KML" // exportFormat
            << true  // writeFormatted
            << false // writeZtgpsExtensions
            << false // writeSpaces
            << 2     // indentLevel
            << true  // writeAllTrk
            << true  // writeAllRte
            << true  // writeAllWpt
            << false // writeNoTrk
            << false // writeNoRte
            << false // writeNoWpt
            << false // writeSelectedTrk
            << false // writeSelectedRte
            << false // writeSelectedWpt
            << GeoFormat::Kml           // exportFormat
            << (GeoIoFeature::AllTypes | GeoIoFeature::Formatted | GeoIoFeature::AllData)
               ;

    QTest::newRow("fit-tabs-all")
            << "FIT" // exportFormat
            << true  // writeFormatted
            << false // writeZtgpsExtensions
            << false // writeSpaces
            << 2     // indentLevel
            << true  // writeAllTrk
            << true  // writeAllRte
            << true  // writeAllWpt
            << false // writeNoTrk
            << false // writeNoRte
            << false // writeNoWpt
            << false // writeSelectedTrk
            << false // writeSelectedRte
            << false // writeSelectedWpt
            << GeoFormat::Fit           // exportFormat
            << (GeoIoFeature::AllTypes | GeoIoFeature::Formatted | GeoIoFeature::AllData)
               ;

    QTest::newRow("tcx-tabs-all")
            << "TCX" // exportFormat
            << true  // writeFormatted
            << false // writeZtgpsExtensions
            << false // writeSpaces
            << 2     // indentLevel
            << true  // writeAllTrk
            << true  // writeAllRte
            << true  // writeAllWpt
            << false // writeNoTrk
            << false // writeNoRte
            << false // writeNoWpt
            << false // writeSelectedTrk
            << false // writeSelectedRte
            << false // writeSelectedWpt
            << GeoFormat::Tcx          // exportFormat
            << (GeoIoFeature::AllTypes | GeoIoFeature::Formatted | GeoIoFeature::AllData)
               ;
}

void TestZtgps::export_dialog()
{
    QFETCH(QString, exportFormat);
    QFETCH(bool, writeFormatted);
    QFETCH(bool, writeZtgpsExtensions);
    QFETCH(bool, writeSpaces);
    QFETCH(int,  indentLevel);
    QFETCH(bool, writeAllTrk);
    QFETCH(bool, writeAllRte);
    QFETCH(bool, writeAllWpt);
    QFETCH(bool, writeNoTrk);
    QFETCH(bool, writeNoRte);
    QFETCH(bool, writeNoWpt);
    QFETCH(bool, writeSelectedTrk);
    QFETCH(bool, writeSelectedRte);
    QFETCH(bool, writeSelectedWpt);
    QFETCH(GeoFormat, expectedExportFormat);
    QFETCH(GeoIoFeature, expectedFeatures);

    // Find action to show import dialog
    auto* exportTrack = m_mainWindow_private->findChild<QAction*>("action_Export_Track");
    QVERIFY(exportTrack != nullptr);

    // It's difficult to directly use the import action in this test suite because it shows
    // a modal file selector.  Instead, we'll exercise the dialog directly.
    ExportDialog& exportDialog = m_mainWindow_private->m_exportOpts;

    auto* btnExportFormat          = exportDialog.findChild<QComboBox*>("exportFormat");
    auto* btnWriteFormatted        = exportDialog.findChild<QCheckBox*>("writeFormatted");
    auto* btnWriteZtgpsExtensions  = exportDialog.findChild<QCheckBox*>("writeZtgpsExtensions");
    auto* btnWriteSpaces           = exportDialog.findChild<QRadioButton*>("writeSpaces");
    auto* btnWriteTabs             = exportDialog.findChild<QRadioButton*>("writeTabs");
    auto* btnIndentLevel           = exportDialog.findChild<QSpinBox*>("indentLevel");
    auto* btnWriteAllTrk           = exportDialog.findChild<QRadioButton*>("writeAllTrk");
    auto* btnWriteAllRte           = exportDialog.findChild<QRadioButton*>("writeAllRte");
    auto* btnWriteAllWpt           = exportDialog.findChild<QRadioButton*>("writeAllWpt");
    auto* btnWriteNoTrk            = exportDialog.findChild<QRadioButton*>("writeNoTrk");
    auto* btnWriteNoRte            = exportDialog.findChild<QRadioButton*>("writeNoRte");
    auto* btnWriteNoWpt            = exportDialog.findChild<QRadioButton*>("writeNoWpt");
    auto* btnWriteSelectedTrk      = exportDialog.findChild<QRadioButton*>("writeSelectedTrk");
    auto* btnWriteSelectedRte      = exportDialog.findChild<QRadioButton*>("writeSelectedRte");
    auto* btnWriteSelectedWpt      = exportDialog.findChild<QRadioButton*>("writeSelectedWpt");

    QVERIFY(btnExportFormat         != nullptr);
    QVERIFY(btnWriteFormatted       != nullptr);
    QVERIFY(btnWriteZtgpsExtensions != nullptr);
    QVERIFY(btnWriteSpaces          != nullptr);
    QVERIFY(btnWriteTabs            != nullptr);
    QVERIFY(btnIndentLevel          != nullptr);
    QVERIFY(btnWriteAllTrk          != nullptr);
    QVERIFY(btnWriteAllRte          != nullptr);
    QVERIFY(btnWriteAllWpt          != nullptr);
    QVERIFY(btnWriteNoTrk           != nullptr);
    QVERIFY(btnWriteNoRte           != nullptr);
    QVERIFY(btnWriteNoWpt           != nullptr);
    QVERIFY(btnWriteSelectedTrk     != nullptr);
    QVERIFY(btnWriteSelectedRte     != nullptr);
    QVERIFY(btnWriteSelectedWpt     != nullptr);

    QCOMPARE(undoMgr().undoCount(), 0);

    // Setup dialog box params
    btnExportFormat->setCurrentText(exportFormat);
    btnWriteFormatted->setChecked(writeFormatted);
    btnWriteZtgpsExtensions->setChecked(writeZtgpsExtensions);
    btnWriteSpaces->setChecked(writeSpaces);
    btnWriteTabs->setChecked(!writeSpaces);
    btnIndentLevel->setValue(indentLevel);
    btnWriteAllTrk->setChecked(writeAllTrk);
    btnWriteAllRte->setChecked(writeAllRte);
    btnWriteAllWpt->setChecked(writeAllWpt);
    btnWriteNoTrk->setChecked(writeNoTrk);
    btnWriteNoRte->setChecked(writeNoRte);
    btnWriteNoWpt->setChecked(writeNoWpt);
    btnWriteSelectedTrk->setChecked(writeSelectedTrk);
    btnWriteSelectedRte->setChecked(writeSelectedRte);
    btnWriteSelectedWpt->setChecked(writeSelectedWpt);

    exportDialog.show(); // to set enables
    exportDialog.hide();
    const bool isBinary = GeoLoad::isBinary(exportFormat);

    QCOMPARE(btnWriteFormatted->isEnabled(), !isBinary);
    QCOMPARE(btnWriteSpaces->isEnabled(), !isBinary);
    QCOMPARE(btnWriteTabs->isEnabled(), !isBinary);
    QCOMPARE(btnIndentLevel->isEnabled(), !isBinary);

    QCOMPARE(expectedExportFormat,                 exportDialog.exportFormat());
    QCOMPARE(expectedFeatures,                     exportDialog.features());
    QCOMPARE(btnIndentLevel->value(),              exportDialog.indentLevel());
}

void TestZtgps::climb_analysis() const
{
    const GeoLoadParams loadParams(GeoIoFeature::Trk, { "Gravel", "Recreation" });
    const QString climbTestGpx      = QFINDTESTDATA(DATAROOT "gps/ClimbTest.gpx");
    const QString climbTestNoEleGpx = QFINDTESTDATA(DATAROOT "gps/ClimbTest-NoEle.gpx");
    const QString noClimbTestGpx    = QFINDTESTDATA(DATAROOT "gps/mystic_basin_trail.gpx");

    app().newSession();
    createMainWindowPrivate(m_mainWindow_private);

    auto* trackPane = m_mainWindow_private->findPane<TrackPane>();
    QVERIFY(trackPane != nullptr);

    // Add a climb analysis pane
    m_mainWindow_private->addPane(m_mainWindow_private->paneFactory(PaneClass::ClimbAnalysis), nullptr, true, trackPane);

    auto* climbAnalysisPane = m_mainWindow_private->findPane<ClimbAnalysisPane>();
    QVERIFY(climbAnalysisPane != nullptr);
    QCOMPARE(app().climbModel().rowCount(), 0);

    // UI QTreeView component
    auto* climbView = climbAnalysisPane->findChild<QTreeView*>();
    QVERIFY(climbView != nullptr);

    // Import test tracks
    QVERIFY(!GeoLoad::anyFailed(m_mainWindow_private->importTracks(loadParams, { { climbTestGpx }, { noClimbTestGpx }, { climbTestNoEleGpx} })));
    QCOMPARE(app().trackModel().rowCount(), 5); // 1 from ClimbTest, 3 from mystic_basin_trail

    // Find the climb test row
    const QModelIndex climbIdx = app().trackModel().findRow(QModelIndex(), "Climb Analysis Test Track", TrackModel::Name);
    QVERIFY(climbIdx.isValid());

    app().climbModel().deferredUpdate(climbIdx);
    app().climbModel().deferredUpdate();  // force update: normally this happens on a deferred timer

    QCOMPARE(app().climbModel().rowCount(), 8);

    // Verify count displayed in the climb view
    QCOMPARE(climbView->model()->rowCount(), 8);

    // Sanity test the climb analysis
    const QVector<QVector<std::tuple<ModelType, float, float>>> expected = {
        { { ClimbModel::Start,       4312.0f, 1.00f },
          { ClimbModel::Length,      3201.0f, 1.00f },
          { ClimbModel::Vertical,     187.4f, 1.00f },
          { ClimbModel::Steep,         0.12f, 0.05f },
          { ClimbModel::AvgMovSpeed,    2.4f, 0.20f } },

        { { ClimbModel::Start,       7524.0f, 1.00f },
          { ClimbModel::Length,      1719.0f, 1.00f },
          { ClimbModel::Vertical,     -78.0f, 1.00f },
          { ClimbModel::Steep,        -0.09f, 0.05f },
          { ClimbModel::AvgMovSpeed,    7.5f, 0.20f } },

        { { ClimbModel::Start,       9986.0f, 1.00f },
          { ClimbModel::Length,     14806.0f, 1.00f },
          { ClimbModel::Vertical,     488.0f, 1.00f },
          { ClimbModel::Steep,         0.16f, 0.05f } },

        { { ClimbModel::Start,      24847.0f, 1.00f },
          { ClimbModel::Length,      7792.0f, 1.00f },
          { ClimbModel::Vertical,    -263.0f, 1.00f },
          { ClimbModel::Steep,        -0.12f, 0.05f } },

        { { ClimbModel::Start,      32684.0f, 1.00f },
          { ClimbModel::Length,       909.0f, 1.00f },
          { ClimbModel::Vertical,      80.0f, 1.00f },
          { ClimbModel::Steep,         0.11f, 0.05f } },

        { { ClimbModel::Start,      33644.0f, 1.00f },
          { ClimbModel::Length,      5842.0f, 1.00f },
          { ClimbModel::Vertical,    -211.3f, 1.00f },
          { ClimbModel::Steep,        -0.11f, 0.05f } },

        { { ClimbModel::Start,      45706.0f, 1.00f },
          { ClimbModel::Length,      2132.0f, 1.00f },
          { ClimbModel::Vertical,      81.0f, 1.00f },
          { ClimbModel::Steep,         0.09f, 0.05f } },

        { { ClimbModel::Start,      47960.0f, 1.00f },
          { ClimbModel::Length,      4280.0f, 1.00f },
          { ClimbModel::Vertical,    -206.0f, 1.00f },
          { ClimbModel::Steep,        -0.14f, 0.05f } },
    };

    const auto about = [](const float v0, const float ref, const float range) {
        return (ref - range) <= v0 && v0 <= (ref + range);
    };

    QString err;

    // Walk through model, compare values with expected ones
    Util::Recurse(app().climbModel(), [&](const QModelIndex& idx) {
        if (idx.row() < expected.size()) {
            for (const auto& val : expected.at(idx.row())) {
                const float modelVal = idx.sibling(idx.row(), std::get<ModelType>(val)).data(Util::RawDataRole).toFloat();
                if (!about(modelVal, std::get<1>(val), std::get<2>(val)))
                    err = QString("Row: %1 Col: %2 Expected: %3 Actual: %4")
                         .arg(idx.row())
                         .arg(std::get<ModelType>(val))
                         .arg(std::get<1>(val))
                         .arg(modelVal).toUtf8().constData();
            }
        }
        return true;
    });

    QVERIFY2(err.isEmpty(), qUtf8Printable(err));

    // Should find no climbs on this track
    const QModelIndex noClimbIdx = app().trackModel().findRow(QModelIndex(), "Mystic River Basin Trails: LONG TRACK", TrackModel::Name);
    QVERIFY(noClimbIdx.isValid());

    app().climbModel().deferredUpdate(noClimbIdx);
    app().climbModel().deferredUpdate();  // force update: normally this happens on a deferred timer

    QCOMPARE(app().climbModel().rowCount(), 0);
    QCOMPARE(climbView->model()->rowCount(), 0);

    // Test track with no elevations
    const QModelIndex climbNoEleIdx = app().trackModel().findRow(QModelIndex(), "Climb Analysis Test NoEle", TrackModel::Name);
    QVERIFY(climbNoEleIdx.isValid());

    app().climbModel().deferredUpdate(climbNoEleIdx);
    app().climbModel().deferredUpdate();  // force update: normally this happens on a deferred timer

    QCOMPARE(app().climbModel().rowCount(), 0);
    QCOMPARE(climbView->model()->rowCount(), 0);
}

void TestZtgps::zone_analysis() const
{
    // We need this tag for power estimations
    const GeoLoadParams loadParams(GeoIoFeature::AllTypes | GeoIoFeature::AllAux, { "Road", "Recreation" });
    const QString withHrTest = QFINDTESTDATA(DATAROOT "gps/with-hr.gpx");

    app().newSession();
    createMainWindowPrivate(m_mainWindow_private);

    auto* trackPane = m_mainWindow_private->findPane<TrackPane>();
    QVERIFY(trackPane != nullptr);

    // Add a zone analysis pane
    m_mainWindow_private->addPane(m_mainWindow_private->paneFactory(PaneClass::ZoneSummary), nullptr, true, trackPane);

    app().cfgData().setupDefaultConfig(); // default config options

    auto* zonePane = m_mainWindow_private->findPane<ZonePane>();
    QVERIFY(zonePane != nullptr);

    // Import test tracks
    QVERIFY(!GeoLoad::anyFailed(m_mainWindow_private->importTracks(loadParams, { { withHrTest } })));
    QCOMPARE(app().trackModel().rowCount(), 1);

    auto* chartView = zonePane->findChild<QtCharts::QChartView*>();
    QVERIFY(chartView != nullptr);

    auto* methodCombo = zonePane->findChild<QComboBox*>();
    QVERIFY(methodCombo != nullptr);

    QtCharts::QChart* chart = chartView->chart();
    QVERIFY(chart != nullptr);
    QCOMPARE(chart->series().size(), 1);

    auto* pieSeries = qobject_cast<QtCharts::QPieSeries*>(chart->series().at(0));
    QVERIFY(pieSeries != nullptr);

    // Set person data
    PersonModel& people = app().cfgData().people;
    QVERIFY(people.setData(people.index(0, PersonModel::MaxHR), 200, Qt::EditRole));
    QVERIFY(people.setData(people.index(0, PersonModel::FTP), 220, Qt::EditRole));

    // Select the row we've added
    trackPane->select(app().trackModel().index(0,0));
    // Set the person to use
    app().trackModel().setPerson(people.index(0, 0));

    // Sanity test the zone info: presetIdx, methodIdx, lowerPct, upperPct
    struct testData_t {
        int   presetIdx;
        int   methodIdx;

        int   sliceIdx;
        float lowerPct;
        float upperPct;
        int   slices;
    };

    const QVector<testData_t> testData = {
        { 0, 0,   1, 0.12, 0.13, 4 },  // %MaxHR, 3 zone test
        { 0, 0,   2, 0.42, 0.43, 4 },
        { 0, 0,   3, 0.45, 0.46, 4 },

        { 1, 0,   0, 0.00, 0.01, 5 },  // %MaxHR, 5 zone test
        { 1, 0,   1, 0.00, 0.01, 5 },
        { 1, 0,   2, 0.21, 0.23, 5 },
        { 1, 0,   3, 0.39, 0.40, 5 },
        { 1, 0,   4, 0.37, 0.38, 5 },

        { 2, 0,   0, 0.00, 0.01, 5 },  // %MaxHR, 7 zone test
        { 2, 0,   1, 0.00, 0.01, 5 },
        { 2, 0,   2, 0.22, 0.23, 5 },
        { 2, 0,   3, 0.39, 0.40, 5 },
        { 2, 0,   4, 0.37, 0.39, 5 },

        { 1, 1,   0, 0.10, 0.12, 6 },  // %FTP, 5 zone test
        { 1, 1,   1, 0.45, 0.47, 6 },
        { 1, 1,   2, 0.16, 0.18, 6 },
        { 1, 1,   3, 0.07, 0.09, 6 },
        { 1, 1,   4, 0.03, 0.05, 6 },
        { 1, 1,   5, 0.13, 0.15, 6 },
    };

    int prevPreset = -1;
    int prevMethod = -1;

    for (const auto& data : testData) {
        if (prevPreset != data.presetIdx || prevMethod != data.methodIdx) {
            app().cfgData().zones.setPresetModel(data.presetIdx);
            methodCombo->setCurrentIndex(data.methodIdx);

            zonePane->refreshChart(0); // force update, since otherwise it happens on a timer
            prevPreset = data.presetIdx;
            prevMethod = data.methodIdx;
        }

        QCOMPARE(pieSeries->slices().size(), data.slices);

        QVERIFY(pieSeries->slices().at(data.sliceIdx)->percentage() >= data.lowerPct &&
                pieSeries->slices().at(data.sliceIdx)->percentage() <= data.upperPct);

        // Verify tooltip
        zonePane->sliceHovered(pieSeries->slices().at(data.sliceIdx), true);

        QVERIFY(pieSeries->slices().at(data.sliceIdx)->isExploded());

        for (ModelType mt : { TrackModel::TotalTime, TrackModel::Length,
                              TrackModel::AvgMovPower, TrackModel::AvgHR }) {
            QVERIFY2(zonePane->m_chart->toolTip().contains(TrackModel::mdName(mt)),
                     qUtf8Printable(TrackModel::mdName(mt)));
        }

        zonePane->sliceHovered(pieSeries->slices().at(data.sliceIdx), false);

        QVERIFY(zonePane->m_chart->toolTip().isEmpty());
    }
}

void TestZtgps::activity_summary() const
{
    // We need this tag for power estimations
    const GeoLoadParams loadParams1(GeoIoFeature::Trk, { "Road", "Recreation" });
    const GeoLoadParams loadParams2(GeoIoFeature::Trk, { "Gravel", "Recreation" });
    const QString withHrTest = QFINDTESTDATA(DATAROOT "gps/with-hr.gpx");
    const QString mysticGpx = QFINDTESTDATA(DATAROOT "gps/mystic_basin_trail.gpx");

    app().newSession();
    createMainWindowPrivate(m_mainWindow_private);

    auto* trackPane = m_mainWindow_private->findPane<TrackPane>();
    QVERIFY(trackPane != nullptr);

    // Add an activity summary analysis pane
    m_mainWindow_private->addPane(m_mainWindow_private->paneFactory(PaneClass::ActivitySummary), nullptr, true, trackPane);

    app().cfgData().setupDefaultConfig(); // default config options

    auto* activityPane = m_mainWindow_private->findPane<ActivitySummaryPane>();
    QVERIFY(activityPane != nullptr);

    // Import test tracks
    QVERIFY(!GeoLoad::anyFailed(m_mainWindow_private->importTracks(loadParams1, { { withHrTest } })));
    QCOMPARE(app().trackModel().rowCount(), 1);

    QVERIFY(!GeoLoad::anyFailed(m_mainWindow_private->importTracks(loadParams2, { { mysticGpx } })));
    QCOMPARE(app().trackModel().rowCount(), 4);

    // Set an alternate tag for one of the mystic basin tracks, for test purposes.
    const QModelIndex tagIdxHike = app().trackModel().index(2, TrackModel::Tags);
    app().trackModel().setData(tagIdxHike, "Hike", Util::RawDataRole);

    auto* chartView = activityPane->findChild<QtCharts::QChartView*>();
    QVERIFY(chartView != nullptr);

    QtCharts::QChart* chart = chartView->chart();
    QVERIFY(chart != nullptr);

    auto* dateSpan = activityPane->findChild<QComboBox*>("dateSpan");
    QVERIFY(dateSpan != nullptr);
    if (dateSpan == nullptr)  // make static analyzer happy
        return;

    const auto testTooltip = [activityPane](const QtCharts::QAbstractBarSeries* barSeries) {
        activityPane->hovered(true, 0, barSeries->barSets()[0]);

        for (ModelType mt : { TrackModel::Length, TrackModel::MovingTime }) {
            QVERIFY2(activityPane->m_chart->toolTip().contains(TrackModel::mdName(mt)),
                     qUtf8Printable(TrackModel::mdName(mt)));
        }

        activityPane->hovered(false, 0, barSeries->barSets()[0]);
    };

    // Test month mode
    {
        activityPane->setDateSpan(ActivitySummaryPane::Span::Month); // per month
        activityPane->refreshChart(0); // force update, since otherwise it happens on a timer

        QCOMPARE(chart->series().size(), 1);  // always a single series. Bars are the BarSets

        const auto* barSeries = qobject_cast<const QtCharts::QAbstractBarSeries*>(chart->series().constFirst());
        QVERIFY(barSeries != nullptr);
        // QVERIFY above will abort the test on nullptr, but this makes the static analyzer happy.
        if (barSeries == nullptr)
            return;

        QCOMPARE(barSeries->count(), 3); // 3 total categories, one per tag

        for (const auto* set : barSeries->barSets())
            QCOMPARE(set->count(), 2); // 2 stacked bars.

        // Test tooltips
        testTooltip(barSeries);
    }

    {
        dateSpan->setCurrentIndex(int(ActivitySummaryPane::Span::Week));
        QVERIFY(activityPane->m_dateSpan == ActivitySummaryPane::Span::Week);

        // Test week mode: should have 3 bars now instead of 2
        activityPane->setDateSpan(ActivitySummaryPane::Span::Week); // per month
        activityPane->refreshChart(0); // force update, since otherwise it happens on a timer

        const auto* barSeries = qobject_cast<const QtCharts::QAbstractBarSeries*>(chart->series().constFirst());
        QVERIFY(barSeries != nullptr);
        if (barSeries == nullptr)
            return;

        QCOMPARE(barSeries->count(), 3); // 3 total categories, one per tag

        for (const auto* set : barSeries->barSets())
            QCOMPARE(set->count(), 3); // 3 stacked bars.

        // Test tooltips
        testTooltip(barSeries);
    }

    // TODO: test non-stacked bar modes, such as average speed
}

void TestZtgps::add_point_mode() const
{
    {
        createMainWindowPrivate(m_mainWindow_private);
       //  m_mainWindow_private->close();
        deleteMainWindowPrivate(m_mainWindow_private);
    }

    app().newSession();
    createMainWindowPrivate(m_mainWindow_private);

    auto* trackPane = m_mainWindow_private->findPane<TrackPane>();
    QVERIFY(trackPane != nullptr);

    auto* trackMap = m_mainWindow_private->findPane<TrackMap>();
    QVERIFY(trackMap != nullptr);

    auto* mapPane = m_mainWindow_private->findPane<MapPane>();
    QVERIFY(mapPane != nullptr);

    auto* moveMode         = m_mainWindow_private->findChild<QAction*>("action_Map_Move_Mode");
    auto* addPointsMode    = m_mainWindow_private->findChild<QAction*>("action_Add_Points_Mode");
    auto* selectPointsMode = m_mainWindow_private->findChild<QAction*>("action_Select_Points_Mode");

    QCOMPARE(app().trackModel().rowCount(), 0);

    QVERIFY(moveMode != nullptr);
    QVERIFY(addPointsMode != nullptr);
    QVERIFY(selectPointsMode != nullptr);

    QVERIFY(moveMode != nullptr && moveMode->isChecked());
    QVERIFY(moveMode != nullptr && moveMode->isEnabled());
    QVERIFY(addPointsMode != nullptr && !addPointsMode->isChecked());
    QVERIFY(addPointsMode != nullptr && !addPointsMode->isEnabled());
    QVERIFY(selectPointsMode != nullptr && !selectPointsMode->isChecked());
    QVERIFY(selectPointsMode != nullptr && !selectPointsMode->isEnabled());

    // Create a new track
    auto* createNewTrack = m_mainWindow_private->findChild<QAction*>("action_Create_New_Track");
    QVERIFY(createNewTrack != nullptr);

    NewTrackDialog* newTrackDialog = &m_mainWindow_private->getNewTrackDialog();
    QVERIFY(newTrackDialog != nullptr);

    // Since the dialog is modal, we queue this up
    QTimer::singleShot(0, newTrackDialog, &QDialog::reject);
    createNewTrack->trigger();

    // After rejecting dialog, still no tracks
    QCOMPARE(app().trackModel().rowCount(), 0);
    QCOMPARE(undoMgr().undoCount(), 0);

    // Now accept
    QTimer::singleShot(0, newTrackDialog, &QDialog::accept);
    createNewTrack->trigger();

    // After accepting dialog, one tracks
    QCOMPARE(app().trackModel().rowCount(), 1);
    QCOMPARE(undoMgr().undoCount(), 1);
    QVERIFY(undoMgr().undo());
    QCOMPARE(app().trackModel().rowCount(), 0);
    QCOMPARE(undoMgr().undoCount(), 0);
    QCOMPARE(undoMgr().redoCount(), 1);

    // Accept again
    QTimer::singleShot(0, newTrackDialog, &QDialog::accept);
    createNewTrack->trigger();

    const QModelIndex newTrackIdx = app().trackModel().index(0, TrackModel::Name);

    // Verify non-empty name
    QVERIFY(app().trackModel().data(newTrackIdx, Util::RawDataRole).toString().length() > 10);

    PointModel* pm = app().trackModel().geoPoints(newTrackIdx);
    QVERIFY(pm != nullptr);

    const QModelIndex seg1Idx = pm->index(0, 0);
    QCOMPARE(pm->rowCount(), 1);
    QCOMPARE(pm->rowCount(seg1Idx), 0);

    // Add and select actions should be enabled now that we have a track
    QVERIFY(moveMode != nullptr && moveMode->isChecked());
    QVERIFY(moveMode != nullptr && moveMode->isEnabled());
    QVERIFY(addPointsMode != nullptr && !addPointsMode->isChecked());
    QVERIFY(addPointsMode != nullptr && addPointsMode->isEnabled());
    QVERIFY(selectPointsMode != nullptr && !selectPointsMode->isChecked());
    QVERIFY(selectPointsMode != nullptr && selectPointsMode->isEnabled());

    // Enable add points mode
    addPointsMode->trigger();
    QVERIFY(moveMode != nullptr && !moveMode->isChecked());
    QVERIFY(moveMode != nullptr && moveMode->isEnabled());
    QVERIFY(addPointsMode != nullptr && addPointsMode->isChecked());
    QVERIFY(addPointsMode != nullptr && addPointsMode->isEnabled());
    QVERIFY(selectPointsMode != nullptr && !selectPointsMode->isChecked());
    QVERIFY(selectPointsMode != nullptr && selectPointsMode->isEnabled());

    QTest::mouseClick(trackMap, Qt::LeftButton, Qt::NoModifier, QPoint(20, 20));
    QCOMPARE(pm->rowCount(seg1Idx), 1);
    QVERIFY(pm->getItem(pm->index(0, 0, seg1Idx))->test(PointItem::Flags::Select));

    QTest::mouseClick(trackMap, Qt::LeftButton, Qt::NoModifier, QPoint(30, 20));
    QCOMPARE(pm->rowCount(seg1Idx), 2);
    QVERIFY(!pm->getItem(pm->index(0, 0, seg1Idx))->test(PointItem::Flags::Select));
    QVERIFY(pm->getItem(pm->index(1, 0, seg1Idx))->test(PointItem::Flags::Select));

    // Re-enable move mode
    moveMode->trigger();
    QVERIFY(moveMode->isChecked());
    QVERIFY(moveMode->isEnabled());
    QVERIFY(!addPointsMode->isChecked());
    QVERIFY(addPointsMode->isEnabled());
    QVERIFY(selectPointsMode != nullptr && !selectPointsMode->isChecked());
    QVERIFY(selectPointsMode != nullptr && selectPointsMode->isEnabled());

    QTest::mouseClick(trackMap, Qt::LeftButton, Qt::NoModifier, QPoint(20, 20));
    QCOMPARE(pm->rowCount(seg1Idx), 2);

    // The new points shouldn't have elevations or times
    QVERIFY(!pm->getItem(pm->index(0, 0, seg1Idx))->hasEle());
    QVERIFY(!pm->getItem(pm->index(0, 0, seg1Idx))->hasTime());

    QVERIFY(undoMgr().undo());  // undo add
    QCOMPARE(pm->rowCount(seg1Idx), 1);
    QVERIFY(undoMgr().undo());  // undo add
    QCOMPARE(pm->rowCount(seg1Idx), 0);
    QVERIFY(undoMgr().redo());  // redo add
    QCOMPARE(pm->rowCount(seg1Idx), 1);
    QVERIFY(undoMgr().redo());  // redo add
    QCOMPARE(pm->rowCount(seg1Idx), 2);

    // Enable selection mode
    selectPointsMode->trigger();
    QVERIFY(!moveMode->isChecked());
    QVERIFY(moveMode->isEnabled());
    QVERIFY(!addPointsMode->isChecked());
    QVERIFY(addPointsMode->isEnabled());
    QVERIFY(selectPointsMode->isChecked());
    QVERIFY(selectPointsMode->isEnabled());

    QTest::mouseClick(trackMap, Qt::LeftButton, Qt::NoModifier, QPoint(18, 16)); // close to point 0
    QCOMPARE(pm->rowCount(seg1Idx), 2);
    QVERIFY(pm->getItem(pm->index(0, 0, seg1Idx))->test(PointItem::Flags::Select));
    QVERIFY(!pm->getItem(pm->index(1, 0, seg1Idx))->test(PointItem::Flags::Select));

    QTest::mouseClick(trackMap, Qt::LeftButton, Qt::NoModifier, QPoint(32, 16)); // close to point 1
    QCOMPARE(pm->rowCount(seg1Idx), 2);
    QVERIFY(!pm->getItem(pm->index(0, 0, seg1Idx))->test(PointItem::Flags::Select));
    QVERIFY(pm->getItem(pm->index(1, 0, seg1Idx))->test(PointItem::Flags::Select));

    const PointItem::Lat_t latBefore = pm->getItem(pm->index(1, 0, seg1Idx))->lat();
    const PointItem::Lon_t lonBefore = pm->getItem(pm->index(1, 0, seg1Idx))->lon();
    // simulate drag
    QTest::mousePress(trackMap, Qt::LeftButton, Qt::NoModifier, QPoint(32, 16));

    // For some reason, QTest::mouseMove isn't working on Kubuntu 18.04, even though QTest::mousePress and
    // QTest::mouseRelease are working fine.  We fake it instead.
    const bool mouseMoveWorkaround = true;
    if (mouseMoveWorkaround) {
        QMouseEvent event(QEvent::MouseMove, QPoint(50, 50), Qt::NoButton, Qt::NoButton, Qt::NoModifier);
        trackMap->mouseMoveEvent(&event); // invoke handler directly
    } else {
        QTest::mouseMove(trackMap, QPoint(50, 50));
    }

    QTest::mouseRelease(trackMap, Qt::LeftButton, Qt::NoModifier, QPoint(50, 50));

    // Verify point has moved
    QVERIFY(latBefore != pm->getItem(pm->index(1, 0, seg1Idx))->lat());
    QVERIFY(lonBefore != pm->getItem(pm->index(1, 0, seg1Idx))->lon());

    QVERIFY(undoMgr().undo());  // undo the drag
    QCOMPARE(pm->rowCount(seg1Idx), 2);

    // Verify point is back where it started
    QVERIFY(latBefore == pm->getItem(pm->index(1, 0, seg1Idx))->lat());
    QVERIFY(lonBefore == pm->getItem(pm->index(1, 0, seg1Idx))->lon());

    QVERIFY(undoMgr().redo());  // redo the drag

    // Verify point has moved
    QVERIFY(latBefore != pm->getItem(pm->index(1, 0, seg1Idx))->lat());
    QVERIFY(lonBefore != pm->getItem(pm->index(1, 0, seg1Idx))->lon());

    moveMode->trigger();  // reset to initial state
}

// Get most recent conf file
QString TestZtgps::sampleConfDir(uint32_t data_ver, uint32_t conf_ver)
{
    data_ver = (data_ver > 0) ? data_ver : cfgDataVersions.back().data_ver;
    conf_ver = (conf_ver > 0) ? conf_ver : cfgDataVersions.back().conf_ver;

    return QString("%1oldformat/0x%2/cfgDataVersion.%3")
            .arg(DATAROOT)
            .arg(data_ver, 0, 16)
            .arg(conf_ver);
}

QString TestZtgps::sampleConfFile(uint32_t data_ver, uint32_t conf_ver)
{
    const QString name = "ZTGPS-Sample.conf";

    return QFINDTESTDATA(sampleConfDir(data_ver, conf_ver) + QDir::separator() + name);
}

void TestZtgps::simplify_dialog() const
{
    const auto test = [](const char* info, QModelIndex idx, uint points, int undoCount) {
        const uint pointsCnt = app().trackModel().data(TrackModel::Points, idx, Util::RawDataRole).toUInt();

        if (pointsCnt != points || app().undoMgr().undoCount() != undoCount)
            QWARN(info);

        QCOMPARE(pointsCnt, points);
        QCOMPARE(app().undoMgr().undoCount(), undoCount);
        QVERIFY(app().undoMgr().isDirty() == (undoCount > 0));
    };

    const auto testPreview = [](const TrackSimplifyDialog* simplifyDialog,
                             int before, int after, int tracks) {
        QCOMPARE(simplifyDialog->simplifiedPointCount().m_beforePoints, before);
        QCOMPARE(simplifyDialog->simplifiedPointCount().m_afterPoints, after);
        QCOMPARE(simplifyDialog->simplifiedPointCount().m_tracks, tracks);
    };

    cfgDataWritable().setupDefaultConfig();  // to get the default tag set

    createMainWindowPrivate(m_mainWindow_private);
    QVERIFY(m_mainWindow_private);

    const QString confFile = sampleConfFile();

    QSettings settings(confFile, QSettings::IniFormat);
    CmdLine loadOld("testztgps", "1.0", BuildDate, { "testztgps", "--private-session", "--conf", confFile });
    loadOld.processArgs();   // set new session file

    MainWindow mainWindow(nullptr, &loadOld);

    // Find dialogs and widgets
    auto* simplifyDialog = mainWindow.findChild<TrackSimplifyDialog*>();
    QVERIFY(simplifyDialog != nullptr);

    auto* byDistRadio = simplifyDialog->findChild<QRadioButton*>("byDistRadio");
    QVERIFY(byDistRadio != nullptr);

    auto* filterDist = simplifyDialog->findChild<QDoubleSpinBox*>("filterDist");
    QVERIFY(byDistRadio != nullptr);

    auto* buttonBox = simplifyDialog->findChild<QDialogButtonBox*>("buttonBox");
    QVERIFY(buttonBox != nullptr);

    auto* adaptiveRadio = simplifyDialog->findChild<QRadioButton*>("byAdaptiveRadio");
    QVERIFY(adaptiveRadio != nullptr);

    auto* filterAdaptive = simplifyDialog->findChild<QDoubleSpinBox*>("filterAdaptive");
    QVERIFY(filterAdaptive != nullptr);

    // We can't focus normally since we haven't shown any windows.
    auto* trackPane = mainWindow.findPane<TrackPane>();
    QVERIFY(trackPane != nullptr);

    auto* pointPane = mainWindow.findPane<PointPane>();
    QVERIFY(pointPane != nullptr);

    const QModelIndex track0Idx = app().trackModel().index(0, 0);
    const QModelIndex track3Idx = app().trackModel().index(3, 0);

    trackPane->setFocus();
    mainWindow.newFocus(trackPane);

    // Single track selection simplify tests
    {
        simplifyDialog->show();  // open it
        trackPane->select(track0Idx); // select it in the track pane

        test("initial state", track0Idx, 166u, 0);

        // Set some parameters
        byDistRadio->setChecked(true);
        filterDist->setValue(1.0); // in meters

        // This setSelections exists because we don't use the TrackSimplifyDialog::exec() entry point
        const QModelIndexList selections = { track0Idx };
        simplifyDialog->setSelections(&app().trackModel(), &selections);

        emit buttonBox->accepted();

        QVERIFY(simplifyDialog->isVisible() == false);

        test("1m filter, 166->163", track0Idx, 163u, 1);

        simplifyDialog->show();  // open it again
        byDistRadio->setChecked(true);
        filterDist->setValue(50.0); // in meters

        // Preview mode makes no changes
        (void)app().trackModel().simplify(selections, simplifyDialog->simplifyParams(SimplifiableModel::Mode::Preview));
        simplifyDialog->hide();

        test("50m filter, preview", track0Idx, 163u, 1);

        simplifyDialog->show();
        simplifyDialog->accept();

        test("50m filter, execute", track0Idx, 119u, 2);

        app().undoMgr().undo();

        test("undo of 50m filter", track0Idx, 163u, 1);

        // Try 5m adaptive filter
        adaptiveRadio->setChecked(true);

        filterAdaptive->setValue(5.0); // in meters

        // Check preview data
        testPreview(simplifyDialog, 163, 138, 1);

        simplifyDialog->show();
        simplifyDialog->accept();

        test("5m adaptive", track0Idx, 138u, 2);

        app().undoMgr().undo();

        test("undo of 5m adaptive", track0Idx, 163u, 1);

        // Try 15s time filter
        auto* timeRadio = simplifyDialog->findChild<QRadioButton*>("byTimeRadio");
        QVERIFY(timeRadio != nullptr);
        timeRadio->setChecked(true);

        auto* filterTime = simplifyDialog->findChild<QTimeEdit*>("filterTime");
        QVERIFY(filterTime != nullptr);
        simplifyDialog->setTimeS(Dur_t(15)); // seconds

        // Check preview data
        testPreview(simplifyDialog, 163, 83, 1);

        simplifyDialog->show();
        simplifyDialog->accept();

        test("15s by time", track0Idx, 83u, 2);

        app().undoMgr().undo();

        test("undo 15s by time", track0Idx, 163u, 1);

        // Undo back to original state
        app().undoMgr().undo();

        test("undo to original state", track0Idx, 166u, 0);
    }

    // Simplify on multi-selected tracks
    {
        const QModelIndexList selections = { track0Idx, track3Idx };

        trackPane->select({track0Idx, track3Idx});
        // This setSelections exists because we don't use the TrackSimplifyDialog::exec() entry point
        simplifyDialog->setSelections(&app().trackModel(), &selections);

        // Try 5m adaptive filter
        byDistRadio->setChecked(true);
        filterDist->setValue(50.0); // in meters

        test("initial state track 0", track0Idx, 166u, 0);
        test("initial state track 1", track3Idx, 18443u, 0);
        testPreview(simplifyDialog, 166 + 18443, 119u + 12412u, 2);

        simplifyDialog->show();
        simplifyDialog->accept();

        test("5m adaptive track 0", track0Idx, 119u, 1);
        test("5m adaptive track 1", track3Idx, 12412u, 1);

        app().undoMgr().undo();

        // Verify both tracks are undone
        test("post-undo state track 0", track0Idx, 166u, 0);
        test("post-undo state track 1", track3Idx, 18443u, 0);
    }

    // test segement level simplify
    {
        PointModel* track0Pts = app().trackModel().geoPoints(track0Idx);
        const QModelIndex track0Seg0 = track0Pts->index(0, 0); // track 0 seg 0

        const QModelIndexList selections = { track0Seg0 };
        simplifyDialog->setSelections(track0Pts, &selections);

        pointPane->select(track0Seg0); // select track 0 seg 0

        byDistRadio->setChecked(true);
        filterAdaptive->setValue(50.0); // in meters

        test("trk0 seg0: initial state", track0Idx, 166u, 0);
        testPreview(simplifyDialog, 166, 119u, 1);

        simplifyDialog->show();
        simplifyDialog->accept();

        test("trk0 seg0: 50m dist", track0Idx, 119u, 1);

        app().undoMgr().undo();

        test("trk0 seg0: post undo", track0Idx, 166u, 0);
    }

    // test spoint level simplify
    {
        PointModel* track0Pts = app().trackModel().geoPoints(track0Idx);
        const QModelIndex track0Seg0 = track0Pts->index(0, 0); // track 0 seg 0

        // Append a set of point selections
        QModelIndexList selections;
        for (int r = 10; r < 40; ++r)
            selections.append(track0Pts->index(r, 0, track0Seg0));

        simplifyDialog->setSelections(track0Pts, &selections);

        byDistRadio->setChecked(true);
        filterAdaptive->setValue(50.0); // in meters

        test("trk0 seg0 pts[10-40]: initial state", track0Idx, 166u, 0);
        testPreview(simplifyDialog, 30, 21u, 1);

        simplifyDialog->show();
        simplifyDialog->accept();

        test("trk0 seg0 pts[10-40]: 50m dist", track0Idx, 157u, 1);

        app().undoMgr().undo();
        test("trk0 seg0 pts[10-40]: post undo", track0Idx, 166u, 0);
    }
}

// Common setup for DCP tests
std::tuple<TrackPane*, PointPane*, bool> TestZtgps::dcpSetup() const
{
    static const std::tuple<TrackPane*, PointPane*, bool> bad = { nullptr, nullptr, false };

    if (m_mainWindow_private == nullptr)
        return bad;

    auto* trackPane = m_mainWindow_private->findPane<TrackPane>();
    if (trackPane == nullptr)
        return bad;

    auto* pointPane = m_mainWindow_private->findPane<PointPane>();
    if (pointPane == nullptr)
        return bad;

    const QString mysticGpx = QFINDTESTDATA(DATAROOT "gps/mystic_basin_trail.gpx");
    Models orig;

    // Load into app models
    {
        GeoLoad loader(nullptr, app().trackModel(), app().waypointModel());

        if (!loader.load(mysticGpx))
            return bad;

        if (app().trackModel().rowCount() != 5)
            return bad;
    }

    // Start with clean undos
    undoMgr().clear();
    undoMgr().setDirty(false);

    return { trackPane, pointPane, true };
}

void TestZtgps::reverse_track_data() const
{
    createMainWindowPrivate(m_mainWindow_private);
    app().newSession();
}

void TestZtgps::reverse_track() const
{
    auto [ trackPane, pointPane, ok ] = dcpSetup();
    QVERIFY(trackPane != nullptr && pointPane != nullptr && ok);

    Models orig;

    // For comparison purposes, load to stack based model
    {
        const QString mysticGpx = QFINDTESTDATA(DATAROOT "gps/mystic_basin_trail.gpx");
        GeoLoad loader(nullptr, orig.m_trkModel, orig.m_wptModel);
        QVERIFY(loader.load(mysticGpx));
        QVERIFY(orig.m_trkModel.rowCount() == 5);

        // Start with clean undos
        undoMgr().clear();
        undoMgr().setDirty(false);
    }

    // Focus track pane so we can test enable/disable
    trackPane->setFocus();
    m_mainWindow_private->newFocus(trackPane);

    // Find reverse action
    auto* reverse = m_mainWindow_private->findChild<QAction*>("action_Reverse_Selection");
    QVERIFY(reverse != nullptr);
    trackPane->select(QModelIndex(), QItemSelectionModel::Clear);
    QVERIFY(!reverse->isEnabled());

    const QModelIndex  trackPaneTrk0 = trackPane->model()->index(2, 0);
    const QModelIndex  trk0 = Util::MapDown(trackPaneTrk0);
    const QModelIndex  origTrk0 = orig.m_trkModel.index(trk0.row(), 0); // matching index in orig

    QCOMPARE(undoMgr().undoCount(), 0);
    QCOMPARE(undoMgr().redoCount(), 0);

    // Select a track
    trackPane->select(trackPaneTrk0);
    QCOMPARE(trackPane->model()->index(0, TrackModel::Segments).data(Util::RawDataRole).toInt(), 1);
    QVERIFY(reverse->isEnabled());

    QCOMPARE(undoMgr().isDirty(), false);
    reverse->trigger();
    QCOMPARE(undoMgr().isDirty(), true);

    // Verify seg0 point count is the same after reversal
    const PointModel* loadedPtModel = app().trackModel().geoPoints(trk0);
    const PointModel* origPtModel = orig.geoPoints(origTrk0);

    const QModelIndex loadedSeg0 = loadedPtModel->index(0, 0);
    const QModelIndex origSeg0   = origPtModel->index(0, 0);

    const int segRowCount = loadedPtModel->rowCount(loadedSeg0);
    QCOMPARE(segRowCount, origPtModel->rowCount(origSeg0));

    const auto testRows = [&](bool reverse) {
        for (int ptRow = 0; ptRow < segRowCount; ++ptRow) {
            for (const auto mt : { PointModel::Time, PointModel::Lat, PointModel::Lon, PointModel::Hr, PointModel::Cad }) {
                const QModelIndex loadedPt = loadedPtModel->index(ptRow, mt, loadedSeg0);
                const QModelIndex origPt   = origPtModel->index(reverse ? segRowCount - ptRow - 1 : ptRow,
                                                                mt, origSeg0);

                if (mt == PointModel::Time) {
                    if (ptRow > 0)
                        QVERIFY(loadedPt.data(Util::RawDataRole).toDateTime() >
                                loadedPtModel->index(ptRow - 1, mt, loadedSeg0).data(Util::RawDataRole).toDateTime());
                } else {
                    QCOMPARE(loadedPt.data(Util::RawDataRole), origPt.data(Util::RawDataRole));
                }
            }
        }
    };

    testRows(true);

    // Test after undo
    QCOMPARE(undoMgr().undoCount(), 1);
    QCOMPARE(undoMgr().redoCount(), 0);
    QVERIFY(undoMgr().undo());
    QVERIFY(!undoMgr().undo());  // can't undo a 2nd time
    QCOMPARE(undoMgr().isDirty(), false);
    QCOMPARE(undoMgr().undoCount(), 0);
    QCOMPARE(undoMgr().redoCount(), 1);
    testRows(false);

    // Test after redo
    QVERIFY(undoMgr().redo());
    QVERIFY(!undoMgr().redo());  // can't redo a second time
    QCOMPARE(undoMgr().isDirty(), true);
    QCOMPARE(undoMgr().undoCount(), 1);
    QCOMPARE(undoMgr().redoCount(), 0);
    testRows(true);
}

void TestZtgps::split_merge_segments_data() const
{
    createMainWindowPrivate(m_mainWindow_private);
    app().newSession();
}

void TestZtgps::split_merge_segments() const
{
    auto [ trackPane, pointPane, ok ] = dcpSetup();
    QVERIFY(trackPane != nullptr && pointPane != nullptr && ok);

    trackPane->setFocus();
    m_mainWindow_private->newFocus(trackPane);

    // Find split action
    auto* split = pointPane->findChild<QAction*>("action_Split_Segments");
    QVERIFY(split != nullptr);

    // Test split/undo/etc behavior on tracks
    {
        // Select track in track pane
        const QModelIndex  trackPaneTrk0 = trackPane->model()->index(2, 0);
        trackPane->select(trackPaneTrk0);

        // Focus point pane
        pointPane->setFocus();
        m_mainWindow_private->newFocus(pointPane);

        QVERIFY(!split->isEnabled());

        PointModel* ptModel = pointPane->currentPoints();
        const QModelIndex seg0 = ptModel->index(0, 0);

        const int initSegPtCount = ptModel->rowCount(seg0);
        QCOMPARE(initSegPtCount, 166);
        QCOMPARE(pointPane->model()->rowCount(), 1);  // 1 segment

        // Select a few split points
        pointPane->select({ pointPane->model()->index(20, 0, seg0),
                            pointPane->model()->index(80, 0, seg0) },
                          QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
        QVERIFY(split->isEnabled());

        // Split
        QCOMPARE(undoMgr().isDirty(), false);
        split->trigger();
        QCOMPARE(undoMgr().isDirty(), true);

        // Should have 3 segments of provided sizes
        QCOMPARE(pointPane->model()->rowCount(), 3);  // 3 segment
        QCOMPARE(pointPane->model()->rowCount(pointPane->model()->index(0, 0)), 20);
        QCOMPARE(pointPane->model()->rowCount(pointPane->model()->index(1, 0)), 60);
        QCOMPARE(pointPane->model()->rowCount(pointPane->model()->index(2, 0)), 86);

        // Undo
        QCOMPARE(undoMgr().undoCount(), 1);
        QCOMPARE(undoMgr().redoCount(), 0);
        QVERIFY(undoMgr().undo());
        QCOMPARE(undoMgr().isDirty(), false);
        QCOMPARE(app().trackModel().isDirty(), false);
        QCOMPARE(undoMgr().undoCount(), 0);
        QCOMPARE(undoMgr().redoCount(), 1);
        QCOMPARE(pointPane->model()->rowCount(), 1);  // 1 segment
        QCOMPARE(pointPane->model()->rowCount(pointPane->model()->index(0, 0)), 166);

        // Redo
        QVERIFY(undoMgr().redo());
        QCOMPARE(undoMgr().isDirty(), true);
        QCOMPARE(app().trackModel().isDirty(), true);
        QCOMPARE(undoMgr().undoCount(), 1);
        QCOMPARE(undoMgr().redoCount(), 0);
        QCOMPARE(pointPane->model()->rowCount(), 3);  // 3 segment
        QCOMPARE(pointPane->model()->rowCount(pointPane->model()->index(0, 0)), 20);
        QCOMPARE(pointPane->model()->rowCount(pointPane->model()->index(1, 0)), 60);
        QCOMPARE(pointPane->model()->rowCount(pointPane->model()->index(2, 0)), 86);

        // Select segments to merge
        auto* merge = m_mainWindow_private->findChild<QAction*>("action_Merge_Selection");
        m_mainWindow_private->updateActionsDeferred(); // normally happens on a timer
        QVERIFY(merge != nullptr);
        QVERIFY(!merge->isEnabled());

        pointPane->select({ pointPane->model()->index(0, 0),
                            pointPane->model()->index(1, 0) },
                          QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
        m_mainWindow_private->updateActionsDeferred();  // normally happens on a timer
        QVERIFY(merge->isEnabled());

        // Clear undos before merge
        undoMgr().clear();
        undoMgr().setDirty(false);

        // Trigger merge
        merge->trigger();
        QCOMPARE(undoMgr().isDirty(), true);
        QCOMPARE(app().trackModel().isDirty(), true);

        // Should have 2 segments of provided sizes
        QCOMPARE(pointPane->model()->rowCount(), 2);  // 2 segments
        QCOMPARE(pointPane->model()->rowCount(pointPane->model()->index(0, 0)), 80);
        QCOMPARE(pointPane->model()->rowCount(pointPane->model()->index(1, 0)), 86);

        // Undo
        QCOMPARE(undoMgr().undoCount(), 1);
        QCOMPARE(undoMgr().redoCount(), 0);
        QVERIFY(undoMgr().undo());
        QCOMPARE(undoMgr().isDirty(), false);
        QCOMPARE(app().trackModel().isDirty(), false);
        QCOMPARE(undoMgr().undoCount(), 0);
        QCOMPARE(undoMgr().redoCount(), 1);
        QCOMPARE(pointPane->model()->rowCount(), 3);  // 1 segment
        QCOMPARE(pointPane->model()->rowCount(pointPane->model()->index(0, 0)), 20);
        QCOMPARE(pointPane->model()->rowCount(pointPane->model()->index(1, 0)), 60);
        QCOMPARE(pointPane->model()->rowCount(pointPane->model()->index(2, 0)), 86);

        // Check disabled when mixture of segments and points are selected
        pointPane->select({ pointPane->model()->index(0, 0),
                            pointPane->model()->index(1, 0),
                            pointPane->model()->index(8, 0, pointPane->model()->index(0, 0)) },
                          QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
        m_mainWindow_private->updateActionsDeferred();  // normally happens on a timer
        QVERIFY(!merge->isEnabled());

        // Check disabled when points from different segments are selected
        pointPane->select({ pointPane->model()->index(8, 0, pointPane->model()->index(0, 0)),
                            pointPane->model()->index(8, 0, pointPane->model()->index(1, 0)) },
                          QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
        m_mainWindow_private->updateActionsDeferred();  // normally happens on a timer
        QVERIFY(!merge->isEnabled());

        // Redo
        QVERIFY(undoMgr().redo());
        QCOMPARE(undoMgr().isDirty(), true);
        QCOMPARE(app().trackModel().isDirty(), true);
        QCOMPARE(undoMgr().undoCount(), 1);
        QCOMPARE(undoMgr().redoCount(), 0);
        QCOMPARE(pointPane->model()->rowCount(), 2);  // 2 segments
        QCOMPARE(pointPane->model()->rowCount(pointPane->model()->index(0, 0)), 80);
        QCOMPARE(pointPane->model()->rowCount(pointPane->model()->index(1, 0)), 86);
    }

    // Verify we can't split if TrackType==Rte
    {
        const QModelIndex  trackPaneRte0 = trackPane->model()->index(0, 0);
        trackPane->select(trackPaneRte0);

        // Focus point pane
        pointPane->setFocus();
        m_mainWindow_private->newFocus(pointPane);

        PointModel* ptModel = pointPane->currentPoints();
        const QModelIndex pointPaneSeg0 = ptModel->index(0, 0);

        pointPane->select({ pointPane->model()->index(2, 0, pointPaneSeg0) },
                          QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);

        QVERIFY(!split->isEnabled());
    }
}

void TestZtgps::merge_points_data() const
{
    createMainWindowPrivate(m_mainWindow_private);
    app().newSession();
}

void TestZtgps::merge_points() const
{
    auto [ trackPane, pointPane, ok ] = dcpSetup();
    QVERIFY(trackPane != nullptr && pointPane != nullptr && ok);

    trackPane->setFocus();
    m_mainWindow_private->newFocus(trackPane);

    // Select track in track pane
    const QModelIndex  trackPaneTrk0 = trackPane->model()->index(2, 0);
    trackPane->select(trackPaneTrk0);

    // Focus point pane
    pointPane->setFocus();
    m_mainWindow_private->newFocus(pointPane);

    auto* merge = m_mainWindow_private->findChild<QAction*>("action_Merge_Selection");
    m_mainWindow_private->updateActionsDeferred(); // normally happens on a timer
    QVERIFY(merge != nullptr);
    QVERIFY(!merge->isEnabled());

    const QModelIndex ptPaneSeg0 = pointPane->model()->index(0, 0);

    // Select some points to merge, in one contiguous group
    pointPane->select({ pointPane->model()->index(0, 0, ptPaneSeg0),
                        pointPane->model()->index(1, 0, ptPaneSeg0),
                        pointPane->model()->index(2, 0, ptPaneSeg0),
                        pointPane->model()->index(3, 0, ptPaneSeg0) },
                      QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
    m_mainWindow_private->updateActionsDeferred();  // normally happens on a timer
    QVERIFY(merge->isEnabled());

    QCOMPARE(pointPane->model()->rowCount(), 1);  // 1 segment
    QCOMPARE(pointPane->model()->rowCount(pointPane->model()->index(0, 0)), 166);

    // Merge the points
    QCOMPARE(undoMgr().isDirty(), false);
    QCOMPARE(app().trackModel().isDirty(), false);
    merge->trigger();
    QCOMPARE(undoMgr().isDirty(), true);
    QCOMPARE(app().trackModel().isDirty(), true);

    QCOMPARE(pointPane->model()->rowCount(), 1);  // 1 segment
    QCOMPARE(pointPane->model()->rowCount(pointPane->model()->index(0, 0)), 163);

    // Undo
    QCOMPARE(undoMgr().undoCount(), 1);
    QCOMPARE(undoMgr().redoCount(), 0);
    QVERIFY(undoMgr().undo());
    QCOMPARE(undoMgr().isDirty(), false);
    QCOMPARE(app().trackModel().isDirty(), false);
    QCOMPARE(undoMgr().undoCount(), 0);
    QCOMPARE(undoMgr().redoCount(), 1);

    QCOMPARE(pointPane->model()->rowCount(), 1);  // 1 segment
    QCOMPARE(pointPane->model()->rowCount(pointPane->model()->index(0, 0)), 166);

    // Redo
    QVERIFY(undoMgr().redo());
    QCOMPARE(undoMgr().isDirty(), true);
    QCOMPARE(app().trackModel().isDirty(), true);
    QCOMPARE(undoMgr().undoCount(), 1);
    QCOMPARE(undoMgr().redoCount(), 0);
    QCOMPARE(pointPane->model()->rowCount(), 1);  // 1 segment
    QCOMPARE(pointPane->model()->rowCount(pointPane->model()->index(0, 0)), 163);
}

void TestZtgps::delete_selection_data() const
{
    createMainWindowPrivate(m_mainWindow_private);
    app().newSession();
}

void TestZtgps::delete_selection() const
{
    auto [ trackPane, pointPane, ok ] = dcpSetup();
    QVERIFY(trackPane != nullptr && pointPane != nullptr && ok);

    trackPane->setFocus();
    m_mainWindow_private->newFocus(trackPane);

    auto* remove = m_mainWindow_private->findChild<QAction*>("action_Delete_Selection");
    QVERIFY(remove != nullptr);
    QVERIFY(!remove->isEnabled());

    QCOMPARE(undoMgr().isDirty(), false);
    QCOMPARE(app().trackModel().isDirty(), false);

    // Select track 3 in track pane
    trackPane->select(trackPane->model()->index(3, 0));
    QVERIFY(remove->isEnabled());

    // Remove
    QCOMPARE(app().trackModel().rowCount(), 5);
    remove->trigger();
    QCOMPARE(app().trackModel().rowCount(), 4);
    QCOMPARE(undoMgr().isDirty(), true);
    QCOMPARE(app().trackModel().isDirty(), true);
    QCOMPARE(undoMgr().undoCount(), 1);
    QCOMPARE(undoMgr().redoCount(), 0);

    // Select other two tracks
    trackPane->select({ trackPane->model()->index(0, 0),
                        trackPane->model()->index(1, 0) },
                      QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
    QVERIFY(remove->isEnabled());

    remove->trigger();
    QCOMPARE(app().trackModel().rowCount(), 2);
    QCOMPARE(undoMgr().isDirty(), true);
    QCOMPARE(app().trackModel().isDirty(), true);
    QCOMPARE(undoMgr().undoCount(), 2);
    QCOMPARE(undoMgr().redoCount(), 0);

    // Undo
    QVERIFY(undoMgr().undo());
    QCOMPARE(app().trackModel().rowCount(), 4);
    QCOMPARE(undoMgr().isDirty(), true);
    QCOMPARE(app().trackModel().isDirty(), true);
    QCOMPARE(undoMgr().undoCount(), 1);
    QCOMPARE(undoMgr().redoCount(), 1);

    // Undo again
    QVERIFY(undoMgr().undo());
    QCOMPARE(app().trackModel().rowCount(), 5);
    QCOMPARE(undoMgr().isDirty(), false);
    QCOMPARE(app().trackModel().isDirty(), false);
    QCOMPARE(undoMgr().undoCount(), 0);
    QCOMPARE(undoMgr().redoCount(), 2);

    // Select track to test point deletion
    const QModelIndex  trackPaneTrk0 = trackPane->model()->index(2, 0);
    trackPane->select(trackPaneTrk0);

    const QModelIndex ptSeg0 = pointPane->model()->index(0, 0);

    pointPane->setFocus();
    m_mainWindow_private->newFocus(pointPane);
    QVERIFY(!remove->isEnabled());
    QCOMPARE(pointPane->model()->rowCount(ptSeg0), 166);

    for (int row = 0; row<10; ++row)
        pointPane->select( pointPane->model()->index(row, 0, ptSeg0),
                           QItemSelectionModel::Select | QItemSelectionModel::Rows);
    QVERIFY(remove->isEnabled());

    QCOMPARE(undoMgr().isDirty(), false);
    QCOMPARE(app().trackModel().isDirty(), false);
    remove->trigger();
    QCOMPARE(pointPane->model()->rowCount(ptSeg0), 156);
    QCOMPARE(undoMgr().isDirty(), true);
    QCOMPARE(app().trackModel().isDirty(), true);

    // Undo
    QVERIFY(undoMgr().undo());
    QCOMPARE(pointPane->model()->rowCount(ptSeg0), 166);
    QCOMPARE(undoMgr().isDirty(), false);
    QCOMPARE(app().trackModel().isDirty(), false);

    // Redo
    QVERIFY(undoMgr().redo());
    QCOMPARE(pointPane->model()->rowCount(ptSeg0), 156);
    QCOMPARE(undoMgr().isDirty(), true);
    QCOMPARE(app().trackModel().isDirty(), true);
}

void TestZtgps::edit_speed_data() const
{
    createMainWindowPrivate(m_mainWindow_private);
    app().newSession();
}

void TestZtgps::edit_speed() const
{
    auto [ trackPane, pointPane, ok ] = dcpSetup();
    QVERIFY(trackPane != nullptr && pointPane != nullptr && ok);

    // Make TrackPane active
    trackPane->setFocus();
    m_mainWindow_private->newFocus(trackPane);

    QCOMPARE(undoMgr().isDirty(), false);
    QCOMPARE(app().trackModel().isDirty(), false);

    // Select track 3 in track pane
    trackPane->select(trackPane->model()->index(3, 0));

    // Make PointPane active
    pointPane->setFocus();
    m_mainWindow_private->newFocus(pointPane);

    auto* pointView = pointPane->findChild<QTreeView*>();
    QVERIFY(pointView != nullptr);

    QAbstractItemDelegate* speedDelegate = pointView->itemDelegateForColumn(PointModel::Speed);
    QVERIFY(speedDelegate != nullptr);

    const QModelIndex ptSeg0 = pointPane->model()->index(0, 0);

    const auto testData = [&](PointPane* pointPane, int column, int begin, int end, qreal value) {
        for (int row = begin; row<end; ++row) {
            const QVariant data = pointPane->model()->data(pointPane->model()->index(row, column, ptSeg0), Util::RawDataRole);
            if (data.toDouble() < value * 0.9999 || data.toDouble() > value * 1.0001)
                QWARN(qUtf8Printable(QString::number(data.toDouble())));
            QVERIFY(data.toDouble() >= value * 0.9999);
            QVERIFY(data.toDouble() <= value * 1.0001);
        }
    };

    const qreal kphToMps = 1.0/3.6;
    const qreal sToNs = 1e9;

    const auto testLen = [&](PointPane* pointPane) {
        int r = 0;
        for (const qreal len : { 37.5195, 53.3469, 53.3980, 56.3356, 58.3795, 49.8405, 48.3104, 52.4459, 51.6236 }) {
            testData(pointPane, PointModel::Length, r, r+1, len);
            ++r;
        }
    };

    const auto testOrig = [&](PointPane* pointPane) {
        int r = 0;
        for (const qreal speed : { 13.50702, 19.20489, 19.22329, 20.28080, 21.01663, 17.94259, 17.39174, 18.88052, 18.58449 }) {
            testData(pointPane, PointModel::Speed, r, r+1, speed * kphToMps);
            ++r;
        }

        testLen(pointPane);

        for (int r = 0; r < pointPane->model()->rowCount() - 1; ++r)
            testData(pointPane, PointModel::Duration, r, r+1, 10.0 * sToNs);
    };

    const auto testUpdated = [&](PointPane* pointPane, int begin, int end, qreal speed) {
        int r = 0;
        for (const qreal duration : { 10.0, 10.0, 19.223, 20.280, 21.016, 17.942, 10.0, 10.0, 10.0, 10.0 }) {
            testData(pointPane, PointModel::Duration, r, r+1, duration * sToNs);
            ++r;
        }

        testLen(pointPane);

        for (int r = begin; r < end; ++r)
            testData(pointPane, PointModel::Speed, r, r+1, speed * kphToMps);
    };

    // Simulate using the delegate to change speeds
    const auto setData = [&](PointPane* pointPane, int begin, int end, qreal value) {
        QModelIndex editingIdx;

        // multi-select several speed columns
        for (int row = begin; row<end; ++row) {
            editingIdx = pointPane->model()->index(row, PointModel::Speed, ptSeg0);
            pointPane->select( editingIdx, QItemSelectionModel::Select | QItemSelectionModel::Rows);
        }

        // simulate updating one of them
        QDoubleSpinBox* editor =
                qobject_cast<QDoubleSpinBox*>(speedDelegate->createEditor(pointView, QStyleOptionViewItem(), editingIdx));
        QVERIFY(editor != nullptr);

        speedDelegate->setEditorData(editor, editingIdx);
        editor->setValue(value);
        speedDelegate->setModelData(editor, pointPane->model(), editingIdx);
        speedDelegate->destroyEditor(editor, editingIdx);
    };

    QCOMPARE(undoMgr().isDirty(), false);
    QCOMPARE(app().trackModel().isDirty(), false);

    testOrig(pointPane);
    setData(pointPane, 2, 6, 10.0); // editing in config units, i.e, kph
    testUpdated(pointPane, 2, 6, 10.0);

    QCOMPARE(undoMgr().isDirty(), true);
    QCOMPARE(app().trackModel().isDirty(), true);
    QCOMPARE(undoMgr().undoCount(), 1);
    QCOMPARE(undoMgr().redoCount(), 0);

    QVERIFY(undoMgr().undo());
    QCOMPARE(undoMgr().isDirty(), false);
    QCOMPARE(app().trackModel().isDirty(), false);
    QCOMPARE(undoMgr().undoCount(), 0);
    QCOMPARE(undoMgr().redoCount(), 1);

    testOrig(pointPane);

    QVERIFY(undoMgr().redo());
    QCOMPARE(undoMgr().isDirty(), true);
    QCOMPARE(app().trackModel().isDirty(), true);
    QCOMPARE(undoMgr().undoCount(), 1);
    QCOMPARE(undoMgr().redoCount(), 0);

    testUpdated(pointPane, 2, 6, 10.0);

    setData(pointPane, 2, 3, 5.0); // editing in config units, i.e, kph
    testData(pointPane, PointModel::Duration, 2, 3, 38.446 * sToNs);
    testData(pointPane, PointModel::Duration, 3, 4, 20.280 * sToNs);

    QCOMPARE(undoMgr().isDirty(), true);
    QCOMPARE(app().trackModel().isDirty(), true);
    QCOMPARE(undoMgr().undoCount(), 2);
    QCOMPARE(undoMgr().redoCount(), 0);

    QVERIFY(undoMgr().undo());
    QCOMPARE(undoMgr().isDirty(), true);
    QCOMPARE(app().trackModel().isDirty(), true);
    QCOMPARE(undoMgr().undoCount(), 1);
    QCOMPARE(undoMgr().redoCount(), 1);

    testUpdated(pointPane, 2, 6, 10.0);

    QVERIFY(undoMgr().redo());
    QCOMPARE(undoMgr().isDirty(), true);
    QCOMPARE(app().trackModel().isDirty(), true);
    QCOMPARE(undoMgr().undoCount(), 2);
    QCOMPARE(undoMgr().redoCount(), 0);

    testData(pointPane, PointModel::Duration, 2, 3, 38.446 * sToNs);
    testData(pointPane, PointModel::Duration, 3, 4, 20.280 * sToNs);
}

void TestZtgps::person_model()
{
    PersonModel personModel;

    personModel.clear();
    QVERIFY(personModel.rowCount() == 0);

    personModel.appendRow();
    QVERIFY(personModel.rowCount() == 1);

    QVERIFY(personModel.setData(personModel.index(0, PersonModel::Name), "Tiglath-Pileser I", Util::RawDataRole));
    QVERIFY(personModel.setData(personModel.index(0, PersonModel::Weight), 60, Util::RawDataRole));
    QVERIFY(personModel.setData(personModel.index(0, PersonModel::Birthdate), QDate(1990, 1, 1), Util::RawDataRole));

    QVERIFY(personModel.data(personModel.index(0, PersonModel::MaxHR)).toUInt() > 60 &&
            personModel.data(personModel.index(0, PersonModel::MaxHR)).toUInt() < 200);

    QVERIFY(personModel.setData(personModel.index(0, PersonModel::MaxHR), 210, Qt::EditRole));

    QCOMPARE(personModel.data(personModel.index(0, PersonModel::MaxHR)).toUInt(), 210U);

    QVERIFY(personModel.setData(personModel.index(0, PersonModel::MaxHR), 0, Qt::EditRole));

    QVERIFY(personModel.data(personModel.index(0, PersonModel::MaxHR)).toUInt() > 60 &&
            personModel.data(personModel.index(0, PersonModel::MaxHR)).toUInt() < 200);

    QVERIFY(personModel.setData(personModel.index(0, PersonModel::FTP), 210, Qt::EditRole));
    QCOMPARE(personModel.data(personModel.index(0, PersonModel::FTP), Util::RawDataRole).toUInt(), 210U);
}

template <class MODEL> void TestZtgps::testModel()
{
    MODEL model;

    for (int mt = 0; mt < model.columnCount(); ++mt) {
        QVERIFY(!MODEL::mdName(mt).isEmpty());
        QVERIFY(!MODEL::mdWhatsthis(mt).isEmpty());
        QVERIFY(!MODEL::mdTooltip(mt).isEmpty());

        // TODO: C++20: use std::popcount to test number of set bits
        // There must be two set bits: one for H alignment, one for V alignment.
        QCOMPARE(unsigned(std::bitset<8>(MODEL::mdAlignment(mt)).count()), 2U);
    }
}

void TestZtgps::model_metadata()
{
    testModel<ClimbModel>();
    testModel<FilterModel>();
    testModel<GeoLocModel>();
    testModel<PersonModel>();
    testModel<PointModel>();
    testModel<TagModel>();

    testModel<TrackModel>();
    testModel<ViewModel>();
    testModel<WaypointModel>();
    testModel<ZoneModel>();
}

void TestZtgps::auto_import_data()
{
    QTest::addColumn<CfgData::AutoImportMode>("mode");
    QTest::addColumn<QString>("pattern");
    QTest::addColumn<QStringList>("tags");
    QTest::addColumn<CfgData::AutoImportPost>("post");
    QTest::addColumn<QString>("suffix");
    QTest::addColumn<QString>("command");
    QTest::addColumn<bool>("stdout");
    QTest::addColumn<int>("timeout");
    QTest::addColumn<int>("tracks");
    QTest::addColumn<int>("tmpGpx");      // post-import *.gpx count in original dir
    QTest::addColumn<int>("tmpTcx");      // post-import *.tcx count in original dir
    QTest::addColumn<int>("tmpSuffix");   // post-import renameSuffix count in original dir
    QTest::addColumn<int>("tmpTxt");      // post-import *.txt count in original dir
    QTest::addColumn<int>("oldGpx");      // post-import *.gpx count in copy(old) dir
    QTest::addColumn<int>("oldTcx");      // post-import *.tcx count in copy(old) dir
    QTest::addColumn<int>("exitCode");    // command expected exit code
    QTest::addColumn<QProcess::ProcessError>("error");

    QTest::addRow("disabled")
            << CfgData::AutoImportMode::Disabled
            << "*.fit *.gpx *.kml *.tcx"
            << QStringList()
            << CfgData::AutoImportPost::Backup
            << ".imported" // suffix
            << ""          // command
            << false       // stdout
            << 15          // timeout
            << 0           // tracks
            << 2           // tmpGpx
            << 1           // tmpTcx
            << 0           // tmpSuffix
            << 1           // tmpTxt
            << 0           // oldGpx
            << 0           // oldTcx
            << 0           // exitCode
            << QProcess::UnknownError
               ;

    QTest::addRow("gpx-backup")
            << CfgData::AutoImportMode::Menu
            << "*.gpx"
            << QStringList({"Gravel"})
            << CfgData::AutoImportPost::Backup
            << ".imported" // suffix
            << ""          // command
            << false       // stdout
            << 15          // timeout
            << 2           // tracks
            << 0           // tmpGpx
            << 1           // tmpTcx
            << 2           // tmpSuffix
            << 1           // tmpTxt
            << 0           // oldGpx
            << 0           // oldTcx
            << 0           // exitCode
            << QProcess::UnknownError
               ;

    QTest::addRow("gpx-tcx-backup")
            << CfgData::AutoImportMode::Startup
            << "*.gpx *.tcx"
            << QStringList({"Gravel", "Earth-01"})
            << CfgData::AutoImportPost::Backup
            << ".imported" // suffix
            << ""          // command
            << false       // stdout
            << 15          // timeout
            << 3           // tracks
            << 0           // tmpGpx
            << 0           // tmpTcx
            << 3           // tmpSuffix
            << 1           // tmpTxt
            << 0           // oldGpx
            << 0           // oldTcx
            << 0           // exitCode
            << QProcess::UnknownError
               ;

    QTest::addRow("gpx-tcx-move")
            << CfgData::AutoImportMode::Menu
            << "*.gpx *.tcx"
            << QStringList()
            << CfgData::AutoImportPost::Move
            << ".imported" // suffix
            << ""          // command
            << false       // stdout
            << 15          // timeout
            << 3           // tracks
            << 0           // tmpGpx
            << 0           // tmpTcx
            << 0           // tmpSuffix
            << 1           // tmpTxt
            << 2           // oldGpx
            << 1           // oldTcx
            << 0           // exitCode
            << QProcess::UnknownError
               ;

    QTest::addRow("gpx-delete")
            << CfgData::AutoImportMode::Menu
            << "*.gpx"
            << QStringList()
            << CfgData::AutoImportPost::Delete
            << ".imported" // suffix
            << ""          // command
            << false       // stdout
            << 15          // timeout
            << 2           // tracks
            << 0           // tmpGpx
            << 1           // tmpTcx
            << 0           // tmpSuffix
            << 1           // tmpTxt
            << 0           // oldGpx
            << 0           // oldTcx
            << 0           // exitCode
            << QProcess::UnknownError
               ;

    QTest::addRow("gpx-leave")
            << CfgData::AutoImportMode::Menu
            << "*.gpx"
            << QStringList()
            << CfgData::AutoImportPost::Leave
            << ".imported" // suffix
            << ""          // command
            << false       // stdout
            << 15          // timeout
            << 2           // tracks
            << 2           // tmpGpx
            << 1           // tmpTcx
            << 0           // tmpSuffix
            << 1           // tmpTxt
            << 0           // oldGpx
            << 0           // oldTcx
            << 0           // exitCode
            << QProcess::UnknownError
               ;

    QTest::addRow("badfile-txt")
            << CfgData::AutoImportMode::Menu
            << "*.txt"
            << QStringList()
            << CfgData::AutoImportPost::Delete
            << ".imported" // suffix
            << ""          // command
            << false       // stdout
            << 15          // timeout
            << 0           // tracks
            << 2           // tmpGpx
            << 1           // tmpTcx
            << 0           // tmpSuffix
            << 1           // tmpTxt
            << 0           // oldGpx
            << 0           // oldTcx
            << 0           // exitCode
            << QProcess::UnknownError
               ;

    const QString mysticGpx    = QFINDTESTDATA(DATAROOT "gps/mystic_basin_trail.gpx");

    QTest::addRow("cat-stdout")
            << CfgData::AutoImportMode::Menu
            << ""
            << QStringList()
            << CfgData::AutoImportPost::Delete
            << ".imported" // suffix
            << QString("cat ") + mysticGpx
            << true        // stdout
            << 15          // timeout
            << 5           // tracks
            << 2           // tmpGpx
            << 1           // tmpTcx
            << 0           // tmpSuffix
            << 1           // tmpTxt
            << 0           // oldGpx
            << 0           // oldTcx
            << 0           // exitCode
            << QProcess::UnknownError
               ;

    QTest::addRow("tcx-cat-stdout")
            << CfgData::AutoImportMode::Menu
            << "*.tcx"
            << QStringList()
            << CfgData::AutoImportPost::Delete
            << ".imported" // suffix
            << QString("cat ") + mysticGpx
            << true        // stdout
            << 15          // timeout
            << 6           // tracks
            << 2           // tmpGpx
            << 0           // tmpTcx
            << 0           // tmpSuffix
            << 1           // tmpTxt
            << 0           // oldGpx
            << 0           // oldTcx
            << 0           // exitCode
            << QProcess::UnknownError
               ;

    QTest::addRow("cat-no-file")
            << CfgData::AutoImportMode::Menu
            << ""
            << QStringList()
            << CfgData::AutoImportPost::Delete
            << ".imported" // suffix
            << QString("cat /not/a/file/at/all")
            << true        // stdout
            << 15          // timeout
            << 0           // tracks
            << 2           // tmpGpx
            << 1           // tmpTcx
            << 0           // tmpSuffix
            << 1           // tmpTxt
            << 0           // oldGpx
            << 0           // oldTcx
            << 1           // exitCode
            << QProcess::UnknownError
               ;

    QTest::addRow("gpx-bad-command")
            << CfgData::AutoImportMode::Menu
            << "*.gpx"
            << QStringList()
            << CfgData::AutoImportPost::Delete
            << ".imported" // suffix
            << "/not/a/command"
            << false       // stdout
            << 15          // timeout
            << 0           // tracks
            << 2           // tmpGpx
            << 1           // tmpTcx
            << 0           // tmpSuffix
            << 1           // tmpTxt
            << 0           // oldGpx
            << 0           // oldTcx
            << 255         // exitCode;
            << QProcess::FailedToStart
               ;

    QTest::addRow("gpx-timeout")
            << CfgData::AutoImportMode::Menu
            << "*.gpx"
            << QStringList()
            << CfgData::AutoImportPost::Delete
            << ".imported" // suffix
            << "sleep 10"  // command
            << false       // stdout
            << 1           // timeout
            << 0           // tracks
            << 2           // tmpGpx
            << 1           // tmpTcx
            << 0           // tmpSuffix
            << 1           // tmpTxt
            << 0           // oldGpx
            << 0           // oldTcx
            << -1          // exitCode;
            << QProcess::Crashed
               ;

    // Test var substitution and globbing
    QTest::addRow("cat-var-glob")
            << CfgData::AutoImportMode::Menu
            << ""
            << QStringList()
            << CfgData::AutoImportPost::Delete
            << ".imported" // suffix
            << "cat ${AutoImportDir}/Climb*gpx"
            << true        // stdout
            << 15          // timeout
            << 1           // tracks
            << 2           // tmpGpx
            << 1           // tmpTcx
            << 0           // tmpSuffix
            << 1           // tmpTxt
            << 0           // oldGpx
            << 0           // oldTcx
            << 0           // exitCode;
            << QProcess::UnknownError
               ;
}

void TestZtgps::auto_import() const
{
    QFETCH(CfgData::AutoImportMode, mode);
    QFETCH(QString, pattern);
    QFETCH(QStringList, tags);
    QFETCH(CfgData::AutoImportPost, post);
    QFETCH(QString, suffix);
    QFETCH(QString, command);
    QFETCH(bool, stdout);
    QFETCH(int, timeout);
    QFETCH(int, tracks);
    QFETCH(int, tmpGpx);
    QFETCH(int, tmpTcx);
    QFETCH(int, tmpSuffix);
    QFETCH(int, tmpTxt);
    QFETCH(int, oldGpx);
    QFETCH(int, oldTcx);
    QFETCH(int, exitCode);
    QFETCH(QProcess::ProcessError, error);

    createMainWindowPrivate(m_mainWindow_private);

    app().newSession();
    cfgDataWritable().reset();

    AppConfig& appConfig = m_mainWindow_private->m_appConfig;
    appConfig.show();
    appConfig.hide();

    auto* action_Auto_Import          = m_mainWindow_private->findChild<QAction*>("action_Auto_Import");
    auto* autoImportGroup             = appConfig.findChild<QGroupBox*>("autoImportGroup");
    auto* autoImportMode              = appConfig.findChild<QComboBox*>("autoImportMode");
    auto* autoImportDir               = appConfig.findChild<QLineEdit*>("autoImportDir");
    auto* autoImportPattern           = appConfig.findChild<QLineEdit*>("autoImportPattern");
    auto* autoImportTagButton         = appConfig.findChild<QToolButton*>("autoImportTagButton");
    auto* autoImportPost              = appConfig.findChild<QComboBox*>("autoImportPost");
    auto* autoImportBackupSuffix      = appConfig.findChild<QLineEdit*>("autoImportBackupSuffix");
    auto* autoImportBackupDir         = appConfig.findChild<QLineEdit*>("autoImportBackupDir");
    auto* autoImportCommand           = appConfig.findChild<QLineEdit*>("autoImportCommand");
    auto* autoImportStdout            = appConfig.findChild<QCheckBox*>("autoImportStdout");
    auto* autoImportTimeout           = appConfig.findChild<QSlider*>("autoImportTimeout");
    auto* labelAutoImportMode         = appConfig.findChild<QLabel*>("labelAutoImportMode");
    auto* labelAutoImportBackupSuffix = appConfig.findChild<QLabel*>("labelAutoImportBackupSuffix");
    auto* labelAutoImportBackupDir    = appConfig.findChild<QLabel*>("labelAutoImportBackupDir");
    auto* labelAutoImportStdout       = appConfig.findChild<QLabel*>("labelAutoImportStdout");
    auto* labelAutoImportTimeout      = appConfig.findChild<QLabel*>("labelAutoImportTimeout");
    auto* autoImportTagFrame          = appConfig.findChild<QFrame*>("autoImportTagFrame");

    QVERIFY(action_Auto_Import          != nullptr);
    QVERIFY(autoImportGroup             != nullptr);
    QVERIFY(autoImportMode              != nullptr);
    QVERIFY(autoImportDir               != nullptr);
    QVERIFY(autoImportPattern           != nullptr);
    QVERIFY(autoImportTagButton         != nullptr);
    QVERIFY(autoImportPost              != nullptr);
    QVERIFY(autoImportBackupSuffix      != nullptr);
    QVERIFY(autoImportBackupDir         != nullptr);
    QVERIFY(autoImportCommand           != nullptr);
    QVERIFY(autoImportStdout            != nullptr);
    QVERIFY(autoImportTimeout           != nullptr);
    QVERIFY(labelAutoImportMode         != nullptr);
    QVERIFY(labelAutoImportBackupSuffix != nullptr);
    QVERIFY(labelAutoImportBackupDir    != nullptr);
    QVERIFY(autoImportTagFrame          != nullptr);

    // Make static analyzer happy. For some reason, QVERIFY alone doesn't do the trick.
    if (action_Auto_Import          == nullptr ||
        autoImportGroup             == nullptr ||
        autoImportMode              == nullptr ||
        autoImportDir               == nullptr || 
        autoImportPattern           == nullptr ||
        autoImportTagButton         == nullptr ||
        autoImportPost              == nullptr ||
        autoImportBackupSuffix      == nullptr ||
        autoImportBackupDir         == nullptr ||
        autoImportCommand           == nullptr ||
        autoImportStdout            == nullptr ||
        autoImportTimeout           == nullptr ||
        labelAutoImportMode         == nullptr ||
        labelAutoImportBackupSuffix == nullptr ||
        labelAutoImportBackupDir    == nullptr ||
        labelAutoImportStdout       == nullptr ||
        labelAutoImportTimeout      == nullptr ||
        autoImportTagFrame          == nullptr)
        return;

    QTemporaryDir tmpDir;
    QTemporaryDir oldDir;

    // Copy some data files to the tmpDir
    {
        const QString parcours  = QFINDTESTDATA(DATAROOT "gps/Plougasou-plestin-parcours.gpx");
        const QString sampleTcx = QFINDTESTDATA(DATAROOT "gps/sample_file.tcx");
        const QString climbTest = QFINDTESTDATA(DATAROOT "gps/ClimbTest.gpx");
        const QString readme    = QFINDTESTDATA(DATAROOT "gps/sample_file.README.txt");

        const auto copy = [&tmpDir](const QString& file) {
            QFile(file).copy(tmpDir.path() + QDir::separator() + QFileInfo(file).fileName());
        };

        copy(parcours);
        copy(parcours);
        copy(sampleTcx);
        copy(climbTest);
        copy(readme);
    }

    // Normally we have a min timeout of 5s, but for testing purposes,
    // allow 1s.
    autoImportTimeout->setMinimum(1);

    // set UI widgets
    autoImportMode->setCurrentIndex(int(mode));
    autoImportDir->setText(tmpDir.path());
    autoImportPattern->setText(pattern);
    appConfig.setAutoImportTags(tags);
    autoImportPost->setCurrentIndex(int(post));
    autoImportBackupDir->setText(oldDir.path());
    autoImportBackupSuffix->setText(suffix);
    autoImportCommand->setText(command);
    autoImportStdout->setChecked(stdout);
    autoImportTimeout->setValue(timeout);

    // We should properly use appConfig.accept() here, but it does some slow GUI things,
    // so to help test suite performance, we bypass it and directly update the cfgData()
    // That this bypasses the undo features, so we cannot test that, but it's tested
    // elsewhere.
    appConfig.updateCfgFromUI();   // ... appConfig.accept();
    m_mainWindow_private->updateActions(); // ... appConfig.accept();

    QVERIFY(action_Auto_Import->isEnabled() == (mode != CfgData::AutoImportMode::Disabled));

    if (mode == CfgData::AutoImportMode::Disabled) {
        const auto allWidgets = autoImportGroup->findChildren<QWidget*>();

        // all interace widgets should get disabled under this mode
        for (const auto* w : allWidgets) {
            if (w != autoImportMode && w != labelAutoImportMode &&
                (dynamic_cast<const QCheckBox*>(w)   != nullptr ||
                 dynamic_cast<const QComboBox*>(w)   != nullptr ||
                 dynamic_cast<const QLineEdit*>(w)   != nullptr ||
                 dynamic_cast<const QToolButton*>(w) != nullptr ||
                 dynamic_cast<const QLabel*>(w)      != nullptr))
                QVERIFY2(!w->isEnabled(), qUtf8Printable(w->objectName()));
        }
    } else {
        // Test some other enable states
        QCOMPARE(autoImportBackupSuffix->isEnabled(),      post == CfgData::AutoImportPost::Backup);
        QCOMPARE(labelAutoImportBackupSuffix->isEnabled(), post == CfgData::AutoImportPost::Backup);
        QCOMPARE(autoImportBackupDir->isEnabled(),         post == CfgData::AutoImportPost::Move);
        QCOMPARE(labelAutoImportBackupDir->isEnabled(),    post == CfgData::AutoImportPost::Move);
        QCOMPARE(labelAutoImportStdout->isEnabled(),       !command.isEmpty());
        QCOMPARE(labelAutoImportTimeout->isEnabled(),      !command.isEmpty());

        action_Auto_Import->trigger(); // trigger the auto-import

        // If we're running a sub-process, hang around for it to finish.
        if (!command.isEmpty()) {
            // wake up sometimes to let timers run, or the timeout won't happen.
            for (int x = 0; x < 100; ++x) {
                QTest::qWait(100);
                if (!m_mainWindow_private->m_autoImportTimer.isActive())
                    break;
            }

            if (exitCode >= 0)
                QCOMPARE(m_mainWindow_private->m_autoImportProcess.exitCode(), exitCode);

            QCOMPARE(m_mainWindow_private->m_autoImportProcess.error(), error);
        }
    }

    // Compare track count
    QCOMPARE(app().trackModel().rowCount(), tracks);

    // Test tags on imported tracks
    for (int row = 0; row < app().trackModel().rowCount(); ++row)
        QCOMPARE(app().trackModel().index(row, TrackModel::Tags).data(Util::RawDataRole).toStringList(),
                 tags);

    // Look for tags getting added to the tag frame
    QCOMPARE(autoImportTagFrame->findChildren<QLabel*>().count(), tags.count());

    // Test file residue in original tmpDir
    QCOMPARE(QDir(tmpDir.path()).entryInfoList({"*.gpx"}, QDir::Files).count(), tmpGpx);
    QCOMPARE(QDir(tmpDir.path()).entryInfoList({"*.tcx"}, QDir::Files).count(), tmpTcx);
    QCOMPARE(QDir(tmpDir.path()).entryInfoList({"*.txt"}, QDir::Files).count(), tmpTxt);
    QCOMPARE(QDir(tmpDir.path()).entryInfoList({QString("*") + suffix}, QDir::Files).count(), tmpSuffix);

    // Test file residue in oldDir where files may be moved
    QCOMPARE(QDir(oldDir.path()).entryInfoList({"*.gpx"}, QDir::Files).count(), oldGpx);
    QCOMPARE(QDir(oldDir.path()).entryInfoList({"*.tcx"}, QDir::Files).count(), oldTcx);
    QCOMPARE(QDir(oldDir.path()).entryInfoList({"*.txt"}, QDir::Files).count(), 0);
    QCOMPARE(QDir(oldDir.path()).entryInfoList({QString("*") + suffix}, QDir::Files).count(), 0);

    const auto testBadInput = [](QLineEdit* lineEdit, const char* text) {
        if (lineEdit->isEnabled()) {
            QVERIFY(lineEdit->styleSheet().isEmpty());
            lineEdit->setText(text);
            QVERIFY(!lineEdit->styleSheet().isEmpty());
        }
    };

    // Test colorization of text entries for bad dirs etc
    testBadInput(autoImportDir, "/this/path/does/not/exist");
    testBadInput(autoImportBackupDir, "/this/path/does/not/exist");
    testBadInput(autoImportCommand, "bad-command >foo");
}

void TestZtgps::cleanup()
{
}

QString TestZtgps::gold(const QString& suffix)
{
    return QFINDTESTDATA(gold() + QTest::currentTestFunction() + QDir::separator() + QTest::currentDataTag() + suffix);
}

int main(int argc, char *argv[])
{
    // Force timezone to GMT to normalize any QDateTime output
    qputenv("TZ", "GMT");

    App::setApplicationName(Appname);
    App::setApplicationDisplayName(Apptitle);
    App::setApplicationVersion(Version);

    // avoid any perturbations to user's home dir
    const QTemporaryDir home_tmpdir;
    qputenv("HOME", qUtf8Printable(home_tmpdir.path()));

    // Don't give test command line to our application
    std::array<const char*, 1> ztgpsArgv = { Appname };
    int ztgpsArgc = ztgpsArgv.size();

    CmdLine opts(Appname, Version, BuildDate, ztgpsArgc, ztgpsArgv.data());

    App app(ztgpsArgc, ztgpsArgv.data(), opts);
    if (const int rc = app.rc(); rc != 0)
        return Exit::code(rc);

    app.setProperty("test", true);
    QTest::setMainSourcePath(__FILE__, QT_TESTCASE_BUILDDIR);

    TestZtgps tester;
    return QTest::qExec(&tester, argc, argv);
}
