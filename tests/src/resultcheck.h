/*
    Copyright 2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef RESULTCHECK_H
#define RESULTCHECK_H

#include <optional>
#include <QString>
#include <QVector>
#include <QRegularExpression>

static const constexpr char* RcOK       = "OK";
static const constexpr char* RcBadRc    = "Bad RC";
static const constexpr char* RcProcess  = "Process status";
static const constexpr char* RcResource = "Resource not found";
static const constexpr char* RcGoldOut  = "Gold difference: stdout";
static const constexpr char* RcGoldErr  = "Gold difference: stderr";

// check string or file against regex, with specified min/max match counts
class ResultCheck {
public:
    ResultCheck(const QString& file, const QRegularExpression& regex, int min, int max = -1) :
        m_file(file), m_regex(regex), m_min(min), m_max(max == -1 ? min : max)
    { }

    ResultCheck(const QString& regex, int min, int max = -1) :
        ResultCheck("", QRegularExpression(regex, QRegularExpression::DotMatchesEverythingOption), min, max)
    { }

    ResultCheck(const QString& file, const QString& regex, int min, int max = -1) :
        ResultCheck(file, QRegularExpression(regex), min, max)
    { }

    ResultCheck(const QString& file, const ResultCheck& check) :
        ResultCheck(file, check.m_regex, check.m_min, check.m_max)
    { }

    std::optional<QString> operator()() const;
    std::optional<QString> operator()(const QString&) const;

    static const ResultCheck isGpx;
    static const ResultCheck isKml;
    static const ResultCheck isTcx;

private:
    QString            m_file;
    QRegularExpression m_regex;
    int                m_min;
    int                m_max;
};

class ResultCheckAll : public QVector<ResultCheck> {
public:
    using QVector::QVector;

    ResultCheckAll(const std::initializer_list<ResultCheck>& init, const QString& file) :
        QVector::QVector(init),
        m_file(file)
    { }

    std::optional<QString> operator()() const;
    std::optional<QString> operator()(const QString&) const;

private:
    QString m_file;
};

Q_DECLARE_METATYPE(ResultCheckAll)

#endif // RESULTCHECK_H
