/*
    Copyright 2020-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TESTZTGPS_H
#define TESTZTGPS_H

#include <optional>

#include <QtTest>
#include <QStringList>
#include <QTemporaryDir>
#include <QDir>
#include <QVector>

#include "src/version.h"
#include "src/util/cmdline.h"

class ResultCheckAll;
class TreeModel;
class MainWindow;
class TrackPane;
class PointPane;

class TestZtgps : public QObject
{
    Q_OBJECT

public:
    TestZtgps();
    ~TestZtgps() override;

private slots:
    static void usage_data();
    static void usage();             // test --help options

    void batch_err_data() const;
    static void batch_err();         // batch mode error tests

    void convert_data() const;
    static void convert();           // batch mode conversions

    void convert_cmd_data() const;
    static void convert_cmd();       // token testing of conversion with actual process launch

    void create_all_pane_classes_data() const;
    void create_all_pane_classes();  // test creating all pane types

    void model_tooltips() const;     // test tooltips for tracks, points, etc

    static void old_formats_data();
    static void old_formats();       // test loading of old data formats

    void gps_device_data() const;
    void gps_device() const;         // test detection of GPS devices

    void save_load() const;          // save/load models, and test some main window functionality

    void clipboard_data() const;
    void clipboard() const;

    static void model_load_benchmark_data();
    static void model_load_benchmark(); // benchmark loading of data files

    static void uiload_benchmark_data();
    static void uiload_benchmark();      // benchmark UI loading and creation from file

    void mainwin_creation_benchmark() const; // benchmark UI loading and creation from file

    void doc_dialog() const;             // test documentation dialog
    void tag_selector() const;           // tag selector
    void app_config() const;             // appconfig dialogs
    static void about_dialog();          // about dialog
    void gpsd_version_dialog() const;    // gpsd version list dialog
    void newpane_dialog() const;         // test new pane dialog

    // remove absolute path dependencies in strings
    static QString removeAbsPath(const QString& re, const QString& repl, const QString&);

    static void query_data();
    static void query();                 // query parsing

    static void query_match_benchmark(); // query benchmark
    static void query_parse_benchmark(); // query benchmark

    static void undo_view();             // undo/redo tests

    static void geopol_data();
    static void geopol_benchmark();      // geopol region benchmarks
    static void geopol();                // geopol region tests

    static void geoloc_data();           // Do this LAST before benchmarks, due to async geoloc load.
    static void geoloc();
    static void geoloc_benchmark();      // geoloc database benchmarks

    static void model_var_expand_data(); // from ldutils: we test here to have access to a TrackModel
    static void model_var_expand();      // ...

    static void model_text_edit_data();  // from ldutils: we test here to have access to a TrackModel
    static void model_text_edit();       // ...

    static void save_formats_data();     // test saving in various formats
    void save_formats();                 // ...

    void route_point_aux();              // test route point aux data

    void import_dialog_data();           // test import dialog
    void import_dialog();                // ...

    void export_dialog_data();           // test export dialog
    void export_dialog();                // ...

    void climb_analysis() const;         // test climb analysis pane
    void zone_analysis() const;          // test zone analysis pane
    void activity_summary() const;       // test activity summary pane

    void add_point_mode() const;         // add point mode

    void simplify_dialog() const;        // test track simplify dialog

    void reverse_track_data() const;     // test reversing track points
    void reverse_track() const;          // ...

    void split_merge_segments_data() const; // test split track
    void split_merge_segments() const;   // ...

    void merge_points_data() const;      // test point merging
    void merge_points() const;           // ...

    void delete_selection_data() const;  // delete track/point/etc
    void delete_selection() const;       // ...

    void edit_speed_data() const;        // test editing of speed in PointPanes
    void edit_speed() const;             // test editing of speed in PointPanes

    static void person_model();          // test person model

    static void model_metadata();        // test model metadata

    static void auto_import_data();      // test auto-import features
    void auto_import() const;            // ...

    void cleanup();                      // clean up

private:
    template <class MODEL> static void testModel();

    std::tuple<TrackPane*, PointPane*, bool> dcpSetup() const;

    static void writeTestFailInfo(const QString& infoTxt,
                                  const QString& stdoutGoldTxt, const QString& stdoutProgTxt,
                                  const QString& stderrGoldTxt = QString(), const QString& stderrProgTxt = QString());

    static QString gold() { return QString("../gold") + QDir::separator(); }
    static QString exe()  { return QString(".") + QDir::separator() + Appname; };

    static QString gold(const QString& suffix);  // gold file with suffix

    static void captureOutErr(QtMsgType, const QMessageLogContext&, const QString& msg);
    static void clearOutErr();

    void createMainWindowPrivate(MainWindow*&) const;
    static void deleteMainWindowPrivate(MainWindow*&);

    QString tmp(const QString& file) const { return m_tmpDir.filePath(file); }

    QString testPaneCreate() const;

    static QString testOldConfFile(bool loadModels = true);
    static QString testConvert();
    static QString testCmdLine();
    static QString testProcess();
    static QString testGeoLoc();
    static bool testQuery(const TreeModel*);
    static QString testOldDataFile();
    static QString testOutErrRc(const QString& stdout, const QString& stderr,
                                const QStringList& args, int rc);
    static bool modelContains(const TreeModel*, const QString& queryStr);    static std::optional<QString> testOldDataFile(TreeModel*, const QString& suffix);

    static QString   m_stdout;  // stdout capture for cmd line tests
    static QString   m_stderr;  // stderr capture for cmd line tests
    QTemporaryDir    m_tmpDir;  // scratch dir for temp files.  removed on destruction.
    mutable CmdLine  m_privateSessionCL;  // cmd line for private session creation
    static const QString m_testQuery;

    // Description of config files for tests which exercise loading old ones.
    struct cfgDataVersions_t {
        const char* path;
        uint32_t data_ver;
        uint32_t conf_ver;
        uint32_t expected_trk;
        uint32_t expected_wpt;
    };

    // Helpers to get paths to dir containing ZTGPS-Sample.conf
    static QString sampleConfDir(uint32_t data_ver = 0, uint32_t conf_ver = 0);

    // Helpers to get paths to ZTGPS-Sample.conf files
    static QString sampleConfFile(uint32_t data_ver = 0, uint32_t conf_ver = 0);

    static const QVector<cfgDataVersions_t> cfgDataVersions;

    // Hacky use of mutable, but whatever, it's just a test suite.
    mutable MainWindow* m_mainWindow_private;
};

#define DATAROOT "../data/"

#endif // TESTZTGPS_H
