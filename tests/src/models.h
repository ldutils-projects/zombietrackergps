/*
    Copyright 2020-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MODELS_H
#define MODELS_H

#include "src/core/app.h"
#include "src/core/trackmodel.h"
#include "src/core/pointmodel.h"
#include "src/core/waypointmodel.h"
// #include "src/core/geolocmodel.h"
#include "src/dev-io/gpsmodel.h"

// Convenience
struct Models {
    TrackModel    m_trkModel;
    PointModel    m_pointModel;
    WaypointModel m_wptModel;
    ViewModel     m_viewModel;
    FilterModel   m_filterModel;
    GpsModel      m_gpsModel;
//    GeoLocModel   m_geoLoc;

    const TreeModel* operator()(App::Model mt) const {
        switch (mt) {
        case App::Model::Track:     return &m_trkModel;
        case App::Model::View:      return &m_viewModel;
        case App::Model::Filter:    return &m_filterModel;
        case App::Model::Waypoint:  return &m_wptModel;
        case App::Model::Point:     return &m_pointModel;
        case App::Model::GpsDevice: return &m_gpsModel;
//        case App::Model::GeoLoc    return &m_geoLoc;
        default:                   return nullptr;
        }
    }

    PointModel* geoPoints(const QModelIndex& idx) { return m_trkModel.geoPoints(idx); }
    const PointModel* geoPoints(const QModelIndex& idx) const { return m_trkModel.geoPoints(idx); }

    TreeModel* operator()(App::Model mt) {
        return const_cast<TreeModel*>((*const_cast<const Models*>(this))(mt));
    }
};

#endif // MODELS_H
