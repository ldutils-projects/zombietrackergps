#!/bin/bash

usage() {
    echo 'Refreshes art assets from source files.'
    echo 'Run from directory containing 'source''
    echo
    echo '--dry-run     Show commands but do not execute them.'
    echo '--no-flags    Avoid recoding flags.'
    echo '--no-remove   Do not remove old files.'
    echo '--no-geopol   Do not create political boundary data.'
    echo '--no-art      Do not create art.'
    echo '--no-icons    Do not create icon data.'
    echo '--no-pubkey   Do not generate PGP key.'
    echo '--no-gpsimage Do not generate gpsimage data.'
    exit 1
}

startqrc() {
    local prefix="${2:-/art}"
    echo "<RCC>
    <qresource prefix=\"${prefix}\">" > "${1}"
}

endqrt() {
    echo '    </qresource>
</RCC>
' >> "$1"
}

preview=''
genflags=true
remove=true
icons=true
pubkey=true
genart=true

npssym='us-nps-symbols.qrc'
art='art.qrc'
artL='art-light.qrc'
artD='art-dark.qrc'
geopol='geopol.qrc'
gpsimage=true

for f in "$@"; do
    case "$f" in
        --help) usage;;
        --preview|--dry-run) preview=echo;;
        --no-flags)          genflags=false; remove=false;;
        --no-remove)         remove=false;;
        --no-geopol)         geopol='';;
        --no-art)            genart=false; remove=false;;
        --no-icons)          icons=false; remove=false;;
        --no-pubkey)         pubkey=false; remove=false;;
        --no-gpsimage)       gpsimage=false; remove=false;;
    esac
done
     
if [ ! -d source ]; then
    echo "source link not found"
    exit 5
fi

if [ ! -d data ]; then
    echo "data not found"
    exit 5
fi

if ! which potrace >/dev/null; then
    echo "potrace not found"
    exit 5
fi

if [ "$preview" != 'echo' ]; then
    $genart && startqrc "${art}"
    $genart && startqrc "${artL}"
    $genart && startqrc "${artD}"
    $genart && startqrc "${npssym}"
    [ -n "$geopol" ] && startqrc "${geopol}" "data"
fi

if [ -n "$geopol" ]; then
   echo 'GeoPol: ----------------------------------------------------------------------'

   geojson_to_dat="$(which geojson-to-dat)"

   if [ ! -x ${geojson_to_dat} ]; then
       echo "geojson-to-dat not found"
       exit 5
   fi

   if ! $preview ${geojson_to_dat} --ztgps -0 data/GeoJson/*_0.json -1 data/GeoJson/*_1.json -o geopol/geopol.dat; then
       echo "$(basename ${geojson_to_dat}) failed"
       exit 5
   fi

    [ "$preview" != 'echo' ] && echo "        <file>geopol/geopol.dat</file>" >> "${geopol}"
    [ "$preview" != 'echo' ] && echo '100%'
fi

if $genart; then
    echo 'Points: ------------------------------------------------------------------------'

    if [ "$preview" != 'echo' ]; then
        find 'points' -type f | sort | sed -e 's:^:        <file>:' -e 's:$:</file>:' >> "${art}"
    fi

    [ "$preview" != 'echo' ] && echo '100%'
fi

if $genart; then
    echo 'Tags: ------------------------------------------------------------------------'

    $remove && $preview /bin/rm -rf tags

    tagdarkfg='#f2f2f2'
    taglightfg='#4d4d4d'

    totalCount=$(find source/tags/{Activity,Category,Transport} -type f | wc -l)
    count=0

    find source/tags/{Activity,Category,Transport} -type f | sort | while read source; do
        target="${source#*/*/}"
        target="${target%.png}.svg"

        targetL="tags/Light/$target"
        targetD="tags/Dark/$target"
        aliasName="tags/$target"

        basename="$(basename "${source%png}svg")"
        svgheight=0.5

        if [ "$preview" = 'echo' ]; then
            $preview "convert \"$source\" ppm:- | potrace --height ${svgheight} -C \"$tagdarkfg\" --svg - -o -"
            $preview "convert \"$source\" ppm:- | potrace --height ${svgheight} -C \"$taglightfg\" --svg - -o -"
        else
            mkdir -p "$(dirname "$targetL")"
            mkdir -p "$(dirname "$targetD")"

            while [ $(jobs | wc -l) -gt $(nproc) ]; do sleep 0.5; done

            {
                $preview convert "$source" ppm:- | potrace --height ${svgheight} -C "$tagdarkfg" --svg - -o - | sed -e '/<metadata>/,/<\/metadata>/ d' -e 's/^ *//' > "$targetD"
                $preview convert "$source" ppm:- | potrace --height ${svgheight} -C "$taglightfg" --svg - -o - | sed -e '/<metadata>/,/<\/metadata>/ d' -e 's/^ *//' > "$targetL"
            } &

            [ "$preview" != 'echo' ] && echo "        <file alias=\"$aliasName\">${targetL}</file>" >> "${artL}"
            [ "$preview" != 'echo' ] && echo "        <file alias=\"$aliasName\">${targetD}</file>" >> "${artD}"
        fi

        [ "$preview" != 'echo' ] && echo -ne "$[++count*100/totalCount]%\r"
    done

    [ "$preview" != 'echo' ] && echo

    echo 'Tags/Other: --------------------------------------------------------------------'

    for dir in Misc Misc/Battery Misc/Food Misc/Medical Seasons Signs Weather; do
        $preview mkdir -p "tags/${dir}"
    done

    find source/tags/{Misc,Seasons,Signs,Weather} -type f | sort | while read source; do
        target="${source#*/}"
        $preview cp "$source" "$target"
        [ "$preview" != 'echo' ] && echo "        <file>${target}</file>" >> "${art}"
    done

    [ "$preview" != 'echo' ] && echo '100%'
fi

if $genart; then
    echo 'Flags: ------------------------------------------------------------------------'
    # Some flag files are very large as SVG, so we make little icon bitmaps instead.

    # TODO: somehow use gimp to batch-convert the XCF file

    totalCount=$(find 'source/tags' -type f -wholename '*/Flags/*' | wc -l)
    count=0

    find 'source/tags' -type f -wholename '*/Flags/*' | sort | while read source; do
        target="${source#*/}"
        target="${target%.???}"
        target="${target}.jpg"
        [[ "$target" = */Countries/* ]] && quality=85 || quality=80

        while [ $(jobs | wc -l) -gt $[$(nproc) * 2] ]; do sleep 0.5; done

        if $genflags; then
            [ "$preview" != 'echo' ] && mkdir -p "$(dirname "$target")"
            $preview convert -quality $quality -strip -resize 72x72 "$source" "$target" &
        fi

        [ "$preview" != 'echo' ] && echo "        <file>${target}</file>" >> "${art}"
        [ "$preview" != 'echo' ] && echo -ne "$[++count*100/totalCount]%\r"
    done
fi

if $icons; then
    echo 'Icons: ------------------------------------------------------------------------'
    if [ "$preview" != 'echo' ]; then
        for color in light dark; do
            qrc="icons-${color}.qrc"
            startqrc "${qrc}" "icons"
            find "icons/${color}/hicolor" -iname '*.svg' | sort | while read line; do
                echo "        <file alias=\"${line#*/*/}\">${line}</file>" >> "${qrc}"
            done
            endqrt "${qrc}"
        done
    fi

    [ "$preview" != 'echo' ] && echo '100%'
fi

if $gpsimage; then
    echo 'GPS Images: -------------------------------------------------------------------'

    startqrc "gps-images.qrc" "gps-images"

    totalCount=$(find 'source/gps-images' -type f -wholename '*.png' | wc -l)
    count=0
    find 'source/gps-images' -type f -wholename '*.png' | sort | while read source; do
        target="gps-images/$(basename "$source")"

        while [ $(jobs | wc -l) -gt $[$(nproc) * 2] ]; do sleep 0.5; done

        $preview convert -strip -rotate 90 -resize 256x48 "$source" "$target" &

        [ "$preview" != 'echo' ] && echo "        <file>${target}</file>" >> "gps-images.qrc"
        [ "$preview" != 'echo' ] && echo -ne "$[++count*100/totalCount]%\r"
    done

    endqrt "gps-images.qrc"

    [ "$preview" != 'echo' ] && echo '100%'
fi

if $genart; then
    echo 'Logos: ------------------------------------------------------------------------'
    if [ "$preview" != 'echo' ]; then
        find 'logos' -type f | sort | sed -e 's:^:        <file>:' -e 's:$:</file>:' >> "${art}"
    fi

    [ "$preview" != 'echo' ] && echo '100%'
fi

if $genart; then
    echo 'UI: ------------------------------------------------------------------------'
    if [ "$preview" != 'echo' ]; then
        find 'ui' -type f | sort | grep -v docs | sed -e 's:^:        <file>:' -e 's:$:</file>:' >> "${art}"
    fi

    [ "$preview" != 'echo' ] && echo '100%'
fi

if $genart; then
    echo 'NPS Symbols: -----------------------------------------------------------------'
    if [ "$preview" != 'echo' ]; then
        find 'us-nps-symbols' -type f | sort | sed -e 's:^:        <file>:' -e 's:$:</file>:' >> "${npssym}"
    fi

    [ "$preview" != 'echo' ] && echo '100%'
fi

if $pubkey; then
    echo 'Pubkey: ------------------------------------------------------------------------'
    if [ "$preview" != 'echo' ]; then
        gpg --export --armor ldztgps@khasekhemwy.net > '../data/pubkey.asc'
    fi
    
    [ "$preview" != 'echo' ] && echo '100%'
fi

if [ "$preview" != 'echo' ]; then
    $genart && endqrt "${art}"
    $genart && endqrt "${artL}"
    $genart && endqrt "${artD}"
    $genart && endqrt "${npssym}"
    [ -n "$geopol" ] && endqrt "${geopol}"
fi
