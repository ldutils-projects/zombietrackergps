#-----------------------------------------------------------------------
# Copyright 2023 Loopdawg Software
# 
# ZombieTrackerGPS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------

# Generic data
set(CPACK_GENERATOR "DEB;RPM")
set(CPACK_SOURCE_GENERATOR "External")

set(CPACK_PACKAGE_NAME "${EXENAME}")
set(CPACK_PACKAGE_VENDOR "Loopdawg Software")
set(CPACK_PACKAGE_VERSION "${PKGVERSION}")
set(CPACK_PACKAGE_INSTALL_DIRECTORY "${EXENAME}")
set(CPACK_PACKAGE_CONTACT "loopdawg <loopdawg@see.manpage.for.email>")
set(CMAKE_PROJECT_HOMEPAGE_URL "https://www.zombietrackergps.net/ztgps")
set(CPACK_PACKAGE_ICON "${CMAKE_SOURCE_DIR}/art/logos/projects/zombietrackergps.png")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Fitness and GPS track manager using Qt and KDE Marble maps")
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_SOURCE_DIR}/LICENSE.md")
set(CPACK_PACKAGE_DESCRIPTION "ZombieTrackerGPS is a fitness and GPS track manager for KDE.  It can import
several standard GPS formats such as GPX, TCX, and KML, and provides
sophisticated query and filtering abilities, a customizable user interface,
and integration with the KDE Marble Map for use with OpenStreetMap or other
map providers.  It is designed for outdoor sports and recreation activities
such as bicycling, hiking, snowboarding, light aircraft, etc, and includes
custom icons for many popular outdoor activities.")

# Source generation
set(CPACK_SOURCE_PACKAGE_FILE_NAME "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION}")
set(CPACK_SOURCE_IGNORE_FILES "/\\\\.git/" "/.*\\\\.json" "/.*\\\\.user")
set(CPACK_EXTERNAL_PACKAGE_SCRIPT "${LDUTILS_ROOT}/${CMAKE_INSTALL_DATAROOTDIR}/cmake/${LDUTILS}/pack-tar.cmake")

# DEB generation
if(${CMAKE_SIZEOF_VOID_P} EQUAL 8)
   set(CPACK_DEBIAN_PACKAGE_ARCHITECTURE "amd64")
elseif(${CMAKE_SIZEOF_VOID_P} EQUAL 4)
   set(CPACK_DEBIAN_PACKAGE_ARCHITECTURE "i386")
endif()

set(CPACK_DEBIAN_PACKAGE_DEPENDS "libmarblewidget-qt5-28 (>= 20.3.1), marble-data (>= 20.3.1), libqt5charts5 (>= 5.11.3), libqt5svg5 (>= 5.11.3), libqt5widgets5 (>= 5.11.3), libqt5gui5 (>= 5.11.3), libqt5xml5 (>= 5.11.3), libqt5core5a (>= 5.11.3), libstdc++6 (>= 8.3), libc6 (>= 2.28), libgcc-s1 | libgcc1")
set(CPACK_DEBIAN_PACKAGE_SUGGESTS "marble")
set(CPACK_DEBIAN_PACKAGE_SECTION "misc")
set(CPACK_DEBIAN_PACKAGE_PRIORITY "optional")
set(CPACK_DEBIAN_COMPRESSION_TYPE "xz")
set(CPACK_DEBIAN_FILE_NAME "DEB-DEFAULT")

# RPM generation
if(${CMAKE_SIZEOF_VOID_P} EQUAL 8)
   set(CPACK_RPM_PACKAGE_ARCHITECTURE "x86_64")
elseif(${CMAKE_SIZEOF_VOID_P} EQUAL 4)
   set(CPACK_RPM_PACKAGE_ARCHITECTURE "i386")
endif()

set(CPACK_RPM_PACKAGE_REQUIRES "libmarblewidget-qt5-28 >= 20.3.1, marble-data >= 20.3.1, libQt5Charts5 >= 5.11.3, libQt5Svg5 >= 5.11.3, libQt5Widgets5 >= 5.11.3, libQt5Gui5 >= 5.11.3, libQt5Xml5 >= 5.11.3, libQt5Core5 >= 5.11.3, libstdc++6 >= 8.3, glibc >= 2.28, libgcc_s1")
# set(CPACK_RPM_PACKAGE_SUGGESTS "marble")  # This is not yet supported in CPack's RPM generator
set(CPACK_RPM_PACKAGE_LICENSE "GPLv3")
set(CPACK_RPM_COMPRESSION_TYPE "xz")
set(CPACK_RPM_PACKAGE_GROUP "misc")
set(CPACK_RPM_PACKAGE_DESCRIPTION "${CPACK_PACKAGE_DESCRIPTION}")
set(CPACK_RPM_FILE_NAME "RPM-DEFAULT")
set(CPACK_RPM_PACKAGE_AUTOREQ "no")

include(CPack)
