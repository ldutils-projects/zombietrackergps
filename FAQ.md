# FAQ

### Q: Can ZombieTrackerGPS support Glonass / Galileo?

A: Yes. All that matters is that the load format is one of the supported ones. Currently, those are:

   * FIT 
   * GPX 
   * KML 
   * TCX

### Q: Can ZombieTrackerGPS support Google Maps?

A: Technically, yes, with no changes required. However, legally, this is prohibited by Google's TOS, and cannot be included. Please support open data sources such as [OpenStreetMap](https://wiki.openstreetmap.org/wiki/Donations) so that it will remain possible to create software like this without the permission of and commercial surveillance by advertising agencies.

### Q: Which widget toolkit does ZombieTrackerGPS use?

A: [Qt](https://www.qt.io/), for the [KDE](https://kde.org/) or [LXQt](https://en.wikipedia.org/wiki/Lxqt) desktops.

### Q: Can ZombieTrackerGPS be used on non-KDE desktops?

A: Yes, but it will require the Qt libraries to be installed, and libmarble from KDE. It will use the theme (icons, colors, etc) set for Qt applications, rather than Gnome in that case.

### Q: Can I still use ZombieTrackerGPS if no zombie apocalypse is in progress?

A: Yes.

### Q: Will ZombieTrackerGPS send my GPS tracks to data brokers or ad agencies?

A: No. Privacy of your data is a central design goal. All program data resides on your local disk.

### Q: Can ZombieTrackerGPS support the linux gpsd daemon?

A: ~~Not yet, but eventually.~~ Yes, as of [version 1.04](history.html#1.04).

### Q: Can ZombieTrackerGPS support non-Garmin GPS units?

A: Yes, as long as they can provide one of the supported file formats.

### Q: It's slow to pan around the map when I have many tracks.

A: This is a performance issue in the KDE Marble Map, which is not using hardware accelerated rendering. It may be possible to improve this in the future. In the meantime, it's best to create a track filter to show only the most recent weeks or months of data most of the time.

A: Note that as of [ZTGPS 1.02](history.html#1.02), a new configuration option is available to allow interactive map panning without drawing track lines. The lines will be drawn again once the movement stops. This can improve interactivity with a large number of tracks.

### Q: What is the filesystem path for the GPS data storage?

A: The default project is saved in   $HOME/.local/share/zombietrackergps/ However, you can save new projects to any location via the "Save As..." menu.

### Q: Can ZTGPS import data from GadgetBridge or other tools?

A: You should be able to import data from any tool which supports the common GPX format.  Note that as of [Version 1.12](history.html#1.12), ZTGPS can auto-import data from a directory or by running an arbitrary external tool to obtain the data.  See the *Configuration/AutoImport* section of the in-app documentation for details.

### Q: Can ZombieTrackerGPS visualize GPS tracks from other planets?

A: Yes. Martian, Lunar, Venusian, and other maps including Jovian and Saturnian moons are supported if installed on the system. ZTGPS has you covered.

### Q: Are all these questions *really* asked frequently?

A: No.

