#-----------------------------------------------------------------------
# Copyright 2023 Loopdawg Software
# 
# ZombieTrackerGPS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------

set(DATA_INSTALL_DIR "${CMAKE_INSTALL_DATAROOTDIR}/${EXENAME}")

install(DIRECTORY "${CMAKE_CURRENT_LIST_DIR}/sample" DESTINATION "${DATA_INSTALL_DIR}")
install(FILES "${CMAKE_CURRENT_LIST_DIR}/pubkey.asc" DESTINATION "${DATA_INSTALL_DIR}")
install(FILES "${CMAKE_CURRENT_LIST_DIR}/copyright" DESTINATION "${CMAKE_INSTALL_DOCDIR}")

# Modify and install the desktop icon
ldutils_install_desktop_file(EXENAME      "${EXENAME}"
                             ARTIFACT_DIR "${ARTIFACT_DIR}"
                             DESKTOP_NAME "${EXENAME}.desktop"
                             ICON_NAME    "${EXENAME}.png")
