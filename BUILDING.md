# Build Requirements

## Debian GNU/Linux 10 (buster)

* Libraries:

        sudo apt install libc6-dev libmarble-dev libqt5charts5-dev libqt5svg5-dev libstdc++-8-dev qtbase5-dev 

* Tools:

        sudo apt install build-essential cmake qttools5-dev-tools 

## Debian GNU/Linux 12 (bookworm)

* Libraries:

        sudo apt install libc6-dev libmarble-dev libqt5charts5-dev libqt5svg5-dev libstdc++-12-dev qtbase5-dev 

* Tools:

        sudo apt install build-essential cmake qttools5-dev-tools 

## openSUSE Leap 15.4

* Libraries:

        sudo zypper install glibc-devel libQt5Charts5-devel libQt5Concurrent-devel libQt5Core-devel libQt5Gui-devel libqt5-qtsvg-devel libQt5Widgets-devel libQt5Xml-devel libstdc++6-devel-gcc7 marble-devel 

* Tools:

        sudo zypper install pattern:devel_basis cmake-full libqt5-linguist 

## Arch Linux

* Libraries:

        sudo pacman --needed -S gcc glibc marble-common qt5-base qt5-charts qt5-svg 

* Tools:

        sudo pacman --needed -S base-devel cmake qt5-tools 

# Building

1. Build and install the ldutils library. See the ldutils BUILDING.md file for details.

	 By default, the build expects to find a **libldutils** directory as a sibling of the ZombieTrackerGPS build directory. That is where the ldutils install process puts it by default. This works if ldutils and ZombieTrackerGPS are siblings.  If another location is desired, the **LDUTILS_ROOT** cmake (not environment!) variable can be used to point to the alternate location.

2. Create and cd to a build tree.  This should be outside the source tree.

        mkdir build-ZombieTrackerGPS-Release && cd build-ZombieTrackerGPS-Release

3. Create the build system:

        cmake -DCMAKE_BUILD_TYPE=Release /path/to/ZombieTrackerGPS

4. Build.  LC_CTYPE may need to be set in some environments to allow filenames with non-ASCII characters.

        LC_CTYPE=UTF-8 cmake --build . -j $(nproc)

5. Install the build.

  * Any cmake version:

        sudo make install

  * cmake 3.15 and later can also use:

        sudo cmake --install .

# Running

	 See the LIBRARIES.md file for runtime dependencies.

