# Version History

## Version 1.15
* The old qmake build system has been removed, since Qt is deprecating qmake and recommends cmake instead.  Please see the updated BUILDING instructions. This will impact anyone building or packaging the software. The new cmake build is different, but less fragile.
* RPM package names now comply with the recommended distro styles. They were close before, but used an underscore where they should have used a dash.
* Updated out of date FAQ.md, BUILDING.md, and LIBRARIES.md.
* Build fixes for OpenSUSE. The build test matrix now covers OpenSUSE, as do BUILDING.md and LIBRARIES.md documentation.
* The new build system fixes false shared library dependencies for certain distros which resulted from the previous qmake package system.
* cpack is now be used to produce .deb, rpm, and .tar distro packages.
* Improve generation of localization translation files from source in build process. This will result in less version control spam.
* Build fixes for latest Arch.
* This release is otherwise functionally similar to 1.14, with a few very minor UI related fixes, and exists primarily to roll out the new build scheme.

## Version 1.14
* The Garmin GPX "DisplayColor" extension is now supported when importing or exporting GPX tracks. Note that this extension uses color *names*, such as "Blue", so less than 20 total colors are supported. The names will be saved on GPX export, if and only if the exact RGB values match. There is no nearest-match selection.
* If more than one GPS device appeared in the GPS import window, the wrong device could be used for import.
* There is a new export option to read and write a custom ZTGPS extension in GPX files to store ZTGPS specific data, such as track tags. Command line options are also available: see the manpage or --help-batch option.
* Fixed defect introduced in version 1.13 where the color dialog failed to set tag colors in the configuration window. Sorry about that.
* The acceleration calculation suffered from accuracy limitations arising from two calculated differentiations, and was never very useful. It has been removed from the per-point data to use that space for other features. It is also removed from data queries and units configuration.
* Fixed defect where water temperature imported from GPX files was stored as air temperature in ztgps.
* Individual track points can now store point names, comments, and other string data imported from GPX files. For size reasons, this data is stored out of band, and on demand. Since most points do not have string data, this avoids blowing up the per-point memory footprint for rarely used features.
* Some formerly in-band per-point data such as water depth, water temperature, and course/bearing fields from GPX files, are now also held out of band. This will increase the point memory footprint when using those features, but shrink it for common use cases. It also results in a 10% file size reduction in most cases. The import dialog has options to load, or not load, this data.
* There are new PointPane columns for Name, Comment, Description, Symbol, and Type. None are displayed by default, but they can be enabled with the "Show Columns" combobox in the Point Pane. The per-point tooltips will display comments which are shorter than a given threshold inline. Longer text can be displayed by hovering over that column. The threshold is configurable in Configuration/UI/Tooltips.
* Similarly, the track Notes field will now appear in track tooltips if it is shorter than the configured threshold.
* Fixed a bug which could cause a program crash if the configuration window was displayed after every track was deleted.
* Added some European style circular road sign graphics for tags.
* GPX file Routes can now be imported, and the import and export track dialogs have new options to control this feature. Routes are displayed in Track Panes, with a a type field of "Rte".  To show only routes, for example, you can type "Type == Rte" into the Track Pane query box. Routes will be preserved as Routes when exporting GPX files, but not when exporting KML or FIT files, where they will be exported as track data. Route shaping points are not yet supported.
* There is a new "Route" tag.
* There is a new option in the New Track dialog to create routes.
* Routes can now be converted to tracks, or vice versa, by editing the "Type" column in a Track Pane, which will display a combo box with the choices. This column was not previously editable.
* The import dialog now has an option to auto-apply a tag to routes from GPX files. Since routes are often used prospectively, it is convenient to auto-apply a tag to all of them on import. The new "Route" tag is used by default, but it can be selected.
* Improved behavior on tracks with zero time between consecutive points.
* Note: Qt version 5.15.8 improperly auto-sizes tooltips sometimes, truncating the text. I do not believe this is a ZTGSP bug, but it does impact ZTGPS tooltips sometimes. Often, the tooltip will display properly after another try.
* Auto-import temporary files were created in the application working directory, rather than a temporary directory. This could make auto-import fail if the current directory was not writable.
* Added support for GPSD library version 30 (API version 14).
* Updated PGP key (--pubkey)
* The official .deb builds are now produced on Debian 10, rather than Ubuntu 18.04. Ubuntu 18.04 is 5 years old and is no longer supported by ZTGPS for either execution or compilation. It may still compile there, but you are on your own. The minimum Qt version is now 5.11.3, from 2018.

## Version 1.13
* Usability: The in-app documentation browser now supports searching. The standard ZTGPS query language is available, e.g, with booleans, parentheses, and regular expressions. The page browser will subset to the patches matching the query, and matching items highlighted. Return moves to the next match.
* Usability: Similar search functionality is available in the program configuration window. The configuration UI elements will be searched.
* Editing: Point speed is obtained from collected per-point data or calculated from locations and timestamps. It is now possible to directly edit point speed, which will change the timestamps of all subsequent points in the track. See the new documentation page under Track Editing/Changing Speed for more information.
* Editing: Measured point speed can be unset, falling back to the distance vs time calculation.
* There is a new Reverse Track edit feature which does what it says on the tin.
* The track line pane was not updating when point data changed for the track being displayed. Now it does.
* Fixed handling of backslash-escaped quotes in query strings. Other minor fixes to query tokenization.
* In rare circumtances, it was possible to cause a crash with out-of-time-order points in a track.
* The Merge feature has been improved and expanded. Selecting multiple segments in a PointPane will merge those segments. Selecting multiple points will merge the points into an average of the selected points. Points cannot be merged across segments. The old "Merge Segments" feature was a subset of this improvement and has been removed.
* Track points are displayed during point selection (e.g, in a Point Pane) even if "Draw Track Points" is disabled in the Interactive Movement Behavior setting. That means you can see which point(s) you have selected even if points are disabled during map panning for performance reasons.
* Added missing units indicators for HR, Cad, and Temp, while editing data in a PointPane.
* If a track segment (not the points within) was selected in a Point Pane, the Zoom-To feature was was broken. It now works correctly.
* Minor improvements to map pane behavior after certain edit operations.
* Improvements to end-of-segment point data, to prevent speeds and distances from carrying across segments. Segments now fully divide the track.
* Removed TZ dependency from test suite.

## Version 1.12
* An Auto-Import feature now allows automatic importation of files from either external tools, or defined directories. This was a feature requested by several users. See the Auto-Import section under the Configuration area of the documentation for setup information.
* Redesign of the configuration dialog. The numerous cluttered tabs were becoming unwieldy. There is a new, tree based page selector. Many config pages have been decluttered to make them easier to understand. All settings are still available, but the organization is improved.
* The Simplify Track feature was not marking track data as dirty, so the track would not get saved to disk after a simplify operation unless something else subsequently marked it as dirty. Sorry about that. You may want to double check that any simplifications you made "stuck". The simplify feature is useful to save space when tracks have many more points than you really need.
* Track moving power is now calculated using only points which have non-zero power. For example, time spent coasting on a bicycle does not factor into the average power displayed for the track. This is a more intuitive number, and will yield somewhat higher values depending on the amount of time spent not producing power.
* Heart rate percentages are now calculated based on age as of the start of each individual track, rather than on current age for all tracks. This presents more sensible data for older tracks. The same is true for Training Zone charts using the %HR method. This only happens when using the auto-calculated Max HR in the "People" tab of the program configuration, which is (220-age). See the "Configuration/People" documentation section for details.
* The Simplify Track preview info now makes more sense when selecting multiple points rather than entire tracks.
* The Simplify dialog was not properly reacting to the Adaptive Mode radio button.
* The Filter and View panes now works more like the others: single click to activate an item, and double click to edit.
* New Tandem transport icon for tandem bicycles.
* New Recumbent transport icon for recumbent trikes.
* Fixed incorrect tooltip in Climb Analysis pane column headers.
* Fixed track line pane buffer overflow With tracks holding multiple coincident points in different track segments.
* Improved tooltip behavior over Activity Summary charts. The annoying teardown and redraw no longer happens.
* Improved tooltip presentation and content for the Zone Analysis pie chart slices.
* Status bar now displays the map mode icon (Move, Select, or Add Points). Note that you can right click the status bar to customize it.
* Added new 'Help/Visit ZombieTrackerGPS Web Site' menu to open the web site in the desktop default web browser.
* The "Help/Donations" link to the ZTGPS donation page in the About dialog was incorrect, and has been fixed.
* Documentation TOC entries were mistakenly non-localizable.
* Building: Fixed some clang-tidy warnings. Not all, but a lot. In a few cases, the static analyzer is off its rocker.

## Version 1.11
* The tag selector dialog now presents convenience quick-buttons showing tags from recently added tracks.
* A new Training Zone Analysis pane is available, which displays a pie chart displaying time spent in training zones across one or more tracks. The world is awash in training zone models, so there are presets available for 3, 5, and 7 zone models, and full customization is available in the program configuration area.
* Fixed text editor bug: copy and cut actions were not enabled after "select all" under recent Qt versions.
* Added support for gpsd API version 14 (libgps.so.29)
* New CargoBike transport icon. Because cargo bikes vary widely, there are no default power estimation parameters, but you can add them in the program configuration for your own case.
* Once the Max BPM value for a person was manually set in the program configuration, it could not easily be restored to the auto-computed (220-age) value by setting it to 0. This is fixed.
* A FTP ("Functional Threshold Power") can now be configured for each person. This is used for training zone calculations.
* Several panes which allow selecting subsets of points, such as the Climb Analysis pane and the new Training Zone pane, now behave better when changing pane focus. They will mirror their point selection to any Point Pane, but also maintain an internal list and restore it if the pane loses and regains focus. There are still a few rough edges here, but it is improved on the previous version.
* New Package and Shopping icons in Misc category, for utility and cargo cycling.
* Improved in-app documentation for track tagging features and tag editor.
* Compilation fixes for gcc 12 (e.g, recent Arch linux).
* Minor fixes to tooltip HTML validity.
* Fixed defect where tag icons in tooltips would sometimes display the wrong icon.
* Test suite no longer leaves temporary directory spam in /tmp.

## Version 1.10
* Fixed a bug introduced in V1.09 which failed to save secondary UI windows with the session.  Sorry about that.
* There is a new Simple View Pane, which allows free-form rich text (e.g, tables or paragraphs) display of track data with variable substitution. This lets you create customizable, simple views of the selected track data similar to the device displays in some handheld GPS units. See the in-app documentation for more details.
* The rich text editor now has an "Copy Html" context menu to copy HTML source to the system clipboard, and a "Paste HTML" menu to replace the current editor contents with HTML from the clipboard.
* The rich text editor now enables and disables cut, copy, and paste actions depending on selected text state.
* There is a new configuration option under "Map Display/Interactive Movement Behavior" to control map inertial movement. If checked, the map will have inertia during interactive movement. It is unchecked by default.
* For cycling, the average cadence calculation is now determined based only on pedaling time.  Non-pedaling coasting time is no longer factored in, which makes the average more sensible.
* Added Goto Lat/Lon dialog, accessible using the Ctrl+G keyboard shortcut or via the "View/Goto Lat Lon..." menu.
* The Map Theme selector in Map Panes now allows the enter key (on most platforms) to select the map theme.
* The Map Theme selector is now wider to better support high res displays.
* The Device Dialog list was disabled by default on newer Qt versions, after showing the dialog. This is fixed.
* Added a Patch icon in the Misc section, for noting tracks where the puncture gods were displeased.

## Version 1.09
* Added a new Climb Analysis Pane, which detects climbs and descents within tracks and displays related information.  If a Point Pane is displayed, the climbs can be clicked on to select that range of points, which will also display the range in any visible Line Pane.  Climbs and descents are visually indicated in the Climb Analysis Pane using colorized icons, for climbs exceeding 1200, 700, 400, 200, and 100 vertical meters with at least 3% average grade. (Sorry, Netherlands and Florida).  These thresholds can be changed in the new Climbs Colorizer tab in the configuration dialog.  The pane can display the steepest short (e.g, 100m) section, which is often more interesting than the average grade which includes gentler start and end sections.
* There are several new map interaction modes to allow interactive editing of tracks by clicking or dragging in Map Panes. These modes are controlled from the Edit menu or the toolbar, and can be used to interactively drag track points or add new points. See the Map Modes section of the program tutorial for more information.
* There is a new "Edit/Create New Track..." menu to create a new, empty track, which can be used with the new map editing modes to create a track from scratch by clicking on the map. Note that this can only create locations, not time stamps or elevations, so the resulting track will not have speed or other derived data.
* The point grade calculation is more robust against one or a few inaccurate elevation values, as are sometimes produced from GPS units.  This change reduces the incidence of preposterous min and max grade values arising from such anomalies. This has the side effect of also improving the max power estimations. It works best with an elevation filter size of at least a few points.  (The default is 5).
* Fixed a defect which could (rarely) cause a crash if the Line Pane's mouse position indicator exactly coincided with the first point in a segment after the first one in a multi-segment track.
* Using the mousewheel to zoom in Track Line Panes now centers the zoom on the cursor indicator, rather than the left side of the graph. It is now more convenient to zoom in on a particular area of interest without re-panning as you go. Note that the "Reset Zoom" context menu will restore the full view.
* If a <= 1.08 ZTGPS save was created on a Qt 5.9 based OS, the OS was updated to one using Qt 5.14, and the old save was loaded, the filter data would not successfully load. I believe that to be a Qt defect, but I have added a workaround in ZTGPS 1.09. The workaround requires that a save is made with V1.09 *before* updating to Qt 5.15.
* The original colorizer defaults reversed blue and green with respect to their meanings for ski or mountain bike slopes. These will be swapped upon load of old saves, if and only if the colors were not modifed by hand.
* Line pane range selection by shift clicking on a second point only worked if the second point was before the first. It now works in either selection order.
* Fixed the Activity Summary "Max Date Spans" setting in the configuration settings, which was not being saved and restored.
* The Line Pane now has a Zoom to Range context menu to zoom the X axis to just the selected range.
* The Line Pane now has an "Adjust Axis" context menu to set the X axis to start and end at round numbers such as "15.0 km" rather than "13.47 km". This is useful after zooming or panning to an arbitrary spot.
* Undo of certain point editing features was not updating the map display. The data was correct, but the Map Pane displayed old data until something else caused a redraw. This is fixed.
* The track location convolution feature has been removed. It did not work well with the new interactive editing modes. This also makes the in-memory storage of tracks smaller.
* Fix an Inf problem in the Track Line Pane on tracks which contain one and only one point.
* The Point Pane tooltip for range selections now omits rows with no data, for visual tidiness.
* Fixes for Qt 5.15 related to initial default session creation.

## Version 1.08
* The prior methods of adding and replacing new UI panes involved cumbersome menu traverals. These are still available, but a new, much friendlier method is available from the "Pane/Pane Dialog..." menu, or by pressing the Ctrl+P shortcut. This displays a dialog with previews of pane types, and allows common pane actions such as replacing an existing pane or opening in a new window. The search bar at the top can be used to find panes by name. Double-click and drag and drop behavior is available: see the popup tooltip or in-app documentation page for details.
* If multiple points are selected in a Point Pane, the tooltip over the selection will show aggregate properties of the range of points, such as min, average, and max grades, etc. If the mouse is hovered over a non-selected item, the normal single-point info tooltip is displayed as before.
* The Track Line Pane will now visually display the range of points selected in a Point Pane. Range indicator colors and widths can be configured in the program configuration window under the "Graph" tab.
* There is now a drag distance threshold in the Track Line Pane. This prevents interpreting clicks as tiny drag events, so single-click point selection is more robust. This avoids the previous behavior where click-to-select would sometimes be ignored.
* Shift clicking in a Point Pane will select the range between the selected point, and the shift-clicked point.  This is a handy way to select point ranges by visualizing elevation or other data.
* Added FatBike and Snowmobile transport tags. Note that fatbike tire rolling resistances vary widely: you can adjust it in the program configuration.
* The track notes rich text editor now has a right-arrow button on the bottom which will display a list of track variables that can be inserted into the note. The variables will be expanded from the track's data when the tooltip is displayed. See the in-app documentation for details.
* The toolbar and menu actions for pane movement are now enable/disabled as appropriate.
* The busy cursor is displayed for long undo operations, such as for window configuration changes.
* At qmake time, the project file now checks for ldutils, to present a friendly error pointing to the root cause if ldutils hasn't been compiled and "make install"-ed.
* The point colorizer now colors cycling cadences in Point Panes. Yellow, orange, or red are used to colorize either high cadences, or low cadances at high power.
* Build fixes for Qt 5.15 and Arch.
* Improved formatting of in-app documentation.
* Added new Average Grade and Average Elevation track pane columns. Neither are displayed by default.
* Unit suffixes with exponents, such as m/s^2, were not correctly parsed in queries. This is fixed.
* Exporting just selected (as opposed to all) tracks was broken. This is fixed.
* The Track Line Pane value legend no longer displays "nan" after changing the current track.
* The rich text editor will now focus the text area upon display, so you no longer have to click on it again after it appears. The cursor is set to the end of any present text.
* The rich text editor now has table editing features. See the in-app documentation under "Settings/Show Tutorial..." for details.
* Attempt #2 to prevent the Track Comparison Pane axis values from being clipped.

## Version 1.07
* A new Activity Summary pane can show an overview of activity over weeks, months, or years, broken out by activity tags in a bart chart, colorized by tag colors. This lets you easily see, for example, distance covered per month while visualizing how much of that distance is running vs road cycling vs mountain biking. Other data can be visualized such as total moving time for a month, or total vertical ascent. Only the first tag on each track is used for this purpose, to avoid double-counting tracks. This pane is not displayed in the default UI, but can be added using the normal pane menus. Bars can be clicked to select the tracks corresponding to a date range and tag, or shift-clicked to select the date range for all tags. Context menus are available to control aspects of the chart display.
* For UI space savings, the Track Comparison Pane no longer has a button for showing and hiding the graph axes. This functionality is still available from the pane's context menu.
* Track min and max speeds no longer display "inf" for the speed on tracks with no timestamps.
* Changing configuration settings from the "Settings/Configure..." dialog no longer collapses tree nodes in Track Detail Panes.
* To improve interactive performance when moving through entries in a Track Pane using the keyboard, Point Panes now update on a 0.25 second deferred timer rather than immediately. Interactivity can also be improved by setting "Display Active Track" in the Map Display tab of the configuration options in the "Interactive Movement Behavior" area.
* The tag selector dialog is now slightly wider by default, to prevent clipping longer tag names.
* Upgrades to the web site at: https://zombietrackergps.net/ztgps
* Improvements to Track Line Pane axis labels to reduce clipping in small chart sizes.
* The Track Line Pane's value legend has been improved. It will always display the time at the cursor, and now helpfully displays grade if elevations are being plotted.
* When the keyboard is use to pan a zoomed in Track Line Pan, the value legend now dynamically updates.
* Fix small status bar bug which caused time and distance icons to go missing.
* The GPS device import dialog entries would un-select themselves a few moments after being selected with the mouse, which was annoying. This no longer happens.
* The GPS device import dialog now displays a small image of the specific GPS device detected on the system. This process is imperfect and only works for a selection of devices. I own almost none of them, so I cannot verify most of them.

## Version 1.06
* Added command line batch / scriptable conversions between GPS file formats.  See the manpage, and "zombietrackergps --help" for details about this feature.
* Google's FloC mechanism is now blocked on the https://www.zombietrackergps.net/ztgps web site.  See https://www.eff.org/deeplinks/2021/03/googles-floc-terrible-idea for more info about FLoC.
* Drag and drop reordering within View panes and other tree lists now creates proper undos.
* Drag and drop of one or more files from an external file browser such as Dolphin is now supported to import supported formats (.gpx, .fit, etc). This is the same functionality provided by the File/Import From File... menu, but may be more convenient.
* View pane entries can be dragged to map panes to move to that view.
* Track pane entries can be dragged to map panes to zoom to one or more tracks.
* Waypoint pane entries can be dragged to map panes to zoom to the bounds of all dropped waypoints.
* Filter pane entries can be dragged to track panes to view the tracks selected by that filter.
* Added support for GPSD API version 11 (libgps.so.28)
* Added new "Help/Supported GPSD Versions...". This displays a dialog of which GPSD library versions are supported by this version of ZombieTrackerGPS. The versions found on the system at runtime are displayed with a checkmark.

## Version 1.05
* Support for waypoints.  There is a new Waypoint Pane, and they can be imported from and exported to GPX files.  If the waypoint supplies a symbol, ZTGPS will attempt to find a matching waypoint icon from its builtin set.  Otherwise, this can be done manually with the "Set Icon..." context menu. See the in-program documentation page for the Waypoint Pane for details.
* Main window hotkeys (e.g, 'z' to zoom to the active selection) are now installed on secondary windows as well.
* The "Export All Tracks" option in the export dialog is fixed.
* The program crash on exit if a secondary window had UI focus is fixed.
* The US National Park Sevice (USNPS) map symbols, and some in similar style, are now available for waypoints, view preset icons, or other purposes.
* If autosave on exit fails, the exit is aborted, to allow a chance to manually save to another path.
* Numbered backup files are now saved at most once per session, rather than once per save.
* Internal track point storage is more memory efficient by 40%, and on-disk track files are about 30% smaller.
* Added Cache tag icon for tracks related to geocaching.
* Sessions are now locked to prevent accidental conflicts with another running ZTGPS.
* Previous .png point icons are replaced by .svg's, which scale better.  Old saves will be autoconverted on load.
* Secondary window title can now be customized from their tab bar's context menu.  This is useful with KDE's task bar.
* WinterBike transport category added, with higher rolling resistance than other types of bicycles.  There is more per-tire-model variance here, so the exact values can be adjusted for your situation in the Tags tab of the Settings dialog.
* Quoted strings in queries now work correctly.
* Fixed crash in the KML format importer on some files.
* "~" is now a synonym for the regex operator '=~' in queries, "&&" for "&", and "||" for "|".
* Application focus is no longer stolen upon program launch.
* Added --private-session command line option to disable saves.  Useful for temporary sessions along with --conf.
* Undoing a UI change (e.g, adding or removing a tab) no longer clears the undo stack, and is available for redo.
* The project structure has changed. If you build from source, it's best to use a fresh tree.  See updated BUILDING.

## Version 1.04
* Added live capture support from GPSD.  See the new section in the "Importing GPS Data / Live Capture (GPSD)" section of the in-program documentation for details.
* Added boundary data for the states of India.  Indian states do not have flags per se, but state emblems will be displayed in lieu thereof.
* Tracks without elevations attached to points are now visible in Map Panes.
* Changing the font size with ctrl+/ctrl- or the program menu will now update any secondary windows.
* Added new tags: Snowshoe and Paddleboard.
* The people database (Settings/Configure.../People) now has columns for age and maximum heart rate.
* There are new track columns for min/avg/max heart rate as a % of maximum, if set as above.  This information will also be displayed in track tooltips.
* Upon track import, the selected active person will now be applied to the track, so person-specific power, energy, and HR data will be displayed. Previously this required a restart or re-selection of the active person.
* External links in the in-program documentation browser will now open in your desktop's defined external browser.  For example, Perl-style regular expressions now link to online Perl documentation.
* Documentation browser improvements: forward and back buttons, and hover-text for external URLs.
* On-disk geopolitical data now uses a custom binary format to remove the Arch build warnings about deprecated Qt APIs. This is also faster to parse.
* There are now translation files for 19 languages in the 'translations' directory of the source tree. These can be used with Qt's "linguist" tool for localizations.
* Added batch tile download dialog, with limited tile count. Please respect server bandwidth.

## Version 1.03
* Multiple tracks can now be merged into a single track using the "Merge Selection" menu or tool button.
* Tracks with excessive sample points can be simplified using the "Simplify Selection" menu or tool button. For example, there's little need to sample a hiking track once per second, and it adversely impacts storage and map performance. You can simplify multiple tracks at once from a track pane, or segments or point ranges of a single track in a point pane.
* Clicking in the line chart will now center the nearest point in the Map Pane, and select it in the Track Point pane.
* "About" dialog links (EFF, OSM, etc) now open in a web browser. Previously they were broken.
* The resource files now build no matter the user's desktop locale setting. Previously this failed under certain locales.
* Undo/Redo now display a descriptive statusbar message.
* Some missing tooltips and statusbar tips have been added for buttons and menus.
* Convenience: double clicking a GPS device in the import dialog now functions identically to clicking the "Import" button.
* Importing a FIT track with empty segments no longer crashes.
* Compilation fixes for Qt 5.15 on Arch linux to fix uses of APIs removed in Qt 5.15. Thanks to Arch users on gitlab for reports, testing, and patches.
* The program startup process is now better at re-applying the most recently used track filter, so the expected tracks will be displayed in map panes after startup.
* New tags under Seasons: Autumn, Spring, Summer, and Winter. Useful to augment activities which are not season specific, such as winter vs summer hiking, or viewing all winter activities regardless of sport.
* New tags under Misc: Firefighter and Wildfire. Useful for Hotshot crews, fire aviation, etc, as a nod to those fighting recent wildfires in Australia, Russia, Western USA, and other locales.
* New tags: Cold, Warm, Hot under Weather.
* Minor startup improvements to remove redundant track refreshes in map panes.
* Improvements to track selection on device import.
* Added "About" window and web site donation links for Inkscape and GIMP. Both are used in the creation of ZTGPS.

## Version 1.02
* Fixed crash in "Split Segment(s)" menu in Track Point Panes.
* Added configuration option under the "Map Display" tab to draw track lines only when the map is still.  This can improve interactive performance if you have many tracks.  This is located under the "Interactive Movement Behavior" box.  There is another option controlling whether to draw track points during interactive movement.
* Added Wind icon to Weather set.

## Version 1.01
* Added map search bar and controls above the map pane. These searches are performed against a local database and do not use online sources.  The database contains city and region names, mountains, rivers and lakes, parks, and forests, sometimes but not always in multiple languages. See the new "Map Queries" documentation page under "Help/Show Tutorial..." for more information.
* Added missing regional flag for Hauts-de-France.
* The active filter is now re-run after tracks are imported, so they will appear in the track list if they match the active filter.  This also applies after an undo operation which restores deleted tracks or other data.
* The import source (filename or GPS device name) is now available as a track column called "Source".
* New Bikepack activity icon.
* New transport icon for Gravel biking.  The icon is similar to the Road bike icon, but can be visually distinguished by the color, sloped top tube, and disc brakes.
* Improve grade computation for low speeds, such as cycling up very steep hills.
* Units configuration for heart rate and cadence.
* Auto-Flagging was not working on OpenSUSE due to an apparent difference in libmarble behavior from Debian.

## Version 1.00
* Added undo/redo for most things (track/point/view/filter changes, some UI state, the program config dialog state).
* Added separate (non-dirtying) map view undo.  View history can be traversed with '[' and ']' keys or the mouse forward and back buttons.
* The "New View" dialog is improved and auto-suggests flags.
* The status bar now has a context menu to configure which data it displays.
* The status bar can now show the hierarchical name or 2 letter ID of the region under the cursor.
* The single toolbar is now several separate ones, grouped by functionality, which can be hidden, shown, or moved separately.

## Version 0.97
* The tag specific units override was causing bad units to be used in all cases.  This is fixed.
* Automatic flag icons applied to tracks.  Only begin/end points are considered.  This may be slightly off near border areas due to size and space constraints.
* The status bar now shows the flags for the area under the mouse cursor.
* The status bar's lat/lon display now uses the user configured lan/lon format.
* Custom tooltips for Tag and Flag track columns to show specific data rather than the generic tooltip.
* Track pane was not resizing its font when the font was changed with "View/Enlarge Font" or "Shrink Font".
* New query operator "!~", which matches data NOT containing a given regular expression.  E.g, "Tags !~ Car|Bus" is equivalent to "(! Tags =~ Car|Bus)"
* New exclusive-or operator "^" in query language.
* New Misc icons for battery state, food, and medical (e.g. for injury tags)
* New Misc icon for bonking ("hitting the wall", running out of energy).
* Added many missing country level flags.  Almost all countries are now represented.
* Added missing state level flags for Mexico (0.96 only had a subset).
* Added regional (province/state/prefecture/oblast) flags for additional countries: Argentina, Austria, Belgium, Brazil, Croatia, Czech Republic, Egypt, Ethiopia, Hungary, Indonesia, Italy, Japan, Netherlands, Norway, Pakistan, Poland, Russia, Slovakia, South Africa, South Korea, Sweden, Switzerland, and Ukraine.
* Improvements to startup time.

## Version 0.96

* New "View/Reload Visible Area" menu to refresh visible map areas from network sources.
* Date comparisons in queries (such as "Begin_Date >= 01-Jan-2019") were broken, and now work.
* Added some missing UI tooltips.
* Fixed build warnings on g++ 9.2. It should compile with no errors or warnings on 9.2 now.

## Version 0.95

* Secondary windows are now kept on top of the main window by default.  This behavior is selectable via a tab-bar context menu on secondary windows: "Keep Window on Top".
* Add running-oriented min/mile and min/km reciprocal speed units.
* Allow tag-specific speed unit override.  E.g, min/mile for running, and mi/hr for cycling.
* Add tooltips to the Track Point pane showing data for each sample point.
* New activity icons for parachutes.
* New wheelchair and handcycle activity icons for disabled athletes.
* New Misc icon category (earth, internet, hills, eco, etc) from FreeSVG.org, under CC license.

## Version 0.90

* FIT format import support.  This should let many newer GPS devices work.
* Improved device sync to work with more devices (via "File/Import from Device...")
* Simple, single track at a time FIT exporter.
* Added new tag icons for Surf, Nordic, Soccer, HangGlide, and Horse.
* Status bar summary is now recalculated from scratch on pane focus.
* Fix power estimation for running and hiking.
* Fix missing average temperature calculation.
* Arch compilation fixes:
  * Remove use of QFontMetrics::width()
  * Don't use dpkg-architecture command on non-debian systems
  * Fix link issue against libmarblewidget (also effected newer Ubuntus)
* Add OpenStreetMap donation link in About window
* Add missing build dependency to BUILDING
* Minor typo and art asset fixes

## Version 0.80

* Initial public alpha.

